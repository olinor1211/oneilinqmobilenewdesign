package com.knx.inquirer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.rvadapters.RVAdapterRelatedArticle;
import com.knx.inquirer.utils.ApiService;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebNewsActivity extends AppCompatActivity {
    private static final String TAG = "WebNewsActivity";

    private WebView webView;
    private WebView webView2;
    private WebView insertWebView;
    private ImageView imageBack;
    private ImageView imageFont;
    private ImageView imageShare;
    private ImageView imageBookmark;
    private ImageView logo;
    private ImageView mMainImage, mArticleInsertImage;
    private TextView mSectionName, mTitle, mAuthor, mDate;
    private ShimmerFrameLayout mShimmerViewContainer;
//    private NestedScrollView mNestedScrollView;
    private ImageView bannerIvImage;
    private RecyclerView rvRelatedArticle;
    private RVAdapterRelatedArticle rvAdapter;
    private ConstraintLayout clArticleHolder, flRelatedContainer, clInsertContentHolder;
    private CarouselView carouselView, mArticleInsertCarousel;
    private SimpleExoPlayer exoPlayer3, exoPlayer4;
    private PlayerView videoArticle, mArticleInsertVideo;
    private TextView mArticleInsertTitle,mArticleInsertReadMore;
    private ImageListener imageListener, imageInsertListener;
    private TextView labelRelalatedArticle;




    //androidx
    private String adbanner;  //private int adbanner;
//    private Global.LoaderDialog loaderDialog;

    private long startTime, timeInMilliseconds = 0;
    private long currentTime = 0;
    private int timeCounter;
    private int fontSw = 0;
    private final int NOTIFICATION_PERMISSION_CODE = 10;
    private final int BOOKMARK_PERMISSION_CODE = 20;

    private String bannerAdLink;
    private String idStr;
    private String titleStr = "";
    private String imageStr = "";
    private String typeStr = "";
    private String pubdateStr = "";
    private String contentStr = "";
    private String newImage1tStr = "";
    private String contentStr2 = "";
    private String authorStr = "";
    private String customStr = "";
    private String bannerLink;
    private String bannerImage;
    private Boolean hasLoadedOnce = false;
    private String htmlStr = "";
    private String htmlStr2 = "";
    private String htmlStr3 = "";
    private String httpLinkUrl;
    private String sectionId;
    //Insert
    private String insertTitle="";
    private String insertType="";
    private String insertNewsId="";
    private String insertContent="";

    private boolean hasInsert = false;
    private boolean hasNewDailyKey = false;


//    private String fromMyInquirer = "";
//    private String nightMode =

    private ApiService apiService;
    private SharedPrefs sf;
    private Handler customHandler = new Handler();
    private List<String> imgItems;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DatabaseHelper helper;
    private int section = 0;
//    public List<BookmarksNewModel> relatedArticles = new ArrayList<>();
//    public List<Related> relatedArticles = new ArrayList<>();
    public List<JsonObject> relatedArticles = new ArrayList<>();
//    @Subscribe(threadMode = ThreadMode.POSTING)
//    public void onEvent(String msg) {
//    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: WebNewsActivityLC");
        sf.setWebNewsActOpen(false);
        setResult(Global.NEWS_ARTICLE);
        Global.isArticleViewed = false;
        if(!Global.isRelatedArticle)finish(); //Do not finish if activity is ReCreated
//        overridePendingTransition(R.anim.stay, R.anim.slide_out_left);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sf = new SharedPrefs(this);
        //NOTE: set the THEME before setContentView() //Note: Define your OWN (R.style.LightTheme //
        //android:textColor="?attr/metaTextColor"   android:textColor="?attr/textColor"  android:background="?attr/bgColor"
//        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) setTheme(R.style.DarkTheme);
//        else setTheme(R.style.LightTheme);
        if(sf.getNightMode())setTheme(R.style.DarkTheme);
        else setTheme(R.style.LightTheme);
        setContentView(R.layout.web_news_view);
        helper = new DatabaseHelper(this);
//        loaderDialog = new Global.LoaderDialog();
        sf.setWebNewsActOpen(true);
        apiService = ServiceGenerator.createService(ApiService.class);

//        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }

        try{
            if (Build.VERSION.SDK_INT < VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        }catch(Exception e){
            Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
        }

        /**Story Reads Counts **/
        if(sf.isNewUser()){  //default value of isNewUser is TRUE
            Log.d(TAG, "onCreate: isNewUser: " +sf.isNewUser());
            int count =  sf.getStoryReadsCount() + 1;
            sf.setStoryReadsCount(count);
            Log.d(TAG, "onCreate: StoryReadsCount: " + sf.getStoryReadsCount());
        }

        try {
            initViews();
            //Check if it is from INSERT:SuperShare sponsored: DONT DISPLAY SECTION_NAME
            if(Global.isInsertClicked){
                Global.isInsertClicked = false;
                mSectionName.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            Log.d(TAG, "onCreate: JSONException"+e.getMessage());
        }
        removeAd(); //RemoveFirst
        initViewListeners();

        //Check if From DeepLink
        if(getIntent().hasExtra("fromDeepLink")){
            retrieveForDeepLink();
            Log.d(TAG, "onCreate: fromDeepLink");
        }else{
            //Fetch Data Controller: Check if Recreated: FOR RELATED_ARTICLES Data
            if(Global.isRelatedArticle) fetchNewData(); //fetchNewData is FOR RELATED_ARTICLES Data
            else fetchIntentExtras();
            Log.d(TAG, "onCreate: fromFragment");
        }

        getSectionName(sectionId);
        loadViews();
//        getRelatedArticleList();

        checkIfBookmarked(idStr); //after fetch
        configWebViewSettings();
        if(hasInsert){
            configWebView2Settings(); //
        }

        if(httpLinkUrl!=null){
            Log.d(TAG, "onCreate: httpLinkUrl called");
            webView.loadUrl(httpLinkUrl);  //avoid loading of webArticle  ..generateWebArticle()
        }else{
            Log.d(TAG, "onCreate: No httpLinkUrl called");
            generateWebArticle();
            webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
            if(hasInsert){
                generateWeb2Article();
                generateInsertWebArticle();
                webView2.loadDataWithBaseURL("http:///android_asset/", htmlStr2, "text/html", null, null);
                insertWebView.loadDataWithBaseURL("http:///android_asset/", htmlStr3, "text/html", null, null);
            }
        }

//        loadHtml(); //REMOVE Temp: onProgressChanged move to MyWebViewClient

        initFontSettings();

        checkIfFiveStoryReads();

        Log.d(TAG, "SECTION_NAME_NOW: "+sf.getSectionName());
    }
    private void getSectionName(String sectionId) {
//        Gson gson2 = new Gson();
//        String actualData = gson2.toJson(Global.sectionList);
//        Log.d(TAG, "getSectionNameNOW: DATA "+ actualData);
//        Log.d(TAG, "getSectionNameNOW: NAME "+sf.getSectionName());
//        Log.d(TAG, "getSectionNameNOW: ID  "+sectionId);
//        Log.d(TAG, "getSectionNameNOW: SIZE  "+Global.sectionList.size());
        if(Global.sectionList.size()>0) {
            for(int x=0; x<Global.sectionList.size();x++){
                String id = String.valueOf(Global.sectionList.get(x).getId());
                if(id.equals(sectionId)){
                    String sectName = String.valueOf(Global.sectionList.get(x).getTitle());
                    sf.setSectionName(sectName);
                    Log.d(TAG, "getSectionNameNOW: "+sectName);
                }
            }
        }
    }

    private void loadViews() {
        //Hide SectionName if Not TYPE=0
        if(!typeStr.equals("0")) mSectionName.setVisibility(View.GONE);

        Log.d(TAG, "TYPE_ : "+typeStr);

//        setCarouselImageListener("");
        Log.d(TAG, "IMAGE_: "+ imageStr);

        //SECTION_NAME ChangeColor Controller
        setTitleColor(sf.getSectionName().toLowerCase()); //setTitleColor
        switch (sf.getSectionName().toLowerCase()) {
            case "property":
//                setTitleColor(Global.sectionProperty);
                section = R.drawable.inquirerproperty; ///????
                break;
            case "entertainment":
//                setTitleColor(Global.sectionEntertainment);
                section = R.drawable.inquirerentertainment;
                break;
            case "lifestyle":
//                setTitleColor(Global.sectionLifestyle);
                section = R.drawable.inquirerlifestyle;
                break;
            case "global nation":
                section = R.drawable.inquirerglobalnation; //???
                break;
            case "sports":
//                setTitleColor(Global.sectionSports);
                section = R.drawable.inquirersports;
                break;
            case "technology":
//                setTitleColor(Global.sectionTechnology);
                section = R.drawable.inquirertechnology;
                break;
            case "business":
//                setTitleColor(Global.sectionBusiness);
                section = R.drawable.inquirerbusiness;
                break;
            case "opinion":
//                setTitleColor(Global.sectionOpinion);
                section = R.drawable.inquireropinion_blue;
                break;
            case "board talk":
                section = R.drawable.inquirerboardtalk;
                break;

            case "motoring":
                section = R.drawable.inquirermobility;
                break;

            case "inquirer rebound":
                section = R.drawable.inquirerrebound;
                break;
            default:
                section = R.drawable.inquirernewsinfo; //???
                break;
        }

        //Display MainImage
        if(!imageStr.isEmpty()){
            switch (typeStr){  //switch (typeStr){
                case "4":
                    carouselView.setVisibility(View.VISIBLE);
                    mMainImage.setVisibility(View.GONE);
                    videoArticle.setVisibility(View.GONE);
//                    //TEST CAROUSEL
//                    imageStr = "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618920580_Covid%2019-1.jpg," +
//                            "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618910647_000_1UQ6RX.jpeg," +
//                            "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618916490_Front-Page141452-620x354.jpg," +
//                            "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618912845_a33b4a6125f7b3a1d62d316723cef69c.jpg";

//                   try{
                    showCarousel(imageStr); //setCarouselImageListener(imageStr,"main"); //showCarousel(imageStr); /
//                   }catch(Exception e){
//                       Log.d(TAG, "loadViews: Exception: "+e.getMessage());
//                   }
                    break;
                case "6":
                case "7":
                    //TEST VIDEO
//                    imageStr = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
                    videoArticle.setVisibility(View.VISIBLE);
                    mMainImage.setVisibility(View.GONE);
                    carouselView.setVisibility(View.GONE);

                    try{
                        showVideo(imageStr);
                    }catch(Exception e){
                        Log.d(TAG, "loadViews: Exception: "+e.getMessage());
                    }
                    break;
                default:
//                    setCarouselImageListener("","");
                    mMainImage.setVisibility(View.VISIBLE);
                    carouselView.setVisibility(View.GONE);
                    videoArticle.setVisibility(View.GONE);
                    //DISPLAY IMAGE depends on the AspectRatio of IMAGE from URL using .override()
                    //NOTE:  override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)) with height WRAP_CONTENT and width MATCH_PARENT with android:adjustViewBounds="true" android:scaleType="fitXY"
                   //Check aspectRatio first if its landScape View of Portrait

                    try{
                        showImage(imageStr, mMainImage);
                    }catch (Exception e){
                        Log.d(TAG, "loadViews: Exception: "+ e.getMessage());
                    }

//                    GlideApp
//                            .with(this)
//                            .load(imageStr)//  .priority(Priority.HIGH)
//                            .transition(withCrossFade())
//                            .apply(requestOptions().override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)) //.apply(requestOptions()) // .apply(requestOptions().override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL))
//                            .into(mMainImage);
//                    ViewCompat.setTransitionName(mMainImage, "mMainImage"); //setTransitionName

                break;
            }
        }else{
            mMainImage.setImageResource(section);
//            GlideApp
//                    .with(this)
//                    .load(section) // .load(imageStr)
////                    .transition(withCrossFade())
////                    .apply(requestOptions())
//                    .into(mMainImage);//
//            ViewCompat.setTransitionName(mMainImage, "mMainImage"); //setTransitionName
        }


        //Display InsertImage
        if(!newImage1tStr.isEmpty()){
            hasInsert = true;
            //Display Insert IF
//            if(!newImage1tStr.isEmpty()) hasInsert = true;
//            else clInsertContentHolder.setVisibility(View.GONE); //NO RELATED, HIDE IT

            switch (insertType){  //switch (typeStr){
                case "4":
                    mArticleInsertCarousel.setVisibility(View.VISIBLE);
                    mArticleInsertImage.setVisibility(View.GONE);
                    mArticleInsertVideo.setVisibility(View.GONE);
//                    //TEST CAROUSEL
//                    imageStr = "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618920580_Covid%2019-1.jpg," +
//                            "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618910647_000_1UQ6RX.jpeg," +
//                            "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618916490_Front-Page141452-620x354.jpg," +
//                            "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618912845_a33b4a6125f7b3a1d62d316723cef69c.jpg";
//                   try{
                    showInsertCarousel(newImage1tStr);//setCarouselImageListener(newImage1tStr,"insert"); //showInsertCarousel(newImage1tStr);
//                   }catch (Exception e){
//                       Log.d(TAG, "loadViews: Exception: "+ e.getMessage());
//                   }
                    break;


                case "3":
                case "6":
                case "7":
                    //TEST VIDEO
//                    imageStr = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
                    mArticleInsertVideo.setVisibility(View.VISIBLE);
                    mArticleInsertCarousel.setVisibility(View.GONE);
                    mArticleInsertImage.setVisibility(View.GONE);
                    try{
                        showInsertVideo(newImage1tStr);
                    }catch (Exception e){
                        Log.d(TAG, "loadViews: Exception: "+ e.getMessage());
                    }
                    break;
                default:
//                    setCarouselImageListener("","");
                    mArticleInsertImage.setVisibility(View.VISIBLE);
                    mArticleInsertCarousel.setVisibility(View.GONE);
                    mArticleInsertVideo.setVisibility(View.GONE);
                    //DISPLAY IMAGE depends on the AspectRatio of IMAGE from URL using .override()
                    //NOTE:  override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)) with height WRAP_CONTENT and width MATCH_PARENT with android:adjustViewBounds="true" android:scaleType="fitXY"
                    try{
                        showImage(newImage1tStr, mArticleInsertImage);
                    }catch (Exception e){
                        Log.d(TAG, "loadViews: Exception: "+ e.getMessage());
                    }

//                    GlideApp
//                            .with(this)
//                            .load(newImage1tStr)
//                            .transition(withCrossFade())
//                            .apply(requestOptions().override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL))//.apply(requestOptions()) //
//                            .into(mArticleInsertImage);
//                    ViewCompat.setTransitionName(mArticleInsertImage, "mArticleInsertImage"); //setTransitionName
                    break;
            }
        }else{
            clInsertContentHolder.setVisibility(View.GONE);
            mArticleInsertImage.setImageResource(section);
//            GlideApp
//                    .with(this)
//                    .load(newImage1tStr)
//                    .transition(withCrossFade())
//                    .apply(requestOptions())
//                    .into(mArticleInsertImage);//
//            ViewCompat.setTransitionName(mArticleInsertImage, "mArticleInsertImage"); //setTransitionName
        }

        //Display MainTitle,MainAuthor..
        mSectionName.setText(sf.getSectionName().toUpperCase()); //mSectionName.setText(sf.getSectionName());
//        mTitle.setText(titleStr.toUpperCase());
//        mAuthor.setText(authorStr.toUpperCase());
//        mDate.setText(pubdateStr.toUpperCase());
        //Display InsertTitle
        mArticleInsertTitle.setText(insertTitle.toUpperCase());
        //
        if(isValidated(titleStr)) {
            mTitle.setVisibility(View.VISIBLE);
            mTitle.setText(titleStr.toUpperCase());
        }
        if(isValidated(authorStr)){
            mAuthor.setVisibility(View.VISIBLE);
            mAuthor.setText(authorStr.toUpperCase());
        }
        if(isValidated(pubdateStr)){
            mDate.setVisibility(View.VISIBLE);
            mDate.setText(pubdateStr.toUpperCase());
        }
    }

//    private void setCarouselImageListener(String images, String from) {
//        try{
//            imageListener = (position1, imageView) -> {
//                if(!images.isEmpty() && from.equals("main")){
//                    String imagesUrl[] = images.split(",");
//                    carouselView.setPageCount(imagesUrl.length);
//                    GlideApp
//                            .with(this)
//                            .load(imagesUrl[position1])
//                            .transition(withCrossFade())
//                            .apply(requestOptions())
//                            .into(imageView);
//                    ViewCompat.setTransitionName(imageView, idStr); //setTransitionName
//                }
//            };
//
//            imageInsertListener = (position1, imageView) -> {
//                if(!images.isEmpty() && from.equals("insert")){
//                    String imagesUrl[] = images.split(",");
//                    mArticleInsertCarousel.setPageCount(imagesUrl.length);
//
//                    GlideApp
//                            .with(this)
//                            .load(imagesUrl[position1])
//                            .transition(withCrossFade())
//                            .apply(requestOptions())
//                            .into(imageView);
//                    ViewCompat.setTransitionName(imageView, idStr); //setTransitionName
//                }
//            };
//
//            mArticleInsertCarousel.setImageListener(imageInsertListener);
//            carouselView.setImageListener(imageListener);
//
//        }catch (Exception e){
//            Log.d(TAG, "loadViews: Exception: "+e.getMessage());
//        }
//    }

    private void showCarousel(String images) {
        String imagesUrl[] = images.split(",");

//        if (carouselView != null) {
           imageListener = (position1, imageView) -> {
                Glide
                        .with(this)
                        .load(imagesUrl[position1])
//                        .transition(withCrossFade())
                        .apply(requestOptions())
                        .into(imageView);
//                ViewCompat.setTransitionName(imageView, idStr); //setTransitionName
            };

//        }
        try{
            carouselView.setImageListener(imageListener);
            carouselView.setPageCount(imagesUrl.length);

        }catch(RuntimeException e){
            Log.d(TAG, "showInsertCarousel: Exception"+e.getMessage());
        }
    }

    private void showInsertCarousel(String images) {
        String imagesUrl[] = images.split(",");
//        if (mArticleInsertCarousel != null) {
        imageInsertListener = (position1, imageView) -> {
            Glide
                        .with(this)
                        .load(imagesUrl[position1])
//                        .transition(withCrossFade())
                        .apply(requestOptions())
                        .into(imageView);
//                ViewCompat.setTransitionName(imageView, idStr); //setTransitionName
            };
        try{
            mArticleInsertCarousel.setImageListener(imageInsertListener);
            mArticleInsertCarousel.setPageCount(imagesUrl.length);
        }catch(RuntimeException e){
            Log.d(TAG, "showInsertCarousel: Exception"+e.getMessage());
        }

//        }
    }

    //
    private void showImage(String imgStr, ImageView imgView){

        //Loading Image and get the Height and Width of the ImageUrl: And load the Image as Bitmap to make it Loading Faster
        Glide.with(this)
                .asBitmap()
                .load(imgStr)
                .error(section)
                .apply(requestOptions())
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                        int width = resource.getWidth();  //  int width = bitmap.getWidth();
//                        int height = resource.getHeight();   // int height = bitmap.getHeight();
                        //Doubled the width and height just to make more clearer bitmap
//                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(resource, resource.getWidth()*2, resource.getHeight()*2, true);
                        imgView.setImageBitmap(resource);//imgView.setImageBitmap(resource);//scaledBitmap
                    }
                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }

    private void showVideo(String image){
        BandwidthMeter bandwidthMeter3 = new DefaultBandwidthMeter();
        TrackSelector trackSelector3 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter3));
        exoPlayer3 = ExoPlayerFactory.newSimpleInstance(this, trackSelector3);

        DefaultHttpDataSourceFactory dataSourceFactory3 = new DefaultHttpDataSourceFactory("exoplayer_3");
        MediaSource mediaSource3 = new ExtractorMediaSource.Factory(dataSourceFactory3).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(image));

        videoArticle.setUseController(true);  //play, stop
        videoArticle.setPlayer(exoPlayer3);
        videoArticle.setShowBuffering(true);

        exoPlayer3.prepare(mediaSource3);
        exoPlayer3.setPlayWhenReady(true);
        exoPlayer3.setVolume(35f);
    }

    private void showInsertVideo(String image){

        BandwidthMeter bandwidthMeter3 = new DefaultBandwidthMeter();
        TrackSelector trackSelector3 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter3));
        exoPlayer4 = ExoPlayerFactory.newSimpleInstance(this, trackSelector3);

        DefaultHttpDataSourceFactory dataSourceFactory3 = new DefaultHttpDataSourceFactory("exoplayer_4");
        MediaSource mediaSource3 = new ExtractorMediaSource.Factory(dataSourceFactory3).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(image));

//        mArticleInsertVideo.setControllerAutoShow(true);
        mArticleInsertVideo.setUseController(true);  //play, stop button
        mArticleInsertVideo.hideController();
        mArticleInsertVideo.setPlayer(exoPlayer4);
        mArticleInsertVideo.setShowBuffering(true);

        exoPlayer4.prepare(mediaSource3);
        exoPlayer4.setPlayWhenReady(true);
        exoPlayer4.setVolume(0f); //exoPlayer4.setVolume(35f);
        exoPlayer4.setRepeatMode(Player.REPEAT_MODE_ALL);

    }

//    private void setTitleColor(String strColor) {
//        mSectionName.getBackground().setColorFilter(Color.parseColor(strColor), PorterDuff.Mode.SRC_ATOP); //OK
//    }

    private void setTitleColor(String sectionName) {
        if(Global.sectionList.size()>0){
            for(int x=0; x < Global.sectionList.size(); x++){
                if(Global.sectionList.get(x).getTitle().toLowerCase().equals(sectionName)){
                    mSectionName.getBackground().setColorFilter(Color.parseColor(Global.sectionList.get(x).getColor()), PorterDuff.Mode.SRC_ATOP); //OK
                }
            }
        }
    }

    private void checkIfBookmarked(String idStr) {
        if(Global.bookmarkIdList.contains(Integer.valueOf(idStr))){
            imageBookmark.setImageResource(R.drawable.new_bookmark_fill);
        }else imageBookmark.setImageResource(R.drawable.new_bookmark_empty);
    }

//         if(Global.sectionList.size()>0) {
//        for(int x=0; x<Global.sectionList.size();x++){
//            String id = String.valueOf(Global.sectionList.get(x).getId());
//            if(id.equals(sectionId)){
//                String sectName = String.valueOf(Global.sectionList.get(x).getTitle());
//                sf.setSectionName(sectName);
//                Log.d(TAG, "getSectionNameNOW: "+sectName);
//            }
//        }
//    }

    private void retrieveForDeepLink() {
        httpLinkUrl = Global.httpLinkUrl;
        idStr = Global.idStr;
        titleStr = Global.titleStr;
        imageStr = Global.imageStr;
        typeStr = Global.typeStr;
        pubdateStr = Global.pubdateStr;
        sectionId = Global.sectionId;
        contentStr = Global.contentStr;
        newImage1tStr = Global.newImage1tStr;
        insertTitle = Global.insertTitle;
        insertType = Global.insertType;
        insertNewsId = Global.insertNewsId;
        insertContent = Global.insertContent;
        contentStr2 = Global.contentStr2;
        authorStr = Global.authorStr;
        customStr = Global.customStr;
        adbanner = Global.adbanner;
    }

    //checkIfFiveStoryReads  after counter
    private void checkIfFiveStoryReads() {
        if(sf.getStoryReadsCount()>=5 && sf.isNewUser() && !sf.hasNotificationAllowed()){ //
            Log.d(TAG, "onCreate: read story count " + 5);
            sf.setStoryReadsCount(0);
            sf.setNewUser(false); //
            Global.NotificationPermission dialog = new Global.NotificationPermission(sf);
            dialog.showDialog(this,getResources().getString(R.string.notification_dialog));

        }else {
            Log.d(TAG, "checkIfFiveStoryReads: StoryReadsCount: "+sf.getStoryReadsCount());
            Log.d(TAG, "checkIfFiveStoryReads: is NewUser: "+sf.isNewUser());
            Log.d(TAG, "checkIfFiveStoryReads: is NotificationAllowed: "+sf.hasNotificationAllowed());
        }
    }

    private void initFontSettings() {
        //Replace with SF
        String fontStr = sf.getFontString();

        if (fontStr.equals("0")) {
            adjustTextSize(0);
            fontSw = 0;
            webView.getSettings().setTextZoom(100);
            if(hasInsert){
                webView2.getSettings().setTextZoom(100);
                insertWebView.getSettings().setTextZoom(100);
            }
        } else if (fontStr.equals("1")) {
            adjustTextSize((float) 1.5);
            fontSw = 1;
            webView.getSettings().setTextZoom(110);
            if(hasInsert){
                webView2.getSettings().setTextZoom(110);
                insertWebView.getSettings().setTextZoom(110);
            }
        } else if (fontStr.equals("2")) {
            adjustTextSize(3);
            fontSw = 2;
            webView.getSettings().setTextZoom(120);
            if(hasInsert){
                webView2.getSettings().setTextZoom(120);
                insertWebView.getSettings().setTextZoom(120);
            }
        }

    }

    private void adjustTextSize(float size){
        mTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,17+size);
        mAuthor.setTextSize(TypedValue.COMPLEX_UNIT_SP,11+ size);
        mDate.setTextSize(TypedValue.COMPLEX_UNIT_SP,11+ size);
        mArticleInsertTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,17+ size);
        mArticleInsertReadMore.setTextSize(TypedValue.COMPLEX_UNIT_SP,14+ size);
    }


    private void configWebViewSettings() {
//      //Dark Mode for VERSION_CODES.Q

        webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.setWebChromeClient(new WebChromeClient()); //OLD
        webView.setWebViewClient(new MyWebViewClient());
//        webView.getSettings().setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    private void configWebView2Settings() {
        webView2.getSettings().setLoadsImagesAutomatically(true);
        webView2.getSettings().setAllowContentAccess(true);
        webView2.getSettings().setDomStorageEnabled(true);
        webView2.getSettings().setJavaScriptEnabled(true);
        webView2.setWebViewClient(new MyWebViewClient());
        //
        insertWebView.getSettings().setLoadsImagesAutomatically(true);
        insertWebView.getSettings().setAllowContentAccess(true);
        insertWebView.getSettings().setDomStorageEnabled(true);
        insertWebView.getSettings().setJavaScriptEnabled(true);
        insertWebView.setWebViewClient(new MyWebViewClient()); //MyWebViewClient: OPEN link externally from android webview

        if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            webView2.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            insertWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    //MyWebViewClient: OPEN link externally from android webview
    public final class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Intent i = null;
            if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                i = new Intent(Intent.ACTION_VIEW, request.getUrl());
            }
            if(i!=null)startActivity(i);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            imageFont.setClickable(false);
            imageShare.setClickable(false);
            imageBookmark.setClickable(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            imageFont.setClickable(true);
            imageShare.setClickable(true);
            imageBookmark.setClickable(true);
            mShimmerViewContainer.setVisibility(View.GONE);
            mShimmerViewContainer.stopShimmer();
            //VISIBLE After mShimmerViewContainer GONE
            clArticleHolder.setVisibility(View.VISIBLE);
            if(!newImage1tStr.isEmpty())clInsertContentHolder.setVisibility(View.VISIBLE);
            if(relatedArticles.size()>0)flRelatedContainer.setVisibility(View.VISIBLE); //NO RELATED

//            else {
                //Change the Height of the RV to remove the Excess Empty space in the bottom screen
                //get the Layout Parameter of the Specific View
//                ViewGroup.LayoutParams layoutParams = rvRelatedArticle.getLayoutParams();
//                layoutParams.height = getResources().getDimensionPixelSize(R.dimen._1sdp);
//                rvRelatedArticle.setLayoutParams(layoutParams);
//            }

        }
    }


    private void loadHtml(){
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    imageFont.setClickable(true);
                    imageShare.setClickable(true);
                    imageBookmark.setClickable(true);
                    mShimmerViewContainer.setVisibility(View.GONE);
                    mShimmerViewContainer.stopShimmer();
                    //VISIBLE After mShimmerViewContainer GONE
                    clArticleHolder.setVisibility(View.VISIBLE);
//                    if(!newImage1tStr.isEmpty())clInsertContentHolder.setVisibility(View.VISIBLE);
//                    if(relatedArticles.size()>0)flRelatedContainer.setVisibility(View.VISIBLE); //NO RELATED


                }
            }
        });

    }

    //generateWebArticle
    private void generateWebArticle() {
        htmlStr = htmlStr + "<!DOCTYPE html> " +
                "<html>";
        htmlStr = htmlStr +
                "<script src=\"file:///android_asset/jquery-3.5.1.min.js\"></script>" + //new jquery-3.5.1.min.js
                "<script src=\"file:///android_asset/listCarousel.js\"></script>" + //listCarousel.js
                "<script src=\"https://api.dmcdn.net/all.js\"></script>" +  //api.dmcdn.net/all.js
                "<head>" + "<meta name=\"viewport\" content=\"width=device-width, user-scalable=no\" />" +  //meta ..name,content,user-scalable
                "<style type=\"text/css\"> " + //style type,
                ".slider-previous{ display:none;}" +  //style  .slider-previous
                "table { width=100% }" +  //style for TABLE
                "div {word-wrap: break-word; padding: 10px;}" +  //style for DIV
                "@font-face {font-family: MyFont; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-BOLD.otf\")} " + //style div
                "@font-face {font-family: MyFont2; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-LIGHT.otf\")} " +
                "@font-face {font-family: MyFont3; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-REGULAR.otf\")}" +
                "</style></head>";   //style end

        htmlStr = htmlStr + (sf.getNightMode() ? Global.NIGHT_MODE : Global.NORMAL_MODE); //style for BODY
//        htmlStr = htmlStr + "<body style='margin:0;padding:0;'>"; //style for BODY

//        //REPLACED CAROUSEL4, VIDEO 6,7
//        if(!imageStr.isEmpty()){
////            if (imageStr.length() > 0) {
//
//            htmlStr = htmlStr + "<br>";
//
//            //carousel
//            if (typeStr.equals("4")) {
//                imgItems = Arrays.asList(imageStr.split(","));
//                htmlStr = htmlStr + "<ul id=\"cms-slider\">";
//                for (int i = 0; i < imgItems.size(); i++) {
//                    htmlStr = htmlStr + "<li><img width=100% height=200dp  src=" + imgItems.get(i) + "></li>";
//                }
//                htmlStr = htmlStr + "</ul>" ;
//            }else if (typeStr.equals("7")) {
////            String videos = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4";
//            htmlStr = htmlStr +
//                    "<div>" +
//                    "<video style=\"width:100%; height:auto; padding-bottom:0;\"" +
//                    "controls poster=\"file:///android_asset/images/img_2.png\" autoplay muted>" +
//                    "<source src=" + imageStr +
//                    " type=\"video/mp4\">" +
//                    "</video>" +
//                    "</div>";
//            }else {
//                    htmlStr = htmlStr + "<center><img width=100% src=" + imageStr + "></center>";
//                }
//
//        } else {
//            htmlStr = htmlStr + "";
//        }
//
//        //NEW ADDITIONAL: SECTION_NAME // <span background-color=#000000;>
//        htmlStr = htmlStr + "<br><div><span style=\"background-color:#ddd;\"><strong><font size=1 color=#FFFFFF face=MyFont3></span>" + sf.getSectionName().toUpperCase() + "</font></strong><br>";//<br><br>;
////        htmlStr = htmlStr + "<br><div><strong><font size=1 color=#FFFFFF face=MyFont3>" + sf.getSectionName().toUpperCase() + "</font></strong><br>";//<br><br>;
//
//        //OPTION: TO BE REPLACED
//        if(!titleStr.isEmpty()) //title  size=4
//        htmlStr = htmlStr + "<br><div><strong><font size=3.5 color=#000000 face=MyFont>" + titleStr.toUpperCase() + "</font></strong><br>";//<br><br>;
//
//        if(!authorStr.isEmpty()) //author color=#949494
//        htmlStr = htmlStr + "<font color=#000000 size=1.5 face=MyFont3>" + authorStr.toUpperCase() + "<br>" + "</font>"; // authorStr.toUpperCase() + "<br>" + "</font>";
//
//        if(!pubdateStr.isEmpty()) //date color=#949494
//          htmlStr = htmlStr + "<font color=#000000 size=1.5 face=MyFont3>" + pubdateStr.toUpperCase() + "<br>" + "</font></div>";


        //contentString
        if(!contentStr.isEmpty()) {
            if(contentStr.contains("\n")) contentStr.replace("\n", "<br>");
//                htmlStr = htmlStr + "<div><font face=MyFont3>" + contentStr.replace("\n", "<br>") + "</font></div><br>";
            htmlStr = htmlStr + "<div><font face=MyFont3>" + contentStr + "</font></div><br>";
            htmlStr = htmlStr + "<script>$(function() {$('#cms-slider').listCarousel();})</script>";
        }else{
            htmlStr = htmlStr + "";
        }
        htmlStr = htmlStr + "</body></html>";
    }

    private void generateWeb2Article() {
        htmlStr2 = htmlStr2 + "<!DOCTYPE html> " +
                "<html>";
        htmlStr2 = htmlStr2 +
                "<script src=\"file:///android_asset/jquery-3.5.1.min.js\"></script>" + //new jquery-3.5.1.min.js
                "<script src=\"file:///android_asset/listCarousel.js\"></script>" + //listCarousel.js
                "<script src=\"https://api.dmcdn.net/all.js\"></script>" +  //api.dmcdn.net/all.js
                "<head>" + "<meta name=\"viewport\" content=\"width=device-width, user-scalable=no\" />" +  //meta ..name,content,user-scalable
                "<style type=\"text/css\"> " + //style type,
                ".slider-previous{ display:none;}" +  //style  .slider-previous
                "table { width=100% }" +  //style for TABLE
                "div {word-wrap: break-word; padding: 10px;}" +  //style for DIV
                "@font-face {font-family: MyFont; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-BOLD.otf\")} " + //style div
                "@font-face {font-family: MyFont2; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-LIGHT.otf\")} " +
                "@font-face {font-family: MyFont3; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-REGULAR.otf\")}" +
                "</style></head>";   //style end
        htmlStr2 = htmlStr2 + (sf.getNightMode() ? Global.NIGHT_MODE : Global.NORMAL_MODE); //style for BODY
        //content
        if(!contentStr2.isEmpty()) {
            htmlStr2 = htmlStr2 + "<div><font face=MyFont3>" + contentStr2.replace("\n", "<br>") + "</font></div><br>";
            htmlStr2 = htmlStr2 + "<script>$(function() {$('#cms-slider').listCarousel();})</script>";
        }else{
            htmlStr2 = htmlStr2 + "";
        }

        htmlStr2 = htmlStr2 + "</body></html>";
    }

    private void generateInsertWebArticle() {
        htmlStr3 = htmlStr3 + "<!DOCTYPE html> " +
                "<html>";
        htmlStr3 = htmlStr3 +
                "<script src=\"file:///android_asset/jquery-3.5.1.min.js\"></script>" + //new jquery-3.5.1.min.js
                "<script src=\"file:///android_asset/listCarousel.js\"></script>" + //listCarousel.js
                "<script src=\"https://api.dmcdn.net/all.js\"></script>" +  //api.dmcdn.net/all.js
                "<head>" + "<meta name=\"viewport\" content=\"width=device-width, user-scalable=no\" />" +  //meta ..name,content,user-scalable
                "<style type=\"text/css\"> " + //style type,
                ".slider-previous{ display:none;}" +  //style  .slider-previous
                "table { width=100% }" +  //style for TABLE
                "div {word-wrap: break-word; padding: 10px;}" +  //style for DIV
                "@font-face {font-family: MyFont; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-BOLD.otf\")} " + //style div
                "@font-face {font-family: MyFont2; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-LIGHT.otf\")} " +
                "@font-face {font-family: MyFont3; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-REGULAR.otf\")}" +
                "</style></head>";   //style end
        htmlStr3 = htmlStr3 + (sf.getNightMode() ? Global.NIGHT_MODE : Global.NORMAL_MODE); //style for BODY
        //content
        if(!insertContent.isEmpty()) {
            //filter: Check if type 2 then dont display the content: CONTENT in type 2 is URL_LINK
            if(!insertType.equals("2") && !insertType.equals("3") ){
                htmlStr3 = htmlStr3 + "<div><font face=MyFont2>" + insertContent.replace("\n", "<br>") + "</font></div><br>";
                htmlStr3 = htmlStr3 + "<script>$(function() {$('#cms-slider').listCarousel();})</script>";
            }

        }else{
            htmlStr3 = htmlStr3 + "";
        }
        htmlStr3 = htmlStr3 + "</body></html>";
    }

    private void initViewListeners() {
        imageBack.setOnClickListener(v -> {
//           overridePendingTransition(R.anim.stay, R.anim.slide_in_left); //not working To be tested next time
            //RESOLVE: BUGS arrowBack to fragment FINISHED the app and exited when app brought to background
            onBackPressed();
        });

        imageFont.setOnClickListener(v -> {
            if (fontSw == 0) {
                adjustTextSize((float) 1.5);
                fontSw = 1;
                sf.setFontString("1");
                webView.getSettings().setTextZoom(110);
                if(hasInsert){
                    webView2.getSettings().setTextZoom(110);
                    insertWebView.getSettings().setTextZoom(110);
                }
            } else if (fontSw == 1) {
                adjustTextSize(3);
                fontSw = 2;
                sf.setFontString("2");
                webView.getSettings().setTextZoom(120);
                if(hasInsert){
                    webView2.getSettings().setTextZoom(120);
                    insertWebView.getSettings().setTextZoom(120);
                }
            } else if (fontSw == 2) {
                adjustTextSize(0);
                fontSw = 0;
                sf.setFontString("0");
                webView.getSettings().setTextZoom(100);
                if(hasInsert){
                    webView2.getSettings().setTextZoom(100);
                    insertWebView.getSettings().setTextZoom(100);
                }
            }
        });

        //sharelink
        imageShare.setOnClickListener(v -> {
           shareLink(idStr, typeStr);  //shareLink2(idStr, typeStr);
        });

        imageBookmark.setOnClickListener(v -> {
            imageBookmark.setEnabled(false);
            //Check the status of the ImageView First to know what process. Then Control the Button by disabling/enabling
            if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                if(imageBookmark.getDrawable().getConstantState().equals(imageBookmark.getContext().getDrawable(R.drawable.new_bookmark_empty).getConstantState())){

    //            if(imageBookmark.getDrawable().getConstantState()==getResources().getDrawable(R.drawable.new_bookmark_empty).getConstantState()){
                    addToBookmark(Integer.valueOf(idStr),titleStr,imageStr,typeStr,pubdateStr,contentStr,newImage1tStr,contentStr2,authorStr,customStr,adbanner,sf.getSectionName().toUpperCase());
                }else{
                    deleteOneBookmark(Integer.valueOf(idStr));
                }
            }
            imageBookmark.setEnabled(true);
        });

        bannerIvImage.setOnClickListener(v -> {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(bannerAdLink));
                startActivity(intent);
            }catch(Exception e){
                Log.d("TAGGG", "Ad Intent "+ e.getMessage());
                return;
            }
        });

        //SuperShare Sponsor is CLICKED
        mArticleInsertImage.setOnClickListener(v->{
            if(isAlreadyClick())insertClicked();
        });

        mArticleInsertCarousel.setImageClickListener(position -> {
//            insertClicked();
            if(isAlreadyClick())insertClicked();
        });

        //EXOPLAYER ONCLICK LISTENER:
        mArticleInsertVideo.getVideoSurfaceView().setOnClickListener(view -> {
            mArticleInsertVideo.showController();
            exoPlayer4.setVolume(35f);
//            if(isAlreadyClick())insertClicked();
        });

        mArticleInsertTitle.setOnClickListener(v->{
            if(isAlreadyClick())insertClicked();
        });

        mArticleInsertReadMore.setOnClickListener(v->{
            if(isAlreadyClick())insertClicked();
        });

        //Super Sensitive, just touch its trigger
//        mArticleInsertVideo.setOnTouchListener((view, motionEvent) -> {
//            //use the motionEvent to make it not so sensitive. Make it onCLick
////            if(motionEvent.getAction() == .)insertClicked();
//            return true;
//        });
    }

//    private void validateFile(){
//        File file = new File(this.getExternalFilesDir(""),".Inquirer/"+idStr + ".html");
//        File file2 = new File(this.getExternalFilesDir(""),".Inquirer/"+idStr + ".txt");
//        String f1 = file.getAbsolutePath();
//        String f2 = file2.getAbsolutePath();
//        Log.d(TAG, "validateFile: INQUIRER_FILE: 1 "+ f1);
//        Log.d(TAG, "validateFile: INQUIRER_FILE: 2 "+ f2);
//
//        if (file.exists()) {
//            Global.DeleteFile(f1);
//            Global.DeleteFile(f2);
//            Toast.makeText(getApplicationContext(), "Bookmark deleted", Toast.LENGTH_SHORT).show();
//            imageBookmark.setImageResource(R.drawable.bookmark);
//        } else {
//            //writing/saving  to file storage
//            try{
//                Log.d(TAG, "validateFile: INQUIRER_FILE html "+htmlStr);
//                Global.WriteFile(htmlStr, f1);
//                Global.WriteFile(titleStr + "<del>" + typeStr + "<del>" + pubdateStr  + "<del>" + idStr, f2);
//                Toast.makeText(getApplicationContext(), "Saved to Bookmarks", Toast.LENGTH_SHORT).show();
//                imageBookmark.setImageResource(R.drawable.check);
//            }catch(Exception e){
//                Log.d(TAG, "validateFile: Exception_Error WRITE_FILE: "+e.getMessage());
//            }
//        }
//    }

//    String link = news.get(n).getLink();
//                        fragment.checkImageLink(link);

    private Boolean isAlreadyClick(){
        if (System.currentTimeMillis() - currentTime < 2500) {
            return false;
        }
        currentTime = System.currentTimeMillis();
        return true;
    }

    public void checkImageLink(String link) {
        if (isValidated(link)) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(link));
                startActivity(intent);
            } catch (Exception e) {
                Log.d(TAG, "checkImageLink: Try_Exception: "+ e.getMessage());
                return;
            }
        }
    }


    //CHECK the TYPE HERE
    private void insertClicked(){
        if (Global.isNetworkAvailable(this)) {
            Global.isInsertClicked = true;
            //filter: check if type 2:
            if(insertType.equals("2") || insertType.equals("3")) checkImageLink(insertContent);
            else viewRelatedArticle(insertNewsId);

        }else {
            Global.ViewDialog alert = new Global.ViewDialog();
            alert.showDialog(this, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
        }
    }

    private void fetchIntentExtras() {

//        String images = "https://homepages.cae.wisc.edu/~ece533/images/arctichare.png,https://homepages.cae.wisc.edu/~ece533/images/arctichare.png,https://homepages.cae.wisc.edu/~ece533/images/arctichare.png,https://homepages.cae.wisc.edu/~ece533/images/arctichare.png,https://homepages.cae.wisc.edu/~ece533/images/arctichare.png,https://homepages.cae.wisc.edu/~ece533/images/arctichare.png,https://homepages.cae.wisc.edu/~ece533/images/arctichare.png";
//        String images =  "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4";
        Intent intent = getIntent();
        httpLinkUrl = intent.getStringExtra("url");  //from notif frag
        Log.d(TAG, "fetchIntentExtras: httpLinkUrl " + httpLinkUrl);
        idStr = intent.getStringExtra("id");
        sectionId = intent.getStringExtra("sectionId");
        titleStr = intent.getStringExtra("title");
        imageStr = intent.getStringExtra("photo");
        ///
        typeStr = intent.getStringExtra("type");
//       imageStr = images;
//        typeStr = "7"; //TEST TYPE
        pubdateStr = intent.getStringExtra("pubdate");
        contentStr = intent.getStringExtra("content1");
        contentStr2 = intent.getStringExtra("content2");
        authorStr = intent.getStringExtra("author");
        customStr = intent.getStringExtra("custom");
        adbanner = intent.getStringExtra("adbanner");
        //Insert
        insertTitle = intent.getStringExtra("insertTitle");
        insertType = intent.getStringExtra("insertType");
        insertNewsId = intent.getStringExtra("insertNewsId");
        insertContent = intent.getStringExtra("insertContent");
        newImage1tStr = intent.getStringExtra("image1");

        //Test Insert VIDEO
//        insertTitle = "Title";
//        insertType = "7"; //7
//        insertNewsId = "146983";
//        newImage1tStr = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4";
//        insertContent = "https://www.google.com";

        if(!isValidated(sectionId)) sectionId ="";
        if(!isValidated(titleStr)) titleStr ="";
        if(!isValidated(imageStr)) imageStr ="";
        if(!isValidated(typeStr)) typeStr ="";
        if(!isValidated(pubdateStr)) pubdateStr ="";
        if(!isValidated(contentStr)) contentStr ="";
        if(!isValidated(newImage1tStr)) newImage1tStr ="";
        if(!isValidated(insertTitle)) insertTitle ="";
        if(!isValidated(insertType)) insertType ="";
        if(!isValidated(insertNewsId)) insertNewsId ="";
        if(!isValidated(insertContent)) insertContent ="";
        if(!isValidated(contentStr2)) contentStr2 ="";
        if(!isValidated(authorStr)) authorStr ="";
        if(!isValidated(customStr)) customStr ="";
        if(!isValidated(adbanner)) adbanner ="";

        //androidx //Usage: if from DeepLinkActivity
        saveForDeepLink();

        //check banner ad
        checkAd2(adbanner);

        Log.d(TAG, "fetchIntentExtras: NEWS_ID === " + idStr);
        Log.d(TAG, "fetchIntentExtras: NEWS_ID === " + sectionId);

    }

    private void fetchNewData() {
        idStr = Global.idStr;
        titleStr = Global.titleStr;
        imageStr = Global.imageStr ;
        typeStr = Global.typeStr;
        pubdateStr = Global.pubdateStr;
        sectionId = Global.sectionId;
        contentStr = Global.contentStr;
        newImage1tStr = Global.newImage1tStr;
        insertTitle = Global.insertTitle;
        insertType = Global.insertType;
        insertNewsId = Global.insertNewsId;
        insertContent = Global.insertContent;
        contentStr2 =  Global.contentStr2;
        authorStr =  Global.authorStr;
        customStr =  Global.customStr;
        adbanner =  Global.adbanner;

        if(!isValidated(titleStr)) titleStr ="";
        if(!isValidated(imageStr)) imageStr ="";
        if(!isValidated(typeStr)) typeStr ="";
        if(!isValidated(pubdateStr)) pubdateStr ="";
        if(!isValidated(sectionId)) sectionId ="";
        if(!isValidated(contentStr)) contentStr ="";
        if(!isValidated(newImage1tStr)) newImage1tStr ="";
        if(!isValidated(contentStr2)) contentStr2 ="";
        if(!isValidated(authorStr)) authorStr ="";
        if(!isValidated(customStr)) customStr ="";
        if(!isValidated(adbanner)) adbanner ="";
        if(!isValidated(insertType)) insertType ="";

//        //Display Insert IF
//        if(!newImage1tStr.isEmpty()) hasInsert = true;
//        else clInsertContentHolder.setVisibility(View.GONE); //NO INSERT, HIDE IT

        //androidx //Usage: if from DeepLinkActivity
        saveForDeepLink();
        //check banner ad
        checkAd2(adbanner);
        //Enabling onBackPressed: Make Global.isRelatedArticle FALSE again
        Global.isRelatedArticle = false;
    }

//    private boolean isValidated(String string){
//
//        try{
//            if(!string.isEmpty()) return true; //Check if Empty
//        }catch(Exception e){
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
//        }finally {
//            if(string!=null) return true;  //Check if null
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
//        }
//        return false;
//    }

    private boolean isValidated(String string){

        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error isEmpty** ");
            try{
                Log.d(TAG, "isDisplayType01Validated: Exception_Error isNull** ");
                if(string!=null) return true;  //Check if null
            }catch(Exception ex){
                Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
            }
        }
        return false;
    }



    private void saveForDeepLink() {
        Global.httpLinkUrl = httpLinkUrl;
        Global.idStr = idStr;
        Global.titleStr = titleStr;
        Global.imageStr = imageStr;
        Global.typeStr = typeStr;
        Global.pubdateStr = pubdateStr;
        Global.sectionId = sectionId;
        Global.contentStr = contentStr;
        Global.newImage1tStr = newImage1tStr;
        Global.insertTitle = insertTitle;
        Global.insertType = insertType;
        Global.insertNewsId = insertNewsId;
        Global.insertContent = insertContent;
        Global.contentStr2 = contentStr2;
        Global.authorStr = authorStr;
        Global.customStr = customStr;
        Global.adbanner = adbanner;

        if(!isValidated(Global.httpLinkUrl)) Global.httpLinkUrl ="";
        if(!isValidated(Global.sectionId)) Global.sectionId ="";
        if(!isValidated(Global.idStr)) Global.httpLinkUrl ="";
        if(!isValidated(Global.titleStr)) Global.titleStr ="";
        if(!isValidated(Global.imageStr)) Global.imageStr ="";
        if(!isValidated(Global.typeStr)) Global.typeStr ="";
        if(!isValidated(Global.pubdateStr)) Global.pubdateStr ="";
        if(!isValidated(Global.contentStr)) Global.contentStr ="";
        if(!isValidated(Global.newImage1tStr)) Global.newImage1tStr ="";
        if(!isValidated(Global.contentStr2)) Global.contentStr2 ="";
        if(!isValidated(Global.authorStr)) Global.authorStr ="";
        if(!isValidated(Global.customStr)) Global.customStr ="";
        if(!isValidated(Global.insertType)) Global.insertType ="";

//        //Display Insert IF
//        if(!newImage1tStr.isEmpty()) hasInsert = true;
//        else clInsertContentHolder.setVisibility(View.GONE); //NO INSERT, HIDE IT

    }

    private void initViews() throws JSONException {

        mShimmerViewContainer = findViewById(R.id.shimmer_article);
//        mNestedScrollView = findViewById(R.id.nsvContainer);
        imageBack = findViewById(R.id.imageBack);
        imageFont = findViewById(R.id.imageFont);
        imageShare = findViewById(R.id.imageShare);
        imageBookmark = findViewById(R.id.imageBookmark);
        logo = findViewById(R.id.logo);
        mMainImage = findViewById(R.id.ivArticleMainImage);
        mArticleInsertImage = findViewById(R.id.ivArticleInsertImage);
        mArticleInsertCarousel = findViewById(R.id.ivArticleInsertCarousel);
        mArticleInsertVideo = findViewById(R.id.ivArticleInsertVideo);
        mArticleInsertTitle = findViewById(R.id.tvArticleInsertTitle);
        mArticleInsertReadMore = findViewById(R.id.tvArticleInsertReadMore);

        carouselView = findViewById(R.id.cvArticleMainImageCarousel);
        videoArticle = findViewById(R.id.pvArticleMainImageVideo);

        mSectionName = findViewById(R.id.tvArticleSectionName);
        mTitle = findViewById(R.id.tvArticleTitle);
        mAuthor = findViewById(R.id.tvArticleAuthor);
        mDate = findViewById(R.id.tvArticleDate);
        webView = findViewById(R.id.webView);
        bannerIvImage = findViewById(R.id.ivAdImageWebNews);
        rvRelatedArticle = findViewById(R.id.rvArticleRelated);
        flRelatedContainer = findViewById(R.id.flRelatedArticleContainer);
        labelRelalatedArticle = findViewById(R.id.labelRelalatedArticle);
        clInsertContentHolder = findViewById(R.id.clInsertContentHolder);
        clArticleHolder = findViewById(R.id.webviewHolder);
        webView2 = findViewById(R.id.webView2);
        insertWebView = findViewById(R.id.insertWebView);

        //Start shimmer animation before loading article
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        mShimmerViewContainer.startShimmer();
        //Disable clicking until article was fully loaded
        imageFont.setClickable(false);
        imageShare.setClickable(false);
        imageBookmark.setClickable(false);

        Gson gson = new Gson();
            String artivcles = gson.toJson(Global.relatedArticles);
            Log.d(TAG, "getBookmarkList: ARTICLE_RELATED: "+artivcles);

        //GetRelated
        Log.d(TAG, "initViews: RELATED_ARTI "+Global.relatedArticles.size());
//        Log.d(TAG, "initViews: RELATED_ARTI "+artivcles);

        relatedArticles.clear();
        relatedArticles.addAll(Global.relatedArticles);
        if(relatedArticles.size()!=0)flRelatedContainer.setVisibility(View.VISIBLE); //WITH RELATED
        else {
            flRelatedContainer.setVisibility(View.GONE);
            //Change the Height of the Specific ViewGroup
//            //Change the Height of the RV to remove the Excess Empty space in the bottom screen
//            //get the Layout Parameter of the Specific View
//            ViewGroup.LayoutParams layoutParams = flRelatedContainer.getLayoutParams();
//            flRelatedContainer.getLayoutParams().height =  getResources().getDimensionPixelSize(R.dimen._169sdp);
//            rvRelatedArticle.getLayoutParams().height =  getResources().getDimensionPixelSize(R.dimen._150sdp);

//            layoutParams.height = getResources().getDimensionPixelSize(R.dimen._1sdp);
//            rvRelatedArticle.setLayoutParams(layoutParams);
//            labelRelalatedArticle.setVisibility(View.GONE);  //NO RELATED
        }

        //RV Settings
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),layoutManager.getOrientation()); //item decorations
        rvRelatedArticle.setHasFixedSize(true);
        rvRelatedArticle.setLayoutManager(layoutManager);
        rvAdapter = new RVAdapterRelatedArticle(this, relatedArticles);
        rvRelatedArticle.setAdapter(rvAdapter); //Setting Adapter
//        adapter.notifyDataSetChanged(); //call only when there are dataInTheAdapter notifyDataSetChanged

        //From API method
//        relatedArticles.clear(); //Always Clear the List so that the ADAPTER will refresh when there is a DELETION or CHANGES
//        relatedArticles.addAll(data);
//        if(bookmarks.size()==0){
//            mClEmptyContainer.setVisibility(View.VISIBLE);
//        }else mClEmptyContainer.setVisibility(View.GONE);
//        rvAdapter.notifyDataSetChanged(); //cal



    }

    /**NEW ADDITIONAL NewDesign*/
//    private void getRelatedArticleList() {
//        BookmarkViewModel bookmarkViewModel; //Creating a view_model object
//        bookmarkViewModel = ViewModelProviders.of(this).get(BookmarkViewModel.class); //initialize view_model PROVIDER
//        bookmarkViewModel.getBookmarkList().observe(this, data->{ //Accessing view_model METHOD
//            //Get the RelatedArticleLIST: Will use as the DATA in the ADAPTER
//            relatedArticles.clear(); //Always Clear the List so that the ADAPTER will refresh when there is a DELETION or CHANGES
//            relatedArticles.addAll(data);
//            if(relatedArticles.size()==0){
//                flRelatedContainer.setVisibility(View.GONE);
//            }else flRelatedContainer.setVisibility(View.VISIBLE);
//            rvAdapter.notifyDataSetChanged(); //call only when there are dataInTheAdapter notifyDataSetChanged
//            //DEBUGGING
////            Gson gson = new Gson();
////            String bookmarkData = gson.toJson(data);
////            Log.d(TAG, "getBookmarkList: ARTICLE_RELATED: "+bookmarkData);
//        });
//    }

    //sharelink
    private void shareLink(String id, String type){
        String hashed = Global.Generate32SHA512(sf.getDailyKey()  +"inq" + sf.getDeviceId() + "-megashare/" + id);
        apiService.getShareLink(sf.getDeviceId(), id, type, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {

                        hasNewDailyKey = false;
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_TEXT, titleStr + "\nvia Inquirer Mobile: " + link);

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(WebNewsActivity.this, "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                        Log.d(TAG, "onSuccess: LINK_SHARE**** "+ link);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey();
                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)) toastMessage(TAG+": onError: "+ e.getMessage()); // //Display in developer
                        Log.d(TAG, "onError: "+ e.getMessage());
                    }
                });
    }

    //sharelink
    private void shareLink2(String id, String type){
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megashare/" + id);
        apiService.getShareLink2(String.valueOf(unixTime), sf.getDeviceId(), id, type, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {

                        Log.d(TAG, "onSuccess: LINK_SHARE**** "+ link);

                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");

                        emailIntent.putExtra(Intent.EXTRA_TEXT, titleStr + "\nvia Inquirer Mobile: " + link);

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(WebNewsActivity.this, "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }
    //androidx
    public void checkAd2(String section_id) {
        removeAd(); //Remove Ad First
        Log.d(TAG, "checkAd2: getBannerAd2 section_id_CALLED: " + section_id);
        //CHECK if Global.bannerList is not empty
        if (Global.bannerList.size() > 0) {  //Global.bannerList
            for (int x = 0; x < Global.sectionsBanner2.size(); x++) {  //Iterate in Global.sectionsBanner not in Global.bannerList
                if (Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString().equals(getBannerList(section_id))) {
                    Log.d(TAG, "checkAd2: getBannerAd2 Searched_FOUND: "+ Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString()+" == "+getBannerList(section_id));
                    String image = Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("image").getAsString();
                    String link = Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("link").getAsString();
                    Log.d(TAG, "checkAd2: getBannerAd2 Searched_FOUND: IMAGE: "+image+" == LINK: "+link);
                    displayAd(image, link); //Global.sectionsBanner
                    break;
                }
            }
        }
    }

    private String getBannerList(String sectionId){
        String section_ID = "-1";
        //Note: Already check that Global.bannerList is not empty
        //Finding section_id inside Global.bannerList
        for(int x = 0; x<Global.bannerList.size(); x++){
            if(Global.bannerList.get(x).equals(sectionId)){
                section_ID = Global.bannerList.get(x);
                return section_ID;
            }
        }
        return section_ID;
    }

    //androidx
    private void displayAd(String image, String link) {
        Log.d(TAG, "displayAd: getBannerAd2 IMAGE: "+image);
        bannerIvImage.setVisibility(View.VISIBLE);
        bannerAdLink = link;
        //GlideApp Usage: Need: MyGlide and UnsafeOkHttpClient JavaClass
        Glide.with(this)
                .load(image)
//                .transition(withCrossFade())
                .apply(requestOptions())
                .into(bannerIvImage);
//        ViewCompat.setTransitionName(bannerIvImage, "BannerAdWebNews"); //setTransitionName
    }

    public void removeAd(){
        bannerIvImage.setVisibility(View.GONE);
    }

    private RequestOptions requestOptions(){
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(section)
                .error(section)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .diskCacheStrategy(DiskCacheStrategy.DATA) //  .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        return options;
    }

    public void displayFullview (String video) {

        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//DO NOT FORGET THIS EVER
            intent.setDataAndType(Uri.parse(video), "video/mp4");
            startActivity(intent);
        }catch(Exception e){
            Log.d(TAG, "Video6Link "+ e.getMessage());
            return;
        }

    }

    /** Records of seconds stay in article view **/
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
//            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            timeCounter++;
//            Log.d(TAG, "run: timeInMilliseconds.. "+ timeInMilliseconds);
            Log.d(TAG, "run: counting.. "+timeCounter);
            customHandler.postDelayed(this, 2000);
        }
    };

    //registerTimeArticleReads
    private void registerTimeArticleReads(String deviceId, String newsId, int seconds, String password){
        apiService.registerTimeArticleReads(deviceId, newsId, seconds, password)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseBody responseBody) {
                        Log.d(TAG, "onSuccess: registerTimeArticleReads called: NEWS_ID= "+ newsId+ " TIME= " +seconds);

                        try {
                            String r = responseBody.string();
                            Log.d(TAG, "onSuccess: ResponseBody:" + r);
                            if(r.contains("Store Successfully")){///
//                                sf.setNotificationAllowed(false);
                                Log.d(TAG, "onSuccess: registerTimeArticleReads Successfully");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: registerTimeArticleReads error  getMessage" + e.getMessage());
                    }
                });
    }

    @Override
    public void onResume() {
//        Log.d(TAG, "onResume: " + timeCounter);
//        startTime = SystemClock.uptimeMillis();
        timeCounter=0;
        customHandler.postDelayed(updateTimerThread, 2000);  //calling handler runnable
//        stop();
        Log.d(TAG, "onResume: WebNewsActivityLC" );
        Log.d(TAG, "onResume: called..ListView Stay " + sf.getListViewReadsCount() );  //from article

        //CONTINUE PLAYING
        if(exoPlayer4!=null ) exoPlayer4.setPlayWhenReady(true);
        if(exoPlayer3!=null)  exoPlayer3.setPlayWhenReady(true);

        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: WebNewsActivityLC");
//        Global.isItemClicked = false;
        customHandler.removeCallbacks(updateTimerThread);  //stop handler runnable
        Log.d(TAG, "onPause: stop counting.." + timeCounter );
//         sf.setStayInArticle((int)timeInMilliseconds);
        sf.setStayInArticle(timeCounter);
        super.onPause();
    }

    @Override  //onStart called once,
    public void onStart() {
//        EventBus.getDefault().register(this); //
//        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 2000);  //calling handler runnable
        Log.d(TAG, "onStart: WebNewsActivityLC" );
        super.onStart();
    }

    @Override
    public void onStop() {
//        EventBus.getDefault().unregister(this);
        Global.isItemClicked=false;
        Log.d(TAG, "onStop: CALLED1"+ sf.getStayInArticle());
        customHandler.removeCallbacks(updateTimerThread);  //stop handler runnable
        if(sf.getStayInArticle()>=1){
            Log.d(TAG, "onStop: CALLED getStayInArticle***");
            registerTimeArticleReads(sf.getDeviceId(), idStr, sf.getStayInArticle(),Global.passStoreUserReadTime);
            Log.d(TAG, "onStop: CALLED registerTimeArticleReads***");
        }
        Log.d(TAG, "onStop: CALLED2"+ sf.getStayInArticle());

        //PAUSE PLAYING
        if(exoPlayer4!=null ) exoPlayer4.setPlayWhenReady(false);
        if(exoPlayer3!=null)  exoPlayer3.setPlayWhenReady(false);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            if(!Global.isRelatedArticle)finish(); //Do not finish if activity is ReCreated //RESOLVED:
        }catch(Exception e){
            Log.d(TAG, "onStop: Exception_ERROR"+ e.getMessage());
        }
    }

    /**New Additional NewDesign*/
    public void addToBookmark(int newsId, String title, String mainImage, String type, String date, String content1, String image1, String content2, String author, String custom, String bannerSectionId, String sectionName) {
        sf.setLastBookmarkCount(sf.getLastBookmarkCount()+1);
        Log.d(TAG, "addToBookmark:getLastBookmarkCount "+sf.getLastBookmarkCount());

        //insert/save to database
        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, sf.getLastBookmarkCount(),title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectionName);
//        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectionName);
        helper.saveBookmark(bookmarkModel);
        //Instant Updating the Latest Bookmarking: ACCESSING the ALL ViewHolder
        imageBookmark.setImageResource(R.drawable.new_bookmark_fill);
        toastMessage("This article is added to your bookmark.");
    }

    public void deleteOneBookmark(int newsId) {
        helper.deleteOneBookmark(newsId);
        //Instant Updating the Latest Bookmarking:
        imageBookmark.setImageResource(R.drawable.new_bookmark_empty);
        toastMessage("Bookmark deleted.");
    }

    public void toastMessage(String message) {
        if(Global.toast!=null) Global.toast.cancel(); //Remove First the existing toast
        Global.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT); Global.toast.show(); //Show NEW TOAST
    }
//
//    public void viewRelatedArticle(String relatedNewsId) {
//        Log.d(TAG, "viewRelatedArticle:NEWS_ID: "+relatedNewsId);
////        showLoader();
//        long unixTime = System.currentTimeMillis() / 1000L;
//        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megaarticle/" + relatedNewsId);
//        apiService.getArticle(String.valueOf(unixTime),sf.getDeviceId(), relatedNewsId, hashed)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<Article>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        compositeDisposable.add(d);
//                    }
//
//                    @Override
//                    public void onSuccess(Article article) {
//
//                        String content1 = article.getContent1().isEmpty() ? article.getContent2a() : article.getContent2b();
//                        String content2 = article.getContent2b();
////                        hideLoader();
//                        Global.idStr = relatedNewsId;
//                        Global.titleStr = article.getTitle();
//                        Global.imageStr = article.getCreative();
//                        Global.typeStr = article.getType();
//                        Global.pubdateStr = article.getPubdate();
//                        Global.sectionId = article.getSectionId();
////                        Global.contentStr = article.getContent().get(0).toString();
//                        Global.contentStr = content1;
//
////                        Global.newImage1tStr = article.getInsert().getCreative();
//                        Global.newImage1tStr = "";
////                        Global.contentStr2 = article.getContent().get(1).toString();
//                        Global.contentStr2 = content2;
//
//                        Global.authorStr = article.getByline();
//                        Global.customStr = article.getCustom();
//                        Global.isRelatedArticle = true; //FLAG
//
//                        //Get the Related Article
//                        if(article.getRelated()!=null){
//                            Global.relatedArticles.clear();
//                            Global.relatedArticles.addAll(article.getRelated());
//                        }
//                        //GET SuperShare Details
//                        recreate(); //Refreshing the ACTIVITY = Calling the onCreate again, but calling first the onStop,onPause...and the rest
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
////                        hideLoader();
//                        String error_msg = Global.getResponseCode(e);
//                        Toast.makeText(getApplicationContext(), error_msg, Toast.LENGTH_SHORT).show();
//                    }
//                });
//    }

    public void viewRelatedArticle(String relatedNewsId) {
        Log.d(TAG, "viewRelatedArticle: NEWS_ID RELATED/INSERT : "+relatedNewsId);
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + relatedNewsId);
        apiService.getArticle2(String.valueOf(unixTime),sf.getDeviceId(), relatedNewsId, hashed)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try{
                            hasNewDailyKey = false;
                            String content1="";
                            String content2="";
                            String insertNewId="";
                            String insertType="";
                            String insertTitle="";
                            String insertImage="";
                            String insertContent="";

                            //Sometimes NO Section_ID and DATE: Occur on ADS Type1
                            String section_id="", pubdate="", author="", title="", photo="", type="";
                            if(response.body().has("section_id")) section_id = !response.body().get("section_id").isJsonNull() ? response.body().get("section_id").getAsString() : "";
                            if(response.body().has("pubdate")) pubdate = !response.body().get("pubdate").isJsonNull() ? response.body().get("pubdate").getAsString() : "";
                            if(response.body().has("byline")) author = !response.body().get("byline").isJsonNull() ? response.body().get("byline").getAsString() : "";
                            if(response.body().has("title")) title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
                            if(response.body().has("creative")) photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
                            if(response.body().has("type")) type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";

                            //Get the SuperShare: Check if has an INSERT (for SuperShare)
                            if(response.body().has("insert")){
                                //Check if CONTENT as an ARRAY
//                                content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
//                                content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
//                                insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
//                                insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
//                                insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
//                                insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
//                                insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";

                                if(response.body().has("content")){
                                    content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
                                    content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
                                }
                                if(response.body().get("insert").getAsJsonObject().has("id")) insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("type"))  insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("title")) insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("creative")) insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("content"))  insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
                            }else {
                                //Check if CONTENT as a STRING
                                if(response.body().has("content")) content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
                            }
                            //Get the Related Article: First Check if has RelatedArticle
                            Global.relatedArticles.clear();
                            if(response.body().has("related")){
                                Log.d(TAG, "onResponse: CONTENT_  test1");
                                for(int x=0; x<response.body().get("related").getAsJsonArray().size();x++){
                                    Global.relatedArticles.add(response.body().get("related").getAsJsonArray().get(x).getAsJsonObject());
                                }
                            }
                            //Update Data for REFRESH/RECREATE
                            Global.idStr = relatedNewsId;
                            Global.titleStr = title;
                            Global.imageStr = photo;
                            Global.typeStr = type;
                            Global.pubdateStr = pubdate;
                            Global.sectionId = section_id;
                            Global.contentStr = content1;
                            Global.newImage1tStr = insertImage;
                            Global.insertTitle = insertTitle;
                            Global.insertType = insertType;
                            Global.insertNewsId = insertNewId;
                            Global.insertContent = insertContent;
                            Global.contentStr2 = content2;
                            Global.authorStr = author;
                            Global.customStr = "";
                            Global.adbanner = "";
                            Global.isRelatedArticle = true; //FLAG
                            //
                            finish();
                            startActivity(new Intent(WebNewsActivity.this, WebNewsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
//                            recreate(); //recreate NOT BEST WAY Refreshing the ACTIVITY = Calling the onCreate again, but calling first the onStop,onPause...and the rest
                        }catch (Exception e){
                            Log.d(TAG, "onResponse: onArticleRequestError viewArticle Exception: "+ e.getCause());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        if(!hasNewDailyKey) getDailyKey();
                        String error_msg = Global.getResponseCode(t);

                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
                            toastMessage(TAG+": onFailure: "+ t.getMessage());
                        }else{
                            toastMessage(error_msg+" on id #"+relatedNewsId);
                        }
                        Log.d(TAG, "onError: onArticleRequestError Msg"+ t.getMessage());
                    }
                });
    }

    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    hasNewDailyKey = true;
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }



}