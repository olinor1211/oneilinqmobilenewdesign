-keep class com.synnapps.carouselview.* { *; }
#-keep class com.synnapps.carouselview.** { *; } RESOLVED carouselview.** to carouselview.*-keepclassmembers enum * { *; }
-keepattributes *Annotation*
-keepclassmembers class * {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

-keep public class com.google.android.material.bottomnavigation.BottomNavigationView { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationMenuView { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationPresenter { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationItemView { *; }


#
#    -keep com.android.org.conscrypt.SSLParametersImpl
#    -keep org.apache.harmony.xnet.provider.jsse.SSLParametersImpl
#    -keep org.bouncycastle.jsse.BCSSLParameters
#    -keep org.bouncycastle.jsse.BCSSLSocket
#    -keep org.bouncycastle.jsse.provider.BouncyCastleJsseProvider
#    -keep org.openjsse.javax.net.ssl.SSLParameters
#    -keep org.openjsse.javax.net.ssl.SSLSocket
#    -keep org.openjsse.net.ssl.OpenJSSE

-dontwarn com.android.org.conscrypt.SSLParametersImpl
-dontwarn org.apache.harmony.xnet.provider.jsse.SSLParametersImpl
-dontwarn org.bouncycastle.jsse.BCSSLParameters
-dontwarn org.bouncycastle.jsse.BCSSLSocket
-dontwarn org.bouncycastle.jsse.provider.BouncyCastleJsseProvider
-dontwarn org.openjsse.javax.net.ssl.SSLParameters
-dontwarn org.openjsse.javax.net.ssl.SSLSocket
-dontwarn org.openjsse.net.ssl.OpenJSSE



