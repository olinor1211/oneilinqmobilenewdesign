/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.knx.inquirer;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.knx.inquirer";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 191;
  public static final String VERSION_NAME = "11.91";
}
