package com.knx.inquirer.database;

import android.database.Cursor;
import androidx.annotation.NonNull;
import androidx.lifecycle.ComputableLiveData;
import androidx.lifecycle.LiveData;
import androidx.room.EntityInsertionAdapter;
import androidx.room.InvalidationTracker.Observer;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.model.PrimeLikeModel;
import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;
import com.knx.inquirer.inqprime.model.PrimeSectionsModel;
import com.knx.inquirer.inqprime.model.PrimeTopicsModel;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.models.Option;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.utils.Converters;
import java.lang.Boolean;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unchecked")
public final class NewsDao_Impl implements NewsDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfNewsModel;

  private final EntityInsertionAdapter __insertionAdapterOfFeed;

  private final EntityInsertionAdapter __insertionAdapterOfSection;

  private final EntityInsertionAdapter __insertionAdapterOfOption;

  private final EntityInsertionAdapter __insertionAdapterOfNotificationModel;

  private final EntityInsertionAdapter __insertionAdapterOfPrimeBookmarkModel;

  private final EntityInsertionAdapter __insertionAdapterOfPrimeLikeModel;

  private final EntityInsertionAdapter __insertionAdapterOfPrimeAccountInformationModel;

  private final EntityInsertionAdapter __insertionAdapterOfPrimeSectionsModel;

  private final EntityInsertionAdapter __insertionAdapterOfPrimeTopicsModel;

  private final EntityInsertionAdapter __insertionAdapterOfPrimeNewsOfflineModel;

  private final EntityInsertionAdapter __insertionAdapterOfBookmarksNewModel;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllNews;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllCustomNews;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllSections;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllCustomSections;

  private final SharedSQLiteStatement __preparedStmtOfUpdateNotifStatus2;

  private final SharedSQLiteStatement __preparedStmtOfReadAllNotification;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllNotifications;

  private final SharedSQLiteStatement __preparedStmtOfDeleteOneNotification;

  private final SharedSQLiteStatement __preparedStmtOfDeleteOnePrimeBookmark;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimeAvatar;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimeEmail;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimeMobileNo;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimeCardNo;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimePlan;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimeSectionStatus;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimeTopicsStatus;

  private final SharedSQLiteStatement __preparedStmtOfDeleteSectionPrimeNewsOffline;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllPrimeNewsOffline;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimeNews;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePrimeNewsImage;

  private final SharedSQLiteStatement __preparedStmtOfDeleteOneBookmark;

  public NewsDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfNewsModel = new EntityInsertionAdapter<NewsModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `News`(`id`,`type`,`title`,`image`,`pubdate`,`link`,`label`,`section`) VALUES (?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, NewsModel value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getType() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getType());
        }
        if (value.getTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getTitle());
        }
        if (value.getImage() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getImage());
        }
        if (value.getPubdate() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getPubdate());
        }
        if (value.link == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.link);
        }
        if (value.label == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.label);
        }
        if (value.section == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.section);
        }
      }
    };
    this.__insertionAdapterOfFeed = new EntityInsertionAdapter<Feed>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `CustomNews`(`id`,`type`,`title`,`image`,`pubdate`,`link`,`label`,`section`) VALUES (?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Feed value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getType() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getType());
        }
        if (value.getTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getTitle());
        }
        if (value.getImage() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getImage());
        }
        if (value.getPubdate() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getPubdate());
        }
        if (value.link == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.link);
        }
        if (value.label == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.label);
        }
        if (value.section == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.section);
        }
      }
    };
    this.__insertionAdapterOfSection = new EntityInsertionAdapter<Section>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `Sections`(`id`,`title`,`url`,`color`,`isSelected`) VALUES (?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Section value) {
        stmt.bindLong(1, value.getId());
        if (value.getTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitle());
        }
        if (value.getUrl() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getUrl());
        }
        if (value.getColor() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getColor());
        }
        final int _tmp;
        _tmp = value.isSelected() ? 1 : 0;
        stmt.bindLong(5, _tmp);
      }
    };
    this.__insertionAdapterOfOption = new EntityInsertionAdapter<Option>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `CustomSections`(`id`,`title`,`url`,`isSelected`) VALUES (?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Option value) {
        stmt.bindLong(1, value.getId());
        if (value.getTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitle());
        }
        if (value.getUrl() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getUrl());
        }
        final int _tmp;
        _tmp = value.isSelected() ? 1 : 0;
        stmt.bindLong(4, _tmp);
      }
    };
    this.__insertionAdapterOfNotificationModel = new EntityInsertionAdapter<NotificationModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `Notifs`(`id`,`news_id`,`image`,`link`,`message`,`title`,`timestamp`,`readstatus`) VALUES (nullif(?, 0),?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, NotificationModel value) {
        stmt.bindLong(1, value.getId());
        if (value.getNews_id() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getNews_id());
        }
        if (value.getImage() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getImage());
        }
        if (value.getLink() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getLink());
        }
        if (value.getMessage() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getMessage());
        }
        if (value.getTitle() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getTitle());
        }
        final Long _tmp;
        _tmp = Converters.dateToTimestamp(value.getTimestamp());
        if (_tmp == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindLong(7, _tmp);
        }
        stmt.bindLong(8, value.getReadstatus());
      }
    };
    this.__insertionAdapterOfPrimeBookmarkModel = new EntityInsertionAdapter<PrimeBookmarkModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `PrimeBookmark`(`newsId`,`image`,`title`,`date`,`author`) VALUES (?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, PrimeBookmarkModel value) {
        stmt.bindLong(1, value.getNewsId());
        if (value.getImage() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getImage());
        }
        if (value.getTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getTitle());
        }
        if (value.getDate() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDate());
        }
        if (value.getAuthor() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getAuthor());
        }
      }
    };
    this.__insertionAdapterOfPrimeLikeModel = new EntityInsertionAdapter<PrimeLikeModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `PrimeLike`(`newsId`) VALUES (?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, PrimeLikeModel value) {
        stmt.bindLong(1, value.getNewsId());
      }
    };
    this.__insertionAdapterOfPrimeAccountInformationModel = new EntityInsertionAdapter<PrimeAccountInformationModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `PrimeAccountInformation`(`userId`,`accountEmail`,`accountPassword`,`userName`,`accountName`,`mobileNo`,`gender`,`address`,`birthday`,`avatar`,`cardNo`,`plan`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, PrimeAccountInformationModel value) {
        stmt.bindLong(1, value.getUserId());
        if (value.getAccountEmail() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getAccountEmail());
        }
        if (value.getAccountPassword() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getAccountPassword());
        }
        if (value.getUserName() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getUserName());
        }
        if (value.getAccountName() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getAccountName());
        }
        if (value.getMobileNo() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getMobileNo());
        }
        if (value.getGender() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getGender());
        }
        if (value.getAddress() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getAddress());
        }
        if (value.getBirthday() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getBirthday());
        }
        if (value.getAvatar() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getAvatar());
        }
        if (value.getCardNo() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.getCardNo());
        }
        if (value.getPlan() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.getPlan());
        }
      }
    };
    this.__insertionAdapterOfPrimeSectionsModel = new EntityInsertionAdapter<PrimeSectionsModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `PrimeSections`(`primeId`,`PrimeSections`,`Status`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, PrimeSectionsModel value) {
        stmt.bindLong(1, value.getPrimeId());
        if (value.getPrimeSections() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getPrimeSections());
        }
        final Integer _tmp;
        _tmp = value.getStatus() == null ? null : (value.getStatus() ? 1 : 0);
        if (_tmp == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, _tmp);
        }
      }
    };
    this.__insertionAdapterOfPrimeTopicsModel = new EntityInsertionAdapter<PrimeTopicsModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `PrimeTopics`(`PrimeId`,`PrimeTopics`,`Status`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, PrimeTopicsModel value) {
        stmt.bindLong(1, value.getPrimeId());
        if (value.getPrimeTopics() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getPrimeTopics());
        }
        final Integer _tmp;
        _tmp = value.getStatus() == null ? null : (value.getStatus() ? 1 : 0);
        if (_tmp == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, _tmp);
        }
      }
    };
    this.__insertionAdapterOfPrimeNewsOfflineModel = new EntityInsertionAdapter<PrimeNewsOfflineModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `PrimeNewsOffline`(`news_Id`,`image_Type`,`title`,`photo`,`published_Date`,`author`,`content`,`description`,`link`,`tag`,`section`,`offline_photo`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, PrimeNewsOfflineModel value) {
        if (value.getNews_Id() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getNews_Id());
        }
        if (value.getImage_Type() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getImage_Type());
        }
        if (value.getTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getTitle());
        }
        if (value.getPhoto() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getPhoto());
        }
        if (value.getPublished_Date() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getPublished_Date());
        }
        if (value.getAuthor() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getAuthor());
        }
        if (value.getContent() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getContent());
        }
        if (value.getDescription() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getDescription());
        }
        if (value.getLink() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getLink());
        }
        if (value.getTag() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getTag());
        }
        stmt.bindLong(11, value.getSection());
        if (value.getOffline_photo() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindBlob(12, value.getOffline_photo());
        }
      }
    };
    this.__insertionAdapterOfBookmarksNewModel = new EntityInsertionAdapter<BookmarksNewModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `BookmarksNew`(`newsId`,`id`,`title`,`mainImage`,`type`,`date`,`content1`,`image1`,`content2`,`author`,`custom`,`bannerSectionId`,`sectionName`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, BookmarksNewModel value) {
        stmt.bindLong(1, value.getNewsId());
        stmt.bindLong(2, value.getId());
        if (value.getTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getTitle());
        }
        if (value.getMainImage() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getMainImage());
        }
        if (value.getType() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getType());
        }
        if (value.getDate() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getDate());
        }
        if (value.getContent1() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getContent1());
        }
        if (value.getImage1() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getImage1());
        }
        if (value.getContent2() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getContent2());
        }
        if (value.getAuthor() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getAuthor());
        }
        if (value.getCustom() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.getCustom());
        }
        if (value.getBannerSectionId() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.getBannerSectionId());
        }
        if (value.getSectionName() == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.getSectionName());
        }
      }
    };
    this.__preparedStmtOfDeleteAllNews = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM News";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllCustomNews = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM CustomNews";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllSections = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM Sections";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllCustomSections = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM CustomSections";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateNotifStatus2 = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE Notifs SET readstatus = ? WHERE news_id =?";
        return _query;
      }
    };
    this.__preparedStmtOfReadAllNotification = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE Notifs SET readstatus = 1";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllNotifications = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM Notifs";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteOneNotification = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM Notifs WHERE id = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteOnePrimeBookmark = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM PrimeBookmark WHERE newsId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimeAvatar = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeAccountInformation SET avatar = ? WHERE userId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimeEmail = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeAccountInformation SET accountEmail = ? WHERE userId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimeMobileNo = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeAccountInformation SET mobileNo = ? WHERE userId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimeCardNo = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeAccountInformation SET cardNo = ? WHERE userId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimePlan = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeAccountInformation SET `plan` = ? WHERE userId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimeSectionStatus = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeSections SET status = ? WHERE primeId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimeTopicsStatus = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeTopics SET status = ? WHERE primeId = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteSectionPrimeNewsOffline = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM PrimeNewsOffline WHERE section = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllPrimeNewsOffline = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM PrimeNewsOffline";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimeNews = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeNewsOffline SET author = ?, content = ?, image_Type = ? WHERE news_Id = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdatePrimeNewsImage = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE PrimeNewsOffline SET offline_photo = ? WHERE news_Id = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteOneBookmark = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BookmarksNew WHERE newsId = ?";
        return _query;
      }
    };
  }

  @Override
  public void saveAll(List<NewsModel> news) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfNewsModel.insert(news);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void saveAllCustomNews(List<Feed> news) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfFeed.insert(news);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void saveSections(List<Section> sections) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfSection.insert(sections);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void saveCustomSections(List<Option> sections) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfOption.insert(sections);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void saveNotification(NotificationModel notifs) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfNotificationModel.insert(notifs);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void savePrimeBookmark(PrimeBookmarkModel bookmark) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfPrimeBookmarkModel.insert(bookmark);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void savePrimeLike(PrimeLikeModel like) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfPrimeLikeModel.insert(like);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void savePrimeAccountInformation(PrimeAccountInformationModel primeAccountInformationModel) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfPrimeAccountInformationModel.insert(primeAccountInformationModel);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void savePrimeSections(PrimeSectionsModel status) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfPrimeSectionsModel.insert(status);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void savePrimeTopics(PrimeTopicsModel status) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfPrimeTopicsModel.insert(status);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void savePrimeNews(List<PrimeNewsOfflineModel> news) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfPrimeNewsOfflineModel.insert(news);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void saveBookmark(BookmarksNewModel bookmark) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfBookmarksNewModel.insert(bookmark);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAllNews() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllNews.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllNews.release(_stmt);
    }
  }

  @Override
  public void deleteAllCustomNews() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllCustomNews.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllCustomNews.release(_stmt);
    }
  }

  @Override
  public void deleteAllSections() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllSections.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllSections.release(_stmt);
    }
  }

  @Override
  public void deleteAllCustomSections() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllCustomSections.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllCustomSections.release(_stmt);
    }
  }

  @Override
  public void updateNotifStatus2(int status, String newsId) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateNotifStatus2.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, status);
      _argIndex = 2;
      if (newsId == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, newsId);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateNotifStatus2.release(_stmt);
    }
  }

  @Override
  public void readAllNotification() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfReadAllNotification.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfReadAllNotification.release(_stmt);
    }
  }

  @Override
  public void deleteAllNotifications() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllNotifications.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllNotifications.release(_stmt);
    }
  }

  @Override
  public void deleteOneNotification(int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteOneNotification.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteOneNotification.release(_stmt);
    }
  }

  @Override
  public void deleteOnePrimeBookmark(int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteOnePrimeBookmark.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteOnePrimeBookmark.release(_stmt);
    }
  }

  @Override
  public void updatePrimeAvatar(String avatar, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimeAvatar.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (avatar == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, avatar);
      }
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimeAvatar.release(_stmt);
    }
  }

  @Override
  public void updatePrimeEmail(String email, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimeEmail.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (email == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, email);
      }
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimeEmail.release(_stmt);
    }
  }

  @Override
  public void updatePrimeMobileNo(String number, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimeMobileNo.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (number == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, number);
      }
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimeMobileNo.release(_stmt);
    }
  }

  @Override
  public void updatePrimeCardNo(String number, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimeCardNo.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (number == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, number);
      }
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimeCardNo.release(_stmt);
    }
  }

  @Override
  public void updatePrimePlan(String plan, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimePlan.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (plan == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, plan);
      }
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimePlan.release(_stmt);
    }
  }

  @Override
  public void updatePrimeSectionStatus(Boolean status, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimeSectionStatus.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      final Integer _tmp;
      _tmp = status == null ? null : (status ? 1 : 0);
      if (_tmp == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindLong(_argIndex, _tmp);
      }
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimeSectionStatus.release(_stmt);
    }
  }

  @Override
  public void updatePrimeTopicsStatus(Boolean status, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimeTopicsStatus.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      final Integer _tmp;
      _tmp = status == null ? null : (status ? 1 : 0);
      if (_tmp == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindLong(_argIndex, _tmp);
      }
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimeTopicsStatus.release(_stmt);
    }
  }

  @Override
  public void deleteSectionPrimeNewsOffline(int sectionId) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteSectionPrimeNewsOffline.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, sectionId);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteSectionPrimeNewsOffline.release(_stmt);
    }
  }

  @Override
  public void deleteAllPrimeNewsOffline() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllPrimeNewsOffline.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllPrimeNewsOffline.release(_stmt);
    }
  }

  @Override
  public void updatePrimeNews(String author, String content, String imageType, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimeNews.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (author == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, author);
      }
      _argIndex = 2;
      if (content == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, content);
      }
      _argIndex = 3;
      if (imageType == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, imageType);
      }
      _argIndex = 4;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimeNews.release(_stmt);
    }
  }

  @Override
  public void updatePrimeNewsImage(byte[] offline_photo, int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePrimeNewsImage.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (offline_photo == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindBlob(_argIndex, offline_photo);
      }
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePrimeNewsImage.release(_stmt);
    }
  }

  @Override
  public void deleteOneBookmark(int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteOneBookmark.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteOneBookmark.release(_stmt);
    }
  }

  @Override
  public LiveData<List<NewsModel>> getAllNews() {
    final String _sql = "SELECT * FROM News LIMIT 20";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<NewsModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<NewsModel> compute() {
        if (_observer == null) {
          _observer = new Observer("News") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfType = _cursor.getColumnIndexOrThrow("type");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
          final int _cursorIndexOfPubdate = _cursor.getColumnIndexOrThrow("pubdate");
          final int _cursorIndexOfLink = _cursor.getColumnIndexOrThrow("link");
          final int _cursorIndexOfLabel = _cursor.getColumnIndexOrThrow("label");
          final int _cursorIndexOfSection = _cursor.getColumnIndexOrThrow("section");
          final List<NewsModel> _result = new ArrayList<NewsModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final NewsModel _item;
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            final String _tmpType;
            _tmpType = _cursor.getString(_cursorIndexOfType);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            final String _tmpPubdate;
            _tmpPubdate = _cursor.getString(_cursorIndexOfPubdate);
            final String _tmpLink;
            _tmpLink = _cursor.getString(_cursorIndexOfLink);
            final String _tmpLabel;
            _tmpLabel = _cursor.getString(_cursorIndexOfLabel);
            final String _tmpSection;
            _tmpSection = _cursor.getString(_cursorIndexOfSection);
            _item = new NewsModel(_tmpId,_tmpType,_tmpTitle,_tmpImage,_tmpPubdate,_tmpLink,_tmpLabel,_tmpSection);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<Integer> getNewsRecordCount() {
    final String _sql = "SELECT COUNT() FROM News";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<Integer>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected Integer compute() {
        if (_observer == null) {
          _observer = new Observer("News") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final Integer _result;
          if(_cursor.moveToFirst()) {
            final Integer _tmp;
            if (_cursor.isNull(0)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(0);
            }
            _result = _tmp;
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<Feed>> getAllCustomNews() {
    final String _sql = "SELECT * FROM CustomNews";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Feed>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<Feed> compute() {
        if (_observer == null) {
          _observer = new Observer("CustomNews") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfType = _cursor.getColumnIndexOrThrow("type");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
          final int _cursorIndexOfPubdate = _cursor.getColumnIndexOrThrow("pubdate");
          final int _cursorIndexOfLink = _cursor.getColumnIndexOrThrow("link");
          final int _cursorIndexOfLabel = _cursor.getColumnIndexOrThrow("label");
          final int _cursorIndexOfSection = _cursor.getColumnIndexOrThrow("section");
          final List<Feed> _result = new ArrayList<Feed>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Feed _item;
            _item = new Feed();
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            _item.setId(_tmpId);
            final String _tmpType;
            _tmpType = _cursor.getString(_cursorIndexOfType);
            _item.setType(_tmpType);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            _item.setTitle(_tmpTitle);
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            _item.setImage(_tmpImage);
            final String _tmpPubdate;
            _tmpPubdate = _cursor.getString(_cursorIndexOfPubdate);
            _item.setPubdate(_tmpPubdate);
            _item.link = _cursor.getString(_cursorIndexOfLink);
            _item.label = _cursor.getString(_cursorIndexOfLabel);
            _item.section = _cursor.getString(_cursorIndexOfSection);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<Integer> getCustomNewsRecordCount() {
    final String _sql = "SELECT COUNT() FROM CustomNews";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<Integer>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected Integer compute() {
        if (_observer == null) {
          _observer = new Observer("CustomNews") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final Integer _result;
          if(_cursor.moveToFirst()) {
            final Integer _tmp;
            if (_cursor.isNull(0)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(0);
            }
            _result = _tmp;
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<Section>> getAllSections() {
    final String _sql = "SELECT * FROM Sections";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Section>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<Section> compute() {
        if (_observer == null) {
          _observer = new Observer("Sections") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfUrl = _cursor.getColumnIndexOrThrow("url");
          final int _cursorIndexOfColor = _cursor.getColumnIndexOrThrow("color");
          final int _cursorIndexOfIsSelected = _cursor.getColumnIndexOrThrow("isSelected");
          final List<Section> _result = new ArrayList<Section>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Section _item;
            final boolean _tmpIsSelected;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfIsSelected);
            _tmpIsSelected = _tmp != 0;
            _item = new Section(_tmpIsSelected);
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            _item.setId(_tmpId);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            _item.setTitle(_tmpTitle);
            final String _tmpUrl;
            _tmpUrl = _cursor.getString(_cursorIndexOfUrl);
            _item.setUrl(_tmpUrl);
            final String _tmpColor;
            _tmpColor = _cursor.getString(_cursorIndexOfColor);
            _item.setColor(_tmpColor);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<Integer> getSectionRecordCount() {
    final String _sql = "SELECT COUNT() FROM Sections";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<Integer>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected Integer compute() {
        if (_observer == null) {
          _observer = new Observer("Sections") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final Integer _result;
          if(_cursor.moveToFirst()) {
            final Integer _tmp;
            if (_cursor.isNull(0)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(0);
            }
            _result = _tmp;
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<Option>> getAllCustomSections() {
    final String _sql = "SELECT * FROM CustomSections";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Option>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<Option> compute() {
        if (_observer == null) {
          _observer = new Observer("CustomSections") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfUrl = _cursor.getColumnIndexOrThrow("url");
          final int _cursorIndexOfIsSelected = _cursor.getColumnIndexOrThrow("isSelected");
          final List<Option> _result = new ArrayList<Option>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Option _item;
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            final boolean _tmpIsSelected;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfIsSelected);
            _tmpIsSelected = _tmp != 0;
            _item = new Option(_tmpId,_tmpTitle,_tmpIsSelected);
            final String _tmpUrl;
            _tmpUrl = _cursor.getString(_cursorIndexOfUrl);
            _item.setUrl(_tmpUrl);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<Integer> getCustomSectionRecordCount() {
    final String _sql = "SELECT COUNT() FROM CustomSections";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<Integer>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected Integer compute() {
        if (_observer == null) {
          _observer = new Observer("CustomSections") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final Integer _result;
          if(_cursor.moveToFirst()) {
            final Integer _tmp;
            if (_cursor.isNull(0)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(0);
            }
            _result = _tmp;
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<NotificationModel>> getAllNotifications() {
    final String _sql = "SELECT * FROM Notifs ORDER BY timestamp DESC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<NotificationModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<NotificationModel> compute() {
        if (_observer == null) {
          _observer = new Observer("Notifs") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfNewsId = _cursor.getColumnIndexOrThrow("news_id");
          final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
          final int _cursorIndexOfLink = _cursor.getColumnIndexOrThrow("link");
          final int _cursorIndexOfMessage = _cursor.getColumnIndexOrThrow("message");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfTimestamp = _cursor.getColumnIndexOrThrow("timestamp");
          final int _cursorIndexOfReadstatus = _cursor.getColumnIndexOrThrow("readstatus");
          final List<NotificationModel> _result = new ArrayList<NotificationModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final NotificationModel _item;
            final String _tmpNews_id;
            _tmpNews_id = _cursor.getString(_cursorIndexOfNewsId);
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            final String _tmpLink;
            _tmpLink = _cursor.getString(_cursorIndexOfLink);
            final String _tmpMessage;
            _tmpMessage = _cursor.getString(_cursorIndexOfMessage);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            final Date _tmpTimestamp;
            final Long _tmp;
            if (_cursor.isNull(_cursorIndexOfTimestamp)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getLong(_cursorIndexOfTimestamp);
            }
            _tmpTimestamp = Converters.fromTimestamp(_tmp);
            final int _tmpReadstatus;
            _tmpReadstatus = _cursor.getInt(_cursorIndexOfReadstatus);
            _item = new NotificationModel(_tmpNews_id,_tmpImage,_tmpLink,_tmpTitle,_tmpMessage,_tmpTimestamp,_tmpReadstatus);
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            _item.setId(_tmpId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<Integer> getUnreadNotifsCount() {
    final String _sql = "SELECT COUNT(*) FROM (SELECT * FROM Notifs ORDER BY id DESC LIMIT 10) WHERE readstatus = 0";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<Integer>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected Integer compute() {
        if (_observer == null) {
          _observer = new Observer("Notifs") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final Integer _result;
          if(_cursor.moveToFirst()) {
            final Integer _tmp;
            if (_cursor.isNull(0)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(0);
            }
            _result = _tmp;
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<PrimeBookmarkModel>> getPrimeBookmarks() {
    final String _sql = "SELECT * FROM PrimeBookmark";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<PrimeBookmarkModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<PrimeBookmarkModel> compute() {
        if (_observer == null) {
          _observer = new Observer("PrimeBookmark") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfNewsId = _cursor.getColumnIndexOrThrow("newsId");
          final int _cursorIndexOfImage = _cursor.getColumnIndexOrThrow("image");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfDate = _cursor.getColumnIndexOrThrow("date");
          final int _cursorIndexOfAuthor = _cursor.getColumnIndexOrThrow("author");
          final List<PrimeBookmarkModel> _result = new ArrayList<PrimeBookmarkModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final PrimeBookmarkModel _item;
            final int _tmpNewsId;
            _tmpNewsId = _cursor.getInt(_cursorIndexOfNewsId);
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            final String _tmpDate;
            _tmpDate = _cursor.getString(_cursorIndexOfDate);
            final String _tmpAuthor;
            _tmpAuthor = _cursor.getString(_cursorIndexOfAuthor);
            _item = new PrimeBookmarkModel(_tmpNewsId,_tmpImage,_tmpTitle,_tmpDate,_tmpAuthor);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<PrimeLikeModel>> getPrimeLike() {
    final String _sql = "SELECT * FROM PrimeLike";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<PrimeLikeModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<PrimeLikeModel> compute() {
        if (_observer == null) {
          _observer = new Observer("PrimeLike") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfNewsId = _cursor.getColumnIndexOrThrow("newsId");
          final List<PrimeLikeModel> _result = new ArrayList<PrimeLikeModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final PrimeLikeModel _item;
            final int _tmpNewsId;
            _tmpNewsId = _cursor.getInt(_cursorIndexOfNewsId);
            _item = new PrimeLikeModel(_tmpNewsId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<PrimeAccountInformationModel>> getPrimeAccountInformation() {
    final String _sql = "SELECT * FROM PrimeAccountInformation";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<PrimeAccountInformationModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<PrimeAccountInformationModel> compute() {
        if (_observer == null) {
          _observer = new Observer("PrimeAccountInformation") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("userId");
          final int _cursorIndexOfAccountEmail = _cursor.getColumnIndexOrThrow("accountEmail");
          final int _cursorIndexOfAccountPassword = _cursor.getColumnIndexOrThrow("accountPassword");
          final int _cursorIndexOfUserName = _cursor.getColumnIndexOrThrow("userName");
          final int _cursorIndexOfAccountName = _cursor.getColumnIndexOrThrow("accountName");
          final int _cursorIndexOfMobileNo = _cursor.getColumnIndexOrThrow("mobileNo");
          final int _cursorIndexOfGender = _cursor.getColumnIndexOrThrow("gender");
          final int _cursorIndexOfAddress = _cursor.getColumnIndexOrThrow("address");
          final int _cursorIndexOfBirthday = _cursor.getColumnIndexOrThrow("birthday");
          final int _cursorIndexOfAvatar = _cursor.getColumnIndexOrThrow("avatar");
          final int _cursorIndexOfCardNo = _cursor.getColumnIndexOrThrow("cardNo");
          final int _cursorIndexOfPlan = _cursor.getColumnIndexOrThrow("plan");
          final List<PrimeAccountInformationModel> _result = new ArrayList<PrimeAccountInformationModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final PrimeAccountInformationModel _item;
            final int _tmpUserId;
            _tmpUserId = _cursor.getInt(_cursorIndexOfUserId);
            final String _tmpAccountEmail;
            _tmpAccountEmail = _cursor.getString(_cursorIndexOfAccountEmail);
            final String _tmpAccountPassword;
            _tmpAccountPassword = _cursor.getString(_cursorIndexOfAccountPassword);
            final String _tmpUserName;
            _tmpUserName = _cursor.getString(_cursorIndexOfUserName);
            final String _tmpAccountName;
            _tmpAccountName = _cursor.getString(_cursorIndexOfAccountName);
            final String _tmpMobileNo;
            _tmpMobileNo = _cursor.getString(_cursorIndexOfMobileNo);
            final String _tmpGender;
            _tmpGender = _cursor.getString(_cursorIndexOfGender);
            final String _tmpAddress;
            _tmpAddress = _cursor.getString(_cursorIndexOfAddress);
            final String _tmpBirthday;
            _tmpBirthday = _cursor.getString(_cursorIndexOfBirthday);
            final String _tmpAvatar;
            _tmpAvatar = _cursor.getString(_cursorIndexOfAvatar);
            final String _tmpCardNo;
            _tmpCardNo = _cursor.getString(_cursorIndexOfCardNo);
            final String _tmpPlan;
            _tmpPlan = _cursor.getString(_cursorIndexOfPlan);
            _item = new PrimeAccountInformationModel(_tmpUserId,_tmpAccountEmail,_tmpAccountPassword,_tmpUserName,_tmpAccountName,_tmpMobileNo,_tmpGender,_tmpAddress,_tmpBirthday,_tmpAvatar,_tmpCardNo,_tmpPlan);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public List<PrimeSectionsModel> getPrimeSections() {
    final String _sql = "SELECT * FROM PrimeSections";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfPrimeId = _cursor.getColumnIndexOrThrow("primeId");
      final int _cursorIndexOfPrimeSections = _cursor.getColumnIndexOrThrow("PrimeSections");
      final int _cursorIndexOfStatus = _cursor.getColumnIndexOrThrow("Status");
      final List<PrimeSectionsModel> _result = new ArrayList<PrimeSectionsModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final PrimeSectionsModel _item;
        _item = new PrimeSectionsModel();
        final int _tmpPrimeId;
        _tmpPrimeId = _cursor.getInt(_cursorIndexOfPrimeId);
        _item.setPrimeId(_tmpPrimeId);
        final String _tmpPrimeSections;
        _tmpPrimeSections = _cursor.getString(_cursorIndexOfPrimeSections);
        _item.setPrimeSections(_tmpPrimeSections);
        final Boolean _tmpStatus;
        final Integer _tmp;
        if (_cursor.isNull(_cursorIndexOfStatus)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(_cursorIndexOfStatus);
        }
        _tmpStatus = _tmp == null ? null : _tmp != 0;
        _item.setStatus(_tmpStatus);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public LiveData<List<PrimeSectionsModel>> getPrimeSectionsStatus() {
    final String _sql = "SELECT * FROM PrimeSections WHERE status = 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<PrimeSectionsModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<PrimeSectionsModel> compute() {
        if (_observer == null) {
          _observer = new Observer("PrimeSections") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfPrimeId = _cursor.getColumnIndexOrThrow("primeId");
          final int _cursorIndexOfPrimeSections = _cursor.getColumnIndexOrThrow("PrimeSections");
          final int _cursorIndexOfStatus = _cursor.getColumnIndexOrThrow("Status");
          final List<PrimeSectionsModel> _result = new ArrayList<PrimeSectionsModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final PrimeSectionsModel _item;
            _item = new PrimeSectionsModel();
            final int _tmpPrimeId;
            _tmpPrimeId = _cursor.getInt(_cursorIndexOfPrimeId);
            _item.setPrimeId(_tmpPrimeId);
            final String _tmpPrimeSections;
            _tmpPrimeSections = _cursor.getString(_cursorIndexOfPrimeSections);
            _item.setPrimeSections(_tmpPrimeSections);
            final Boolean _tmpStatus;
            final Integer _tmp;
            if (_cursor.isNull(_cursorIndexOfStatus)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(_cursorIndexOfStatus);
            }
            _tmpStatus = _tmp == null ? null : _tmp != 0;
            _item.setStatus(_tmpStatus);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<PrimeTopicsModel>> getPrimeTopicsStatus() {
    final String _sql = "SELECT * FROM PrimeTopics WHERE status = 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<PrimeTopicsModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<PrimeTopicsModel> compute() {
        if (_observer == null) {
          _observer = new Observer("PrimeTopics") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfPrimeId = _cursor.getColumnIndexOrThrow("PrimeId");
          final int _cursorIndexOfPrimeTopics = _cursor.getColumnIndexOrThrow("PrimeTopics");
          final int _cursorIndexOfStatus = _cursor.getColumnIndexOrThrow("Status");
          final List<PrimeTopicsModel> _result = new ArrayList<PrimeTopicsModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final PrimeTopicsModel _item;
            _item = new PrimeTopicsModel();
            final int _tmpPrimeId;
            _tmpPrimeId = _cursor.getInt(_cursorIndexOfPrimeId);
            _item.setPrimeId(_tmpPrimeId);
            final String _tmpPrimeTopics;
            _tmpPrimeTopics = _cursor.getString(_cursorIndexOfPrimeTopics);
            _item.setPrimeTopics(_tmpPrimeTopics);
            final Boolean _tmpStatus;
            final Integer _tmp;
            if (_cursor.isNull(_cursorIndexOfStatus)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(_cursorIndexOfStatus);
            }
            _tmpStatus = _tmp == null ? null : _tmp != 0;
            _item.setStatus(_tmpStatus);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public List<PrimeNewsOfflineModel> getArticleContent(int id) {
    final String _sql = "SELECT * FROM PrimeNewsOffline WHERE news_Id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfNewsId = _cursor.getColumnIndexOrThrow("news_Id");
      final int _cursorIndexOfImageType = _cursor.getColumnIndexOrThrow("image_Type");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfPhoto = _cursor.getColumnIndexOrThrow("photo");
      final int _cursorIndexOfPublishedDate = _cursor.getColumnIndexOrThrow("published_Date");
      final int _cursorIndexOfAuthor = _cursor.getColumnIndexOrThrow("author");
      final int _cursorIndexOfContent = _cursor.getColumnIndexOrThrow("content");
      final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("description");
      final int _cursorIndexOfLink = _cursor.getColumnIndexOrThrow("link");
      final int _cursorIndexOfTag = _cursor.getColumnIndexOrThrow("tag");
      final int _cursorIndexOfSection = _cursor.getColumnIndexOrThrow("section");
      final int _cursorIndexOfOfflinePhoto = _cursor.getColumnIndexOrThrow("offline_photo");
      final List<PrimeNewsOfflineModel> _result = new ArrayList<PrimeNewsOfflineModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final PrimeNewsOfflineModel _item;
        final String _tmpNews_Id;
        _tmpNews_Id = _cursor.getString(_cursorIndexOfNewsId);
        final String _tmpImage_Type;
        _tmpImage_Type = _cursor.getString(_cursorIndexOfImageType);
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        final String _tmpPhoto;
        _tmpPhoto = _cursor.getString(_cursorIndexOfPhoto);
        final String _tmpPublished_Date;
        _tmpPublished_Date = _cursor.getString(_cursorIndexOfPublishedDate);
        final String _tmpAuthor;
        _tmpAuthor = _cursor.getString(_cursorIndexOfAuthor);
        final String _tmpContent;
        _tmpContent = _cursor.getString(_cursorIndexOfContent);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        final String _tmpLink;
        _tmpLink = _cursor.getString(_cursorIndexOfLink);
        final String _tmpTag;
        _tmpTag = _cursor.getString(_cursorIndexOfTag);
        final int _tmpSection;
        _tmpSection = _cursor.getInt(_cursorIndexOfSection);
        final byte[] _tmpOffline_photo;
        _tmpOffline_photo = _cursor.getBlob(_cursorIndexOfOfflinePhoto);
        _item = new PrimeNewsOfflineModel(_tmpNews_Id,_tmpImage_Type,_tmpTitle,_tmpPhoto,_tmpPublished_Date,_tmpAuthor,_tmpContent,_tmpDescription,_tmpLink,_tmpTag,_tmpSection,_tmpOffline_photo);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public LiveData<List<PrimeNewsOfflineModel>> getAllPrimeNews() {
    final String _sql = "SELECT * FROM PrimeNewsOffline LIMIT 20";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<PrimeNewsOfflineModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<PrimeNewsOfflineModel> compute() {
        if (_observer == null) {
          _observer = new Observer("PrimeNewsOffline") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfNewsId = _cursor.getColumnIndexOrThrow("news_Id");
          final int _cursorIndexOfImageType = _cursor.getColumnIndexOrThrow("image_Type");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfPhoto = _cursor.getColumnIndexOrThrow("photo");
          final int _cursorIndexOfPublishedDate = _cursor.getColumnIndexOrThrow("published_Date");
          final int _cursorIndexOfAuthor = _cursor.getColumnIndexOrThrow("author");
          final int _cursorIndexOfContent = _cursor.getColumnIndexOrThrow("content");
          final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("description");
          final int _cursorIndexOfLink = _cursor.getColumnIndexOrThrow("link");
          final int _cursorIndexOfTag = _cursor.getColumnIndexOrThrow("tag");
          final int _cursorIndexOfSection = _cursor.getColumnIndexOrThrow("section");
          final int _cursorIndexOfOfflinePhoto = _cursor.getColumnIndexOrThrow("offline_photo");
          final List<PrimeNewsOfflineModel> _result = new ArrayList<PrimeNewsOfflineModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final PrimeNewsOfflineModel _item;
            final String _tmpNews_Id;
            _tmpNews_Id = _cursor.getString(_cursorIndexOfNewsId);
            final String _tmpImage_Type;
            _tmpImage_Type = _cursor.getString(_cursorIndexOfImageType);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            final String _tmpPhoto;
            _tmpPhoto = _cursor.getString(_cursorIndexOfPhoto);
            final String _tmpPublished_Date;
            _tmpPublished_Date = _cursor.getString(_cursorIndexOfPublishedDate);
            final String _tmpAuthor;
            _tmpAuthor = _cursor.getString(_cursorIndexOfAuthor);
            final String _tmpContent;
            _tmpContent = _cursor.getString(_cursorIndexOfContent);
            final String _tmpDescription;
            _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            final String _tmpLink;
            _tmpLink = _cursor.getString(_cursorIndexOfLink);
            final String _tmpTag;
            _tmpTag = _cursor.getString(_cursorIndexOfTag);
            final int _tmpSection;
            _tmpSection = _cursor.getInt(_cursorIndexOfSection);
            final byte[] _tmpOffline_photo;
            _tmpOffline_photo = _cursor.getBlob(_cursorIndexOfOfflinePhoto);
            _item = new PrimeNewsOfflineModel(_tmpNews_Id,_tmpImage_Type,_tmpTitle,_tmpPhoto,_tmpPublished_Date,_tmpAuthor,_tmpContent,_tmpDescription,_tmpLink,_tmpTag,_tmpSection,_tmpOffline_photo);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<BookmarksNewModel>> getAllBookmarks() {
    final String _sql = "SELECT * FROM BookmarksNew ORDER BY id  DESC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<BookmarksNewModel>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<BookmarksNewModel> compute() {
        if (_observer == null) {
          _observer = new Observer("BookmarksNew") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfNewsId = _cursor.getColumnIndexOrThrow("newsId");
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfMainImage = _cursor.getColumnIndexOrThrow("mainImage");
          final int _cursorIndexOfType = _cursor.getColumnIndexOrThrow("type");
          final int _cursorIndexOfDate = _cursor.getColumnIndexOrThrow("date");
          final int _cursorIndexOfContent1 = _cursor.getColumnIndexOrThrow("content1");
          final int _cursorIndexOfImage1 = _cursor.getColumnIndexOrThrow("image1");
          final int _cursorIndexOfContent2 = _cursor.getColumnIndexOrThrow("content2");
          final int _cursorIndexOfAuthor = _cursor.getColumnIndexOrThrow("author");
          final int _cursorIndexOfCustom = _cursor.getColumnIndexOrThrow("custom");
          final int _cursorIndexOfBannerSectionId = _cursor.getColumnIndexOrThrow("bannerSectionId");
          final int _cursorIndexOfSectionName = _cursor.getColumnIndexOrThrow("sectionName");
          final List<BookmarksNewModel> _result = new ArrayList<BookmarksNewModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final BookmarksNewModel _item;
            final int _tmpNewsId;
            _tmpNewsId = _cursor.getInt(_cursorIndexOfNewsId);
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            final String _tmpContent1;
            _tmpContent1 = _cursor.getString(_cursorIndexOfContent1);
            _item = new BookmarksNewModel(_tmpNewsId,_tmpId,_tmpContent1);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            _item.setTitle(_tmpTitle);
            final String _tmpMainImage;
            _tmpMainImage = _cursor.getString(_cursorIndexOfMainImage);
            _item.setMainImage(_tmpMainImage);
            final String _tmpType;
            _tmpType = _cursor.getString(_cursorIndexOfType);
            _item.setType(_tmpType);
            final String _tmpDate;
            _tmpDate = _cursor.getString(_cursorIndexOfDate);
            _item.setDate(_tmpDate);
            final String _tmpImage1;
            _tmpImage1 = _cursor.getString(_cursorIndexOfImage1);
            _item.setImage1(_tmpImage1);
            final String _tmpContent2;
            _tmpContent2 = _cursor.getString(_cursorIndexOfContent2);
            _item.setContent2(_tmpContent2);
            final String _tmpAuthor;
            _tmpAuthor = _cursor.getString(_cursorIndexOfAuthor);
            _item.setAuthor(_tmpAuthor);
            final String _tmpCustom;
            _tmpCustom = _cursor.getString(_cursorIndexOfCustom);
            _item.setCustom(_tmpCustom);
            final String _tmpBannerSectionId;
            _tmpBannerSectionId = _cursor.getString(_cursorIndexOfBannerSectionId);
            _item.setBannerSectionId(_tmpBannerSectionId);
            final String _tmpSectionName;
            _tmpSectionName = _cursor.getString(_cursorIndexOfSectionName);
            _item.setSectionName(_tmpSectionName);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }
}
