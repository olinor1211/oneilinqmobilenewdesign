package com.knx.inquirer.database;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public final class AppDatabase_Impl extends AppDatabase {
  private volatile NewsDao _newsDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(24) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `News` (`id` TEXT NOT NULL, `type` TEXT, `title` TEXT, `image` TEXT, `pubdate` TEXT, `link` TEXT, `label` TEXT, `section` TEXT, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `CustomNews` (`id` TEXT NOT NULL, `type` TEXT, `title` TEXT, `image` TEXT, `pubdate` TEXT, `link` TEXT, `label` TEXT, `section` TEXT, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Sections` (`id` INTEGER NOT NULL, `title` TEXT, `url` TEXT, `color` TEXT, `isSelected` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `CustomSections` (`id` INTEGER NOT NULL, `title` TEXT, `url` TEXT, `isSelected` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Notifs` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `news_id` TEXT, `image` TEXT, `link` TEXT, `message` TEXT, `title` TEXT, `timestamp` INTEGER, `readstatus` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `PrimeBookmark` (`newsId` INTEGER NOT NULL, `image` TEXT, `title` TEXT, `date` TEXT, `author` TEXT, PRIMARY KEY(`newsId`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `PrimeLike` (`newsId` INTEGER NOT NULL, PRIMARY KEY(`newsId`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `PrimeAccountInformation` (`userId` INTEGER NOT NULL, `accountEmail` TEXT, `accountPassword` TEXT, `userName` TEXT, `accountName` TEXT, `mobileNo` TEXT, `gender` TEXT, `address` TEXT, `birthday` TEXT, `avatar` TEXT, `cardNo` TEXT, `plan` TEXT, PRIMARY KEY(`userId`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `PrimeSections` (`primeId` INTEGER NOT NULL, `PrimeSections` TEXT, `Status` INTEGER, PRIMARY KEY(`primeId`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `PrimeTopics` (`PrimeId` INTEGER NOT NULL, `PrimeTopics` TEXT, `Status` INTEGER, PRIMARY KEY(`PrimeId`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `PrimeNewsOffline` (`news_Id` TEXT NOT NULL, `image_Type` TEXT, `title` TEXT, `photo` TEXT, `published_Date` TEXT, `author` TEXT, `content` TEXT, `description` TEXT, `link` TEXT, `tag` TEXT, `section` INTEGER NOT NULL, `offline_photo` BLOB, PRIMARY KEY(`news_Id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `BookmarksNew` (`newsId` INTEGER NOT NULL, `id` INTEGER NOT NULL, `title` TEXT, `mainImage` TEXT, `type` TEXT, `date` TEXT, `content1` TEXT, `image1` TEXT, `content2` TEXT, `author` TEXT, `custom` TEXT, `bannerSectionId` TEXT, `sectionName` TEXT, PRIMARY KEY(`newsId`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"b04d8d328209c92a309498976dea1793\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `News`");
        _db.execSQL("DROP TABLE IF EXISTS `CustomNews`");
        _db.execSQL("DROP TABLE IF EXISTS `Sections`");
        _db.execSQL("DROP TABLE IF EXISTS `CustomSections`");
        _db.execSQL("DROP TABLE IF EXISTS `Notifs`");
        _db.execSQL("DROP TABLE IF EXISTS `PrimeBookmark`");
        _db.execSQL("DROP TABLE IF EXISTS `PrimeLike`");
        _db.execSQL("DROP TABLE IF EXISTS `PrimeAccountInformation`");
        _db.execSQL("DROP TABLE IF EXISTS `PrimeSections`");
        _db.execSQL("DROP TABLE IF EXISTS `PrimeTopics`");
        _db.execSQL("DROP TABLE IF EXISTS `PrimeNewsOffline`");
        _db.execSQL("DROP TABLE IF EXISTS `BookmarksNew`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsNews = new HashMap<String, TableInfo.Column>(8);
        _columnsNews.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsNews.put("type", new TableInfo.Column("type", "TEXT", false, 0));
        _columnsNews.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsNews.put("image", new TableInfo.Column("image", "TEXT", false, 0));
        _columnsNews.put("pubdate", new TableInfo.Column("pubdate", "TEXT", false, 0));
        _columnsNews.put("link", new TableInfo.Column("link", "TEXT", false, 0));
        _columnsNews.put("label", new TableInfo.Column("label", "TEXT", false, 0));
        _columnsNews.put("section", new TableInfo.Column("section", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysNews = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesNews = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoNews = new TableInfo("News", _columnsNews, _foreignKeysNews, _indicesNews);
        final TableInfo _existingNews = TableInfo.read(_db, "News");
        if (! _infoNews.equals(_existingNews)) {
          throw new IllegalStateException("Migration didn't properly handle News(com.knx.inquirer.models.NewsModel).\n"
                  + " Expected:\n" + _infoNews + "\n"
                  + " Found:\n" + _existingNews);
        }
        final HashMap<String, TableInfo.Column> _columnsCustomNews = new HashMap<String, TableInfo.Column>(8);
        _columnsCustomNews.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsCustomNews.put("type", new TableInfo.Column("type", "TEXT", false, 0));
        _columnsCustomNews.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsCustomNews.put("image", new TableInfo.Column("image", "TEXT", false, 0));
        _columnsCustomNews.put("pubdate", new TableInfo.Column("pubdate", "TEXT", false, 0));
        _columnsCustomNews.put("link", new TableInfo.Column("link", "TEXT", false, 0));
        _columnsCustomNews.put("label", new TableInfo.Column("label", "TEXT", false, 0));
        _columnsCustomNews.put("section", new TableInfo.Column("section", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCustomNews = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesCustomNews = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoCustomNews = new TableInfo("CustomNews", _columnsCustomNews, _foreignKeysCustomNews, _indicesCustomNews);
        final TableInfo _existingCustomNews = TableInfo.read(_db, "CustomNews");
        if (! _infoCustomNews.equals(_existingCustomNews)) {
          throw new IllegalStateException("Migration didn't properly handle CustomNews(com.knx.inquirer.models.Feed).\n"
                  + " Expected:\n" + _infoCustomNews + "\n"
                  + " Found:\n" + _existingCustomNews);
        }
        final HashMap<String, TableInfo.Column> _columnsSections = new HashMap<String, TableInfo.Column>(5);
        _columnsSections.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsSections.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsSections.put("url", new TableInfo.Column("url", "TEXT", false, 0));
        _columnsSections.put("color", new TableInfo.Column("color", "TEXT", false, 0));
        _columnsSections.put("isSelected", new TableInfo.Column("isSelected", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysSections = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesSections = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoSections = new TableInfo("Sections", _columnsSections, _foreignKeysSections, _indicesSections);
        final TableInfo _existingSections = TableInfo.read(_db, "Sections");
        if (! _infoSections.equals(_existingSections)) {
          throw new IllegalStateException("Migration didn't properly handle Sections(com.knx.inquirer.models.Section).\n"
                  + " Expected:\n" + _infoSections + "\n"
                  + " Found:\n" + _existingSections);
        }
        final HashMap<String, TableInfo.Column> _columnsCustomSections = new HashMap<String, TableInfo.Column>(4);
        _columnsCustomSections.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsCustomSections.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsCustomSections.put("url", new TableInfo.Column("url", "TEXT", false, 0));
        _columnsCustomSections.put("isSelected", new TableInfo.Column("isSelected", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCustomSections = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesCustomSections = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoCustomSections = new TableInfo("CustomSections", _columnsCustomSections, _foreignKeysCustomSections, _indicesCustomSections);
        final TableInfo _existingCustomSections = TableInfo.read(_db, "CustomSections");
        if (! _infoCustomSections.equals(_existingCustomSections)) {
          throw new IllegalStateException("Migration didn't properly handle CustomSections(com.knx.inquirer.models.Option).\n"
                  + " Expected:\n" + _infoCustomSections + "\n"
                  + " Found:\n" + _existingCustomSections);
        }
        final HashMap<String, TableInfo.Column> _columnsNotifs = new HashMap<String, TableInfo.Column>(8);
        _columnsNotifs.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsNotifs.put("news_id", new TableInfo.Column("news_id", "TEXT", false, 0));
        _columnsNotifs.put("image", new TableInfo.Column("image", "TEXT", false, 0));
        _columnsNotifs.put("link", new TableInfo.Column("link", "TEXT", false, 0));
        _columnsNotifs.put("message", new TableInfo.Column("message", "TEXT", false, 0));
        _columnsNotifs.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsNotifs.put("timestamp", new TableInfo.Column("timestamp", "INTEGER", false, 0));
        _columnsNotifs.put("readstatus", new TableInfo.Column("readstatus", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysNotifs = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesNotifs = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoNotifs = new TableInfo("Notifs", _columnsNotifs, _foreignKeysNotifs, _indicesNotifs);
        final TableInfo _existingNotifs = TableInfo.read(_db, "Notifs");
        if (! _infoNotifs.equals(_existingNotifs)) {
          throw new IllegalStateException("Migration didn't properly handle Notifs(com.knx.inquirer.models.NotificationModel).\n"
                  + " Expected:\n" + _infoNotifs + "\n"
                  + " Found:\n" + _existingNotifs);
        }
        final HashMap<String, TableInfo.Column> _columnsPrimeBookmark = new HashMap<String, TableInfo.Column>(5);
        _columnsPrimeBookmark.put("newsId", new TableInfo.Column("newsId", "INTEGER", true, 1));
        _columnsPrimeBookmark.put("image", new TableInfo.Column("image", "TEXT", false, 0));
        _columnsPrimeBookmark.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsPrimeBookmark.put("date", new TableInfo.Column("date", "TEXT", false, 0));
        _columnsPrimeBookmark.put("author", new TableInfo.Column("author", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysPrimeBookmark = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesPrimeBookmark = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoPrimeBookmark = new TableInfo("PrimeBookmark", _columnsPrimeBookmark, _foreignKeysPrimeBookmark, _indicesPrimeBookmark);
        final TableInfo _existingPrimeBookmark = TableInfo.read(_db, "PrimeBookmark");
        if (! _infoPrimeBookmark.equals(_existingPrimeBookmark)) {
          throw new IllegalStateException("Migration didn't properly handle PrimeBookmark(com.knx.inquirer.inqprime.model.PrimeBookmarkModel).\n"
                  + " Expected:\n" + _infoPrimeBookmark + "\n"
                  + " Found:\n" + _existingPrimeBookmark);
        }
        final HashMap<String, TableInfo.Column> _columnsPrimeLike = new HashMap<String, TableInfo.Column>(1);
        _columnsPrimeLike.put("newsId", new TableInfo.Column("newsId", "INTEGER", true, 1));
        final HashSet<TableInfo.ForeignKey> _foreignKeysPrimeLike = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesPrimeLike = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoPrimeLike = new TableInfo("PrimeLike", _columnsPrimeLike, _foreignKeysPrimeLike, _indicesPrimeLike);
        final TableInfo _existingPrimeLike = TableInfo.read(_db, "PrimeLike");
        if (! _infoPrimeLike.equals(_existingPrimeLike)) {
          throw new IllegalStateException("Migration didn't properly handle PrimeLike(com.knx.inquirer.inqprime.model.PrimeLikeModel).\n"
                  + " Expected:\n" + _infoPrimeLike + "\n"
                  + " Found:\n" + _existingPrimeLike);
        }
        final HashMap<String, TableInfo.Column> _columnsPrimeAccountInformation = new HashMap<String, TableInfo.Column>(12);
        _columnsPrimeAccountInformation.put("userId", new TableInfo.Column("userId", "INTEGER", true, 1));
        _columnsPrimeAccountInformation.put("accountEmail", new TableInfo.Column("accountEmail", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("accountPassword", new TableInfo.Column("accountPassword", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("userName", new TableInfo.Column("userName", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("accountName", new TableInfo.Column("accountName", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("mobileNo", new TableInfo.Column("mobileNo", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("gender", new TableInfo.Column("gender", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("address", new TableInfo.Column("address", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("birthday", new TableInfo.Column("birthday", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("avatar", new TableInfo.Column("avatar", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("cardNo", new TableInfo.Column("cardNo", "TEXT", false, 0));
        _columnsPrimeAccountInformation.put("plan", new TableInfo.Column("plan", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysPrimeAccountInformation = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesPrimeAccountInformation = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoPrimeAccountInformation = new TableInfo("PrimeAccountInformation", _columnsPrimeAccountInformation, _foreignKeysPrimeAccountInformation, _indicesPrimeAccountInformation);
        final TableInfo _existingPrimeAccountInformation = TableInfo.read(_db, "PrimeAccountInformation");
        if (! _infoPrimeAccountInformation.equals(_existingPrimeAccountInformation)) {
          throw new IllegalStateException("Migration didn't properly handle PrimeAccountInformation(com.knx.inquirer.inqprime.model.PrimeAccountInformationModel).\n"
                  + " Expected:\n" + _infoPrimeAccountInformation + "\n"
                  + " Found:\n" + _existingPrimeAccountInformation);
        }
        final HashMap<String, TableInfo.Column> _columnsPrimeSections = new HashMap<String, TableInfo.Column>(3);
        _columnsPrimeSections.put("primeId", new TableInfo.Column("primeId", "INTEGER", true, 1));
        _columnsPrimeSections.put("PrimeSections", new TableInfo.Column("PrimeSections", "TEXT", false, 0));
        _columnsPrimeSections.put("Status", new TableInfo.Column("Status", "INTEGER", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysPrimeSections = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesPrimeSections = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoPrimeSections = new TableInfo("PrimeSections", _columnsPrimeSections, _foreignKeysPrimeSections, _indicesPrimeSections);
        final TableInfo _existingPrimeSections = TableInfo.read(_db, "PrimeSections");
        if (! _infoPrimeSections.equals(_existingPrimeSections)) {
          throw new IllegalStateException("Migration didn't properly handle PrimeSections(com.knx.inquirer.inqprime.model.PrimeSectionsModel).\n"
                  + " Expected:\n" + _infoPrimeSections + "\n"
                  + " Found:\n" + _existingPrimeSections);
        }
        final HashMap<String, TableInfo.Column> _columnsPrimeTopics = new HashMap<String, TableInfo.Column>(3);
        _columnsPrimeTopics.put("PrimeId", new TableInfo.Column("PrimeId", "INTEGER", true, 1));
        _columnsPrimeTopics.put("PrimeTopics", new TableInfo.Column("PrimeTopics", "TEXT", false, 0));
        _columnsPrimeTopics.put("Status", new TableInfo.Column("Status", "INTEGER", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysPrimeTopics = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesPrimeTopics = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoPrimeTopics = new TableInfo("PrimeTopics", _columnsPrimeTopics, _foreignKeysPrimeTopics, _indicesPrimeTopics);
        final TableInfo _existingPrimeTopics = TableInfo.read(_db, "PrimeTopics");
        if (! _infoPrimeTopics.equals(_existingPrimeTopics)) {
          throw new IllegalStateException("Migration didn't properly handle PrimeTopics(com.knx.inquirer.inqprime.model.PrimeTopicsModel).\n"
                  + " Expected:\n" + _infoPrimeTopics + "\n"
                  + " Found:\n" + _existingPrimeTopics);
        }
        final HashMap<String, TableInfo.Column> _columnsPrimeNewsOffline = new HashMap<String, TableInfo.Column>(12);
        _columnsPrimeNewsOffline.put("news_Id", new TableInfo.Column("news_Id", "TEXT", true, 1));
        _columnsPrimeNewsOffline.put("image_Type", new TableInfo.Column("image_Type", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("photo", new TableInfo.Column("photo", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("published_Date", new TableInfo.Column("published_Date", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("author", new TableInfo.Column("author", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("content", new TableInfo.Column("content", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("description", new TableInfo.Column("description", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("link", new TableInfo.Column("link", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("tag", new TableInfo.Column("tag", "TEXT", false, 0));
        _columnsPrimeNewsOffline.put("section", new TableInfo.Column("section", "INTEGER", true, 0));
        _columnsPrimeNewsOffline.put("offline_photo", new TableInfo.Column("offline_photo", "BLOB", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysPrimeNewsOffline = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesPrimeNewsOffline = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoPrimeNewsOffline = new TableInfo("PrimeNewsOffline", _columnsPrimeNewsOffline, _foreignKeysPrimeNewsOffline, _indicesPrimeNewsOffline);
        final TableInfo _existingPrimeNewsOffline = TableInfo.read(_db, "PrimeNewsOffline");
        if (! _infoPrimeNewsOffline.equals(_existingPrimeNewsOffline)) {
          throw new IllegalStateException("Migration didn't properly handle PrimeNewsOffline(com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel).\n"
                  + " Expected:\n" + _infoPrimeNewsOffline + "\n"
                  + " Found:\n" + _existingPrimeNewsOffline);
        }
        final HashMap<String, TableInfo.Column> _columnsBookmarksNew = new HashMap<String, TableInfo.Column>(13);
        _columnsBookmarksNew.put("newsId", new TableInfo.Column("newsId", "INTEGER", true, 1));
        _columnsBookmarksNew.put("id", new TableInfo.Column("id", "INTEGER", true, 0));
        _columnsBookmarksNew.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsBookmarksNew.put("mainImage", new TableInfo.Column("mainImage", "TEXT", false, 0));
        _columnsBookmarksNew.put("type", new TableInfo.Column("type", "TEXT", false, 0));
        _columnsBookmarksNew.put("date", new TableInfo.Column("date", "TEXT", false, 0));
        _columnsBookmarksNew.put("content1", new TableInfo.Column("content1", "TEXT", false, 0));
        _columnsBookmarksNew.put("image1", new TableInfo.Column("image1", "TEXT", false, 0));
        _columnsBookmarksNew.put("content2", new TableInfo.Column("content2", "TEXT", false, 0));
        _columnsBookmarksNew.put("author", new TableInfo.Column("author", "TEXT", false, 0));
        _columnsBookmarksNew.put("custom", new TableInfo.Column("custom", "TEXT", false, 0));
        _columnsBookmarksNew.put("bannerSectionId", new TableInfo.Column("bannerSectionId", "TEXT", false, 0));
        _columnsBookmarksNew.put("sectionName", new TableInfo.Column("sectionName", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysBookmarksNew = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesBookmarksNew = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoBookmarksNew = new TableInfo("BookmarksNew", _columnsBookmarksNew, _foreignKeysBookmarksNew, _indicesBookmarksNew);
        final TableInfo _existingBookmarksNew = TableInfo.read(_db, "BookmarksNew");
        if (! _infoBookmarksNew.equals(_existingBookmarksNew)) {
          throw new IllegalStateException("Migration didn't properly handle BookmarksNew(com.knx.inquirer.models.BookmarksNewModel).\n"
                  + " Expected:\n" + _infoBookmarksNew + "\n"
                  + " Found:\n" + _existingBookmarksNew);
        }
      }
    }, "b04d8d328209c92a309498976dea1793", "a2cde236b257d309ecdb2adac3c35c00");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "News","CustomNews","Sections","CustomSections","Notifs","PrimeBookmark","PrimeLike","PrimeAccountInformation","PrimeSections","PrimeTopics","PrimeNewsOffline","BookmarksNew");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `News`");
      _db.execSQL("DELETE FROM `CustomNews`");
      _db.execSQL("DELETE FROM `Sections`");
      _db.execSQL("DELETE FROM `CustomSections`");
      _db.execSQL("DELETE FROM `Notifs`");
      _db.execSQL("DELETE FROM `PrimeBookmark`");
      _db.execSQL("DELETE FROM `PrimeLike`");
      _db.execSQL("DELETE FROM `PrimeAccountInformation`");
      _db.execSQL("DELETE FROM `PrimeSections`");
      _db.execSQL("DELETE FROM `PrimeTopics`");
      _db.execSQL("DELETE FROM `PrimeNewsOffline`");
      _db.execSQL("DELETE FROM `BookmarksNew`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public NewsDao getNewsDao() {
    if (_newsDao != null) {
      return _newsDao;
    } else {
      synchronized(this) {
        if(_newsDao == null) {
          _newsDao = new NewsDao_Impl(this);
        }
        return _newsDao;
      }
    }
  }
}
