package com.knx.inquirer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.knx.inquirer.models.Article;
import com.knx.inquirer.models.PostArticleSearch;
import com.knx.inquirer.models.SearchModel;
import com.knx.inquirer.rvadapters.RVAdapterEmpty_Search;
import com.knx.inquirer.rvadapters.RVAdapterSearchedNews;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SearchActivity extends AppCompatActivity {

	private ImageView imageBack;
	private EditText editSearch;
	private RecyclerView recyclerView;
	private LinearLayoutManager layoutManager;
	private RecyclerView.Adapter adapter;
    private View mListTouchInterceptor;
	private RVAdapterSearchedNews adapter_search;

	private SharedPrefs sf;

	private ApiService apiService;
	private ArrayList<SearchModel> searchmodel = new ArrayList<>();
	private CompositeDisposable compositeDisposable = new CompositeDisposable();

	private Global.LoaderDialog loaderDialog;
	private Global.ViewDialog alertErrorArticle;
	private String TAG = SearchActivity.class.getName();

	@Subscribe(threadMode = ThreadMode.POSTING)
	public void onItemPostEvent(PostArticleSearch item) {
		Log.d(TAG, "onItemPostEvent: Called***");
		viewArticle(String.valueOf(item.getItempos()));
	}

//	@Override
//	protected void attachBaseContext(Context newBase) {
//		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//	}

	//Calligraphy3 ADDED for Tablet Android10
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_list_view);

		sf = new SharedPrefs(this);

		Global.isItemClicked = false;

		EventBus.getDefault().register(this);

		try{
			if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}else{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
		}catch(Exception e){
			Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
		}

		apiService = ServiceGenerator.createService(ApiService.class);

		loaderDialog = new Global.LoaderDialog();
		alertErrorArticle = new Global.ViewDialog();

		recyclerView = findViewById(R.id.recyclerviewsearch);
		recyclerView.setHasFixedSize(true);
		layoutManager = new LinearLayoutManager(this);
		recyclerView.setLayoutManager(layoutManager);

        mListTouchInterceptor = findViewById(R.id.mListTouchInterceptor);

		editSearch = findViewById(R.id.editSearchParam);

		editSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (editSearch.getText().toString().equals("")) {
                	toastMessage("Please provide a keyword to search");
                } else {
                    searchmodel.clear();
                    recyclerView.setAdapter(null);
                    searchNews( editSearch.getText().toString());
                    HideKeyboard();
                }

            }
            return true;
        });


		imageBack = findViewById(R.id.imgClose);
		imageBack.setOnClickListener(view -> onBackPressed());

		showKeyboardWithFocus(editSearch, this);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==Global.NEWS_ARTICLE){
			Log.d(TAG, "onActivityResult:22 CALLED");
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		Global.isItemClicked = false;
	}

	public void showLoader() {
		loaderDialog.showDialog(this);
	}

	public void hideLoader() {
		loaderDialog.dismissDialog(this);
	}

	public void enableNewsInterceptor(){
		mListTouchInterceptor.setClickable(true);
	}

	public void disableNewsInterceptor(){
		mListTouchInterceptor.setClickable(false);
	}

	public void onArticleRequest() {
//		Global.isItemClicked = true;
		showLoader();
	}

	public void onArticleRequestSuccess() {
//		Global.isItemClicked = false;
		disableNewsInterceptor();
		hideLoader();
	}

	public void onArticleRequestError() {
		Global.isItemClicked = false;
		hideLoader();
	}
    private void searchNews(String keyword){
		showLoader();
		long unixTime = System.currentTimeMillis() / 1000L;
		String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megasearch/" + keyword);
		Log.i("SEARCH_HASHED", hashed);

		apiService.getSearchedNews(String.valueOf(unixTime), sf.getDeviceId(), keyword, hashed)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new SingleObserver<ArrayList<SearchModel>>() {
					@Override
					public void onSubscribe(Disposable d) {
						compositeDisposable.add(d);
					}

					@Override
					public void onSuccess(ArrayList<SearchModel> searchModels) {
						hideLoader();
						if(searchModels.size() > 0) {
							adapter = new RVAdapterSearchedNews( getApplicationContext(), searchModels, sf);
							recyclerView.setAdapter(adapter);
							adapter.notifyDataSetChanged();
						}else{
							adapter = new RVAdapterEmpty_Search();
							recyclerView.setAdapter(adapter);
						}
					}

					@Override
					public void onError(Throwable e) {
						getDailyKey(); //
						hideLoader();
						String error_msg = Global.getResponseCode(e);
						//For Developer Debugging check email and name and mobile
						if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
							toastMessage(TAG+": onError: "+ e.getMessage());
						}else{
							toastMessage(error_msg);
						}
					}
				});
	}

//	private void viewArticle(String id) {
//
//		onArticleRequest();
//		long unixTime = System.currentTimeMillis() / 1000L;
//		String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megaarticle/" + id);
//		apiService.getArticle(String.valueOf(unixTime), sf.getDeviceId(), id, hashed)
//				.subscribeOn(Schedulers.io())
//				.observeOn(AndroidSchedulers.mainThread())
//				.subscribe(new SingleObserver<Article>() {
//					@Override
//					public void onSubscribe(Disposable d) {
//						compositeDisposable.add(d);
//					}
//
//					@Override
//					public void onSuccess(Article article) {
//
//						Global.isItemClicked = true;
//						onArticleRequestSuccess();
//						String sectionId = article.getSectionId();
//						String title = article.getTitle();
//						String photo = article.getCreative();
//						String type = article.getType();
//						String pubdate = article.getPubdate();
////						String content1 = article.getContent() != null ? article.getContent().get(0).toString() : "";
////						String image1 = article.getInsert() != null ? article.getInsert().getCreative() : "";
//						String image1 = "";
//						String content1 = article.getContent1().isEmpty() ? article.getContent2a() : article.getContent2b();
//						String content2 = article.getContent2b();
//
////						String content2 = article.getContent() != null ? article.getContent().get(1).toString() : "";
//						String author = article.getByline();
//						String custom = article.getCustom();
//
//						//Get the Related Article
//						if(article.getRelated()!=null){
//							Global.relatedArticles.clear();
//							Global.relatedArticles.addAll(article.getRelated());
//						}
//
//						//Get the SuperShare Details
//						//androidx
//						Intent intent = new Intent();
//						intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
//						intent.putExtra("id", id);
//						intent.putExtra("sectionId", sectionId);
//						intent.putExtra("title", title);
//						intent.putExtra("photo", photo);
//						intent.putExtra("type", type);
//						intent.putExtra("pubdate", pubdate);
//						intent.putExtra("content1", content1);
//						intent.putExtra("image1", image1);
//						intent.putExtra("content2", content2);
//						intent.putExtra("author", author);
//						intent.putExtra("custom", custom);
//						intent.putExtra("adbanner", "");
//						startActivityForResult(intent, Global.NEWS_ARTICLE);
//					}
//
//					@Override
//					public void onError(Throwable e) {
//						hideLoader();
//						disableNewsInterceptor();
//						onArticleRequestError();
//
//						String error_msg = Global.getResponseCode(e);
//						Toast.makeText(SearchActivity.this, error_msg, Toast.LENGTH_SHORT).show();
//					}
//				});
//	}

	private void viewArticle(String id) {
		onArticleRequest();
		long unixTime = System.currentTimeMillis() / 1000L;
		String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
		apiService.getArticle2(String.valueOf(unixTime),sf.getDeviceId(), id, hashed)
				.enqueue(new Callback<JsonObject>() {
					@Override
					public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
						try{
							Global.isItemClicked = true;
							onArticleRequestSuccess();
							String content1="";
							String content2="";
							String insertNewId="";
							String insertType="";
							String insertTitle="";
							String insertImage="";
							String insertContent="";
							//Sometimes NO Section_ID and DATE: Occur on ADS Type1
							String section_id="", pubdate="", author="", title="", photo="", type="";
							if(response.body().has("section_id")) section_id = !response.body().get("section_id").isJsonNull() ? response.body().get("section_id").getAsString() : "";
							if(response.body().has("pubdate")) pubdate = !response.body().get("pubdate").isJsonNull() ? response.body().get("pubdate").getAsString() : "";
							if(response.body().has("byline")) author = !response.body().get("byline").isJsonNull() ? response.body().get("byline").getAsString() : "";
							if(response.body().has("title")) title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
							if(response.body().has("creative")) photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
							if(response.body().has("type")) type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";

							//Get the SuperShare: Check if has an INSERT (for SuperShare)
							if(response.body().has("insert")){
								//Check if CONTENT as an ARRAY
//								content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
//								content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
//								insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
//								insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
//								insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
//								insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
//								insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
//								Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
								if(response.body().has("content")){
									content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
									content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
								}
								if(response.body().get("insert").getAsJsonObject().has("id")) insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
								if(response.body().get("insert").getAsJsonObject().has("type"))  insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
								if(response.body().get("insert").getAsJsonObject().has("title")) insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
								if(response.body().get("insert").getAsJsonObject().has("creative")) insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
								if(response.body().get("insert").getAsJsonObject().has("content"))  insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
								Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
							}else {
								//Check if CONTENT as a STRING
								if(response.body().has("content")) content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
							}
							//Get the Related Article: First Check if has RelatedArticle
							Global.relatedArticles.clear();
							if(response.body().has("related")){
								Log.d(TAG, "onResponse: CONTENT_  test1");
								for(int x=0; x<response.body().get("related").getAsJsonArray().size();x++){
									Global.relatedArticles.add(response.body().get("related").getAsJsonArray().get(x).getAsJsonObject());
								}
							}
							//INTENT
							Global.isItemClicked = true;
							Intent intent = new Intent();
							intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
							intent.putExtra("id", id);
							intent.putExtra("sectionId", section_id);
							intent.putExtra("title", title);
							intent.putExtra("photo", photo);
							intent.putExtra("type", type);
							intent.putExtra("pubdate", pubdate);
							intent.putExtra("content1", content1);
							intent.putExtra("image1", insertImage);
							intent.putExtra("insertTitle", insertTitle);
							intent.putExtra("insertType", insertType);
							intent.putExtra("insertNewsId", insertNewId);
							intent.putExtra("insertContent", insertContent);
							intent.putExtra("content2", content2);
							intent.putExtra("author", author);
							intent.putExtra("custom", ""); //No USED
							intent.putExtra("adbanner", "");
							startActivityForResult(intent, Global.NEWS_ARTICLE);

						}catch (Exception e){
							Log.d(TAG, "onResponse: viewArticle Exception: "+ e.getCause());
						}
					}

					@Override
					public void onFailure(Call<JsonObject> call, Throwable t) {
						getDailyKey();
						onArticleRequestError();
						String error_msg = Global.getResponseCode(t);
						//For Developer Debugging check email and name and mobile
						if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
							toastMessage(TAG+": onFailure: "+ t.getMessage());
						}else{
							toastMessage(error_msg);
						}
					}
				});
	}


	private void getDailyKey(){
		apiService.getDailyKey2().enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if(response.isSuccessful()){
					sf.setDailyKey(response.body());
				}
			}
			@Override
			public void onFailure(Call<String> call, Throwable t) {
				Log.d(TAG, "onFailure: ");
			}
		});
	}

	public void toastMessage(String message) {
		if(Global.toast!=null) Global.toast.cancel(); //Remove First the existing toast
		Global.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT); Global.toast.show(); //Show NEW TOAST
	}


	private void showKeyboardWithFocus(View v, Activity a) {
		try {
			v.requestFocus();
			InputMethodManager imm = (InputMethodManager) a.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
			a.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void HideKeyboard() {
		// Check if no view has focus:
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}


	@Override
	protected void onDestroy() {
		if (!compositeDisposable.isDisposed()) {
			compositeDisposable.dispose();
		}
		super.onDestroy();
	}

	public void checkVideoLink (String link) {

		if (link != null) {
			try {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.addCategory(Intent.CATEGORY_BROWSABLE);
				intent.setData(Uri.parse(link));
				startActivity(intent);
			}catch(Exception e){
				Log.d("TAGGG", "Video3Link "+ e.getMessage());
				return;
			}

		}else{
			return;
		}
	}


	public void checkImageLink(String link){

		if (link != null) {
			try {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.addCategory(Intent.CATEGORY_BROWSABLE);
				intent.setData(Uri.parse(link));
				startActivity(intent);
			}catch(Exception e){
				Log.d("TAGGG", "ImageDoubleLink "+ e.getMessage());
				return;
			}
		}else{
			return;
		}
	}


	public void displayFullview (String video) {

		try {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//DO NOT FORGET THIS EVER
			intent.setDataAndType(Uri.parse(video), "video/mp4");
			startActivity(intent);
		}catch(Exception e){
			Log.d("TAGGG", "Video6Link "+ e.getMessage());
			return;
		}

	}
}
