package com.knx.inquirer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class Tutorial2Activity extends AppCompatActivity {
	private static final String TAG = "Tutorial2Activity";
	
	private ImageView imageView;
    
	@SuppressLint("NewApi")
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.tutorial1_view);

		try{
			if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}else{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
		}catch(Exception e){
			Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
		}
	
	    imageView = (ImageView)findViewById(R.id.imageView);
	    
	    imageView.setOnTouchListener(new View.OnTouchListener() {  
            public boolean onTouch(View v, MotionEvent event) {
      
            if(event.getAction() == MotionEvent.ACTION_DOWN){
	            Intent intent = new Intent();
                intent.setClassName("com.knx.inquirer", "com.knx.inquirer.QrActivity");
                startActivity(intent);
                finish();
              }  
              return true;
            }
        });
	}

}
