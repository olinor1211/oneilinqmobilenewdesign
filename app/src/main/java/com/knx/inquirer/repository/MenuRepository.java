package com.knx.inquirer.repository;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.Featured;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.models.MenuResponse;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuRepository {
    private static final String TAG = "MenuRepository";

    private ApiService apiService;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData<Boolean> menuRepoError;
    private MutableLiveData<Throwable> throwableMutableLiveData;
    private DatabaseHelper helper;
    //androidx implementation
    private MutableLiveData<JsonObject> menuResponse3;
    private MutableLiveData<MenuResponse> menuResponse;

    private String dKey;

    //for mobility
    private ArrayList<Section> sectionmodel = new ArrayList<>();
    private ArrayList<Section> sectionList = new ArrayList<>();



    public MenuRepository(Context context, CompositeDisposable disposable, SharedPrefs sf) {
        this.compositeDisposable = disposable;
        this.sf = sf;
        helper = new DatabaseHelper(context);
    }

    //GetMenu for androidx
    //ListOfUser: MyInqEDIT, MyInq, Breaking
    public MutableLiveData<JsonObject> getMenu(){
//
        menuRepoError = new MutableLiveData<>();
        menuResponse3 = new MutableLiveData<>();
        throwableMutableLiveData = new MutableLiveData<>();
        apiService = ServiceGenerator.createService(ApiService.class);

        String dailyKey = getDailyKey();
        long unixTime = System.currentTimeMillis() / 1000L;

        Log.d(TAG, "getNews: getMenu DAILY_KEY: "+ sf.getDailyKey());
        Log.d(TAG, "getNews: getMenu DEVICE_ID: "+ sf.getDeviceId());
        Log.d(TAG, "getNews: getMenu TOKEN: "+ sf.getFirebaseToken());
        Log.d(TAG, "getNews: getMenu dailyKey: "+ dailyKey);

//      OLD_BASE_URL but stillWorking:  String hashed = Global.Generate32SHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megamenu");
//        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megamenu");
        String hashed = Global.Generate32SHA512(dailyKey + unixTime +"inq" + sf.getDeviceId() + "-megamenu");

        Log.d(TAG, "getMenu: getMenu MD5: "+ hashed);
        apiService.getMenu3(String.valueOf(unixTime),sf.getDeviceId(), sf.getFirebaseToken(), hashed)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if(response.isSuccessful()){

                            //DEBUGGING
//                            Log.d(TAG, "onResponse: "+response.body());
//                            Log.d(TAG, "onResponse Android: "+response.body().get("menu").getAsJsonObject().get("Android"));
//                            Log.d(TAG, "onResponseSections: "+response.body().get("menu").getAsJsonObject().get("sections").getAsJsonArray());
//                            Log.d(TAG, "onResponseSectionsOBJECT: "+response.body().get("menu").getAsJsonObject().get("sections").getAsJsonObject());
//                            String sections = gson.toJson(response.body().get("menu").getAsJsonObject().get("sections").getAsJsonArray()); //Convert to JSON STRING First
                            Gson gson = new Gson();
                            String sections = gson.toJson(response.body().get("menu").getAsJsonObject().get("sections").getAsJsonArray());
                            try {
                                JSONArray jsonArray =  new JSONArray(sections);
                                Log.d(TAG, "onResponse:sectionList  2 "+ jsonArray.getJSONObject(0).getString("title"));
                                Log.d(TAG, "onResponse:sectionList  3 "+ jsonArray.length());
                                Global.sectionList.clear();
                                sectionList.clear();
                                    for(int x=0; x<jsonArray.length(); x++){
                                        sectionList.add(new Section(
                                                jsonArray.getJSONObject(x).getInt("id"),
                                                jsonArray.getJSONObject(x).getString("title"),
                                                "",
                                                jsonArray.getJSONObject(x).getString("color"),
                                                false));
                                    }
                            } catch (JSONException e) {
                                Log.d(TAG, "onResponse: JSONException "+e.getMessage());
                            }
                            Global.sectionList.addAll(sectionList); //Adding all sections in the GLobal Note: must be add First into local arrayList before Adding to Global
                            Log.d(TAG, "onResponse:sectionList "+  Global.sectionList.size());

                            menuRepoError.setValue(false); //menuRepoError.postValue(false);  //
                            menuResponse3.setValue(response.body()); //menuResponse.postValue(menu);  //
                        }else{
                            //ERROR_BODY
                            menuRepoError.setValue(true);
                            Global.getMenu3ErrorBody = response.errorBody().toString();  //
                            Log.d(TAG, "onError: MenuRepo errorBody "+response.errorBody());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.d(TAG, "onFailure: MenuRepo : "+t.getMessage());
                        menuRepoError.setValue(true);
                        throwableMutableLiveData.setValue(t);
                    }
                });
        return menuResponse3;
    }

    //OFFLINE Section_Tags
    public LiveData<List<Section>> getCachedSections(){
        return helper.getSections();
    }

    public LiveData<Boolean> getMenuError(){
        return menuRepoError;
    }

    public LiveData<Integer> getSectionRecordCount(){
        return helper.getSectionRecordCount();
    }
    public LiveData<Throwable> getMenuThrowable(){
        return throwableMutableLiveData;
    }

    public LiveData<Integer> getNewRecordCount() {
        return helper.getNewsRecordCount();
    }

    private String getDailyKey(){
        dKey = sf.getDailyKey();
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    dKey = response.body();
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: getDailyKey: "+t.getMessage());
            }
        });
        return dKey;
    }
}

