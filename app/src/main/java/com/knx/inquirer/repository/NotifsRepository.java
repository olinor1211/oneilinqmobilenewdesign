package com.knx.inquirer.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.NotificationModel;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class NotifsRepository {

    private DatabaseHelper helper;
    private CompositeDisposable compositeDisposable;

    public NotifsRepository(Context context, CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
        helper = new DatabaseHelper(context);
    }
    //select only here
    public LiveData<List<NotificationModel>> getNotifications(){
      return helper.getNotifications();
    }
    //select only here
    public LiveData<Integer> getUnreadNotifsCount(){
        return helper.getUnreadNotifsCount();
    }

}
