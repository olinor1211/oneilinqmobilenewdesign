package com.knx.inquirer.repository;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.knx.inquirer.models.MenuAdResponse;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BannerAdRepository {
    private static final String TAG = "BannerAdRepository";

    private MutableLiveData<MenuAdResponse> mutableBannerAdData; //    private MutableLiveData<MenuAdResponse> mutableBannerAdData;
    private MutableLiveData<Boolean> isBannerAdRepoError;
    private MutableLiveData<Throwable> throwableBannerAdData;

    private CompositeDisposable compositeDisposable;
    private ApiService apiService;
    private SharedPrefs sf;
    private String hashed;

    public BannerAdRepository(Context context) {
        compositeDisposable = new CompositeDisposable();
        apiService = ServiceGenerator.createService(ApiService.class);
        sf = new SharedPrefs(context);
        hashed = Global.Generate32SHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megamenu");
        Log.d(TAG, "getBannerAdRepoConstructor: CALLED");
    }

    public MutableLiveData<MenuAdResponse> getBannerAd() {  //public MutableLiveData<MenuAdResponse> getBannerAd() {
        // ALL MutableLiveData MUST BE INITIALIZE
        isBannerAdRepoError = new MutableLiveData<>();
        mutableBannerAdData = new MutableLiveData<>();
        throwableBannerAdData = new MutableLiveData<>();

        Log.d(TAG, "getBannerAd2 CALLED_DAILY_KEY "+ sf.getDailyKey());
        Log.d(TAG, "getBannerAd2 CALLED_DEVICE_ID "+ sf.getDeviceId());
        long unixTime = System.currentTimeMillis() / 1000L;

        hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megamenu");
        apiService.getBannerAd(String.valueOf(unixTime),sf.getDeviceId(), sf.getFirebaseToken(), hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<MenuAdResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(MenuAdResponse menuAdResponse) {
                        //For Debugging
                        Gson gson = new Gson();
                        String res = gson.toJson(menuAdResponse);
                        Log.d(TAG, "onSuccess: getBannerAd2 CALLED: "+res);
                        isBannerAdRepoError.setValue(false); //Error false
                        mutableBannerAdData.setValue(menuAdResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: Throwable getBannerAd2 CALLED"+ e.getMessage());
                        isBannerAdRepoError.setValue(true);  //Error true
                        throwableBannerAdData.setValue(e);
                    }
                });

        return mutableBannerAdData;
    }

    public LiveData<Boolean> isBannerAdRepoError(){
        return isBannerAdRepoError;
    }

    public LiveData<Throwable> getBannerAdError(){
        return throwableBannerAdData;
    }

}


























//always partner with a Handler..WORKING but Cannot handle enough by runnable once go to activity
//    private Runnable convertAndSave = new Runnable() {
//        public void run() {
//            try {
//                //use newsPrimeList after saving all
//                for(int x = 0; x<newsPrimeList.size(); x++){
//                    //convert image
//                    byte[] imageByte = convertBitmapToByteArray(convertUrlToBitmap(newsPrimeList.get(x).getPhoto()));
//                    helper.updatePrimeNewsImage(imageByte,Integer.valueOf(newsPrimeList.get(x).getNews_Id()));
//                    Log.d(TAG, "run: called" + x + ".. "+ "" + " title.. " + newsPrimeList.get(x).getTitle());
//                    runnableHandler.postDelayed(convertAndSave, 200);
//                }
////                runnableHandler.removeCallbacks(convertAndSave); //stop runnable after forLoop
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    };



//WORKING
//    public MutableLiveData<List<PrimeNewsBySectionModel>> getPrimeNewsBySection(int section) {
////        String deviceKey = "4616724363963694"; //tester  //4616724363963694
////        String deviceId = "f8fcf876237111fb";  //tester
//          mutableNewsData = new MutableLiveData<>();
//
////        primeApiService = ServiceGenerator.createService(PrimeApiService.class);
////        hashed = Global.Generate32SHA512(deviceKey + "inq" + PrimeGlobal.DEVICE_ID + "-megasection/" + section);
////
////         primeApiService.getPrimeNewsBySection(PrimeGlobal.DEVICE_ID, section, hashed)
////
////                .subscribeOn(Schedulers.io())
////                .observeOn(AndroidSchedulers.mainThread())
////                .subscribe(new SingleObserver<ArrayList<PrimeNewsBySectionModel>>() {
////                    @Override
////                    public void onSubscribe(Disposable d) {
////                        Log.d(TAG, "onSubscribe: getPrimeNewsBySection called");
////                        compositeDisposable.add(d);
////                    }
//
//                    @Override
//                    public void onSuccess(ArrayList<PrimeNewsBySectionModel> news) {
//                        Log.d(TAG, "onSuccess:getPrimeNewsBySection called "+news.get(0).getNewsId());
//                        mutableNewsData.setValue(news);
//
////                        helper.deleteNews();
////                        helper.insertNews(news);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e("NEWS_API_ERR", e.toString());
//                    }
//                });
//        return mutableNewsData;
//    }

