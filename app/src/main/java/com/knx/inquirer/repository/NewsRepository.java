package com.knx.inquirer.repository;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsRepository {
    private static final String TAG = "NewsRepository";

    private ApiService apiService;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData<List<NewsModel>> mutableNewsData;
    private MutableLiveData<Boolean> newsRepoError;
    private MutableLiveData<Throwable> throwableMutableLiveData;
    private String hashed;
    private DatabaseHelper helper;
    private Context context;
    private String dKey;

    public NewsRepository(Context context, CompositeDisposable disposable, SharedPrefs sf) {
        this.compositeDisposable = disposable;
        this.sf = sf;
        helper = new DatabaseHelper(context);
        this.context = context;
    }

    //ListOfUser: Breaking:getMenuAndNews    Breaking:getNewsBySection
    public LiveData<List<NewsModel>> getNews(int lastSection, int pos){

        Log.d(TAG, "getNews: getNewsBySection SectionId_Param: "+ lastSection);
        Log.d(TAG, "getNews: getNewsBySection SectionIdRVPosition: "+ pos);

        mutableNewsData = new MutableLiveData<>();
        newsRepoError = new MutableLiveData<>();
        throwableMutableLiveData = new MutableLiveData<>();
        apiService = ServiceGenerator.createService(ApiService.class);

        String dailyKey = getDailyKey();
        long unixTime = System.currentTimeMillis() / 1000L;
        Log.d(TAG, "getNews: getNewsBySection dailyKey: "+ dailyKey);

        hashed = Global.Generate32SHA512(dailyKey + unixTime + "inq" + sf.getDeviceId() + "-megasection/" + lastSection);
//        hashed = Global.Generate32SHA512(sf.getDailyKey() +unixTime + "inq" + sf.getDeviceId() + "-megasection/" + lastSection);
        apiService.getNewsBySection(String.valueOf(unixTime),sf.getDeviceId(), lastSection, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<NewsModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe:getNewsBySection called");
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ArrayList<NewsModel> news) {
                        Log.d(TAG, "onSuccess: NewsRepository getNewsBySection called");
                        //for Debugging
//                        Gson gson = new Gson();
//                        String res = gson.toJson(news);
//                        Log.d(TAG, "onSuccess: NewsRepository getNewsBySection GSON_response: "+res);

//                        //TEST DATA
//                       String imageStr = "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618920580_Covid%2019-1.jpg," +
//                                "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618910647_000_1UQ6RX.jpeg," +
//                                "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618916490_Front-Page141452-620x354.jpg," +
//                                "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618912845_a33b4a6125f7b3a1d62d316723cef69c.jpg";
//                            //Carousel OK new:
//                         news.add(7,new NewsModel("", "4", "CARAOUSEL", imageStr, "September 09, 2019 02:53pm","", "", "sports"));//
//
//                        news.add(7,new NewsModel("", "10", "DailymotionLINK_TEST_BREAKING", "https://www.dailymotion.com/video/x7cyruo", "September 09, 2019 02:53pm","https://www.facebook.com", "", "sports"));//
//                        news.add(6,new NewsModel("", "110", "", "https://open.spotify.com/embed-podcast/episode/0OaQFPoHlFeDh5XAjtHH9K", "","", "", ""));//
//////                        news.add(10,new NewsModel("", "8", "Dailymotion", "https://www.dailymotion.com/video/x7cyruo", "September 09, 2019 02:53pm","https://combo.staticflickr.com/ap/build/images/refencing-announcement/bird2.jpg", "", "sports"));//
//////                        news.add(14,new NewsModel("", "8", "Dailymotion", "https://www.dailymotion.com/video/x7cyruo", "September 09, 2019 02:53pm","https://combo.staticflickr.com/ap/build/images/refencing-announcement/bird2.jpg", "", "sports"));//
//                        news.add(9,new NewsModel("", "111", "", "https://open.spotify.com/embed-podcast/episode/0ZvvRuhhlr4YRkRjhTANd9", "","", "", ""));//
//                        news.add(12,new NewsModel("", "112", "", "https://open.spotify.com/embed-podcast/episode/2sf5g6aHUtq1AHP6RPNSqf", "","", "", ""));//
//                        news.add(15,new NewsModel("", "113", "", "https://open.spotify.com/embed-podcast/episode/5RjS4UV9TFYITVj6EY5RiX", "","", "", ""));//
//                        news.add(18,new NewsModel("", "114", "", "https://open.spotify.com/embed-podcast/episode/7fiNcOsalODlOuaiEttUF6", "","", "", ""));//
////
                        mutableNewsData.setValue(news);
                        newsRepoError.setValue(false);

                        //Only save section if API has response
//                        sf.setLastSection(lastSection);

                        helper.deleteNews();
                        helper.insertNews(news);
                    }

                    @Override
                    public void onError(Throwable e) {
//                        Toast.makeText(context, "ERROR_NewsRepo: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Error: Throwable getNewsBySection STRING: "+e.toString());
                        Log.d(TAG, "Error: Throwable getNewsBySection MESSAGE:  "+e.getMessage());
                        newsRepoError.setValue(true);  //if TRUE..will Display OFFLINE NEWS //can be replace with GLOBAL
                        throwableMutableLiveData.setValue(e);
                        //Save previously selected section when error is encountered
                        sf.setLastSectionPosition(pos);
                    }
                });

            return mutableNewsData;

    }


    public LiveData<List<NewsModel>> getCachedNews(){
        return helper.getNews();
    }

    //will be the Observer for OFFLINE NEWS
    public LiveData<Boolean> getNewsError(){
        return newsRepoError;
    }

    public LiveData<Throwable> getNewsThrowable(){
        return throwableMutableLiveData;
    }

    public LiveData<Integer> getNewsRecordCount(){
        return helper.getNewsRecordCount();
    }

    private String getDailyKey(){
        dKey = sf.getDailyKey();
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    dKey = response.body();
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: getDailyKey: "+t.getMessage());
            }
        });
        return dKey;
    }



}
