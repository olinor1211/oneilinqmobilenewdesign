package com.knx.inquirer.repository;
import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyInqNewsBySectionRepository {

    private ApiService apiService;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData<List<Feed>> mutableNewsData;
    private MutableLiveData<Boolean> newsRepoError;
    private MutableLiveData<Throwable> throwableMutableLiveData;
    private String hashed;
    private DatabaseHelper helper;

    public MyInqNewsBySectionRepository(Context context, CompositeDisposable disposable, SharedPrefs sf) {
        this.compositeDisposable = disposable;
        this.sf = sf;
        helper = new DatabaseHelper(context);
    }

    public LiveData<List<Feed>> getCustomNews(int mParam, int mPos){

        mutableNewsData = new MutableLiveData<>();
        newsRepoError = new MutableLiveData<>();
        throwableMutableLiveData = new MutableLiveData<>();
        long unixTime = System.currentTimeMillis() / 1000L;
        hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megasection/" + mParam);

        apiService = ServiceGenerator.createService(ApiService.class);

        apiService.getCustomNewsBySection(String.valueOf(unixTime), sf.getDeviceId(), mParam, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<Feed>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ArrayList<Feed> news) {
                        //TEST DATA
                        //Podcast test
//                        Feed nmodel = new Feed();
//                        nmodel.setId("");
//                        nmodel.setType("110"); //podcast
//                        nmodel.setTitle("");  //  Jesus loves you !
//                        nmodel.setPubdate("");
//                        nmodel.setLabel("");
//                        nmodel.setLink("");
//                        nmodel.setImage("https://open.spotify.com/embed-podcast/episode/5RjS4UV9TFYITVj6EY5RiX");
//                        news.add(6, nmodel); // newsmodel.add(6, nmodel);
////
//////            //Podcast test
//                        Feed nmodel2 = new Feed();
//                        nmodel2.setId("");
//                        nmodel2.setType("111"); //podcast
//                        nmodel2.setTitle("");  //  Jesus loves you !
//                        nmodel2.setPubdate("");
//                        nmodel2.setLabel("");
//                        nmodel2.setLink("");
//                        nmodel2.setImage("https://open.spotify.com/embed-podcast/episode/3wE04BDlvbBoLkgdKcDnXS");
//                        news.add(8, nmodel2);  // newsmodel.add(8, nmodel2);
//////
//////            //Podcast test
//                        Feed nmodel3 = new Feed();
//                        nmodel3.setId("");
//                        nmodel3.setType("112"); //podcast
//                        nmodel3.setTitle("");  //  Jesus loves you !
//                        nmodel3.setPubdate("");
//                        nmodel3.setLabel("");
//                        nmodel3.setLink("");
//                        nmodel3.setImage("https://open.spotify.com/embed-podcast/episode/4C45juGzkUxA4t0O4fOEtt");
//                        news.add(10, nmodel3); // newsmodel.add(10, nmodel3);
//////
//                //DM Link test
//                        Feed nmodel4 = new Feed();
//                        nmodel4.setId("");
//                        nmodel4.setType("10"); //dm link
//                        nmodel4.setTitle("DailymotionLINK_TEST_MY_INQ");  //  Jesus loves you !
//                        nmodel4.setPubdate("September 09, 2020 02:53pm");
//                        nmodel4.setLabel("");//Entertainment_Test
//                        nmodel4.setLink("https://www.google.com");
//                        nmodel4.setImage("https://www.dailymotion.com/video/x7cyruo");
//                        news.add(7, nmodel4);

                        //News test
//                        Feed nmodel4 = new Feed();
//                        nmodel4.setId(null);
//                        nmodel4.setType("0"); //dm link
//                        nmodel4.setTitle("This is a long title for the this news. This title is for testing purposes only and cannot be treated seriously. So please be in good mood always for this is not tended to make you feel bad in any way.");  //  Jesus loves you !
//                        nmodel4.setPubdate("September 09, 2020 02:53pm");
//                        nmodel4.setLabel("");//Entertainment_Test
//                        nmodel4.setLink(null);
//                        nmodel4.setImage("https://s3.ap-southeast-1.amazonaws.com/img.inq.news/opinion/Michael.jpg");
//                        news.add(7, nmodel4);

                        mutableNewsData.setValue(news);
                        newsRepoError.setValue(false);

                        sf.setLastSectionCustom(mParam);

                        //Save to db
/*                        helper.deleteCustomNews();
                        helper.insertCustomNews(news);*/
                    }

                    @Override
                    public void onError(Throwable e) {
                        newsRepoError.setValue(true);
                        throwableMutableLiveData.setValue(e);

                        //Save previously selected section when error is encountered
                        sf.setLastCustomSectionPosition(mPos);
                    }
                });

            return mutableNewsData;

    }


    public LiveData<List<Feed>> getCachedNews(){
        return helper.getCustomNews();
    }

    public LiveData<Boolean> getCustomNewsError(){
        return newsRepoError;
    }

    public LiveData<Throwable> getCustomNewsThrowable(){
        return throwableMutableLiveData;
    }

    public LiveData<Integer> getNewsRecordCount(){
        return helper.getCustomNewsRecordCount();
    }
}
