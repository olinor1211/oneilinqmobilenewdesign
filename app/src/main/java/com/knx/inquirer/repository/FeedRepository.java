package com.knx.inquirer.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.Featured;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.FeedResponse;
import com.knx.inquirer.models.Option;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class FeedRepository {
    private static final String TAG = "FeedRepository";

    private ApiService apiService;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData<FeedResponse> feedResponseData;
    private MutableLiveData<Boolean> feedRepoError;
    private DatabaseHelper helper;

    //for mobility
    private ArrayList<Option> optionmodel = new ArrayList<>();

    public FeedRepository(Context context, CompositeDisposable disposable, SharedPrefs sf) {
        this.compositeDisposable = disposable;
        this.sf = sf;

        helper = new DatabaseHelper(context);
    }

    public MutableLiveData<FeedResponse> getMenuandNews(){

        feedRepoError = new MutableLiveData<>();
        feedResponseData = new MutableLiveData<>();
        apiService = ServiceGenerator.createService(ApiService.class);

        //two apiService done here   ...getDailyKey and getFeed
        apiService.getDailyKey()
                .flatMap((Function<String, SingleSource<FeedResponse>>) key -> {
                    long unixTime = System.currentTimeMillis() / 1000L;
//                    String hashed OLD = Global.Generate32SHA512(key +unixTime + "inq" + sf.getDeviceId() + "-megaregfeed/" + sf.getOptionId());
                    String hashed = Global.Generate32SHA512(key + unixTime + "inq" + sf.getDeviceId() + "-megaregfeed2");
                    sf.setDailyKey(key);
                    return apiService.getFeed(String.valueOf(unixTime),sf.getDeviceId(), String.valueOf(sf.getDeviceKey()), hashed);
//                   OLD  return apiService.getFeed(String.valueOf(unixTime),sf.getDeviceId(), String.valueOf(sf.getOptionId()), hashed);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<FeedResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }


                    @Override
                    public void onSuccess(FeedResponse response) {
                        Log.d(TAG, "onSuccess: Feed REPO1 ");
                        feedRepoError.setValue(false);
                        feedResponseData.setValue(response);

                        sf.setLastSectionCustom(99);
                        sf.setLastCustomSectionPosition(0);

                        //Save to db
                        helper.deleteCustomSections();
//                        helper.insertCustomSections(response.getOption());

                        //for mobility
                        optionmodel.clear();
                        optionmodel.addAll(response.getOption());

                        //adding the object of Featured to Sections
//                        ArrayList<Featured> featured = menu.getMenu().getFeatured();
//                        ArrayList<Featured> featured = new ArrayList<>();
//                        featured.add(new Featured(100,"Mobility","https://www.google.com",false)) ;
//                        featured.add(new Featured(101,"Cinema","https://www.yahoo.com",false)) ;
//                        if (featured!=null){
//                            for (int x = 0; x<featured.size(); x++){
////                                optionmodel.add(new Option(menu.getMenu().getFeatured().get(x).getId(),menu.getMenu().getFeatured().get(x).getTitle(),menu.getMenu().getFeatured().get(x).getUrl(),false));
//                                optionmodel.add(new Option(featured.get(x).getId(),featured.get(x).getTitle(),featured.get(x).getUrl(),false)) ;
//
//                            }
//                        }

                        //remove tag menu no.4
                        for(int x = 0; x<optionmodel.size(); x++){
                            Log.d(TAG, "getCustomNews: optionmodel " + (optionmodel.get(x).getId()));
                            if(optionmodel.get(x).getId()==4){
                                optionmodel.remove(x);
                            }
                        }

                        Log.d(TAG, "onSuccess: Feed REPO3 ");

                        helper.insertCustomSections(optionmodel); //insert/save to the database

                        helper.deleteCustomNews();
                        Log.d(TAG, "onSuccess: Feed REPO4 ");
                        helper.insertCustomNews(response.getFeed());
                        Log.d(TAG, "onSuccess: Feed REPO5 ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        feedRepoError.setValue(true);

//                        EventBus.getDefault().post(new PostApiError(e.toString()));
                        Log.e("FEED_API_ERR", e.toString());
                    }
                });

        return feedResponseData;
    }

    public LiveData<List<Option>> getCachedCustomSections(){
        return helper.getCustomSections();
    }

    public LiveData<List<Feed>> getCachedCustomNews(){
        return helper.getCustomNews();
    }

    public LiveData<Boolean> getFeedError(){
        return feedRepoError;
    }

    public LiveData<Integer> getCustomSectionRecordCount(){
        return helper.getCustomSectionRecordCount();
    }

    public LiveData<Integer> getCustomNewsRecordCount(){
        return helper.getCustomNewsRecordCount();
    }

}
