package com.knx.inquirer.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.knx.inquirer.models.TrendingModel;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TrendingRepository {
    private static final String TAG = "TrendingRepository";

    private ApiService apiService;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData<List<TrendingModel>> mutableTrendingNews;
    private MutableLiveData<Boolean> trendingRepoError;
    private MutableLiveData<Throwable> throwableMutableLiveData;
    private String hashed;

    public TrendingRepository(Context context, CompositeDisposable disposable, SharedPrefs sf) {
        this.compositeDisposable = disposable;
        this.sf = sf;
    }

    public LiveData<List<TrendingModel>> getTrendingNews(){

        mutableTrendingNews = new MutableLiveData<>();
        trendingRepoError = new MutableLiveData<>();
        throwableMutableLiveData = new MutableLiveData<>();
        long unixTime = System.currentTimeMillis() / 1000L;
        hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megatrending");
        apiService = ServiceGenerator.createService(ApiService.class);
        apiService.getTrending(String.valueOf(unixTime), sf.getDeviceId(), hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<TrendingModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        Log.d(TAG, "onSubscribe: CALLED ");

                    }

                    @Override
                    public void onSuccess(ArrayList<TrendingModel> news) {
                        Log.d(TAG, "onSuccess: "+ news.toString());
                        mutableTrendingNews.setValue(news);
                        trendingRepoError.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        trendingRepoError.setValue(true);
                        throwableMutableLiveData.setValue(e);
                        Log.d(TAG, "onError: Throwable "+ e.getMessage());
                    }
                });

            return mutableTrendingNews;
    }


    public LiveData<Boolean> getNewsError(){
        return trendingRepoError;
    }

    public LiveData<Throwable> getNewsThrowable(){
        return throwableMutableLiveData;
    }
}
