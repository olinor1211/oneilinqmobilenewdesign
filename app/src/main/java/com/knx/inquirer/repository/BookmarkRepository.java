package com.knx.inquirer.repository;

import android.content.Context;
import androidx.lifecycle.LiveData;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.BookmarksNewModel;
import java.util.List;

public class BookmarkRepository {
    private static final String TAG = "BookmarkRepository";
    private DatabaseHelper helper;
    private Context context;

    public BookmarkRepository(Context applicationContext) {
        helper = new DatabaseHelper(context);
    }

    public LiveData<List<BookmarksNewModel>> getBookmarkList(){return helper.getAllBookmarks();}
}
