package com.knx.inquirer.repository;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.FeedResponse;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyInqNewsRepository {
    private static final String TAG = "MyInqNewsRepository";

    private ApiService apiService;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData<List<Feed>> myInqFeed;
    private MutableLiveData<Boolean> myInqRepoError;
    private MutableLiveData<Throwable> throwableMutableLiveData;
    private DatabaseHelper helper;
    private String dKey;

    public MyInqNewsRepository(Context context, CompositeDisposable disposable, SharedPrefs sf) {
        this.compositeDisposable = disposable;
        this.sf = sf;

        helper = new DatabaseHelper(context);
    }

    //Determine only by UNIQUE_ID of USER: sf.getOptionId()
    //ListOfUser: Custom,
    public MutableLiveData<List<Feed>> getMyInqNews(){

        myInqRepoError = new MutableLiveData<>();
        myInqFeed = new MutableLiveData<>();
        throwableMutableLiveData = new MutableLiveData<>();
        apiService = ServiceGenerator.createService(ApiService.class);

        String dailyKey = getDailyKey();
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(dailyKey + unixTime + "inq" + sf.getDeviceId() + "-megaregfeed2");
//        String hashed OLD = Global.Generate32SHA512(dailyKey + unixTime + "inq" + sf.getDeviceId() + "-megaregfeed/" + sf.getOptionId());

//        OLD apiService.getFeed(String.valueOf(unixTime), sf.getDeviceId(), String.valueOf(sf.getOptionId()), hashed)
        apiService.getFeed(String.valueOf(unixTime), sf.getDeviceId(), String.valueOf(sf.getDeviceKey()), hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<FeedResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(FeedResponse response) {

                        myInqRepoError.setValue(false);
                        myInqFeed.setValue(response.getFeed());

                        sf.setLastSectionCustom(99);

                        //Save to db
                        helper.deleteCustomNews();
                        helper.insertCustomNews(response.getFeed());
                    }

                    @Override
                    public void onError(Throwable e) {
                        throwableMutableLiveData.setValue(e);
                        myInqRepoError.setValue(true);
                    }
                });

        return myInqFeed;
    }

    public LiveData<List<Feed>> getMyInqCachedNews(){
        return helper.getCustomNews();
    }

    public LiveData<Integer> getNewsRecordCount(){
        return helper.getCustomNewsRecordCount();
    }

    public LiveData<Boolean> getMyInqError(){
        return myInqRepoError;
    }

    public LiveData<Throwable> getMyInqThrowable(){
        return throwableMutableLiveData;
    }

    private String getDailyKey(){
        dKey = sf.getDailyKey();
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    dKey = response.body();
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: getDailyKey: "+t.getMessage());
            }
        });
        return dKey;
    }
}
