package com.knx.inquirer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import com.knx.inquirer.utils.SharedPrefs;

public class BridgeActivity extends Activity{

	private SharedPrefs sf;
	private boolean viewed;

	  public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);

	        sf = new SharedPrefs(this);
		  	viewed =  sf.isQRTutViewed();

	        if (!viewed){
	        	sf.setQRTutViewed(true);
	            Intent intent = new Intent();
	            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.Tutorial2Activity");
	    	    startActivity(intent);
			}
	        else
	        {
	            Intent intent = new Intent();
	            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.QrActivity");
	    	    startActivity(intent);
			}
		  finish();
	  }
}
