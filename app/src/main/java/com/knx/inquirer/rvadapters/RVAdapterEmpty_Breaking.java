package com.knx.inquirer.rvadapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentBreaking;
import com.knx.inquirer.utils.Global;

public class RVAdapterEmpty_Breaking extends RecyclerView.Adapter<RVAdapterEmpty_Breaking.ViewHolder> {
    private static final String TAG = "RVAdapterEmpty_Breaking";

    private FragmentBreaking fragment;
    private String err_msg;
    private int method;

    public RVAdapterEmpty_Breaking(FragmentBreaking fragment, String err_msg, int method) {
        this.fragment = fragment;
        this.err_msg = err_msg;
        this.method = method;
    }

    @Override
    public RVAdapterEmpty_Breaking.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_empty, parent, false);
        RVAdapterEmpty_Breaking.ViewHolder viewHolder = new RVAdapterEmpty_Breaking.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapterEmpty_Breaking.ViewHolder holder, int position) {
        holder.error.setText(err_msg);
    }

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public Button retry;
        public TextView error;

        public ViewHolder(View itemView) {
            super(itemView);

            retry = itemView.findViewById(R.id.btnRetry);
            error = itemView.findViewById(R.id.tvErrorMsg);


            retry.setOnClickListener(view -> {
                Global.isRetry = true;
//                fragment.setBlankAdapter();
                if(method == 0) {
                    Log.d(TAG, "ViewHolder: Retry_  "+method );
                    fragment.RetryFetchMenuAndNews();
                }else{
                    fragment.RetryFetchNewsOnly();
                    Log.d(TAG, "ViewHolder: Retry_ "+method );

                }
            });


        }
    }
}
