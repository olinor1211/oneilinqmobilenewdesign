package com.knx.inquirer.rvadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;

public class RVAdapter_NoNotifs extends RecyclerView.Adapter<RVAdapter_NoNotifs.ViewHolder> {

    public RVAdapter_NoNotifs() {
    }

    @Override
    public RVAdapter_NoNotifs.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notifs_empty, parent, false);
        RVAdapter_NoNotifs.ViewHolder viewHolder = new RVAdapter_NoNotifs.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapter_NoNotifs.ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);


        }
    }
}
