package com.knx.inquirer.rvadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentCustom;

public class RVAdapterEmpty_Custom extends RecyclerView.Adapter<RVAdapterEmpty_Custom.ViewHolder> {

    private FragmentCustom fragment;

    public RVAdapterEmpty_Custom(FragmentCustom fragment) {
        this.fragment = fragment;
    }

    @Override
    public RVAdapterEmpty_Custom.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_empty, parent, false);
        RVAdapterEmpty_Custom.ViewHolder viewHolder = new RVAdapterEmpty_Custom.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapterEmpty_Custom.ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private Button retry;

        public ViewHolder(View itemView) {
            super(itemView);

            retry = itemView.findViewById(R.id.btnRetry);

            retry.setOnClickListener(view -> {
                fragment.setBlankAdapter();
                fragment.RetryFetchCustom();
            });

        }
    }
}