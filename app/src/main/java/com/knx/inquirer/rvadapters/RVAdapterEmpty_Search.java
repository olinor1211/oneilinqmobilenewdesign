package com.knx.inquirer.rvadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;

public class RVAdapterEmpty_Search extends RecyclerView.Adapter<RVAdapterEmpty_Search.ViewHolder> {


    public RVAdapterEmpty_Search(){}

    @Override
    public RVAdapterEmpty_Search.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.noresults, parent, false);
        RVAdapterEmpty_Search.ViewHolder viewHolder = new RVAdapterEmpty_Search.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapterEmpty_Search.ViewHolder holder, int position) {}

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise no item will show
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);


        }
    }
}
