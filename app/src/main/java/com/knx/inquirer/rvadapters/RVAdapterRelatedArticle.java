package com.knx.inquirer.rvadapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.knx.inquirer.R;
import com.knx.inquirer.WebBookmarksActivity;
import com.knx.inquirer.WebNewsActivity;
import com.knx.inquirer.fragments.FragmentBookmarks;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.models.Related;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class RVAdapterRelatedArticle extends RecyclerView.Adapter<RVAdapterRelatedArticle.ViewHolder> {
    private static final String TAG = "RVAdapterRelatedArticle";

//    public List<BookmarksNewModel> relatedArticles = new ArrayList<>();
//    public List<Related> relatedArticles = new ArrayList<>();
    public List<JsonObject> relatedArticles = new ArrayList<>();
    private Context context;
    private int sect =  R.drawable.inquirernewsinfo;
    private JSONArray jArray;
    private JSONObject jsonObj;

//    public RVAdapterRelatedArticle(Context context, List<Related>  relatedArticles) throws JSONException {
    public RVAdapterRelatedArticle(Context context, List<JsonObject>  relatedArticles) throws JSONException {
        super();
        this.relatedArticles = relatedArticles;
        this.context = context;
        //relatedArticles is an ARRAY with JSON_OBJECT as a its VALUE   ex: [{},{},{}]
//        Gson gson = new Gson();
//        String articles = gson.toJson(relatedArticles); //Convert to JSON STRING First
//        jArray = new JSONArray(relatedArticles); //then Convert to JSONArray, NOTE: JSONArray not JsonArray
//        jsonObj = jArray.getJSONObject(0); //READ/GET the JSON ARRAY Value as getJSONObject(index)
//        String titel = jobj.getString("title"); ////READ/GET the ACTUAL VALUE of specific keyName
//                Log.d(TAG, "RVAdapterRelatedArticle:GGG "+ titel);


//        jsonObj = new JSONObject(articles);
//        this.jArray = jsonObj.getJSONArray(articles);
//        Log.d(TAG, "RVAdapterRelatedArticle:GGG "+ jsonObj.toString());
//        Log.d(TAG, "RVAdapterRelatedArticle:GGG "+ jsonObj.get("title"));

//        JSONObject jObj = jArray.getJSONObject(i);
//        String id = jObj.optString("id");
//        String name=jObj.optString("name");

////        this.relatedArticles.add(artivcles);


//        String artivcles2 = gson.toJson(relatedArticles);
////            Log.d(TAG, "getBookmarkList: ARTICLE_RELATED: "+bookmarkData);
//
//        //GetRelated
//        Log.d(TAG, "initViews: RELATED_ARTI_ RELE "+Global.relatedArticles.size());
//        Log.d(TAG, "initViews: RELATED_ARTI RELE "+artivcles);
//
//        Log.d(TAG, "initViews: RELATED_ARTI_ RELE2 "+relatedArticles.size());
//        Log.d(TAG, "initViews: RELATED_ARTI RELE2 "+artivcles);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.related_articles_list_new, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
       String mainImage = "";
       String title = "";
       String date = "";
//       String type = ""; //No type in API for Now: Dont filter switch

//        switch (type) {
//            case "2":  sect = R.drawable.inquirersports;
//                break;
//            case "3":  sect = R.drawable.inquirerentertainment;
//                break;
//            case "5":  sect = R.drawable.inquirertechnology;
//                break;
//            case "6":  sect = R.drawable.inquirerglobalnation;
//                break;
//            case "7":  sect = R.drawable.inquirerbusiness;
//                break;
//            case "8":  sect = R.drawable.inquirerlifestyle;
//                break;
//            case "13":  sect = R.drawable.inquireropinion;
//                break;
//            default: sect = R.drawable.inquirernewsinfo;
//        }

        if(relatedArticles.get(position).has("image")) mainImage = !relatedArticles.get(position).getAsJsonObject().get("image").isJsonNull() ? relatedArticles.get(position).getAsJsonObject().get("image").getAsString() : "";
        if(relatedArticles.get(position).has("title")) title = !relatedArticles.get(position).getAsJsonObject().get("title").isJsonNull() ? relatedArticles.get(position).getAsJsonObject().get("title").getAsString() : "";
        if(relatedArticles.get(position).has("pubdate")) date = !relatedArticles.get(position).getAsJsonObject().get("pubdate").isJsonNull() ? relatedArticles.get(position).getAsJsonObject().get("pubdate").getAsString() : "";

        //No need to validate if Null or Empty: Already validates at the top: If Empty it will Display the error(sect)
//        if(isValidated(mainImage)) {
//            GlideApp.with(context)
//                    .load(mainImage)
////                    .transition(withCrossFade())
//                    .thumbnail(0.05f)
//                    .apply(requestOptions())
//                    .into(holder.ivImage);
//        }

        try{
            showImage(mainImage, holder.ivImage);
        }catch(Exception e ){
            Log.d(TAG, "onBindViewHolder: "+ e.getMessage());
        }


        //Using Picasso only Here
//        try{
//            Picasso.get()
//                    .load(mainImage)
//                    .noFade()
//                    .noPlaceholder()
//                    .error(sect)
//                    .into(holder.ivImage);
//        }catch(Exception e ){
//            Log.d(TAG, "onBindViewHolder: "+ e.getMessage());
//        }

        holder.tvTitle.setText(title);
        holder.tvDate.setText(date);
    }

    private RequestOptions requestOptions(){
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(sect)
                .skipMemoryCache(true)
                .error(sect)
//                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC) //  .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        return options;
    }

    private void showImage(String imgStr, ImageView imgView){
        //Loading Image and get the Height and Width of the ImageUrl: And load the Image as Bitmap to make it Loading Faster
        Glide.with(context)
                .asBitmap()
                .load(imgStr)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        //NO Need to Doubled the width and height just to make more clearer bitmap. Only has Small ContainerFrame
                        imgView.setImageBitmap(resource);//imgView.setImageBitmap(resource);
                    }
                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }

    @Override
    public int getItemCount() {
        return relatedArticles.size();
    }

    private boolean isValidated(String string){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
        }finally {
            if(string!=null) return true;  //Check if null
            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
        }
        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public TextView tvTitle, tvDate;
        public ImageView ivImage, share, delete;

        public ViewHolder(final View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.ivRelatedImage);
            tvTitle = itemView.findViewById(R.id.tvRelatedTitle);
            tvDate = itemView.findViewById(R.id.tvRelatedDate);

            itemView.setOnClickListener(view -> {
                //CHECKPOINT HERE: Avoid Multiple Viewing of Article
                if (( SystemClock.elapsedRealtime() - Global.lastClickTime) < 1000){
                    Global.isItemClicked=true;
                    Log.d(TAG, "onSuccess: onPauseBreaking AVOIDED");
                }else Global.isItemClicked=false;

                Global.lastClickTime =  SystemClock.elapsedRealtime();

                int pos = getAdapterPosition();
                String id ="";
                try {
//                    id = jArray.getJSONObject(pos).getString("id");
//                    id = relatedArticles.get(pos).getAsJsonObject().get("id").getAsString();
                    id = !relatedArticles.get(pos).getAsJsonObject().get("id").isJsonNull() ? relatedArticles.get(pos).getAsJsonObject().get("id").getAsString() : "";
                    Log.d(TAG, "viewRelatedArticle:NEWS_ID: "+id);
                } catch (Exception e) {
                    Log.d(TAG, "ViewHolder: Exception "+ e.getMessage());
                }
//                Global.isItemClicked = false;
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
//                        EventBus.getDefault().post(Global.RELATED_ARTICLE+"/"+relatedArticles.get(pos).getNewsId());
//                        if(context instanceof WebNewsActivity) ((WebNewsActivity)context).viewRelatedArticle(id);
                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }
}
//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); – Otra Jul 1 '11 at 12:40