package com.knx.inquirer.rvadapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.knx.inquirer.models.PostArticleBreaking;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentTrending;
import com.knx.inquirer.models.PostArticleTrending;
import com.knx.inquirer.models.TrendingModel;
import com.knx.inquirer.utils.PlayerWebView;
import com.knx.inquirer.utils.SharedPrefs;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

//Recyclerview Adapter when app is online
public class RVAdapterTrending extends RecyclerView.Adapter {
    private static final String TAG = "RVAdapterTrending";

    public ArrayList<TrendingModel> news;
    private FragmentTrending fragment;
    private Context context;
    private SharedPrefs sf;
    public String _id, section, photo, type;
    private List<String> imgItems;
    private AudioManager amanager;
    private int itemposition;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_IMAGE = 2;
    private final int VIEW_TYPE_VIDEO = 3;
    private final int VIEW_TYPE_CAROUSEL = 4;
    private final int VIEW_TYPE_IMAGE_DOUBLE = 5;
    private final int VIEW_TYPE_VIDEO_DOUBLE = 6;
    private final int VIEW_TYPE_VIDEO_ARTICLE = 7;
    private final int VIEW_TYPE_DM = 8;
    private final int VIEW_TYPE_INQPRIME = 9;


    //test
    private SimpleExoPlayer exoPlayer, exoPlayer2, exoPlayer3;

    public RVAdapterTrending(Context context, ArrayList<TrendingModel> news, FragmentTrending fragment, SharedPrefs sf) {
        super();
        this.news = news;
        this.context = context;
        this.fragment = fragment;
        this.sf = sf;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_row, parent, false);
////        RVAdapterTrending.ViewHolder viewHolder = new RVAdapterTrending.ViewHolder(v);
////
////        return viewHolder;

        if (viewType == VIEW_TYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_row, parent, false);
            vh = new TrendingItemViewHolder(v);  //calling instance of viewHolder

        }else if(viewType == VIEW_TYPE_IMAGE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_items, parent, false);
            vh = new TrendingImageTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_list_items, parent, false);
            vh = new TrendingVideoTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_CAROUSEL) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carousel_list_items, parent, false);
            vh = new TrendingCarouselTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_IMAGE_DOUBLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_double_list_items, parent, false);
            vh = new TrendingImageDoubleViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO_DOUBLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_double_list_items, parent, false);
            vh = new TrendingVideoDoubleTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO_ARTICLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_article_list_items, parent, false);
            vh = new TrendingVideoArticleTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_DM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_motion_list_items, parent, false);
            vh = new TrendingDMTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_INQPRIME) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inqprime_list_row, parent, false);
        vh = new TrendingInqPrimeTypeViewHolder(v);

        }else  {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_loading, parent, false);
            vh = new TrendingProgressViewHolder(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        //Passing data from URL
        _id = news.get(position).getId();
        photo = news.get(position).getImage();
        type = news.get(position).getType();

//        holder.tag.setVisibility(View.VISIBLE);
//        holder.tag.setText("TRENDING");

        //Showing data on the views
        if (holder instanceof TrendingItemViewHolder || holder instanceof TrendingVideoTypeViewHolder || holder instanceof TrendingImageDoubleViewHolder || holder instanceof TrendingVideoDoubleTypeViewHolder || holder instanceof TrendingCarouselTypeViewHolder || holder instanceof  TrendingVideoArticleTypeViewHolder || holder instanceof  TrendingImageTypeViewHolder || holder instanceof TrendingDMTypeViewHolder || holder instanceof TrendingInqPrimeTypeViewHolder) {

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.inquirernewsinfo)
                .error(R.drawable.inquirernewsinfo)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .priority(Priority.HIGH);

        RequestOptions options_ads = new RequestOptions()
                .fitCenter()
                .placeholder(R.drawable.inquirernewsinfo)
                .error(R.drawable.inquirernewsinfo)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .priority(Priority.HIGH);


        switch (type) {
            case "0":
            case "1":

                ( (TrendingItemViewHolder) holder ).tag.setVisibility(View.VISIBLE);
                ( (TrendingItemViewHolder) holder ).tag.setText("TRENDING");

                if (isValidated(news.get(position).getTitle())) {
//                    if (!news.get(position).getTitle().equals("null") || !news.get(position).getTitle().equals("")) {
                    ( (TrendingItemViewHolder) holder ).newstitle.setText(news.get(position).getTitle());
                } else {
                    ( (TrendingItemViewHolder) holder ).newstitle.setText("");
                }

                if (isValidated(news.get(position).getPubdate())) {
//                if (!news.get(position).getPubdate().equals("null") || !news.get(position).getPubdate().equals("")) {
                    ( (TrendingItemViewHolder) holder ).newspubdate.setText(news.get(position).getPubdate());
                } else {
                    ( (TrendingItemViewHolder) holder ).newspubdate.setText("");
                }

                ( (TrendingItemViewHolder) holder ).newsphoto.setVisibility(View.VISIBLE);
//                ( (TrendingItemViewHolder) holder ).adscraper.setVisibility(View.GONE);
//                ( (TrendingItemViewHolder) holder ).newsimageslider.setVisibility(View.GONE);
//                    ((ItemViewHolder) holder).newsvideo.setVisibility(View.GONE);

                if (isValidated(photo)) {
//                if (!photo.equals("")) {
                    //Load images into ImageView

                    Glide
                            .with(context)
                            .load(photo)
                            .transition(withCrossFade())
                            .apply(options)
                            .into(( (TrendingItemViewHolder) holder ).newsphoto);
//                    ViewCompat.setTransitionName(( (TrendingItemViewHolder) holder ).newsphoto, news.get(position).getId());

                }
//                else {
//
//                    switch (sf.getLastSection()) {
//                        case 1:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirernewsinfo)
//                                    .into(( (ItemViewHolder) holder ).newsphoto);
//                            break;
//                        case 2:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirersports)
//                                    .into(( (ItemViewHolder) holder ).newsphoto);
//                            break;
//                        case 3:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirerentertainment)
//                                    .into(( (ItemViewHolder) holder ).newsphoto);
//                            break;
//                        case 5:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirertechnology)
//                                    .into(( (ItemViewHolder) holder ).newsphoto);
//                            break;
//                        case 6:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirerglobalnation)
//                                    .into(( (ItemViewHolder) holder ).newsphoto);
//                            break;
//                        case 7:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirerbusiness)
//                                    .into(( (ItemViewHolder) holder ).newsphoto);
//                            break;
//
//                        case 8:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirerlifestyle)
//                                    .into(( (ItemViewHolder) holder ).newsphoto);
//                            break;
//
//                        default:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirernewsinfo)
//                                    .into(( (ItemViewHolder) holder ).newsphoto);
//                    }
//                }
                break;

            case "2": //Image

                Log.d("TAGGG", "onBindViewHolder: CASE 5: called" + photo);

                if (isValidated(news.get(position).getTitle())) {
//                if (news.get(position).getTitle().isEmpty()) {
                    ((TrendingImageTypeViewHolder) holder).textImageTitle.setText("");

                } else {
                    ((TrendingImageTypeViewHolder) holder).textImageTitle.setText(news.get(position).getTitle());
                }

                //noTag
                ((TrendingImageTypeViewHolder) holder).textImageSponsored.setVisibility(View.GONE);


//                if (news.get(position).getLabel().isEmpty()) {
//                    ((TrendingImageTypeViewHolder) holder).textImageSponsored.setText("");
//                } else {
//                    ((TrendingImageTypeViewHolder) holder).textImageSponsored.setText(news.get(position).getLabel());
//                }

                    if (isValidated(photo)) {
//                if (!photo.isEmpty()) {
                    Glide
                            .with(context)
                            .load(photo)
                            .transition(withCrossFade())
                            .apply(options)
                            .into(( (TrendingImageTypeViewHolder) holder ).imageOrdinaryPhoto);
                }
                break;

            case "3": //Video

//
                if (isValidated(news.get(position).getTitle())) {

//                    if (news.get(position).getTitle().isEmpty()) {
                    ( (TrendingVideoTypeViewHolder) holder ).video3Title.setText("");
                } else {
                    ( (TrendingVideoTypeViewHolder) holder ).video3Title.setText(news.get(position).getTitle());
                }

                //noTag
                ( (TrendingVideoTypeViewHolder) holder ).video3Sponsored.setVisibility(View.GONE);

//                if (news.get(position).getLabel().isEmpty()) {
//                    ( (TrendingVideoTypeViewHolder) holder ).video3Sponsored.setText("");
//                } else {
//                    ( (TrendingVideoTypeViewHolder) holder ).video3Sponsored.setText(news.get(position).getLabel());
//                }

                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

                DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                ( (TrendingVideoTypeViewHolder) holder ).newsVideo.setUseController(false);  //play, stop
                ( (TrendingVideoTypeViewHolder) holder ).newsVideo.setPlayer(exoPlayer);
                ( (TrendingVideoTypeViewHolder) holder ).newsVideo.setShowBuffering(true);

                exoPlayer.prepare(mediaSource);
                exoPlayer.setPlayWhenReady(true);
                exoPlayer.setVolume(0f);
//                    exoPlayer.setRepeatMode(exoPlayer.REPEAT_MODE_ONE);
                break;


            case "4":  // Carousel

                String images = news.get(position).getImage();
                String imagesUrl[] = images.split(",");

//                if (isValidated(( TrendingCarouselTypeViewHolder)holder ).carouselView)) {
                    if (( (TrendingCarouselTypeViewHolder)holder ).carouselView !=null ){

                    ImageListener imageListener = (position1, imageView) -> {
                        Picasso.get()
                                .load(imagesUrl[position1])
                                .noFade()
                                .placeholder(R.drawable.inquirernewsinfo)
                                .into(imageView);
                    };

                    ( (TrendingCarouselTypeViewHolder)holder ).carouselView.setPageCount(imagesUrl.length);
                    ( (TrendingCarouselTypeViewHolder)holder ).carouselView.setImageListener(imageListener);
                    //noTag
                    ( (TrendingCarouselTypeViewHolder)holder ).textCarouselTag.setVisibility(View.GONE);
                    if(!news.get(position).getTitle().isEmpty()){
                        ( (TrendingCarouselTypeViewHolder)holder ).textCarouselTitle.setText(news.get(position).getTitle());
                    }else{( (TrendingCarouselTypeViewHolder)holder ).textCarouselTitle.setVisibility(View.GONE);}

                }
                break;


            case "5": //ImageDouble

                Log.d("TAGGG", "onBindViewHolder: CASE 5: called" + photo);

                if (isValidated(news.get(position).getTitle())) {
//                if (news.get(position).getTitle().isEmpty()) {
                    ((TrendingImageDoubleViewHolder) holder).textDoubleTitle.setText("");

                } else {
                    ((TrendingImageDoubleViewHolder) holder).textDoubleTitle.setText(news.get(position).getTitle());
                }

                //noTag
                ((TrendingImageDoubleViewHolder) holder).textDoubleSponsored.setVisibility(View.GONE);

//                if (news.get(position).getLabel().isEmpty()) {
//                    ((TrendingImageDoubleViewHolder) holder).textDoubleSponsored.setText("");
//                } else {
//                    ((TrendingImageDoubleViewHolder) holder).textDoubleSponsored.setText(news.get(position).getLabel());
//                }
                if (isValidated(photo)) {
//                if (!photo.isEmpty()) {
                    Glide
                            .with(context)
                            .load(photo)
                            .transition(withCrossFade())
                            .apply(options)
                            .into(( (TrendingImageDoubleViewHolder) holder ).imageDoubePhoto);
                }
                break;


            case "6": //VideoDouble height
//
                if (isValidated(news.get(position).getTitle())) {

//                    if (news.get(position).getTitle().isEmpty()) {
                    ((TrendingVideoDoubleTypeViewHolder) holder).video6Title.setText("");
                } else {
                    ((TrendingVideoDoubleTypeViewHolder) holder).video6Title.setText(news.get(position).getTitle());
                }

                //noTag
                ((TrendingVideoDoubleTypeViewHolder) holder).video6Sponsored.setVisibility(View.GONE);


//                if (news.get(position).getLabel().isEmpty()) {
//                    ((TrendingVideoDoubleTypeViewHolder) holder).video6Sponsored.setText("");
//                } else {
//                    ((TrendingVideoDoubleTypeViewHolder) holder).video6Sponsored.setText(news.get(position).getLabel());
//                }

                BandwidthMeter bandwidthMeter2 = new DefaultBandwidthMeter();
                TrackSelector trackSelector2 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter2));
                exoPlayer2 = ExoPlayerFactory.newSimpleInstance(context, trackSelector2);

                DefaultHttpDataSourceFactory dataSourceFactory2 = new DefaultHttpDataSourceFactory("exoplayer_video2");
                MediaSource mediaSource2 = new ExtractorMediaSource.Factory(dataSourceFactory2).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                ( (TrendingVideoDoubleTypeViewHolder) holder ).newsVideoDouble.setUseController(false);  //play, stop
                ( (TrendingVideoDoubleTypeViewHolder) holder ).newsVideoDouble.setPlayer(exoPlayer2);
                ( (TrendingVideoDoubleTypeViewHolder) holder ).newsVideoDouble.setShowBuffering(true);

                exoPlayer2.prepare(mediaSource2);
                exoPlayer2.setPlayWhenReady(true);
                exoPlayer2.setVolume(0f);

//                    exoPlayer2.setRepeatMode(exoPlayer2.REPEAT_MODE_ONE);
                break;


            case "7": //Video Article

                if (isValidated(news.get(position).getTitle())) {

//                    if (news.get(position).getTitle().isEmpty()) {
                    ( (TrendingVideoArticleTypeViewHolder) holder ).video7Title.setText("");
                } else {
                    ( (TrendingVideoArticleTypeViewHolder) holder ).video7Title.setText(news.get(position).getTitle());
                }

                //noTag
                ( (TrendingVideoArticleTypeViewHolder) holder ).video7Sponsored.setVisibility(View.GONE);

//                if (news.get(position).getLabel().isEmpty()) {
//                    ( (TrendingVideoArticleTypeViewHolder) holder ).video7Sponsored.setText("");
//                } else {
//                    ( (TrendingVideoArticleTypeViewHolder) holder ).video7Sponsored.setText(news.get(position).getLabel());
//                }

                BandwidthMeter bandwidthMeter3 = new DefaultBandwidthMeter();
                TrackSelector trackSelector3 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter3));
                exoPlayer3 = ExoPlayerFactory.newSimpleInstance(context, trackSelector3);

                DefaultHttpDataSourceFactory dataSourceFactory3 = new DefaultHttpDataSourceFactory("exoplayer_video3");
                MediaSource mediaSource3 = new ExtractorMediaSource.Factory(dataSourceFactory3).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                ( (TrendingVideoArticleTypeViewHolder) holder ).newsVideoArticle.setUseController(false);  //play, stop
                ( (TrendingVideoArticleTypeViewHolder) holder ).newsVideoArticle.setPlayer(exoPlayer3);
                ( (TrendingVideoArticleTypeViewHolder) holder ).newsVideoArticle.setShowBuffering(true);

                exoPlayer3.prepare(mediaSource3);
                exoPlayer3.setPlayWhenReady(true);
                exoPlayer3.setVolume(0f);
//                    exoPlayer3.setRepeatMode(exoPlayer3.REPEAT_MODE_ONE);

                break;

            case "8": //Dailymotion

                String vidUrl = news.get(position).getImage();

                if (isValidated(news.get(position).getTitle())) {

//                    if (news.get(position).getTitle().isEmpty()) {
                    ( (TrendingDMTypeViewHolder) holder ).textDmTitle.setText("");
                } else {
                    ( (TrendingDMTypeViewHolder) holder ).textDmTitle.setText(news.get(position).getTitle());
                }

//                if (news.get(position).getLabel().isEmpty()) {
//                    ( (TrendingDMTypeViewHolder) holder ).textDmSponsored.setText("");
//                } else {
//                    ( (TrendingDMTypeViewHolder) holder ).textDmSponsored.setText(news.get(position).getLabel());
//                }

                ((TrendingDMTypeViewHolder) holder).dmPlayerWebView.showControls(false);
                ((TrendingDMTypeViewHolder) holder).dmPlayerWebView.mute();
                ((TrendingDMTypeViewHolder) holder).dmPlayerWebView.setPlayWhenReady(true);
                ((TrendingDMTypeViewHolder) holder).dmPlayerWebView.load(vidUrl);

                break;


            case "9":
                Log.d(TAG, "onBindViewHolder: Type 9 - InqPrime");

                if (isValidated(news.get(position).getTitle())) {
//                    if (!news.get(position).getTitle().equals("null") || !news.get(position).getTitle().equals("")) {
                    ( (TrendingInqPrimeTypeViewHolder) holder ).newsTitleInqPrime.setText(news.get(position).getTitle());
                } else {
                    ( (TrendingInqPrimeTypeViewHolder) holder ).newsTitleInqPrime.setText("");
                }

                if (isValidated(news.get(position).getPubdate())) {
//                if (!news.get(position).getPubdate().equals("null") || !news.get(position).getPubdate().equals("")) {
                    ( (TrendingInqPrimeTypeViewHolder) holder ).newsPubdateInqPrime.setText(news.get(position).getPubdate());
                } else {
                    ( (TrendingInqPrimeTypeViewHolder) holder ).newsPubdateInqPrime.setText("");
                }

                if (isValidated(photo)) {
//                if (!photo.isEmpty()) {
                    Glide
                            .with(context)
                            .load(photo)
                            .transition(withCrossFade())
                            .apply(options)
                            .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);

//                        ViewCompat.setTransitionName(( (InqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime, news.get(position).getId());
                }
                else {
                    switch (sf.getLastSection()) {
//                        case 1:
//                            Glide
//                                    .with(context)
//                                    .load(R.drawable.inquirernewsinfo)
//                                    .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
//                            break;
                        case 2:
                            Glide
                                    .with(context)
                                    .load(R.drawable.inquirersports)
                                    .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                            break;
                        case 3:
                            Glide
                                    .with(context)
                                    .load(R.drawable.inquirerentertainment)
                                    .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                            break;
                        case 5:
                            Glide
                                    .with(context)
                                    .load(R.drawable.inquirertechnology)
                                    .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                            break;
                        case 6:
                            Glide
                                    .with(context)
                                    .load(R.drawable.inquirerglobalnation)
                                    .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                            break;
                        case 7:
                            Glide
                                    .with(context)
                                    .load(R.drawable.inquirerbusiness)
                                    .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                            break;

                        case 8:
                            Glide
                                    .with(context)
                                    .load(R.drawable.inquirerlifestyle)
                                    .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                            break;

                        default:
                            Glide
                                    .with(context)
                                    .load(R.drawable.inquirernewsinfo)
                                    .into(( (TrendingInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                    }
                }
                break;

        }

        }else{
                ((TrendingProgressViewHolder) holder).progressBar.setIndeterminate(true);
            }

    }

    @Override
    public int getItemViewType(int position) {

        if (news.get(position) != null) {
            String type = news.get(position).getType();
            switch(type){
                case "0":
                case "1":
                    return VIEW_TYPE_ITEM;

                case "2":
                    return VIEW_TYPE_IMAGE;

                case "3":
                    return VIEW_TYPE_VIDEO;

                case "4":
                    return VIEW_TYPE_CAROUSEL;

                case "5":
                    return VIEW_TYPE_IMAGE_DOUBLE;

                case "6":
                    return VIEW_TYPE_VIDEO_DOUBLE;

                case "7":
                    return VIEW_TYPE_VIDEO_ARTICLE;

                case "8":
                    return VIEW_TYPE_DM;

                case "9":
                    Log.d(TAG, "getItemViewType: InqPrime " + type);
                    return VIEW_TYPE_INQPRIME;

                default:
                    return -1;

            }

        }else {
            return VIEW_TYPE_LOADING;
        }
    }


    @Override
    public int getItemCount() {
        return news.size();
    }


    private boolean isValidated(String string){

        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
        }finally {
            if(string!=null) return true;  //Check if null
            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
        }
        return false;
    }


    public class TrendingImageTypeViewHolder extends RecyclerView.ViewHolder {
        private TextView textImageTitle, textImageSponsored;
        private ImageView imageOrdinaryPhoto;

        public TrendingImageTypeViewHolder(@NonNull View itemView) {
            super(itemView);

            imageOrdinaryPhoto = itemView.findViewById(R.id.imageOrdinaryPhoto);
            textImageSponsored = itemView.findViewById(R.id.textImageSponsored);
            textImageTitle = itemView.findViewById(R.id.textImageTitle);

            itemView.setOnClickListener(v -> {
//                releasePlayer();

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    fragment.checkImageLink(link);

                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    public class TrendingVideoTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideo;
        TextView video3Title, video3Sponsored;
//          RelativeLayout video3Layout;


        public TrendingVideoTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideo = itemView.findViewById(R.id.news_video);
            this.video3Sponsored = itemView.findViewById(R.id.video3Sponsored);
            this.video3Title = itemView.findViewById(R.id.video3Title);
//            this.video3Layout = itemView.findViewById(R.id.rlVideo3);

            itemView.setOnClickListener(v -> {

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    fragment.checkVideoLink(link);

                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    public class TrendingVideoDoubleTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideoDouble;
        TextView video6Title, video6Sponsored;

        public TrendingVideoDoubleTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideoDouble = itemView.findViewById(R.id.news_video6);
            this.video6Sponsored = itemView.findViewById(R.id.video6Sponsored);
            this.video6Title = itemView.findViewById(R.id.video6Title);

            itemView.setOnClickListener(v -> {

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    fragment.checkVideoLink(link);

                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    public class TrendingVideoArticleTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideoArticle;
        TextView video7Title, video7Sponsored;

        public TrendingVideoArticleTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideoArticle = itemView.findViewById(R.id.news_video7);
            this.video7Sponsored = itemView.findViewById(R.id.video7Sponsored);
            this.video7Title = itemView.findViewById(R.id.video7Title);


            itemView.setOnClickListener(v -> {


                int n = getAdapterPosition();
                String videoUrl = news.get(n).getImage();

                if (Global.isNetworkAvailable(context)) {

                    fragment.displayFullview(videoUrl);

//                         Toast.makeText(context, "Carousel Clicked" + position, Toast.LENGTH_SHORT).show();
                    if (!Global.isItemClicked) {
                        Global.isItemClicked = true;

                        //  EventBus.getDefault().post(new PostArticleTrending("43816"));
                        EventBus.getDefault().post(new PostArticleTrending(news.get(itemposition).getId()));

                    }
                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }


            });

        }
    }

    public class TrendingCarouselTypeViewHolder extends RecyclerView.ViewHolder {

        private CarouselView carouselView;
        private TextView textCarouselTag, textCarouselTitle;

        public TrendingCarouselTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.carouselView = itemView.findViewById(R.id.carouselView);
            this.textCarouselTag = itemView.findViewById(R.id.textCarouselTag);
            this.textCarouselTitle = itemView.findViewById(R.id.textCarouselTitle);

            carouselView.setImageClickListener(position -> {

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {
//                         Toast.makeText(context, "Carousel Clicked" + position, Toast.LENGTH_SHORT).show();
                    if (!Global.isItemClicked) {
                        Global.isItemClicked = true;
//                        EventBus.getDefault().post(new PostArticleTrending("43816"));
                        EventBus.getDefault().post(new PostArticleTrending(news.get(itemposition).getId()));
                    }
                } else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });
        }
    }

    public class TrendingImageDoubleViewHolder extends RecyclerView.ViewHolder {
        private TextView textDoubleTitle, textDoubleSponsored;
        private ImageView imageDoubePhoto;

        public TrendingImageDoubleViewHolder(@NonNull View itemView) {
            super(itemView);

            imageDoubePhoto = itemView.findViewById(R.id.imageDoublePhoto);
            textDoubleSponsored = itemView.findViewById(R.id.textDoubleSponsored);
            textDoubleTitle = itemView.findViewById(R.id.textDoubleTitle);

            itemView.setOnClickListener(v -> {
//                releasePlayer();

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    fragment.checkImageLink(link);

                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    public class TrendingDMTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerWebView dmPlayerWebView;
        TextView textDmSponsored, textDmTitle;
        ImageView gradient;

        public TrendingDMTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.dmPlayerWebView = itemView.findViewById(R.id.dmPlayerWebView);
            this.textDmSponsored = itemView.findViewById(R.id.textDmSponsored);
            this.textDmTitle = itemView.findViewById(R.id.textDmTitle);
            this.gradient = itemView.findViewById(R.id.imageDmGradient);


            gradient.setOnClickListener(v -> {

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    fragment.checkVideoLink(link);

                } else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }


    //
    //InqPrimeTypeViewHolder
    private class TrendingInqPrimeTypeViewHolder extends RecyclerView.ViewHolder {
        //Views
        TextView newsTitleInqPrime, newsPubdateInqPrime, tagInqPrime;
        ImageView mPrimeTag, mImagePhotoInqPrime;

        public TrendingInqPrimeTypeViewHolder(View itemView) {
            super(itemView);

            newsTitleInqPrime = itemView.findViewById(R.id.textTitleInqPrime);
            newsPubdateInqPrime = itemView.findViewById(R.id.textDatesInqPrime);
            mImagePhotoInqPrime = itemView.findViewById(R.id.imagePhotoInqPrime);
            mPrimeTag = itemView.findViewById(R.id.primeTag);
            tagInqPrime = itemView.findViewById(R.id.tvTagInqPrime);

            itemView.setOnClickListener(v -> {

                if (Global.isNetworkAvailable(context)) {
                    Log.d(TAG, "InqPrimeTypeViewHolder: Clicked type " + type);
                    itemposition = getAdapterPosition();
                    EventBus.getDefault().post(new PostArticleTrending(news.get(itemposition).getId()));

                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }


    //
    private class TrendingProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        TrendingProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.pLoadingMore);
        }
    }


    public class TrendingItemViewHolder extends RecyclerView.ViewHolder {
        //Views
        public TextView newstitle, newspubdate, tag;
        public ImageView newsphoto,adscraper;
        public SliderLayout newsimageslider;
//        public VideoView newsvideo;

        public TrendingItemViewHolder(View itemView) {
            super(itemView);

            newstitle = itemView.findViewById(R.id.textTitle);
            newspubdate = itemView.findViewById(R.id.textDetails);
            newsphoto = itemView.findViewById(R.id.imagePhoto);
//            adscraper = itemView.findViewById(R.id.imageSkyScraper);
//            newsimageslider = itemView.findViewById(R.id.slider);
//            newsvideo = itemView.findViewById(R.id.videoNews);

            tag = itemView.findViewById(R.id.tvTag);

            itemView.setOnClickListener(v -> {
                itemposition = getAdapterPosition();

                if (!Global.isItemClicked) {
                    Global.isItemClicked = true;

                    if(news.get(itemposition).getLink() != null){

                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.get(itemposition).getLink()));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    } else {
                        EventBus.getDefault().post(new PostArticleTrending(news.get(itemposition).getId()));
                    }
                }
            });

        }

    }


    public void releasePlayer() {

        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
        }else if (exoPlayer2 != null) {
            exoPlayer2.release();
            exoPlayer2 = null;

        }else if (exoPlayer3 != null) {
            exoPlayer3.release();
            exoPlayer3 = null;
        }
    }

    public void pauseVideoPlay() {

        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.getPlaybackState();
        }else if (exoPlayer2 != null) {
            exoPlayer2.setPlayWhenReady(false);
            exoPlayer2.getPlaybackState();

        }else if (exoPlayer3 != null) {
            exoPlayer3.setPlayWhenReady(false);
            exoPlayer3.getPlaybackState();
        }
    }
}
