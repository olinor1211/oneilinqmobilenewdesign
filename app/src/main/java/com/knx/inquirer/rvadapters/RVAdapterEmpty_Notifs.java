package com.knx.inquirer.rvadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentNotification;

public class RVAdapterEmpty_Notifs extends RecyclerView.Adapter<RVAdapterEmpty_Notifs.ViewHolder> {

    private FragmentNotification fragment;
    private String err_msg;

    public RVAdapterEmpty_Notifs(FragmentNotification fragment, String err_msg) {
        this.fragment = fragment;
        this.err_msg = err_msg;
    }

    @Override
    public RVAdapterEmpty_Notifs.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_empty, parent, false);
        RVAdapterEmpty_Notifs.ViewHolder viewHolder = new RVAdapterEmpty_Notifs.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapterEmpty_Notifs.ViewHolder holder, int position) {
        holder.error.setText(err_msg);
    }

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public Button retry;
        public TextView error;

        public ViewHolder(View itemView) {
            super(itemView);

            retry = itemView.findViewById(R.id.btnRetry);
            error = itemView.findViewById(R.id.tvErrorMsg);

            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.rvNotifs.setAdapter(null);
         
                }
            });


        }
    }
}
