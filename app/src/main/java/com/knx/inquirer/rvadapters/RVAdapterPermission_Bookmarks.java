package com.knx.inquirer.rvadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentBookmarks;

import org.greenrobot.eventbus.EventBus;

public class RVAdapterPermission_Bookmarks extends RecyclerView.Adapter<RVAdapterPermission_Bookmarks.ViewHolder> {

    private FragmentBookmarks fragment;

    public RVAdapterPermission_Bookmarks(FragmentBookmarks fragment) {
    this.fragment = fragment;
    }

    @Override
    public RVAdapterPermission_Bookmarks.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmarks_permission, parent, false);
        RVAdapterPermission_Bookmarks.ViewHolder viewHolder = new RVAdapterPermission_Bookmarks.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapterPermission_Bookmarks.ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public Button requestPermission;

        public ViewHolder(View itemView) {
            super(itemView);

            requestPermission = itemView.findViewById(R.id.btnRequest);

            requestPermission.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post("88");
                }
            });


        }
    }
}
