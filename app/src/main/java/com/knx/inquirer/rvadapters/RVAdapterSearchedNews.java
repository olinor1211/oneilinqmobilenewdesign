package com.knx.inquirer.rvadapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.slider.library.SliderLayout;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.knx.inquirer.SearchActivity;
import com.knx.inquirer.models.PostArticleSearch;
import com.knx.inquirer.models.SearchModel;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.R;
import com.knx.inquirer.utils.PlayerWebView;
import com.knx.inquirer.utils.SharedPrefs;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

//Recyclerview Adapter when app is online
public class RVAdapterSearchedNews extends RecyclerView.Adapter{
    private static final String TAG = "RVAdapterSearchedNews";

    public ArrayList<SearchModel> news;
    private Context context;
    private SharedPrefs sf;
    public String _id, section, photo, type;
    private List<String> imgItems;
    private AudioManager amanager;

    private int itemposition;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_IMAGE = 2;
    private final int VIEW_TYPE_VIDEO = 3;
    private final int VIEW_TYPE_CAROUSEL = 4;
    private final int VIEW_TYPE_IMAGE_DOUBLE = 5;
    private final int VIEW_TYPE_VIDEO_DOUBLE = 6;
    private final int VIEW_TYPE_VIDEO_ARTICLE = 7;
    private final int VIEW_TYPE_DM = 8;
    private final int VIEW_TYPE_INQPRIME = 9;

    //test
    private SimpleExoPlayer exoPlayer, exoPlayer2, exoPlayer3;

    public RVAdapterSearchedNews(Context context, ArrayList<SearchModel> news, SharedPrefs sf) {
        super();
        this.news = news;
        this.context = context;
        this.sf = sf;

//        if(!sf.getSectionName().equals("SEARCH NEWS") && !sf.getSectionName().equals("MY INQUIRER") ){
//            sf.setSectionNameTempContainer(sf.getSectionName());
//        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_row, parent, false);
            vh = new SearchItemViewHolder(v);  //calling instance of viewHolder
        }else if(viewType == VIEW_TYPE_IMAGE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_items, parent, false);
            vh = new SearchImageTypeViewHolder(v);
        }else if(viewType == VIEW_TYPE_VIDEO) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_list_items, parent, false);
            vh = new SearchVideoTypeViewHolder(v);
        }else if(viewType == VIEW_TYPE_CAROUSEL) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carousel_list_items, parent, false);
            vh = new SearchCarouselTypeViewHolder(v);
        }else if(viewType == VIEW_TYPE_IMAGE_DOUBLE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_double_list_items, parent, false);
            vh = new SearchImageDoubleViewHolder(v);
        }else if(viewType == VIEW_TYPE_VIDEO_DOUBLE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_double_list_items, parent, false);
            vh = new SearchVideoDoubleTypeViewHolder(v);
        }else if(viewType == VIEW_TYPE_VIDEO_ARTICLE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_article_list_items, parent, false);
            vh = new SearchVideoArticleTypeViewHolder(v);
        }else if(viewType == VIEW_TYPE_DM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_motion_list_items, parent, false);
            vh = new SearchDMTypeViewHolder(v);
        }else if(viewType == VIEW_TYPE_INQPRIME) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inqprime_list_row, parent, false);
            vh = new SearchInqPrimeTypeViewHolder(v);
        }else  {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_loading, parent, false);
            vh = new SearchProgressViewHolder(v);
        }
        return vh;
    }

    private boolean isValidated(String string){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
        }finally {
            if(string!=null) return true;  //Check if null
            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
        }
        return false;
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, int position) {
        //Passing data from URL
        _id = news.get(position).getId();
        photo = news.get(position).getImage();
        type = news.get(position).getType();

        //Showing data on the views
        if (holder instanceof SearchItemViewHolder || holder instanceof SearchVideoTypeViewHolder || holder instanceof SearchImageDoubleViewHolder || holder instanceof SearchVideoDoubleTypeViewHolder || holder instanceof SearchCarouselTypeViewHolder || holder instanceof  SearchVideoArticleTypeViewHolder || holder instanceof  SearchImageTypeViewHolder || holder instanceof SearchDMTypeViewHolder || holder instanceof SearchInqPrimeTypeViewHolder) {

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.inquirernewsinfo)
                    .error(R.drawable.inquirernewsinfo)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .priority(Priority.HIGH);

            switch (type) {
                case "0":
                case "1":
                    ( (SearchItemViewHolder) holder ).tag.setVisibility(View.GONE);
                    ( (SearchItemViewHolder) holder ).bookmark.setVisibility(View.GONE);
                    if (isValidated(news.get(position).getTitle())) ((SearchItemViewHolder) holder ).newstitle.setText(news.get(position).getTitle());//news.get(position).getTitle()
                    else ( (SearchItemViewHolder) holder ).newstitle.setText("");

                    if (isValidated(news.get(position).getPubdate())) ( (SearchItemViewHolder) holder ).newspubdate.setText(news.get(position).getPubdate()); //
                    else ( (SearchItemViewHolder) holder ).newspubdate.setText("");

                    ( (SearchItemViewHolder) holder ).newsphoto.setVisibility(View.VISIBLE);
                    if (isValidated(photo)){
                        Glide
                                .with(context)
                                .load(photo)
                                .error(R.drawable.inquirernewsinfo)
                                .placeholder(R.drawable.inquirernewsinfo)
                                .transition(withCrossFade())
                                .apply(options)
                                .into(( (SearchItemViewHolder) holder ).newsphoto);
                    ViewCompat.setTransitionName(( (SearchItemViewHolder) holder ).newsphoto, news.get(position).getId());
                    }else{
                        Glide
                        .with(context)
                        .load(R.drawable.inquirernewsinfo)
                        .into(( (SearchItemViewHolder) holder ).newsphoto);
                    }
                    break;

                case "2": //Image
                    if (isValidated(news.get(position).getTitle())) ( (SearchImageTypeViewHolder) holder ).textImageTitle.setText(news.get(position).getTitle());
                    else ( (SearchImageTypeViewHolder) holder ).textImageTitle.setText("");
                    //noTag
                    ((SearchImageTypeViewHolder) holder).textImageSponsored.setVisibility(View.GONE);

                    if(isValidated(photo)){
                        Glide
                                .with(context)
                                .load(photo)
                                .transition(withCrossFade())
                                .apply(options)
                                .into(( (SearchImageTypeViewHolder) holder ).imageOrdinaryPhoto);
                    }
                    break;

                case "3": //Video
                    if (isValidated(news.get(position).getTitle())) ( (SearchVideoTypeViewHolder) holder ).video3Title.setText(news.get(position).getTitle());
                    else ( (SearchVideoTypeViewHolder) holder ).video3Title.setText("");
                    //noTag
                    ( (SearchVideoTypeViewHolder) holder ).video3Sponsored.setVisibility(View.GONE);
//                if (news.get(position).getLabel().isEmpty()) {
//                    ( (TrendingVideoTypeViewHolder) holder ).video3Sponsored.setText("");
//                } else {
//                    ( (TrendingVideoTypeViewHolder) holder ).video3Sponsored.setText(news.get(position).getLabel());
//                }

                    BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                    TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                    exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

                    DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                    MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                    ( (RVAdapterTrending.TrendingVideoTypeViewHolder) holder ).newsVideo.setUseController(false);  //play, stop
                    ( (RVAdapterTrending.TrendingVideoTypeViewHolder) holder ).newsVideo.setPlayer(exoPlayer);
                    ( (RVAdapterTrending.TrendingVideoTypeViewHolder) holder ).newsVideo.setShowBuffering(true);

                    exoPlayer.prepare(mediaSource);
                    exoPlayer.setPlayWhenReady(true);
                    exoPlayer.setVolume(0f);
//                    exoPlayer.setRepeatMode(exoPlayer.REPEAT_MODE_ONE);
                    break;


                case "4":  // Carousel
                    String images = news.get(position).getImage();
                    String imagesUrl[] = images.split(",");
                    if (( (SearchCarouselTypeViewHolder)holder ).carouselView !=null ){

                        ImageListener imageListener = (position1, imageView) -> {
                            Picasso.get()
                                    .load(imagesUrl[position1])
                                    .noFade()
                                    .placeholder(R.drawable.inquirernewsinfo)
                                    .into(imageView);
                        };

                        ( (SearchCarouselTypeViewHolder)holder ).carouselView.setPageCount(imagesUrl.length);
                        ( (SearchCarouselTypeViewHolder)holder ).carouselView.setImageListener(imageListener);
//                    ( (TrendingCarouselTypeViewHolder)holder ).textCarouselTag.setText(news.get(position).getLabel());
                        //noTag
                        ( (SearchCarouselTypeViewHolder)holder ).textCarouselTag.setVisibility(View.GONE);

                        if(isValidated(news.get(position).getTitle()))( (SearchCarouselTypeViewHolder)holder ).textCarouselTitle.setText(news.get(position).getTitle());
                        else ((SearchCarouselTypeViewHolder)holder ).textCarouselTitle.setVisibility(View.GONE);
                    }
                    break;


                case "5": //ImageDouble
                    if(isValidated(news.get(position).getTitle())) ((SearchImageDoubleViewHolder) holder).textDoubleTitle.setText(news.get(position).getTitle());
                    else  ((SearchImageDoubleViewHolder) holder).textDoubleTitle.setText("");
                    //noTag
                    ((SearchImageDoubleViewHolder) holder).textDoubleSponsored.setVisibility(View.GONE);
                    if(isValidated(photo)){
                        Glide
                                .with(context)
                                .load(photo)
                                .transition(withCrossFade())
                                .apply(options)
                                .into(( (SearchImageDoubleViewHolder) holder ).imageDoubePhoto);
                    }
                    break;
                case "6": //VideoDouble height
                    if(isValidated(news.get(position).getTitle()))((SearchVideoDoubleTypeViewHolder) holder).video6Title.setText(news.get(position).getTitle());
                    else  ((SearchVideoDoubleTypeViewHolder) holder).video6Title.setText("");
                    //noTag
                    ((SearchVideoDoubleTypeViewHolder) holder).video6Sponsored.setVisibility(View.GONE);

                    BandwidthMeter bandwidthMeter2 = new DefaultBandwidthMeter();
                    TrackSelector trackSelector2 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter2));
                    exoPlayer2 = ExoPlayerFactory.newSimpleInstance(context, trackSelector2);

                    DefaultHttpDataSourceFactory dataSourceFactory2 = new DefaultHttpDataSourceFactory("exoplayer_video2");
                    MediaSource mediaSource2 = new ExtractorMediaSource.Factory(dataSourceFactory2).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                    ( (SearchVideoDoubleTypeViewHolder) holder ).newsVideoDouble.setUseController(false);  //play, stop
                    ( (SearchVideoDoubleTypeViewHolder) holder ).newsVideoDouble.setPlayer(exoPlayer2);
                    ( (SearchVideoDoubleTypeViewHolder) holder ).newsVideoDouble.setShowBuffering(true);

                    exoPlayer2.prepare(mediaSource2);
                    exoPlayer2.setPlayWhenReady(true);
                    exoPlayer2.setVolume(0f);
                    break;

                case "7": //Video Article
                    if (isValidated(news.get(position).getTitle()))( (SearchVideoArticleTypeViewHolder) holder ).video7Title.setText(news.get(position).getTitle());
                    else ( (SearchVideoArticleTypeViewHolder) holder ).video7Title.setText("");
                    //noTag
                    ( (SearchVideoArticleTypeViewHolder) holder ).video7Sponsored.setVisibility(View.GONE);

                    BandwidthMeter bandwidthMeter3 = new DefaultBandwidthMeter();
                    TrackSelector trackSelector3 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter3));
                    exoPlayer3 = ExoPlayerFactory.newSimpleInstance(context, trackSelector3);

                    DefaultHttpDataSourceFactory dataSourceFactory3 = new DefaultHttpDataSourceFactory("exoplayer_video3");
                    MediaSource mediaSource3 = new ExtractorMediaSource.Factory(dataSourceFactory3).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                    ( (SearchVideoArticleTypeViewHolder) holder ).newsVideoArticle.setUseController(false);  //play, stop
                    ( (SearchVideoArticleTypeViewHolder) holder ).newsVideoArticle.setPlayer(exoPlayer3);
                    ( (SearchVideoArticleTypeViewHolder) holder ).newsVideoArticle.setShowBuffering(true);

                    exoPlayer3.prepare(mediaSource3);
                    exoPlayer3.setPlayWhenReady(true);
                    exoPlayer3.setVolume(0f);
                    break;

                case "8": //Dailymotion
                    //test
                    String vidUrl = news.get(position).getImage();
//                    String vidUrl = "x7dtt4u"
                    if(isValidated(news.get(position).getTitle())) ( (SearchDMTypeViewHolder) holder ).textDmTitle.setText(news.get(position).getTitle());
                    else  ( (SearchDMTypeViewHolder) holder ).textDmTitle.setText("");

                    if(isValidated(news.get(position).getLabel()))( (SearchDMTypeViewHolder) holder ).textDmSponsored.setText(news.get(position).getLabel());
                    else  ( (SearchDMTypeViewHolder) holder ).textDmSponsored.setText("");

                    ((SearchDMTypeViewHolder) holder).dmPlayerWebView.showControls(false);
                    ((SearchDMTypeViewHolder) holder).dmPlayerWebView.mute();
                    ((SearchDMTypeViewHolder) holder).dmPlayerWebView.setPlayWhenReady(true);
                    ((SearchDMTypeViewHolder) holder).dmPlayerWebView.load(vidUrl);
                    break;

                case "9": //Type 9 - InqPrime
                    if(isValidated(news.get(position).getTitle()))  ( (SearchInqPrimeTypeViewHolder) holder ).newsTitleInqPrime.setText(news.get(position).getTitle());
                    else   ( (SearchInqPrimeTypeViewHolder) holder ).newsTitleInqPrime.setText("");

                    if(isValidated(news.get(position).getPubdate())) ( (SearchInqPrimeTypeViewHolder) holder ).newsPubdateInqPrime.setText(news.get(position).getPubdate());
                    else  ( (SearchInqPrimeTypeViewHolder) holder ).newsPubdateInqPrime.setText("");

                    if(isValidated(photo)){
                        Glide
                                .with(context)
                                .load(photo)
                                .transition(withCrossFade())
                                .apply(options)
                                .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                    }
                    else {
                        switch (sf.getLastSection()) {
                            case 2:
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirersports)
                                        .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                                break;
                            case 3:
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirerentertainment)
                                        .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                                break;
                            case 5:
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirertechnology)
                                        .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                                break;
                            case 6:
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirerglobalnation)
                                        .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                                break;
                            case 7:
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirerbusiness)
                                        .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                                break;

                            case 8:
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirerlifestyle)
                                        .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                                break;
                            case 13:
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquireropinion_blue)
                                        .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                                break;

                            default:
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirernewsinfo)
                                        .into(( (SearchInqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime);
                        }
                    }
                    break;
            }

        }else{
            ((SearchProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }
    @Override
    public int getItemCount() {
        return news.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (news.get(position) != null) {
            String type = news.get(position).getType();
            switch(type){
                case "0":
                case "1":
                    return VIEW_TYPE_ITEM;

                case "2":
                    return VIEW_TYPE_IMAGE;

                case "3":
                    return VIEW_TYPE_VIDEO;

                case "4":
                    return VIEW_TYPE_CAROUSEL;

                case "5":
                    return VIEW_TYPE_IMAGE_DOUBLE;

                case "6":
                    return VIEW_TYPE_VIDEO_DOUBLE;

                case "7":
                    return VIEW_TYPE_VIDEO_ARTICLE;

                case "8":
                    return VIEW_TYPE_DM;

                case "9":
                    return VIEW_TYPE_INQPRIME;

                default:
                    return -1;

            }

        }else {
            return VIEW_TYPE_LOADING;
        }
    }


    public class SearchImageTypeViewHolder extends RecyclerView.ViewHolder {
        private TextView textImageTitle, textImageSponsored;
        private ImageView imageOrdinaryPhoto;

        public SearchImageTypeViewHolder(@NonNull View itemView) {
            super(itemView);

            imageOrdinaryPhoto = itemView.findViewById(R.id.imageOrdinaryPhoto);
            textImageSponsored = itemView.findViewById(R.id.textImageSponsored);
            textImageTitle = itemView.findViewById(R.id.textImageTitle);

            itemView.setOnClickListener(v -> {
//                releasePlayer();

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    (( SearchActivity ) context).checkImageLink(link);

                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    public class SearchVideoTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideo;
        TextView video3Title, video3Sponsored;
//          RelativeLayout video3Layout;


        public SearchVideoTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideo = itemView.findViewById(R.id.news_video);
            this.video3Sponsored = itemView.findViewById(R.id.video3Sponsored);
            this.video3Title = itemView.findViewById(R.id.video3Title);
//            this.video3Layout = itemView.findViewById(R.id.rlVideo3);

            itemView.setOnClickListener(v -> {

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    ((SearchActivity) context).checkVideoLink(link);

                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    public class SearchVideoDoubleTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideoDouble;
        TextView video6Title, video6Sponsored;

        public SearchVideoDoubleTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideoDouble = itemView.findViewById(R.id.news_video6);
            this.video6Sponsored = itemView.findViewById(R.id.video6Sponsored);
            this.video6Title = itemView.findViewById(R.id.video6Title);

            itemView.setOnClickListener(v -> {

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    ((SearchActivity)context).checkVideoLink(link);

                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    public class SearchVideoArticleTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideoArticle;
        TextView video7Title, video7Sponsored;

        public SearchVideoArticleTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideoArticle = itemView.findViewById(R.id.news_video7);
            this.video7Sponsored = itemView.findViewById(R.id.video7Sponsored);
            this.video7Title = itemView.findViewById(R.id.video7Title);


            itemView.setOnClickListener(v -> {


                int n = getAdapterPosition();
                String videoUrl = news.get(n).getImage();

                if (Global.isNetworkAvailable(context)) {

                    ((SearchActivity)context).displayFullview(videoUrl);

//                         Toast.makeText(context, "Carousel Clicked" + position, Toast.LENGTH_SHORT).show();
                    if (!Global.isItemClicked) {
                        Global.isItemClicked = true;

//                        fragment.displayFullview(videoUrl);

                        //eventBus post  test:  article id: 43816
                        //  EventBus.getDefault().post(new PostArticleBreaking("43816"));
                        EventBus.getDefault().post(new PostArticleSearch(news.get(itemposition).getId()));

                    }
                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }


            });

        }
    }

    public class SearchCarouselTypeViewHolder extends RecyclerView.ViewHolder {

        private CarouselView carouselView;
        private TextView textCarouselTag, textCarouselTitle;

        public SearchCarouselTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.carouselView = itemView.findViewById(R.id.carouselView);
            this.textCarouselTag = itemView.findViewById(R.id.textCarouselTag);
            this.textCarouselTitle = itemView.findViewById(R.id.textCarouselTitle);

            carouselView.setImageClickListener(position -> {

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {
//                         Toast.makeText(context, "Carousel Clicked" + position, Toast.LENGTH_SHORT).show();
                    if (!Global.isItemClicked) {
                        Global.isItemClicked = true;
//                        EventBus.getDefault().post(new PostArticleBreaking("43816"));
//                        EventBus.getDefault().post(new PostArticleBreaking(news.get(itemposition).getId()));
                    }
                } else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });
        }
    }

    public class SearchImageDoubleViewHolder extends RecyclerView.ViewHolder {
        private TextView textDoubleTitle, textDoubleSponsored;
        private ImageView imageDoubePhoto;

        public SearchImageDoubleViewHolder(@NonNull View itemView) {
            super(itemView);

            imageDoubePhoto = itemView.findViewById(R.id.imageDoublePhoto);
            textDoubleSponsored = itemView.findViewById(R.id.textDoubleSponsored);
            textDoubleTitle = itemView.findViewById(R.id.textDoubleTitle);

            itemView.setOnClickListener(v -> {
//                releasePlayer();

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    ((SearchActivity)context).checkImageLink(link);

                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    public class SearchDMTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerWebView dmPlayerWebView;
        TextView textDmSponsored, textDmTitle;
        ImageView gradient;
        boolean mFullscreen = false;

        public SearchDMTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.dmPlayerWebView = itemView.findViewById(R.id.dmPlayerWebView);
            this.textDmSponsored = itemView.findViewById(R.id.textDmSponsored);
            this.textDmTitle = itemView.findViewById(R.id.textDmTitle);
            this.gradient = itemView.findViewById(R.id.imageDmGradient);


            gradient.setOnClickListener(v -> {

                int n = getAdapterPosition();
                String link = news.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    ((SearchActivity)context).checkVideoLink(link);

                } else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    //InqPrimeTypeViewHolder
        private class SearchInqPrimeTypeViewHolder extends RecyclerView.ViewHolder {
            //Views
            TextView newsTitleInqPrime, newsPubdateInqPrime, tagInqPrime;
            ImageView mPrimeTag, mImagePhotoInqPrime;

            public SearchInqPrimeTypeViewHolder(View itemView) {
                super(itemView);

                newsTitleInqPrime = itemView.findViewById(R.id.textTitleInqPrime);
                newsPubdateInqPrime = itemView.findViewById(R.id.textDatesInqPrime);
                mImagePhotoInqPrime = itemView.findViewById(R.id.imagePhotoInqPrime);
                mPrimeTag = itemView.findViewById(R.id.primeTag);
                tagInqPrime = itemView.findViewById(R.id.tvTagInqPrime);

                itemView.setOnClickListener(v -> {

                    if (Global.isNetworkAvailable(context)) {
                        Log.d(TAG, "SearchInqPrimeTypeViewHolder: Clicked type " + type);
                        itemposition = getAdapterPosition();
                        EventBus.getDefault().post(new PostArticleSearch(news.get(itemposition).getId()));

                    }else {
                        Global.ViewDialog alert = new Global.ViewDialog();
                        alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                    }
                });
            }
        }

    private class SearchProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        SearchProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.pLoadingMore);
        }
    }

    public class SearchItemViewHolder extends RecyclerView.ViewHolder {
        //Views
        public TextView newstitle, newspubdate, tag;
        public ImageView newsphoto,bookmark;
        public SliderLayout newsimageslider;
//        public VideoView newsvideo;

        public SearchItemViewHolder(View itemView) {
            super(itemView);

            newstitle = itemView.findViewById(R.id.textTitle);
            newspubdate = itemView.findViewById(R.id.textDetails);
            newsphoto = itemView.findViewById(R.id.imagePhoto);
            bookmark = itemView.findViewById(R.id.ivNewsListBookmark);
//            newsimageslider = itemView.findViewById(R.id.slider);
//            newsvideo = itemView.findViewById(R.id.videoNews);

            tag = itemView.findViewById(R.id.tvTag);

            itemView.setOnClickListener(v -> {
                itemposition = getAdapterPosition();
                if (!Global.isItemClicked) {
                    Global.isItemClicked = true;
                    if(isValidated(news.get(itemposition).getLink())){
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.get(itemposition).getLink()));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } else {
                        EventBus.getDefault().post(new PostArticleSearch(news.get(itemposition).getId()));
                    }
                }
            });

        }

    }

    public void releasePlayer() {

        if (exoPlayer != null || exoPlayer2 != null || exoPlayer3 != null) {
            exoPlayer.release(); exoPlayer2.release(); exoPlayer3.release();
            exoPlayer = null; exoPlayer2 = null; exoPlayer3 = null;
        }
//        viewHolderParent = null;
    }

    public void pauseVideoPlay() {

        if (exoPlayer != null || exoPlayer2 != null || exoPlayer3 != null) {
            exoPlayer.setPlayWhenReady(false); exoPlayer2.setPlayWhenReady(false); exoPlayer3.setPlayWhenReady(false);
            exoPlayer.getPlaybackState(); exoPlayer2.getPlaybackState(); exoPlayer3.getPlaybackState();
        }
    }
}
