package com.knx.inquirer.rvadapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.knx.inquirer.constants.Constants;
import com.knx.inquirer.WebNewsActivity;
import com.knx.inquirer.models.DatabaseModel;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentBreaking;
import com.knx.inquirer.utils.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.PlayerListener;
import tcking.github.com.giraffeplayer2.VideoView;
import tv.danmaku.ijk.media.player.IjkTimedText;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class RVAdapterOffline extends RecyclerView.Adapter {

    private String photoStr = "";
    private String titleStr = "";
    private String contentStr = "";
    private String pubdateStr = "";
    private String linkStr = "";
    private String authorStr = "";
    private String voteStr = "";

    private AudioManager amanager;

    private int sect;

    public List<DatabaseModel> newsoffline;
    private FragmentBreaking fragment;
    private Context context;
    private SharedPrefs sf;
    public String _id, section, photo, type;
    private List<String> imgItems;
    private int itemposition;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;


    public RVAdapterOffline(Context context, List<DatabaseModel> newsoffline, FragmentBreaking fragment, SharedPrefs sf) {
        super();
        this.newsoffline = newsoffline;
        this.context = context;
        this.fragment = fragment;
        this.sf = sf;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_TYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_row, parent, false);
            vh = new ItemViewHolder(v);

        }else{

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,  int position) {

        if(position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if(holder instanceof ItemViewHolder) {
            //Passing data from URL
            _id = newsoffline.get(position).getNewsid();
            section = newsoffline.get(position).getSection();
            photo = newsoffline.get(position).getPhoto();
            type = newsoffline.get(position).getNewstype();

            //Showing data on the views

//            ((ItemViewHolder) holder).article_link.setText(news.get(position).link);

            if(!newsoffline.get(position).getTitle().equals("null") || !newsoffline.get(position).getTitle().equals("")) {
                ((ItemViewHolder) holder).newstitle.setText(newsoffline.get(position).getTitle());
            }else{
                ((ItemViewHolder) holder).newstitle.setText("");
            }

            if(!newsoffline.get(position).getPubdate().equals("null") || !newsoffline.get(position).getPubdate().equals("")) {
                ((ItemViewHolder) holder).newspubdate.setText(newsoffline.get(position).getPubdate());
            }else{
                ((ItemViewHolder) holder).newspubdate.setText("");
            }
            switch (section){

                case "1":  sect = R.drawable.inquirernewsinfo;
                    break;
                case "2":  sect = R.drawable.inquirersports;
                    break;
                case "3":  sect = R.drawable.inquirerentertainment;
                    break;

                case "5":  sect = R.drawable.inquirertechnology;
                    break;
                case "6":  sect = R.drawable.inquirerglobalnation;
                    break;
                case "7":  sect = R.drawable.inquirerbusiness;
                    break;
                case "8":  sect = R.drawable.inquirerlifestyle;
                    break;

                default: sect = R.drawable.inquirernewsinfo;
            }
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.inquirernewsinfo)
                    .error(R.drawable.inquirernewsinfo)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .priority(Priority.HIGH);

            RequestOptions options_ads = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.inquirernewsinfo)
                    .error(R.drawable.inquirernewsinfo)
                    //.skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .priority(Priority.HIGH);

            switch (type) {
                case "0":
                case "1":
                case "2":  //Type = Image, GIF, 2in1

                    ((ItemViewHolder) holder).newsphoto.setVisibility(View.VISIBLE);
                    ((ItemViewHolder) holder).adscraper.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).newsimageslider.setVisibility(View.INVISIBLE);
                    ((ItemViewHolder) holder).newsvideo.setVisibility(View.INVISIBLE);

                    if (!photo.equals("")) {
                        //Load images into ImageView
                        Glide
                                .with(context)
                                .load(photo)
                                .transition(withCrossFade())
                                .apply(options)
                                .into( ((ItemViewHolder) holder).newsphoto);

                        ViewCompat.setTransitionName( ((ItemViewHolder) holder).newsphoto, newsoffline.get(position).getNewsid());

                    } else {

                        switch (section) {
                            case "1":
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirernewsinfo)
                                        .into(((ItemViewHolder) holder).newsphoto);
                                break;
                            case "2":
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirersports)
                                        .into(((ItemViewHolder) holder).newsphoto);
                                break;
                            case "3":
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirerentertainment)
                                        .into(((ItemViewHolder) holder).newsphoto);
                                break;
                            case "5":
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirertechnology)
                                        .into(((ItemViewHolder) holder).newsphoto);
                                break;
                            case "6":
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirerglobalnation)
                                        .into(((ItemViewHolder) holder).newsphoto);
                                break;
                            case "7":
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirerbusiness)
                                        .into(((ItemViewHolder) holder).newsphoto);
                                break;

                            case "8":
                                Glide
                                        .with(context)
                                        .load(R.drawable.inquirerlifestyle)
                                        .into(((ItemViewHolder) holder).newsphoto);
                                break;
                        }
                    }

                    break;

                case "3": //Video

                    amanager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

                    ((ItemViewHolder) holder).newsvideo.setVisibility(View.VISIBLE);
                    ((ItemViewHolder) holder).adscraper.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).newsimageslider.setVisibility(View.INVISIBLE);
                    ((ItemViewHolder) holder).newsphoto.setVisibility(View.INVISIBLE);

                    ((ItemViewHolder) holder).newsvideo.setVideoPath(photo).setFingerprint(position);
                    ((ItemViewHolder) holder).newsvideo.getVideoInfo().setTitle(newsoffline.get(position).getTitle());
                    ((ItemViewHolder) holder).newsvideo.getVideoInfo().setPortraitWhenFullScreen(false);
                    ((ItemViewHolder) holder).newsvideo.getPlayer().start();

                    ((ItemViewHolder) holder).newsvideo.setPlayerListener(new PlayerListener() {
                        @Override
                        public void onPrepared(GiraffePlayer giraffePlayer) {
                            int dur = ((ItemViewHolder) holder).newsvideo.getPlayer().getDuration();
                            amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                            Log.e("DURATION", String.valueOf(dur));

                        }

                        @Override
                        public void onBufferingUpdate(GiraffePlayer giraffePlayer, int percent) {

                        }

                        @Override
                        public boolean onInfo(GiraffePlayer giraffePlayer, int what, int extra) {
                            return false;
                        }

                        @Override
                        public void onCompletion(GiraffePlayer giraffePlayer) {

                        }

                        @Override
                        public void onSeekComplete(GiraffePlayer giraffePlayer) {

                        }

                        @Override
                        public boolean onError(GiraffePlayer giraffePlayer, int what, int extra) {
                            return false;
                        }

                        @Override
                        public void onPause(GiraffePlayer giraffePlayer) {

                            Date currtime = Calendar.getInstance().getTime();

                            int curr = ((ItemViewHolder) holder).newsvideo.getPlayer().getCurrentPosition();

                            Log.e("CURRPOSITION", String.valueOf(curr));
                            Log.e("CURRTIME", String.valueOf(currtime));


                        }

                        @Override
                        public void onRelease(GiraffePlayer giraffePlayer) {

                        }

                        @Override
                        public void onStart(GiraffePlayer giraffePlayer) {
                            amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                        }


                        @Override
                        public void onTargetStateChange(int oldState, int newState) {

                        }

                        @Override
                        public void onCurrentStateChange(int oldState, int newState) {

                        }

                        @Override
                        public void onDisplayModelChange(int oldModel, int newModel) {

                            newModel = ((ItemViewHolder) holder).newsvideo.getPlayer().getDisplayModel();

                            Log.e("MODEL", String.valueOf(newModel));

                            if(newModel == 1){

                                amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);

                            }else{

                                amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);

                            }

//                            if(newModel == 1){
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//                                    amanager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
//                                } else {
//                                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
//                                }
//                            }else{
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//                                    amanager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, 0);
//                                } else {
//                                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
//                                }
//                            }

                        }

                        @Override
                        public void onPreparing(GiraffePlayer giraffePlayer) {

                        }

                        @Override
                        public void onTimedText(GiraffePlayer giraffePlayer, IjkTimedText text) {

                        }

                        @Override
                        public void onLazyLoadProgress(GiraffePlayer giraffePlayer, int progress) {

                        }

                        @Override
                        public void onLazyLoadError(GiraffePlayer giraffePlayer, String message) {

                        }
                    });
                    break;

                case "4":  // Multi-image

                    ((ItemViewHolder) holder).newsimageslider.setVisibility(View.VISIBLE);
                    ((ItemViewHolder) holder).adscraper.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).newsphoto.setVisibility(View.INVISIBLE);
                    ((ItemViewHolder) holder).newsvideo.setVisibility(View.INVISIBLE);

                    //Store images from URL into list
                    imgItems = Arrays.asList(photo.split(","));

                    HashMap<String, String> url_imgs = new HashMap<String, String>();

                    int y = 0;


                    for (int i = 0; i < imgItems.size(); i++) {

                        y = y + 1;
                        if (url_imgs.size() <= imgItems.size())
                            url_imgs.put("image" + y, imgItems.get(i));

                    }

                    for (String name : url_imgs.keySet()) {

                        DefaultSliderView textSliderView = new DefaultSliderView(context);

                        textSliderView
                                .image(url_imgs.get(name))
                                .setScaleType(BaseSliderView.ScaleType.Fit);

                        textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {

                                itemposition = position;

                                if (!Global.isItemClicked) {
                                    Global.isItemClicked = true;
                                    fetchArticle(itemposition);
                                }
                            }
                        });

                        ((ItemViewHolder) holder).newsimageslider.addSlider(textSliderView);
                    }

                    if (imgItems.size() == 1) {
                        ((ItemViewHolder) holder).newsimageslider.stopAutoCycle();
                        ((ItemViewHolder) holder).newsimageslider.getPagerIndicator().setVisibility(View.GONE);
                        ((ItemViewHolder) holder).newsimageslider.setPagerTransformer(false, new BaseTransformer() {
                            @Override
                            protected void onTransform(View view, float v) {
                            }
                        });

                    }

                    ((ItemViewHolder) holder).newsimageslider.setPresetTransformer(SliderLayout.Transformer.Default);

                    break;

                case "5":  //Two-row images

                    ((ItemViewHolder) holder).adscraper.setVisibility(View.VISIBLE);
                    ((ItemViewHolder) holder).newsphoto.setVisibility(View.INVISIBLE);
                    ((ItemViewHolder) holder).newsimageslider.setVisibility(View.INVISIBLE);
                    ((ItemViewHolder) holder).newsvideo.setVisibility(View.INVISIBLE);

                    Glide
                            .with(context)
                            .load(photo)
                            .transition(withCrossFade())
                            .apply(options_ads)
                            .into( ((ItemViewHolder) holder).adscraper);

                    break;

            }

            //Show the SPONSORED tag
            if (!type.equals("0") && !newsoffline.get(position).getLabel().equals("")) {

                ((ItemViewHolder) holder).tag.setVisibility(View.VISIBLE);
                ((ItemViewHolder) holder).tag.setText(newsoffline.get(position).getLabel());

            } else {
                ((ItemViewHolder) holder).tag.setVisibility(View.INVISIBLE);
            }


        }else{
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return newsoffline.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return newsoffline == null ? 0 : newsoffline.size();
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }


    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        //Views
        public TextView newstitle, newspubdate, tag;
        public ImageView newsphoto, adscraper;
        public SliderLayout newsimageslider;
        public VideoView newsvideo;

        public ItemViewHolder(final View itemView) {
            super(itemView);

            newstitle = itemView.findViewById(R.id.textTitle);
            newspubdate = itemView.findViewById(R.id.textDetails);
            newsphoto = itemView.findViewById(R.id.imagePhoto);
//            adscraper = itemView.findViewById(R.id.imageSkyScraper);
//            newsimageslider = itemView.findViewById(R.id.slider);
//            newsvideo = itemView.findViewById(R.id.videoNews);

            tag = itemView.findViewById(R.id.tvTag);


            itemView.setOnClickListener(v -> {
                itemposition = getAdapterPosition();

                Log.d("ITEM_TYPE_OFFLINE", newsoffline.get(itemposition).getNewstype());

                if (!Global.isItemClicked) {
                    Global.isItemClicked = true;

                    //if ad type is skyscraper
                    if(newsoffline.get(itemposition).getNewstype().equals("2") ||  newsoffline.get(itemposition).getNewstype().equals("3") || newsoffline.get(itemposition).getNewstype().equals("5"))
                    {
                        if(!newsoffline.get(itemposition).getNewslink().equals("")){
                            Global.isItemClicked = false;
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newsoffline.get(itemposition).getNewslink()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }else{
                            Global.isItemClicked = false;
//                            Toast.makeText(context, "No link to open", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        fetchArticle(itemposition);
                    }

                }
            });
        }

    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.pLoadingMore);
        }
    }


    private void fetchArticle(int pos){

    if (Global.haveConnected(context)) {

            fragment.onArticleRequest();

            StringRequest articleRequestOffline = new StringRequest(Request.Method.POST, Constants.BASE_URL + "article",
                    response -> {

                        fragment.onArticleRequestSuccess();

                        try {

                            JSONObject obj = new JSONObject(response);

                            if (obj.isNull("creative") && !obj.has("creative")) {
                                photoStr = "";
                            } else {
                                photoStr = obj.getString("creative");
                            }

                            if (obj.isNull("title") && !obj.has("title")) {
                                titleStr = "";
                            } else {
                                titleStr = obj.getString("title");
                            }

                            if (obj.isNull("content") && !obj.has("content")) {
                                contentStr = "";
                            } else {
                                contentStr = obj.getString("content");
                            }

                            if (obj.isNull("pubdate") && !obj.has("pubdate")) {
                                pubdateStr = "";
                            } else {
                                pubdateStr = obj.getString("pubdate");
                            }

                            if (obj.isNull("byline") && !obj.has("byline")) {
                                authorStr = "";
                            } else {
                                authorStr = obj.getString("byline");
                            }

                            if (obj.isNull("link") && !obj.has("link")) {
                                linkStr = "";
                            } else {
                                linkStr = obj.getString("link");
                            }

                            Intent intent = new Intent(context, WebNewsActivity.class);

                            intent.putExtra("id", newsoffline.get(pos).getNewsid());
                            intent.putExtra("title", titleStr);
                            intent.putExtra("photo", photoStr);
                            intent.putExtra("type", newsoffline.get(pos).getNewstype());
                            intent.putExtra("pubdate", pubdateStr);
                            intent.putExtra("link", linkStr);
                            intent.putExtra("content1", contentStr);
                            intent.putExtra("author", authorStr);
                            intent.putExtra("vote", voteStr);

                            intent.putExtra("transname", newsoffline.get(pos).getNewsid());

                            context.startActivity(intent);

/*                            ActivityOptions options = null;

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                options = ActivityOptions.makeSceneTransitionAnimation(
                                        (AppCompatActivity) context, view, view.getTransitionName());
                            }

                            ActivityCompat.startActivity(context, intent, options.toBundle());

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                ((MainActivity)context).getWindow().setEnterTransition(null);
                            }*/


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    volleyError -> {

                        fragment.onArticleRequestError();

                    }) {
                @Override
                protected Map<String, String> getParams() {

                    preferences = PreferenceManager.getDefaultSharedPreferences(context);
                    editor = preferences.edit();


                    String toHash = Global.GenerateSHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megaarticle/" + newsoffline.get(pos).getNewsid());
                    String hashedKey = toHash.substring(0,32);

                    Map<String, String> params = new HashMap<>();

                    params.put("device", sf.getDeviceId());
                    params.put("key", sf.getDailyKey());
                    params.put("id", newsoffline.get(pos).getNewsid());
                    params.put("md5", hashedKey);

                    return params;
                }
            };

            RequestQueue articleRequest = Volley.newRequestQueue(context);
            articleRequest.add(articleRequestOffline);
            articleRequestOffline.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        }else{
        //fragment.onArticleRequestError();
        Global.ViewDialog alert = new Global.ViewDialog();
        alert.showDialog(context, "Unable to load article\nPlease check your network connection");

    }

    }


}
