package com.knx.inquirer.rvadapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.fragments.FragmentNotification;
import com.knx.inquirer.models.NotificationModel;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ValueAnimator;
import com.xiaofeng.flowlayoutmanager.cache.Line;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.reactivex.observers.TestObserver;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

//Recyclerview Adapter when to fetch notifications
public class RVAdapterNotification extends RecyclerView.Adapter {
    private static final String TAG = "RVAdapterNotification";

    public List<NotificationModel> notifs;
    private Context context;
    private FragmentNotification fragment;
    private DatabaseHelper helper;
    private final int VIEW_TYPE_ITEM = 0;

    public RVAdapterNotification(Context context, List<NotificationModel> notif, FragmentNotification fragment, DatabaseHelper helper) {
        super();
        this.notifs = notif;
        this.context = context;
        this.fragment = fragment;
        this.helper = helper;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_row, parent, false));
        } else {
            return new DeleteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.delete_notification_row, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof ViewHolder) {

            if (notifs.get(position).getReadstatus() == 0) {
                ((ViewHolder) holder).notifHolder.setBackgroundColor(context.getResources().getColor(R.color.notification_unread_color));
                ((ViewHolder) holder).newsdata.setTextColor(context.getResources().getColor(R.color.black));
                ((ViewHolder) holder).newsstamp.setTextColor(context.getResources().getColor(R.color.black));
//                ((ViewHolder) holder).notifHolder.setBackgroundColor(Color.parseColor("#C8F1F1F1"));
            } else {
//                ((ViewHolder) holder).notifHolder.setBackgroundColor(context.getResources().getColor(R.color.sports));
                ((ViewHolder) holder).notifHolder.setBackgroundColor(context.getResources().getColor(R.color.transparent_white));
            }

            //Showing data on the views
            String finaldata = notifs.get(position).getTitle().toUpperCase(); //notifs.get(position).getMessage();
            String image =  notifs.get(position).getImage();
            ((ViewHolder) holder).newsdata.setText(finaldata.replaceAll("\\n", ""));
            ((ViewHolder) holder).newsstamp.setText(Global.convertDateToString(notifs.get(position).getTimestamp())); // ((ViewHolder) holder).newsstamp.setText(notifs.get(position).getTimestamp());

//            //RESOLVED:Hide/Unhide BottomNavBar Controller
//            if(Global.isNotifItemDeleted){
//                Global.isNotifItemDeleted = false;
//                if(Global.isNotifItemScrollable){
//                    Log.d(TAG, "onBindViewHolder: showBottomNav ");
//                    EventBus.getDefault().post("showBotNav");
//                }
//            }

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.inquirernewsinfo)
                    .error(R.drawable.inquirernewsinfo)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .transform(new MultiTransformation<>(new CenterCrop()));

//                    .transform(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(10)));

            if (isValidated(image)) { //if (!image.isEmpty() && image!=null) {
                ((ViewHolder) holder).ivNotification.setVisibility(View.VISIBLE);

//                Glide
//                        .with(context)
//                        .load(image)
////                        .fitCenter()//fit image then center
////                        .centerCrop()// crop image then center
////                        .transition(withCrossFade())
////                        .apply(options)
//                        .into(( (ViewHolder) holder ).ivNotification);

                //
                Glide
                        .with(context)
                        .load(image)
                        .thumbnail(0.05f)
                        .apply(options)
                        .into(((ViewHolder) holder ).ivNotification);
//                    .transition(withCrossFade())

            }else {
                ((ViewHolder) holder).ivNotification.setVisibility(View.GONE);
                Log.d(TAG, "onBindViewHolder: No notification image ");
            }

            holder.itemView.setOnClickListener(v -> {

                if (Global.isNetworkAvailable(context)) {
                    //AVOID MULTIPLE-CLICKING
                    if (( SystemClock.elapsedRealtime() - Global.lastClickTime) < 1000) return;
                    Global.lastClickTime =  SystemClock.elapsedRealtime();

                    //call direct the helper
//                    helper.updateNotificationStatus(1, notifs.get(position).getId());  //notification to read status
                    helper.updateNotificationStatus2(1, notifs.get(position).getNews_id());  //notification to read status
                    Log.d(TAG, "onBindViewHolder:  helper.updateNotificationStatus " + notifs.get(position).getId());

                    if (isValidated(notifs.get(position).getLink())) { //if (!notifs.get(position).getLink().isEmpty() && notifs.get(position).getLink()!=null) {
                        if (notifs.get(position).getLink().contains("http")) {

                            Intent intent = new Intent();
                            intent.putExtra("url", notifs.get(position).getLink());
                            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
                            context.startActivity(intent);
                        }
                        else if (!notifs.get(position).getLink().contains("inq")) {
                            Log.d(TAG, "onBindViewHolder:  not contains(\"inq\") called ");
                            fragment.linkError();
                        }
                        else if (notifs.get(position).getLink().substring(0, 3).equals("inq")) {
                            Log.d(TAG, "onBindViewHolder: equals(\"inq\") called ");
                            String articleID = notifs.get(position).getLink().substring(notifs.get(position).getLink().lastIndexOf("/") + 1);
                            fragment.viewArticle(articleID);
                        }
                        else{
                            Log.d(TAG, "onBindViewHolder: linkError called ");
                            fragment.linkError();
                        }
                    }else{
                        Log.d(TAG, "onBindViewHolder: No PrimeNotification Link ");
                    }



                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });


        }else{
            ((DeleteViewHolder) holder).deleteHolder.setOnClickListener(v -> {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Would you like to remove all notifications?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                fragment.deleteNotifs();
                            }
                        })
                        .setNegativeButton("No", (dialog, which) -> {
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        int VIEW_TYPE_DELETE = 1;
        return (position == notifs.size()) ? VIEW_TYPE_DELETE : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return notifs.size() + 1;
    }
//    public int getItemCount() {
//        return notifs.size();
//    }


    //
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public TextView newsdata, newsstamp;
        public ConstraintLayout notifHolder;
        public ImageView ivNotification;

        public ViewHolder(final View itemView) {
            super(itemView);
            notifHolder = itemView.findViewById(R.id.notifHolder);
            newsdata = itemView.findViewById(R.id.txtdata);
            newsstamp = itemView.findViewById(R.id.txtstamp);
            ivNotification = itemView.findViewById(R.id.ivNotification);
        }
    }

    //
    public class DeleteViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout deleteHolder;

        public DeleteViewHolder(View itemView) {
            super(itemView);
            deleteHolder = itemView.findViewById(R.id.deleteHolder);
        }
    }

    private boolean isValidated(String string){

        try{
            if(!string.isEmpty()) return true;
        }catch(Exception e){
            Log.d(TAG, "isValidated: Exception_Error: "+e.getMessage());
        }finally {
            if(string!=null) return true;  //Check if null
            Log.d(TAG, "isValidated: Exception_Error Finally_Executed** ");
        }
        return false;
    }


}