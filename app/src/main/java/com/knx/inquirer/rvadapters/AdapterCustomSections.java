package com.knx.inquirer.rvadapters;


import android.content.Context;
import android.media.AudioManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.fragments.FragmentCustom;
import com.knx.inquirer.models.Option;
import com.knx.inquirer.models.PostPodcastWebNewsAct;
import com.knx.inquirer.models.PostUIUpdate;
import com.knx.inquirer.R;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.SharedPrefs;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;


public class AdapterCustomSections extends RecyclerView.Adapter {
    private static final String TAG = "AdapterCustomSections";

    private ArrayList<Option> sections;
    private Context context;
    private FragmentCustom fragment;
    private SharedPrefs sf;

    //test
    private RecyclerView recyclerView;

    private final int VIEW_TYPE_ITEM = 0;

    String id;

    private int selectedPosition = 0;
    private int tmp_last_pos;

    public AdapterCustomSections(Context context, ArrayList<Option> sections, FragmentCustom fragment, SharedPrefs sf) {
        super();
        this.sections = sections;
        this.context = context;
        this.fragment = fragment;
        this.sf = sf;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.tags_layout, parent, false));
        } else {
            //automatically add the edit view at the last  ...cause of IF/ELSE
            return new EditViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.tag_edit, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).title.setText(sections.get(position).getTitle());

            holder.setIsRecyclable(false);

            sections.get(sf.getLastCustomSectionPosition()).setSelected(true);

            //DEFAULT: the BACKGROUND of the TAG/SECTION
            ((ItemViewHolder) holder).title.setBackgroundResource(sections.get(position).isSelected() ? R.drawable.tag_active : R.drawable.tags);


            //TAGS/SECTION ITEM
            ((ItemViewHolder) holder).title.setOnClickListener(v -> {

                //for mobility.. pass the URL
                String url = sections.get(position).getUrl();
                sf.setSectionUrl(url);
                int n = sections.get(position).getId();   //Section_ID


//                int section_id = sections.get(position).getId(); //Section_ID
//                sf.setSectionMenuID(n); //new assignment
                sf.setLastSectionCustom(n); //new assignment

//                sf.setLastSection(section_id); //Update LastSection

                Log.d(TAG, "onBindViewHolder: LAST_SECTION " + sf.getLastSectionCustom());
                Log.d(TAG, "onBindViewHolder: sectionsName " + sections.get(position).getTitle());

                if (Global.isNetworkAvailable(context)) {

//                    fragment.removeAd();  //Redundant: has inside the checkAd2 removeAd to clear the ImageView
                    fragment.checkAd2(String.valueOf(n));
                    Log.d(TAG, "onBindViewHolder: DebugStopper ");

                    if (!sections.get(position).isSelected()) {

                        //podcast REMOVING the PLAYBACK_BUTTON
//                        removeStopPodcastAudio();
                        //
                        EventBus.getDefault().post(new PostUIUpdate("dismiss_myinq"));

                        if (sf.getLastCustomSectionPosition() == -1) {
                            sections.get(0).setSelected(false);
                            sf.setLastCustomSectionPosition(0);  //index
                        } else {
                            sections.get(sf.getLastCustomSectionPosition()).setSelected(false);
                        }

                        sections.get(position).setSelected(true);

                        sections.get(sf.getLastCustomSectionPosition()).setSelected(false);

                        tmp_last_pos = sf.getLastCustomSectionPosition(); //index

                        sf.setLastCustomSectionPosition(position);  //index

                        notifyDataSetChanged();

                        //CHANGE the BACKGROUND of the TAG/SECTION
                        ((ItemViewHolder) holder).title.setBackgroundResource(sections.get(position).isSelected() ? R.drawable.tag_active : R.drawable.tags);

                        //by Section Filter  //for mobility..pass also the URL
//                        fragment.ChangeSection(n, tmp_last_pos, url); //NO MORE SECTIONS HERE
//                        fragment.ChangeSection(sections.get(position).getId(), tmp_last_pos);

                        Log.d(TAG, "onBindViewHolder: DebugStopper ChangeSection ");

                    }

                } else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });


        } else {

            //Edit item inside sectionsMenu
            ((EditViewHolder) holder).editPrefs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment.callEditPrefs();
                }
            });
        }

    }

    public void removeStopPodcastAudio(){
        EventBus.getDefault().removeStickyEvent(PostPodcastWebNewsAct.class);  //for WebNewsAct
        EventBus.getDefault().post("removePlaybackButton");  //for FragmentMyInq Page

        ((AudioManager) context.getSystemService(
                context.AUDIO_SERVICE)).requestAudioFocus(
                null,
                AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
    }

    public void refreshSection(){
        sections.get(0).setSelected(false);
        for (Option section: sections) {
            if(section.isSelected())
                section.setSelected(false);
        }
        sections.get(tmp_last_pos).setSelected(true);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        int VIEW_TYPE_EDIT = 1;
        return (position == sections.size()) ? VIEW_TYPE_EDIT : VIEW_TYPE_ITEM;
    }

    //Note:  + 1 is for the addition EditViewHolder for edit
    @Override
    public int getItemCount() {
        return sections.size() + 1;
    }



    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ConstraintLayout cl;

        //Initializing Views
        public ItemViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.tvtag);
            cl = itemView.findViewById(R.id.tagholder);

        }

    }

    private class EditViewHolder extends RecyclerView.ViewHolder {
        public TextView editPrefs;

        public EditViewHolder(View itemView) {
            super(itemView);
            editPrefs = itemView.findViewById(R.id.tveditprefs);

        }
    }

}
