package com.knx.inquirer.rvadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentBookmarks;

public class RVAdapterEmpty_Bookmarks extends RecyclerView.Adapter<RVAdapterEmpty_Bookmarks.ViewHolder> {

    private FragmentBookmarks fragment;

    public RVAdapterEmpty_Bookmarks(FragmentBookmarks fragment) {
    this.fragment = fragment;
    }

    @Override
    public RVAdapterEmpty_Bookmarks.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmarks_empty, parent, false);
        RVAdapterEmpty_Bookmarks.ViewHolder viewHolder = new RVAdapterEmpty_Bookmarks.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapterEmpty_Bookmarks.ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
