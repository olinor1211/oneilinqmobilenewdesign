package com.knx.inquirer.rvadapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.models.TagsModel;

import java.util.List;


public class AdapterPreferedTags extends RecyclerView.Adapter<AdapterPreferedTags.ViewHolder> {
    private static final String TAG = "AdapterPreferedTags";

    private List<TagsModel> tagsList;
    //ArrayList<String> tags;
    Context context;

    private int selectedPosition = -1;

    public AdapterPreferedTags(Context context, List<TagsModel> tags) {
        super();
        this.tagsList = tags;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pref_tags_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final TagsModel model = tagsList.get(position); //SHortCut

        //Show all sections EXCEPT with ID's 20 above
        holder.title.setVisibility(model.getIsHidden().equals("Y")?View.GONE:View.VISIBLE); // holder.title.setVisibility(model.getId()>=20?View.GONE:View.VISIBLE);
        holder.title.setText(model.getTitle()); //holder.title.setText(model.getTitle());
        //
        holder.title.setBackgroundResource(model.isSelected() ? R.drawable.pref_tag_active : R.drawable.pref_tags);

        holder.setIsRecyclable(false);  //to tell the RecyclerView not to recycler...so it is much faster to load

        holder.title.setOnClickListener(v -> {
            model.setSelected(!model.isSelected());  //Setting the RVList item Selected or unSelected by Negating the current state
            holder.title.setBackgroundResource(model.isSelected() ? R.drawable.pref_tag_active : R.drawable.pref_tags);
            Log.d(TAG, "onBindViewHolder: toggle SECTION_TAG_SELECTED: T/F : " + model.isSelected());
        });
    }

    @Override
    public int getItemCount() {
        return tagsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ConstraintLayout cl;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvtag);
            cl = itemView.findViewById(R.id.tagholder);
        }
    }

}
