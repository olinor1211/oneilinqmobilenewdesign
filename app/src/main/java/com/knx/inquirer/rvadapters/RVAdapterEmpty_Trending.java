package com.knx.inquirer.rvadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentBreaking;
import com.knx.inquirer.fragments.FragmentTrending;

public class RVAdapterEmpty_Trending extends RecyclerView.Adapter<RVAdapterEmpty_Trending.ViewHolder> {

    private FragmentTrending fragment;
    private String err_msg;

    public RVAdapterEmpty_Trending(FragmentTrending fragment, String err_msg) {
        this.fragment = fragment;
        this.err_msg = err_msg;
    }

    @Override
    public RVAdapterEmpty_Trending.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_empty, parent, false);
        RVAdapterEmpty_Trending.ViewHolder viewHolder = new RVAdapterEmpty_Trending.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapterEmpty_Trending.ViewHolder holder, int position) {

        holder.error.setText(err_msg);
    }

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public Button retry;
        public TextView error;

        public ViewHolder(View itemView) {
            super(itemView);

            retry = itemView.findViewById(R.id.btnRetry);
            error = itemView.findViewById(R.id.tvErrorMsg);

            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.RetryFetchTrending();
                }
            });


        }
    }
}
