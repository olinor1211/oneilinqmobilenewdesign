package com.knx.inquirer.rvadapters;


import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentBreaking;
import com.knx.inquirer.fragments.FragmentCustom;
import com.knx.inquirer.models.PostPodcastWebNewsAct;
import com.knx.inquirer.models.PostUIUpdate;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.SharedPrefs;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class AdapterSections extends RecyclerView.Adapter<AdapterSections.ViewHolder> {

    private static final String TAG = "AdapterSections";
    private ArrayList<Section> sections;
    private FragmentBreaking fragment;
    private FragmentCustom fragmentcustom;
    private SharedPrefs sf;
    private Context context;
    private int tmp_last_pos;
    private String newColor;

    public AdapterSections(Context context, ArrayList<Section> sections, FragmentBreaking fragment, SharedPrefs sf) {
        super();
        this.context = context;
        this.sections = sections;
        this.fragment = fragment;
        this.sf = sf;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tags_layout, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: NOTIFIED**** ");
        //NOTE: sf.getLastSectionPosition() is the INDEX of the RVList
        String sectionName = sections.get(position).getTitle().trim();
        String color = sections.get(position).getColor().trim();
        //
        holder.title.setText(sectionName.toUpperCase());

        //RVList will not be Recycled, always displayed like ListView
        //Remove: holder.setIsRecyclable(false): This is the CAUSE of OVERLAPPING because the CHANGES in the Item is being DELAYED

        //Default to ACTIVE Section_Tag  if ID is equal to -1 or Make it ACTIVE the last ID that has been click by MAKING the isSELECTED to TRUE
        if (sf.getLastSectionPosition() == -1) {
            sections.get(0).setSelected(true);
        } else {
            sections.get(sf.getLastSectionPosition()).setSelected(true);  //ACTIVE Section_Tag
        }

//        if(){
//
//        }






        //CHANGED BackGround Color
        //CHANGED Section_Tag COLOR to ACTIVE or NOT_ACTIVE. IF isSelected() is TRUE
        if(sections.get(position).isSelected()){
            holder.underline.setVisibility(View.VISIBLE);

//            switch (sectionName) {
//
//                case "Property":
//                    newColor = colorLighten(Global.sectionProperty);
//                    break;
//                case "Entertainment":
//                    newColor = colorLighten(Global.sectionEntertainment);
//                    break;
//                case "Lifestyle":
//                    newColor = colorLighten(Global.sectionLifestyle);
//                    break;
//                case "Global News":
//                    newColor = colorLighten(Global.sectionGlobalNews);
//                    break;
//                case "Sports":
//                    newColor = colorLighten(Global.sectionSports);
//                    break;
//                case "Technology":
//                    newColor = colorLighten(Global.sectionTechnology);
//                    break;
//                case "Business":
//                    newColor = colorLighten(Global.sectionBusiness);
//                    break;
//                case "Opinion":
//                    newColor = colorLighten(Global.sectionOpinion);
//                    break;
//                default:
//                    newColor = colorLighten(Global.sectionNews);
//                    break;
//            }
//
            newColor = colorLighten(color);
            holder.title.getBackground().setColorFilter(Color.parseColor(newColor), PorterDuff.Mode.SRC_ATOP); //OK
            holder.underline.getBackground().setColorFilter(Color.parseColor(newColor), PorterDuff.Mode.SRC_ATOP);
            Log.d(TAG, "onBindViewHolder: NOTIFIED**** TRUE "+ sectionName+" - LastSectionPosition: "+sf.getLastSectionPosition());
        }
        else {
            holder.underline.setVisibility(View.INVISIBLE);
            holder.title.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP); //OK

            //Change ColorBackgroundTint Temporary OK
//
//            switch (sectionName) {
//                case "Property":
//                    //Convert String to int Color: Color:Global.sectionProperty should be from API
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionProperty), PorterDuff.Mode.SRC_ATOP); //OK
//                    //                ViewCompat.setBackgroundTintList(holder.title, ColorStateList.valueOf(context.getResources().getColor(R.color.property)));  //IntColor: OK
//                    break;
//                case "Entertainment":
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionEntertainment), PorterDuff.Mode.SRC_ATOP); //OK
//                    //                ViewCompat.setBackgroundTintList(holder.title, ColorStateList.valueOf(context.getResources().getColor(R.color.entertainment))); //IntColor: OK
//                    break;
//                case "Lifestyle":
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionLifestyle), PorterDuff.Mode.SRC_ATOP);
//                    break;
//                case "Global News":
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionGlobalNews), PorterDuff.Mode.SRC_ATOP);
//                    break;
//                case "Sports":
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionSports), PorterDuff.Mode.SRC_ATOP);
//                    break;
//                case "Technology":
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionTechnology), PorterDuff.Mode.SRC_ATOP); //OK
//                    break;
//                case "Business":
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionBusiness), PorterDuff.Mode.SRC_ATOP); //OK
//                    break;
//                case "Opinion":
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionOpinion), PorterDuff.Mode.SRC_ATOP); //OK
//                    break;
//                default:
//                    holder.title.getBackground().setColorFilter(Color.parseColor(Global.sectionNews), PorterDuff.Mode.SRC_ATOP); //OK
//                    break;
//            }


            Log.d(TAG, "onBindViewHolder: NOTIFIED**** FALSE"+ sectionName+" - LastSectionPosition: "+sf.getLastSectionPosition());



        }
    }

    private String colorLighten(String color) {
        color = color.substring(0,1)+"e6"+ color.substring(1); //f2=95% e6=90%
        return color;
    }

//    private String colorLighten(String color) {
//        color = color.substring(0,1)+"e6"+ color.substring(1); //f2=95% e6=90%
//        return color;
//    }

    public void refreshSection(){
        for (Section section: sections) {
            if(section.isSelected())
            section.setSelected(false);
        }
        sections.get(tmp_last_pos).setSelected(true);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return sections.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView underline;
        public AppCompatTextView title;
        //Constructor
        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvtag);
            underline = itemView.findViewById(R.id.sectionUnderline);

            title.setOnClickListener(v -> {
                int position = getAdapterPosition();
                //for FEATURED/MOBILITY UsageOnly get/pass the URL
                String url = sections.get(position).getUrl();
                sf.setSectionUrl(url); //
                int section_id = sections.get(position).getId();
                sf.setLastSection(section_id); //Update LastSection is newsID from API
                sf.setSectionName(title.getText().toString().trim());//SetSectionName
                sf.setSectionNameTempContainer(title.getText().toString().trim()); //TEMP_SEctionName: Controller when returning to SECTION TAB without CLICKING the sectionTag
                Log.d(TAG, "onBindViewHolder: SECTION_CLICKED: "+ sections.get(position).getTitle() +" - "+ section_id);

                if (Global.isNetworkAvailable(context)) {
                    //Check ADBanner
                    fragment.checkAd2(String.valueOf(section_id));
                    //Check IF Not Already Click: Nothing will happen if CLICK the same SectionTag
                    if (!sections.get(position).isSelected()) {
                        //dismiss_breaking = dismissing snackBar
                        EventBus.getDefault().post(new PostUIUpdate("dismiss_breaking"));
                        //For Default new installation: NO ACTIVE TAGS = setSelected(false);
                        // FIRST: CLEARING ACTIVE_TAGS or The Previous ACTIVE SectionTag
                        if (sf.getLastSectionPosition() == -1) {
                            sections.get(0).setSelected(false);
                            sf.setLastSectionPosition(0);
                        } else {
                            //Update Previous Section_Tags INACTIVE = setSelected(false)
                            sections.get(sf.getLastSectionPosition()).setSelected(false);
                        }

                        //Update CURRENT Section_Tags ACTIVE = setSelected(true)
                        sections.get(position).setSelected(true);
                        //
                        tmp_last_pos = sf.getLastSectionPosition();

                        //Set the CURRENT Section_Tags index as LastSectionPosition Before CHANGING SECTION fragment.ChangeSection
                        sf.setLastSectionPosition(position);
                        //Refresh the ADAPTER after SAVING the LastSectionPosition():
                        // This will READ/UPDATE the //Default to ACTIVE Section_Tag  if ID is equal to -1 or Make it ACTIVE the last ID that has been click by MAKING the isSELECTED to TRUE
                        // Also will read the //Change ColorBackgroundTint if SELECTED is TRUE
                        notifyDataSetChanged();

                        Log.d(TAG, "onBindViewHolder: LAST_SECTION_POSITION: " +  sf.getLastSectionPosition());
                        //change section  .. Param: Section_id, SectionTagIndex, Url(reserved for FEATURED/MOBILITY: If ever Mobility is clicked)
                        fragment.ChangeSection(sf.getLastSection(), tmp_last_pos, url);
                    }

                } else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }
}
