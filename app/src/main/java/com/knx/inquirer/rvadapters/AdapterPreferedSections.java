package com.knx.inquirer.rvadapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.models.TagsModel;

import java.util.List;


public class AdapterPreferedSections extends RecyclerView.Adapter<AdapterPreferedSections.ViewHolder> {
    private static final String TAG = "AdapterPrefSections";
    private List<TagsModel> tagsList;
    private Context context;

    public AdapterPreferedSections(Context context, List<TagsModel> tags) {
        super();
        this.tagsList = tags;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pref_section_list_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TagsModel model = tagsList.get(position); //

        holder.title.setVisibility(model.getIsHidden().equals("Y")?View.GONE:View.VISIBLE);
        holder.title.setText(model.getTitle());

        holder.title.setBackgroundResource(model.isSelected() ? R.drawable.pref_section_reg_active : R.drawable.pref_section_reg);
        holder.setIsRecyclable(false);  //caseToCase basis: to tell the RecyclerView not to recycler...so it is much faster to load

        holder.title.setOnClickListener(v -> {
            model.setSelected(!model.isSelected());  //Setting the RVList item Selected or unSelected by Negating the current state
            holder.title.setBackgroundResource(model.isSelected() ? R.drawable.pref_section_reg_active : R.drawable.pref_section_reg);
        });
    }

    @Override
    public int getItemCount() {
        return tagsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvPrefSection);
        }
    }

}
