package com.knx.inquirer.rvadapters;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.fragments.FragmentMyInqEdit;

public class RVAdapterEmpty_Tags_Edit extends RecyclerView.Adapter<RVAdapterEmpty_Tags_Edit.ViewHolder> {

    private FragmentMyInqEdit fragment;

    public RVAdapterEmpty_Tags_Edit(FragmentMyInqEdit fragment) {
        this.fragment = fragment;
    }

    @Override
    public RVAdapterEmpty_Tags_Edit.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tags_empty, parent, false);
        RVAdapterEmpty_Tags_Edit.ViewHolder viewHolder = new RVAdapterEmpty_Tags_Edit.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAdapterEmpty_Tags_Edit.ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public Button retry;

        public ViewHolder(View itemView) {
            super(itemView);

            retry = itemView.findViewById(R.id.btnRetry);

            retry.setOnClickListener(view -> {
                fragment.setBlankAdapter();
                fragment.RetryFetchTags();
            });


        }
    }
}
