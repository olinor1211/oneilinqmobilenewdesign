package com.knx.inquirer.rvadapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.knx.inquirer.WebNewsActivity;
import com.knx.inquirer.models.BookmarksNewModel;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.R;
import com.knx.inquirer.WebBookmarksActivity;
import com.knx.inquirer.fragments.FragmentBookmarks;
import com.knx.inquirer.models.BookmarksModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class RVAdapterBookmarks extends RecyclerView.Adapter<RVAdapterBookmarks.ViewHolder> {
    private static final String TAG = "RVAdapterBookmarks";

    FragmentBookmarks fragment;
//    public ArrayList<BookmarksModel> bookmarks;
    public List<BookmarksNewModel> bookmarks = new ArrayList<>();

    private Context context;
//    private String photo, mainImage, title, date;
    private WebBookmarksActivity bookmarksActivity;
    private int sect;
    private boolean isDeleted = false;

    public RVAdapterBookmarks(Context context, List<BookmarksNewModel> bookmarks, FragmentBookmarks fragment) {
        super();
        this.bookmarks = bookmarks;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmarks_list_new, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        //Showing data on the views
        String mainImage = bookmarks.get(position).getMainImage();
        String title = bookmarks.get(position).getTitle();
        String date = bookmarks.get(position).getDate();
        String type = bookmarks.get(position).getType();
        String sectionName = bookmarks.get(position).getSectionName();

        Log.d(TAG, "onBindViewHolder: "+ sectionName);

        switch (sectionName.toLowerCase()) {
            case "property":
                sect = R.drawable.inquirerproperty;
                break;
            case "entertainment":
                sect = R.drawable.inquirerentertainment;
                break;
            case "lifestyle":
                sect = R.drawable.inquirerlifestyle;
                break;
            case "global nation":
                sect = R.drawable.inquirerglobalnation; //???
                break;
            case "sports":
                sect = R.drawable.inquirersports;
                break;
            case "technology":
                sect = R.drawable.inquirertechnology;
                break;
            case "business":
                sect = R.drawable.inquirerbusiness;
                break;
            case "opinion":
                sect = R.drawable.inquireropinion;
                break;
            case "board talk":
                sect = R.drawable.inquirerboardtalk;
                break;
            case "motoring":
                sect = R.drawable.inquirermobility;
                break;
            case "inquirer rebound":
                sect = R.drawable.inquirerrebound;
                break;
            default:
                sect = R.drawable.inquirernewsinfo; //???
                break;
        }

//        switch (type) {
//            case "2":  sect = R.drawable.inquirersports;
//                break;
//            case "3":  sect = R.drawable.inquirerentertainment;
//                break;
//            case "5":  sect = R.drawable.inquirertechnology;
//                break;
//            case "6":  sect = R.drawable.inquirerglobalnation;
//                break;
//            case "7":  sect = R.drawable.inquirerbusiness;
//                break;
//            case "8":  sect = R.drawable.inquirerlifestyle;
//                break;
//            case "12":  sect = R.drawable.inquirerproperty;
//                break;
//            case "13":  sect = R.drawable.inquireropinion_blue;
//                break;
//            case "16":  sect = R.drawable.inquirerboardtalk;
//                break;
//            case "17":  sect = R.drawable.inquirermobility;
//                break;
//            case "19":  sect = R.drawable.inquirerrebound;
//                break;
//            default: sect = R.drawable.inquirernewsinfo;
//        }


        RequestOptions options = new RequestOptions()
                .placeholder(sect)
                .error(sect)
                .transform(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(10)));

        if(isValidated(mainImage)){
            Glide.with(context)
                    .load(mainImage)
                    .thumbnail(0.05f)
                    .apply(options)
                    .into(holder.ivImage);
        }else{
            holder.ivImage.setImageResource(sect);
//            GlideApp.with(context)
//                    .load(sect)
//                    .thumbnail(0.05f)
//                    .into(holder.ivImage);
        }

//        GlideApp.with(context)
//                .load(mainImage)
////                .transition(withCrossFade())
//                .thumbnail(0.05f)
//                .apply(options)
//                .into(holder.ivImage);
//        ViewCompat.setTransitionName(holder.ivImage, "bookmarkPhoto"); //setTransitionName

        holder.tvTitle.setText(title);
        holder.tvDate.setText(date);

    }

    private boolean isValidated(String string){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isValidated: Exception_Error: "+e.getMessage());
        }finally {
            if(string!=null) return true;  //Check if null
            Log.d(TAG, "isValidated: Exception_Error Finally_Executed** ");
        }
        return false;
    }

    private void showImage(String imgStr, ImageView imgView){
        //Loading Image and get the Height and Width of the ImageUrl: And load the Image as Bitmap to make it Loading Faster
        Glide.with(context)
                .asBitmap()
                .error(sect)
                .load(imgStr)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        //NO Need to Doubled the width and height just to make more clearer bitmap. Only has Small ContainerFrame
                        imgView.setImageBitmap(resource);//imgView.setImageBitmap(resource);
                    }
                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }


    @Override
    public int getItemCount() {
        return bookmarks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //Views
        public TextView tvTitle, tvDate;
        public ImageView ivImage, share, delete;

        public ViewHolder(final View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.bookmarkImage);
            tvTitle = itemView.findViewById(R.id.bookmarkTitle);
            tvDate = itemView.findViewById(R.id.bookmarkDate);

            share = itemView.findViewById(R.id.bookmarkShare);
            delete = itemView.findViewById(R.id.bookmarkDelete);
            share.setOnClickListener(this);
            delete.setOnClickListener(this);
            ivImage.setOnClickListener(this);
            tvTitle.setOnClickListener(this);
            tvDate.setOnClickListener(this);

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                ivImage.setClipToOutline(true);
//            }
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            int id = bookmarks.get(pos).getNewsId();
            String title = bookmarks.get(pos).getTitle();
            String mainImage = bookmarks.get(pos).getMainImage();
            String type = bookmarks.get(pos).getType();
            String date = bookmarks.get(pos).getDate();
            String content1 = bookmarks.get(pos).getContent1();
            String image1 = bookmarks.get(pos).getImage1();
            String content2 = bookmarks.get(pos).getContent2();
            String author = bookmarks.get(pos).getAuthor();
            String custom = bookmarks.get(pos).getCustom();
            String bannerSectionId = bookmarks.get(pos).getBannerSectionId();
            String sectionName = bookmarks.get(pos).getSectionName();
            String insertTitle = "";
            String insertType = "";
            String insertNewsId = "";
            String insertContent = "";

            switch (view.getId()){
                case R.id.bookmarkShare:
                    fragment.shareArticle(bookmarks.get(pos).getTitle(), bookmarks.get(pos).getType(), String.valueOf(bookmarks.get(pos).getNewsId()));
//                    fragment.shareArticle2(bookmarks.get(pos).getTitle(), bookmarks.get(pos).getType(), String.valueOf(bookmarks.get(pos).getNewsId()));
                    break;
                case R.id.bookmarkDelete:
                    showDeleteDialog(pos);
                    break;
                default:
//                    EventBus.getDefault().post("bookmarkArticle");
//                    fragment.viewArticle(id,title,mainImage,type,date,content1,image1,content2,author,custom,bannerSectionId,sectionName);
                    fragment.viewArticle(id,title,mainImage,type,date,content1,image1,insertTitle,insertType,insertNewsId,insertContent,content2,author,custom,bannerSectionId,sectionName);
                    break;
            }
        }
    }
    private void showDeleteDialog(int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Remove this article \"" + bookmarks.get(position).getTitle() + "\" ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fragment.deleteOneBookmark(bookmarks.get(position).getNewsId(), position);
                    }
                })
                .setNegativeButton("No", (dialog, which) -> {
                })
                .setCancelable(false)
                .create()
                .show();
    }
}