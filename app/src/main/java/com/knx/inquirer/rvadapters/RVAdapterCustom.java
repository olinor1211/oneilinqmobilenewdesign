package com.knx.inquirer.rvadapters;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.fragments.FragmentCustom;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.PostArticleBreaking;
import com.knx.inquirer.models.PostArticleCustom;
import com.knx.inquirer.models.PostPodcastPlaybackButtonCustom;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.PlayerWebView;
import com.knx.inquirer.utils.SharedPrefs;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.AUDIO_SERVICE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class RVAdapterCustom extends RecyclerView.Adapter {

    private static final String TAG = "RVAdapterCustom";

    private AudioManager amanager;

    private int sect;
    private ArrayList<Feed> feeds;
    private Context context;
    private FragmentCustom fragment;
    private SharedPrefs sf;
    private String _id, photo, type, title, pubdate, label;
    private List<String> imgItems;
    private int itemposition;
    private DatabaseHelper helper;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_IMAGE = 2;
    private final int VIEW_TYPE_VIDEO = 3;
    private final int VIEW_TYPE_CAROUSEL = 4;
    private final int VIEW_TYPE_IMAGE_DOUBLE = 5;
    private final int VIEW_TYPE_VIDEO_DOUBLE = 6;
    private final int VIEW_TYPE_VIDEO_ARTICLE = 7;
    private final int VIEW_TYPE_DM = 8;
    private final int VIEW_TYPE_INQPRIME = 9;
    private final int VIEW_TYPE_DM_LINK = 10;
//    private final int VIEW_TYPE_PODCAST10 = 110;
//    private final int VIEW_TYPE_PODCAST11 = 111;
//    private final int VIEW_TYPE_PODCAST12 = 112;
//    private final int VIEW_TYPE_PODCAST13 = 113;
//    private final int VIEW_TYPE_PODCAST14 = 114;


    //TYPE
    private final String NEWS_ITEM_0 = "0";
    private final String NEWS_ITEM_1 = "1";
    private final String IMAGE = "2";
    private final String VIDEO = "3";
    private final String CAROUSEL = "4";
    private final String IMAGE_DOUBLE = "5";
    private final String VIDEO_DOUBLE = "6";
    private final String VIDEO_ARTICLE = "7";
    private final String DAILY_MOTION = "8";
    private final String INQ_PRIME_TAG = "9";
    private final String DAILY_MOTION_LINK = "10";
//    private final String PODCAST0 = "110";
//    private final String PODCAST1 = "111";
//    private final String PODCAST2 = "112";
//    private final String PODCAST3 = "113";
//    private final String PODCAST4 = "114";


    private OnLoadMoreListener loadMoreListener;
    private boolean isLoading = false, isMoreDataAvailable = true;

    //ExoPLayer
    private SimpleExoPlayer exoPlayer, exoPlayer2, exoPlayer3;

    private ViewGroup.LayoutParams dmLayoutParams;

    public RVAdapterCustom(Context context, ArrayList<Feed> feeds, FragmentCustom fragment, SharedPrefs sf) {
        super();

        this.feeds = feeds;
        this.context = context;
        this.fragment = fragment;
        this.sf = sf;
        helper = new DatabaseHelper(context);
    }

    //viewHolder(item)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;   //

        if (viewType == VIEW_TYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_row, parent, false);
            vh = new ItemViewHolder(v);  //calling instance of viewHolder

        }else if(viewType == VIEW_TYPE_IMAGE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_items, parent, false);
            vh = new ImageTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_list_items, parent, false);
            vh = new VideoTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_CAROUSEL) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carousel_list_items, parent, false);
            vh = new CarouselTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_IMAGE_DOUBLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_double_list_items, parent, false);
            vh = new ImageDoubleViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO_DOUBLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_double_list_items, parent, false);
            vh = new VideoDoubleTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO_ARTICLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_article_list_items, parent, false);
            vh = new VideoArticleTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_DM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_motion_list_items, parent, false);
            vh = new DMTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_DM_LINK) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_motion_link_list_items, parent, false);
            vh = new DMLinkTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_INQPRIME) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inqprime_list_row, parent, false);
            vh = new InqPrimeTypeViewHolder(v);

        }
        else  {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }

        return vh;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore(); //Will be called only after IF isMoreDataAvailable
        }

        if (!(holder instanceof ProgressViewHolder)) {
                //Passing data from URL
            _id = feeds.get(position).getId();
            photo = feeds.get(position).getImage();
            type = feeds.get(position).getType();
            title = feeds.get(position).getTitle();
            pubdate = feeds.get(position).getPubdate();
            label = feeds.get(position).getLabel();
            String tes = feeds.get(position).getSection();

            Log.d(TAG, "onBindViewHolder: LAST_SECTION TEST: "+ tes);
            //remove temp
            //Cannot determine what kind of news
//                switch (sf.getLastSectionCustom()){ //switch (sf.getSectionMenuID()){  //switch (sf.getLastSection()){
//
//                case 2:  sect = R.drawable.inquirersports;
//                    break;
//                case 3:  sect = R.drawable.inquirerentertainment;
//                    break;
//                case 5:  sect = R.drawable.inquirertechnology;
//                    break;
//                case 6:  sect = R.drawable.inquirerglobalnation;
//                    break;
//                case 7:  sect = R.drawable.inquirerbusiness;
//                    break;
//                case 8:  sect = R.drawable.inquirerlifestyle;
//                    break;
//                case 13:  sect = R.drawable.inquireropinion_blue;
//                    break;
//
//                default: sect = R.drawable.inquirernewsinfo;
//            }

            //DEFAULT
            sect = R.drawable.inquirernewsinfo;



            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(sect)
                    .error(sect)
                    //.skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .priority(Priority.HIGH);
//
//            RequestOptions options_ads = new RequestOptions()
//                    .centerCrop()
//                    .placeholder(R.drawable.inquirernewsinfo)
//                    .error(R.drawable.inquirernewsinfo)
//                    //.skipMemoryCache(true)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .priority(Priority.HIGH);


            switch (type) {
                case NEWS_ITEM_0:
//                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
//                    TextView newsTitle = ((ItemViewHolder) holder ).newstitle;
//                    TextView newsDate = ((ItemViewHolder) holder ).newspubdate;
//                    ImageView newsPhoto = ((ItemViewHolder) holder ).newsphoto;
//                    displayType01(options,_id,title,pubdate,photo,newsTitle,newsDate,newsPhoto);
//                    break;

                case NEWS_ITEM_1:
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    checkIfBookmarked(((ItemViewHolder) holder), feeds.get(position).getId());
                    TextView newsTitle1 = ((ItemViewHolder) holder ).newstitle;
                    TextView newsDate1 = ((ItemViewHolder) holder ).newspubdate;
                    ImageView newsPhoto1 = ((ItemViewHolder) holder ).newsphoto;
                    displayType01(options,_id,title,pubdate,photo,newsTitle1,newsDate1,newsPhoto1);
                    break;

                case IMAGE: //Image
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    TextView adTitle2 = ((ImageTypeViewHolder) holder ).textImageTitle;
                    TextView adSponsor2= ((ImageTypeViewHolder) holder ).textImageSponsored;
                    ImageView adPhoto2 = ((ImageTypeViewHolder) holder ).imageOrdinaryPhoto;
                    displayType25(options, sect,title,label,photo,adTitle2,adSponsor2,adPhoto2); //label here
                    break;

                case VIDEO: //Video
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+VIDEO);
                    if (isValidated2(feeds.get(position).getTitle(), VIDEO+" title")) { //if (isValidated(feeds.get(position).getTitle())) {
                        ( (VideoTypeViewHolder) holder ).video3Title.setText(feeds.get(position).getTitle());
                        ( (VideoTypeViewHolder) holder ).video3Title.setVisibility(VISIBLE);
                    } else ( (VideoTypeViewHolder) holder ).video3Title.setVisibility(INVISIBLE);

                    if (isValidated(feeds.get(position).getLabel())) {
                        ( (VideoTypeViewHolder) holder ).video3Sponsored.setText(feeds.get(position).getLabel());
                        ( (VideoTypeViewHolder) holder ).video3Sponsored.setVisibility(VISIBLE);
                    } else ( (VideoTypeViewHolder) holder ).video3Sponsored.setVisibility(INVISIBLE);

                    if (isValidated(photo)) {
                        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                        exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

                        DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                        ( (VideoTypeViewHolder) holder ).newsVideo.setUseController(false);  //play, stop
                        ( (VideoTypeViewHolder) holder ).newsVideo.setPlayer(exoPlayer);
                        ( (VideoTypeViewHolder) holder ).newsVideo.setShowBuffering(true);

                        exoPlayer.prepare(mediaSource);
                        exoPlayer.setPlayWhenReady(true);
                        exoPlayer.setVolume(0f);
//                    exoPlayer.setRepeatMode(exoPlayer.REPEAT_MODE_ONE);
                    }
                    break;


                case CAROUSEL:  // Carousel
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    String images = feeds.get(position).getImage();
                    String imagesUrl[] = images.split(",");

                    if (( (CarouselTypeViewHolder)holder ).carouselView !=null ){
                        ImageListener imageListener = (position1, imageView) -> {
                            Glide
                                    .with(context)
                                    .load(imagesUrl[position1])
                                    .transition(withCrossFade())
                                    .placeholder(R.drawable.inquirernewsinfo)
                                    .apply(options)
                                    .into(imageView);
                            ViewCompat.setTransitionName(imageView, String.valueOf(_id)); //setTransitionName

                        };

                        ( (CarouselTypeViewHolder)holder ).carouselView.setPageCount(imagesUrl.length);
                        ( (CarouselTypeViewHolder)holder ).carouselView.setImageListener(imageListener);

                        //androidx
                        if(isValidated(feeds.get(position).getLabel())){
                            ( (CarouselTypeViewHolder)holder ).textCarouselTag.setText(feeds.get(position).getLabel());
                            ( (CarouselTypeViewHolder)holder ).textCarouselTag.setVisibility(VISIBLE);
                        }else{( (CarouselTypeViewHolder)holder ).textCarouselTag.setVisibility(INVISIBLE);}

                        //androidx
                        if (isValidated2(feeds.get(position).getTitle(), CAROUSEL+" title")) { //if (isValidated(feeds.get(position).getTitle())) {
                            ( (CarouselTypeViewHolder)holder ).textCarouselTitle.setText(feeds.get(position).getTitle());
                            ( (CarouselTypeViewHolder)holder ).textCarouselTitle.setVisibility(VISIBLE);

                        }else{( (CarouselTypeViewHolder)holder ).textCarouselTitle.setVisibility(INVISIBLE);}

                    }
                    break;


                case IMAGE_DOUBLE: //ImageDouble
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    TextView adTitle5 = ((ImageDoubleViewHolder) holder ).textDoubleTitle;
                    TextView adSponsor5= ((ImageDoubleViewHolder) holder ).textDoubleSponsored;
                    ImageView adPhoto5 = ((ImageDoubleViewHolder) holder ).imageDoubePhoto;
                    displayType25(options,sect,title,label,photo,adTitle5,adSponsor5,adPhoto5); //label here
                    break;

                case VIDEO_DOUBLE: //VideoDouble height
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    if (isValidated2(feeds.get(position).getTitle(), VIDEO_DOUBLE+" title")) { //if (isValidated(feeds.get(position).getTitle())) {
                            ((VideoDoubleTypeViewHolder) holder).video6Title.setText(feeds.get(position).getTitle());
                        ((VideoDoubleTypeViewHolder) holder).video6Title.setVisibility(VISIBLE);
                    } else ((VideoDoubleTypeViewHolder) holder).video6Title.setVisibility(INVISIBLE);

                    if (isValidated(feeds.get(position).getLabel())) {
                        ((VideoDoubleTypeViewHolder) holder).video6Sponsored.setText(feeds.get(position).getLabel());
                        ((VideoDoubleTypeViewHolder) holder).video6Sponsored.setVisibility(VISIBLE);
                    } else ((VideoDoubleTypeViewHolder) holder).video6Sponsored.setVisibility(INVISIBLE);

                    if (isValidated(photo)) {
                        BandwidthMeter bandwidthMeter2 = new DefaultBandwidthMeter();
                        TrackSelector trackSelector2 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter2));
                        exoPlayer2 = ExoPlayerFactory.newSimpleInstance(context, trackSelector2);

                        DefaultHttpDataSourceFactory dataSourceFactory2 = new DefaultHttpDataSourceFactory("exoplayer_video2");
                        MediaSource mediaSource2 = new ExtractorMediaSource.Factory(dataSourceFactory2).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                        ( (VideoDoubleTypeViewHolder) holder ).newsVideoDouble.setUseController(false);  //play, stop
                        ( (VideoDoubleTypeViewHolder) holder ).newsVideoDouble.setPlayer(exoPlayer2);
                        ( (VideoDoubleTypeViewHolder) holder ).newsVideoDouble.setShowBuffering(true);

                        exoPlayer2.prepare(mediaSource2);
                        exoPlayer2.setPlayWhenReady(true);
                        exoPlayer2.setVolume(0f);
//                    exoPlayer2.setRepeatMode(exoPlayer2.REPEAT_MODE_ONE);
                    }
                    break;

                case VIDEO_ARTICLE: //Video Article
                    if (isValidated2(feeds.get(position).getTitle(), VIDEO_ARTICLE+" title")) { //if (isValidated(feeds.get(position).getTitle())) {
                        Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                        ( (VideoArticleTypeViewHolder) holder ).video7Title.setText(feeds.get(position).getTitle());
                        ( (VideoArticleTypeViewHolder) holder ).video7Title.setVisibility(VISIBLE);
                    } else ( (VideoArticleTypeViewHolder) holder ).video7Title.setVisibility(INVISIBLE);

                    if (isValidated(feeds.get(position).getLabel())) {
                        ( (VideoArticleTypeViewHolder) holder ).video7Sponsored.setText(feeds.get(position).getLabel());
                        ( (VideoArticleTypeViewHolder) holder ).video7Sponsored.setVisibility(VISIBLE);
                    } else ( (VideoArticleTypeViewHolder) holder ).video7Sponsored.setVisibility(INVISIBLE);

                    if (isValidated(photo)) {
                        BandwidthMeter bandwidthMeter3 = new DefaultBandwidthMeter();
                        TrackSelector trackSelector3 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter3));
                        exoPlayer3 = ExoPlayerFactory.newSimpleInstance(context, trackSelector3);

                        DefaultHttpDataSourceFactory dataSourceFactory3 = new DefaultHttpDataSourceFactory("exoplayer_video3");
                        MediaSource mediaSource3 = new ExtractorMediaSource.Factory(dataSourceFactory3).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                        ( (VideoArticleTypeViewHolder) holder ).newsVideoArticle.setUseController(false);  //play, stop
                        ( (VideoArticleTypeViewHolder) holder ).newsVideoArticle.setPlayer(exoPlayer3);
                        ( (VideoArticleTypeViewHolder) holder ).newsVideoArticle.setShowBuffering(true);

                        exoPlayer3.prepare(mediaSource3);
                        exoPlayer3.setPlayWhenReady(true);
                        exoPlayer3.setVolume(0f);
//                    exoPlayer3.setRepeatMode(exoPlayer3.REPEAT_MODE_ONE);

                    }
                    break;


                case DAILY_MOTION: //Dailymotion
//                    Map<String, String> dmParams = new HashMap<>();
//                    dmParams.put("syndication", Global.syndicationKey);
//                    dmParams.put("app", Global.appPackageName);
//                    dmParams.put("api", "nativeBridge");

                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);

                    ImageView imageView =  ( (DMTypeViewHolder) holder ).gradient;
                    imageView.setVisibility(VISIBLE);
                    imageView.setImageResource(sect);//No LINK

                    //androidx
                    if (isValidated2(feeds.get(position).getTitle(), DAILY_MOTION+" title "+feeds.get(position).getTitle())) { //if (isValidated(feeds.get(position).getTitle())) {
                        ( (DMTypeViewHolder) holder ).textDmTitle.setText(feeds.get(position).getTitle());
                        ( (DMTypeViewHolder) holder ).textDmTitle.setVisibility(VISIBLE);
                    }else{
                        ( (DMTypeViewHolder) holder ).textDmTitle.setVisibility(INVISIBLE);
                    }

                    //androidx
                    if(isValidated(feeds.get(position).getLabel())){
                        ( (DMTypeViewHolder) holder ).textDmSponsored.setText(feeds.get(position).getLabel());
                        ( (DMTypeViewHolder) holder ).textDmSponsored.setVisibility(VISIBLE);
                    }else{
                        ( (DMTypeViewHolder) holder ).textDmSponsored.setVisibility(INVISIBLE);
                    }

                    //androidx
                    String vidUrl = feeds.get(position).getImage();
                    //androidx
                    if (isValidated2(vidUrl, DAILY_MOTION+" vidUrl "+ vidUrl)) { //if(isValidated(vidUrl)){
                        try{
                            int li = vidUrl.lastIndexOf('/')+1;
                            vidUrl = vidUrl.substring(li);

                            ((DMTypeViewHolder) holder).dmPlayerWebView.showControls(true);
                            ((DMTypeViewHolder) holder).dmPlayerWebView.mute(); //no param
                            ((DMTypeViewHolder) holder).dmPlayerWebView.setPlayWhenReady(true); //true
                            ((DMTypeViewHolder) holder).dmPlayerWebView.load(vidUrl); //dmParams
                            setDelayDisplayThumbnail(imageView);  //DISAPPEARING the TOP DISPLAY/TOP_GRADIENT in 1secs..
                        }catch(Exception e){
                            Log.d(TAG, "onBindViewHolder: Exception_Error: DM "+ e.getMessage());
                        }
                    }else{
                        displayDefaultImage(imageView);
                    }
                    break;

                case INQ_PRIME_TAG:
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    TextView newsTitle9 = ((InqPrimeTypeViewHolder) holder ).newsTitleInqPrime;
                    TextView newsDate9 = ((InqPrimeTypeViewHolder) holder ).newsPubdateInqPrime;
                    ImageView newsPhoto9 = ((InqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime;
                    displayType01(options,_id,title,pubdate,photo,newsTitle9,newsDate9,newsPhoto9); //pubdate here
                    break;

                case DAILY_MOTION_LINK:  //

                    //USE sect as the TOP DISPLAY while waiting the VIDEO to Prepare
                    ImageView imageView1 = ( (DMLinkTypeViewHolder) holder ).gradient;  //
                    imageView1.setImageResource(sect);// Using the section image as the TOP DISPLAY

                    //androidx
                    if (isValidated2(feeds.get(position).getTitle(), DAILY_MOTION_LINK+" title")) { //if (isValidated(feeds.get(position).getTitle())) {
                        ( (DMLinkTypeViewHolder) holder ).textDmTitle.setText(feeds.get(position).getTitle());
                        ( (DMLinkTypeViewHolder) holder ).textDmTitle.setVisibility(VISIBLE);
                    }else{
                        ( (DMLinkTypeViewHolder) holder ).textDmTitle.setVisibility(INVISIBLE);
                    }

                    //androidx
                    if(isValidated(feeds.get(position).getLabel())){
                        ( (DMLinkTypeViewHolder) holder ).textDmSponsored.setText(feeds.get(position).getLabel());
                        ( (DMLinkTypeViewHolder) holder ).textDmSponsored.setVisibility(VISIBLE);
                    }else{
                        ( (DMLinkTypeViewHolder) holder ).textDmSponsored.setVisibility(INVISIBLE);
                    }

                    //androidx
                    String videoUrl = feeds.get(position).getImage();
                    //androidx
                    if(isValidated(videoUrl)){
                        try{
                            int li = videoUrl.lastIndexOf('/')+1;
                            videoUrl = videoUrl.substring(li);

                            ((DMLinkTypeViewHolder) holder).dmPlayerWebView.showControls(true);
                            ((DMLinkTypeViewHolder) holder).dmPlayerWebView.mute(); //no param
                            ((DMLinkTypeViewHolder) holder).dmPlayerWebView.setPlayWhenReady(true); //true
                            ((DMLinkTypeViewHolder) holder).dmPlayerWebView.load(videoUrl); //dmParams
                            setDelayDisplayThumbnail(imageView1);  //DISAPPEARING the TOP DISPLAY/TOP_GRADIENT in 1secs..
//                            imageView1.setImageResource(R.drawable.no_color); //Return no_color as the TOP DISPLAY

                        }catch(Exception e){
                            Log.d(TAG, "onBindViewHolder: Exception_Error: DM "+ e.getMessage());
                        }
                    }else{
                        displayDefaultImage(imageView1);
                    }
                    break;
            }

        }else{
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    //NEW ADDITIONAL new_design
    public void deleteOneBookmark(ImageView bookmark, int newsId) {
        helper.deleteOneBookmark(newsId);
        //Instant Updating the Latest Bookmarking:
        bookmark.setImageResource(R.drawable.bookmark_prime);
        Global.isBookmark = false;
        fragment.toastMessage("Bookmark deleted.");
//        Toast.makeText(context.getApplicationContext(), "Bookmark deleted.", Toast.LENGTH_SHORT).show();
    }

    private void checkIfBookmarked(ItemViewHolder holder, String idStr) {
        if(Global.bookmarkIdList.contains(Integer.valueOf(idStr))){
            holder.bookmark.setImageResource(R.drawable.new_bookmark_fill); //holder.bookmark.setImageResource(R.drawable.bookmark_fill);
        }else holder.bookmark.setImageResource(R.drawable.new_bookmark_empty); //holder.bookmark.setImageResource(R.drawable.bookmark_prime);
    }

    @Override
    public int getItemViewType(int position) {

        if (feeds.get(position) != null) {
            String type = feeds.get(position).getType();
            switch(type){
                case NEWS_ITEM_0:
                    return VIEW_TYPE_ITEM;
                case NEWS_ITEM_1:
                    Log.d(TAG, "getItemViewType: News " + type);
                    return VIEW_TYPE_ITEM;

                case IMAGE:
                    Log.d(TAG, "getItemViewType: News " + type);
                    return VIEW_TYPE_IMAGE;

                case VIDEO:
                    Log.d(TAG, "getItemViewType: Video " + type);
                    return VIEW_TYPE_VIDEO;

                case CAROUSEL:
                    Log.d(TAG, "getItemViewType: Carousel " + type);
                    return VIEW_TYPE_CAROUSEL;

                case IMAGE_DOUBLE:
                    Log.d(TAG, "getItemViewType: ImageDouble " + type);
                    return VIEW_TYPE_IMAGE_DOUBLE;

                case VIDEO_DOUBLE:
                    Log.d(TAG, "getItemViewType: VideoDouble " + type);
                    return VIEW_TYPE_VIDEO_DOUBLE;

                case VIDEO_ARTICLE:
                    Log.d(TAG, "getItemViewType: VideoArticle " + type);
                    return VIEW_TYPE_VIDEO_ARTICLE;

                case DAILY_MOTION:
                    Log.d(TAG, "getItemViewType: DM " + type);
                    return VIEW_TYPE_DM;

                case INQ_PRIME_TAG:
                    Log.d(TAG, "getItemViewType: InqPrime " + type);
                    return VIEW_TYPE_INQPRIME;

                case DAILY_MOTION_LINK:
                    Log.d(TAG, "getItemViewType: DMLink"+ type);
                    return VIEW_TYPE_DM_LINK;

                default:
                    return -1;
            }

        }else {
            return VIEW_TYPE_LOADING;
        }
//        return news.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return feeds == null ? 0 : feeds.size();
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    //
    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    //INTERFACE
    public interface OnLoadMoreListener {
        void onLoadMore();
    }


    //NEED PARAM: INTERFACE OnLoadMoreListener but Supplied with ArrowFunction instead in the FragmentCustom. ????? That should return loadMoreListener
    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }


    //test ImageTypeViewHolder
    public class ImageTypeViewHolder extends RecyclerView.ViewHolder {
        private TextView textImageTitle, textImageSponsored;
        private ImageView imageOrdinaryPhoto;

        public ImageTypeViewHolder(@NonNull View itemView) {
            super(itemView);

            imageOrdinaryPhoto = itemView.findViewById(R.id.imageOrdinaryPhoto);
            textImageSponsored = itemView.findViewById(R.id.textImageSponsored);
            textImageTitle = itemView.findViewById(R.id.textImageTitle);

            itemView.setOnClickListener(v -> {
                Log.d(TAG, "CLICKED TYPE : "+type);
                int n = getAdapterPosition();
                String link = feeds.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {

                    fragment.checkImageLink(link);

                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }


    //test VideoTypeViewHolder
    public class VideoTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideo;
        TextView video3Title, video3Sponsored;
//          RelativeLayout video3Layout;

        public VideoTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideo = itemView.findViewById(R.id.news_video);
            this.video3Sponsored = itemView.findViewById(R.id.video3Sponsored);
            this.video3Title = itemView.findViewById(R.id.video3Title);
//            this.video3Layout = itemView.findViewById(R.id.rlVideo3);
            itemView.setOnClickListener(v -> {

                int n = getAdapterPosition();
                String link = feeds.get(n).getLink();

                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        fragment.checkVideoLink(link);
                    }
                }else {
//                    fragment.disableNewsInterceptor();
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }


    //test VideoDoubleTypeViewHolder
    public class VideoDoubleTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideoDouble;
        TextView video6Title, video6Sponsored;

        public VideoDoubleTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideoDouble = itemView.findViewById(R.id.news_video6);
            this.video6Sponsored = itemView.findViewById(R.id.video6Sponsored);
            this.video6Title = itemView.findViewById(R.id.video6Title);

            itemView.setOnClickListener(v -> {
                int n = getAdapterPosition();
                String link = feeds.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        fragment.checkVideoLink(link);
                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }

    //test VideoArticleTypeViewHolder
    public class VideoArticleTypeViewHolder extends RecyclerView.ViewHolder {
        PlayerView newsVideoArticle;
        TextView video7Title, video7Sponsored;

        public VideoArticleTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            this.newsVideoArticle = itemView.findViewById(R.id.news_video7);
            this.video7Sponsored = itemView.findViewById(R.id.video7Sponsored);
            this.video7Title = itemView.findViewById(R.id.video7Title);

            itemView.setOnClickListener(v -> {
                int n = getAdapterPosition();
                String videoUrl = feeds.get(n).getImage();

                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        Global.isArticleViewed = false; ////RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
//                        EventBus.getDefault().post(new PostArticleCustom(feeds.get(n).getId())); //
                        EventBus.getDefault().post(new PostArticleCustom(feeds.get(n).getId(),type)); //

                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }

    //test CarouselTypeViewHolder
    public class CarouselTypeViewHolder extends RecyclerView.ViewHolder {
        private CarouselView carouselView;
        private TextView textCarouselTag, textCarouselTitle;

        public CarouselTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.carouselView = itemView.findViewById(R.id.carouselView);
            this.textCarouselTag = itemView.findViewById(R.id.textCarouselTag);
            this.textCarouselTitle = itemView.findViewById(R.id.textCarouselTitle);

            carouselView.setImageClickListener(position -> {
                int n = getAdapterPosition();
                String link = feeds.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        Global.isArticleViewed = false; ////RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
//                        EventBus.getDefault().post(new PostArticleCustom(feeds.get(n).getId())); //
                        EventBus.getDefault().post(new PostArticleCustom(feeds.get(n).getId(),type)); //

                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }


    //test ImageDoubleViewHolder
    public class ImageDoubleViewHolder extends RecyclerView.ViewHolder {
        private TextView textDoubleTitle, textDoubleSponsored;
        private ImageView imageDoubePhoto;

        public ImageDoubleViewHolder(@NonNull View itemView) {
            super(itemView);
            imageDoubePhoto = itemView.findViewById(R.id.imageDoublePhoto);
            textDoubleSponsored = itemView.findViewById(R.id.textDoubleSponsored);
            textDoubleTitle = itemView.findViewById(R.id.textDoubleTitle);

            itemView.setOnClickListener(v -> {
                int n = getAdapterPosition();
                String link = feeds.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        fragment.checkImageLink(link);
                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }

    //test DMTypeViewHolder
    public class DMTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerWebView dmPlayerWebView;
        TextView textDmSponsored, textDmTitle;
        ImageView gradient;
        public DMTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            this.dmPlayerWebView = itemView.findViewById(R.id.dmPlayerWebView);
            this.textDmSponsored = itemView.findViewById(R.id.textDmSponsored);
            this.textDmTitle = itemView.findViewById(R.id.textDmTitle);
            this.gradient = itemView.findViewById(R.id.imageDmGradient);
        }
    }
    // DMLinkTypeViewHolder
    public class DMLinkTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerWebView dmPlayerWebView;
        TextView textDmSponsored, textDmTitle;
        ImageView gradient;

        public DMLinkTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.dmPlayerWebView = itemView.findViewById(R.id.dmLinkPlayerWebView);
            this.textDmSponsored = itemView.findViewById(R.id.textDmLinkSponsored);
            this.textDmTitle = itemView.findViewById(R.id.textDmLinkTitle);
            this.gradient = itemView.findViewById(R.id.imageDmLinkGradient);

            gradient.setOnClickListener(v -> {  // gradient.setOnClickListener(v -> {
                int n = getAdapterPosition();
                String link = feeds.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        fragment.checkVideoLink(link);
                    }

                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }

   //InqPrimeTypeViewHolder
    private class InqPrimeTypeViewHolder extends RecyclerView.ViewHolder {
        //Views
        TextView newsTitleInqPrime, newsPubdateInqPrime, tagInqPrime;
        ImageView mPrimeTag, mImagePhotoInqPrime;

        public InqPrimeTypeViewHolder(View itemView) {
            super(itemView);

            newsTitleInqPrime = itemView.findViewById(R.id.textTitleInqPrime);
            newsPubdateInqPrime = itemView.findViewById(R.id.textDatesInqPrime);
            mImagePhotoInqPrime = itemView.findViewById(R.id.imagePhotoInqPrime);
            mPrimeTag = itemView.findViewById(R.id.primeTag);
            tagInqPrime = itemView.findViewById(R.id.tvTagInqPrime);

            itemView.setOnClickListener(v -> {

                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        itemposition = getAdapterPosition();
                        Global.isArticleViewed = false; ////RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
//                        EventBus.getDefault().post(new PostArticleCustom(feeds.get(itemposition).getId())); //
                        EventBus.getDefault().post(new PostArticleCustom(feeds.get(itemposition).getId(),type)); //

                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }

    //First ViewHolder ItemViewHolder
    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView newstitle, newspubdate, tag;
        ImageView newsphoto, bookmark;;

        public ItemViewHolder(View itemView) {
            super(itemView);

            newstitle = itemView.findViewById(R.id.textTitle);
            newspubdate = itemView.findViewById(R.id.textDetails);
            newsphoto = itemView.findViewById(R.id.imagePhoto);
            tag = itemView.findViewById(R.id.tvTag);
            bookmark = itemView.findViewById(R.id.ivNewsListBookmark);

            bookmark.setOnClickListener(this);
            newstitle.setOnClickListener(this);
            newspubdate.setOnClickListener(this);
            newsphoto.setOnClickListener(this);
            tag.setOnClickListener(this);
        }

        //@SuppressLint("UseCompatLoadingForDrawables")
        @Override
        public void onClick(View view) {
            //NOTE: Global.isBookmark is CONTROLLER for ALL bookmarkImageView to NOT process until the previous is DONE processing
            if (( SystemClock.elapsedRealtime() - Global.lastClickTime) < 1000){
                Global.isItemClicked=true;
                Log.d(TAG, "onSuccess: onPauseBreaking AVOIDED");
            }else Global.isItemClicked=false;
            Global.lastClickTime =  SystemClock.elapsedRealtime();

            int itemposition = getAdapterPosition();
            if (Global.isNetworkAvailable(context)) {
                switch (view.getId()){
                    case R.id.ivNewsListBookmark:
                        Log.d(TAG, "onClick: TAG_TAG1 "+Global.isBookmark);
                        bookmark.setEnabled(false);
                        if(!Global.isBookmark){
                            //NEED TRY CATCH: BUGS_ENCOUNTERED when SUPER_FAST clicking of bookmark icon
                            try{
                                Global.isBookmark = true;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    if(bookmark.getDrawable().getConstantState().equals(bookmark.getContext().getDrawable(R.drawable.new_bookmark_empty).getConstantState())){ //R.drawable.bookmark_prime
    //                                if(bookmark.getDrawable().getConstantState()==context.getResources().getDrawable(R.drawable.bookmark_prime).getConstantState()){
                                        if(fragment.saveBookmark(feeds.get(itemposition).getId())){ //CALL API then SAVING BOOKMARK to DB
                                            bookmark.setImageResource(R.drawable.new_bookmark_fill); //bookmark.setImageResource(R.drawable.bookmark_fill);
                                            Log.d(TAG, "onClick: TAG_TAG2 "+Global.isBookmark);
                                        }
                                    }else{
                                        deleteOneBookmark(bookmark, Integer.valueOf(Integer.valueOf(feeds.get(itemposition).getId())));
                                        Log.d(TAG, "onClick: TAG_TAG2 "+Global.isBookmark);
                                    }
                                }
                            }catch(Exception e){
                                Log.d(TAG, "BOOKMARK onClick: Exception_ERROR "+e.getMessage());
                            }
                        }
                        bookmark.setEnabled(true);
                        break;
                    default:
                        if(!Global.isItemClicked){
                            Global.isItemClicked = true;
                            Global.isArticleViewed = false; ////RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
//                            EventBus.getDefault().post(new PostArticleCustom(feeds.get(itemposition).getId())); //
//                            EventBus.getDefault().post(new PostArticleCustom(feeds.get(itemposition).getId(),type)); //
                            //ForBookMark ONLY
                            EventBus.getDefault().post(new PostArticleCustom(feeds.get(itemposition).getId(),type,feeds.get(itemposition).getImage(),feeds.get(itemposition).getTitle(),feeds.get(itemposition).getPubdate()));
                        }
                        break;
                }

            }else {
                Global.ViewDialog alert = new Global.ViewDialog();
                alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
            }

        }
    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.pLoadingMore);
        }
    }

    public void releasePlayer() {
        if (exoPlayer != null) { exoPlayer.release(); exoPlayer = null;  }
        if (exoPlayer2 != null) { exoPlayer2.release(); exoPlayer2 = null;  }
        if (exoPlayer3 != null) { exoPlayer3.release(); exoPlayer3 = null;  }
    }

    public void pauseVideoPlay() {
        if (exoPlayer != null) { exoPlayer.setPlayWhenReady(false);  exoPlayer.getPlaybackState(); }
        if (exoPlayer2 != null) { exoPlayer2.setPlayWhenReady(false);  exoPlayer2.getPlaybackState(); }
        if (exoPlayer3 != null) { exoPlayer3.setPlayWhenReady(false);  exoPlayer3.getPlaybackState(); }
    }


    public void displayType01( RequestOptions mainOption, String mainId, String mainTitle,String mainDate, String mainPhoto, TextView news_title, TextView news_date, ImageView iv_news_photo){

        //for Debugging
        Log.d(TAG, "displayType01: CALLED**  NEWS_ID: "+mainId);
        Log.d(TAG, "displayType01: CALLED**  TITLE: "+mainTitle);
        Log.d(TAG, "displayType01: CALLED**  DATE: "+mainDate);
        Log.d(TAG, "displayType01: CALLED**  PHOTO: "+mainPhoto);
        Log.d(TAG, "displayType01: TITLE "+isValidated(mainTitle));
        Log.d(TAG, "displayType01: DATE "+isValidated(mainDate));
        Log.d(TAG, "displayType01: PHOTO "+isValidated(mainPhoto));

        if (isValidated2(mainTitle, "displayType01 "+mainTitle)) { //if (isValidated(mainTitle)) {
            news_title.setText(mainTitle);
            news_title.setVisibility(VISIBLE);
        } else news_title.setVisibility(INVISIBLE);

        if (isValidated(mainDate)) {
            news_date.setText(mainDate);
            news_date.setVisibility(VISIBLE);
        } else news_date.setVisibility(INVISIBLE);  //

        iv_news_photo.setVisibility(VISIBLE);

        if (isValidated(mainPhoto)) {
            displayImage01(mainId, mainPhoto, mainOption, iv_news_photo);
        } else displayDefaultImage(iv_news_photo);  //Default

    }

    public void displayType25( RequestOptions mainOption, int section, String mainTitle, String mainLabel, String mainPhoto, TextView ad_title, TextView ad_sponsor, ImageView ad_photo){

        //for Debugging
        Log.d(TAG, "displayType25: CALLED**  TITLE: "+mainTitle);
        Log.d(TAG, "displayType25: CALLED**  PHOTO: "+mainPhoto);
        Log.d(TAG, "displayType25: TITLE "+isValidated(mainTitle));
        Log.d(TAG, "displayType25: PHOTO "+isValidated(mainPhoto));


        //androidx
        if (isValidated2(mainTitle, "displayType25 "+mainTitle)) { //if (isValidated(mainTitle)) {
            ad_title.setText(mainTitle);
            ad_title.setVisibility(VISIBLE);
        } else ad_title.setVisibility(INVISIBLE);

        //androidx
        if (isValidated(mainLabel)) {
            ad_sponsor.setText(mainLabel);
            ad_sponsor.setVisibility(VISIBLE);
        } else ad_sponsor.setVisibility(INVISIBLE);


        //androidx
        if (isValidated(mainPhoto)) {
            displayImage25(mainOption, mainPhoto, section, ad_photo);
        } else displayDefaultImage(ad_photo);

    }

    public void displayImage01(String id, String photo, RequestOptions request_option, ImageView photo_container ){
        Glide
                .with(context)
                .load(photo)
                .transition(withCrossFade())
                .apply(request_option)
                .into(photo_container);
        ViewCompat.setTransitionName(photo_container, id); //setTransitionName
    }

    public void displayImage25(RequestOptions request_option, String photo, int section, ImageView photo_container ){
        Glide
                .with(context)
                .load(photo)
                .transition(withCrossFade())
                .apply(request_option)
                .into(photo_container);
        ViewCompat.setTransitionName(photo_container, String.valueOf(section)); //setTransitionName
    }

    public void displayDefaultImage(ImageView iv){
        iv.setImageResource(R.drawable.inquirernewsinfo);

        //REMOVE TEMP
//        switch (sf.getLastSection()) {
//            case 2:
//                iv.setImageResource(R.drawable.inquirersports);
//                break;
//            case 3:
//                iv.setImageResource(R.drawable.inquirerentertainment);
//                break;
//            case 5:
//                iv.setImageResource(R.drawable.inquirertechnology);
//                break;
//            case 6:
//                iv.setImageResource(R.drawable.inquirerglobalnation);
//                break;
//            case 7:
//                iv.setImageResource(R.drawable.inquirerbusiness);
//                break;
//
//            case 8:
//                iv.setImageResource(R.drawable.inquirerlifestyle);
//                break;
//            case 12:
//                iv.setImageResource(R.drawable.inquirerproperty);
//                break;
//
//            case 13:
//                iv.setImageResource(R.drawable.inquireropinion_blue);
//                break;
//
//            case 16:
//                iv.setImageResource(R.drawable.inquirerboardtalk);
//                break;
//
//            case 17:
//                iv.setImageResource(R.drawable.inquirermobility);
//                break;
//            case 19:
//                iv.setImageResource(R.drawable.inquirerrebound);
//                break;
//            default:
//                iv.setImageResource(R.drawable.inquirernewsinfo);
//                break;
//        }
    }

//    public void displayDefaultImage(ImageView iv){
//        switch (sf.getLastSection()) {
////            case 1:  //SAME WITH DEFAULT
////                Glide
////                        .with(context)
////                        .load(R.drawable.inquirernewsinfo)
////                        .into(iv);
////                break;
//            case 2:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirersports)
//                        .into(iv);
//                break;
//            case 3:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirerentertainment)
//                        .into(iv);
//                break;
//            case 5:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirertechnology)
//                        .into(iv);
//                break;
//            case 6:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirerglobalnation)
//                        .into(iv);
//                break;
//            case 7:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirerbusiness)
//                        .into(iv);
//                break;
//
//            case 8:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirerlifestyle)
//                        .into(iv);
//                break;
//            case 12:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirerproperty)
//                        .into(iv);
//                break;
//
//            case 13:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquireropinion_blue)
//                        .into(iv);
//                break;
//
//            case 16:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirerboardtalk)
//                        .into(iv);
//                break;
//
//            case 17:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirermobility)
//                        .into(iv);
//                break;
//            case 19:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirerrebound)
//                        .into(iv);
//                break;
//
//            default:
//                Glide
//                        .with(context)
//                        .load(R.drawable.inquirernewsinfo)
//                        .into(iv);
//        }
//    }

//    private boolean isValidated(String string){
//        try{
//            if(!string.isEmpty()) return true; //Check if Empty
//        }catch(Exception e){
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
//        }finally {
//            if(string!=null) return true;  //Check if null
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
//        }
//        return false;
//    }

    private boolean isValidated(String string){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
            if(string!=null) return true;  //Check if null
        }
        return false;
    }

    //For Debugging Purpose
    private boolean isValidated2(String string, String label){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            if(string!=null) return true;  //Check if null
        }
        return false;
    }

//    private boolean isValidated2(String string, String label){
//        try{
//            if(!string.isEmpty()) return true; //Check if Empty
//        }catch(Exception e){
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error: Label "+label);
//
//        }finally {
//            if(string!=null) return true;  //Check if null
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error: Label "+label);
//
//        }
//        return false;
//    }


    private void setDelayDisplayThumbnail(ImageView thumbNail) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            thumbNail.setVisibility(View.GONE);
//            arrow.setVisibility(View.GONE);
        }, 1000); //1000
    }

    //podcast
//    private void checkVideoSoundPlaying() {
//        AudioManager mAudioManager = (AudioManager) context.getSystemService(AUDIO_SERVICE);
//        if (mAudioManager.isMusicActive()) {
//            EventBus.getDefault().post(new PostPodcastPlaybackButtonCustom(0, "pause")); //CHANGING playBUTTON in frAGMENT
//        }
//    }

    //PODCAST USAGE
    //INSIDE ADAPTER: Monitoring specific viewholder item  if attach to window or not
//    @Override
//    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
//        super.onViewDetachedFromWindow(holder);
//    }
//
//
//    @Override
//    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
//        super.onViewAttachedToWindow(holder);
//
//    }
}



