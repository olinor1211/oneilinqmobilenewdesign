package com.knx.inquirer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.graphics.Typeface;
//import android.hardware.Camera;
//import android.hardware.Camera;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
//import androidx.annotation.Size;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.location.LocationListener;
import com.knx.inquirer.utils.CameraSurfaceView;
import com.knx.inquirer.utils.GPSTracker;
import com.knx.inquirer.utils.Global;

import timber.log.Timber;

import static android.location.LocationManager.*;
import static me.dm7.barcodescanner.core.CameraUtils.getCameraInstance;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class ReportActivity extends AppCompatActivity implements LocationListener{
	private static final String TAG = "ReportActivity";

	//androidx
	private String cameraId;
	protected CameraDevice cameraDevice;
	protected CameraCaptureSession cameraCaptureSessions;
	protected CaptureRequest captureRequest;
	protected CaptureRequest.Builder captureRequestBuilder;
	private Size imageDimension;
	private ImageReader imageReader;
	private File file;
	private boolean mFlashSupported;
	private Handler mBackgroundHandler;
	private HandlerThread mBackgroundThread;

	private TextureView textureView;
	private ImageView cameraImage;
	private ImageView galleryImage;
	private TextView qrText;
	private TextView reportText;
	private TextView cancelText;
	private ImageView imageExit;
	private ImageView imageSwitch;

	private static final SparseIntArray ORIENTATIONS = new SparseIntArray(); //??

	static {
		ORIENTATIONS.append(Surface.ROTATION_0, 90);  //keys,value
		ORIENTATIONS.append(Surface.ROTATION_90, 0);
		ORIENTATIONS.append(Surface.ROTATION_180, 270);
		ORIENTATIONS.append(Surface.ROTATION_270, 180);
	}

	private static final int SELECT_PICTURE = 1;

	//androidx implementation
	private final int REQUEST_TAKE_PHOTO = 02;
	private LocationManager mLocationManager;
	private long LOCATION_REFRESH_TIME = 2000;
	private float LOCATION_REFRESH_DISTANCE = 10;
	private Location location;
	private double latitude=0;
	private double longitude=0;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.report_view);

		try{
			if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}else{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
		}catch(Exception e){
			Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
		}

		qrText = findViewById(R.id.textQr);
		reportText = findViewById(R.id.textReport);
		cancelText = findViewById(R.id.cancelText);
		cameraImage = findViewById(R.id.cameraImage);
		galleryImage = findViewById(R.id.galleryImage);

		textureView = findViewById(R.id.texture);
		textureView.setSurfaceTextureListener(textureListener);
		cameraImage.setOnClickListener(v -> takePicture());

		Typeface tf = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/MalloryMP-Bold.otf");

		//Resolved for: exposed beyond app through ClipData.Item.getUri()
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

        qrText.setTypeface(tf);
        qrText.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

            	   Intent intent = new Intent();
                   intent.setClassName("com.knx.inquirer", "com.knx.inquirer.BridgeActivity");
                   startActivity(intent);

                   finish();
            }
        });
        reportText.setTypeface(tf);
        reportText.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

            }
        });
        cancelText.setTypeface(tf);
        cancelText.setOnClickListener(v -> finish());

        galleryImage.setOnClickListener(v -> {
        		Intent intent = new Intent();
			    intent.setType("image/*");
			    intent.setAction(Intent.ACTION_PICK);
			    startActivityForResult(intent, SELECT_PICTURE);
		});
}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		Bitmap bitmap = null;

		if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK) {
			Uri selectedImage = data.getData();
			File imageFile = new File(getRealPathFromURI(selectedImage)); //getRealPathFromURI

//			//CONVERTING TO bitmap Image COmpress... not working
			InputStream imageStream = null;
			Bitmap bmp = null;
			byte[] byteArray = null;
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			try {
				imageStream = getContentResolver().openInputStream(selectedImage);
				bmp = BitmapFactory.decodeStream(imageStream);
			    bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream); ////For Bitmap Result
//				byteArray = stream.toByteArray(); //REMOVED_TEMP_Reserved_For_Used: For ByteArray Result

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			try {
				stream.close();
				stream = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
//			//SAVING PHOTO
			try {
				//Resize Bitmap
				Bitmap resizedBitmap = getResizedBitmap(bmp, 640);
				//savePhoto ByteArray TEMP_REMOVED
//			    saveByteImage(byteArray);
				//savePhoto Bitmap
				savePhoto(resizedBitmap);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	//SurfaceTextureListener
	TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
		@Override
		public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surfaceTexture, int i, int i1) {
			//open your camera here
			Log.d(TAG, "onSurfaceTextureAvailable: SurfaceTextureListener OPENING the CAMERA1");
			openCamera();
			Log.d(TAG, "onSurfaceTextureAvailable: SurfaceTextureListener OPENING the CAMERA2");

		}
		@Override
		public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surfaceTexture, int i, int i1) {
		}
		@Override
		public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surfaceTexture) {
			return false;
		}
		@Override
		public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surfaceTexture) {
		}
	};



	//CameraDevice.StateCallback
	private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
		@Override
		public void onOpened(@NonNull CameraDevice cameraDev) {
			//This is called when the camera is open
			Log.i(TAG, "StateCallback: stateCallback CAMERA is onOpened");
			cameraDevice = cameraDev;
			createCameraPreview();
		}

		@Override
		public void onDisconnected(@NonNull CameraDevice cameraDevice) {
			Log.i(TAG, "StateCallback: stateCallback CAMERA is onDisconnected");
			cameraDevice.close();
		}

		@Override
		public void onError(@NonNull CameraDevice cameraDevice, int i) {
			Log.e(TAG, "onError: CameraDevice.StateCallback: I:  "+i);
			Log.e(TAG, "onError: CameraDevice.StateCallback: CAMERA_ID: "+cameraDevice.getId());
			Log.e(TAG, "onError: CameraDevice.StateCallback: String: "+cameraDevice.toString());
			cameraDevice.close();
//			cameraDevice = null;
		}
	};

	//getRealPathFromURI exFrom Gallery/Photos
	private String getRealPathFromURI(Uri contentURI) {
		String result;
		Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		if (cursor == null) { // Source is Dropbox or other similar local file path
			result = contentURI.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			result = cursor.getString(idx);
			cursor.close();
		}
		return result;
	}

	//startBackgroundThread
	protected void startBackgroundThread() {
		mBackgroundThread = new HandlerThread("Camera Background");
		mBackgroundThread.start();
		mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
		Log.d(TAG, "startBackgroundThread: CALLED***");

	}

	//stopBackgroundThread
	protected void stopBackgroundThread() {
		mBackgroundThread.quitSafely();
		try {
			mBackgroundThread.join();
			mBackgroundThread = null;
			mBackgroundHandler = null;
		} catch (InterruptedException e) {
			Log.e(TAG, "stopBackgroundThread: InterruptedException: ", e.getCause());
//			e.printStackTrace();
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	protected void takePicture() {
		if (cameraDevice == null) {
			Log.e(TAG, "cameraDevice is null");
			return;
		}
		//CameraManager
		CameraManager manager = null;
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
			manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
		}
		try {
			//Getting CameraCharacteristics
			CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
			android.util.Size[] jpegSizes = null; //Size Array
			if (characteristics != null) {
				jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
			}
			//default H and W
			int width = 640;
			int height = 480;
			if (jpegSizes != null && jpegSizes.length < 0) {
				width = jpegSizes[0].getWidth();
				height = jpegSizes[0].getHeight();
				Log.d(TAG, "takePicture: width: "+ width);
				Log.d(TAG, "takePicture: height: "+ height);
			}
			//
			Log.d(TAG, "takePicture: CameraCharacteristics: "+ characteristics.toString());
			Log.d(TAG, "takePicture: jpegSizes: "+ Arrays.toString(jpegSizes));
			Log.d(TAG, "takePicture: jpegSizeswidth: "+ jpegSizes[0].getWidth());
			Log.d(TAG, "takePicture:jpegSizesheight: "+  jpegSizes[0].getHeight());

			ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
			List<Surface> outputSurfaces = new ArrayList<Surface>(2);
			outputSurfaces.add(reader.getSurface());
			outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));

			final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
			captureBuilder.addTarget(reader.getSurface());
			captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
			// Orientation
			int rotation = getWindowManager().getDefaultDisplay().getRotation();
			captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

			Log.d(TAG, "takePicture: rotation "+ rotation);
			Log.d(TAG, "takePicture: ORIENTATIONS LengthSize "+  ORIENTATIONS.size());
			Log.d(TAG, "takePicture: ORIENTATIONS rotation "+  ORIENTATIONS.get(rotation));
			Log.d(TAG, "takePicture: ORIENTATIONS 1 "+  ORIENTATIONS.get(1));
			Log.d(TAG, "takePicture: ORIENTATIONS SparseIntArray_TO_STRING "+  Arrays.toString(new SparseIntArray[]{ORIENTATIONS}));
			

			final File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/inq_photo.jpg");

			//OnImageAvailableListener
			ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
				@Override
				public void onImageAvailable(ImageReader reader) {
					Image image = null;
					try {
						image = reader.acquireLatestImage();
						ByteBuffer buffer = image.getPlanes()[0].getBuffer();
						byte[] bytes = new byte[buffer.capacity()];
						buffer.get(bytes);
						//SAVE PHOTO
						save(bytes);
//						Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						if (image != null) {
							image.close();
						}
					}
				}

				private void save(byte[] bytes) throws IOException {
					OutputStream output = null;
					try {
						output = new FileOutputStream(file);
						output.write(bytes);

					} finally {
						if (null != output) {
							output.close();
						}
					}
				}
			};

			//SETTING OnImageAvailableListener
			reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);

			//CameraCaptureSession CaptureCallback captureListener
			final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
				@Override
				public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
					super.onCaptureCompleted(session, request, result);
					Log.d(TAG, "onCaptureCompleted: ");
//					Toast.makeText(ReportActivity.this, "CaptureCallback captureListener: onCaptureCompleted:Saved:" + file, Toast.LENGTH_SHORT).show();
					Global.reportPhotoPath = file.getAbsolutePath();
					sendEmail();
//					createCameraPreview();
				}
			};

			//CREATING CaptureSession
			cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
				@Override
				public void onConfigured(CameraCaptureSession session) {
					try {
						session.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
					} catch (CameraAccessException e) {
						Log.e(TAG, "onConfigured: createCaptureSession ", e.getCause());
//						e.printStackTrace();
					}
				}

				@Override
				public void onConfigureFailed(CameraCaptureSession session) {
				}
			}, mBackgroundHandler);

		} catch (CameraAccessException e) {
			Log.e(TAG, "takePicture: createCaptureSession ", e.getCause());
//			e.printStackTrace();
		}
	}




	//createCameraPreview
	protected void createCameraPreview() {
		try {
			SurfaceTexture texture = textureView.getSurfaceTexture();
			assert texture != null;
			texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
			Surface surface = new Surface(texture);
			captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
			captureRequestBuilder.addTarget(surface);
			cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
				@Override
				public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
					//The camera is already closed
					if (null == cameraDevice) {
						return;
					}
					// When the session is ready, we start displaying the preview.
					cameraCaptureSessions = cameraCaptureSession;
					updatePreview();
				}

				@Override
				public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
					Log.d(TAG, "onConfigureFailed: change ");
//					Toast.makeText(ReportActivity.this, "Configuration change", Toast.LENGTH_SHORT).show();
				}
			}, null);
		} catch (CameraAccessException e) {
			Log.e(TAG, "createCameraPreview: CameraAccessException: ",e.getCause() );
//			e.printStackTrace();
		}
	}

	//
	private void saveByteImage(byte[] bytes) throws IOException {
		Log.d(TAG, "saveByteImage: Global.reportPhotoPath "+ Global.reportPhotoPath);
		OutputStream output = null;
		try {
			output = new FileOutputStream(Global.reportPhotoPath);
			output.write(bytes);
		} finally {
			if (output != null ) {
				output.close();
			}
		}
	}

	//openCamera
	private void openCamera() {
		CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
		try {
			cameraId = manager.getCameraIdList()[0];
			CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
			StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
			assert map != null;
			imageDimension = map.getOutputSizes(SurfaceTexture.class)[0]; //imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
			// Add permission for camera and let user grant the permission
			manager.openCamera(cameraId, stateCallback, null);
			Log.d(TAG, "openCamera: manager.openCamera() CALLED** ");


		} catch (CameraAccessException e) {
			Log.e(TAG, "openCamera: CameraAccessException: ",e.getCause() );
//			e.printStackTrace();
		}

		Log.d(TAG, "openCamera END of Method CALLED***");
	}


	//updatePreview
	protected void updatePreview() {
		if (null == cameraDevice) {
			Log.e(TAG, "updatePreview error, return");
		}
		captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
		try {
			cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
		} catch (CameraAccessException e) {
			Log.e(TAG, "updatePreview: CameraAccessException: ",e.getCause() );
//			e.printStackTrace();
		}
	}

	//closeCamera
	private void closeCamera() {
		if (null != cameraDevice) {
			cameraDevice.close();
			cameraDevice = null;
		}
		if (null != imageReader) {
			imageReader.close();
			imageReader = null;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.e(TAG, "onResume");
		startBackgroundThread();
		if (textureView.isAvailable()) {
			openCamera();
			Log.d(TAG, "onResume: openCamera CALLED***");
		} else {
			textureView.setSurfaceTextureListener(textureListener);
			Log.d(TAG, "onResume: openCamera NOT_CALLED***");
		}
	}

	@Override
	protected void onPause() {
		Log.e(TAG, "onPause");
		//closeCamera();
		stopBackgroundThread();
		super.onPause();
	}



	public void deletePhoto()
	{
		File sd = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		if(sd.isDirectory() && sd.exists()){
			File[] fname = sd.listFiles();
			for (File tempFile: fname) {
				tempFile.delete();
			}
		}
	}

	public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
		int width = image.getWidth();
		int height = image.getHeight();

		float bitmapRatio = (float)width / (float) height;
		if (bitmapRatio > 1) {
			width = maxSize;
			height = (int) (width / bitmapRatio);
		} else {
			height = maxSize;
			width = (int) (height * bitmapRatio);
		}
		return Bitmap.createScaledBitmap(image, width, height, true);
	}

	public void sendEmail()
	{
//		if(GetLocation()){
//			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
//			emailIntent.setType("application/image");
//			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"tips@inquirer.net"});
//			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Inquirer Report");
//			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "NAME:\nLOCATION: " + Global.location + "\nDATE: " + GetDate() + "\nTIME: " + GetTime() + "\nSTORY:\n\nDISCLAIMER:  When you submit your content, you give INQUIRER.net permission to use such content and you own or was given ownership to use and distribute and allow INQUIRER.net to use it in full. You hereby grant to INQUIRER.net a royalty-free, irrevocable, perpetual, worldwide and fully sublicensable license to use, reproduce, modify, adapt, publish, translate, distribute and display in whole or in part in any form through its publishing platforms.\n\n");
//			emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//			emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+Global.reportPhotoPath)); //shouldBe: "file:///storage.."   //emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///mnt/sdcard/output.png"));
//			startActivity(Intent.createChooser(emailIntent, "Please send story via email."));
//		}

		if(GetLocation()){
			try{
				sendViaGmail();
				Log.d(TAG, "sendEmail: sendViaGmail ");
			}catch(Exception e){
				Log.d(TAG, "sendEmail: Exception_ERROR: "+e.getMessage());
				Log.d(TAG, "sendEmail: Exception_ERROR: Going to sendViaOthers");
				sendViaOthers();
			}
		}
	}

	private void sendViaGmail(){
		final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		final PackageManager pm = getPackageManager(); //Create Instance of PackageManager
		final List<ResolveInfo> matches = pm.queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY); /// GETTING ALL IntentActivities in the DEVICE
		Log.d(TAG, "sendViaGmail: ResolveInfo "+ matches);
		ResolveInfo best = null;
		for (final ResolveInfo info : matches){
			if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail")){
				Log.d(TAG, "sendViaGmail: ResolveInfo "+ info);
//                sample result info = ResolveInfo{1a81832 com.google.android.gm/.ComposeActivityGmailExternal m=0x608000}
				// sample result in Intent.ACTION_VIEW for ResolveInfo ResolveInfo{67eb0f6 com.google.android.gm/.EmlViewerActivityGmail m=0x608000}
				best = info;
				break;
			}
		}

		if (best != null){
			Log.d(TAG, "sendViaGmail: ResolveInfo "+ best.activityInfo.packageName); //RESULT: com.google.android.gm
			Log.d(TAG, "sendViaGmail: ResolveInfo "+ best.activityInfo.name); //RESULT: com.google.android.gm.ComposeActivityGmailExternal  .. Intent.ACTION_VIEW = com.google.android.gm.EmlViewerActivityGmail
			Log.d(TAG, "sendViaGmail: ResolveInfo going to BEST 1 ");
			intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
		}else{
			Log.d(TAG, "sendViaGmail: ResolveInfo going to BEST 2 ");
			intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmailExternal");
		}
		//
		intent.setType("message/rfc822"); // intent.setType("text/plain");  intent.setType("*/*");
		intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"tips@inquirer.net"}); //RECIPIENT = SAME intent.setData(Uri.parse("mailto:"+Global.EMAIL)); SEND_TO
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Inquirer Report");
		intent.putExtra(android.content.Intent.EXTRA_TEXT, "NAME:\nLOCATION: " + Global.location + "\nDATE: " + GetDate() + "\nTIME: " + GetTime() + "\nSTORY:\n\nDISCLAIMER:  When you submit your content, you give INQUIRER.net permission to use such content and you own or was given ownership to use and distribute and allow INQUIRER.net to use it in full. You hereby grant to INQUIRER.net a royalty-free, irrevocable, perpetual, worldwide and fully sublicensable license to use, reproduce, modify, adapt, publish, translate, distribute and display in whole or in part in any form through its publishing platforms.\n\n");
		intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+Global.reportPhotoPath));
		startActivity(intent);
		Log.d(TAG, "sendViaGmail: ResolveInfo going to BEST 3 ");
	}

	private void sendViaOthers(){
		try{
			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
			emailIntent.setType("application/image");
			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"tips@inquirer.net"});
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Inquirer Report");
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "NAME:\nLOCATION: " + Global.location + "\nDATE: " + GetDate() + "\nTIME: " + GetTime() + "\nSTORY:\n\nDISCLAIMER:  When you submit your content, you give INQUIRER.net permission to use such content and you own or was given ownership to use and distribute and allow INQUIRER.net to use it in full. You hereby grant to INQUIRER.net a royalty-free, irrevocable, perpetual, worldwide and fully sublicensable license to use, reproduce, modify, adapt, publish, translate, distribute and display in whole or in part in any form through its publishing platforms.\n\n");
			emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+Global.reportPhotoPath)); //shouldBe: "file:///storage.."   //emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///mnt/sdcard/output.png"));
			startActivity(Intent.createChooser(emailIntent, "Please send story via email."));
		}catch(Exception e){
			Log.d(TAG, "sendViaOthers: Exception_ERROR: "+e.getMessage());
		}
	}

	//GETTING the LOCATION
	public boolean GetLocation()
	{
		try {
			GPSTracker gps = new GPSTracker(this);
			mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
			// getting GPS status
			boolean isGPSEnabled = mLocationManager.isProviderEnabled(GPS_PROVIDER);
			// getting network status
			boolean isNetworkEnabled = mLocationManager.isProviderEnabled(NETWORK_PROVIDER);
			Log.d(TAG, "GetLocation: isGPSEnabled: "+ isGPSEnabled);
			Log.d(TAG, "GetLocation: isNetworkEnabled: "+ isNetworkEnabled);
			Log.d(TAG, "GetLocation: canGetLocation: "+ gps.canGetLocation());

			if(gps.canGetLocation()){
				//WORKING FINE
				if (isNetworkEnabled) {
					mLocationManager.requestLocationUpdates(NETWORK_PROVIDER,LOCATION_REFRESH_TIME,LOCATION_REFRESH_DISTANCE, this::onLocationChanged);
//				//Optional: use onLocationChanged instead to GET latitude and longitude
					if (mLocationManager != null) {
						location = mLocationManager.getLastKnownLocation(NETWORK_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
							getAddress(latitude, longitude); //
							Log.d(TAG, "GetLocation: LONGTITUDE NETWORK : "+ longitude);
						}
					}
				}else{
					//SECONDARY Option
					if (isGPSEnabled) {
						mLocationManager.requestLocationUpdates(GPS_PROVIDER,LOCATION_REFRESH_TIME,LOCATION_REFRESH_DISTANCE, this::onLocationChanged);
						if (mLocationManager != null) {
							location = mLocationManager.getLastKnownLocation(GPS_PROVIDER);
							if (location != null) {
								latitude = location.getLatitude();
								longitude = location.getLongitude();
								Log.d(TAG, "GetLocation: LONGTITUDE GPS : "+ longitude);
								getAddress(latitude, longitude); //
							}
						}
					}
				}
				//
				return true;
			}else{
				Toast.makeText(this, "Please turn on Location Services!", Toast.LENGTH_LONG).show();
				Handler handler = new Handler();
				handler.postDelayed(() -> {
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivity(intent);
//					finish();
				}, 3000);
				return false;
			}
	    }
		catch (Exception e)
		{
			Log.d(TAG, "GetLocation: Exception_Error: "+ e.getMessage());
			Toast.makeText(this, "Unable to get location!", Toast.LENGTH_LONG).show();
		}
		return true;
	  }
    //LocationListener
	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		getLatitude();
		getLongitude();
		Log.d(TAG, "onLocationChanged: GetLocation : LATITUDE "+ getLatitude());
		Log.d(TAG, "onLocationChanged: GetLocation : LONGTITUDE  "+ getLongitude());

	}

	//androidx
	private void getAddress(double latitude, double longitude ){
		if (latitude!=0  && longitude!=0){
			Log.d(TAG, "GetLocation: getAddress latitude: "+latitude);
			Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
			List<Address> addresses;
			try {
				addresses = gcd.getFromLocation(latitude, longitude, 1);
				if (addresses.size() > 0) {
					Global.location = addresses.get(0).getLocality(); //Get Address Locality
					String add = addresses.get(0).getAddressLine(0); //Get Address Location
					Log.d(TAG, "GetLocation: CITY/TOWN: "+Global.location);
					Log.d(TAG, "GetLocation: ADDRESS: "+add);
				}
				Log.d(TAG, "GetLocation: addresses "+ addresses.size());
				Log.d(TAG, "GetLocation: addresses "+ addresses.get(0));
			} catch (IOException e) {
				Log.d(TAG, "getAddress: IOException_Error "+e.getMessage());
			}
		}
	}

	//androidx
	private double getLatitude(){
		if(location != null){
			latitude = location.getLatitude();
		}
		return latitude;
	}

	//androidx
	private double getLongitude(){
		if(location != null){
			longitude = location.getLongitude();
		}
		return longitude;
	}

	  private String GetDate()
	  {
		  String dateStr = "";
		  Date date = new Date();
		  SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yyyy");
		  dateStr = dateFormat.format(date);
		  return dateStr;
	  }

	  private String GetTime()
	  {
		  String timeStr = "";
		  Date date = new Date();
		  SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");
		  timeStr = dateFormat.format(date);
		  return timeStr;
	  }

//    //callback
//	Camera.ShutterCallback shutterCallback = () -> Log.d(TAG, "onShutter'd");
//	//callback
//	PictureCallback rawPictureCallback = (data, camera) -> {
//		Log.d(TAG, "onPictureTaken - raw");
//	};
//	//callback
//	PictureCallback jpegPictureCallback = (data, camera) -> Log.d(TAG, "onPictureTaken - jpeg");


	////TAKE PICTURE CAMERA  .. SAVE bitmap to FILE..with throws IOException
	private void savePhoto( Bitmap bitmap ) throws IOException {
//		String imageFileName = "inquirer_photo";
//		File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		File imageFile = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/inq_gallery_photo.jpg");
		try {
//			File imageFile = File.createTempFile(imageFileName, ".jpg", storageDir); //creating the image from fileDirectory //
			FileOutputStream fos = new FileOutputStream(imageFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);  //compress bitmap
			fos.flush();
			fos.close();
			//the last photo taken
			String path = imageFile.getAbsolutePath();
			Global.reportPhotoPath = path;
			//
			sendEmail();

		} catch (IOException e) {
			Log.d(TAG, "savePhoto: Exception_Error: "+ e.getMessage());
		}
	}

//	public void onResume() {
//		super.onResume();
//		Log.d(TAG, "onResume: CALLED **");
//		cameraSurfaceView = new CameraSurfaceView(this);
//		mCamera = cameraSurfaceView.getCamera();
//		mFrameLayoutPreview.addView(cameraSurfaceView);
//	}
//
//	public void onPause() {
//		super.onPause();
//		if (mCamera != null) {
//			mCamera.stopPreview();
//		}
//	}
}

//RESERVED
/*		imageSwitch = findViewById(R.id.imageSwitch);
	    imageSwitch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click

             	try {
                	camera.stopPreview();
                    camera.release();

                    if(cameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                    {
                    	PrimeGlobal.rotateSw = 1;
                        cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                    }
                    else
                    {
                    	PrimeGlobal.rotateSw = 0;
                        cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                    }

        			// open the camera
        			camera = Camera.open(cameraId);
        			camera.setDisplayOrientation(90);

        		} catch (RuntimeException e) {
        			// check for exceptions
        			System.err.println(e);
        			return;
        		}
        		Camera.Parameters param;
        		param = camera.getParameters();

        		// modify parameter
				List<String> focusModes = param.getSupportedFocusModes();
				if(focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)){
					param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
				} else
				if(focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)){
					param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
				}

				camera.setParameters(param);
        		try {
        			// The Surface has been created, now tell the camera where to draw
        			// the preview.

        			camera.setPreviewDisplay(surfaceHolder);
        			camera.startPreview();
        		} catch (Exception e) {
        			// check for exceptions
        			System.err.println(e);
        			return;
        		}
            }
        });*/
