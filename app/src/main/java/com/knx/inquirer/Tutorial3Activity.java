package com.knx.inquirer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class Tutorial3Activity extends AppCompatActivity {
	private static final String TAG = "Tutorial3Activity";
	
	private ImageView imageView;
 	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.tutorial2_view);

		try{
			if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}else{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
		}catch(Exception e){
			Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
		}

	    imageView = (ImageView)findViewById(R.id.imageView);
	    
	    imageView.setOnTouchListener(new View.OnTouchListener() {  
            public boolean onTouch(View v, MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN){
				ActivityCompat.requestPermissions(Tutorial3Activity.this,
						new String[]{
								Manifest.permission.ACCESS_FINE_LOCATION
						}, 1);
              }  
              return true;
            }
        });
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case 1: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED)
				{
					Intent intent = new Intent();
					intent.setClassName("com.knx.inquirer", "com.knx.inquirer.ReportActivity");
					startActivity(intent);
					finish();
				} else {
					Toast.makeText(Tutorial3Activity.this, "Permission denied to access your location", Toast.LENGTH_SHORT).show();
				}
				return;
			}

		}
	}
}
