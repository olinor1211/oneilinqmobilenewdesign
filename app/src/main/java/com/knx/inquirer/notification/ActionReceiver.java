package com.knx.inquirer.notification;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

//Note: This notification Receiver will be trigger from FirebaseMessaging
public class ActionReceiver extends BroadcastReceiver {
    @SuppressLint("MissingPermission")
    @Override
    public void onReceive(Context context, Intent intent) {
        //get intent from FirebaseMessagingService getBroadcast
        String action = intent.getStringExtra("action_share");
        String title = intent.getStringExtra("share_title");
        String content = intent.getStringExtra("share_content");

        if(action.equals("notifShare")){
            performShare(context, title, content);
        }
        //This is used to close the notification tray
        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(it);

    }

    private void performShare(Context context, String title, String content) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_TEXT, title + "\nvia Inquirer Mobile: " + content);

        try {
            context.startActivity(Intent.createChooser(emailIntent, "Share with..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There is no share client installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
