package com.knx.inquirer.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.knx.inquirer.R;
import com.knx.inquirer.WebDeepLinkActivity;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.utils.Global;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
//Note: PrimeNotification using firebase ...the FirebaseToken is get from Splash
//Info: The onMessageReceived method will receive all the Messages from FirebaseMessagingService first. Then use this to send notification.
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private DatabaseHelper helper;
//    private Intent toLaunch;

    private static final String TAG = "FirebaseMessagingServic";


    //NOte: To get new token
    //message received from firebase/notification then save into the database
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
    Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FirebaseMessagingService_onMessageReceived_CALLED****");
//        getPrefernceHelperInstace().setBoolean(reactAppContext, ENABLE_NOTIFICATION, showNotifcation);
        // Check if message contains a NOTIFICATION payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message PrimeNotification Body: " + remoteMessage.getNotification().getBody());
            Log.d(TAG, "Message PrimeNotification Title: " + remoteMessage.getNotification().getTitle());
            Log.d(TAG, "Message PrimeNotification Icon: " + remoteMessage.getNotification().getIcon());
            Log.d(TAG, "Message PrimeNotification Link: " + remoteMessage.getNotification().getLink());

//            NotificationModel nmodel = new NotificationModel( remoteMessage.getData().get("image"), remoteMessage.getData().get("link"), remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), remoteMessage.getData().get("date"), 0);
//            helper.insertNotification(nmodel);
        }

        // Check if message contains a DATA payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "onMessageReceived: NOTIFICATION DATA: " + remoteMessage.getData());

            //Get the newsId from the link and putExtra to intent
            //Fixing bugs on notification badge counter for unread articles. (Ref. sir Anjo’s concern #1.)
            String newsId = "";
            try{
                int li = remoteMessage.getData().get("link").indexOf("/"); //int li = link.indexOf("/");
                newsId = remoteMessage.getData().get("link").substring(li+1);
            }catch(Exception e){
                Log.d(TAG, "onMessageReceived: NOTIFICATION Exception_Error: "+e.getMessage());
            }

            //showNotificationMessage Method
            showNotificationMessage(remoteMessage.getData().get("title") ,remoteMessage.getData().get("body"), remoteMessage.getData().get("image"), remoteMessage.getData().get("link"), remoteMessage.getData().get("date"));
            //add notification to java object
            NotificationModel nmodel = new NotificationModel( newsId, remoteMessage.getData().get("image"), remoteMessage.getData().get("link"), remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), Global.convertToDate(remoteMessage.getData().get("date")), 0);
//            NotificationModel nmodel = new NotificationModel( newsId, remoteMessage.getData().get("image"), remoteMessage.getData().get("link"), remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), remoteMessage.getData().get("date"), 0);
            //save modelObject to database
            helper.insertNotification(nmodel);
        }
    }

    //No new token here, Use  observeToken(){FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener()...} then observeToken().subscribe(new Observer<String>() {
    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FirebaseMessagingService_onNewToken_GOT_NEW_TOKEN****:"+token);
    }

    private void showNotificationMessage(String title, String message, String imgUrl, String link, String timestamp) {
        //NotificationManager service
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //NotificationManager builder
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, getResources().getString(R.string.default_notification_channel_id));

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            //NotificationChannel
            NotificationChannel mChannel = new NotificationChannel(getResources().getString(R.string.default_notification_channel_id), getResources().getString(R.string.default_notification_channel_name), NotificationManager.IMPORTANCE_HIGH);
            mChannel.enableLights(true);
            mChannel.enableVibration(true);
            mChannel.setShowBadge(true);  //
            mChannel.setLightColor(Color.parseColor("#4078a7"));
            //if notificationManager !=null
            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }else{
            mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
        }

        //This is the Pending Intent for Share Action button  ..ActionReceiver  intent to ActionReceiver
        Intent intentActionShare = new Intent(getApplicationContext(), ActionReceiver.class);
        intentActionShare.putExtra("action_share", "notifShare");
        intentActionShare.putExtra("share_title", title);
        intentActionShare.putExtra("share_content", !link.isEmpty() ? link : message );
        //PendingIntent getBroadcast
        PendingIntent pendingIntentShare = PendingIntent.getBroadcast(getApplicationContext(), (int)System.currentTimeMillis(), intentActionShare, 0);
        NotificationCompat.Action actionShare = new NotificationCompat.Action (R.drawable.share, "Share", pendingIntentShare);

//        ..getActivity intent to MainActivity
        Intent toLaunch = new Intent(getApplicationContext(), WebDeepLinkActivity.class);
        toLaunch.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        toLaunch.putExtra("newsLink", link);
        toLaunch.putExtra("newsTitle", title);
        toLaunch.putExtra("newsType", "0");


        //PendingIntent getActivity
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), (int)System.currentTimeMillis(), toLaunch, PendingIntent.FLAG_UPDATE_CURRENT);
//        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), (int)System.currentTimeMillis(), toLaunch, 0);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Bitmap notifIcon = null;
        if(!imgUrl.isEmpty()){
            notifIcon = createBitmapFromUrl(imgUrl);
        }else{
            notifIcon = createBitmapFromDrawable();
        }

//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, getResources().getString(R.string.default_notification_channel_id));
        mBuilder.setSmallIcon(R.mipmap.ic_launcher); //image
        mBuilder.setLargeIcon(notifIcon);  //image
        mBuilder.setContentTitle(title); //title
        mBuilder.setContentText(message);  //message
        mBuilder.setStyle(new NotificationCompat.BigPictureStyle()  //image style
                        .bigPicture(notifIcon)
                        .bigLargeIcon(null));
        mBuilder.setAutoCancel(true);
        mBuilder.setSound(defaultSoundUri);
        mBuilder.setLights(Color.parseColor("#4078a7"), 3000, 3000);
        mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
//        mBuilder.addAction(actionShare); //Action  ..remove temporarily
        mBuilder.setContentIntent(pendingIntent);

        notificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        helper = new DatabaseHelper(this);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FirebaseMessagingService_CLASS_CALLED****");
    }

    //createBitmapFromUrl
    public static Bitmap createBitmapFromUrl(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    //createBitmapFromDrawable
    public Bitmap createBitmapFromDrawable() {
        Bitmap bitmap = null;
        bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        return bitmap;
    }
}
