package com.knx.inquirer.models;

public class BookmarksModel {
    public String filename;
    public String photo;
    public String title;
    public String type;
    public String id;
    public String date;
    public String author;

    public BookmarksModel(){

    }
}
