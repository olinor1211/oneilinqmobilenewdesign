package com.knx.inquirer.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "News")
public class NewsModel {
    @PrimaryKey
    @SerializedName("id")
    @NonNull
    private String id;

    @SerializedName("type")
    private String type;

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("pubdate")
    private String pubdate;

    @SerializedName("link")
    public String link;

    @SerializedName("custom")
    public String label;

    @SerializedName("section")
    public String section;

    //Empty Constructor

    //Testing purposes Constructor not empty
    public NewsModel(String id, String type ,String title, String image, String pubdate, String link, String label, String section) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.image = image;
        this.pubdate = pubdate;
        this.link = link;
        this.label = label;
        this.section = section;

    }


    //Getters and Setters

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubdate() {
        return pubdate;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    //???
    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
        } else {
            NewsModel news = (NewsModel) object;
            if (this.id.equals(news.getId())) {
                result = true;
            }
        }
        return result;
    }
}
