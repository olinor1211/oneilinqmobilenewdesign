package com.knx.inquirer.models;

import com.google.gson.annotations.SerializedName;

public class MyOption {
    @SerializedName("option")
    private int option;

    public int getOption() {
        return option;
    }
    public void setOption(int option) {
        this.option = option;
    }
}
