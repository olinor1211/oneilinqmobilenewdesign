package com.knx.inquirer.models;

public class PostArticleCustom {

    private String itempos="";
    private String newsType="";
    private String newsPhoto="";
    private String newsTitle="";
    private String newsDate="";


    public PostArticleCustom(String itempos) {
        this.itempos = itempos;
    }

    public PostArticleCustom(String itempos, String newsType) {
        this.itempos = itempos;
        this.newsType = newsType;
        this.newsPhoto = "";
        this.newsTitle = "";
        this.newsDate = "";
    }

    public PostArticleCustom(String itempos, String newsType, String newsPhoto, String newsTitle, String newsDate) {
        this.itempos = itempos;
        this.newsType = newsType;
        this.newsPhoto = newsPhoto;
        this.newsTitle = newsTitle;
        this.newsDate = newsDate;
    }

    public String getItempos() {
        return itempos;
    }
    public String getNewsType() {
        return newsType;
    }

    public String getNewsPhoto() {
        return newsPhoto;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public String getNewsDate() {
        return newsDate;
    }
}
