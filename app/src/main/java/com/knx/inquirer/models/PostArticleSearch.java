package com.knx.inquirer.models;

public class PostArticleSearch {

    public final String itempos;

    public PostArticleSearch(String itempos) {
        this.itempos = itempos;
    }

    public String getItempos() {
        return itempos;
    }
}
