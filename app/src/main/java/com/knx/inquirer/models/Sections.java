package com.knx.inquirer.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Sections {

    @SerializedName("id")
    private String id;  // ID is not int ; RESOLVED: change to STRING. ERROR: API_ERROR_MESSAGE java.lang.IllegalStateException: Expected BEGIN_OBJECT but was STRING at line 1 column 1

    @SerializedName("title")
    private String title;

    @SerializedName("banner")
    public Banner banner;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Banner getBanner() {
        return banner;
    }

}


