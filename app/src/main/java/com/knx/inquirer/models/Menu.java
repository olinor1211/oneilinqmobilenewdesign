package com.knx.inquirer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    @SerializedName("iOS")
    private Double iOS;

    @SerializedName("Android")
    private Double android;

    @SerializedName("API")
    private Integer aPI;

    @SerializedName("key")
    private String key;
//
    //with deep node value
    @SerializedName("sections")
    private ArrayList<Section> sections = null;

    @SerializedName("featured")
    private ArrayList<Featured> featured = null;

    public Double getIOS() {
        return iOS;
    }

    public void setIOS(Double iOS) {
        this.iOS = iOS;
    }

    public Double getAndroid() {
        return android;
    }

    public void setAndroid(Double android) {
        this.android = android;
    }

    public Integer getAPI() {
        return aPI;
    }

    public void setAPI(Integer aPI) {
        this.aPI = aPI;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<Section> getSections() {
        return sections;
    }

    public void setSections(ArrayList<Section> sections) {
        this.sections = sections;
    }

    public ArrayList<Featured> getFeatured() {
        return featured;
    }

    public void setFeatured(ArrayList<Featured> featured) {
        this.featured = featured;
    }

    }

