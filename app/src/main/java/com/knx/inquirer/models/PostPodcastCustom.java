package com.knx.inquirer.models;

public class PostPodcastCustom {

    private int position = 0;
    private String podcastUrl = "";
    private String title = "";
    private int viewHolder = 0;


    public PostPodcastCustom(int position, String podcastUrl, String title, int viewHolder) {
        this.position = position;
        this.podcastUrl = podcastUrl;
        this.title = title;
        this.viewHolder = viewHolder;
    }
    public int getPosition() {
        return position;
    }

    public String getPodcastUrl() {
        return podcastUrl;
    }

    public String getTitle() {
        return title;
    }
    public int getViewHolder() {
        return viewHolder;
    }
}
