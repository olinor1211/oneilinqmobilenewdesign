package com.knx.inquirer.models;

public class PostArticleTrending {

    public final String itempos;

    public PostArticleTrending(String itempos) {
        this.itempos = itempos;
    }

    public String getItempos() {
        return itempos;
    }
}
