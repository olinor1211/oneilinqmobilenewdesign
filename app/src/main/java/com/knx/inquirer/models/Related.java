package com.knx.inquirer.models;

import com.google.gson.annotations.SerializedName;

public class Related {
    @SerializedName("id")
    private String id;
    @SerializedName("image")
    private String image;
    @SerializedName("title")
    private String title;
    @SerializedName("pubdate")
    private String pubdate;

    public Related(String id, String image, String title, String pubdate) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.pubdate = pubdate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }

    public String getId() {
        return id;
    }
    public String getImage() {
        return image;
    }
    public String getTitle() {
        return title;
    }
    public String getPubdate() {
        return pubdate;
    }

}
