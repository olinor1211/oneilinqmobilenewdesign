package com.knx.inquirer.models;

import com.google.gson.annotations.SerializedName;

public class MenuAdResponse {

    @SerializedName("menu")
    MenuAd menu;

    public MenuAd getMenu() {
        return menu;
    }

    public void setMenu(MenuAd menu) {
        this.menu = menu;
    }
}