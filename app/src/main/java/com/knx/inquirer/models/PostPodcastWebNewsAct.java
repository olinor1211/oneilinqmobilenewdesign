package com.knx.inquirer.models;

public class PostPodcastWebNewsAct {

    private String title = "";
    private String playBack = "";
    private Boolean isPlayed = false;
    private int viewHolder = 0;

    public PostPodcastWebNewsAct(String title, Boolean isPlayed, int viewHolder, String playBack) {  //, Boolean isPlayed
        this.title = title;
        this.isPlayed = isPlayed;
        this.viewHolder = viewHolder;
        this.playBack = playBack;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getPlayed() {
        return isPlayed;
    }

    public int getViewHolder() {
        return viewHolder;
    }

    public String getPlayBack() {
        return playBack;
    }
}



    //    private int position = 0;
////    private String podcastUrl = "";
//    private String title = "";
////    private int viewHolder = 0;
//
//
//    public PostPodcastWebNewsAct(int position, String podcastUrl, String title, int viewHolder) {
////        this.position = position;
////        this.podcastUrl = podcastUrl;
//        this.title = title;
////        this.viewHolder = viewHolder;
//    }
////    public int getPosition() {
////        return position;
////    }
//
////    public String getPodcastUrl() {
////        return podcastUrl;
////    }
//
//    public String getTitle() {
//        return title;
//    }
////    public int getViewHolder() {
////        return viewHolder;
////    }
