package com.knx.inquirer.models;

public class PostPodcastPlaybackButtonCustom {

    private int viewHolder = 0;
    private String playBack = "";

    public PostPodcastPlaybackButtonCustom(int viewHolder, String playBack) {
        this.viewHolder = viewHolder;
        this.playBack =  playBack;
    }

    public int getViewHolder() {
        return viewHolder;
    }

    public String getPlayBack() {
        return playBack;
    }
}

