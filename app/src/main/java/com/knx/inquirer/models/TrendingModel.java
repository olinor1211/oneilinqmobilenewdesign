package com.knx.inquirer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrendingModel {
    @SerializedName("id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("pubdate")
    private String pubdate;
    @SerializedName("link")
    public String link;

    public TrendingModel(String id, String type ,String title, String image, String pubdate, String link) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.image = image;
        this.pubdate = pubdate;
        this.link = link;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPubdate() {
        return pubdate;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
