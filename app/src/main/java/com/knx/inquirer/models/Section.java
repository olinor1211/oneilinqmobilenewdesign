package com.knx.inquirer.models;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@Entity(tableName = "Sections")
public class Section {

    @PrimaryKey
    @SerializedName("id")
    @NonNull
    private int id;
    @SerializedName("title")
    private String title;

    @SerializedName("url")
    private String url;

    @SerializedName("color")
    private String color;

    private boolean isSelected;

    public Section(boolean isSelected) {
        this.isSelected = isSelected;
    }

    //for mobility
    @Ignore
    public Section(int id, String title, String url, String color, boolean isSelected) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.color = color;
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
