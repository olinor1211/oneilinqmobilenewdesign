package com.knx.inquirer.models;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.knx.inquirer.utils.SharedPrefs;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "BookmarksNew")
public class BookmarksNewModel {
 //Dont autoGenerate primary key to make onConflict = OnConflictStrategy.REPLACE take effect.. making newsId as primary key
//    @PrimaryKey(autoGenerate = true)
    //11 items
    @PrimaryKey
    @NonNull
    private int newsId;
    private int id;
    private String title;
    private String mainImage;
    private String type;
    private String date;
    private String content1;
    private String image1;
    private String content2;
    private String author;
    private String custom;
    private String bannerSectionId;
    private String sectionName;

    @Ignore
    public BookmarksNewModel(int newsId, int id, String title, String mainImage, String type, String date, String content1, String image1, String content2, String author, String custom, String bannerSectionId, String sectionName) {
        this.newsId = newsId;
        this.title = title;
        this.mainImage = mainImage;
        this.type = type;
        this.date = date;
        this.content1 = content1;
        this.image1 = image1;
        this.content2 = content2;
        this.author = author;
        this.custom = custom;
        this.bannerSectionId = bannerSectionId;
        this.sectionName = sectionName;
        this.id = id;
    }

    public BookmarksNewModel(int newsId, int id, String type, String image, String title, String date, String content) {
        this.newsId = newsId;
        this.title = title;
        this.mainImage = image;
        this.content1 = content;
        this.type = type;
        this.date = date;
        this.image1 = image;
        this.content2 = "";
        this.author = "";
        this.custom = "";
        this.bannerSectionId = "";
        this.sectionName = "";
        this.id = id;
    }

    public BookmarksNewModel(int newsId, int id, String content1) {
        this.newsId = newsId;
        this.id = id;
        this.content1 = content1;
        this.mainImage = "";
        this.type = "";
        this.date = "";
        this.image1 = "";
        this.content2 = "";
        this.author = "";
        this.custom = "";
        this.bannerSectionId = "";
        this.sectionName = "";
    }

    public int getNewsId() {
        return newsId;
    }

    public String getTitle() {
        return title;
    }

    public String getMainImage() {
        return mainImage;
    }

    public String getType() {
        return type;
    }

    public String getDate() {
        return date;
    }

    public String getContent1() {
        return content1;
    }

    public String getImage1() {
        return image1;
    }

    public String getContent2() {
        return content2;
    }

    public String getAuthor() {
        return author;
    }

    public String getCustom() {
        return custom;
    }

    public String getBannerSectionId() {
        return bannerSectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public void setBannerSectionId(String bannerSectionId) {
        this.bannerSectionId = bannerSectionId;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
}
