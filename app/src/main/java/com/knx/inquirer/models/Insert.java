package com.knx.inquirer.models;


import com.google.gson.annotations.SerializedName;

public class Insert {
    @SerializedName("id")
    private String id="";
    @SerializedName("type")
    private String type="";
    @SerializedName("title")
    private String title="";
    @SerializedName("creative")
    private String creative="";
    @SerializedName("content")
    private String content="";

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getCreative() {
        return creative;
    }

    public String getContent() {
        return content;
    }
}
