package com.knx.inquirer.models;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "Notifs")
public class NotificationModel {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String news_id;
    private String image;
    private String link;
    private String message;
    private String title; //test
    private Date timestamp;  //String timestamp;
    private int readstatus;

    public NotificationModel(String news_id, String image, String link, String title,  String message, Date timestamp, int readstatus) {
        this.news_id = news_id;
        this.image = image;
        this.link = link;
        this.title = title;
        this.message = message;
        this.timestamp = timestamp;
        this.readstatus = readstatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//    public String getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(String timestamp) {
//        this.timestamp = timestamp;
//    }


    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getReadstatus() {
        return readstatus;
    }

    public void setReadstatus(int readstatus) {
        this.readstatus = readstatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}


//package com.knx.inquirer.models;
//
//
//        import androidx.room.Entity;
//        import androidx.room.PrimaryKey;
//
//@Entity(tableName = "Notifs")
//public class NotificationModel {
//
//    @PrimaryKey(autoGenerate = true)
//    private int id;
//    private String news_id;
//    private String image;
//    private String link;
//    private String message;
//    private String title; //test
//    private String timestamp;
//    private int readstatus;
//
//    public NotificationModel(String news_id, String image, String link, String title,  String message, String timestamp, int readstatus) {
//        this.news_id = news_id;
//        this.image = image;
//        this.link = link;
//        this.title = title;
//        this.message = message;
//        this.timestamp = timestamp;
//        this.readstatus = readstatus;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getNews_id() {
//        return news_id;
//    }
//
//    public void setNews_id(String news_id) {
//        this.news_id = news_id;
//    }
//
//    public String getImage() {
//        return image;
//    }
//
//    public void setImage(String image) {
//        this.image = image;
//    }
//
//    public String getLink() {
//        return link;
//    }
//
//    public void setLink(String link) {
//        this.link = link;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public String getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(String timestamp) {
//        this.timestamp = timestamp;
//    }
//
//    public int getReadstatus() {
//        return readstatus;
//    }
//
//    public void setReadstatus(int readstatus) {
//        this.readstatus = readstatus;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//}
