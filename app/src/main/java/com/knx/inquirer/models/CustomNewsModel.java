package com.knx.inquirer.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "CustomNews")
public class CustomNewsModel {
    @PrimaryKey
    @SerializedName("id")
    @NonNull
    private String id;

    @SerializedName("type")
    private String type;

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("pubdate")
    private String pubdate;

    @SerializedName("link")
    public String link;

    @SerializedName("custom")
    public String label;

    @SerializedName("section")
    public String section;

    //Empty Constructor
    public CustomNewsModel(){

    }

    //Getters and Setters

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubdate() {
        return pubdate;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
        } else {
            CustomNewsModel news = (CustomNewsModel) object;
            if (this.id.equals(news.getId())) {
                result = true;
            }
        }
        return result;
    }
}
