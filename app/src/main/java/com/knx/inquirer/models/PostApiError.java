package com.knx.inquirer.models;

public class PostApiError {

    public String error;


    public PostApiError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
