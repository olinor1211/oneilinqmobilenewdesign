package com.knx.inquirer.models;

public class NotifCounterEvent {

    public final int notif;

    public NotifCounterEvent(int notif) {
        this.notif = notif;
    }

    public int getNotif() {
        return notif;
    }
}
