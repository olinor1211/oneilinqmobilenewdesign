package com.knx.inquirer.models;

public class NewsModel_CustomNews {

    public String newsid;
    public String title;
    public String pubdate;
    public String section;
    public String photo;
    public String type;
    public String link;
    public String label;

    public NewsModel_CustomNews(){

    }

    public String getNewsid() {
        return newsid;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
            result = false;
        } else {
            NewsModel_CustomNews news = (NewsModel_CustomNews) object;
            if (this.newsid == news.getNewsid()) {
                result = true;
            }
        }
        return result;
    }
}
