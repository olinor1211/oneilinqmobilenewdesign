package com.knx.inquirer.models;

import com.knx.inquirer.rvadapters.RVAdapterBreaking;
import com.knx.inquirer.rvadapters.RVAdapterCustom;

public class PostPodcastBreaking {

    private int position = 0;
    private String podcastUrl = "";
    private String title = "";
    private int viewHolder = 0;


    public PostPodcastBreaking(int position, String podcastUrl, String title, int viewHolder) {
        this.position = position;
        this.podcastUrl = podcastUrl;
        this.title = title;
        this.viewHolder = viewHolder;
    }
    public int getPosition() {
        return position;
    }

    public String getPodcastUrl() {
        return podcastUrl;
    }

    public String getTitle() {
        return title;
    }
    public int getViewHolder() {
        return viewHolder;
    }
}
