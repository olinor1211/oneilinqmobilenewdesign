package com.knx.inquirer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Article_old_ {

    @SerializedName("type")
    private String type="";

    @SerializedName("section_id")
    private String sectionId="";

    @SerializedName("creative")
    private String creative="";

    @SerializedName("title")
    private String title="";

    @SerializedName("byline")
    private String byline="";

    @SerializedName("pubdate")
    private String pubdate="";
    //custom: NOT AVAILABLE in NEW API2
    @SerializedName("custom")
    private String custom="";
    //content: ARRAY of String with NoKeyIndex must Use  @Expose
    @SerializedName("content")
    @Expose
    private String content = "";//private List<Content> content = null;

//    @SerializedName("content")
//    private String content = "";

//    @SerializedName("insert")
//    private Insert insert;

    @SerializedName("related")
    @Expose
    private List<Related> related = null;

    public String getType() {
        return type;
    }

    public String getSectionId() {
        return sectionId;
    }

    public String getCreative() {
        return creative;
    }

    public String getTitle() {
        return title;
    }

    public String getByline() {
        return byline;
    }

    public String getPubdate() {
        return pubdate;
    }

    public String getCustom() {
        return custom;
    }

//    public List<String> getContent() {
//        return content;
//    }
//    public List<Content> getContent() {
//        return content;
//    }
    public String getContent() {
        return content;
    }

//    public Insert getInsert() {
//        return insert;
//    }
    public List<Related> getRelated() {
        return related;
    }


//    public static class DataStateDeserializer implements JsonDeserializer<UserResponse> {
//
//        @Override
//        public UserResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//            UserResponse userResponse = new Gson().fromJson(json, UserResponse.class);
//            JsonObject jsonObject = json.getAsJsonObject();
//
//            if (jsonObject.has("data")) {
//                JsonElement elem = jsonObject.get("data");
//                if (elem != null && !elem.isJsonNull()) {
//                    if(elem.isJsonPrimitive()){
//                        userResponse.setMessage(elem.getAsString());
//                    }else{
//
//                        userResponse.setFirstname(elem.getAsJsonObject().get("firstname").getAsString());
//                        userResponse.setLastname(elem.getAsJsonObject().get("lastname").getAsString());
//                        userResponse.setMobilenumber(elem.getAsJsonObject().get("mobilenumber").getAsString());
//                        userResponse.setEmailid(elem.getAsJsonObject().get("emailid").getAsString());
//                        userResponse.setTimezone(elem.getAsJsonObject().get("timezone").getAsString());
//                    }
//                }
//            }
//            return userResponse ;
//        }

}
