package com.knx.inquirer.models;

public class PostUIUpdate {

    public String msg;


    public PostUIUpdate(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
