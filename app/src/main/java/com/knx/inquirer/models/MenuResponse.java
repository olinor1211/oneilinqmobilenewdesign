package com.knx.inquirer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MenuResponse {
    @SerializedName("menu")
    private Menu menu;

    public Menu getMenu() {
    return menu;
    }

//    public void setMenu(Menu menu) {
//        this.menu = menu;
//    }
}

