package com.knx.inquirer.models;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.List;

public class Article {

    @SerializedName("type")
    private String type="";

    @SerializedName("section_id")
    private String sectionId="";

    @SerializedName("creative")
    private String creative="";

    @SerializedName("title")
    private String title="";

    @SerializedName("byline")
    private String byline="";

    @SerializedName("pubdate")
    private String pubdate="";
    //custom: NOT AVAILABLE in NEW API2
    @SerializedName("custom")
    private String custom="";
    //content: ARRAY of String with NoKeyIndex must Use  @Expose
    @SerializedName("content")
    private Object content = "";//private List<Content> content = null;

    @SerializedName("insert")
    private Insert insert;

    @SerializedName("related")
    @Expose
    private List<Related> related = null;

    //FOR CONTENT Deserialization
    private String content1 = "";
    private String content2a = "";
    private String content2b = "";

    public String getType() {
        return type;
    }

    public String getSectionId() {
        return sectionId;
    }

    public String getCreative() {
        return creative;
    }

    public String getTitle() {
        return title;
    }

    public String getByline() {
        return byline;
    }

    public String getPubdate() {
        return pubdate;
    }

    public String getCustom() {
        return custom;
    }

    public List<Related> getRelated() {
        return related;
    }

    public Object getContent() {
        return content;
    }

    public String getContent1() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getContent2a() {
        return content2a;
    }

    public void setContent2a(String content2a) {
        this.content2a = content2a;
    }

    public String getContent2b() {
        return content2b;
    }

    public void setContent2b(String content2b) {
        this.content2b = content2b;
    }

    //CLASS Inside ARTICLE POJO
        //Deserializing the DataType of the content if ARRAY(Object) or primitive
        public static class DataStateDeserializer implements JsonDeserializer<Article> {
            @Override
            public Article deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                Article articleResponse = new Gson().fromJson(json, Article.class);
                 JsonObject jsonObject = json.getAsJsonObject();
                if(jsonObject.has("content")){
                    JsonElement element = jsonObject.get("content"); //GETTING the CONTENT as ELEMENT
                    if(element !=null && !element.isJsonNull()){
                        if(element.isJsonPrimitive()){
                            articleResponse.setContent1(element.getAsString());
                        }else{
                            articleResponse.setContent2a(element.getAsJsonArray().get(0).getAsString());
                            articleResponse.setContent2b(element.getAsJsonArray().get(1).getAsString());
                        }
                    }
                }
                return articleResponse;
            }
        }
}
