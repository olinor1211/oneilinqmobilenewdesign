package com.knx.inquirer.models;

public class TagsModel {

    private int id;
    private String title;
    private boolean isSelected;
    private String isHidden;

    public TagsModel(int id, String title, boolean isSelected, String isHidden){
        this.id = id;
        this.title = title;
        this.isSelected = isSelected;
        this.isHidden = isHidden;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public String getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(String isHidden) {
        this.isHidden = isHidden;
    }
}
