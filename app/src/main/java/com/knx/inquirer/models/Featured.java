package com.knx.inquirer.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Featured {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String url;

    private boolean isSelected;

    //empty for instance
//    public Featured(){}

    public Featured(int id, String title, String url, boolean isSelected) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}