package com.knx.inquirer;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.knx.inquirer.inqprime.utils.PrimeGlobal;
import com.knx.inquirer.models.MenuAdResponse;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodels.BannerAdViewModel;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";

    private SharedPrefs sf;
    private WebView webView;
    private String newToken = "";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private BannerAdViewModel bannerAdViewModel;
    //androidx
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****SPLASH_ACTIVITY_CALLED****");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****SPLASH_LAYOUT_CALLED****");
        //for lower version
//        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }

        try{
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        }catch(Exception e){
            Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
        }

        sf = new SharedPrefs(this);
        //Set Device ID
        String deviceId = Secure.getString(getBaseContext().getContentResolver(), Secure.ANDROID_ID);
        sf.setDeviceId(deviceId);
        PrimeGlobal.DEVICE_ID = deviceId;

        //for androidx
        apiService = ServiceGenerator.createService(ApiService.class);
        getDailyKey();

        webView = findViewById(R.id.wvSplash);
        webView.loadUrl("file:///android_asset/splash.html");
        webView.getSettings().setJavaScriptEnabled(true);

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            registerFirebaseToken(); //registerFirebaseToken
        }, 3000);

    }


    public void proceedNext(){
        Global.isMainActivityOpen = false;
        Intent intent = new Intent();
        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.MainActivity");
        //Test Notification DeepLink
//        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebDeepLinkActivity");
//        intent.putExtra("newsLink","inq/126202");

        startActivity(intent);
        finish();
    }

    //new thread // Observable
    private Observable<String> observeToken(){

        return Observable.just(getFirebaseId())  //Returns the Observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    //get firebase id   // getFirebaseId
    //GETTING FIREBASE INSTANCE no new token here yet, just a promise.
    private String getFirebaseId(){
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult ->
                newToken = instanceIdResult.getToken());
        Log.d(TAG, "getFirebaseId: "+ newToken);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****getFirebaseId_GOT_NEW_TOKEN****: "+ newToken);
        return newToken;
    }

    //getting New Token from firebase...
//    private String getFirebaseId() {
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String token = instanceIdResult.getToken();
//                sf.setFirebaseToken(token);
//                Log.d("SPLASH", "getFirebaseId: " + token);
//                return token;
//            }
//        });
//
//    }

    private void subscribeToTopic(){  // my_inquirer_news   // my_inquirer_news_test
        FirebaseMessaging.getInstance().subscribeToTopic("myinqnews").addOnSuccessListener(this, new OnSuccessListener<Void>() {
//        FirebaseMessaging.getInstance().subscribeToTopic("my_inquirer_news").addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "onSuccess: subscribeToTopic called");
            }
        });
    }

    //register firebase token
    private void registerFirebaseToken(){
        observeToken().subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(String token) {
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(SplashActivity.this, "Unable to register phone for notification", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onComplete() {
                subscribeToTopic();  //subscribing to topic
                sf.setFirebaseToken(newToken); //setting firebase token
                Log.d(TAG, "onComplete: FIREBASE_TOKEN "+ sf.getFirebaseToken());
                Log.d(TAG, "onComplete: DEVICE_ID "+ sf.getDeviceId());
                Log.d(TAG, "onCreate: FLOW_SEQUENCE ****registerFirebaseToken_GOT_NEW_TOKEN****: "+ newToken);
                proceedNext();
            }
        });
    }

    //dispose threads
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    //getDailyKey for androidx
    private void getDailyKey(){
        Log.d(TAG, "getDailyKey: RUN");
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                    Log.d(TAG, "onResponse: getDailyKey: "+sf.getDailyKey());
                }else{
                    Log.d(TAG, "errorBody: getDailyKey: "+response.errorBody().toString());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: getDailyKey: "+t.getMessage());
            }
        });
    }
}