package com.knx.inquirer;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QrActivity extends Activity implements ZXingScannerView.ResultHandler {
    private static final String TAG = "QrActivity";

    private TextView cancelText;
    private TextView reportText;
    private TextView qrText;

    private static final String FLASH_STATE = "FLASH_STATE";

    private ZXingScannerView mScannerView;
    private boolean mFlash;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); //setResultHandler
        // You can optionally set aspect ratio tolerance level
        // that is used in calculating the optimal Camera preview size
        mScannerView.setAspectTolerance(0.1f);
        mScannerView.startCamera();
        mScannerView.setFlash(mFlash);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.camera_view);

        try{
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        }catch(Exception e){
            Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
        }

        //Setting up the ZXingScannerView
        ViewGroup contentFrame = findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this); //Adding viewGroup to ZXingScannerView
        contentFrame.addView(mScannerView);  //Adding/Attaching ZXingScannerView into viewGroup (R.id.content_frame)

        Typeface tf = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/MalloryMP-Bold.otf");

        reportText = findViewById(R.id.textReport);

        reportText.setTypeface(tf);

        reportText.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.Bridge2Activity");
            startActivity(intent);
            finish();
        });

        qrText = findViewById(R.id.textQr);

        qrText.setTypeface(tf);

        qrText.setOnClickListener(v -> {

        });

        cancelText = findViewById(R.id.cancelText);

        cancelText.setTypeface(tf);

        cancelText.setOnClickListener(v -> finish());


    }

    //
    public void GoToWebsite(String urlStr) {
        mFlash = !mFlash;
        Intent intent = new Intent();
        intent.putExtra("url", urlStr);
        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebActivity");
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }


    //implementating methods
    @Override
    public void handleResult(Result result) {
        GoToWebsite(result.getText());
    }

    public void toggleFlash(View v) {
        mFlash = !mFlash;  //make it toggle
        mScannerView.setFlash(mFlash);
    }
}

