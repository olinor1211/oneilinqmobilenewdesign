package com.knx.inquirer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.amitshekhar.DebugDB;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
//import com.google.zxing.common.StringUtils;
//import com.google.zxing.common.StringUtils;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.fragments.FragmentAppSettings;
import com.knx.inquirer.fragments.FragmentBookmarks;
import com.knx.inquirer.fragments.FragmentBreaking;
import com.knx.inquirer.fragments.FragmentCustom;
import com.knx.inquirer.fragments.FragmentMyInq;
import com.knx.inquirer.fragments.FragmentMyInqChangePassword;
import com.knx.inquirer.fragments.FragmentMyInqEdit;
import com.knx.inquirer.fragments.FragmentMyInqHolder;
import com.knx.inquirer.fragments.FragmentMyInqLogin;
import com.knx.inquirer.fragments.FragmentMyInqLoginRegister;
import com.knx.inquirer.fragments.FragmentMyInqRegistrationP1;
import com.knx.inquirer.fragments.FragmentMyInqRegistrationP2;
import com.knx.inquirer.fragments.FragmentMyInqSuccess;
import com.knx.inquirer.fragments.FragmentMyInqUpdateUserDetails;
import com.knx.inquirer.fragments.FragmentNotification;
import com.knx.inquirer.fragments.FragmentTutorial;
import com.knx.inquirer.models.PostUIUpdate;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.CustomViewPager;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryMenu;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryNotifs;
import com.knx.inquirer.viewmodels.MenuViewModel;
import com.knx.inquirer.viewmodels.NotifsViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.disposables.CompositeDisposable;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


//implements AudioManager.OnAudioFocusChangeListener
public class MainActivity extends AppCompatActivity implements View.OnClickListener{ //, AudioManager.OnAudioFocusChangeListener
    private static final String TAG = "MainActivity";

//    private BottomNavigationViewEx navigation;
    private CustomViewPager viewpager;
    private ViewPagerAdapter vpadapter; //Class INSIDE this CLASS MAIN
    private QBadgeView badgeView;
    private ImageView mSearch,mCamera,mLogo,mSetting;
    private TextView mMyInquirer, mSection, mBookmark, mFeedback;
    //
//    private final int MY_INQUIRER = 0;
    private final int SECTIONS = 1;
    private final int BOOKMARKS = 2;
//    private final int FEEDBACK = 3; Must be Updated if MainMenu has additional
    private final int APP_SETTING = 3;
    private final int ALERT = 4;
    private final int MY_INQUIRER_EDIT = 5;
    private final int MY_INQUIRER_CUSTOM = 6;
    private final int MY_INQUIRER_INQ = 7;
    private final int MY_INQUIRER_SUCCESS = 8;
    private final int MY_INQUIRER_LOGIN_REGISTER = 9; //Disable
    private final int MY_INQUIRER_REGISTER_P1 = 10;
    private final int MY_INQUIRER_LOGIN = 11;
    private final int MY_INQUIRER_REGISTER_P2 = 12;
    private final int MY_INQUIRER_EDIT_UPDATE = 13;
    private final int MY_INQUIRER_CHANGE_PASS = 14;

    private long lastPress;
    private double newVersion = 0;
    public double currentVersionCode;

    private ApiService apiService;

    private SharedPrefs sf;
    private DatabaseHelper helper;

    private boolean isTutViewed;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    //Calligraphy3
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    //new design additional
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg){
        switch(msg){
            case "appSetting":
                appSettingFragment();
                break;
            case "appAlert":
                appAlertFragment();
                break;
            case "appMyInqEdit":
            case "changePassSuccess":
                appMyInqEditFragment(); //sameFLOW with checking if registered
                break;
            case "myInqUpdated":
            case "loginSuccess":
                myInqNewsDisplayed();
                break;
            case "myInqSuccess":
                myInqSuccess();
                break;
            case "myInqLogin":
                myInqLogin();
                break;
            case "myInqRegister":
                myInqRegisterP1();
                break;
            case "continueRegistration":
                myInqRegisterP2();
                break;
            case "changePass":
                myInqChangePass();
                break;




        }
    }

    private void myInqChangePass() {
        viewpager.setCurrentItem(MY_INQUIRER_CHANGE_PASS); //
        menuItemSelected(mMyInquirer);
    }

    private void myInqLogin() {
        viewpager.setCurrentItem(MY_INQUIRER_LOGIN); //
        menuItemSelected(mMyInquirer);
    }

    private void myInqRegisterP1() {
        viewpager.setCurrentItem(MY_INQUIRER_REGISTER_P1);
        menuItemSelected(mMyInquirer);
    }

    private void myInqRegisterP2() {
        viewpager.setCurrentItem(MY_INQUIRER_REGISTER_P2);
        menuItemSelected(mMyInquirer);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****MainActivity_ACTIVITY_CALLED****");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****MainActivity_LAYOUT_CALLED****");

//        Log.d(TAG, "DATABASE DEBUG : "+ DebugDB.getAddressLog());  //Remove when RELEASE

        sf = new SharedPrefs(this);
        apiService = ServiceGenerator.createService(ApiService.class);
        helper = new DatabaseHelper(this);
        isTutViewed = sf.isTutViewed(); // isTutViewed

        Log.d(TAG, "onCreate: VERSION_CODES "+Build.VERSION_CODES.O);
        Log.d(TAG, "onCreate: VERSION_VERSION "+android.os.Build.VERSION.SDK_INT);
        try{
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        }catch(Exception e){
            Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
        }

        //for NOTIFICATION DeepLink
        Global.isMainActivityOpen = true;
        //TEST INSERT NOTIFICATION
        //("newsLink","inq/137248"); //137248 justInDOH  01359 NASACOSTA  133885
//        String image = "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1623054922_0607COVID.jpg";//"https://getaway.ph/wp-content/uploads/2021/04/Screen-Shot-2021-04-20-at-7.39.11-AM.png"; //
//        Date date =  Global.convertToDate("2021-03-26 16:09:00");  //"2021-03-25 16:07:00";   //String date = "2021-03-25 16:07:00";
//        String link = "inq/137248";
//        String title = "Those giant sea turtles and where to find them in PH";
//        String message = "HELLO ONEIL";
//        String newsid = "137248";
//        helper.insertNotification(new NotificationModel(newsid,image,link,title,message,date,0)); //re-insert again


        //After VersionUpdate Display again the Notif Permission Dialog IF hasNotificationAllowed is FALSE, User not allowed notification
        //Check_First if from VersionUpdate
        if(sf.hasAskNotifPermissionUpdateVersion()){
            sf.setAskNotifPermissionUpdateVersion(false);
            //Check if setNotificationAllowed is F        ../fg/. mSE then MAKE setNewUser TRUE
            if(!sf.hasNotificationAllowed()){
                sf.setNewUser(true); //COUNTING of StoryReads Start again..
            }
        }

        initViews();

//      //QBadgeView new Instance
        badgeView = new QBadgeView(MainActivity.this);
//
        checkIfTutViewed();
//        checkVersionUpdate2();  //
         checkVersionUpdate();
////
////    //updating the badgeCount everytime from ViewModel
        updateBadgeCount(); //remove badge temp

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==Global.NEWS_ARTICLE){
            Log.d(TAG, "onActivityResult: startActivityForResult 22 CALLED");
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
    }

//    private void showBottomNav(){
//        HideBottomViewOnScrollBehavior hbv = new HideBottomViewOnScrollBehavior();
//        hbv.slideDown(navigation);
//    }
    //
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        if(requestCode != RESULT_CANCELED) {
            switch (requestCode) {

                case 69: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        loadScanner();
                    } else {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                            //Show permission explanation dialog...
                            Toast.makeText(this, "You cannot scan QR codes unless you allow camera access", Toast.LENGTH_SHORT).show();

                        } else {
                            //Never ask again selected, or device policy prohibits the app from having that permission.
                            //So, disable that feature, or fall back to another situation...
                            Global.promptPermissionDialog prompt = new Global.promptPermissionDialog();
                            prompt.showDialog(this, "To use this app feature, manually allow camera access from your phone settings");
                        }
                    }
                }
                break;
            }
        }
    }

    //addBadgeAt()
    //Get Notification count
    private void updateBadgeCount(){
        NotifsViewModel notifsViewModel = ViewModelProviders.of(this, new ViewModelFactoryNotifs(getApplication(), compositeDisposable)).get(NotifsViewModel.class);
        notifsViewModel.getUnreadNotifsCount().observe(this, count -> addBadgeAt(4, count));  //count of Unread notif.. pass the position..
    }

    //remove badge in notif temp
    //addBadgeAt method  ..return badgeView
    public Badge addBadgeAt(int position, int number) {
        // add badge
        return badgeView
                .setBadgeNumber(number)  //set number
                .setBadgeTextSize(8,true)
                .setBadgePadding(3,true)
                .setGravityOffset(5, 0, true)  //badge position // .setGravityOffset(12, 2, true)
                .bindTarget(mSetting);  //binding badge to specific item.  //R.id.ivsetting
    }

//    public Badge addBadgeAt(int position, int number) {
//        // add badge
//        return badgeView
//                .setBadgeNumber(number)  //set number
//                .setGravityOffset(12, 2, true)  //badge position
//                .bindTarget(navigation.getBottomNavigationItemView(position));  //binding badge to specific item.
////                .setOnDragStateChangedListener((dragState, badge, targetView) -> {
////                    if (Badge.OnDragStateChangedListener.STATE_SUCCEED == dragState)
////                        Toast.makeText(MainActivity.this, "PrimeNotification removed!", Toast.LENGTH_SHORT).show();
////                });
//    }

    //
    private void checkIfTutViewed(){
        if(!isTutViewed){
            FragmentTutorial popDialog = FragmentTutorial.getInstance();
            popDialog.show(getSupportFragmentManager(), "tutdialog");  //SHOWING FragmentTutorial: show()= DialogFragment public void show(@NonNull FragmentManager manager, @Nullable String tag)
        }
    }

//    private void checkNotificationSettings() {
//
//        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
//        boolean areNotificationsEnabled = notificationManagerCompat.areNotificationsEnabled();
//        Log.d(TAG, "checkNotificationSettings: " + areNotificationsEnabled);
//
//        if(areNotificationsEnabled){
//            sf.setNotificationFlag("true");
//            Log.d(TAG, "checkNotificationSettings: "+areNotificationsEnabled);
//        }else
//            sf.setNotificationFlag("false");
//        Log.d(TAG, "checkNotificationSettings: "+areNotificationsEnabled);
//    }

    private void initViews(){
        viewpager = findViewById(R.id.content);
        mLogo = findViewById(R.id.logo);
        mSetting = findViewById(R.id.ivsetting);
        mSearch = findViewById(R.id.ivsearch);
        mCamera = findViewById(R.id.ivcamera);
        mMyInquirer = findViewById(R.id.mainMyInquirer);
        mSection = findViewById(R.id.mainSection);
        mBookmark = findViewById(R.id.mainBookmark);
//        mFeedback = findViewById(R.id.mainFeedback);
        mLogo.setOnClickListener(this);
        mSetting.setOnClickListener(this);
        mSearch.setOnClickListener(this);
        mCamera.setOnClickListener(this);
        mMyInquirer.setOnClickListener(this);
        mSection.setOnClickListener(this);
        mBookmark.setOnClickListener(this);
//        mFeedback.setOnClickListener(this);

        //ViewPager settings
        viewpager.setPagingEnabled(false);  //not able to swipe make FALSE
        viewpager.setOffscreenPageLimit(8);  //Plus 4 items
        //ADDING fragment into viewPager_ADAPTER then place/set the ADAPTER into viewPager
        setupViewPager(viewpager);

        //Determine which Fragment will be display first
        if(sf.isRegistered()){
            viewpager.setCurrentItem(MY_INQUIRER_CUSTOM);
            menuItemSelected(mMyInquirer);
        } else {
            viewpager.setCurrentItem(SECTIONS);
            menuItemSelected(mSection);
        }

    }

    public void setupViewPager(ViewPager viewPager) {
        //Create new ADAPTER
        vpadapter = new ViewPagerAdapter(getSupportFragmentManager());  //Class inside this MainClass
        //Add Fragment to ADAPTER
        /**UPDATE also the COUNT of viewpager.setOffscreenPageLimit(6); */
        vpadapter.addFragment(new FragmentMyInqHolder());
        vpadapter.addFragment(new FragmentBreaking()); //SECTIONS
        vpadapter.addFragment(new FragmentBookmarks()); //BOOKMARKS
//        vpadapter.addFragment(new FragmentFeedback()); //
        vpadapter.addFragment(new FragmentAppSettings());  //APP_SETTING
        vpadapter.addFragment(new FragmentNotification());  //ALERT
        vpadapter.addFragment(new FragmentMyInqEdit());  //MY_INQUIRER_EDIT
        vpadapter.addFragment(new FragmentCustom());  //MY_INQUIRER_CUSTOM
        vpadapter.addFragment(new FragmentMyInq());  //MY_INQUIRER_INQ
        vpadapter.addFragment(new FragmentMyInqSuccess()); //MY_INQUIRER_SUCCESS
        vpadapter.addFragment(new FragmentMyInqLoginRegister()); //MY_INQUIRER_LOGIN_REGISTER
        vpadapter.addFragment(new FragmentMyInqRegistrationP1()); //MY_INQUIRER_REGISTER_P1
        vpadapter.addFragment(new FragmentMyInqLogin()); //MY_INQUIRER_LOGIN
        vpadapter.addFragment(new FragmentMyInqRegistrationP2()); //MY_INQUIRER_REGISTER_P2
        vpadapter.addFragment(new FragmentMyInqUpdateUserDetails()); //MY_INQUIRER_EDIT_UPDATE
        vpadapter.addFragment(new FragmentMyInqChangePassword()); //MY_INQUIRER_CHANGE_PASS

        //Place/Set the ADAPTER into viewPager itself
        viewPager.setAdapter(vpadapter); //
    }

    @Override
    public void onClick(View v) {
        //
        if (EventBus.getDefault().hasSubscriberForEvent(PostUIUpdate.class)) {
                EventBus.getDefault().post(new PostUIUpdate("dismiss_breaking"));
                EventBus.getDefault().post(new PostUIUpdate("dismiss_myinq"));
        }
        switch (v.getId()){
            case R.id.ivsetting:
                Intent intent2 = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent2);
                //Note: SettingsActivity.Class must have the SAME overridePendingTransition(PARAMS:R.anim.slide_in_right, R.anim.slide_out_left) INSIDE public void finish() after super.finish()
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                break;
            case R.id.ivsearch:
                Intent intent = new Intent();
                intent.setClassName("com.knx.inquirer", "com.knx.inquirer.SearchActivity");
                startActivity(intent);
                break;
            case R.id.ivcamera:
                loadScanner();
                break;
            case R.id.logo:
//                viewArticleNew("01930");
                Log.d(TAG, "onClick: LOGO");
                //NOTIFICATION TEST WEBDEEP
//                Intent inten = new Intent(this, WebDeepLinkActivity.class);
//                inten.putExtra("newsLink","inq/137248"); //137248 justInDOH  01359 NASACOSTA  133885
//                startActivity(inten);
                break;
            case R.id.mainMyInquirer:
                menuItemUnSelected(mSection,mBookmark); //menuItemUnSelected(mSection,mBookmark,mFeedback);
                menuItemSelected(mMyInquirer);
                    if(sf.isRegistered()){
                        viewpager.setCurrentItem(MY_INQUIRER_CUSTOM);
                    }else{
                        viewpager.setCurrentItem(MY_INQUIRER_LOGIN); //MY_INQUIRER_INQ MY_INQUIRER_LOGIN_REGISTER
                    }
//                }
                break;
            case R.id.mainSection:
                menuItemUnSelected(mMyInquirer,mBookmark); // menuItemUnSelected(mMyInquirer,mBookmark,mFeedback);
                menuItemSelected(mSection);
                viewpager.setCurrentItem(SECTIONS);
                break;
            case R.id.mainBookmark:
                menuItemUnSelected(mMyInquirer,mSection); //REMOVE FeedBack: menuItemUnSelected(mMyInquirer,mSection,mFeedback);
                menuItemSelected(mBookmark);
                viewpager.setCurrentItem(BOOKMARKS);
                break;
            default: break;
        }
    }

    private void menuItemSelected(TextView view){
//        Typeface bold = Typeface.createFromAsset(getAssets(),"fonts/SF-UI-DISPLAY-BOLD.otf"); view.setTypeface(bold);
        view.setBackground(getResources().getDrawable(R.drawable.main_underline)); // view.setBackground(getResources().getDrawable(R.drawable.main_underline));
        view.setTextColor(getResources().getColor(R.color.white));
    }

    //With FeedBack This one
//    private void menuItemUnSelected(TextView view1,TextView view2, TextView view3 ){
////        Typeface semibold = Typeface.createFromAsset(getAssets(),"fonts/SF-UI-DISPLAY-SEMIBOLD.otf"); view1.setTypeface(semibold); view2.setTypeface(semibold); view3.setTypeface(semibold);
//        view1.setBackgroundResource(0); view2.setBackgroundResource(0); view3.setBackgroundResource(0);
//        view1.setTextColor(getResources().getColor(R.color.white_blurred)); view2.setTextColor(getResources().getColor(R.color.white_blurred)); view3.setTextColor(getResources().getColor(R.color.white_blurred));
//    }

     private void menuItemUnSelected(TextView view1,TextView view2 ){
        view1.setBackgroundResource(0); view2.setBackgroundResource(0);;
        view1.setTextColor(getResources().getColor(R.color.white_blurred)); view2.setTextColor(getResources().getColor(R.color.white_blurred));
    }

    //loading scanner
    public void loadScanner(){

        if (ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    69);
        } else {
            Intent intent = new Intent();
            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.BridgeActivity");
            startActivity(intent);
        }
    }

    //CLASS viewPager ADAPTER EXTENDS to FragmentPagerAdapter    ...is INSIDE the MainActivity
    class ViewPagerAdapter extends FragmentPagerAdapter {

        //List of Fragment
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {

            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }
    //check version
    private void checkVersionUpdate(){

        currentVersionCode = Double.valueOf(com.knx.inquirer.BuildConfig.VERSION_NAME);  //get app Version code  // Double.valueOf(BuildConfig.VERSION_NAME);
        Log.d(TAG, "checkVersionUpdate: " + currentVersionCode);
        Log.d(TAG, "checkVersionUpdate: APPLICATION_ID " +  BuildConfig.APPLICATION_ID);
        Global.currentVersion = currentVersionCode;
        //Check if the version code from database is updated
        MenuViewModel menuViewModel = ViewModelProviders.of(this, new ViewModelFactoryMenu(getApplication(), compositeDisposable, sf)).get(MenuViewModel.class);
        // get new version result/response (from repo)
        menuViewModel.getMenu().observe(this, response -> {
            //GET BANNER ADS
            getBannerAd2(response);

            try{
                newVersion = response.get("menu").getAsJsonObject().get("Android").getAsDouble();
                Log.d(TAG, "checkVersionUpdate: menuViewModel newVersion "+newVersion);
            }catch(Exception e){
                Log.d(TAG, "checkVersionUpdate: menuViewModel Exception_error: "+ e.getMessage());
            }
            //compare version   ..isNewUser set True
            if(currentVersionCode < newVersion){
                Global.promptUpdateAppDialog prompt = new Global.promptUpdateAppDialog(sf);
                prompt.showDialog(MainActivity.this, "New version available.\nDo you want to update now?");  //call method of class
            }
        });

        menuViewModel.getMenuError().observe(this, aBoolean -> {
            Log.d(TAG, "checkVersionUpdate: menuViewModel HasError T/F: "+aBoolean);
        });
    }

    //destroy retrofit calls  ..every calls have composite param
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onStop() {
        Global.isItemClicked = false;
        Global.isRelatedArticle = false;
        Log.d(TAG, "onStop: LifeCycle");
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onStart() {
        //check if registered already
        if(!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);  //additional
        Log.d(TAG, "onStart: LifeCycle");
        super.onStart();
    }

    //
    @Override
    public void onBackPressed() {
        long currentTime = System.currentTimeMillis();
        if(viewpager.getCurrentItem()==MY_INQUIRER_REGISTER_P2){
            viewpager.setCurrentItem(MY_INQUIRER_REGISTER_P1);
            menuItemSelected(mMyInquirer);
        }
//        viewpager.getCurrentItem()==MY_INQUIRER_LOGIN ||
        else if(viewpager.getCurrentItem()==MY_INQUIRER_REGISTER_P1){
            viewpager.setCurrentItem(MY_INQUIRER_LOGIN);
            menuItemSelected(mMyInquirer);
        }
        //checking if clicked within 5secs  or not
        else if (currentTime - lastPress > 5000) {
            Toast.makeText(this, "Press back again to exit", Toast.LENGTH_LONG).show();
            lastPress = currentTime;
            Log.d(TAG, "onBackPressed: currentTime " + lastPress);

        } else {
            Log.d(TAG, "onBackPressed: LAST_SECTION: "+sf.getLastSection());
            finishAffinity();
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            super.onBackPressed();
        }
    }

    private void getBannerAd2(JsonObject response) {
        Global.bannerList.clear(); //Clear List
        Global.sectionsBanner2 = new JsonArray();  //Clear JsonArray
        Global.sectionsBanner2 = response.get("menu").getAsJsonObject().get("sections").getAsJsonArray(); //response.get("menu").getAsJsonObject().get("sections").getAsJsonArray();
        String id = "";
        String adImage = "";
        try{
            for(int x=0; x< Global.sectionsBanner2.size(); x++) {
                if(response.has("banner")){
                     adImage = !Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("image").isJsonNull() ? Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("image").getAsString() : ""; //Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("image").getAsString();
                     id = !Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("image").isJsonNull() ? Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString() : "";//Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString();
                }
                //CHECK Which Sections has BANNER AD
                if (!adImage.isEmpty()) {
                    Global.bannerList.add(id);
                    Log.d(TAG, "getBannerAd2 HAS_BANNER: " + id + " -> " + Global.sectionsBanner2.get(x).getAsJsonObject().get("title").getAsString());
                }
            }
        }catch(Exception e){
            Log.d(TAG, "getBannerAd2: Try_Exception_error: "+ e.getMessage());
        }

        Log.d(TAG, "getBannerAd2: GlobalbannerList HAS_BANNER_Section_ID: " + Global.bannerList.toString());
        Log.d(TAG, "getBannerAd2: GlobalsectionsBanner2 Sections_BANNER: " + Global.sectionsBanner2.toString());

    }
    /** ALL NewDesign ADDED HERE*/  //menuItemUnSelectAll updated after remove Feedback
    private void appSettingFragment() {
        viewpager.setCurrentItem(APP_SETTING);
        menuItemUnSelectAll(mMyInquirer,mSection,mBookmark); //menuItemUnSelectAll(mMyInquirer,mSection,mBookmark,mFeedback);
    }

    private void appAlertFragment() {
        viewpager.setCurrentItem(ALERT);
        menuItemUnSelectAll(mMyInquirer,mSection,mBookmark); //menuItemUnSelectAll(mMyInquirer,mSection,mBookmark,mFeedback);
    }

    private void appMyInqEditFragment() {
        if(sf.isRegistered()){
            viewpager.setCurrentItem(MY_INQUIRER_EDIT_UPDATE); // //MY_INQUIRER_EDIT
            menuItemUnSelectAll(mMyInquirer,mSection,mBookmark);
        }else{
            viewpager.setCurrentItem(MY_INQUIRER_LOGIN); //Default  MY_INQUIRER_INQ
            menuItemSelected(mMyInquirer); //mMyInquirer MainBtnText
        }
    }

    private void myInqNewsDisplayed() {
        viewpager.setCurrentItem(MY_INQUIRER_CUSTOM); //
        menuItemSelected(mMyInquirer); //mMyInquirer MainBtnText
    }
    private void myInqSuccess() {
        viewpager.setCurrentItem(MY_INQUIRER_SUCCESS); //
        menuItemSelected(mMyInquirer); //mMyInquirer MainBtnText
    }

    private void menuItemUnSelectAll(TextView v1,TextView v2, TextView v3){
        v1.setBackgroundResource(0); v2.setBackgroundResource(0); v3.setBackgroundResource(0);
        v1.setTextColor(getResources().getColor(R.color.white_blurred)); v2.setTextColor(getResources().getColor(R.color.white_blurred));
        v3.setTextColor(getResources().getColor(R.color.white_blurred));
    }


//    private void viewArticleNew(String newsId) {
//        long unixTime = System.currentTimeMillis() / 1000L;
//        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + newsId);
//        apiService.viewArticle(String.valueOf(unixTime), sf.getDeviceId(),newsId, hashed)
//                .subscribeOn(Schedulers.io()) //subscribeOn
//                .observeOn(AndroidSchedulers.mainThread()) //observeOn
//                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
//                    @Override
//                    public void onSubscribe(Disposable d) {compositeDisposable.add(d);}
//
//                    @Override
//                    public void onSuccess(ResponseBody responseBody) {
//                        try {
//                            if(responseBody.string().contains("<!DOCTYPE html>")){
//                                Toast.makeText(MainActivity.this, "Good!", Toast.LENGTH_SHORT).show();
//                                Global.articleView = responseBody.string();
//                            }else{
//                                Toast.makeText(MainActivity.this, "Response Error", Toast.LENGTH_SHORT).show();
//                            }
//                        } catch (Exception e) {
//                            Log.d(TAG, "onSuccess:testWebArticle  "+e.getMessage());
//                        }
//                    }
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.d(TAG, "onError: registerTimeListViewReads error  getMessage" + e.getMessage());
//                    }
//                });
//    }

}




















 //removeItemsUnderline
//    private void removeStylesMenuItem(BottomNavigationViewEx navigationViewEx) {
//        for (int i = 0; i <  navigationViewEx.getMenu().size(); i++) {
//            MenuItem item = navigationViewEx.getMenu().getItem(i);
//            item.setTitle(item.getTitle().toString());
//        }
//    }
//    //removeItemsUnderline
//     private void removeItemsUnderline(BottomNavigationViewEx navigationViewEx) {
//        for (int i = 0; i <  navigationViewEx.getMenu().size(); i++) {
//            MenuItem item = navigationViewEx.getMenu().getItem(i);
//            item.setTitle(item.getTitle().toString());
//        }
//    }
    //Adding underline in the menu underlineMenuItem
//    private void underlineMenuItem(MenuItem item) {
////        String title = item.getTitle() + "\n";
//        SpannableString content = new SpannableString(item.getTitle()); //SpannableString content = new SpannableString(item.getTitle()); //title with new line
//        //Changing Text color
//        content.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, content.length(), 0); //ForegroundColorSpan
//        //Add underline
////        content.setSpan(new UnderlineSpan(), 0, content.length(), 0); //setSpan
//        //making it BOLD
//        content.setSpan(new StyleSpan(Typeface.BOLD),0, content.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        //
//        item.setTitle(content);
//    }
    //Adding underline in the menu underlineMenuItem
//    private void stylesMenuItem(MenuItem item) {
//        SpannableString content = new SpannableString(item.getTitle()); //SpannableString content = new SpannableString(item.getTitle()); //title with new line
//        //Changing Text color
//        content.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, content.length(), 0); //ForegroundColorSpan
//        //making it BOLD
//        content.setSpan(new StyleSpan(Typeface.BOLD),0, content.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
////        content.setSpan(new UnderlineSpan(), 0, content.length(), 0); //setSpan
//        item.setTitle(content);
//    }
//
//    private void underlineSelectedItem(MenuItem item, int position) {
////        String underscore = "           ";
//        SpannableString content = new SpannableString(getString(R.string.text_spaces)); //Create new SpannableString() to get the length of the titleMenu
////        SpannableString content = new SpannableString(item.getTitle()); //Create new SpannableString() to get the length of the titleMenu
//        content.setSpan(new UnderlineSpan(), 0, item.getTitle().length(), 0); //setSpan
////
//////        String underscore = "____________________";
//////        underscore = underscore.substring(0, content.length()); //content.length() = length of the titleMenu
//        underline.setText(content);
//
//        //Adjustment of the textView position
//        ConstraintSet constraintSet = new ConstraintSet(); //declareConstrainSet
//        constraintSet.clone(clActionBar); //Copy ConstraintLayout
//        // set.clear(tvITPDLog.getId(), ConstraintSet.TOP);
//        //Make Changes/Updates in the child
//        constraintSet.setHorizontalBias(
//                R.id.underline,
//                position* 0.33f);
//        constraintSet.applyTo(clActionBar);
//    }
