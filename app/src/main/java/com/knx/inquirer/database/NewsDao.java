package com.knx.inquirer.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.model.PrimeLikeModel;
import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;
import com.knx.inquirer.inqprime.model.PrimeSectionsModel;
import com.knx.inquirer.inqprime.model.PrimeTopicsModel;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.models.Option;
import com.knx.inquirer.models.Section;

import java.util.List;

@Dao
public interface NewsDao {

    /*** News table query ***/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAll(List<NewsModel> news);

    @Query("SELECT * FROM News LIMIT 20")
    LiveData<List<NewsModel>> getAllNews();

    @Query("SELECT COUNT() FROM News")
    LiveData<Integer> getNewsRecordCount();

    @Query("DELETE FROM News")
    void deleteAllNews();

    /*** CustomNews table query ***/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAllCustomNews(List<Feed> news);

    @Query("SELECT * FROM CustomNews")
    LiveData<List<Feed>> getAllCustomNews();

    @Query("SELECT COUNT() FROM CustomNews")
    LiveData<Integer> getCustomNewsRecordCount();

    @Query("DELETE FROM CustomNews")
    void deleteAllCustomNews();


    /*** Section table query ***/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveSections(List<Section> sections);

    @Query("SELECT * FROM Sections")
    LiveData<List<Section>> getAllSections();

    @Query("SELECT COUNT() FROM Sections")
    LiveData<Integer> getSectionRecordCount();

    @Query("DELETE FROM Sections")
    void deleteAllSections();

    /*** Custom Section table query ***/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveCustomSections(List<Option> sections);

    //menu tags
    @Query("SELECT * FROM CustomSections")
    LiveData<List<Option>> getAllCustomSections();

    @Query("SELECT COUNT() FROM CustomSections")
    LiveData<Integer> getCustomSectionRecordCount();

    @Query("DELETE FROM CustomSections")
    void deleteAllCustomSections();

    /*** PrimeNotification table query ***/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveNotification(NotificationModel notifs);

    //ORDER by DATE timestamp is a string here
    @Query("SELECT * FROM Notifs ORDER BY timestamp DESC") // @Query("SELECT * FROM Notifs ORDER BY id DESC")
    LiveData<List<NotificationModel>> getAllNotifications();

//    @Query("SELECT * FROM Notifs ORDER BY id DESC LIMIT 10")  //LIMIT 10
//    LiveData<List<NotificationModel>> getAllNotifications();

    //new
    @Query("UPDATE Notifs SET readstatus = :status WHERE news_id =:newsId")
    void updateNotifStatus2(int status, String newsId);

    //new
    @Query("UPDATE Notifs SET readstatus = 1")
    void readAllNotification();

//    @Query("UPDATE Notifs SET readstatus = :status WHERE id =:id")
//    void updateNotifStatus(int status, int id);

    @Query("SELECT COUNT(*) FROM (SELECT * FROM Notifs ORDER BY id DESC LIMIT 10) WHERE readstatus = 0")
    LiveData<Integer> getUnreadNotifsCount();

    @Query("DELETE FROM Notifs")
    void deleteAllNotifications();

    @Query("DELETE FROM Notifs WHERE id = :id")
    void deleteOneNotification(int id);


    /**PRIME-PRIME-PRIME-PRIME-PRIME-PRIME-PRIME **/  //SELECT--LiveData, INSERT,UPDATE--void
    //BOOKMARK************using LiveData
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void savePrimeBookmark(PrimeBookmarkModel bookmark);
    //has return value LONG.. numbers of deleted rows
    //no return value... void
    @Query("DELETE FROM PrimeBookmark WHERE newsId = :id")
    void deleteOnePrimeBookmark(int id);

    @Query("SELECT * FROM PrimeBookmark")
    LiveData<List<PrimeBookmarkModel>> getPrimeBookmarks();


    //LIKE**************using LiveData
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void savePrimeLike(PrimeLikeModel like);

    @Query("SELECT * FROM PrimeLike")
    LiveData<List<PrimeLikeModel>> getPrimeLike();


//    //ACCOUNT INFORMATION**************using LiveData
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void savePrimeAccountInformation(PrimeAccountInformationModel primeAccountInformationModel);
//
    @Query("SELECT * FROM PrimeAccountInformation")
    LiveData<List<PrimeAccountInformationModel>> getPrimeAccountInformation();
//
    @Query("UPDATE PrimeAccountInformation SET avatar = :avatar WHERE userId = :id")
    void updatePrimeAvatar(String avatar, int id);

    @Query("UPDATE PrimeAccountInformation SET accountEmail = :email WHERE userId = :id")
    void updatePrimeEmail(String email, int id);

    @Query("UPDATE PrimeAccountInformation SET mobileNo = :number WHERE userId = :id")
    void updatePrimeMobileNo(String number, int id);

    @Query("UPDATE PrimeAccountInformation SET cardNo = :number WHERE userId = :id")
    void updatePrimeCardNo(String number, int id);

    @Query("UPDATE PrimeAccountInformation SET `plan` = :plan WHERE userId = :id")
    void updatePrimePlan(String plan, int id);


    //SECTIONS AND TOPICS**************
    //SECTIONS
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void savePrimeSections(PrimeSectionsModel status);

    @Query("UPDATE PrimeSections SET status = :status WHERE primeId = :id")
    void updatePrimeSectionStatus(Boolean status, int id);

    @Query("SELECT * FROM PrimeSections")  //use for Initialization of record
    List<PrimeSectionsModel> getPrimeSections();

    @Query("SELECT * FROM PrimeSections WHERE status = 1")
    LiveData<List<PrimeSectionsModel>> getPrimeSectionsStatus();

    //TOPICS
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void savePrimeTopics(PrimeTopicsModel status);

    @Query("UPDATE PrimeTopics SET status = :status WHERE primeId = :id")
    void updatePrimeTopicsStatus(Boolean status, int id);

    @Query("SELECT * FROM PrimeTopics WHERE status = 1")
    LiveData<List<PrimeTopicsModel>> getPrimeTopicsStatus();


    //NEWS OFFLINE *****************
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void savePrimeNews(List<PrimeNewsOfflineModel> news);

    @Query("SELECT * FROM PrimeNewsOffline WHERE news_Id = :id")
    List<PrimeNewsOfflineModel> getArticleContent(int id);

//    @Query("SELECT * FROM PrimeNewsOffline LIMIT 20")
//    List<PrimeNewsOfflineModel> getAllPrimeNews();

    @Query("SELECT * FROM PrimeNewsOffline LIMIT 20")  //@Query("SELECT * FROM PrimeNewsOffline LIMIT 20")
    LiveData<List<PrimeNewsOfflineModel>> getAllPrimeNews();

    @Query("DELETE FROM PrimeNewsOffline WHERE section = :sectionId")
    void deleteSectionPrimeNewsOffline(int sectionId);

    @Query("DELETE FROM PrimeNewsOffline")
    void deleteAllPrimeNewsOffline();

    @Query("UPDATE PrimeNewsOffline SET author = :author, content = :content, image_Type = :imageType WHERE news_Id = :id")
    void updatePrimeNews(String author, String content , String imageType,   int id);

    @Query("UPDATE PrimeNewsOffline SET offline_photo = :offline_photo WHERE news_Id = :id")
    void updatePrimeNewsImage(byte[] offline_photo, int id);

    /**NEW ADDITIONAL NewDesign*/
    //NEW_BOOKMARK
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveBookmark(BookmarksNewModel bookmark);

    @Query("DELETE FROM BookmarksNew WHERE newsId = :id")
    void deleteOneBookmark(int id);

    @Query("SELECT * FROM BookmarksNew ORDER BY id  DESC")
    LiveData<List<BookmarksNewModel>> getAllBookmarks();


}





//    @Query("SELECT * FROM PrimeBookmark")
//    List<PrimeBookmarkModel> getPrimeBookmarksAfterDeletion();

//    @Query("SELECT EXISTS(SELECT * FROM PrimeBookmark WHERE newsId = :id)")
//    LiveData<Integer> getPrimeBookmarkRedundant(int id);
