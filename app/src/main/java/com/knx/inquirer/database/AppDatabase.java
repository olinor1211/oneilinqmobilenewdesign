package com.knx.inquirer.database;

import android.content.Context;

import androidx.room.RoomDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.TypeConverters;

import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.model.PrimeLikeModel;
import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;
import com.knx.inquirer.inqprime.model.PrimeSectionsModel;
import com.knx.inquirer.inqprime.model.PrimeTopicsModel;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.models.Option;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.utils.Converters;


@Database(entities = {NewsModel.class, Feed.class, Section.class, Option.class, NotificationModel.class, PrimeBookmarkModel.class, PrimeLikeModel.class, PrimeAccountInformationModel.class, PrimeSectionsModel.class, PrimeTopicsModel.class, PrimeNewsOfflineModel.class, BookmarksNewModel.class}, version = 24, exportSchema = false)
@TypeConverters(Converters.class) //New Additional: TYPE Converter for DATE to be able to use DATE as FIELD in the sqlite @TypeConverters not @TypeConverter
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase appDatabase;

    //instance of AppDatabase that returns Database
    public static AppDatabase getInstance(Context context){
        if(appDatabase == null){
            appDatabase = Room.databaseBuilder(context, AppDatabase.class, "database.db")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return appDatabase;  //returning the Database
    }

    public abstract NewsDao getNewsDao();   //create an Abstract of an interface NewsDao which has a Method getNewsDao()
}
