package com.knx.inquirer.database;


import android.content.Context;

import androidx.lifecycle.LiveData;

import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.model.PrimeLikeModel;
import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;
import com.knx.inquirer.inqprime.model.PrimeSectionsModel;
import com.knx.inquirer.inqprime.model.PrimeTopicsModel;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.models.Option;
import com.knx.inquirer.models.Section;

import java.util.List;

public class DatabaseHelper {

    private NewsDao newsDao;

    //upon calling or making an instance of DatabaseHelper class, the Database appDatabase class is being instantiated also
    public DatabaseHelper(Context context) {
        newsDao = AppDatabase.getInstance(context).getNewsDao();
    }

    public LiveData<List<NewsModel>> getNews(){
        return newsDao.getAllNews();
    }

    public void insertNews(List<NewsModel> news){
        newsDao.saveAll(news);
    }

    public void deleteNews(){
        newsDao.deleteAllNews();
    }

    public LiveData<Integer> getNewsRecordCount(){
        return newsDao.getNewsRecordCount();
    }


    public LiveData<List<Section>> getSections(){
        return newsDao.getAllSections();
    }

    public void insertSections(List<Section> sections){
        newsDao.saveSections(sections);
    }

    public void deleteSections(){
        newsDao.deleteAllSections();
    }

    public LiveData<Integer> getSectionRecordCount(){
        return newsDao.getSectionRecordCount();
    }


    /***FOR MYINQ/CUSTOM NEWS AND CUSTOM SECTION***/

    public LiveData<List<Feed>> getCustomNews(){
        return newsDao.getAllCustomNews();
    }

    public void insertCustomNews(List<Feed> news){
        newsDao.saveAllCustomNews(news);
    }

    public void deleteCustomNews(){
        newsDao.deleteAllCustomNews();
    }

    public LiveData<Integer> getCustomNewsRecordCount(){
        return newsDao.getCustomNewsRecordCount();
    }

    public LiveData<List<Option>> getCustomSections(){
        return newsDao.getAllCustomSections();
    }

    public void insertCustomSections(List<Option> sections){
        newsDao.saveCustomSections(sections);
    }

    public void deleteCustomSections(){
        newsDao.deleteAllCustomSections();
    }

    public LiveData<Integer> getCustomSectionRecordCount(){
        return newsDao.getCustomSectionRecordCount();
    }

    /***FOR NOTIFICATIONS***/

    //select
    public LiveData<List<NotificationModel>> getNotifications(){
        return newsDao.getAllNotifications();
    }

    //select
    public LiveData<Integer> getUnreadNotifsCount(){
        return newsDao.getUnreadNotifsCount();
    }

    //create
    public void insertNotification(NotificationModel notif){
        newsDao.saveNotification(notif);
    }

    //modify
//    public void updateNotificationStatus(int status, int id){
//        newsDao.updateNotifStatus(status, id);
//    }

    //androidx
    public void updateNotificationStatus2(int status, String newsId){
        newsDao.updateNotifStatus2(status, newsId);
    }

    //androidx
    public void readAllNotification(){
        newsDao.readAllNotification();
    }

    //modify
    public void deleteAllNotifications(){
        newsDao.deleteAllNotifications();
    }
    //modify
    public void deleteOneNotification(int id){
        newsDao.deleteOneNotification(id);
    }


    /***PRIME-PRIME-PRIME-PRIME***/
    //BOOKMARKS*************
    public void insertPrimeBookmark(PrimeBookmarkModel bookmark){
        newsDao.savePrimeBookmark(bookmark);  //method from newsDao is called
    }
    public  void deleteOnePrimeBookmark(int id){
        newsDao.deleteOnePrimeBookmark(id);
    }
    public LiveData<List<PrimeBookmarkModel>> getPrimeBookmarks(){return newsDao.getPrimeBookmarks();}

    //LIKE******************
    public void insertPrimeLike(PrimeLikeModel like){
        newsDao.savePrimeLike(like);  //method from newsDao is CALLED
    }
    public LiveData<List<PrimeLikeModel>> getPrimeLike(){return newsDao.getPrimeLike();} //method from newsDao is called and RETURNED


    //ACCOUNT INFORMATION*****************************
    public void insertPrimeAccountInformation(PrimeAccountInformationModel information){
        newsDao.savePrimeAccountInformation(information);
    }

    public LiveData<List<PrimeAccountInformationModel>> getPrimeAccountInformation(){return newsDao.getPrimeAccountInformation();} //method from newsDao is called and RETURNED

    public void updatePrimeAvatar(String avatar, int id){ newsDao.updatePrimeAvatar(avatar, id);}

    public void updatePrimeEmail(String email, int id){ newsDao.updatePrimeEmail(email, id);}

    public void updatePrimeMobileNo(String number, int id){ newsDao.updatePrimeMobileNo(number, id);}

    public void updatePrimeCardNo(String number, int id){ newsDao.updatePrimeCardNo(number, id);}

    public void updatePrimePlan(String plan, int id){ newsDao.updatePrimePlan(plan, id);}


    //SECTIONS and TOPICS*******************************
    //Sections
    public void insertPrimeSections(PrimeSectionsModel status){
        newsDao.savePrimeSections(status);
    }

    public LiveData<List<PrimeSectionsModel>> getPrimeSectionsStatus(){return newsDao.getPrimeSectionsStatus();} //method from newsDao is called and RETURNED

    public List<PrimeSectionsModel> getPrimeSections(){return newsDao.getPrimeSections();} //use for Initialization of record

    public void updatePrimeSectionStatus(Boolean status, int id){ newsDao.updatePrimeSectionStatus(status, id);}

    //Topics
    public void insertPrimeTopics(PrimeTopicsModel status){
        newsDao.savePrimeTopics(status);
    }

    public LiveData<List<PrimeTopicsModel>> getPrimeTopicsStatus(){return newsDao.getPrimeTopicsStatus();}

    public void updatePrimeTopicstatus(Boolean status, int id){ newsDao.updatePrimeTopicsStatus(status, id);}


    //NEWS OFFLINE *****************
//    public List<PrimeNewsOfflineModel> getAllPrimeNews(){
//        return newsDao.getAllPrimeNews();
//    }

    public LiveData<List<PrimeNewsOfflineModel>> getAllPrimeNews(){
        return newsDao.getAllPrimeNews();
    }

    public List<PrimeNewsOfflineModel> getArticleContent(int id){
        return newsDao.getArticleContent(id);
    }

    public void insertPrimeNews(List<PrimeNewsOfflineModel> news){
        newsDao.savePrimeNews(news);
    }

    public void deleteAllPrimeNewsOffline(){
        newsDao.deleteAllPrimeNewsOffline();
    }

    public void deleteSectionPrimeNewsOffline(int sectionId){
        newsDao.deleteSectionPrimeNewsOffline(sectionId);
    }

    public void updatePrimeNews(String author, String content, String imageType, int id){ newsDao.updatePrimeNews(author, content, imageType, id);}

    public void updatePrimeNewsImage(byte[] offline_photo, int id){ newsDao.updatePrimeNewsImage( offline_photo, id);}

    /**NEW ADDITIONAL NewDesign*/
    //NEW_BOOKMARK
    public void saveBookmark(BookmarksNewModel bookmark){
        newsDao.saveBookmark(bookmark);
    }
    public void deleteOneBookmark(int id){
        newsDao.deleteOneBookmark(id);
    }
    public LiveData<List<BookmarksNewModel>> getAllBookmarks(){return newsDao.getAllBookmarks();}

}
