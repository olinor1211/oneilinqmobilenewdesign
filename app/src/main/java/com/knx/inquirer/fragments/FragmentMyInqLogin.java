package com.knx.inquirer.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.JsonObject;
import com.knx.inquirer.R;
import com.knx.inquirer.WebActivity;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyInqLogin extends Fragment {
    private final String TAG = "FragmentMyInqLogin";

    private SharedPrefs sf;
    private ApiService apiService;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Global.LoaderDialog loaderDialog;



    private Button mLogin, mRegister;
    private EditText mMobileLogin, mPasswordLogin;

    String mobile="", password="";

    public FragmentMyInqLogin() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_my_inq_login, container, false);
        sf =  new SharedPrefs(getContext());
        apiService = ServiceGenerator.createService(ApiService.class);
        loaderDialog = new Global.LoaderDialog();

        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);
        initViews(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    private void initViews(View view) {
        mMobileLogin = view.findViewById(R.id.tvMobileLogin);
        mPasswordLogin = view.findViewById(R.id.tvPasswordLogin);
        mLogin = view.findViewById(R.id.btnLogin);
        mRegister = view.findViewById(R.id.btnRegLogin);

        mMobileLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mobile = mMobileLogin.getText().toString().trim();
                    if(mobile.isEmpty() || password.isEmpty()){
                        mLogin.setEnabled(false);
                        mLogin.setBackgroundResource(R.drawable.round_button_gray);
                    }
                    else{
                        mLogin.setEnabled(true);
                        mLogin.setBackgroundResource(R.drawable.round_button_blue);
                    }
                }
            });

        mPasswordLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                password = mPasswordLogin.getText().toString().trim();
                if(password.isEmpty() || mobile.isEmpty()){
                    mLogin.setEnabled(false);
                    mLogin.setBackgroundResource(R.drawable.round_button_gray);
                }else{
                    mLogin.setEnabled(true);
                    mLogin.setBackgroundResource(R.drawable.round_button_blue);
                }
            }
        });

        mLogin.setOnClickListener( view1 -> loginUser());
        mRegister.setOnClickListener(view1 ->{EventBus.getDefault().post("myInqRegister");});

    }

    private void loginUser(){
        Log.d(TAG, "Mobile1:"+ mobile + " Pass: "+password);
        mLogin.setEnabled(false);
        showLoader();
        apiService = ServiceGenerator.createService(ApiService.class);
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megalogin");
        apiService.login(String.valueOf(unixTime), sf.getDeviceId(), mobile, password, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonObject>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onSuccess(JsonObject jsonObject) {
                        //{"status":"ok","value":{"devicekey":"489232","userinfo":{"mobile":"09091112233","name":"nnnn","gender":"nan","email":"nnnn@g.com","bday":"2012-11-16","sections":"[19,8,18,6]"}}}
                        hideLoader();
                        try{
                            if(jsonObject.get("status").getAsString().equals("ok")){
                                sf.setDeviceKey(jsonObject.get("value").getAsJsonObject().get("devicekey").getAsInt()); //UNIQUE_ID
                                sf.setAsRegistered(true);
                                String mobile = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("mobile").getAsString();
                                String name = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("name").getAsString();
                                String gender = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("gender").getAsString();
                                String email = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("email").getAsString();
                                String bday = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("bday").getAsString();
                                String sections = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("sections").getAsString();

                                sf.setName(name);
                                sf.setEmail(email);
                                sf.setBdate(bday);
                                sf.setMobile(mobile);
                                sf.setGender(gender);
                                sf.setCustomMenu(sections);

                                EventBus.getDefault().post("loginSuccess"); //
                            }
                            else if(jsonObject.get("status").getAsString().equals("error")){
                                mLogin.setEnabled(true);
                                Toast.makeText(getContext(), "Mobile or password is incorrect", Toast.LENGTH_SHORT).show();
                            }else{
                                try {
                                    mLogin.setEnabled(true);
                                    Toast.makeText(getContext(), "Error: "+jsonObject.get("value").getAsJsonArray().get(0), Toast.LENGTH_SHORT).show();
                                }catch (Exception e) {
                                    Log.d(TAG, "onSuccess: Error JSon Structure: "+ e.getMessage());
                                }
                            }
                        }catch (Exception e){
                            mLogin.setEnabled(true);
                            Log.d(TAG, "onSuccess: Error JSon Structure: "+ e.getMessage());
                            Log.d(TAG, "onSuccess: Error JSon Structure: "+jsonObject.toString());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey();
                        hideLoader();
                        mLogin.setEnabled(true);
                        Log.d(TAG, "onError: Throwable Exception_Error: "+e.getMessage());
                    }
                });
    }
    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }
    public void showLoader() {loaderDialog.showDialog(getContext());}

    public void hideLoader() {
        loaderDialog.dismissDialog(getContext());
    }

}