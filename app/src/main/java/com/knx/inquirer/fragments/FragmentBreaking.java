package com.knx.inquirer.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.knx.inquirer.R;
import com.knx.inquirer.WebNewsActivity;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.models.Featured;
import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.models.PostArticleBreaking;
import com.knx.inquirer.models.PostUIUpdate;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.rvadapters.AdapterSections;
import com.knx.inquirer.rvadapters.RVAdapterBreaking;
import com.knx.inquirer.rvadapters.RVAdapterEmpty_Breaking;
import com.knx.inquirer.utils.ApiService;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryMenu;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryNews;
import com.knx.inquirer.viewmodels.BannerAdViewModel;
import com.knx.inquirer.viewmodels.BookmarkViewModel;
import com.knx.inquirer.viewmodels.MenuViewModel;
import com.knx.inquirer.viewmodels.NewsViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//
import android.os.Handler;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static java.lang.String.valueOf;

public class FragmentBreaking extends Fragment {
    private static final String TAG = "FragmentBreaking";

    /*** Views ***/
    private RecyclerView rvBreaking;
    private RecyclerView recyclerviewSections;
    private SwipeRefreshLayout mSwipeToRefreshLayout;
    private Snackbar snackbar;
    private TextView txtLoading, txtNetStatus;
    private ImageView bannerIvImage;

    //mobility
    private WebView webView;
    /*** Adapters ***/
    private AdapterSections adapter_sections;
    private RVAdapterBreaking adapter_breaking;
    private RVAdapterEmpty_Breaking adapter_empty_breaking;

    /*** Variables ***/
    private String aidStr; //the Last newsId of the element of newsmodel
    private String bannerAdLink;
    private String url;
    private String newsType="";
    private String newsPhoto="";
    private String newsTitle="";
    private String newsDate="";

    //NotifChangedStatusLive
    private MutableLiveData<Boolean> listenNotifChange = new MutableLiveData<>();
    private boolean areNotificationsEnabled = false;

    private boolean hasLoadedOnce = false, isConnected = true;
    private boolean isNetworkChanged = false;
    private boolean hasNewDailyKey = false;
    private Boolean isFragBreakingVisible = false;


    private Boolean isFirstLaunch = false, isLoadedAsRegisteredFirstTime = false, isFragmentVisible = false;

    private ArrayList<NewsModel> newsmodel = new ArrayList<>();
    private ArrayList<Section> sectionmodel = new ArrayList<>();
    private ArrayList<Featured> featuredmodel = new ArrayList<>();
    private ArrayList<Featured> featured = new ArrayList<>();
    private Handler customHandler = new Handler();

    private long startTime, timeInMilliseconds = 0;
    private int timeCounter;
    private int sectionType;
    //androidx
    private String bannerSectionId;
    private String newToken = "";
//    private int cachedLastSectionPosition = -1;

    /*** Utils ***/
    private SharedPrefs sf;
//    private Global.LoaderDialog loaderDialog;
    private Global.MyLoaderDialog loaderDialog;

    /*** ViewModel Components ***/
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiService apiService, apiService2;
    private NewsViewModel newsViewModel;
    private MenuViewModel menuViewModel;
    private BannerAdViewModel bannerAdViewModel;
    private BookmarkViewModel bookmarkViewModel;

    //
    private DatabaseHelper helper;
    private boolean bookmarkResult = false;
    private long mLastClickTime = 0;
//    private List<Related> relatedArticles = new ArrayList<>();
    private ArrayList<JsonObject> relatedArticles = new ArrayList<>();


    //New Additional NewDesign
    private final int BOOKMARK_PERMISSION_CODE = 20;
//    private HashSet<Integer> bookmark_id = new HashSet<>(); //HashSet= does not accept SAME VALUE

    public FragmentBreaking() {
        // Required empty public constructor
    }

    //with podcast
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if (msg.equals("network_access") || msg.equals("no_network_access")) {
            onConnectionChange(msg);
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onItemPostEvent(PostArticleBreaking item) {
        //OLD
        //viewArticle(String.valueOf(item.getItempos()));

        //NEW
        //checkTypeFirstBeforeGettingIt
        if(!String.valueOf(item.getNewsType()).isEmpty()){
            newsType = String.valueOf(item.getNewsType());
            newsPhoto = String.valueOf(item.getNewsPhoto());
            newsTitle = String.valueOf(item.getNewsTitle());
            newsDate = String.valueOf(item.getNewsDate());
        }
        viewArticleNew(String.valueOf(item.getItempos()));
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void dismissSnackbar(PostUIUpdate update) {
        if (update.getMsg().equals("dismiss_breaking"))
            if (snackbar != null && snackbar.isShown())
                snackbar.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FragmentBreaking_FRAGMENT_CALLED****");
        View view = inflater.inflate(R.layout.fragment_breaking, container, false);

        apiService = ServiceGenerator.createService(ApiService.class);
//        apiService2 = ServiceGenerator.createService2(ApiService.class);
        sf = new SharedPrefs(getContext());

        String DEVICE_ID = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        sf.setDeviceId(DEVICE_ID); //Set Device ID immediately
        //for NOTIFICATION DeepLink
        Global.isMainActivityOpen = true;
        //OBSERVE CHANGES OF NOTIFICATION Permission By the USER
        listenNotifChange.observe((LifecycleOwner) getContext(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                Log.d(TAG, "onChanged: listenNotifChange: "+ aBoolean);
                //If Changes Check First ifAlreadyAllowedNotification
                checkIfAlreadyAllowedNotification(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_CHECK_NOTIF_DEVICE_TOKEN);
            }
        });

//        callDebug();//TEST

        initViews(view);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FragmentBreaking_LAYOUT_CALLED****");
        //new Additional new design: checkIfBookmarked
        //mobility
        configWebViewSettings();
        loadHtml();

        if (!sf.isRegistered()) {
            if (!hasLoadedOnce && isAdded()) {
                getMenuAndNews();
                Log.d(TAG, "FRAGMENT_VISIBLE onCreateView: getMenuAndNews Not Registered Called");
                hasLoadedOnce = true;
            }
        } else {
            isLoadedAsRegisteredFirstTime = true;
            Log.d(TAG, "FRAGMENT_VISIBLE onCreateView: Registered Called ");
        }

        //RESOLVED not to have an API ERROR : MAKE sure DAILY key is ALready Available
//        final Handler handler = new Handler();
//        handler.postDelayed(() -> {
//            getBannerAd2();
//        }, 1000);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        postponeEnterTransition(); //

        helper = new DatabaseHelper(getContext());
        sf = new SharedPrefs(getContext());


        //register fragment to EventBus
        if(!EventBus.getDefault().isRegistered(FragmentBreaking.this)) EventBus.getDefault().register(FragmentBreaking.this);
//        EventBus.getDefault().register(FragmentBreaking.this);

        if (sf.isRegistered()) {
            setUserVisibleHint(false);
        } else {
            setUserVisibleHint(true);
        }

    }

//    //NEW ADDITIONAL new_design
//    private void checkIfBookmarked() {
//        File file = new File(getActivity().getApplicationContext().getExternalFilesDir(""),".Inquirer/"+idStr + ".html");
//        if(file.exists()){
//            imageBookmark.setImageResource(R.drawable.check);
//        }
//    }

    //checkIfAlreadyAllowedNotification
    private void checkIfAlreadyAllowedNotification(String deviceId, String token, String deviceType , String password){
        apiService.checkNotificationPermission(deviceId, token, deviceType, password)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseBody responseBody) {

                        try {
                            String r = responseBody.string();
                            Log.d(TAG, "onSuccess: NOTIFICATION checkNotificationPermission checkIfAlreadyAllowedNotification ResponseBody:" + r);
                            if(r.contains("Y")){///
                                sf.setNotificationAllowedAlready(true);
                                sf.setNotificationAllowed(true);
                                sf.setNewUser(false); //setNewUser Remain it false
                                Log.d(TAG, "onSuccess: set as NewUser FALSE");
                                Log.d(TAG, "onSuccess: NOTIFICATION checkIfAlreadyAllowedNotification  permission is ALLOWED");
                            }else if (r.contains("N")){
                                sf.setNotificationAllowedAlready(false);
                                sf.setNotificationAllowed(false); //setNotificationAllowed Make it FALSE to displayDialog again after VersionUpdate
//                                //MovedToMainAct_VersionUpdate_ sf.setNewUser(true) = MUST DEPEND on hasNotificationAllowed_is_False //setNewUser Make it TRUE to displayDialog again after VersionUpdate
                                Log.d(TAG, "onSuccess: set as NewUser TRUE");
                                Log.d(TAG, "onSuccess: checkIfAlreadyAllowedNotification PrimeNotification permission is NOT ALLOWED");
                            }
                            //UpdateApiDeviceNotifPermission
                            updatedDeviceNotifPermission();

                        } catch (IOException e) {
                            Log.d(TAG, "onSuccess: NOTIFICATION IOException_Error: "+e.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: NOTIFICATION checkNotificationPermission checkIfAlreadyAllowedNotification Throwable_Error " + e.getMessage());

                    }
                });
    }

    private void updatedDeviceNotifPermission(){
        Log.d(TAG, "updateDeviceNotifPermission: NOTIFICATION hasNotificationAllowedAlready: "+sf.hasNotificationAllowedAlready());
        Log.d(TAG, "updateDeviceNotifPermission: NOTIFICATION areNotificationsEnabled: "+ areNotificationsEnabled);
        if (areNotificationsEnabled) { //DEFAULT is TRUE upon Installation
            if (sf.hasNotificationAllowedAlready()) {  //if (sf.hasNotificationAllowed()) {
                Log.d(TAG, "updateDeviceNotifPermission; NOTIFICATION checkNotificationSettings: hasNotificationAllowedAlready: TRUE_CALLED***");
            } else {
                //CHECK_FIRST if PermissionChangedManually
                if(!sf.hasNotifPermissionChangedManually()){
                    Log.d(TAG, "updateDeviceNotifPermission: NOTIFICATION hasNotifPermissionChangedManually: NOT YET FALSE_CALLED***");
                    Log.d(TAG, "updateDeviceNotifPermission; NOTIFICATION hasNotifPermissionChangedManually: NOT_YET  BUT NOT_ALLOWED hasNotificationAllowedAlready: "+ sf.hasNotificationAllowedAlready());
                }else{
                    Log.d(TAG, "updateDeviceNotifPermission; NOTIFICATION API_CALLED***POSITIVE");
                    registerNotificationPermission(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_STORE_NOTIF_DEVICE, Global.POSITIVE_NOTIF_PERMISSION);
                }
            }

        } else {
            if (!sf.hasNotificationAllowedAlready()) {
                Log.d(TAG, "updateDeviceNotifPermission: NOTIFICATION checkNotificationSettings: hasNotificationAllowedAlready: FALSE_CALLED***");
            } else {
                Log.d(TAG, "updateDeviceNotifPermission; NOTIFICATION API_CALLED***NEGATIVE");
                registerNotificationPermission(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_STORE_NOTIF_DEVICE, Global.NEGATIVE_NOTIF_PERMISSION);
            }
        }
    }


    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: called");
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
            Log.d(TAG, "onDestroy: called");
        }
        super.onDestroy();
    }

    public void initViews(View view) {
//        //Initialize dialog once only to avoid multiple popups
//        loaderDialog = new Global.LoaderDialog(); //
        //Get instance for Singleton
        loaderDialog =  Global.MyLoaderDialog.getInstance(); //

        //mobility
        webView = view.findViewById(R.id.breaking_webView);
        txtLoading = view.findViewById(R.id.txtloadingsection);
        txtNetStatus = view.findViewById(R.id.txtnetstatus);
        bannerIvImage = view.findViewById(R.id.ivAdImage);
        mSwipeToRefreshLayout = view.findViewById(R.id.swipenews);
        rvBreaking = view.findViewById(R.id.recyclerview);
        recyclerviewSections = view.findViewById(R.id.rvtags);

        txtNetStatus.setVisibility(View.GONE);
//        bannerIvImage.setAlpha(200);
        bannerIvImage.getImageMatrix();
        Log.d(TAG, "initViews: bannerIvImage getImageMatrix: "+ bannerIvImage.getImageMatrix().toShortString());
        bannerIvImage.setOnClickListener(v -> {

            try {
                //Intent to new browser
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(bannerAdLink));
                startActivity(intent);

            } catch (Exception e) {
                Log.d(TAG, "Try_Exception bannerAdLink_intent: " + e.getMessage());
            }

        });

        mSwipeToRefreshLayout.setColorSchemeColors(Color.parseColor("#003877")); //color swipe
        mSwipeToRefreshLayout.setOnRefreshListener(() -> {
            getMoreNewsOnSwipe(sf.getLastSection());
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),layoutManager.getOrientation()); //item decorations
        rvBreaking.setHasFixedSize(true);
        rvBreaking.setLayoutManager(layoutManager);
//        rvBreaking.addItemDecoration(dividerItemDecoration); //item decorations
        //
//        rvBreaking.getRecycledViewPool().setMaxRecycledViews(VIEW_TYPE_PODCAST,0); //Pod
        //
        LinearLayoutManager llmTags = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerviewSections.setLayoutManager(llmTags);

        //RV SCROLL LISTENER
//        rvBreaking.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                int pos = layoutManager.findFirstVisibleItemPosition();
//                int pos2 = layoutManager.findLastVisibleItemPosition();
//                int pos2 = layoutManager.findLastVisibleItemPosition();
//
//                //Idle state of RV:
////                if (newState == RecyclerView.SCROLL_STATE_IDLE){
////                }
//
////                int pos = layoutManager.getPosition();   // track down the item position
//
//                //ACCESSING RecyclerView ViewHolder class
//                RecyclerView.ViewHolder viewHolder = rvBreaking.findViewHolderForAdapterPosition(pos);
////                RecyclerView.ViewHolder viewHolder = rvBreaking.findViewHolderForAdapterPosition(pos);
//                ImageView bookmark = viewHolder.itemView.findViewById(R.id.ivNewsListBookmark); //BOOKMARK ImageVIew
////                String news_id = newsmodel.get(pos).getId(); //
//                //bookmark checking.. newsId is originally String, convert to int
//                int news_ID = Integer.valueOf(newsmodel.get(pos).getId());
//                Log.d(TAG, "onScrollStateChanged: called news_ID " + news_ID);
//
//                if(bookmark_id.contains(news_ID)){ // if(bookmark_id.contains(news_ID)){ //bookmark_id is a HASHset
//                    Log.d(TAG, "onScrollStateChanged: FOUND news_ID " + news_ID);
//                    bookmark.setImageResource(R.drawable.bookmark_fill);
//                }
////                else bookmark.setImageResource(R.drawable.bookmark_prime);
//
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//            }
//        });
//
//
    }


    public void onArticleRequest() {
//        Global.isItemClicked = true;
        showLoader();
        mSwipeToRefreshLayout.setEnabled(false);
    }

    public void onArticleRequestSuccess() {
//        Global.isItemClicked = false;
        hideLoader();
        mSwipeToRefreshLayout.setEnabled(true);
    }

    public void onArticleRequestError() {
        Global.isItemClicked = false;
        hideLoader();
    }

    public void showLoader() {
        //show only if Fragment is VISIBLE
        if(isFragBreakingVisible) loaderDialog.showDialog(getContext());

    }

    public void hideLoader() {
        //show only if Fragment is VISIBLE
        if(isFragBreakingVisible) loaderDialog.dismissDialog(getContext());
    }

    public void RetryFetchMenuAndNews() {
        if(Global.isNetworkAvailable(getContext())){
            if(sf.getDailyKey().equals("NO_KEY")) getDailyKey(); //Resolve Cannot Get NewsData when App OPENED with NO INTERNET: Get Key First
            if(sf.getFirebaseToken().equals("NO_TOKEN")) getFirebaseKey();  //Resolve Cannot Get NewsData when App OPENED with NO INTERNET:  Get Key First
        }
        setBlankAdapter(); //
        mSwipeToRefreshLayout.setRefreshing(false);
        getMenuAndNews();
    }

    //
    public void RetryFetchNewsOnly() {
        if(Global.isNetworkAvailable(getContext())){
            if(sf.getDailyKey().equals("NO_KEY")) getDailyKey(); //Resolve Cannot Get NewsData when App OPENED with NO INTERNET: Get Key First
            if(sf.getFirebaseToken().equals("NO_TOKEN")) getFirebaseKey();  //Resolve Cannot Get NewsData when App OPENED with NO INTERNET:  Get Key First
        }
        setBlankAdapter(); //
        mSwipeToRefreshLayout.setRefreshing(false);
        getNewsBySection(sf.getLastSection(), ( sf.getLastSectionPosition() == -1 ? 0 : sf.getLastSectionPosition() ), url);
    }

    ////  EventBus.getDefault().removeStickyEvent(PostPodcastWebNewsAct.class);
    //change section
    public void ChangeSection(int section, int pos, String webUrl) {

        url = webUrl; //to resolve RetryFetchNewsOnly..

        //send timeCounter after removing callBacks ..get the getLastSection as the reference section
        sf.setListViewReadsCount(timeCounter);  //save.. timeCounter first
        Log.d(TAG, "ChangeSection: Recorded COUNTER is  " + sf.getListViewReadsCount() + " LAST_SECTION: " + sf.getLastSection());
        customHandler.removeCallbacks(updateTimerThread);  //stop.. handler runnable

        if (sf.getListViewReadsCount() >= 1) {
            String sectionId = valueOf(sf.getLastSection());
            registerTimeListViewReads(sf.getDeviceId(), sectionId, sf.getListViewReadsCount(), Global.passStoreUserReadTimeList);
        }
        //restart counting..
        timeCounter = 0;  //reset.. timeCounter
        customHandler.postDelayed(updateTimerThread, 2000);  //start.. handler runnable
        Log.d(TAG, "ChangeSection: counting " + timeCounter);

        sectionType = section;
        Log.d(TAG, "ChangeSection: section is " + sectionType);
        getNewsBySection(section, pos, webUrl);
    }

    //registerTimeListViewReads
    private void registerTimeListViewReads(String deviceId, String section, int seconds, String password) {
        Log.d(TAG, "registerTimeListViewReads: called ");
        apiService.registerTimeListViewReads(deviceId, section, seconds, password)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        Log.d(TAG, "onSubscribe: registerTimeListViewReads: called ");
                    }

                    @Override
                    public void onSuccess(ResponseBody responseBody) {
                        Log.d(TAG, "onSuccess: registerTimeListViewReads called: SECTION= " + section + " TIME= " + seconds);

                        try {
                            String r = responseBody.string();
                            Log.d(TAG, "onSuccess: ResponseBody:" + r);
                            if (r.contains("Store Successfully")) {///
//                                sf.setNotificationAllowed(false);
                                Log.d(TAG, "onSuccess: registerTimeListViewReads Successfully");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: registerTimeListViewReads error  getMessage" + e.getMessage());
                    }
                });
    }

    public void setBlankAdapter() {
        //need to visible the txtLoading first. reserved execution
        txtLoading.setText("Loading sections...");
        rvBreaking.setAdapter(null);
    }

    private void setEmptySection() {
        txtLoading.setText("Failed to load sections");
    }

    //Connection Error
    private void setEmptyAdapter(String msg, int method) {
        adapter_empty_breaking = new RVAdapterEmpty_Breaking(this, msg, method);
        rvBreaking.setAdapter(adapter_empty_breaking);
    }

    private void onPreloadingSection() {
        showLoader();
        mSwipeToRefreshLayout.setEnabled(false);
    }

    private void onSuccessLoadingSection() {
        hideLoader();
        mSwipeToRefreshLayout.setEnabled(true);
    }

    private void onErrorLoadingSection() {
        hideLoader();
        mSwipeToRefreshLayout.setEnabled(true);
    }


    private void onConnectionChange(String msg) {
        Log.d(TAG, "onConnectionChange: isAdded: "+isAdded());
        Log.d(TAG, "onConnectionChange: isVisible: "+isVisible());
        Log.d(TAG, "onConnectionChange: isVisible: "+ isFragmentVisible);
        Log.d(TAG, "onConnectionChange: newsmodel.size: "+newsmodel.size());
        Log.d(TAG, "onConnectionChange: msg: "+ msg);

        if (isAdded() && isVisible())
            if (msg.equals("network_access")) {
                txtNetStatus.setVisibility(View.GONE);
                isNetworkChanged = true;
                //No Need to call
//                if (newsmodel.size() > 0) getMenuAndNews(); //Call API if no news has shown

            } else if (msg.equals("no_network_access")) {
                isNetworkChanged = false;
                txtNetStatus.setVisibility(View.VISIBLE);
                txtNetStatus.setText("No network connection. You are viewing offline news");
            }

        Log.d(TAG, "onConnectionChange: isAdded() and isVisible() called");

    }

    /*** Begin ViewModel methods ***/
    //androidx implementation ONEIL
    private void getMenuAndNews() {
        Log.d(TAG, "getMenuAndNews: isNetworkChanged "+ isNetworkChanged);
        if(!isNetworkChanged) onPreloadingSection(); //Dont display Loading Process when NetWorkIsChanged
        Log.d(TAG, "getMenuAndNews: called");

        /** GET_MENU GET_MENU GET_MENU GET_MENU GET_MENU GET_MENU GET_MENU  */
        menuViewModel = ViewModelProviders.of(FragmentBreaking.this, new ViewModelFactoryMenu(getActivity().getApplication(), compositeDisposable, sf)).get(MenuViewModel.class);
        menuViewModel.getMenu().observe(this, response -> {

            Log.d(TAG, "getMenuAndNews: menuViewModel.getMenu RUN");
            txtLoading.setVisibility(View.GONE); //
            txtNetStatus.setVisibility(View.GONE);

            //FIRST CONTROL WHAT Section_Tags will DISPLAY/ACTIVE
            //**getLastSection** = based on the ID of the Section_Tags
            try {
                Log.d(TAG, "getMenuAndNews: LAST_SECTION _1: " + sf.getLastSection());
                //RESOLVE: For FirstLaunch after Installation: Set the LastSection depends on SectionId of the ZERO Element in the RVList of sectionmodel
                //**LastSectionNewsList** default to Whatever the ID of the first_element in the sections array
                if (sf.getLastSection() <= 0) {
                    sf.setLastSection(response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsInt()); //LastSectionNewsList is depend on the BACKEND_SIDE
                    sf.setSectionName(response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(0).getAsJsonObject().get("title").getAsString().toUpperCase()); //SectionName upon new Installation of the APP: Get the ZERO index title
                    sf.setSectionNameTempContainer(sf.getSectionName());
                    Log.d(TAG, "getMenuAndNews: LAST_SECTION _2: " + sf.getLastSection());
                }
            } catch (Exception e) {
                Log.d(TAG, "getMenuAndNews: Exception_LastSection: " + e.getMessage());
            }

            Log.d(TAG, "getMenuAndNews: LAST_SECTION_3: " + sf.getLastSection());
            //RESOLVED: firstLaunch_firstInstallOfApp display AD if Section placed in zero element in the RViewList has Banenr Ad
            //CHECK if the Section ID has a Banner
            checkAd2(String.valueOf(sf.getLastSection()));
//            //GET BANNER ADS  ..MOVED TO MAIN_ACTIVITY
//            getBannerAd2(response);

            //GET SECTION TAGS
            sectionmodel.clear();
            try {
                for (int x = 0; x < response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().size(); x++) {
                    sectionmodel.add(new Section(
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt(),
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("title").getAsString(),
                            "",
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("color").getAsString(),
                            false));
                }
                //ADD ALSO TO SECTION TAGS: FOR FEATURED SECTIONS like MOBILITY
                if (response.get("menu").getAsJsonObject().has("featured")) {
                    for (int x = 0; x < response.get("menu").getAsJsonObject().get("featured").getAsJsonArray().size(); x++) {
                        sectionmodel.add(new Section(
                                response.get("menu").getAsJsonObject().get("featured").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt(),
                                response.get("menu").getAsJsonObject().get("featured").getAsJsonArray().get(x).getAsJsonObject().get("title").getAsString(),
                                response.get("menu").getAsJsonObject().get("featured").getAsJsonArray().get(x).getAsJsonObject().get("url").getAsString(),
                                response.get("menu").getAsJsonObject().get("featured").getAsJsonArray().get(x).getAsJsonObject().get("color").getAsString(),
                                false));
                    }
                }
            } catch (Exception e) {
                Log.d(TAG, "getMenuAndNews: Exception_Sections_Featured_Error: " + e.getMessage());
            }

            /** Database */
            helper.deleteSections(); //Delete
            helper.insertSections(sectionmodel); //Add
//            //SAVE sectionModel to Global ArrayList
            //MOve to REPO
//            Global.sectionList.clear();
//            Global.sectionList.addAll(sectionmodel);
            //For Debugging: ArrayList To JSON_STRING
//            Gson gson2 = new Gson();
//            String actualData = gson2.toJson(sectionmodel);

            //Set the lastSectionPosition based on last sectionId which is sf.getLastSection()
            int lastPosition = scrollToPosition(sf.getLastSection(), sectionmodel);


            Log.d(TAG, "getMenuAndNews: LastSectionPosition: " + sf.getLastSectionPosition());
            //**getLastSectionPosition** = based to RVList index
            //AdapterSection MENU TAG DISPLAY
//            adapter_sections = new AdapterSections(getContext(), menu.getMenu().getSections(), FragmentBreaking.this, sf);
            adapter_sections = new AdapterSections(getContext(), sectionmodel, FragmentBreaking.this, sf);
            recyclerviewSections.setAdapter(adapter_sections);
            adapter_sections.notifyDataSetChanged();
            //Default Position
            recyclerviewSections.getLayoutManager().scrollToPosition(lastPosition); // recyclerviewSections.getLayoutManager().scrollToPosition(sf.getLastSectionPosition());

            /** GET_NEWS GET_NEWS GET_NEWS GET_NEWS GET_NEWS GET_NEWS GET_NEWS  */
            newsViewModel = ViewModelProviders.of(FragmentBreaking.this, new ViewModelFactoryNews(getActivity().getApplication(), compositeDisposable, sf)).get(NewsViewModel.class);
            newsViewModel.getNews(sf.getLastSection(), ( sf.getLastSectionPosition() == -1 ? 0 : sf.getLastSectionPosition() )).observe(this, news -> {
                Log.d(TAG, "getMenuAndNews: newsViewModel.getNews_RUN");
//                if(!isNetworkChanged) onSuccessLoadingSection(); //DISMISS DIALOG
                onSuccessLoadingSection(); //DISMISS DIALOG
                isNetworkChanged = false; //Default

                //Debugging: convert pojo to json using GSON
//                Gson gson = new Gson();
//                String ne = gson.toJson(news);
//                Log.d(TAG, "getMenuAndNews: NEWS_GSON " + ne); //checking response as String

                try {
                    newsmodel.clear();
                    newsmodel.addAll(news);
                    //TEST DATA Moved to Repository
                    Log.d(TAG, "getMenuAndNews: newsmodel_news_is_ADDED:");
                } catch (Exception e) {
                    Log.d(TAG, "getMenuAndNews: Try_Exception newsmodel_error:" + e.getMessage());
                }
                //RVAdapterBreaking  NEWS DISPLAY
                adapter_breaking = new RVAdapterBreaking(getContext(), newsmodel, this, sf);
                rvBreaking.setAdapter(adapter_breaking);
                adapter_breaking.notifyDataSetChanged();

                //
//                callDebugMenu1();  //Testing Debug
                //calling REMOVE TEMP
//                adapter_breaking.setLoadMoreListener(() -> getMoreNewsOnScroll(sf.getLastSection()));
                if (newsmodel.size() > 0) {
                    adapter_breaking.setMoreDataAvailable(true); //Flag that there are NEWS data
                } else {
                    adapter_breaking.setMoreDataAvailable(false);
                }

            });

            //VIEW_MODEL_COUNT: 2
            //return BOOLEAN getNewsError...then return ERROR Message
            newsViewModel.getNewsError().observe(this, isError -> {
                if (isError) {
                    onErrorLoadingSection();
//                    if(!isNetworkChanged) onErrorLoadingSection();
                    newsViewModel.getNewsThrowable().observe(FragmentBreaking.this, e -> {
                        String err_msg = Global.getResponseCode(e); //e only
                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)) toastMessage(TAG+": NewsError: "+ e.getMessage()); // //Display in developer
                        setEmptyAdapter(err_msg, 1);  //getMenuAndNews getMenu getNewsError getNewsThrowable
                        Log.d(TAG, "getMenuAndNews: newsViewModel.getNewsError HAS ERROR: " + e.getMessage());
                    });
                    Log.d(TAG, "getMenuAndNews: newsViewModel.getNewsError RUN");
                }
//                else {
//                    Log.d(TAG, "getMenuAndNews: newsViewModel.getNewsError NO ERROR");
//                    return;
//                }
            });
        });


        //for OFFLINE..NO CONNECTION
        //VIEW_MODEL_COUNT: 4
        //return BOOLEAN getMenuError...then return ERROR Message
        //Observe menu API error..
        menuViewModel.getMenuError().observe(this, isError -> {
            if (isError) {
//                if(!isNetworkChanged) onErrorLoadingSection();
                onErrorLoadingSection();
                Log.d(TAG, "getMenuAndNews getMenuError: ERROR "+ isError.toString());
                //Observe db OFFLINE MENU COUNT..
                menuViewModel.getSectionRecordCount().observe(FragmentBreaking.this, count -> {
                    if (count > 0) {
                        Log.d(TAG, "getMenuAndNews: getSectionRecordCount "+ count.toString());

                        getCachedMenu(); //GET MENU TAG ..OFFLINE

                        //Display: Network message NO Connection
                        if (!Global.isNetworkAvailable(getContext())) {
                            txtNetStatus.setVisibility(View.VISIBLE);
                            txtNetStatus.setText("No network connection. You are viewing offline news.");
                        } else {
                            txtNetStatus.setVisibility(View.GONE);
                        }

                        //Observe OFFLINE db NEWS COUNT..
                        menuViewModel.getNewsRecordCount().observe(FragmentBreaking.this, count_news -> {
                            Log.d(TAG, "getMenuAndNews: getNewsRecordCount "+ count_news.toString());
                            if (count_news > 0) {
                                getCachedNews(); //GET CACHED NEWS  ..OFFLINE
                            } else {
                                menuViewModel.getMenuThrowable().observe(FragmentBreaking.this, e -> {
                                    String err_msg = Global.getResponseCode(e);
                                    setEmptyAdapter(err_msg, 0); //getMenuAndNews  getMenuError getSectionRecordCount getNewsRecordCount
                                    Log.d(TAG, "getMenuAndNews: getMenuThrowable "+ err_msg);
                                });

                            }
                        });

                    } else {
                        txtNetStatus.setVisibility(View.GONE);
//                        setEmptySection();
                        menuViewModel.getMenuThrowable().observe(FragmentBreaking.this, e -> {
                            String err_msg = Global.getResponseCode(e);
                            setEmptyAdapter(err_msg, 0);
                            Log.d(TAG, "getMenuAndNews: getSectionRecordCount ZERO and ERROR"+ err_msg);
                            if(isNetworkChanged) isNetworkChanged = false; //Default After API reCALLING
                        });
                    }
                });
            }
        });

        /**NEW ADDITIONAL NewDesign: For BookmarkIdList*/  /**NEW ADDITIONAL NewDesign: For BookmarkIdList*/
        bookmarkViewModel = ViewModelProviders.of(this).get(BookmarkViewModel.class); //initialize view_model PROVIDER
        bookmarkViewModel.getBookmarkList().observe(this, data -> { //Accessing view_model METHOD
            Global.bookmarkIdList.clear(); //Need TO CLEAR to be UPDATED every CHANGES
            Log.d(TAG, "getBookmarkList: called size DATA " + data.size());
            //Get the BOOKMARK_ID only: Will use for marking those already bookmarked
            for (int x = 0; x < data.size(); x++) {
                Global.bookmarkIdList.add(data.get(x).getNewsId()); //add to HashSet
            }
            if(adapter_breaking!=null)adapter_breaking.notifyDataChanged();
        });
    }
    //
    private void getNewsBySection(int section, int pos, String url) {

        Log.d(TAG, "getNewsBySection: section " + section);  //id 100 is ok

        //for Mobility
        if (section >= 200) {
            rvBreaking.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            webView.loadUrl("https://inqmobility.com/?m=1");  //"https://www.google.com" //https://inqmobility.com/?m=1  //webView.loadUrl(url);

        } else {
            webView.setVisibility(View.GONE);
//            rvBreaking.setVisibility(View.VISIBLE);  //moved location to..after API success to resolve delay disappearance of last section news coming from mobility

            Log.d(TAG, "getNewsBySection: called .. section" + section);

            onPreloadingSection();

            //RX
            newsViewModel = ViewModelProviders.of(FragmentBreaking.this, new ViewModelFactoryNews(getActivity().getApplication(), compositeDisposable, sf)).get(NewsViewModel.class);
            newsViewModel.getNews(section, pos).observe(FragmentBreaking.this, (List<NewsModel> news) -> {
                Log.d(TAG, "getNewsBySection: newsViewModel.getNews RUN");
                onSuccessLoadingSection();

//                Gson gson = new Gson();
//                String ne2 = gson.toJson(news);
//                Log.d(TAG, "getNewsBySection: GSON_Response: "+ne2);

                try{
                    newsmodel.clear();
                    newsmodel.addAll(news);
                }catch(Exception e){
                    Log.d(TAG, "getNewsBySection: newsViewModel_Error: "+e.getMessage());
                }

                adapter_breaking = new RVAdapterBreaking(getContext(), newsmodel, this, sf);
                rvBreaking.setAdapter(adapter_breaking);
                adapter_breaking.notifyDataSetChanged();

                adapter_breaking.setLoadMoreListener(() -> getMoreNewsOnScroll(sf.getLastSection()));

                if (newsmodel.size() > 0) {
                    rvBreaking.setVisibility(View.VISIBLE);  //Resolved: last section's delay disappearance coming from mobility
                    adapter_breaking.setMoreDataAvailable(true);
                } else {
                    adapter_breaking.setMoreDataAvailable(false);
                }

            });

            newsViewModel.getNewsError().observe(this, isError -> {
                if (isError) {
                    onErrorLoadingSection();
                    newsViewModel.getNewsThrowable().observe(FragmentBreaking.this, e -> {
                        String error_msg = Global.getResponseCode(e);
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
                            toastMessage(TAG+": NewsError: "+ e.getMessage()); // //Display in developer
                        }else{
                            toastMessage(error_msg);
                        }
                    });
                    adapter_sections.refreshSection();
                    recyclerviewSections.getLayoutManager().scrollToPosition(pos); //recyclerviewSections.getLayoutManager().scrollToPosition(pos);
                }
//                else {
//                    return;
//                }
            });

        }
    }

    //new navBar sections ..
    private void getCachedMenu() {

        menuViewModel = ViewModelProviders.of(FragmentBreaking.this, new ViewModelFactoryMenu(getActivity().getApplication(), compositeDisposable, sf)).get(MenuViewModel.class);
        menuViewModel.getCachedMenu().observe(FragmentBreaking.this, (List<Section> sections) -> {
            Log.d(TAG, "getCachedMenu: getMenuAndNews CALLED ");
            txtLoading.setVisibility(View.GONE); //
            ArrayList<Section> sectionArrayList = new ArrayList<>(sections); //Adding sections in the PARAM same with AddALl()
            //DEBUGGING
//            Gson gson = new Gson();
//            String d = gson.toJson(sectionArrayList);
//            Log.d(TAG, "getCachedMenu: Sections "+ d);

            //Set the lastSectionPosition based on last sectionId which is sf.getLastSection()
            int lastPosition = scrollToPosition(sf.getLastSection(), sectionArrayList);

            adapter_sections = new AdapterSections(getContext(), sectionArrayList, FragmentBreaking.this, sf);
            recyclerviewSections.setAdapter(adapter_sections);
            adapter_sections.notifyDataSetChanged();
            //GOto Specific SectionTag
            recyclerviewSections.getLayoutManager().scrollToPosition(lastPosition);  //recyclerviewSections.getLayoutManager().scrollToPosition(sf.getLastSectionPosition());
        });
    }

    //Find the Index of sectionId(which is the sf.getLastSection()) in the List: Because sometimes the sequence is not the same when ONLINE
    private int scrollToPosition(int lastSection, ArrayList<Section> sectionArrayList ){
        if(sectionArrayList.size()>0){
            for(int x=0; x<sectionArrayList.size();x++){
                if(sectionArrayList.get(x).getId()==lastSection){
                    lastSection = x; //exact Index of sectionId in the List OFFLINE
                    sf.setLastSectionPosition(x); //To be used as ACTIVE Section
                    break;
                }
            }
        }
        return lastSection; //return
    }

    //new news fragment breaking
    private void getCachedNews() {
        newsViewModel = ViewModelProviders.of(FragmentBreaking.this, new ViewModelFactoryNews(getActivity().getApplication(), compositeDisposable, sf)).get(NewsViewModel.class);
        newsViewModel.getCachedNews().observe(FragmentBreaking.this, (List<NewsModel> news) -> {

            onSuccessLoadingSection();

            newsmodel.clear();

            newsmodel.addAll(news);

            adapter_breaking = new RVAdapterBreaking(getContext(), newsmodel, this, sf);
            rvBreaking.setAdapter(adapter_breaking);
            adapter_breaking.notifyDataSetChanged();

            adapter_breaking.setLoadMoreListener(() -> getMoreNewsOnScroll(sf.getLastSection()));

            if (newsmodel.size() > 0) {
                adapter_breaking.setMoreDataAvailable(true);
            } else {
                adapter_breaking.setMoreDataAvailable(false);
            }
        });
    }

    /*** End ViewModel methods ***/

    /*** Start RxJava methods ***/

    private void getMoreNewsOnScroll(int section) {

        try{
            if (!newsmodel.contains(null)) {
                aidStr = newsmodel.get(newsmodel.size() - 1).getId();
            }
            //running in MainThread
            rvBreaking.post(() -> {
                if (!newsmodel.contains(null))
                    newsmodel.add(null);
                adapter_breaking.notifyItemInserted(newsmodel.size() - 1);
            });

            apiService.getDailyKey()
                    .flatMap((Function<String, SingleSource<ArrayList<NewsModel>>>) key -> {
                        long unixTime = System.currentTimeMillis() / 1000L;
                        String hashed = Global.Generate32SHA512(key + unixTime + "inq" + sf.getDeviceId() + "-megascroll/" + section + "/" + "b" + "/" + aidStr);
                        sf.setDailyKey(key);
                        return apiService.getMoreNews(String.valueOf(unixTime), sf.getDeviceId(), section, Integer.parseInt(aidStr), "b", hashed);
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<ArrayList<NewsModel>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(ArrayList<NewsModel> news) {

                            if (newsmodel.contains(null)) {
                                newsmodel.remove(newsmodel.size() - 1);
                                adapter_breaking.notifyItemRemoved(newsmodel.size());
                            }

                            newsmodel.addAll(news);
                            hideLoader();
                            adapter_breaking.setLoadMoreListener(() -> {
                                getMoreNewsOnScroll(sf.getLastSection());
                            });

                            if (newsmodel.size() > 0) {
                                adapter_breaking.setMoreDataAvailable(true);
                            } else {
                                adapter_breaking.setMoreDataAvailable(false);
                            }
                            adapter_breaking.notifyDataChanged();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (newsmodel.contains(null)) {
                                newsmodel.remove(newsmodel.size() - 1);
                                adapter_breaking.notifyItemRemoved(newsmodel.size());
                            }

                            String error_msg = Global.getResponseCode(e);

                            snackbar = Snackbar.make(getActivity().findViewById(R.id.snackbarContainer), error_msg, Snackbar.LENGTH_INDEFINITE)
                                    .setAction("MORE NEWS", view -> {
                                        rvBreaking.smoothScrollToPosition(newsmodel.size());
                                        getMoreNewsOnScroll(section);
                                    });
                            snackbar.show();
                        }
                    });

        }catch (Exception e) {
            Log.d(TAG, "getMoreNewsOnScroll: Exception: "+e.getMessage());
        }
    }

    private void getMoreNewsOnSwipe(int section) {
        try{
            mSwipeToRefreshLayout.setRefreshing(true);
            apiService.getDailyKey()
                    .flatMap((Function<String, SingleSource<ArrayList<NewsModel>>>) key -> {
                        long unixTime = System.currentTimeMillis() / 1000L;
                        String hashed = Global.Generate32SHA512(key + unixTime + "inq" + sf.getDeviceId() + "-megascroll/" + section + "/" + "a" + "/" + newsmodel.get(0).getId());
                        sf.setDailyKey(key);
                        return apiService.getMoreNews(String.valueOf(unixTime), sf.getDeviceId(), section, Integer.parseInt(newsmodel.get(0).getId()), "a", hashed);
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<ArrayList<NewsModel>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(ArrayList<NewsModel> news) {

                            mSwipeToRefreshLayout.setRefreshing(false);

                            if (news.size() > 0) {
                                if (!newsmodel.contains(news)) {
                                    newsmodel.addAll(0, news);
                                    //Sort news from latest
                                    Collections.sort(newsmodel, (o1, o2) -> o2.getId().compareTo(o1.getId()));
                                    adapter_breaking.notifyDataChanged();
                                }
                                helper.insertNews(news);  //INSERT news TO DATABASE
                            } else {
                                //getActivity().getApplicationContext() =  has small text in toast
//                                toastMessage("");
                                if(newsmodel.size()>0)toastMessage("Latest news stories are loaded");

//                            Toast.makeText(getActivity().getApplicationContext(), "Already have the latest news", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            mSwipeToRefreshLayout.setRefreshing(false);
//                        String error_msg = Global.getResponseCode(e);
                            if(!Global.isNetworkAvailable(getContext())){
                                toastMessage("No network connection");
                            }else{
                                if(newsmodel.size()>0)toastMessage("Latest news stories are loaded");
                            }

                        }
                    });

        }catch (Exception e){
            mSwipeToRefreshLayout.setRefreshing(false);
            Log.d(TAG, "getMoreNewsOnSwipe: Exception:"+e.getMessage());
        }


    }

    private void viewArticle(String id) {
        onArticleRequest();
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
        apiService.getArticle2(String.valueOf(unixTime),sf.getDeviceId(), id, hashed)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try{
                            Log.d(TAG, "onResponse: VIEW_ARTICLE: "+response.body().toString());
                            hasNewDailyKey = false;
                            //{"type":"1", custom":null, //"byline":null, //"content": //"title":"
                            Global.isItemClicked = true;
                            onArticleRequestSuccess();
                            String content1="";
                            String content2="";
                            String insertNewId="";
                            String insertType="";
                            String insertTitle="";
                            String insertImage="";
                            String insertContent="";
                            //Sometimes NO Section_ID and DATE: Occur on ADS Type1
                            String section_id="", pubdate="", author="", title="", photo="", type="";
                            if(response.body().has("section_id")) section_id = !response.body().get("section_id").isJsonNull() ? response.body().get("section_id").getAsString() : "";
                            if(response.body().has("pubdate")) pubdate = !response.body().get("pubdate").isJsonNull() ? response.body().get("pubdate").getAsString() : "";
                            if(response.body().has("byline")) author = !response.body().get("byline").isJsonNull() ? response.body().get("byline").getAsString() : "";
                            if(response.body().has("title")) title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
                            if(response.body().has("creative")) photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
                            if(response.body().has("type")) type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";

//                            String title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
//                            String photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
//                            String type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";

                            //Check if has an INSERT (for SuperShare)
                            if(response.body().has("insert")){
                                //Check if CONTENT as an ARRAY
                                //response.body().get("content").getAsJsonArray().get(0).getAsString();
//                                content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
//                                content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
//                                insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
//                                insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
//                                insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
//                                insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
//                                insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
//                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
                                if(response.body().has("content")){
                                    content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
                                    content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
                                }
                                if(response.body().get("insert").getAsJsonObject().has("id")) insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("type"))  insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("title")) insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("creative")) insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("content"))  insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
                            }else {
                                //Check if CONTENT as a STRING
                                if(response.body().has("content")) content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
//                                content1 = response.body().get("content").getAsString();
                            }
                            //Get the Related Article: First Check if has RelatedArticle
                            Global.relatedArticles.clear();
                            relatedArticles.clear();
                            if(response.body().has("related")){
                                Log.d(TAG, "onResponse: CONTENT_  test1");
                                for(int x=0; x<response.body().get("related").getAsJsonArray().size();x++){
                                    relatedArticles.add(response.body().get("related").getAsJsonArray().get(x).getAsJsonObject());
//                                    Log.d(TAG, "onResponse: CONTENT_ Related ID "+response.body().get("related").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsString());
//                                    Log.d(TAG, "onResponse: CONTENT_ Related IAMGE "+response.body().get("related").getAsJsonArray().get(x).getAsJsonObject().get("image").getAsString());
//                                    Log.d(TAG, "onResponse: CONTENT_ Related TITLE "+response.body().get("related").getAsJsonArray().get(x).getAsJsonObject().get("title").getAsString());
//                                    Log.d(TAG, "onResponse: CONTENT_ Related DATE "+response.body().get("related").getAsJsonArray().get(x).getAsJsonObject().get("pubdate").getAsString());
                                }
                                //Add to Global
                                Global.relatedArticles.addAll(relatedArticles);
                            }

                            Log.d(TAG, "onSuccess: CONTENT_ section_id: "+ section_id);
                            Log.d(TAG, "onSuccess: CONTENT_ title: "+ title);
                            Log.d(TAG, "onSuccess: CONTENT_ photo: "+ photo);
                            Log.d(TAG, "onSuccess: CONTENT_ type: "+ type);
                            Log.d(TAG, "onSuccess: CONTENT_ author: "+ author); //author
                            Log.d(TAG, "onSuccess: CONTENT_ content1: "+ content1);
                            Log.d(TAG, "onSuccess: CONTENT_ content2: "+ content2);
                            Log.d(TAG, "onSuccess: CONTENT_ insertNewId: "+ insertNewId);
                            Log.d(TAG, "onSuccess: CONTENT_ insertTitle: "+ insertTitle);
                            Log.d(TAG, "onSuccess: CONTENT_ insertType: "+ insertType);
                            Log.d(TAG, "onSuccess: CONTENT_ insertImage: "+ insertImage);
                            Log.d(TAG, "onSuccess: CONTENT_ insertContent: "+ insertContent);

//                            //DEGUGGING
//                           Gson gson = new Gson(); //NOTE: JSONArray()
//                            String ff2 = gson.toJson(Global.relatedArticles);
//                            Log.d(TAG, "onSuccess: CONTENT_ relatedArticles s: "+ ff2);

                            //INTENT
                            Intent intent = new Intent();
                            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
                            intent.putExtra("sectionId", section_id);
                            intent.putExtra("id", id);
                            intent.putExtra("title", title);
                            intent.putExtra("photo", photo);
                            intent.putExtra("type", type);
                            intent.putExtra("pubdate", pubdate);
                            intent.putExtra("content1", content1);
                            intent.putExtra("image1", insertImage);
                            intent.putExtra("insertTitle", insertTitle);
                            intent.putExtra("insertType", insertType);
                            intent.putExtra("insertNewsId", insertNewId);
                            intent.putExtra("insertContent", insertContent);
                            intent.putExtra("content2", content2);
                            intent.putExtra("author", author); //author
                            intent.putExtra("custom", ""); //No USED
                            intent.putExtra("adbanner", bannerSectionId);
                            startActivityForResult(intent, Global.NEWS_ARTICLE);

                        }catch (Exception e){
                            Log.d(TAG, "onResponse: viewArticle Exception: "+ e.getCause());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        if(!hasNewDailyKey) getDailyKey(); //Resolution: For always giving Error After longest time from the backGround. Maybe expiration of DailyKey.
                        onArticleRequestError();
                        String error_msg = Global.getResponseCode(t);
                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
                            toastMessage(TAG+": onFailure: "+ t.getMessage()); // //Display in developer
                        }else{
                            toastMessage(error_msg); // //Display in user
                        }
                        Log.d(TAG, "onError: onArticleRequestError "+ t.getMessage());
                    }
                });
    }

    public void checkAd2(String section_id) {
        removeAd(); //Remove Ad First
        bannerSectionId = section_id; //bannerSectionId = to be passed to WebNewsActivity
        Log.d(TAG, "checkAd2: getBannerAd2 section_id_CALLED: " + section_id);
        //CHECK if Global.bannerList is not empty
        if (Global.bannerList.size() > 0) {  //Global.bannerList
            for (int x = 0; x < Global.sectionsBanner2.size(); x++) {  //Iterate in Global.sectionsBanner not in Global.bannerList
                if (Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString().equals(getBannerList(section_id))) {
                    Log.d(TAG, "checkAd2: getBannerAd2 Searched_FOUND: "+ Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString()+" == "+getBannerList(section_id));
                    String image = Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("image").getAsString();
                    String link = Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("link").getAsString();
                    Log.d(TAG, "checkAd2: getBannerAd2 Searched_FOUND: IMAGE: "+image+" == LINK: "+link);

                    displayAd(image, link); //Global.sectionsBanner
                    break;

                }
            }
        }
    }

    private String getBannerList(String sectionId) {
        String section_ID = "-1";
        //Note: Already check that Global.bannerList is not empty
        //Finding section_id inside Global.bannerList
        for (int x = 0; x < Global.bannerList.size(); x++) {
            if (Global.bannerList.get(x).equals(sectionId)) {
                section_ID = Global.bannerList.get(x);
                return section_ID;
            }
        }
        return section_ID;
    }

/** //setUserVisibleHint is DEPRECATED, temp REMOVAL and Wait for testing in OLD DEVICES
//    @Override
//    public void setUserVisibleHint(boolean isFragmentVisible_) {
//        super.setUserVisibleHint(true);
//        if (this.isVisible()) {
//            if (isFragmentVisible_) {
//            //DMotionVideoSoundController
//            //podcast
////                removeStopPodcastAudio(); REMOVE TEMP
//            Log.d(TAG, "FRAGMENT_VISIBLE: TRUE");
//            isFragmentVisible = true;  //control timer on Stop and Resume
//            customHandler.postDelayed(updateTimerThread, 2000);  //start handler runnable
//            } else {
//                //DMotionVideoSoundController
//                Log.d(TAG, "FRAGMENT_VISIBLE: FALSE");
//                isFragmentVisible = false;  //control timer on Stop and Resume
//                customHandler.removeCallbacks(updateTimerThread);  //stop handler runnable
//            }
//        }
//    }
 */

    private void displayAd(String image, String link) {
        Log.d(TAG, "displayAd: getBannerAd2 IMAGE: "+image);
        bannerIvImage.setVisibility(View.VISIBLE);
        bannerAdLink = link;
        //GlideApp Usage: Need: MyGlide and UnsafeOkHttpClient JavaClass
        Glide.with(getContext())
                .load(image)
                .transition(withCrossFade())
                .centerCrop()
                .apply(requestOptions())
                .into(bannerIvImage);
        ViewCompat.setTransitionName(bannerIvImage, "BannerAd"); //setTransitionName
    }

    public void removeAd() {
        Log.d(TAG, "getBannerAd2 removeAd: CALLED");
        bannerIvImage.setVisibility(View.GONE);
    }

    public void checkVideoLink(String link) {
        if (isValidated(link)) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE); //ABLE TO CHOOSE what Browser will be used
                intent.setData(Uri.parse(link));
                startActivity(intent);
            } catch (Exception e) {
                Log.d(TAG, "Try_Exception checkVideoLink_intent_error: " + e.getMessage());
                return;
            }
        } else {
            Global.isItemClicked = false; //isItemClicked false here: Because after click it does not leave the fragmentBreaking
            return;
        }
    }

    private boolean isValidated(String string){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
        }finally {
            if(string!=null) return true;  //Check if null
            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
        }
        return false;
    }

    private RequestOptions requestOptions(){
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .priority(Priority.HIGH);
        return options;
    }

    public void checkImageLink(String link) {
        if (isValidated(link)) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(link));
                startActivity(intent);
            } catch (Exception e) {
                Log.d(TAG, "checkImageLink: Try_Exception ImageDoubleLink_error: "+ e.getMessage());
                return;
            }
        } else {
            Global.isItemClicked = false; //isItemClicked false here: Because after click it does not leave the fragmentBreaking
            return;
        }
    }

    //registerNotificationPermission
    private void registerNotificationPermission(String deviceId, String token, String deviceType, String password, String notifFlag) {
//        ApiService apiService = ServiceGenerator.createService(ApiService.class);
        apiService.registerNotificationPermission(deviceId, token, deviceType, password, notifFlag)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseBody responseBody) {
                        try {
                            String r = responseBody.string();
                            Log.d(TAG, "onSuccess: ResponseBody:" + r);
                            if (r.contains("Store Successfully")) {///
                                Log.d(TAG, "registerNotificationPermission:  NOTIFICATION checkNotificationSettings RESPONSE: Store Successfully ");
                            }else if(r.contains("Record updated 2 ")){
                                Log.d(TAG, "registerNotificationPermission: NOTIFICATION checkNotificationSettings  RESPONSE: Record updated");
                            }
                        } catch (IOException e) {
                            Log.d(TAG, "onSuccess: IOException_Error: "+e.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        sf.setNotificationAllowed(true);
                        Log.d(TAG, "onError: registerNotificationPermission error  getMessage" + e.getMessage());

                    }
                });
    }

    //For Manual Notif Settings
    private void checkNotificationSettings() {

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getContext());
        areNotificationsEnabled = notificationManagerCompat.areNotificationsEnabled();
        //Initialize Value
        if(listenNotifChange.getValue()==null){
            listenNotifChange.setValue(areNotificationsEnabled);
        }else if(listenNotifChange.getValue()!=areNotificationsEnabled){ //Update/Change Value
            sf.setNotifPermissionChangedManually(true); //Permission is changed Manually
            listenNotifChange.setValue(areNotificationsEnabled);
        }

        //Move to Method
//        if (areNotificationsEnabled) {
//            if (sf.hasNotificationAllowedAlready()) {  //if (sf.hasNotificationAllowed()) {
//                Log.d(TAG, "NOTIFICATION checkNotificationSettings: hasNotificationAllowedAlready: TRUE_CALLED***");
//
//            } else {
////                sf.setNotificationAllowed(true);
//                registerNotificationPermission(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_STORE_NOTIF_DEVICE, Global.POSITIVE_NOTIF_PERMISSION);
//            }
//
//        } else {
//            if (!sf.hasNotificationAllowedAlready()) {
//                Log.d(TAG, "NOTIFICATION checkNotificationSettings: hasNotificationAllowedAlready: FALSE_CALLED***");
//            } else {  //else if (sf.getNotificationFlag().equals("Y")) {
////                Log.d(TAG, "checkNotificationSettings: registerNotificationPermission CALLED N***");
////                sf.setNotificationFlag("N");
////                sf.setNotifStatusChange(true);
////                sf.setNotificationAllowed(false);
//                registerNotificationPermission(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_STORE_NOTIF_DEVICE, Global.NEGATIVE_NOTIF_PERMISSION);
//            }
//        }
    }

    /**
     * Records of seconds stay in article view
     **/
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            //checking if it is running
            try {
                timeCounter++;
                Log.d(TAG, "run: counting.. " + timeCounter);
                customHandler.postDelayed(this, 1000);

            } catch (Exception e) {
                Log.d(TAG, "run: Try_Exception customHandler_error: "+e.getMessage());
            }
        }
    };

//    public void stopPodcast(){
//       adapter_breaking.stopPodcastWebView(); //Flow: access the Adapter, then Adapter (declared nd initialize the instance of ViewHolder) access the ViewHolder method.adapter_breaking.getPodcastWebView().saveState();  //Telling the Adapter to refresh  // adapter_breaking.notifyDataChanged();
//    }
//
//    public void resumePodcast(){
//        adapter_breaking.resumePodcastWebView(); //Flow: access the Adapter, then Adapter (declared nd initialize the instance of ViewHolder) access the ViewHolder method.adapter_breaking.getPodcastWebView().saveState();  //Telling the Adapter to refresh  // adapter_breaking.notifyDataChanged();
//    }
//

    @Override
    public void onResume() {
        hideLoader();
        //RETRIEVING the LATEST getSectionNameTempContainer
        if(sf!=null){
            Log.d(TAG, "onResume: "+sf.getSectionNameTempContainer());
            if(!sf.getSectionName().equals(sf.getSectionNameTempContainer())) sf.setSectionName(sf.getSectionNameTempContainer());
        }

        //for mobility.. display the content of the last section..
        // prevent to display the RecyclerView if the last section is Mobility
        if (sf.getLastSection() >= 200) { //if (sf.getSectionMenuID() >= 200) {
//            Log.d(TAG, "getMenuAndNews: getSectƒionMenuID " + sf.getSectionMenuID());
            Log.d(TAG, "getMenuAndNews: LAST_SECTION: " + sf.getLastSection());
            rvBreaking.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            webView.loadUrl(sf.getSectionUrl());  //"https://www.google.com"

        } else {
            rvBreaking.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
            //New Additional NewDesign: For Bookmark Updating
            if(adapter_breaking!=null) adapter_breaking.notifyDataChanged();
            Log.d(TAG, "onResume: BOOKMARKING");
        }

        //After Asking Permission Dialog
        Log.d(TAG, "onResume: NOTIFICATION checkNotificationSettings  DialogHasDisplayed? hasAskNotificationPermission: "+ sf.hasAskNotificationPermission());
        if (sf.hasAskNotificationPermission()) {
            sf.setAskNotificationPermission(false);
            registerNotificationPermission(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_STORE_NOTIF_DEVICE, sf.getNotificationFlag());
        }

        //for timer .. get the last section, then count time
        if (!isLoadedAsRegisteredFirstTime) {  //will not be called in the first time of registration only..
            Log.d(TAG, "onResume: isLoadedAsRegisteredFirstTime FALSE");
            if (!isFragmentVisible) {
                Log.d(TAG, "onResume: isFragmentVisible FALSE");
                customHandler.removeCallbacks(updateTimerThread);  //stop.. handler runnable
            } else {
                Log.d(TAG, "onResume: isFragmentVisible TRUE");
                customHandler.postDelayed(updateTimerThread, 2000);  //start handler runnable
            }

        } else {
            Log.d(TAG, "onResume: isLoadedAsRegistered TRUE");
            customHandler.removeCallbacks(updateTimerThread);  //stop.. handler runnable
            isLoadedAsRegisteredFirstTime = false;
        }
        Log.d(TAG, "onResume: LAST_SECTION: " + sf.getLastSection());
        Log.d(TAG, "onResume: section default is getLastSectionPosition " + sf.getLastSectionPosition());

        super.onResume();
    }

    @Override
    public void onPause() {
        //SAVE SectinName in the TEMP_CONTAINER to retrieve after returning here FragmentBreaking
//        sf.setSectionNameTempContainer(sf.getSectionName());

        Log.d(TAG, "onPause: "+sf.getSectionNameTempContainer());
        customHandler.removeCallbacks(updateTimerThread);  //stop handler runnable
//        Log.d(TAG, "onPause: stop counting.." + timeCounter );
        sf.setListViewReadsCount(timeCounter);
//        sf.setStayInArticle(timeCounter);
        Log.d(TAG, "onPauseBreaking: called ..Timer is " + sf.getListViewReadsCount());
        super.onPause();
    }

    @Override
    public void onStart() {
        getMenuAndNews();
        checkNotificationSettings();
        super.onStart();
    }

    @Override
    public void onDestroyView() {
//        sf.setPlayClicked(false); //SET PlayButton icon Status to PLAY
//        sf.setPodcastPlaying(false); //RESET Default
//        //podcast Audio
//        if( mMediaControllerCompat.getPlaybackState().getState() == PlaybackStateCompat.STATE_PLAYING ) {
//            mMediaControllerCompat.getTransportControls().pause();
//        }
//        mMediaBrowserCompat.disconnect();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: LifeCycle");

        //send timeCounter, then reset timeCounter
        if (isFragmentVisible) {
            Log.d(TAG, "onStop: Recorded COUNTER is  " + sf.getListViewReadsCount() + " LAST_SECTION: " + sf.getLastSection());
            if (sf.getListViewReadsCount() >= 1) {
                String sectionId = valueOf(sf.getLastSection());
                registerTimeListViewReads(sf.getDeviceId(), sectionId, sf.getListViewReadsCount(), Global.passStoreUserReadTimeList);
                timeCounter = 0;
            }
        }
        super.onStop();
    }




    //mobility
    private void configWebViewSettings() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    //mobility
    private void loadHtml() {
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                onPreloadingSection(); //progressBar
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                onSuccessLoadingSection();  //progressBar
            }
        });
    }

    /**New Additional NewDesign*/
    public boolean saveBookmark(String newsId){
        return  bookmarkArticle(newsId);
    }
    public void addToBookmark(int newsId, String title, String mainImage, String type, String date, String content1, String image1, String content2, String author, String custom, String bannerSectionId,String sectionName) {
        sf.setLastBookmarkCount(sf.getLastBookmarkCount()+1);
        Log.d(TAG, "addToBookmark:getLastBookmarkCount "+sf.getLastBookmarkCount());
        //insert/save to database
        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, sf.getLastBookmarkCount(),title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectionName);
        helper.saveBookmark(bookmarkModel);
        toastMessage("This article is added to your bookmark.");
    }

    private boolean bookmarkArticle(String id) {
//        onArticleRequest();
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
        apiService.getArticle2(String.valueOf(unixTime),sf.getDeviceId(), id, hashed)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try{
                            hasNewDailyKey = false;
                            Global.isItemClicked = true;
                            onArticleRequestSuccess();
                            String content1="";
                            String content2="";
                            String insertNewId="";
                            String insertType="";
                            String insertTitle="";
                            String insertImage="";
                            String insertContent="";
                            String custom="";

                            //Sometimes NO Section_ID and DATE: Occur on ADS Type1
                            String section_id="", pubdate="", author="", title="", photo="", type="";
                            if(response.body().has("section_id")) section_id = !response.body().get("section_id").isJsonNull() ? response.body().get("section_id").getAsString() : "";
                            if(response.body().has("pubdate")) pubdate = !response.body().get("pubdate").isJsonNull() ? response.body().get("pubdate").getAsString() : "";
                            if(response.body().has("byline")) author = !response.body().get("byline").isJsonNull() ? response.body().get("byline").getAsString() : "";
                            if(response.body().has("title")) title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
                            if(response.body().has("creative")) photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
                            if(response.body().has("type")) type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";

                            //Check if has an INSERT (for SuperShare)
                            if(response.body().has("insert")){
                                if(response.body().has("content")){
                                    content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
                                    content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
                                }
                                if(response.body().get("insert").getAsJsonObject().has("id")) insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("type"))  insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("title")) insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("creative")) insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("content"))  insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
                            }else {
                                //Check if CONTENT as a STRING
                                if(response.body().has("content")) content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
//                                content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
                            }

                        //NOTE:BOOKMARKS has NO RELATED ARTICLE
                        if(!title.isEmpty()){
                            addToBookmark(Integer.valueOf(id),title,photo,type,pubdate,content1,insertImage,content2,author,custom,bannerSectionId,sf.getSectionName());
                            bookmarkResult = true;
                        }

                        }catch (Exception e){
                            Log.d(TAG, "onResponse: viewArticle Exception: "+ e.getCause());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        if(!hasNewDailyKey) getDailyKey(); //Resolution: For always giving Error After longest time from the backGround. Maybe expiration of DailyKey.

                        bookmarkResult = false;
                        Global.isBookmark = false;
                        String error_msg = Global.getResponseCode(t);

                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
                            toastMessage(TAG+": onFailure: "+ t.getMessage()); // //Display in developer
                        }else{
                            toastMessage(error_msg); // //Display in user
                        }
                        Log.d(TAG, "onError: onArticleRequestError "+ t.getMessage());
                    }
                });

        return bookmarkResult;
    }

    private void getDailyKey(){
        Log.d(TAG, "getDailyKey: RUN");
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                    hasNewDailyKey = true;
                    Log.d(TAG, "onResponse: getDailyKey: "+sf.getDailyKey());
                }else{
                    Log.d(TAG, "errorBody: getDailyKey: "+response.errorBody().toString());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: getDailyKey: "+t.getMessage());
            }
        });
    }

    //FIREBASE
    private void getFirebaseKey(){
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            registerFirebaseToken(); //registerFirebaseToken
        }, 3000);
    }

    //FIREBASE: getFirebaseId
    private String getFirebaseId(){
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), instanceIdResult ->
                newToken = instanceIdResult.getToken());
        Log.d(TAG, "getFirebaseId: "+ newToken);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****getFirebaseId_GOT_NEW_TOKEN****: "+ newToken);
        return newToken;
    }

    //FIREBASE: subscribeToTopic
    private void subscribeToTopic(){  // my_inquirer_news   // my_inquirer_news_test
        FirebaseMessaging.getInstance().subscribeToTopic("myinqnews").addOnSuccessListener(getActivity(), new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "onSuccess: subscribeToTopic called");
            }
        });
    }
    //FIREBASE: return Observable
    private Observable<String> observeToken(){
        return Observable.just(getFirebaseId())  //Returns the Observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //FIREBASE: registerFirebaseToken: SUBSCRIBE on Observable
    private void registerFirebaseToken(){
        observeToken().subscribe(new io.reactivex.Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(String token) {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
                subscribeToTopic();  //When Observable Complete: Subscribe to TOPIC
                sf.setFirebaseToken(newToken); //setting firebase token
                Log.d(TAG, "onComplete: FIREBASE_TOKEN "+ newToken);
            }
        });
    }

    public void toastMessage(String message) {
        if(Global.toast!=null) Global.toast.cancel(); //Remove First the existing toast
        Global.toast = Toast.makeText(getContext().getApplicationContext(), message, Toast.LENGTH_SHORT); Global.toast.show(); //Show NEW TOAST
    }

    //Reserved
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible){
            isFragBreakingVisible = true;
            customHandler.postDelayed(updateTimerThread, 2000);  //start handler runnable
            Log.d(TAG, "FRAGMENT_VISIBLE: BREAKING_ IN LifeCycle");
        }else{
            isFragBreakingVisible = false;
            customHandler.removeCallbacks(updateTimerThread);  //stop handler runnable
            Log.d(TAG, "FRAGMENT_VISIBLE: BREAKING_ OUT LifeCycle");
        }
    }

    private void viewArticleNew(String newsId) {
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + newsId);
        apiService.viewArticle(String.valueOf(unixTime), sf.getDeviceId(),newsId, hashed)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {compositeDisposable.add(d);}

                    @Override
                    public void onSuccess(ResponseBody responseBody) {
                        try {
                            Global.articleView = responseBody.string();
                            if(Global.articleView.contains("<!DOCTYPE html>")){
                                Intent intent = new Intent(getContext(), WebNewsActivity.class);
                                intent.putExtra("viewArticleNow",Global.articleView );
                                intent.putExtra("newsId",newsId );
                                intent.putExtra("newsType",newsType );
                                intent.putExtra("newsPhoto",newsPhoto );
                                intent.putExtra("newsTitle",newsTitle );
                                intent.putExtra("newsDate",newsDate );

                                startActivityForResult(intent,Global.NEWS_ARTICLE);
                                Log.d(TAG, "onSuccess: DATA1 viewArticleNew "+  Global.articleView);
                            }else{
                                Log.d(TAG, "onSuccess: viewArticleNew Response Error");
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "onSuccess:Exception viewArticleNew  "+e.getMessage());
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: viewArticleNew Throwable " + e.getMessage());
                    }
                });
    }
}















//    public void removeStopPodcastAudio() {
//        Log.d(TAG, "removeStopPodcastAudio: CALLED");
//        EventBus.getDefault().removeStickyEvent(PostPodcastWebNewsAct.class);  //for WebNewsAct
//        EventBus.getDefault().post("removePlaybackButton");  //for FragmentBreaking Page
//
//        ( (AudioManager) getContext().getSystemService(
//                getContext().AUDIO_SERVICE) ).requestAudioFocus(
//                null,
//                AudioManager.STREAM_MUSIC,
//                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
//    }



//    private boolean bookmarkArticle(String id) {
//        long unixTime = System.currentTimeMillis() / 1000L;
//        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megaarticle/" + id);
//        apiService.getArticle(String.valueOf(unixTime),sf.getDeviceId(), id, hashed)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<Article>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        compositeDisposable.add(d);
//                    }
//                    @Override
//                    public void onSuccess(Article article) {
//                        String title = article.getTitle() != null ? article.getTitle() : "";
//                        String photo = article.getCreative() != null ? article.getCreative() : "";
//                        String type = article.getType() != null ? article.getType() : "";
//                        String pubdate = article.getPubdate() != null ? article.getPubdate() : "";
////                        String content1 = article.getContent() != null ? article.getContent().get(0) : "";
////                        String content1 = article.getContent();
//
//                        //contents with deserialize
//                        String content1 = article.getContent1().isEmpty() ? article.getContent2a() :  article.getContent1();
//                        String content2 = article.getContent2b();
//
////                        String content1 = article.getContent() != null ? article.getContent().get(0).toString() : "";
////                        String image1 = article.getInsert() != null ? article.getInsert().getCreative(): "";
//                        String image1 = "";
//
////                        String content2 = article.getContent() != null ? article.getContent().get(1).toString() : "";
////                        String content2 = article.getContent();
//
//                        String author = article.getByline() != null ? article.getByline() : "";
//                        String custom = article.getCustom() != null ? article.getCustom() : "";
//
//                        Log.d(TAG, "onSuccess: CONTENT_ 1: "+ content1);
//                        Log.d(TAG, "onSuccess: CONTENT_ 2: "+ content2);
//                        //Get the Related Article
//
//
//                        //Get the SuperShare Details
//
//                        if(!title.isEmpty()){
//                            addToBookmark(Integer.valueOf(id),title,photo,type,pubdate,content1,image1,content2,author,custom,bannerSectionId,sf.getSectionName());
//                            bookmarkResult = true;
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        bookmarkResult = false;
//                        Global.isBookmark = false;
//                        String error_msg = Global.getResponseCode(e);
//                        Toast.makeText(getContext().getApplicationContext(), error_msg, Toast.LENGTH_SHORT).show();                    }
//                });
//        return bookmarkResult;
//    }


