package com.knx.inquirer.fragments;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.knx.inquirer.R;

//fragment inside fragment
public class FragmentMyInqHolder extends Fragment {
    private static final String TAG = "FragmentMyInqHolder";

    public FragmentMyInqHolder() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myinq_holder, container, false);
        return view;
    }
}
