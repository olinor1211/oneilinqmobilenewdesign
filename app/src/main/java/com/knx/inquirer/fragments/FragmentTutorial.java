package com.knx.inquirer.fragments;

import android.app.Dialog;
import android.os.Bundle;

import android.util.EventLog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.knx.inquirer.R;
import com.knx.inquirer.models.PostUIUpdate;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.SharedPrefs;

import org.greenrobot.eventbus.EventBus;

import me.relex.circleindicator.CircleIndicator;

public class FragmentTutorial extends DialogFragment {
    private static final String TAG = "FragmentTutorial";

    private ViewPager viewPager = null;
    private CircleIndicator indicator;
    private int img[] = { R.drawable.newtutorial1, R.drawable.newtutorial2,R.drawable.newtutorial3,R.drawable.newtutorial4};
    private TextView textSkip;
    private SharedPrefs sf;
    private Dialog dialog;

    //Singleton,
    public static FragmentTutorial getInstance() {
        return new FragmentTutorial();
    }

    public FragmentTutorial() {
        //empty
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme); //set theme style
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FragmentTutorial_FRAGMENT_CALLED****");

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_tutorial, container, false);
        initViews(view);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FragmentTutorial_LAYOUT_CALLED****");

        return view;
    }

    private void initViews(View view) {

        textSkip = view.findViewById(R.id.textSkip);

        textSkip.setOnClickListener(v -> {
            try{
                EventBus.getDefault().post("notificationPermitted");
            }catch(Exception e){
                Log.d(TAG, "initViews: ERROR: registerNotificationPermission "+ e.getMessage());
            }
            sf.setTutViewed(true);
            dismiss();
        });

        //tutorial adapter
        viewPager = view.findViewById(R.id.viewPager);
        indicator = view.findViewById(R.id.imageIndicator);

        viewPager.setAdapter(new ViewPagerAdapter());  //tutorial
        indicator.setViewPager(viewPager);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            public void onPageSelected(int position) {

            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sf = new SharedPrefs(getContext());
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                sf.setTutViewed(true);
                dismiss();
            }
        };
    }

    public class ViewPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
               return img.length;
        }

        @Override
        public Object instantiateItem(View container, int position) {

               ImageView iv = new ImageView(getContext());
               iv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
               iv.setImageResource(img[position]);
               iv.setScaleType(ScaleType.FIT_XY);
               ((ViewPager) container).addView(iv, 0);
          
               return iv;
        }

        @Override
        public void destroyItem(View container, int position, Object obj) {
               ((ViewPager) container).removeView((View) obj);
        }

        @Override
        public boolean isViewFromObject(View container, Object obj) {
               return container == obj;
               // return container == (View) obj;
        }
   }
}   
		

