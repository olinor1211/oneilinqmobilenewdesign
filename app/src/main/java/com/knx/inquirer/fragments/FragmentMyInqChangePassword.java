package com.knx.inquirer.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.JsonObject;
import com.knx.inquirer.R;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyInqChangePassword extends Fragment {
    private final String TAG="FragChangePassword";

    private Button mContinue;
    private EditText mPassword;

    private String password;

    private SharedPrefs sf;
    private ApiService apiService;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Global.LoaderDialog loaderDialog;

    public FragmentMyInqChangePassword() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_my_inq_change_password, container, false);
        sf =  new SharedPrefs(getContext());
        apiService = ServiceGenerator.createService(ApiService.class);
        loaderDialog = new Global.LoaderDialog();

        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);
        initViews(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    private void initViews(View view) {
        mContinue = view.findViewById(R.id.btnContinueChangePass);
        mPassword = view.findViewById(R.id.etPasswordChangePass);

        mContinue.setOnClickListener( view1 -> changePassword());

        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                password = mPassword.getText().toString().trim();
                verify();
            }
        });
    }

    private void verify() {
        if(password.isEmpty() || password.length() <=5) buttonDisable();
        else buttonEnable();
    }

    private void buttonDisable() {
        mContinue.setEnabled(false);
        mContinue.setBackgroundResource(R.drawable.round_button_gray);
    }

    private void buttonEnable() {
        mContinue.setEnabled(true);
        mContinue.setBackgroundResource(R.drawable.round_button_blue);
    }


    private void changePassword(){
        buttonDisable();
        showLoader();
        apiService = ServiceGenerator.createService(ApiService.class);
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megacpass");
        apiService.changePassword(String.valueOf(unixTime), sf.getDeviceId(), sf.getDeviceKey().toString(), password, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonObject>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onSuccess(JsonObject jsonObject) {
                        hideLoader();
                       // SAMPLE:{"status":"error","value":["<p>The Password field must be at least 6 characters in length.<\/p>"]}
                        // SAMPLE: {"status":"ok","value":["ok"]}
                        try{
                            if(jsonObject.get("status").getAsString().equals("ok")){
                                    mPassword.setText("");
                                    mPassword.clearFocus();
                                    Toast.makeText(getContext(), "Password changed successfully!", Toast.LENGTH_SHORT).show();
                                    delayAndContinue();
                            }
                            else if(jsonObject.get("status").getAsString().equals("error")){
                                buttonEnable();
                                Toast.makeText(getContext(), "Error: "+jsonObject.get("value").getAsJsonArray().get(0), Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Log.d(TAG, "onSuccess: Error JSon Structure: "+jsonObject.toString());
                            }
                        }catch (Exception e){
                            buttonEnable();
                            Log.d(TAG, "onSuccess: Exception Error JSon Structure: "+ e.getMessage());
                            Log.d(TAG, "onSuccess: Exception Error JSon Structure: "+jsonObject.toString());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey();
                        hideLoader();
                        buttonEnable();
                        Log.d(TAG, "onError: Throwable Exception_Error: "+e.getMessage());
                    }
                });
    }

    private void delayAndContinue() {
        Handler handler = new Handler();
        handler.postDelayed((Runnable) () -> {
            EventBus.getDefault().post("changePassSuccess");
        },2500);
    }

    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    public void showLoader() {loaderDialog.showDialog(getContext());}

    public void hideLoader() {
        loaderDialog.dismissDialog(getContext());
    }

}