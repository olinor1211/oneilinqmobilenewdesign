package com.knx.inquirer.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.knx.inquirer.R;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class FragmentMyInqLoginRegister extends Fragment {

    private SharedPrefs sf;
    private Button mLogin, mRegister;

    public FragmentMyInqLoginRegister() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_my_inq_login_register, container, false);
        sf =  new SharedPrefs(getContext());
//        apiService = ServiceGenerator.createService(ApiService.class);

        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);
        initViews(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    private void initViews(View view) {
        mLogin = view.findViewById(R.id.btnLogin);
        mRegister = view.findViewById(R.id.btnRegister);

        mLogin.setOnClickListener( view1 -> {
            getLogin();
        });
        mRegister.setOnClickListener(view1 ->{
            getRegister();
        });

    }

    private void getRegister() {EventBus.getDefault().post("myInqRegister");}

    private void getLogin() {
       EventBus.getDefault().post("myInqLogin");
    }
}