package com.knx.inquirer.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.knx.inquirer.R;
import com.knx.inquirer.models.MyOption;
import com.knx.inquirer.models.TagsModel;
import com.knx.inquirer.rvadapters.AdapterPreferedSections;
import com.knx.inquirer.rvadapters.AdapterPreferedTags;
import com.knx.inquirer.rvadapters.RVAdapterBreaking;
import com.knx.inquirer.rvadapters.RVAdapterEmpty_Tags_MyInq;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryMenu;
import com.knx.inquirer.viewmodels.MenuViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyInqRegistrationP2 extends Fragment {
    private final String TAG = "FragmentMyInqRegP2";

    private Button mSubmit;
    private RecyclerView rvPrefSection;
    private CheckBox cbPolicy;
    private TextView mTerms, mPrivacy;
    private ProgressBar mLoadingSections;

    private List<TagsModel> tagsList;
    private JSONArray selectedSectionsList;
    private AdapterPreferedSections adapter;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Global.LoaderDialog loaderDialog;
    private ApiService apiService;


    public FragmentMyInqRegistrationP2() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_my_inq_registration_p2, container, false);
        sf =  new SharedPrefs(getContext());
        loaderDialog = new Global.LoaderDialog();
        apiService = ServiceGenerator.createService(ApiService.class);

        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);
        initViews(view);
        fetchSections();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    private void initViews(View view) {

        mSubmit = view.findViewById(R.id.btnSubmitReg);
        rvPrefSection = view.findViewById(R.id.rvPrefSectionReg);
        cbPolicy = view.findViewById(R.id.cbPolicyReg);
        mTerms = view.findViewById(R.id.tvTermsReg);
        mPrivacy = view.findViewById(R.id.tvPrivacyReg);
        mSubmit = view.findViewById(R.id.btnSubmitReg);
        mLoadingSections = view.findViewById(R.id.pbPrefSectionLoading);

        mSubmit.setOnClickListener(view1 -> getSubmitted());
        mTerms.setOnClickListener(v-> gotoSite("http://inq.news/MMTandC"));
        mPrivacy.setOnClickListener(v->gotoSite("http://inq.news/privacy"));

        cbPolicy.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                mSubmit.setEnabled(true);
                mSubmit.setBackgroundResource(R.drawable.round_button_blue);
            }else{
                mSubmit.setEnabled(false);
                mSubmit.setBackgroundResource(R.drawable.round_button_gray);
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),3,RecyclerView.VERTICAL,true);
        rvPrefSection.setHasFixedSize(true);
        rvPrefSection.setLayoutManager(gridLayoutManager);//
    }

    private void getSubmitted() {
        if(Global.isNetworkAvailable(getContext())){
            //Saving Tags Menu in the List selections
            List<Integer> selections = new ArrayList<>(); //new: Integer List
            selectedSectionsList = new JSONArray(); //Clearing JsonArray
            if(tagsList.size() > 0){
                for (TagsModel model : tagsList) {
                    if (model.isSelected()) { //Filter only the Selected Tags
                        selections.add(model.getId());  //Add Tags ID to the List string //selections.add(String.valueOf(model.getId()));
                        try {
                            JSONObject jsonObject = new JSONObject(); //Creating JsonObject structure
                            jsonObject.put("id", model.getId());
                            jsonObject.put("title", model.getTitle());
                            selectedSectionsList.put(jsonObject); //then put the object to JsonArray
                        } catch (Exception e) {
                            Log.d(TAG, "getSubmitted Exception_Error: "+e.getMessage());
                        }
                    }
                }
            }

            Log.d(TAG, "selectedTagSections: List:" +selectedSectionsList.toString());

            if(selectedSectionsList.length() <= 0 ){
                Toast.makeText(getContext(), "Please select your preferred sections", Toast.LENGTH_SHORT).show();
            }else {
                String selectedTagSections = new Gson().toJson(selections);  //Convert List Selections to String comma-delimited //String selectedTagSections = new Gson().toJson(selections);
                Log.d(TAG, "selectedTagSections STRING: "+selectedTagSections);
                    registerUser(Global.regPass, Global.regEmail, Global.regName, Global.regGender,  Global.regBdate, Global.regMobile, sf.getFirebaseToken(), selectedTagSections);  // registerUser(name, gender, bdate, opts);
            }

        }else{
            Global.ViewDialog alert = new Global.ViewDialog();
            alert.showDialog(getContext(), "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
        }
    }

    public void gotoSite(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void fetchSections(){
        mLoadingSections.setVisibility(View.VISIBLE);
        MenuViewModel menuViewModel = ViewModelProviders.of(this, new ViewModelFactoryMenu(getActivity().getApplication(), compositeDisposable, sf)).get(MenuViewModel.class);
        menuViewModel.getMenu().observe(this, response-> {
            try{
                tagsList = new ArrayList<>(); //Clear first
                for(int x=0; x< response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().size(); x++){
                    tagsList.add(new TagsModel(
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt(),
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("title").getAsString(),
                            false, response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("hidden").getAsString()));
                }
                mLoadingSections.setVisibility(View.INVISIBLE);
            }catch(Exception e){
                Log.d(TAG, "fetchTags: ERROR: "+e.getMessage());
            }

            adapter = new AdapterPreferedSections(getContext(), tagsList);
            rvPrefSection.setAdapter(adapter);
        });

        menuViewModel.getMenuError().observe(this, isError -> {
            if(isError){
                mLoadingSections.setVisibility(View.VISIBLE);
            }
        });
    }

    private void registerUser(String password, String email, String name, String gender, String bdate, String mobile, String notify, String selectedTagSections){
        String genderCode = changeGenderCode(gender);
//        Log.d(TAG, "REGISTERING: "+password+"*" +notify+"*"+name+" * "+gender+" * "+email+" * "+bdate+" * "+mobile+" * "+selectedTagSections);
        showLoader();
//        submit.setText("REGISTERING...");
        mSubmit.setEnabled(false);
        apiService = ServiceGenerator.createService(ApiService.class);
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megaregister");
        apiService.registerUser3(String.valueOf(unixTime), sf.getDeviceId(), password, email, name, genderCode, bdate, mobile, selectedTagSections,sf.getFirebaseToken(), hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonObject>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(JsonObject jsonObject) {
                       //SampleResponse: {"status":"ok","value":{"devicekey":624166}} ////{"status":"error","value":["Invalid JSON value"]}
                        hideLoader();
                        try {
                            if(jsonObject.get("status").getAsString().equals("ok")){
//                            Log.d(TAG, "onSuccess: "+jsonObject.get("value").getAsJsonObject().get("devicekey").getAsInt());
                                sf.setDeviceKey(jsonObject.get("value").getAsJsonObject().get("devicekey").getAsInt()); //UNIQUE_ID
                                sf.setAsRegistered(true);
                                sf.setName(name);
                                sf.setEmail(email);
                                sf.setBdate(bdate);
                                sf.setMobile(mobile);
                                sf.setGender(gender);
                                sf.setCustomMenu(selectedSectionsList.toString());
                                EventBus.getDefault().post("myInqSuccess"); //GO to directly to Fragment FragmentMyInqSuccess
                            }else{
                                mSubmit.setEnabled(true);
                                Toast.makeText(getContext(), "Error: "+jsonObject.get("value").getAsJsonArray().get(0), Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            Log.d(TAG, "onSuccess: Error JSon Structure: "+jsonObject.toString());
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey();
                        hideLoader();
//                        submit.setText("SUBMIT");
                        mSubmit.setEnabled(true);
                        Log.d(TAG, "onError: Throwable Exception_Error: "+e.getMessage());
                    }
                });

    }

    public void showLoader() {loaderDialog.showDialog(getContext());}

    public void hideLoader() {
        loaderDialog.dismissDialog(getContext());
    }

    private String changeGenderCode(String gender){
        switch(gender){
            case "Male":
                return "male";
            case "Female":
                return "female";
            case "LGBTQ":
                return "lgbtq";
            default:
                return "nan";
        }
    }
    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }
}