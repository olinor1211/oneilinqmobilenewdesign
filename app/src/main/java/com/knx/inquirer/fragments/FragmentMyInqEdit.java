package com.knx.inquirer.fragments;


import android.Manifest;
import android.app.DatePickerDialog;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.R;
import com.knx.inquirer.models.TagsModel;
import com.knx.inquirer.rvadapters.AdapterPreferedTags;
import com.knx.inquirer.rvadapters.RVAdapterEmpty_Tags_Edit;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryMenu;
import com.knx.inquirer.viewmodels.MenuViewModel;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyInqEdit extends Fragment {

//    private ProgressBar pLoading;
    private List<TagsModel> tagsList;
    private Button update;
    private TextView cancel;
    private ImageView dropdown_edit;
    private EditText name_edit, email_edit, bdate_edit, mobile_edit;
    private Calendar myCalendar;
    private Spinner spinner;
    private RecyclerView recyclerviewTags;
    private AdapterPreferedTags adapter;
    private RVAdapterEmpty_Tags_Edit adapter_empty_tags;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Global.LoaderDialog loaderDialog;
    private ApiService apiService;

    private SharedPrefs sf;

    private JSONArray jArrayCustomMenu;

    private boolean hasLoadedOnce = false; // your boolean field

    DatePickerDialog.OnDateSetListener date;

    private ShimmerFrameLayout mShimmerViewContainer;

    private String regOpts;

    private static final String TAG = "FragmentMyInqEdit";

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    public FragmentMyInqEdit() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(FragmentMyInqEdit.this)) EventBus.getDefault().register(FragmentMyInqEdit.this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sf =  new SharedPrefs(getContext());
        apiService = ServiceGenerator.createService(ApiService.class);

        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);

        View view = inflater.inflate(R.layout.fragment_my_inq_edit, container, false);
        initViews(view);
        //fetchTags() will be called once here: Because onCreateView is called once only
        //NOTE: sf.setCustomMenu is always Updated in UpdateButton SUCCESS:
        //Note: also tagsList ARRAY inside fetchTags() is always UPDATED because it is INSIDE VIEWMODEL Process
//        fetchTags(); Move to onResume
        return view;
    }

    public void initViews(View view) {

        sf = new SharedPrefs(getContext());
        loaderDialog = new Global.LoaderDialog();
//        pLoading = view.findViewById(R.id.pLoading);

        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.setAlignment(Alignment.CENTER);

        recyclerviewTags = view.findViewById(R.id.recyclersections);
        recyclerviewTags.setLayoutManager(flowLayoutManager);

        name_edit = view.findViewById(R.id.txtEditName);
        spinner = view.findViewById(R.id.spEditGender);
        email_edit = view.findViewById(R.id.tvEditEmail);
        bdate_edit = view.findViewById(R.id.txtEditBdate);
        mobile_edit = view.findViewById(R.id.tvEditMobile);

        update = view.findViewById(R.id.btnUpdatePref);
//        cancel = view.findViewById(R.id.btnCancelUpdate); Remove temp

        mShimmerViewContainer = view.findViewById(R.id.shimmer_tags_edit);

        update.setOnClickListener(v -> {

                    if(Global.isNetworkAvailable(getContext())){

                        List<String> selections = new ArrayList<>();
                        jArrayCustomMenu = new JSONArray(); //Clear
                        //Check first if NOT EMPTY: Resolved Crashed
                        if(tagsList.size()>0){
                            for (TagsModel model : tagsList) {
                                if (model.isSelected()) {
                                    selections.add(String.valueOf(model.getId()));  //Adding to List string
                                    try {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("id", model.getId());
                                        jsonObject.put("title", model.getTitle());
                                        jArrayCustomMenu.put(jsonObject);  //Adding to JsonArray
                                    } catch (Exception e) {
                                        Log.d(TAG, "initViews: Exception_Error: "+e.getMessage());
                                    }
                                }
                            }
                        }
                        if(jArrayCustomMenu.length() < 1){
                            Toast.makeText(getContext(), "Please select at least one news section.", Toast.LENGTH_SHORT).show();
                        }else {
                            try{
                                regOpts = new Gson().toJson(selections);  //Use Gson: ArrayList to String
                                Log.d(TAG, "initViews: regOpts: "+ regOpts);
                                updateUser(regOpts);
                            }catch(Exception e){
                                Log.d(TAG, "initViews: Exception_Error: "+e.getMessage());
                            }
                        }
                    }else{
                        Global.ViewDialog alert = new Global.ViewDialog();
                        alert.showDialog(getContext(), "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                    }

        });

        /**Spinner START */
        ArrayList genderArrayList = new ArrayList();
        genderArrayList.add("Male");
        genderArrayList.add("Female");
        genderArrayList.add("LGBTQ");
        genderArrayList.add("Don't want to say");
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_textview, genderArrayList){
            //View getView() TextView of ArrayList DATA
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent); //getHintTextColors();
                final ColorStateList hint_color = name_edit.getHintTextColors(); //GETTING THE HINT_COLOR or EDITVIEW
                ((TextView) v).setTextColor(hint_color);
                return v;
            }

            //View getDropDownView() TextView of DROP_DOWN_LIST
//            @Override
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View v = null;
//                // If this is the initial dummy entry, make it hidden
//                if (position == 0) {
//                    TextView tv = new TextView(getContext());
//                    tv.setHeight(0);
//                    tv.setVisibility(View.GONE);
//                    v = tv; //v = tv;
//                }else{
//                    // Pass convertView as null to prevent reuse of special case views
//                    v = super.getDropDownView(position, null, parent);
//                }
//                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
//                parent.setVerticalScrollBarEnabled(false);
//                return v;
//            }
        };

        genderAdapter.setDropDownViewResource(R.layout.spinner_textview_dropdown);
        spinner.setAdapter(genderAdapter);
        /**Spinner END */

//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Fragment mF = getParentFragment();
//                // double check
//                if (mF instanceof FragmentMyInqHolder) {
//                    getFragmentManager().beginTransaction()
//                            .replace((getParentFragment()).getView().findViewById(R.id.fragment_container).getId()
//                                    , new FragmentCustom())
//                            .commit();
//                }
//            }
//        });

//        dropdown_edit.setOnClickListener(v -> {
//            spinner.performClick();
//        });

//        initCalendar();

//        bdate_edit.setOnClickListener(v -> new DatePickerDialog(getActivity(), date, myCalendar
//                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                myCalendar.get(Calendar.DAY_OF_MONTH)).show());
    }

//    private void initCalendar(){
//            myCalendar = Calendar.getInstance();
//            date = (view1, year, monthOfYear, dayOfMonth) -> {
//            // TODO Auto-generated method stub
//            myCalendar.set(Calendar.YEAR, year);
//            myCalendar.set(Calendar.MONTH, monthOfYear);
//            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//            updateLabel();
//        };
//    }

//    private void updateLabel() {
//        String myFormat = "yyyy-MM-dd"; //In which you need put here
//        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
//
//        bdate_edit.setText(sdf.format(myCalendar.getTime()));
//    }

    public void setBlankAdapter() {
        recyclerviewTags.setAdapter(null);
    }

    public void showLoader() {
        loaderDialog.showDialog(getContext());
    }

    public void hideLoader() {
        loaderDialog.dismissDialog(getContext());
    }


    //UPDATE SECTIONS
    private void updateUser(String options){

        Log.d(TAG, "updateUser: OPTION_ID _1 "+options);
        showLoader();
        update.setText("UPDATING...");
        update.setEnabled(false);
        //sf.getOptionId() = UNIQUE_ID for the USER having custom section
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() +unixTime +  "inq" + sf.getDeviceId() + "-megaupdate_options/" + sf.getOptionId());
        apiService.registerUpdate(String.valueOf(unixTime), sf.getDeviceId(), sf.getOptionId(), options, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String response) {
                        Log.d(TAG, "onSuccess: UpdateUser " + options);
                        if(response.equals("ok")){
                            //Flag
                            Global.isMyInqUpdated = true;
                            hideLoader();
                            //androidx Saving customSectionTags: Replaced Global.WriteFile
                            sf.setCustomMenu(jArrayCustomMenu.toString());

                            //Just go back to FragmentCustom: new FragmentCustom()
                            if (sf.getOptionId() != 0) {
                                Log.d(TAG, "onSuccess: myInqUpdated ");
                                Global.isMyInqEditSectionUpdated = true;
                                EventBus.getDefault().post("myInqUpdated"); //myInqUpdatedNow myInqUpdated
                            }

                        }else{
                            hideLoader();
                            Toast.makeText(getContext(), "Error encountered while updating", Toast.LENGTH_SHORT).show();
                        }
                        //Enabling
                        update.setText("UPDATE");
                        update.setEnabled(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey();
                        hideLoader();
                        update.setText("UPDATE");
                        update.setEnabled(true);
                        Log.d(TAG, "onError: myInqUpdated Throwable Exception_Error: "+e.getMessage());
                    }
                });

    }

    //androidx implementation
    private void fetchTags(){
        Log.d(TAG, "fetchTags: FragmentMyInqEditDebug getPrefIds2_sectionIds INT_DATA: "+getPrefIds2()); //getPrefIds2() returns int array of sf.getCustomMenu()
        Log.d(TAG, "fetchTags: FragmentMyInqEditDebug CustomMenu_EditedSectionIds sf.getCustomMenu() STRING_DATA: "+sf.getCustomMenu()); //sf.getCustomMenu() is a STRING of Array
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        mShimmerViewContainer.startShimmer();
        MenuViewModel menuViewModel = ViewModelProviders.of(this, new ViewModelFactoryMenu(getActivity().getApplication(), compositeDisposable, sf)).get(MenuViewModel.class);
        menuViewModel.getMenu().observe(this, response -> {
            Log.d(TAG, "fetchTags: FragmentMyInqEditDebug response MENU_DATA_FILTERED_SECTIONS: "+response.get("menu").getAsJsonObject().get("sections").getAsJsonArray());
            mShimmerViewContainer.stopShimmer();
            mShimmerViewContainer.setVisibility(View.GONE);

            tagsList = new ArrayList<>();  //Clear
            try{
                for(int x=0; x< response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().size(); x++){
                    //CHECK if SectionTag ID is stored in the storage/sf: then ADD to tagsList and make isSELECTED to TRUE
                    if (getPrefIds2().contains(response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt())) {
                        Log.d(TAG, "fetchTags: FragmentMyInqEditDebug FOUND/CONTAINS_ID_MakeItTrueSelectedInEditSections: DATA: "+response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt());
                       //FOUND/CONTAINS: ADDING id and title to the ARRAY_LIST tagsList:
                        tagsList.add(new TagsModel(
                                response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt(),
                                response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("title").getAsString(),
                                true, response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("hidden").getAsString()));
                    }
                    //ELSE: then ADD to tagsList and make isSELECTED to FALSE
                    else {
                        Log.d(TAG, "fetchTags: getPrefIds_ELSE DATA: called ");
                        tagsList.add(new TagsModel(
                                response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt(),
                                response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("title").getAsString(),
                                false, response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("hidden").getAsString()));
                        Log.d(TAG, "fetchTags: getPrefIds_ELSE TITLE DATA: "+tagsList.get(x).getTitle());
                        Log.d(TAG, "fetchTags: getPrefIds_ELSE IS_SELECTED DATA: "+tagsList.get(x).isSelected());

                    }
                    adapter = new AdapterPreferedTags(getContext(), tagsList);
                    recyclerviewTags.setAdapter(adapter);
                }
            }catch(Exception e){
                    Log.d(TAG, "fetchTags: Exception_menuViewModel_error: "+e.getMessage());
            }
        });
        menuViewModel.getMenuError().observe(this, isError -> {
            if(isError){
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                adapter_empty_tags = new RVAdapterEmpty_Tags_Edit(FragmentMyInqEdit.this);
                recyclerviewTags.setAdapter(adapter_empty_tags);
            }
        });
    }

    //androidx GETTING THE sf.getCustomMenu
    public List<Integer> getPrefIds2() {
        List<Integer> prefs_ids = new ArrayList<>();
        try {
            if(!sf.getCustomMenu().isEmpty()){
                JSONArray editSectionTagsList = new JSONArray(sf.getCustomMenu());
                for (int i = 0; i < editSectionTagsList.length(); i++) {
                    JSONObject item = editSectionTagsList.getJSONObject(i); //getting each element object
                    prefs_ids.add(item.getInt("id"));  //Adding to List
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "getPrefIds2: Exception_Error: "+e.getMessage());
        }
        return prefs_ids;
    }

    public void RetryFetchTags(){
            fetchTags();
    }

    @Override
    public void onResume() {
        name_edit.setText(sf.getName());
        spinner.setSelection(getGender(sf.getGender())); //
        email_edit.setText(sf.getEmail());//
        bdate_edit.setText(sf.getBdate());
        mobile_edit.setText(sf.getMobile());
        name_edit.setEnabled(false);
        email_edit.setEnabled(false);
        bdate_edit.setEnabled(false);
        mobile_edit.setEnabled(false);
        spinner.setEnabled(false);
        fetchTags();
        super.onResume();
    }

    private int getGender(String gender){
        int gdr = 0;
        switch (gender){
            case "Male":
                gdr = 0;
                break;
            case "Female":
                gdr = 1;
                break;
            case "LGBTQ":
                gdr = 2;
                break;
            case "Don't want to say":
                gdr = 3;
                break;
        }
        return gdr;
    }

    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

}

