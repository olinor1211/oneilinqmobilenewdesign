package com.knx.inquirer.fragments;


import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.knx.inquirer.models.MenuResponse;
import com.knx.inquirer.models.MyOption;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.R;
import com.knx.inquirer.models.TagsModel;
import com.knx.inquirer.rvadapters.AdapterPreferedTags;
import com.knx.inquirer.rvadapters.RVAdapterEmpty_Tags_MyInq;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryMenu;
import com.knx.inquirer.viewmodels.MenuViewModel;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyInq extends Fragment {
    private static final String TAG = "FragmentMyInq";

    private List<TagsModel> tagsList;
    private Button submit;
    private ImageView dropdown;
    private EditText name, bdate, mEmail, mMobile;
    private Calendar myCalendar;
    private Spinner spinner;
    public RecyclerView recyclerviewTags;
    private AdapterPreferedTags adapter;
    private RVAdapterEmpty_Tags_MyInq adapter_empty_tags;
    private CheckBox chkAgree;
    private TextView txtAgree,txtAgree2;


    private SharedPrefs sf;
    private ApiService apiService;


    private JSONArray jArrayCustomMenu;

    private boolean hasLoadedOnce_ = false; // your boolean field

    private DatePickerDialog.OnDateSetListener date;

    private ShimmerFrameLayout mShimmerViewContainer;

    private Global.LoaderDialog loaderDialog;


    String regOpts, regName, regEmail , regGender, regBdate, regMobile; //
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private boolean hasLoadedOnce;

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            //Refresh Only if Not Yet Registered: Not used when registered already
            if(sf!=null){
                if(!sf.isRegistered())getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
            }
        }
    }

    public FragmentMyInq() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(FragmentMyInq.this)) EventBus.getDefault().register(FragmentMyInq.this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        sf =  new SharedPrefs(getContext());
        apiService = ServiceGenerator.createService(ApiService.class);

        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);

        View view = inflater.inflate(R.layout.fragment_my_inq, container, false);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FragmentMyInq_LAYOUT_CALLED****");
        initViews(view);
        fetchTags();
        return view;
    }

    public void initViews(View view) {

        sf = new SharedPrefs(getContext());

        loaderDialog = new Global.LoaderDialog();

        chkAgree = view.findViewById(R.id.chkAgree);
        txtAgree = view.findViewById(R.id.txtAgree);
        txtAgree2 = view.findViewById(R.id.txtAgree2);

        txtAgree.setOnClickListener(v-> gotoSite("http://inq.news/MMTandC"));
        txtAgree2.setOnClickListener(v->gotoSite("http://inq.news/privacy"));


        //DIRECT HTML as TEXT_VIEW value
//        String dark = getResources().getString(R.string.agree_dark);
//        String normal = getResources().getString(R.string.agree_normal);

//        if(sf.getNightMode())  txtAgree.setText(Html.fromHtml(dark));
//        else  txtAgree.setText(Html.fromHtml(normal));
////
//        if(sf.getNightMode())  txtAgree.setText(getActivity().getApplicationContext().getResources().getString(R.string.agree_dark));
//        else  txtAgree.setText(getActivity().getApplicationContext().getResources().getString(R.string.agree_normal));
//        String normal = "<span style=\"color:black\" >I agree to the </span><a href='http://inq.news/MMTandC'>Terms and Conditions </a> <span style=\"color:black\"> and </span> <a href='http://inq.news/privacy'> Privacy Policy </a>";
//        String dark = "<span style=\"color:white\" >I agree to the </span><a href='http://inq.news/MMTandC'>Terms and Conditions </a> <span style=\"color:white\"> and </span> <a href='http://inq.news/privacy'> Privacy Policy </a>";

//        txtAgree.setText(Html.fromHtml("<span style=\"color:black\" >I agree to the </span><a href='http://inq.news/MMTandC'>Terms and Conditions </a> <span style=\"color:black\"> and </span> <a href='http://inq.news/privacy'> Privacy Policy </a>"));

        if(sf.getNightMode()){
            txtAgree.setTextColor(getContext().getResources().getColor(R.color.sky_blue));
            txtAgree2.setTextColor(getContext().getResources().getColor(R.color.sky_blue));
        }

        chkAgree.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                submit.setEnabled(true);
                submit.setBackgroundResource(R.drawable.round_button_darkblue);
            }else{
                submit.setEnabled(false);
                submit.setBackgroundResource(R.drawable.round_button_darkgray);
            }
        });

        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.setAlignment(Alignment.CENTER);

        recyclerviewTags = view.findViewById(R.id.recyclersections);
        recyclerviewTags.setLayoutManager(flowLayoutManager); //NOT LINEAR but FlowLayoutManager

        /**Spinner START */
        ArrayList genderArrayList = new ArrayList();
        genderArrayList.add("Gender");
        genderArrayList.add("Male");
        genderArrayList.add("Female");
        genderArrayList.add("LGBTQ");
        genderArrayList.add("Don't want to say");

        spinner = view.findViewById(R.id.spGender);
//        ArrayAdapter genderAdapter = ArrayAdapter.createFromResource(this, genderArrayList, R.layout.spinner_item);
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_textview, genderArrayList){
//        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/SF-UI-DISPLAY-REGULAR.otf");
            //View getView() TextView of ArrayList DATA
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                final ColorStateList hint_color = name.getHintTextColors(); //GETTING THE HINT_COLOR or EDITVIEW
                if(position == 0) {
                    ((TextView) v).setTextColor(hint_color);
                }
                return v;
            }

            //View getDropDownView() TextView of DROP_DOWN_LIST
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    tv.setVisibility(View.GONE);
                    v = tv; //v = tv;
                }else{
                    // Pass convertView as null to prevent reuse of special case views
                    v = super.getDropDownView(position, null, parent);
                }
                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };

        genderAdapter.setDropDownViewResource(R.layout.spinner_textview_dropdown);
        spinner.setAdapter(genderAdapter);
        /**Spinner END */

//        dropdown = view.findViewById(R.id.ivDropdown);
        name = view.findViewById(R.id.txtname);
        bdate = view.findViewById(R.id.txtbdate);
        mEmail = view.findViewById(R.id.tvEmail);
        mMobile = view.findViewById(R.id.tvMobile);
        submit = view.findViewById(R.id.btnsubmit);
        submit.setEnabled(false);

        mShimmerViewContainer = view.findViewById(R.id.shimmer_tags_reg);

        submit.setOnClickListener(v -> {
            if(Global.isNetworkAvailable(getContext())){
                //Saving Tags Menu in the List selections
                List<String> selections = new ArrayList<>(); //
                jArrayCustomMenu = new JSONArray(); //Clearing JsonArray
                if(tagsList.size() > 0){
                    for (TagsModel model : tagsList) {
                        if (model.isSelected()) { //Filter only the Selected Tags
                            selections.add(String.valueOf(model.getId()));  //Add Tags ID to the List string
                            try {
                                JSONObject jsonObject = new JSONObject(); //Creating JsonObject structure
                                jsonObject.put("id", model.getId());
                                jsonObject.put("title", model.getTitle());
                                jArrayCustomMenu.put(jsonObject); //Add object to JsonArray
                            } catch (Exception e) {
                                Log.d(TAG, "initViews: Exception_Error: "+e.getMessage());
                            }
                        }
                    }
                }

                Log.d(TAG, "initViews: JOptions" +jArrayCustomMenu.toString());

                if(jArrayCustomMenu.length() < 1){
                    Toast.makeText(getContext().getApplicationContext(), "Please fill up all the needed information and select your preferred sections", Toast.LENGTH_SHORT).show();
                }else {
                    regOpts = new Gson().toJson(selections);  //Convert List Selections to String comma-delimited
                    Log.d(TAG, "initViews: regOpts .toJson DATA: "+regOpts);
                    regName = name.getText().toString();  //NAME
                    regEmail = mEmail.getText().toString();  //EMAIL
                    regMobile = mMobile.getText().toString();  //MOBILE
                    regGender = spinner.getSelectedItem().toString();  //GENDER
                    regBdate = bdate.getText().toString();  //BDAY

                    if(TextUtils.isEmpty(regName) || TextUtils.isEmpty(regEmail) || TextUtils.isEmpty(regBdate) || TextUtils.isEmpty(regMobile) || regGender.equals("Gender")) {
                        Toast.makeText(getContext().getApplicationContext(), "Please fill up all the needed information and select your preferred sections", Toast.LENGTH_SHORT).show();
                    }
                    else if (regName.length() <= 1) Toast.makeText(getContext().getApplicationContext(), "Name must be at least 2 characters long.", Toast.LENGTH_SHORT).show();
                    else if(!Patterns.EMAIL_ADDRESS.matcher(regEmail).matches()) Toast.makeText(getContext().getApplicationContext(), "Invalid email address.", Toast.LENGTH_SHORT).show();
                    else if (regMobile.length() != 11) Toast.makeText(getContext().getApplicationContext(), "The mobile number you entered is invalid.", Toast.LENGTH_SHORT).show();
                    else if (!regMobile.matches("0[89][\\d]{9}"))Toast.makeText(getContext().getApplicationContext(), "The mobile number you entered is invalid.", Toast.LENGTH_SHORT).show();
                    else registerUser(regName, regGender, regEmail, regBdate, regMobile, regOpts);  // registerUser(name, gender, bdate, opts);
                }

            }else{
                Global.ViewDialog alert = new Global.ViewDialog();
                alert.showDialog(getContext(), "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
            }


        });


        initCalendar();
        bdate.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog =  new DatePickerDialog(getActivity(), date,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        });
    }

    public void gotoSite(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void initCalendar(){
            myCalendar = Calendar.getInstance();
            myCalendar.add(Calendar.YEAR, -10); //Goes 10 Year Back in time
            date = (view1, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();

        };
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        bdate.setText(sdf.format(myCalendar.getTime()));
    }

    public void showLoader() {
        loaderDialog.showDialog(getContext());
    }

    public void hideLoader() {
        loaderDialog.dismissDialog(getContext());
    }

    //androidx implementation
    public void fetchTags(){
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        mShimmerViewContainer.startShimmer();
        chkAgree.setEnabled(false);

        MenuViewModel menuViewModel = ViewModelProviders.of(FragmentMyInq.this, new ViewModelFactoryMenu(getActivity().getApplication(), compositeDisposable, sf)).get(MenuViewModel.class);
        menuViewModel.getMenu().observe(this, response-> {

            mShimmerViewContainer.stopShimmer();
            mShimmerViewContainer.setVisibility(View.GONE);
            chkAgree.setEnabled(true);

            tagsList = new ArrayList<>(); //Clear first

            try{
                for(int x=0; x< response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().size(); x++){
                    tagsList.add(new TagsModel(
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt(),
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("title").getAsString(),
                            false, response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("hidden").getAsString()));
                }
            }catch(Exception e){
                Log.d(TAG, "fetchTags: Exception_menuViewModel_error: "+e.getMessage());
            }

            //adapter for the menu tags sections AdapterPreferedTags
            adapter = new AdapterPreferedTags(getContext(), tagsList);
            recyclerviewTags.setAdapter(adapter);

            Log.d(TAG, "fetchTags: DATA: "+tagsList.toString());

//            try{
//                for (int i = 0; i < response.getMenu().getSections().size(); i++) {  //Sections
//
//                    //adding to the model as a List
//                    tagsList.add(new TagsModel(response.getMenu().getSections().get(i).getId(), response.getMenu().getSections().get(i).getTitle(), false));
//
//                    //adapter for the menu tags sections AdapterPreferedTags
//                    adapter = new AdapterPreferedTags(getContext(), tagsList);
//                    recyclerviewTags.setAdapter(adapter);
//                }
//            }catch(Exception e){
//                Log.d(TAG, "fetchTags: Exception_menuViewModel_error: "+e.getMessage());
//            }



        });

        menuViewModel.getMenuError().observe(this, isError -> {
            if(isError){
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                chkAgree.setEnabled(false);

                adapter_empty_tags = new RVAdapterEmpty_Tags_MyInq(FragmentMyInq.this);
                recyclerviewTags.setAdapter(adapter_empty_tags);
            }
        });
    }

    //get the selected preferred section tag ..True/False ..
//    public void fetchTags(){
//
//        mShimmerViewContainer.setVisibility(View.VISIBLE);
//        mShimmerViewContainer.startShimmer();
//        chkAgree.setEnabled(false);
//
//        MenuViewModel menuViewModel = ViewModelProviders.of(FragmentMyInq.this, new ViewModelFactoryMenu(getActivity().getApplication(), compositeDisposable, sf)).get(MenuViewModel.class);
//        menuViewModel.getMenu().observe(this, new Observer<MenuResponse>() {
//            @Override
//            public void onChanged(MenuResponse menuResponse) {
//                mShimmerViewContainer.stopShimmer();
//                mShimmerViewContainer.setVisibility(View.GONE);
//                chkAgree.setEnabled(true);
//
//                tagsList = new ArrayList<>();
//
//                try{
//                    for (int i = 0; i < menuResponse.getMenu().getSections().size(); i++) {  //Sections
//
//                        //adding to the model as a List
//                        tagsList.add(new TagsModel(menuResponse.getMenu().getSections().get(i).getId(), menuResponse.getMenu().getSections().get(i).getTitle(), false));
//
//                        //adapter for the menu tags sections AdapterPreferedTags
//                        adapter = new AdapterPreferedTags(getContext(), tagsList);
//                        recyclerviewTags.setAdapter(adapter);
//                    }
//                }catch(Exception e){
//                    Log.d(TAG, "fetchTags: Exception_menuViewModel_error: "+e.getMessage());
//                }
//            }
//        });
//
//        menuViewModel.getMenuError().observe(this, new Observer<Boolean>() {
//            @Override
//            public void onChanged(Boolean isError) {
//                if(isError){
//                    mShimmerViewContainer.stopShimmer();
//                    mShimmerViewContainer.setVisibility(View.GONE);
//                    chkAgree.setEnabled(false);
//
//                    adapter_empty_tags = new RVAdapterEmpty_Tags_MyInq(FragmentMyInq.this);
//                    recyclerviewTags.setAdapter(adapter_empty_tags);
//                }
//            }
//        });
//    }

    private void registerUser(String name, String gender, String email, String bdate, String mobile,String opts){
        String genderCode = changeGenderCode(gender);
        Log.d(TAG, "REGISTERING: "+name+" * "+gender+" * "+email+" * "+bdate+" * "+mobile+" * "+opts);
        showLoader();
        submit.setText("REGISTERING...");
        submit.setEnabled(false);
        apiService = ServiceGenerator.createService(ApiService.class);

        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megaregister/" + name + "/" + genderCode + "/" + bdate + "/" + mobile);
        apiService.registerUser(String.valueOf(unixTime), sf.getDeviceId(), name, genderCode, bdate, mobile, opts, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<MyOption>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(MyOption myOption) {
                        Log.d(TAG, "onSuccess: registered DATA: " + myOption.getOption() );
                        hideLoader();
                        sf.setAsRegistered(true);
                        sf.setOptionId(myOption.getOption()); //UNIQUE_ID for the USER having custom section
                        sf.setName(name);
                        sf.setEmail(email);
                        sf.setBdate(bdate);
                        sf.setMobile(mobile);
                        sf.setGender(gender);
                        Log.d(TAG, "onSuccess: sf.getOptionId 1_or_0 DATA: " + sf.getOptionId());
                        //writing/saving  to file storage  ..custom_menu.txt
//                        try{
//                            Global.WriteFile(jArrayCustomMenu.toString(), Environment.getExternalStorageDirectory().getAbsolutePath() + "/inquirer/custom_menu.txt");
//                        }catch(Exception e){
//                            Log.d(TAG, "onCreate:registerUser Exception_Error WRITE_FILE: "+e.getMessage());
//                        }
                        //androidx Saving customSectionTags: Replaced Global.WriteFile
                        sf.setCustomMenu(jArrayCustomMenu.toString());
                        //going/intent to childFragment from childFragment  ..from FragmentMyInq to FragmentMyInqSuccess
//                        if (sf.getOptionId() != 0) {
//                            //get current fragment
//                            Fragment mF = getParentFragment();  //go back to parent fragment FragmentMyInqHolder, then replace the child fragment into FragmentMyInqSuccess
//                            //if under current fragment
//                            if (mF instanceof FragmentMyInqHolder) {
//                                //intent/calling from one fragment to another fragment ..new FragmentMyInqSuccess()
//                                getFragmentManager().beginTransaction()
//                                        .replace((getParentFragment()).getView().findViewById(R.id.fragment_container).getId()
//                                                , new FragmentMyInqSuccess())
//                                        .commit();
//                            }
//                        }

                        //GO to directly to Fragment FragmentMyInqSuccess
                        if (sf.getOptionId() != 0) {
                            EventBus.getDefault().post("myInqSuccess"); //
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey();
                        hideLoader();
                        submit.setText("SUBMIT");
                        submit.setEnabled(true);
                        Log.d(TAG, "onError: Throwable Exception_Error: "+e.getMessage());
                    }
                });
    }
    public void RetryFetchTags(){
        fetchTags();
    }

    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    private String changeGenderCode(String gender){
        switch(gender){
            case "Male":
                return "M";
            case "Female":
                return "F";
            case "LGBTQ":
                return "L";
            default:
                return "D";
        }
    }
}

