package com.knx.inquirer.fragments;

import android.app.DatePickerDialog;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.knx.inquirer.R;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.SharedPrefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class FragmentMyInqRegistrationP1 extends Fragment implements AdapterView.OnItemSelectedListener{
    private final String TAG = "FragmentRegistrationP1";
    private Boolean hasName = false,hasBDate = false,hasEmail = false,hasMobile = false,hasGender = false, hasPassword = false;

    private Button mContinue;
    private Spinner mSpinner;
    private EditText mName, mBdate, mEmail, mMobile, mPassword;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;

    private SharedPrefs sf;

    public FragmentMyInqRegistrationP1() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_my_inq_registration_p1, container, false);
        sf =  new SharedPrefs(getContext());
//        apiService = ServiceGenerator.createService(ApiService.class);

        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);
        initViews(view);
        spinnerSettings(view);
        calendarSettings();
        return view;
    }

    private void calendarSettings(){
        myCalendar = Calendar.getInstance();
        myCalendar.add(Calendar.YEAR, -10); //Goes 10 Year Back in time
        date = (view1, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        mBdate.setText(sdf.format(myCalendar.getTime()));
        hasBDate = true;
        listenFieldState();
    }

    private void listenFieldState() {
        if(hasName&&hasGender&&hasEmail&&hasBDate&&hasMobile&&hasPassword){
            mContinue.setEnabled(true);
            mContinue.setBackgroundResource(R.drawable.round_button_blue);
            Global.regName = mName.getText().toString().trim();
            Global.regGender  = mSpinner.getSelectedItem().toString().trim();
            Global.regEmail  = mEmail.getText().toString().trim();
            Global.regBdate  = mBdate.getText().toString().trim();
            Global.regMobile  = mMobile.getText().toString().trim();
            Global.regPass  = mPassword.getText().toString().trim();
        }else{
            mContinue.setEnabled(false);
            mContinue.setBackgroundResource(R.drawable.round_button_gray);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    private void initViews(View view) {

        mName = view.findViewById(R.id.etNameReg);
        mBdate = view.findViewById(R.id.etBdateReg);
        mEmail = view.findViewById(R.id.etEmailReg);
        mMobile = view.findViewById(R.id.etMobileReg);
        mPassword = view.findViewById(R.id.etPasswordReg);
        mSpinner = view.findViewById(R.id.spGenderReg);
        mSpinner.setOnItemSelectedListener(this);
        mContinue = view.findViewById(R.id.btnContinueReg);

        mContinue.setOnClickListener(view1 -> getContinue());

        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(!isEmpty(mName)) hasName = true;
                else hasName = false;
                listenFieldState();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        });
        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(!isEmpty(mEmail))hasEmail = true;
                else hasEmail = false;
                listenFieldState();

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        });
        mMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(!isEmpty(mMobile))hasMobile = true;
                else hasMobile = false;
                listenFieldState();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        });

        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(!isEmpty(mPassword))hasPassword = true;
                else hasPassword = false;
                listenFieldState();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        });


        //Working But not used, can be used to any view
//        mSpinner.setOnTouchListener((view12, motionEvent) -> {
//            if(motionEvent.getAction() == MotionEvent.ACTION_UP){
//                Log.d(TAG, "initViews: SPINNER: TOUCHED "+ mSpinner.getSelectedItem());
//                if(mSpinner.getSelectedItem()!="Gender"){
//                    Log.d(TAG, "initViews: SPINNER: OK "+ mSpinner.getSelectedItem());
//                    hasGender = true;
//                    listenFieldState();
//                }
//            }
//            return false;
//        });

        mBdate.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog =  new DatePickerDialog(getActivity(), date,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        });
    }

    private Boolean isEmpty(EditText et) {
        return et.getText().toString().trim().length()==0;
    }

    private void spinnerSettings(View view) {
        ArrayList genderArrayList = new ArrayList();
        genderArrayList.add("Gender");
        genderArrayList.add("Male");
        genderArrayList.add("Female");
        genderArrayList.add("LGBTQ");
        genderArrayList.add("Don't want to say");

        Log.d(TAG, "spinnerSettings: "+genderArrayList);

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_textview, genderArrayList){
            //View getView() TextView of ArrayList DATA
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                final ColorStateList hint_color = mName.getHintTextColors(); //
                if(position == 0) {
                    ((TextView) v).setTextColor(hint_color);
                }
                return v;
            }

            //View getDropDownView() TextView of DROP_DOWN_LIST
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    tv.setVisibility(View.GONE);
                    v = tv; //v = tv;
                }else{
                    // Pass convertView as null to prevent reuse of special case views
                    v = super.getDropDownView(position, null, parent);

                }
                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };

        genderAdapter.setDropDownViewResource(R.layout.spinner_textview_dropdown);
        mSpinner.setAdapter(genderAdapter);

    }

    private void getContinue() {
        if (Global.regName.length() <= 3) Toast.makeText(getContext().getApplicationContext(), "Name must be at least 4 characters long.", Toast.LENGTH_SHORT).show();
        else if(!Patterns.EMAIL_ADDRESS.matcher(Global.regEmail).matches()) Toast.makeText(getContext().getApplicationContext(), "Invalid email address.", Toast.LENGTH_SHORT).show();
        else if (Global.regMobile.length() != 11) Toast.makeText(getContext().getApplicationContext(), "The mobile number you entered is invalid.", Toast.LENGTH_SHORT).show();
        else if (!Global.regMobile.matches("0[89][\\d]{9}"))Toast.makeText(getContext().getApplicationContext(), "The mobile number you entered is invalid.", Toast.LENGTH_SHORT).show();
        else if (Global.regPass.length() <= 5) Toast.makeText(getContext().getApplicationContext(), "Password must be at least 6 characters long.", Toast.LENGTH_SHORT).show();
        else  EventBus.getDefault().post("continueRegistration");
    }

    /**ImplementsInterface: AdapterView.OnItemSelectedListener Can be used to Any View that has an ADAPTER ex: Spinner*/
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "onItemSelected: Spinner ");
        if(adapterView.getItemAtPosition(i)!="Gender"){
            Log.d(TAG, "onItemSelected: Spinner "+adapterView.getItemAtPosition(i));
            hasGender = true;
            listenFieldState();
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}
}