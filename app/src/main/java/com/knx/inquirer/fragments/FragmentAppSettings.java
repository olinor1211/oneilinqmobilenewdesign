package com.knx.inquirer.fragments;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.knx.inquirer.R;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.suke.widget.SwitchButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class FragmentAppSettings extends Fragment implements View.OnClickListener {
    private static final String TAG = "FragmentAppSettings";
    private SwitchButton mNightMode, mNotification ;
    private TextView mMyInquirer, mNight, mNotif;
    private TextView mAppVersion;
    private ImageView mInqViberChatbot, mInqViberCommunity, mInqFacebook, mInqTwitter;
    private ImageView mInqProperty, mInqBusiness, mInqLifestyle, mInqSports, mInqEntertainment;
    private TextView nInqViberChatbot, nInqViberCommunities, nInqMobileFacebook, nInqMobileTwitter;

    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiService apiService;

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg=="notificationPermitted"){
            registerNotificationPermission(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_STORE_NOTIF_DEVICE, "Y");
        }
    }

    public FragmentAppSettings() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(FragmentAppSettings.this)) EventBus.getDefault().register(FragmentAppSettings.this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sf =  new SharedPrefs(getContext());
        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);

        View view =  inflater.inflate(R.layout.fragment_app_settings, container, false);
        apiService = ServiceGenerator.createService(ApiService.class);
        initViews(view);
        loadViews(view);
        Log.d(TAG, "onCreateView: CALLED");
        return view;
    }

    private void loadViews(View view) {
        if(sf.getNightMode()){
            mNightMode.setChecked(true);
            updateDrawableDark();
        }
        else{
            mNightMode.setChecked(false);
            updateDrawableNormal();
        }
        //
        if(sf.getNotificationFlag().equals("Y")) mNotification.setChecked(true);
        else mNotification.setChecked(false);

    }

    public void initViews(View view) {
        mNightMode = view.findViewById(R.id.sw_appSettingNightMode);
        mNotification = view.findViewById(R.id.sw_appSettingNotification);
        mMyInquirer = view.findViewById(R.id.appSettingMyInquirer);
        mNight = view.findViewById(R.id.appSettingNightMode);
        mNotif = view.findViewById(R.id.appSettingNotification);
        mAppVersion = view.findViewById(R.id.appSettingAppVersion);
        mInqViberChatbot = view.findViewById(R.id.ivInqViberChatbot);
        nInqViberChatbot = view.findViewById(R.id.tvInqViberChatbot);
        mInqViberCommunity = view.findViewById(R.id.ivInqViberCommunity);
        nInqViberCommunities = view.findViewById(R.id.tvInqViberCommunities);
        mInqFacebook = view.findViewById(R.id.ivInqFacebook);
        nInqMobileFacebook = view.findViewById(R.id.tvInqMobileFacebook);
        mInqTwitter = view.findViewById(R.id.ivInqTwitter);
        nInqMobileTwitter = view.findViewById(R.id.tvInqMobileTwitter);
        mInqProperty = view.findViewById(R.id.ivInqProperty);
        mInqBusiness = view.findViewById(R.id.ivInqBusiness);
        mInqLifestyle = view.findViewById(R.id.ivInqLifestyle);
        mInqSports = view.findViewById(R.id.ivInqSports);
        mInqEntertainment = view.findViewById(R.id.ivInqEntertainment);

        //OPTIONAL
        mAppVersion.setVisibility(View.VISIBLE);
        mAppVersion.setText("Version "+(Global.currentVersion));
        //RESOLVING ISSUE on AndroidVersion 5.1 DrawableLeft Image is too large: Make it programmatically to fit in the height of the TextView
        //RESOLVING by REMOVING the DrawableLeft in the XML

        //
        mInqViberChatbot.setOnClickListener(this);
        nInqViberChatbot.setOnClickListener(this);
        mInqViberCommunity.setOnClickListener(this);
        nInqViberCommunities.setOnClickListener(this);
        mInqFacebook.setOnClickListener(this);
        nInqMobileFacebook.setOnClickListener(this);
        mInqTwitter.setOnClickListener(this);
        nInqMobileTwitter.setOnClickListener(this);
        mInqProperty.setOnClickListener(this);
        mInqBusiness.setOnClickListener(this);
        mInqLifestyle.setOnClickListener(this);
        mInqSports.setOnClickListener(this);
        mInqEntertainment.setOnClickListener(this);
//
        mNightMode.setOnCheckedChangeListener((view1, isChecked) -> {
            if(isChecked){
                sf.setNightMode(true);
//                updateDrawableDark();
            }
            else{
                sf.setNightMode(false);
//                updateDrawableNormal();
            }
            //REFRESH this Fragment
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
            //REFRESH all Fragment
            EventBus.getDefault().post("night_mode");
        });
        mNotification.setOnCheckedChangeListener((view1, isChecked) -> {
            Log.d(TAG, "initViews: mNotification.setOnCheckedChangeListener "+ isChecked);
            if(isChecked){
                sf.setNotificationFlag("Y");
                registerNotificationPermission(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_STORE_NOTIF_DEVICE, "Y");
            }
            else{
                sf.setNotificationFlag("N");
                registerNotificationPermission(sf.getDeviceId(), sf.getFirebaseToken(), "Android", Global.PASS_STORE_NOTIF_DEVICE, "N");
            }

        });
        mMyInquirer.setOnClickListener(view12 -> {
            EventBus.getDefault().post("appMyInqEdit");
        });
    }

    private void updateDrawableDark() {
        mMyInquirer.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_setting_myinq_edit_dark_mode, 0, 0, 0);  //app_setting_myinq_edit_dark_mode
        mNight.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_setting_night_mode_dark_mode, 0, 0, 0); //app_setting_night_mode_dark_mode
        mNotif.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_setting_notification_dark_mode, 0, 0, 0); //app_setting_notification_dark_mode //test_notif24
    }
    private void updateDrawableNormal() {
        mMyInquirer.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_setting_myinq_edit, 0, 0, 0);  //app_setting_myinq_edit
        mNight.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_setting_night_mode, 0, 0, 0); //app_setting_night_mode
        mNotif.setCompoundDrawablesWithIntrinsicBounds(R.drawable.app_setting_notification, 0, 0, 0); //app_setting_notification
    }

    //registerNotificationPermission
    private void registerNotificationPermission(String deviceId, String token, String deviceType, String password, String notifFlag) {
//        ApiService apiService = ServiceGenerator.createService(ApiService.class);
        apiService.registerNotificationPermission(deviceId, token, deviceType, password, notifFlag)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseBody responseBody) {
                        try {
                            String r = responseBody.string();
                            Log.d(TAG, "onSuccess: ResponseBody:" + r);
                            if (r.contains("Store Successfully")) {///
                                Log.d(TAG, "registerNotificationPermission:  NOTIFICATION_App_Setting RESPONSE: Store Successfully ");
                            }else if(r.contains("Record updated 2 ")){
                                Log.d(TAG, "registerNotificationPermission: NOTIFICATION_App_Setting RESPONSE: Record updated");
                            }
                        } catch (IOException e) {
                            Log.d(TAG, "onSuccess: IOException_Error: "+e.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        sf.setNotificationAllowed(true);
                        Log.d(TAG, "onError: registerNotificationPermission error  getMessage" + e.getMessage());

                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivInqViberChatbot:
            case R.id.tvInqViberChatbot:
                linkOut(Global.LINK_INQM_BOT);
                break;

            case R.id.ivInqViberCommunity:
            case R.id.tvInqViberCommunities:
                linkOut(Global.LINK_VIBER_COMMUNITY);
                break;

            case R.id.ivInqFacebook:
            case R.id.tvInqMobileFacebook:
                linkOut(Global.LINK_INQM_FB);
                break;

            case R.id.ivInqTwitter:
            case R.id.tvInqMobileTwitter:
                linkOut(Global.LINK_INQM_TWITTER);
                break;

            case R.id.ivInqProperty:
                linkOut(Global.LINK_VIBER_PROPERTY);
                break;
            case R.id.ivInqBusiness:
                linkOut(Global.LINK_VIBER_BUSINESS);
                break;
            case R.id.ivInqLifestyle:
                linkOut(Global.LINK_VIBER_LIFESTYLE);
                break;
            case R.id.ivInqSports:
                linkOut(Global.LINK_VIBER_SPORTS);
                break;
            case R.id.ivInqEntertainment:
                linkOut(Global.LINK_VIBER_ENTERTAINMENT);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }

    private void linkOut(String url){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        getContext().startActivity(intent);
    }
}