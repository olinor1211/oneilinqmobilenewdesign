package com.knx.inquirer.fragments;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.models.Article;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.R;
import com.knx.inquirer.models.PostArticleTrending;
import com.knx.inquirer.models.TrendingModel;
import com.knx.inquirer.rvadapters.RVAdapterEmpty_Trending;
import com.knx.inquirer.rvadapters.RVAdapterTrending;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryTrending;
import com.knx.inquirer.viewmodels.TrendingViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FragmentTrending extends Fragment {
    private static final String TAG = "FragmentTrending";
    private RecyclerView rvTrending;
    private LinearLayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private RVAdapterEmpty_Trending adapter_empty_trending;
    private RVAdapterTrending adapter_trending;
    private ArrayList<TrendingModel> trendingModel = new ArrayList<>();

    private SharedPrefs sf;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiService apiService;

    private Global.LoaderDialog loaderDialog;

    public FragmentTrending() {
        // Required empty public constructor
    }

    private boolean hasLoadedOnce = false; // your boolean field

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !hasLoadedOnce) {
                getTrending();
                hasLoadedOnce = true;
            }
        }
    }

    @Override
    public void onDestroy() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();

            //test
//            adapter_trending.releasePlayer();
//            adapter_trending.releasePlayer();
        }
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onItemPostEvent(PostArticleTrending item) {
        viewArticle(item.getItempos());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sf = new SharedPrefs(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FragmentTrending_FRAGMENT_CALLED****");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trending, container, false);

        initViews(view);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FragmentTrending_LAYOUT_CALLED****");


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        EventBus.getDefault().register(FragmentTrending.this);

        apiService = ServiceGenerator.createService(ApiService.class);
    }

    public void initViews(View view) {

        rvTrending = view.findViewById(R.id.recyclerview);
        rvTrending.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        rvTrending.setLayoutManager(layoutManager);

        loaderDialog = new Global.LoaderDialog();

    }

    public void RetryFetchTrending() {
        getTrending();
    }

    public void onArticleRequest() {
        loaderDialog.showDialog(getContext());

    }

    public void onArticleRequestSuccess() {
        loaderDialog.dismissDialog(getContext());

    }

    public void onArticleRequestError() {

        Global.isItemClicked = false;
        loaderDialog.dismissDialog(getContext());

    }

    public void setBlankAdapter(){
        rvTrending.setAdapter(null);
    }

    public void showLoader(){
        setBlankAdapter();
        loaderDialog.showDialog(getContext());
    }

    public void hideLoader(){
        loaderDialog.dismissDialog(getContext());
    }


    private void getTrending(){

        showLoader();

        TrendingViewModel trendingViewModel = ViewModelProviders.of(this, new ViewModelFactoryTrending(getActivity().getApplication(), compositeDisposable, sf)).get(TrendingViewModel.class);
        trendingViewModel.getTrendingNews().observe(this, trending -> {

            hideLoader();

            trendingModel.clear();
            trendingModel.addAll(trending);

//            //test Data TrendingModel constructor should be removed after testing
//            String images = "https://homepages.cae.wisc.edu/~ece533/images/arctichare.png,https://homepages.cae.wisc.edu/~ece533/images/baboon.png,https://homepages.cae.wisc.edu/~ece533/images/cat.png,https://homepages.cae.wisc.edu/~ece533/images/airplane.png,https://homepages.cae.wisc.edu/~ece533/images/peppers.png,http://placehold.it/120x120&text=image3,http://placehold.it/120x120&text=image4";
//            trendingModel.add(new TrendingModel("49300", "3", "Jesus loves you !", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4", "September 09, 2019 02:53pm","https://www.ispot.tv/ad/76Ab/google-chromecast-bigger-blazes"));
//            trendingModel.add(new TrendingModel("49301", "5", "Jesus loves me !", "https://combo.staticflickr.com/ap/build/images/refencing-announcement/bird2.jpg", "September 09, 2019 02:53pm","https://www.flickr.com/lookingahead"));
//            trendingModel.add(new TrendingModel("49302", "6", "Jesus loves everyone !", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4", "September 09, 2019 02:53pm","https://www.ispot.tv/ad/75j2/google-chromecast-bigger-fun"));
//            trendingModel.add(new TrendingModel("49303", "4", "Jesus loves me so much !", images, "September 09, 2019 02:53pm","https://www.ispot.tv/ad/IS1X/pringles-super-bowl-2019-sad-device-song-by-lipps-inc"));
//            trendingModel.add(new TrendingModel("49304", "7", "Jesus loves us!", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4", "September 09, 2019 02:53pm","https://www.ispot.tv/ad/75OO/google-chromecast-bigger-escapes"));
//            trendingModel.add(new TrendingModel("49305", "2", "30th SEA Games !", "https://2019seagames.com/img/about/SEA_Togetherness_Locked_Delivery-01-1.png", "September 09, 2019 02:53pm","https://2019seagames.com"));
//            trendingModel.add(new TrendingModel("49305", "2", "30th SEA Games !", "https://2019seagames.com/img/about/SEA_Togetherness_Locked_Delivery-01-1.png", "September 09, 2019 02:53pm","https://2019seagames.com"));
//            trendingModel.add(new TrendingModel("49104", "9", "Jesus loves you !", "https://2019seagames.com/img/about/SEA_Togetherness_Locked_Delivery-01-1.png", "September 09, 2019 02:53pm","https://www.ispot.tv/ad/76Ab/google-chromecast-bigger-blazes"));


            adapter = new RVAdapterTrending(getContext(), trendingModel, FragmentTrending.this, sf);
            rvTrending.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        });

        trendingViewModel.getNewsError().observe(this, isError -> {
            hideLoader();
            if(isError){
                trendingViewModel.getNewsThrowable().observe(this, e -> {
                    String error_msg = Global.getResponseCode(e);
                    adapter_empty_trending = new RVAdapterEmpty_Trending(FragmentTrending.this, error_msg);
                    rvTrending.setAdapter(adapter_empty_trending);
                });
            }else{
                return;
            }
        });
    }


    private void viewArticle(String id) {
        onArticleRequest();
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);

        apiService.getArticle(String.valueOf(unixTime), sf.getDeviceId(), id, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Article>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Article article) {

                        onArticleRequestSuccess();

                        Intent intent = new Intent();

                        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
                        intent.putExtra("id", id);
                        intent.putExtra("title", article.getTitle());
                        intent.putExtra("photo", article.getCreative());
                        intent.putExtra("type", article.getType());
                        intent.putExtra("pubdate", article.getPubdate());
//                        intent.putExtra("content1", article.getContent().get(0).toString());
//                        String content1 = article.getContent();

                        intent.putExtra("author", article.getByline());
                        //NO CONTENT2 and Image1: TRENDING: NO USED for now
                        startActivityForResult(intent, Global.NEWS_ARTICLE);
                    }
                    @Override
                    public void onError(Throwable e) {
                        hideLoader();
                        onArticleRequestError();
                        String error_msg = Global.getResponseCode(e);
                        Toast.makeText(getContext().getApplicationContext(), error_msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void checkVideoLink (String link) {

        if (link != null) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(link));
                startActivity(intent);
            }catch(Exception e){
                Log.d("TAGGG", "Video3Link "+ e.getMessage());
                return;
            }

        }else{
            return;
        }
    }


    public void checkImageLink(String link){

        if (link != null) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(link));
                startActivity(intent);
            }catch(Exception e){
                Log.d("TAGGG", "ImageDoubleLink "+ e.getMessage());
                return;
            }
        }else{
            return;
        }
    }


    public void displayFullview (String video) {

        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//DO NOT FORGET THIS EVER
            intent.setDataAndType(Uri.parse(video), "video/mp4");
            startActivity(intent);
        }catch(Exception e){
            Log.d("TAGGG", "Video6Link "+ e.getMessage());
            return;
        }

    }


}
