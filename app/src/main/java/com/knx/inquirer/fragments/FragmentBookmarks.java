package com.knx.inquirer.fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.R;
import com.knx.inquirer.rvadapters.RVAdapterBookmarks;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodels.BookmarkViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FragmentBookmarks extends Fragment {
    private static final String TAG = "FragmentBookmarks";

    private ConstraintLayout mClEmptyContainer;
    private TextView mBookmarkEmpty;
//    private ProgressBar pLoading;
    private RecyclerView rvBookmarks;
    private LinearLayoutManager layoutManager;
    private RecyclerView.Adapter adapter;  //RecyclerView.Adapter as adapter is a STATIC VALUE that canbe  Changed

//    public ArrayList<BookmarksModel> bookmarks = new ArrayList<>();
    public List<BookmarksNewModel> bookmarks = new ArrayList<>();

    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DatabaseHelper helper;

    private int ccount=0, tcount=0;

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    public FragmentBookmarks() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        helper = new DatabaseHelper(getContext());
        if(!EventBus.getDefault().isRegistered(FragmentBookmarks.this)) EventBus.getDefault().register(FragmentBookmarks.this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sf =  new SharedPrefs(getContext());
        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_bookmarks, container, false);
        initViews(view);
        getBookmarkList();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void initViews(View view) {

//        pLoading = view.findViewById(R.id.pLoading);
//        pLoading.setVisibility(View.GONE);
        mClEmptyContainer = view.findViewById(R.id.bookmarkClEmptyContainer);
        mBookmarkEmpty = view.findViewById(R.id.bookmarkEmptyIconText);

        rvBookmarks = view.findViewById(R.id.recyclerview);
        rvBookmarks.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        rvBookmarks.setLayoutManager(layoutManager);
        //Scroll Listener
//        adapter = new RVAdapterPermission_Bookmarks(FragmentBookmarks.this);
        adapter = new RVAdapterBookmarks(getContext(), bookmarks, FragmentBookmarks.this);
        rvBookmarks.setAdapter(adapter); //Setting Adapter
//        adapter.notifyDataSetChanged(); //call only when there are dataInTheAdapter notifyDataSetChanged

        rvBookmarks.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//            //To get the total Number of items visible on the screen
                ccount = layoutManager.getChildCount();
//            //To get the total items loaded in the RecyclerView
                tcount = layoutManager.getItemCount();
                isListScrollable(ccount,tcount);
            }
        });
    }

    private boolean isListScrollable(int cc, int tc){
        Global.isItemScrollable = false;
        Log.d(TAG, "isListScrollable showBottomNav "+cc+" == "+tc);
        if(cc==tc){
            Global.isItemScrollable = true;
        }
        return Global.isItemScrollable;
    }

    public void shareArticle(String titleStr, String typeStr, String idStr){
            ApiService apiService = ServiceGenerator.createService(ApiService.class);
            String hashed = Global.Generate32SHA512(sf.getDailyKey() +"inq" + sf.getDeviceId() + "-megashare/" + idStr);
            apiService.getShareLink(sf.getDeviceId(), idStr, typeStr, hashed)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<String>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(String link) {

                            Intent emailIntent = new Intent(Intent.ACTION_SEND);
                            emailIntent.setData(Uri.parse("mailto:"));
                            emailIntent.setType("text/plain");

                            emailIntent.putExtra(Intent.EXTRA_TEXT, titleStr + "\nvia Inquirer Mobile: " + link);

                            try {
                                startActivity(Intent.createChooser(emailIntent, "Share with..."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(getContext(), "There is no share client installed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError: "+e.getMessage());
                        }
                    });
    }

    public void shareArticle2(String titleStr, String typeStr, String idStr){

        ApiService apiService = ServiceGenerator.createService(ApiService.class);
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megashare/" + idStr);

        apiService.getShareLink2(String.valueOf(unixTime), sf.getDeviceId(), idStr, typeStr, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {

                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");

                        emailIntent.putExtra(Intent.EXTRA_TEXT, titleStr + "\nvia Inquirer Mobile: " + link);

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(getContext(), "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: "+e.getMessage());
                    }
                });
    }

    /**NEW ADDITIONAL NewDesign*/
    private void getBookmarkList() {
        BookmarkViewModel bookmarkViewModel; //Creating a view_model object
        bookmarkViewModel = ViewModelProviders.of(this).get(BookmarkViewModel.class); //initialize view_model PROVIDER
        bookmarkViewModel.getBookmarkList().observe(this, data->{ //Accessing view_model METHOD
            Global.bookmarkIdList.clear(); //
            Log.d(TAG, "getBookmarkList: called size DATA "+ data.size());
            //Get the BOOKMARK_ID only: Will use for marking those already bookmarked
            for(int x = 0; x <data.size(); x++) {
                Global.bookmarkIdList.add(data.get(x).getNewsId()); //add to HashSet
            }
            //Get the BOOKMARKLIST: Will use as the DATA in the ADAPTER
            bookmarks.clear(); //Always Clear the List so that the ADAPTER will refresh when there is a DELETION or CHANGES
            bookmarks.addAll(data);
            Gson gson = new Gson();
            Log.d(TAG, "getBookmarkList:bookmarks "+ gson.toJson(bookmarks));
            if(bookmarks.size()==0){
                mClEmptyContainer.setVisibility(View.VISIBLE);
                mBookmarkEmpty.setTextColor(getContext().getResources().getColor(R.color.empty_rv_text));
            }else mClEmptyContainer.setVisibility(View.GONE);
            adapter.notifyDataSetChanged(); //call only when there are dataInTheAdapter notifyDataSetChanged


            //Controller: What ADAPTER will be used/displayed
//            checkBookmarks();
            //DEBUGGING
//            Gson gson = new Gson();
//            String bookmarks = gson.toJson(Global.bookmarkIdList);
////            String bookmarkData = gson.toJson(data);
//            Log.d(TAG, "getBookmarkList: bookmark_id: "+bookmarks);
        });
    }

        public void viewArticle(int id, String title, String mainImage, String type, String date, String content1, String image1, String insertTitle, String insertType, String insertNewsId, String insertContent,String content2, String author, String custom, String bannerSectionId, String sectionName){
//        public void viewArticle(int id, String title, String mainImage, String type, String date, String content1, String image1, String content2, String author, String custom, String bannerSectionId, String sectionName){
        Intent intent = new Intent();
        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebBookmarksActivity");
        intent.putExtra("id", id);
        intent.putExtra("title", title);
        intent.putExtra("photo", mainImage);
        intent.putExtra("type", type);
        intent.putExtra("pubdate", date);
        intent.putExtra("content1", content1);
        intent.putExtra("image1", image1);
        intent.putExtra("content2", content2);
        intent.putExtra("author", author);
        intent.putExtra("custom", custom);
        intent.putExtra("adbanner", bannerSectionId);
        intent.putExtra("sectionName", sectionName);
        intent.putExtra("insertTitle", insertTitle);
        intent.putExtra("insertType", insertType);
        intent.putExtra("insertNewsId", insertNewsId);
        intent.putExtra("insertContent", insertContent);
        startActivityForResult(intent, Global.NEWS_ARTICLE);
    }

    //NEW ADDITIONAL new_design
    public void deleteOneBookmark(int newsId, int position) {
        helper.deleteOneBookmark(newsId);
        Toast.makeText(getActivity().getApplicationContext(), "Bookmark deleted.", Toast.LENGTH_SHORT).show();
    }

//    @Override
//    public void setMenuVisibility(boolean menuVisible) {
//        super.setMenuVisibility(menuVisible);
//        //REFRESH Fragment
//        if(menuVisible) getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
//    }
}
