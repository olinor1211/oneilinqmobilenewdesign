package com.knx.inquirer.fragments;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.knx.inquirer.R;

public class FragmentFeedback extends Fragment {
    private static final String TAG = "FragmentAppSettings";
    private TextView mNightMode;
    public FragmentFeedback() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        EventBus.getDefault().register(FragmentAppSettings.this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_feedback, container, false);
        initViews(view);
        Log.d(TAG, "onCreateView: CALLED");
        return view;
    }

    public void initViews(View view) {
        mNightMode = view.findViewById(R.id.appSettingNightMode);
    }
}
