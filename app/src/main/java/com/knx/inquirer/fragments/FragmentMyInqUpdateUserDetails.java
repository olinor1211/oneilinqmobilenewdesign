package com.knx.inquirer.fragments;

import android.app.DatePickerDialog;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.knx.inquirer.R;
import com.knx.inquirer.models.TagsModel;
import com.knx.inquirer.rvadapters.AdapterPreferedSections;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryMenu;
import com.knx.inquirer.viewmodels.MenuViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyInqUpdateUserDetails extends Fragment{
    private final String TAG = "FragMyInqUpdateDetails";

    private Button mUpdate;
    private Spinner mSpinner;
    private EditText mName, mBdate, mEmail;
    private TextView mChangePass;

    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    private RecyclerView rvPrefSection;
    private ProgressBar mLoadingSections;

    private List<TagsModel> tagsList;
    private JSONArray selectedSectionsList;
    private AdapterPreferedSections adapter;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Global.LoaderDialog loaderDialog;
    private ApiService apiService;

    public FragmentMyInqUpdateUserDetails() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_my_inq_update_user_details, container, false);
        sf =  new SharedPrefs(getContext());
        apiService = ServiceGenerator.createService(ApiService.class);
        loaderDialog = new Global.LoaderDialog();

        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);

        initViews(view);
        spinnerSettings(view);
        calendarSettings();
        displayUserDetails();
        fetchSections();
        return view;
    }


    private void calendarSettings(){
        myCalendar = Calendar.getInstance();
        myCalendar.add(Calendar.YEAR, -10); //Goes 10 Year Back in time
        date = (view1, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        mBdate.setText(sdf.format(myCalendar.getTime()));
        listenFieldState();
    }

    private void listenFieldState() {
        Global.regName = mName.getText().toString().trim();
        Global.regGender  = mSpinner.getSelectedItem().toString().trim();
        Global.regEmail  = mEmail.getText().toString().trim();
        Global.regBdate  = mBdate.getText().toString().trim();
        if(Global.regName.isEmpty()&&Global.regGender.isEmpty()&&Global.regEmail.isEmpty()&&Global.regBdate.isEmpty()){
            mUpdate.setEnabled(false);
            mUpdate.setBackgroundResource(R.drawable.round_button_gray);
        }else{
            mUpdate.setEnabled(true);
            mUpdate.setBackgroundResource(R.drawable.round_button_blue);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    private void initViews(View view) {
        rvPrefSection = view.findViewById(R.id.rvPrefSectionUpdate);
        mLoadingSections = view.findViewById(R.id.pbPrefSectionLoadingUpdate);
        mName = view.findViewById(R.id.etNameUpdate);
        mBdate = view.findViewById(R.id.etBdateUpdate);
        mEmail = view.findViewById(R.id.etEmailUpdate);
        mSpinner = view.findViewById(R.id.spGenderUpdate);
        mChangePass = view.findViewById(R.id.tvChangePassUpdate);
        mUpdate = view.findViewById(R.id.btnUpdate);

        mUpdate.setOnClickListener(view1 -> verify());
        mChangePass.setOnClickListener(view1 -> EventBus.getDefault().post("changePass"));

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),3,RecyclerView.VERTICAL,true);
        rvPrefSection.setHasFixedSize(true);
        rvPrefSection.setLayoutManager(gridLayoutManager);//

        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                listenFieldState();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        });
        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                listenFieldState();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        });

        mBdate.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog =  new DatePickerDialog(getActivity(), date,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        });
    }

    private Boolean isEmpty(EditText et) {
        return et.getText().toString().trim().length()==0;
    }

    private void spinnerSettings(View view) {
        ArrayList genderArrayList = new ArrayList();
        genderArrayList.add("Male");
        genderArrayList.add("Female");
        genderArrayList.add("LGBTQ");
        genderArrayList.add("Don't want to say");

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_textview, genderArrayList){

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                return v;
            }
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                v = super.getDropDownView(position, null, parent);
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };

        genderAdapter.setDropDownViewResource(R.layout.spinner_textview_dropdown);
        mSpinner.setAdapter(genderAdapter);

    }

    private void verify() {
        if (Global.regName.length() <= 3) Toast.makeText(getContext().getApplicationContext(), "Name must be at least 4 characters long.", Toast.LENGTH_SHORT).show();
        else if(!Patterns.EMAIL_ADDRESS.matcher(Global.regEmail).matches()) Toast.makeText(getContext().getApplicationContext(), "Invalid email address.", Toast.LENGTH_SHORT).show();
        else  getUpdated();
    }

    private void getUpdated() {
        Global.regBdate = mBdate.getText().toString().trim(); //
        if(Global.isNetworkAvailable(getContext())){
            //Saving Tags Menu in the List selections
            List<Integer> selections = new ArrayList<>(); //new: Integer List
            selectedSectionsList = new JSONArray(); //Clearing JsonArray
            if(tagsList.size() > 0){
                for (TagsModel model : tagsList) {
                    if (model.isSelected()) { //Filter only the Selected Tags
                        selections.add(model.getId());  //Add Tags ID to the List string //selections.add(String.valueOf(model.getId()));
                        try {
                            JSONObject jsonObject = new JSONObject(); //Creating JsonObject structure
                            jsonObject.put("id", model.getId());
                            jsonObject.put("title", model.getTitle());
                            selectedSectionsList.put(jsonObject); //then put the object to JsonArray
                        } catch (Exception e) {
                            Log.d(TAG, "getSubmitted Exception_Error: "+e.getMessage());
                        }
                    }
                }
            }

            Log.d(TAG, "selectedTagSections: List:" +selectedSectionsList.toString());

            if(selectedSectionsList.length() <= 0 ){
                Toast.makeText(getContext(), "Please select your preferred sections", Toast.LENGTH_SHORT).show();
            }else {
                String selectedTagSections = new Gson().toJson(selections);  //Convert List Selections to String comma-delimited //String selectedTagSections = new Gson().toJson(selections);
                Log.d(TAG, "selectedTagSections STRING: "+selectedTagSections);
                updateUser(Global.regName,Global.regGender,Global.regEmail,Global.regBdate,selectedTagSections);
            }

        }else{
            Global.ViewDialog alert = new Global.ViewDialog();
            alert.showDialog(getContext(), "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
        }
    }

    private void updateUser(String name, String gender, String email, String bdate, String selectedTagSections){
        String genderCode = changeGenderCode(gender);
        showLoader();
        mUpdate.setEnabled(false);
        apiService = ServiceGenerator.createService(ApiService.class);
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megaupdate");
        apiService.updateUserDetails(String.valueOf(unixTime), sf.getDeviceId(), sf.getDeviceKey().toString(),name , genderCode,  email, bdate, selectedTagSections,sf.getFirebaseToken(), hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonObject>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(JsonObject jsonObject) {
                        //SampleResponse: {"status":"ok","value":{"userinfo":{"mobile":"09091112244","name":"nnnggx","gender":"male","email":"gg@d.cx","bday":"2012-11-29","sections":"[8,18]"}}}
                        Log.d(TAG, "onSuccess UPDATE: "+jsonObject.toString());
                        hideLoader();
                        try {
                            if(jsonObject.get("status").getAsString().equals("ok")){
                                String mobile = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("mobile").getAsString();
                                String name = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("name").getAsString();
                                String gender = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("gender").getAsString();
                                String email = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("email").getAsString();
                                String bday = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("bday").getAsString();
                                String sections = jsonObject.get("value").getAsJsonObject().get("userinfo").getAsJsonObject().get("sections").getAsString();

                                sf.setMobile(mobile);
                                sf.setName(name);
                                sf.setEmail(email);
                                sf.setBdate(bday);
                                sf.setGender(gender);
                                sf.setCustomMenu(sections);
                                Toast.makeText(getContext(), "Updated successfully!", Toast.LENGTH_SHORT).show();
                                buttonDisable();
                                delayAndContinue();

                            }else{
                                mUpdate.setEnabled(true);
                                Toast.makeText(getContext(), "Error: "+jsonObject.get("value").getAsJsonArray().get(0), Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            Log.d(TAG, "onSuccess: Error JSon Structure: "+jsonObject.toString());
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey();
                        hideLoader();
                        mUpdate.setEnabled(true);
                        Log.d(TAG, "onError: Throwable Exception_Error: "+e.getMessage());
                    }
                });

    }

    public void fetchSections(){
        mLoadingSections.setVisibility(View.VISIBLE);
        MenuViewModel menuViewModel = ViewModelProviders.of(this, new ViewModelFactoryMenu(getActivity().getApplication(), compositeDisposable, sf)).get(MenuViewModel.class);
        menuViewModel.getMenu().observe(this, response-> {
            try{
                tagsList = new ArrayList<>(); //Clear first
                for(int x=0; x< response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().size(); x++){
                    tagsList.add(new TagsModel(
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("id").getAsInt(),
                            response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("title").getAsString(),
                            false, response.get("menu").getAsJsonObject().get("sections").getAsJsonArray().get(x).getAsJsonObject().get("hidden").getAsString()));
                }
                mLoadingSections.setVisibility(View.INVISIBLE);
            }catch(Exception e){
                Log.d(TAG, "fetchTags: ERROR: "+e.getMessage());
            }

            adapter = new AdapterPreferedSections(getContext(), tagsList);
            rvPrefSection.setAdapter(adapter);
        });

        menuViewModel.getMenuError().observe(this, isError -> {
            if(isError){
                mLoadingSections.setVisibility(View.VISIBLE);
            }
        });
    }

    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }


    private void displayUserDetails() {
        Log.d(TAG, "displayUserDetails: ");
        Log.d(TAG, "displayUserDetails: GENDER "+sf.getGender());
        mName.setText(sf.getName());
        mSpinner.setSelection(getGender(sf.getGender()));
        mEmail.setText(sf.getEmail());//
        mBdate.setText(sf.getBdate());
    }

    private int getGender(String gender) {
            int gdr = 0;
            switch (gender){
                case "male":
                    gdr = 0;
                    break;
                case "female":
                    gdr = 1;
                    break;
                case "lgbtq":
                    gdr = 2;
                    break;
                case "nan":
                    gdr = 3;
                    break;
            }
            return gdr;
    }

    private String changeGenderCode(String gender){
        switch(gender){
            case "Male":
                return "male";
            case "Female":
                return "female";
            case "LGBTQ":
                return "lgbtq";
            default:
                return "nan";
        }
    }

    public void showLoader() {loaderDialog.showDialog(getContext());}

    public void hideLoader() {
        loaderDialog.dismissDialog(getContext());
    }

    private void delayAndContinue() {
        Handler handler = new Handler();
        handler.postDelayed((Runnable) () -> {
            EventBus.getDefault().post("myInqUpdated");
        },2500);
    }

    private void buttonDisable() {
        mUpdate.setEnabled(false);
        mUpdate.setBackgroundResource(R.drawable.round_button_gray);
    }

    @Override
    public void onResume() {
        displayUserDetails();
        fetchSections();
        super.onResume();
    }
}