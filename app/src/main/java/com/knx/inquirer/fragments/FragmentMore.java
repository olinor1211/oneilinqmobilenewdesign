package com.knx.inquirer.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.knx.inquirer.BuildConfig;
import com.knx.inquirer.R;

public class FragmentMore extends BottomSheetDialogFragment {
    private static final String TAG = "FragmentMore";

    private ImageView imageEmail;
    private ImageView imageViber;
    private ImageView imageLine;
    private ImageView imageFB;
    private ImageView dialogClose;

    private TextView tvVersion;

    public static FragmentMore getInstance() {
        return new FragmentMore();
    }

    public FragmentMore() {
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        bottomSheetDialog.setOnShowListener(dialog -> {
//            FrameLayout bottomSheet = bottomSheetDialog.findViewById(android.support.design.R.id.design_bottom_sheet);
            FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);


            BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setSkipCollapsed(true);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        return bottomSheetDialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FRAGMENT_CALLED****");

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_more, container, false);
        initViews(view);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****LAYOUT_CALLED****");

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initViewEvents();
    }

    public void initViews(View view){

        tvVersion = view.findViewById(R.id.txtversion);

        tvVersion.setText("Version " + BuildConfig.VERSION_NAME);

        imageEmail = view.findViewById(R.id.imageEmail);

        imageViber = view.findViewById(R.id.imageViber);

        imageLine = view.findViewById(R.id.imageLine);

        imageFB = view.findViewById(R.id.imageFB);

        dialogClose = view.findViewById(R.id.ivdialogClose);

    }

    public void initViewEvents(){

        imageEmail.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("text/plain");

            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"digital@mymegamobile.com"});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Inquirer Mobile OS " + BuildConfig.VERSION_NAME + " SDK " + Build.VERSION.RELEASE + " " + Build.MANUFACTURER+ " " + Build.MODEL);
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");

            try
            {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
            catch (android.content.ActivityNotFoundException ex)
            {
                Toast.makeText(getActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
            }
        });

        imageViber.setOnClickListener(v -> {
            // TODO Auto-generated method stub

            String url = "http://bit.ly/inqmviber";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        });

        imageLine.setOnClickListener(v -> {
            // TODO Auto-generated method stub

            String url = "http://bit.ly/inqmline";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        });

        imageFB.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            String url = "http://inq.news/InqMFB";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        });



        dialogClose.setOnClickListener(v -> dismiss());

    }

}
