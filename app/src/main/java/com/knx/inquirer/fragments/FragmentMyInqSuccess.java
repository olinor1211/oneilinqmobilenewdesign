package com.knx.inquirer.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.knx.inquirer.R;

import org.greenrobot.eventbus.EventBus;

public class FragmentMyInqSuccess extends Fragment {

    private static final String TAG = "FragmentMyInqSuccess";
    private Button btnContinue;


    public FragmentMyInqSuccess() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myinq_success, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        btnContinue = view.findViewById(R.id.btnRead);
        btnContinue.setOnClickListener(v -> {
            EventBus.getDefault().post("myInqUpdated");
        });
    }

}
