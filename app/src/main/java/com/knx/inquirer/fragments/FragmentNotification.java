package com.knx.inquirer.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.Article;
import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.rvadapters.RVAdapterNotification;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryNotifs;
import com.knx.inquirer.viewmodels.NotifsViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNotification extends Fragment {
    private static final String TAG = "FragmentNotification";

    public RecyclerView rvNotifs;
    private LinearLayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private ApiService apiService;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
//    private Global.LoaderDialog loaderDialog;
    private DatabaseHelper helper;
    private NotifsViewModel notifsViewModel;
    public List<NotificationModel> notifs;
    private ConstraintLayout mClNoNotifs;
    private TextView mNotifEmpty;
    private TextView mMarkAllAsRead;

    private boolean hasLoadedOnce= false; // your boolean field
    //
    private int ccount=0; int tcount=0;

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("night_mode")){
            getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    public FragmentNotification() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(!EventBus.getDefault().isRegistered(FragmentNotification.this)) EventBus.getDefault().register(FragmentNotification.this);
        apiService = ServiceGenerator.createService(ApiService.class);  //
        helper = new DatabaseHelper(getContext());  //
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sf =  new SharedPrefs(getContext());
        if(sf.getNightMode()) inflater.getContext().setTheme(R.style.DarkTheme);
        else inflater.getContext().setTheme(R.style.LightTheme);

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_notification, container, false);
//        snackBarView = view;  //for snackBar
        initViews(view);
        showNotifs();
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****FragmentNotification_LAYOUT_CALLED****");
        return view;
    }

    //Deleting Items in the RV by Swiping. This ItemTouch need to attach in RecyclerView
    ItemTouchHelper.SimpleCallback itemTouchHelperCallback =  new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
            //can able to move item in different location in the Adapter list view
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

            int position = viewHolder.getAdapterPosition();
            int referenceId = notifs.get(position).getId();  //database ref id
            deleteOneNotif(referenceId); //DELETE ONE NOTIFICATION
            adapter.notifyItemChanged(position);  //to Refresh RV list adapter
            //Flag
            Global.isNotifItemDeleted = true;
            Toast.makeText(getContext(), "One notification is removed.", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onSwiped: Deleted notification id  "+ referenceId);

        }

        //disAble to swipe the specific viewHolder Class
        @Override
        public int getSwipeDirs(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            if(viewHolder instanceof RVAdapterNotification.DeleteViewHolder){return 0;}  //disabling by return 0
            return super.getSwipeDirs(recyclerView, viewHolder);
        }
    };

//    public void showLoader() {
//        loaderDialog.showDialog(getContext());
//    }

//    public void hideLoader() {
//        loaderDialog.dismissDialog(getContext());
//    }

    public void initViews(View view){

        mClNoNotifs = view.findViewById(R.id.clNoNotifs);
        mNotifEmpty = view.findViewById(R.id.tvNotifEmpty);
        mMarkAllAsRead = view.findViewById(R.id.tvMarkAllAsRead);

        rvNotifs = view.findViewById(R.id.recyclerview);
        rvNotifs.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),layoutManager.getOrientation()); //item decorations
        rvNotifs.setLayoutManager(layoutManager);
        rvNotifs.addItemDecoration(dividerItemDecoration); //item decorations
//        loaderDialog = new Global.LoaderDialog();

        //Scroll Listener
        rvNotifs.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//            //To get the total Number of items visible on the screen
                ccount = layoutManager.getChildCount();
//            //To get the total items loaded in the RecyclerView
                tcount = layoutManager.getItemCount();
                isListScrollable(ccount,tcount);
//                Log.d(TAG, "onScrolled:BOOKMARK_COUNT child "+ccount);
//                Log.d(TAG, "onScrolled:BOOKMARK_COUNT total_item "+tcount);
            }
        });

        //
        mMarkAllAsRead.setOnClickListener(view1 -> {
            helper.readAllNotification();
        });

        //Set TextColor
//        mMarkAllAsRead.setTextColor(getResources().getColor(R.color.blue));
//        if(sf.getNightMode()) mMarkAllAsRead.setTextColor(getResources().getColor(R.color.white));
//        else mMarkAllAsRead.setTextColor(getResources().getColor(R.color.blue));
    }



    private boolean isListScrollable(int cc, int tc){
        Global.isNotifItemScrollable = false;
        Log.d(TAG, "isListScrollable showBottomNav "+cc+" == "+tc);
        if(cc==tc){
            Global.isNotifItemScrollable = true;
        }
        return Global.isNotifItemScrollable;
    }

    //from a table
    private void showNotifs(){
        notifsViewModel = ViewModelProviders.of(this, new ViewModelFactoryNotifs(getActivity().getApplication(), compositeDisposable)).get(NotifsViewModel.class);
        notifsViewModel.getNotifications().observe(this, notificationModels -> {

            if(notificationModels.size() > 0) {
                mMarkAllAsRead.setVisibility(View.VISIBLE);
                mClNoNotifs.setVisibility(View.INVISIBLE);
                notifs = notificationModels;  //make global variable just to be able to access in the swipe
                adapter = new RVAdapterNotification(getContext(), notifs, FragmentNotification.this, helper);
//                new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvNotifs);  //attaching ItemTouch to RecyclerView
                rvNotifs.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
            }else{
                mMarkAllAsRead.setVisibility(View.INVISIBLE);
                mClNoNotifs.setVisibility(View.VISIBLE);
                mNotifEmpty.setTextColor(getContext().getResources().getColor(R.color.empty_rv_text));
//                adapter = (new RVAdapter_NoNotifs());
//                rvNotifs.setAdapter(adapter);
            }
        });
    }

    //deleting All notifs
    public void deleteNotifs(){
        helper.deleteAllNotifications();
    }

    //deleting ONE notifs
    public void deleteOneNotif(int referenceId){
        helper.deleteOneNotification(referenceId);
    }


    public void linkError(){
        Toast.makeText(getContext().getApplicationContext(), "Link error detected!", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "linkError called ");
        return;
    }


//    public void viewArticle(String id) {
//        showLoader();
//        long unixTime = System.currentTimeMillis() / 1000L;
//        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
//        apiService.getArticle(String.valueOf(unixTime), sf.getDeviceId(), id, hashed)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<Article>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        compositeDisposable.add(d);
//                    }
//
//                    @Override
//                    public void onSuccess(Article article) {
//
//                        hideLoader();
//                        Intent intent = new Intent();
//                        String content1 = article.getContent1().isEmpty() ? article.getContent2a() : article.getContent2b();
//                        String content2 = article.getContent2b();
//
//
////                        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebDeepLinkActivity");
//                        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");   //WebNewsActivity has counting of seconds
//                        intent.putExtra("id", id);
//                        intent.putExtra("title", article.getTitle());
//                        intent.putExtra("photo", article.getCreative());
//                        intent.putExtra("type", article.getType());
//                        intent.putExtra("pubdate", (article.getPubdate() != null ? article.getPubdate() : ""));
////                        intent.putExtra("content1", article.getContent().get(0).toString());
//                        intent.putExtra("content1", content1); //intent.putExtra("content1", article.getContent());
////                        intent.putExtra("image1", (article.getInsert() != null ? article.getInsert().getCreative(): ""));
//                        intent.putExtra("image1", "");
////                        intent.putExtra("content2", article.getContent().get(1).toString());
//                        intent.putExtra("content2",content2);  //String content2 = article.getContent2b();
//                        intent.putExtra("author", (article.getByline() != null ? article.getByline() : ""));
//                        intent.putExtra("custom", article.getCustom());
//                        //Get the Related Article
//                        if(article.getRelated()!=null){
//                            Global.relatedArticles.clear();
//                            Global.relatedArticles.addAll(article.getRelated());
//                        }
//
//
//                        //Get the SuperShare Details
//                        startActivityForResult(intent, Global.NEWS_ARTICLE);
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        hideLoader();
//                        Toast.makeText(getContext(), "Unexpected error detected!", Toast.LENGTH_SHORT).show();
//                        Log.d(TAG, "onError: viewArticle called ");
//                        return;
////                        String error_msg = PrimeGlobal.getResponseCode(e);
////                        Toast.makeText(getContext(), error_msg, Toast.LENGTH_SHORT).show();
//                    }
//                });
//    }

    public void viewArticle(String id) {
//        showLoader();
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
        apiService.getArticle2(String.valueOf(unixTime),sf.getDeviceId(), id, hashed)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try{
//                            hideLoader();
                            String content1="";
                            String content2="";
                            String insertNewId="";
                            String insertType="";
                            String insertTitle="";
                            String insertImage="";
                            String insertContent="";
                            //Sometimes NO Section_ID and DATE: Occur on ADS Type1
                            String section_id="", pubdate="", author="", title="", photo="", type="";
                            if(response.body().has("section_id")) section_id = !response.body().get("section_id").isJsonNull() ? response.body().get("section_id").getAsString() : "";
                            if(response.body().has("pubdate")) pubdate = !response.body().get("pubdate").isJsonNull() ? response.body().get("pubdate").getAsString() : "";
                            if(response.body().has("byline")) author = !response.body().get("byline").isJsonNull() ? response.body().get("byline").getAsString() : "";
                            if(response.body().has("title")) title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
                            if(response.body().has("creative")) photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
                            if(response.body().has("type")) type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";

                            //Get the SuperShare: Check if has an INSERT (for SuperShare)
                            if(response.body().has("insert")){
                                //Check if CONTENT as an ARRAY
//                                content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
//                                content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
//                                insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
//                                insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
//                                insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
//                                insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
//                                insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
//                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");

                                if(response.body().has("content")){
                                    content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
                                    content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
                                }
                                if(response.body().get("insert").getAsJsonObject().has("id")) insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("type"))  insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("title")) insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("creative")) insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("content"))  insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
                            }else {
                                //Check if CONTENT as a STRING
                                if(response.body().has("content")) content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
                            }
                            //Get the Related Article: First Check if has RelatedArticle
                            Global.relatedArticles.clear();
                            if(response.body().has("related")){
                                Log.d(TAG, "onResponse: CONTENT_  test1");
                                for(int x=0; x<response.body().get("related").getAsJsonArray().size();x++){
                                    Global.relatedArticles.add(response.body().get("related").getAsJsonArray().get(x).getAsJsonObject());
                                }
                            }
                            //INTENT
                            Intent intent = new Intent();
                            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
                            intent.putExtra("id", id);
                            intent.putExtra("sectionId", section_id);
                            intent.putExtra("title", title);
                            intent.putExtra("photo", photo);
                            intent.putExtra("type", type);
                            intent.putExtra("pubdate", pubdate);
                            intent.putExtra("content1", content1);
                            intent.putExtra("image1", insertImage);
                            intent.putExtra("insertTitle", insertTitle);
                            intent.putExtra("insertType", insertType);
                            intent.putExtra("insertNewsId", insertNewId);
                            intent.putExtra("insertContent", insertContent);
                            intent.putExtra("content2", content2);
                            intent.putExtra("author", author);
                            intent.putExtra("custom", ""); //No USED
                            intent.putExtra("adbanner", ""); //No USED
                            startActivityForResult(intent, Global.NEWS_ARTICLE);
                        }catch (Exception e){
                            Log.d(TAG, "onResponse: viewArticle Exception: "+ e.getCause());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        getDailyKey();
                        String error_msg = Global.getResponseCode(t);
                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
                            toastMessage(TAG+": onFailure: "+ t.getMessage()); // //Display in developer
                        }else{
                            toastMessage(error_msg); // //Display in user
                        }

                        Log.d(TAG, "onError: onArticleRequestError "+ t.getMessage());
                    }
                });

    }

    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    public void toastMessage(String message) {
        if(Global.toast!=null) Global.toast.cancel(); //Remove First the existing toast
        Global.toast = Toast.makeText(getContext().getApplicationContext(), message, Toast.LENGTH_SHORT); Global.toast.show(); //Show NEW TOAST
    }

}




//sample test
//    public void insertNotif(){
//        NotificationModel nmodel = new NotificationModel("https://combo.staticflickr.com/ap/build/images/refencing-announcement/bird2.jpg", "https://www.flickr.com/lookingahead", "God is good ",  "God's Love and Faithfulness. Jesus the Son of God. The God's favor. ", "September 09, 2019 02:53pm", 0);
//        helper.insertNotification(nmodel);
//    }

////                newsmodel.add(new NewsModel("49301", "5", "Jesus loves me !", "https://combo.staticflickr.com/ap/build/images/refencing-announcement/bird2.jpg", "September 09, 2019 02:53pm","https://www.flickr.com/lookingahead", "God's Love", "sports"));