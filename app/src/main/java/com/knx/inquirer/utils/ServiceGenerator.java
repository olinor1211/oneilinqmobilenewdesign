package com.knx.inquirer.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.knx.inquirer.models.Article;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
//import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ServiceGenerator {
    private static final String TAG = "ServiceGenerator";

    private static Context context;
    private static final String BASE_URL = "https://inq.inq.news/";  //OLD "https://inq.inq.news/api/";
//    private static final String BASE_URL_2 = "https://inqcms.inq.news/api/";

    //GSON new implementation ..REMOVED .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
    private static Gson gson = new GsonBuilder() //
            .serializeNulls()
            .setLenient()
            .registerTypeAdapter(Article.class, new Article.DataStateDeserializer()) //For Deserializer of content DataType, sometimes String or Array
//            .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
            .create();

//    private static Gson gson = new GsonBuilder() //
//            .serializeNulls()
//            .setLenient()
////
////            .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
//            .create();

    //OKHTTP_CLIENT
    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .connectTimeout(15, TimeUnit.SECONDS) //10
            .writeTimeout(15, TimeUnit.SECONDS) //10
            .readTimeout(15, TimeUnit.SECONDS) //10
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build();


    //RETROFIT BUILDER new implementation .. //ADDING HttpLoggingInterceptor in the  Global.getUnsafeOkHttpClient()
    private static Retrofit.Builder builder = new Retrofit.Builder()  //
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create()) //replacement RxJava3
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())  //addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(Global.getUnsafeOkHttpClient());//.client(okHttpClient);//.client(Global.getUnsafeOkHttpClient()); //TRIED USING this=.client(okHttpClient), Failed Certificate

    //separate the .build
    private static Retrofit retrofit = builder.build();

    //2
//    private static Retrofit retrofit2 = builder2.build();

    //separate the .create ..put class as a param in a createService method  ...For inq.inq.news
    public static <S> S createService(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

}






//2
//    private static Retrofit.Builder builder2 = new Retrofit.Builder()  //
//            .baseUrl(BASE_URL_2)
//            .addConverterFactory(ScalarsConverterFactory.create())
//            .addConverterFactory(GsonConverterFactory.create(gson))
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//            .client(PrimeGlobal.getUnsafeOkHttpClient());



//2
//    public static <S> S createService2(
//            Class<S> serviceClass) {
//        return retrofit2.create(serviceClass);
//    }

//REMOVED IN New Implementation  Jan2021
//class
//    public static class NullStringToEmptyAdapterFactory<T> implements TypeAdapterFactory {
//        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
//
//            Class<T> rawType = (Class<T>) type.getRawType();
//            if (rawType != String.class) {
//                return null;
//            }
//            return (TypeAdapter<T>) new StringAdapter();
//        }
//    }
//
//    //class
//    public static class StringAdapter extends TypeAdapter<String> {
//        public String read(JsonReader reader) throws IOException {
//            if (reader.peek() == JsonToken.NULL) {
//                reader.nextNull();
//                return "";
//            }
//            return reader.nextString();
//        }
//        public void write(JsonWriter writer, String value) throws IOException {
//            if (value == null) {
//                writer.nullValue();
//                return;
//            }
//            writer.value(value);
//        }
//    }
