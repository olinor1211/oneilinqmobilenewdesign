package com.knx.inquirer.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.knx.inquirer.models.DatabaseModel;
import com.knx.inquirer.models.DatabaseModel_CustomNews;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHelperUtils extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="inqmobile";
    private static final int DATABASE_VERSION = 3;

    private static final String NEWS_TABLE = "tblnews";
    private static final String NEWS_QUERY = "create table " + NEWS_TABLE +"(newsid TEXT, title TEXT, section TEXT, pubdate TEXT, photo TEXT, newstype TEXT, link TEXT, label TEXT)";

    private static final String CUSTOMNEWS_TABLE = "tblcustomnews";
    private static final String CUSTOMNEWS_QUERY = "create table " + CUSTOMNEWS_TABLE +"(newsid TEXT, title TEXT, section TEXT, pubdate TEXT, photo TEXT, newstype TEXT, link TEXT, label TEXT)";

    Context context;

    public DatabaseHelperUtils(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(NEWS_QUERY);
        db.execSQL(CUSTOMNEWS_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        switch(oldVersion) {
            case 1:
                db.execSQL("DROP TABLE IF EXISTS " + NEWS_TABLE);
                // we want both updates, so no break statement here...
            case 2:
                db.execSQL("DROP TABLE IF EXISTS " + CUSTOMNEWS_TABLE);

            case 3:
                db.execSQL("DROP TABLE IF EXISTS " + NEWS_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + CUSTOMNEWS_TABLE);
        }
            // Create tables again
            onCreate(db);

    }

/* Retrieve  data from database */
    public List<DatabaseModel> getDataFromDBNews(Context context)
    {
        List<DatabaseModel> modelList = new ArrayList<>();
        String query = "SELECT * FROM " + NEWS_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                DatabaseModel model = new DatabaseModel();
                model.setNewsid(cursor.getString(0));
                model.setTitle(cursor.getString(1));
                model.setSection(cursor.getString(2));
                model.setPubdate(cursor.getString(3));
                model.setPhoto(cursor.getString(4));
                model.setNewstype(cursor.getString(5));
                model.setNewslink(cursor.getString(6));
                model.setLabel(cursor.getString(7));

                modelList.add(model);
            }while (cursor.moveToNext());
        }

        Log.d("News_DB_Data", modelList.toString());

        return modelList;
    }

    public List<DatabaseModel_CustomNews> getDataFromDBCustomNews(Context context)
    {
        List<DatabaseModel_CustomNews> cmodelList = new ArrayList<>();
        String query = "SELECT * FROM " + CUSTOMNEWS_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                DatabaseModel_CustomNews model = new DatabaseModel_CustomNews();
                model.setNewsid(cursor.getString(0));
                model.setTitle(cursor.getString(1));
                model.setSection(cursor.getString(2));
                model.setPubdate(cursor.getString(3));
                model.setPhoto(cursor.getString(4));
                model.setNewstype(cursor.getString(5));
                model.setLink(cursor.getString(6));
                model.setLabel(cursor.getString(7));

                cmodelList.add(model);
            }while (cursor.moveToNext());
        }

        Log.d("Custom_News_Data", cmodelList.toString());

        return cmodelList;
    }

    /*delete a row from database*/

    public void deleteARecord(String newsid)
    {
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete(NEWS_TABLE, "_id " + " = ?", new String[] { newsid });
        db.close();

    }

    public void deleteNewsTable()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ NEWS_TABLE);
        db.close();

    }

    public void deleteCustomNewsTable()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ CUSTOMNEWS_TABLE);
        db.close();

    }

}
