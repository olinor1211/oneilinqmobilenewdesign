package com.knx.inquirer.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

public class CustomViewPager extends ViewPager {

    private boolean isPagingEnabled = false;

    //constructor1  ..context
    public CustomViewPager(Context context) {
        super(context);
    }

    //constructor2  ..context and AttributeSet
    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //isPagingEnabled is false ..MotionEvent  ..onTouchEvent
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onTouchEvent(event);
    }

    ///isPagingEnabled is false ..MotionEvent  ..onInterceptTouchEvent
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event);
    }

    //setPagingEnabled ??  ..isPagingEnabled = b
    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }

    //setCurrentItem ??  item and smoothScroll
    @Override
    public void setCurrentItem(int item, boolean smoothScroll) {
        super.setCurrentItem(item, false);
    }

    //setCurrentItem ??  item only
    @Override
    public void setCurrentItem(int item) {
        super.setCurrentItem(item, false);
    }
}

