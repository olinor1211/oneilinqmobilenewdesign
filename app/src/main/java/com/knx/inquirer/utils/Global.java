package com.knx.inquirer.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import com.google.gson.JsonSyntaxException;
import com.knx.inquirer.BuildConfig;
import com.knx.inquirer.R;
import com.knx.inquirer.inqprime.ui.InqPrimeMainActivity;
import com.knx.inquirer.inqprime.ui.PrimeAccountInfo;
import com.knx.inquirer.inqprime.ui.PrimeMembershipBilling;
import com.knx.inquirer.models.MenuAd;
import com.knx.inquirer.models.Related;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.models.Sections;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
////import okhttp3.logging.HttpLoggingInterceptor;
//import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.HttpException;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class Global {
	private static final String TAG = "Global";

	public static boolean isItemClicked = false, isDoneLoadingCustomSection = true, isConnected;
	public static int notifCount = 0;
	public static String dailyKey;

	public static String syndicationKey = "273874"; //For InqMobile from DM content manager
	public static String appPackageName = "com.knx.inquirer"; //
	public static final String PASS_CHECK_NOTIF_DEVICE_TOKEN = "18sjdi82ns9";
	public static final String PASS_STORE_NOTIF_DEVICE = "t59hu82j3";  //checkNotificationSettings
	public static String passStoreUserReadTime = "hBbHdZHkq3Np";
	public static String passStoreUserReadTimeList = "hBbHdZHkq3Np";
	public static final String NEGATIVE_NOTIF_PERMISSION = "N";
	public static final String POSITIVE_NOTIF_PERMISSION = "Y";

	public static String regId;
	public static String deviceId;

	public static String password;
	public static String password2;
	public static String md5param;
	public static String androidId;
	public static double versionCode;
	public static String myVersion;
	public static int sdkVersion;
	public static String manufacturer;
	public static String model;

	public static String url;
	public static String sectionUrl;

	public final static int NEWS_ARTICLE = 22;
	public final static int NEWS_ARTICLE_INQ = 20;
	public final static int NOTIF_ARTICLE = 11;
	public static boolean isArticleViewed = false;

	public static String searchStr;
	public static String aidStr;
	public static int versionS = 0;
	public static int cacheSw = 0;

	public static String latitude = "";
	public static String longitude = "";
	public static String location = "";

	public static Context context;

	public static boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	public static boolean isSDSupportedDevice = Environment.isExternalStorageRemovable();

	public static boolean isVisibleFragmentMyInqHolder = false;
//	public static List<Related> relatedArticles = new ArrayList<>();
	public static ArrayList<JsonObject> relatedArticles = new ArrayList<>();
	public static ArrayList<JsonObject> relatedArticles2 = new ArrayList<>();

	public static ArrayList<String> powerUrlArrayList;
	public static ArrayList<String> powerImageArrayList;

	public static int rotateSw = 0;
	public static int cameraSw = 0;
	public static int section, section_custom;

	public static String opt_id;

	public static String INQ_SHARED_PREF = "INQ_SF";

	private Activity activity;
	private ImageView im;

	//podcast webNewsAct
	public static Boolean isPodPaused = false;
	//podcast
	public static Boolean isPodPlayed = false;
	//Banner Ad
	public static List<Sections> sectionsBanner = new ArrayList<>();
	public static List<String> bannerList = new ArrayList<>();
	//adnroidx
	public static JsonArray sectionsBanner2;
	public static String getMenu3ErrorBody="";
	public static String reportPhotoPath="";

	//for DeepLink=>WebNews
	public static String sectionId = "";
	public static String httpLinkUrl = "";
	public static String idStr = "";
	public static String titleStr = "";
	public static String imageStr = "";
	public static String typeStr = "";
	public static String pubdateStr = "";
	public static String contentStr = "";
	public static String newImage1tStr = "";
	public static String insertTitle = "";
	public static String insertType = "";
	public static String insertNewsId = "";
	public static String insertContent = "";
	public static String contentStr2 = "";
	public static String authorStr = "";
	public static String customStr = "";
	public static String adbanner = "";
	public static final String DNAME = "inqm";
	public static final String DEMAIL = "inqm@ya.com";
	public static final String DMOBILE = "09091211121";

	public static boolean isMainActivityOpen = false;
	public static boolean isRetry = false;
	public static boolean isInsertClicked = false;

	public static boolean isItemScrollable = false;
	public static boolean isNotifItemScrollable = false;
	public static boolean isNotifItemDeleted = false;
	/**NEW_DESIGN*/
	public static String sectionNews = "#004c87";
	public static String sectionSports = "#d51e29";
	public static String sectionEntertainment = "#f76238";
	public static String sectionTechnology = "#48cdf4";
	public static String sectionGlobalNews = "#004c87";
	public static String sectionBusiness = "#51b253";
	public static String sectionLifestyle = "#b24179";
	public static String sectionOpinion = "#9db9cc";
	public static String sectionProperty = "#e8aa01";
	public static boolean isBookmark = false;
	public static int bookmarkPosition = -1;
	public static HashSet<Integer> bookmarkIdList = new HashSet<>(); //HashSet= does not accept SAME VALUE
	public static Toast toast; //
	public static String EMAIL="";
	public static String PASSWORD="";
	public static String GMAIL_PACKAGE_NAME="";
	public static String GMAIL_COMPOSER_ACTIVITY="";
	public static boolean isMyInqEditSectionUpdated = false;
	public static String sectionName="";
	public static long lastClickTime = 0;
	public static final String RELATED_ARTICLE = "relatedArticle";
	public static boolean isRelatedArticle = false;
	public static final String NIGHT_MODE = "<body style='margin:0;padding:0;color:white;background-color:black;'>";
	public static final String NORMAL_MODE =  "<body style='margin:0;padding:0;'>";

	public static final String AGREE_DARK = "<span style=\"color:white\" >I agree to the </span><a href='http://inq.news/MMTandC'>Terms and Conditions </a> <span style=\"color:white\"> and </span> <a href='http://inq.news/privacy'> Privacy Policy </a>";
	public static final String AGREE_NORMAL = "<span style=\"color:black\" >I agree to the </span>";
//	public static final String AGREE_NORMAL = "<span style=\"color:black\" >I agree to the </span><a href='http://inq.news/MMTandC'>Terms and Conditions </a> <span style=\"color:black\"> and </span> <a href='http://inq.news/privacy'> Privacy Policy </a>";
    public static ArrayList<Section> sectionList = new ArrayList<>();
	public static boolean isMyInqUpdated = false;
	public static double currentVersion = 0.00;

	public static final String LINK_VIBER_PROPERTY = "https://inq.news/inqpropertycommunity";
	public static final String LINK_VIBER_BUSINESS = "https://inq.news/inqbusinesscommunity";
	public static final String LINK_VIBER_LIFESTYLE = "https://inq.news/inqlifestylecommunity";
	public static final String LINK_VIBER_SPORTS = "https://inq.news/inqsportscommunity";
	public static final String LINK_VIBER_ENTERTAINMENT = "https://inq.news/inqentertainmentcommunity";
	public static final String LINK_VIBER_COMMUNITY = "https://inq.news/vibercommunity";
	public static final String LINK_INQM_BOT = "https://inq.news/inqbot";
	public static final String LINK_INQM_FB = "https://inq.news/InqMFB";
	public static final String LINK_INQM_TWITTER = "https://inq.news/twitterinqm";

	public static String regPass ="";
	public static String regName ="";
	public static String regGender ="";
	public static String regEmail ="";
	public static String regBdate ="";
	public static String regMobile ="";
	public static String regNotify ="";

	public static void GenerateHashKey(String base)
	{
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}

			Global.password = hexString.toString();
		}
		catch(Exception e)
		{

		}
	}

	//
	public static boolean haveConnected(Context context) {
		//Vars
		boolean wifiIsConnected = false;
		boolean mobileDataIsConnected = false;
		NetworkInfo.State checkMobile;
		NetworkInfo.State checkWifi;
		ConnectivityManager connectManager;

		//init
		connectManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		checkMobile = connectManager.getNetworkInfo(0).getState();
		checkWifi = connectManager.getNetworkInfo(1).getState();

		if (checkMobile == NetworkInfo.State.CONNECTED) {
			mobileDataIsConnected = true;
		} else if (checkWifi == NetworkInfo.State.CONNECTED) {
			wifiIsConnected = true;

		}
		return wifiIsConnected || mobileDataIsConnected;
	}

	//network connected in <service
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public static Date convertToDate(String dateString){
		SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = formatter.parse(dateString);
		} catch (ParseException e) {
			Log.d(TAG, "convertToDate: Exception: "+e.getMessage());
		}
		return date;
	}

	public static String convertDateToString(Date date){
		SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString="";
		try {
			dateString = formatter.format(date);
		} catch (Exception e) {
			Log.d(TAG, "convertToDate: Exception: "+e.getMessage());
		}
		return dateString;
	}

//	        <service
//	android:name=".utils.NetworkSchedulerService"
//	android:exported="true"
//	android:permission="android.permission.BIND_JOB_SERVICE" />
//
//        <receiver android:name=".notification.ActionReceiver" />

	public static void GenerateHashKey2(String base)
	{
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}

			Global.password2 = hexString.toString();
		}
		catch(Exception e)
		{

		}
	}

//	static String GenerateHashKey3(String base)
//	{
//		String key = "";
//
//		try
//		{
//			MessageDigest digest = MessageDigest.getInstance("SHA-256");
//			byte[] hash = digest.digest(base.getBytes("UTF-8"));
//			StringBuffer hexString = new StringBuffer();
//
//			for (int i = 0; i < hash.length; i++) {
//				String hex = Integer.toHexString(0xff & hash[i]);
//				if(hex.length() == 1) hexString.append('0');
//				hexString.append(hex);
//			}
//
//			key = hexString.toString();
//		}
//		catch(Exception e)
//		{
//
//		}
//
//		return key;
//	}

	//generate sha 512
	public static String GenerateSHA512(String base)
	{
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}

			md5param = hexString.toString();


		}
		catch(Exception e)
		{

		}
		return md5param;
	}


//	public void GenerateAndroidIs()
//	{
//
//	}


/*
    public static String GetUrl(String url)
	{
        String result = "";

        try
        {
            HttpClient httpclient = new DefaultHttpClient(); // Create HTTP Client
            HttpGet httpget = new HttpGet(url); // Set the action you want to do
            HttpResponse response = httpclient.execute(httpget); // Executeit
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) // Read line by line
                sb.append(line + "\n");

            result = sb.toString(); // Result is here

            is.close(); // Close the stream
        }
        catch (Exception e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

		return result;
	}
*/

	//
//	public static void CreateDirectory(String dirStr) {
////		File theDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),"." + dirStr);
//		File theDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + dirStr);
//		// if the directory does not exist, create it
//		if (!theDir.exists()) {
//			theDir.mkdir();
//		}
//	}

	public static void CreateDirectory2(Context context, String dirStr) {

		File filePath = new File(context.getExternalFilesDir(""),dirStr); //getExternalFilesDir("") = No Built-in directory, just inside the Android/data/packageName instead
		if(!filePath.exists() && !filePath.isDirectory()){
			filePath.mkdir();
		}
	}

//	public static void CreateDirectory2(Context context, String dirStr) {
//		File filePath = new File(context.getExternalFilesDir(""),dirStr); //getExternalFilesDir("") = No Built-in directory, just inside the Android/data/packageName instead
//		if(!filePath.exists() && !filePath.isDirectory()){
//			filePath.mkdir();
//		}
//	}



/*	public static void CreateDirectory(Context c, String dirStr)
	{

		context = c;

		//If SD card exists
		if(isSDPresent && isSDSupportedDevice) {
			File theDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + dirStr);

			// if the directory does not exist, create it
			if (!theDir.exists()) {
				boolean result = theDir.mkdir();

				if (result) {
					Log.v("EXTERNALL_DIR_INQMOBILE", "success");
				}
			}
		//If SD card does not exists
		}else{
				//File theDir = new File(context.getFilesDir() + dirStr);
				File theDir = new File(Environment.getExternalStorageDirectory()+ dirStr);

				// if the directory does not exist, create it
				if (!theDir.exists()) {
					boolean result = theDir.mkdir();

					if (result) {
						Log.v("INTERNAL_DIR_INQMOBILE", "success");
					}
				}
		}
    }*/

	//
	public static void WriteFile(String dataStr, String fileName)
	{
		try
		{
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter out = new BufferedWriter(fw);
			out.write(dataStr);
			out.close();
		}
		catch (Exception e)
		{
			Log.d(TAG, "WriteFile: Exception_Error: "+e.getMessage());
		}
	}

	// return
	public static String ReadFile(String filePath)
	{
		String result = "";
		FileReader fr = null;
		try
		{
			fr = new FileReader(filePath);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while((s = br.readLine()) != null)
			{
				result = result + s;
				Log.d(TAG, "ReadFile: INQUIRER_FILE result ReadingFIle "+result);
			}
			fr.close();
		}
		catch (Exception e)
		{
			Log.d(TAG, "ReadFile: Exception_Error: "+e.getMessage());
		}

		return result;
	}

	//
	public static void DeleteFile(String fileStr)
	{
		File file = new File(fileStr);
		boolean deleted = file.delete();
	}

//	//delete bookmark boolean
//	public static boolean DeleteBookmark(String fileStr)
//	{
//		File file = new File(fileStr);
//		boolean deleted = file.delete();
//
//		return deleted;
//	}

	//delete bookmark boolean
	public static boolean DeleteBookmark2(Context context,  String fileStr)
	{
		File file = new File(context.getExternalFilesDir(""),".Inquirer/"+fileStr);
		boolean deleted = file.delete();
		return deleted;
	}


	public static String ConvertDate(String dateStr)
	{
		String newDate = "";

		Timestamp ts = Timestamp.valueOf(dateStr);
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
		newDate = sdf.format(ts);

		return newDate;
	}

//	static void UpdateStatistics(String data)
//	{
//		// 		File dbfile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/tmm/tmm.db" );
//		// 		SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(dbfile, null);
//		// 		System.out.println("Its open? "  + db.isOpen());
//
//		// 		String query = "insert into statistics  (section, gid, cid) values(" + PrimeGlobal.sectionStr + "," + PrimeGlobal.gidStr + ',' + PrimeGlobal.cidStr + ")";
//		// 		db.execSQL(query);
//
//		// "?uid=" + PrimeGlobal.regid + "&log[]=" + gid,cid,stamp + "&log[]=" + gid,cid,stamp
//		try {
//			String filename = Environment.getExternalStorageDirectory().getAbsolutePath() + "/inquirerlogs.txt";
//			FileWriter fw = new FileWriter(filename, true);
//			fw.write(data + "|");
//			fw.close();
//		} catch (IOException ioe) {
//		}
//	}

//	static String GetTimeStamp()
//	{
//		Long tsLong = System.currentTimeMillis() / 1000;
//		String ts = tsLong.toString();
//		return ts;
//	}

	//response code
	public static String getResponseCode(Throwable e){
		String error_msg = "Connection error occurred.";
		//Server error
		if(e instanceof HttpException) {
			error_msg =  "Server error encountered";
			//Timeout Error
		}else if(e instanceof SocketTimeoutException){
			error_msg = "Connection timeout reached";
			//No internet
		}else if(e instanceof UnknownHostException){
			error_msg = "Internet connection required";
			//SSL Handshake error
		}else if(e instanceof SSLHandshakeException){
			error_msg = "Server connection error";
		} else if(e instanceof IllegalStateException || e instanceof JsonSyntaxException){
			error_msg = "Connection error occurred";
		}
		return error_msg;
	}

	//CLASS MyLoaderDialog SingleTon to Avoid Multiple Object/Instance FOR CUSTOM
	public static class MyLoaderDialog {
		private Dialog dialog;
		private static final MyLoaderDialog newInstance = new MyLoaderDialog(); //create new Instance
		//Method to get Instance
		public static MyLoaderDialog getInstance() {
			return newInstance;
		}
		//constructor
		private MyLoaderDialog() { }

		public void showDialog(Context context){
			//Control the Loading: Return if SHOWING ang not NULL
			if (dialog != null && dialog.isShowing()) {
				return;
			}
			dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//			dialog.getWindow().setBackgroundDrawableResource(R.color.trans_black_dialog);
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.loader_dialog);
			ImageView ivLoader = dialog.findViewById(R.id.gifLoader);
			Glide.with(context).asGif().load(R.drawable.inqloader2).into(ivLoader);
			dialog.show();

		}
		//Control the Loading
		public void dismissDialog(Context context)
		{
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
		}
	}

//	//CLASS MyLoaderDialog SingleTon to Avoid Multiple Object/Instance FOR BREAKING
//	public static class MyLoaderDialog2 {
//		private Dialog dialog;
//		private static final MyLoaderDialog2 newInstance = new MyLoaderDialog2(); //create new Instance
//		//Method to get Instance
//		public static MyLoaderDialog2 getInstance() {
//			return newInstance;
//		}
//		//constructor
//		private MyLoaderDialog2() { }
//
//		public void showDialog(Context context){
//			//Control the Loading: Return if SHOWING ang not NULL
//			if (dialog != null && dialog.isShowing()) {
//				return;
//			}
//			dialog = new Dialog(context);
//			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
////			dialog.getWindow().setBackgroundDrawableResource(R.color.trans_black_dialog);
//			dialog.setCancelable(false);
//			dialog.setContentView(R.layout.loader_dialog);
//			ImageView ivLoader = dialog.findViewById(R.id.gifLoader);
//			Glide.with(context).asGif().load(R.drawable.inqloader2).into(ivLoader);
//			dialog.show();
//
//		}
//		//Control the Loading
//		public void dismissDialog(Context context)
//		{
//			if (dialog != null && dialog.isShowing()) {
//				dialog.dismiss();
//			}
//		}
//	}

	//CLASS LoaderDialog SingleTon to Avoid Multiple Object
	public static class LoaderDialog {
		Dialog dialog;
		public void showDialog(Context context){
			//Control the Loading
			dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//			dialog.getWindow().setBackgroundDrawableResource(R.color.trans_black_dialog);
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.loader_dialog);
			ImageView ivLoader = dialog.findViewById(R.id.gifLoader);
			Glide.with(context).asGif().load(R.drawable.inqloader2).into(ivLoader);
			dialog.show();
		}
		//Control the Loading
		public void dismissDialog(Context context)
		{
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
		}
	}

	//CLASS ViewDialog  ..public static class
	public static class ViewDialog {

		public void showDialog(Context mContext, String msg){
			final Dialog dialog = new Dialog(mContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.custom_dialog);

			TextView text = dialog.findViewById(R.id.text_dialog);
			text.setText(msg);

			Button okButton =  dialog.findViewById(R.id.btn_ok);
			okButton.setOnClickListener(v -> dialog.dismiss());

			Button settingsButton =  dialog.findViewById(R.id.btn_settings);
			settingsButton.setOnClickListener(v -> {
				Intent intent=new Intent(Settings.ACTION_WIRELESS_SETTINGS).setFlags(FLAG_ACTIVITY_NEW_TASK);
				mContext.startActivity(intent);
				dialog.dismiss();
			});

			dialog.show();
		}
	}

	//CLASS NoInternetDialog
	public static class NoInternetDialog {

		public void showDialog(Context mContext, String msg){
			final Dialog dialog = new Dialog(mContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.custom_dialog_nointernet);

			TextView text = dialog.findViewById(R.id.text_dialog);
			text.setText(msg);

			Button close =  dialog.findViewById(R.id.btn_close);

			close.setOnClickListener(v -> dialog.dismiss());

			dialog.show();

		}
	}

	//Class for showing Dialog.   //public static class
	public static class promptUpdateAppDialog {

		private SharedPrefs sf;

		public promptUpdateAppDialog(SharedPrefs sf){
			this.sf = sf;
		}

		public void showDialog(Context mContext, String msg){
			final Dialog dialog = new Dialog(mContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.custom_promptdialog);

			TextView text = dialog.findViewById(R.id.text_dialog);
			text.setText(msg);

			Button yesButton =  dialog.findViewById(R.id.btn_yes);

			yesButton.setOnClickListener(v -> {
				sf.setAskNotifPermissionUpdateVersion(true); //Flag for UPDATE
				Intent intent;
				intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.knx.inquirer&hl=en"));
				mContext.startActivity(intent);
				((Activity)mContext).finish();
				dialog.dismiss();
			});

//			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));


			Button noButton =  dialog.findViewById(R.id.btn_no);

			noButton.setOnClickListener(v -> dialog.dismiss());

			dialog.show();
			Global.isItemClicked = false;

		}
	}

	//CLASS promptPermissionDialog
	public static class promptPermissionDialog {

		public void showDialog(Context mContext, String msg){
			final Dialog dialog = new Dialog(mContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.custom_permission_dialog);

			TextView text = dialog.findViewById(R.id.text_dialog);
			text.setText(msg);

			Button okButton =  dialog.findViewById(R.id.btn_close);
			okButton.setOnClickListener(v -> dialog.dismiss());

			Button settingsButton =  dialog.findViewById(R.id.btn_settings);
			settingsButton.setOnClickListener(v -> {
				Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
				mContext.startActivity(intent);
				dialog.dismiss();
			});

			dialog.show();
			Global.isItemClicked = false;

		}
	}

	//CLASS promptPermissionDialog
	public static class NotificationPermission {

		private SharedPrefs sf;

		public NotificationPermission(SharedPrefs sf){
			this.sf = sf;
		}

		public void showDialog(Context mContext, String msg){
			final Dialog dialog = new Dialog(mContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.custom_permission_dialog);

			TextView text = dialog.findViewById(R.id.text_dialog);
			text.setText(msg);

			Button no =  dialog.findViewById(R.id.btn_close);
			no.setText("No");
			//
			sf.setAskNotificationPermission(true); //NotificationPermission has been DISPLAYED
			sf.setNotifPermissionChangedManually(false); //PermissionChangedManually FALSE

			no.setOnClickListener(v -> {
				sf.setNotificationAllowed(false);
				sf.setNewUser(false); //to be TRUE on NEXT UPDATE if PERMISSION is STILL "NO" "N"
				sf.setNotificationFlag("N");
				Log.d(TAG, "showDialog: NOTIFICATION NotificationPermission NO.. setNotificationAllowed=FALSE ");
				dialog.dismiss();

			});

			Button yes =  dialog.findViewById(R.id.btn_settings);
			yes.setText("Yes");
			yes.setOnClickListener(v -> {
				sf.setNotificationFlag("Y");
				sf.setNotificationAllowed(true);
				sf.setNewUser(false); //to be TRUE ONLY on NEXT UPDATE if PERMISSION is STILL "NO" "N"
				Log.d(TAG, "showDialog: NOTIFICATION NotificationPermission YES.. setNotificationAllowed=TRUE ");
//				Toast.makeText(mContext, "PrimeGlobal Notif allowed", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			});
			dialog.show();
			Global.isItemClicked = false;

		}
	}


	//CLASS promptPermissionDialog
//	public static class PrimeMainMenu {

//		private SharedPrefs sf;

	//nothing to be pass yet
//		public PrimeMainMenu(SharedPrefs sf){
//			this.sf = sf;
//		}


//	}

	//hashing sha 32
	public static String Generate32SHA512(String base)
	{
		String md5param, hashedString = "";
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}

			md5param = hexString.toString();

			hashedString = md5param.substring(0, 32);

		}
		catch(Exception e)
		{

		}
		return hashedString;
	}

	public static OkHttpClient getUnsafeOkHttpClient() {
		try {
			// Create a trust manager that does not validate certificate chains
			final TrustManager[] trustAllCerts = new TrustManager[] {
					new X509TrustManager() {
						@Override
						public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
						}

						@Override
						public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
						}

						@Override
						public java.security.cert.X509Certificate[] getAcceptedIssuers() {
							return new java.security.cert.X509Certificate[]{};
						}
					}
			};

			// Install the all-trusting trust manager
			final SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
			// Create an ssl socket factory with our all-trusting manager
			final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

			OkHttpClient.Builder builder = new OkHttpClient.Builder();
//			builder.sslSocketFactory(sslSocketFactory);
			builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
			builder.hostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});

			OkHttpClient okHttpClient = new OkHttpClient();
			okHttpClient.retryOnConnectionFailure();
			builder.connectTimeout(10, TimeUnit.SECONDS)
					.writeTimeout(10, TimeUnit.SECONDS)
					//Additional: HttpLoggingInterceptor
					.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
					.readTimeout(10, TimeUnit.SECONDS);
			okHttpClient = builder.build();
			return okHttpClient;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String articleView = "";
	public static String articleViewNotification = "";

	public static String testWeb = "<!DOCTYPE html>\n" +
			"<html lang=\"en\">\n" +
			"\n" +
			"<head>\n" +
			"<link rel=\"icon\" href=\"/favicon.ico\" type=\"image/x-icon\"/>\n" +
			"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
			"<meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
			"<meta name=\"viewport\" content=\"width=device-width\">\n" +
			"<title>InquirerMobile</title>\n" +
			"<style>\n" +
			"html, body {\n" +
			"padding: 0;\n" +
			"margin: 0;\n" +
			"position: relative;\n" +
			"width: 100%;\n" +
			"}\n" +
			"body {\n" +
			"\tbackground-color: #fff;\n" +
			"background-size: 100% 100%;\n" +
			"background-attachment: fixed;\n" +
			"font: 100%/1.2 Tahoma, Geneva, sans-serif;\n" +
			"}\n" +
			"h2 {\n" +
			"\tfont: Bold 120%/1.2 Tahoma, Geneva, sans-serif;\n" +
			"\tmargin-top: 5px;\n" +
			"\tmargin-bottom:2%;\n" +
			"\t\tpadding: 0 2% 2% 2%;\n" +
			"}\n" +
			"p {\n" +
			"\tpadding: 0;\n" +
			"}\n" +
			"\n" +
			".imgarticle {\n" +
			"\tmargin-bottom: 8%;\n" +
			"}\n" +
			".small {\n" +
			"font-size: 14px;\n" +
			"margin:0;\n" +
			"line-height: 1.3em;\n" +
			"\tpadding: 0 2% 2% 2%;\n" +
			"\n" +
			"}\n" +
			"div.main {\n" +
			"\tdisplay: table;\n" +
			"\twidth: 100%;\n" +
			"}\n" +
			".content {\n" +
			"\tpadding: 0 2% 8% 2%;\n" +
			"@media only screen and (min-width: 767px) {\n" +
			"div.main {\n" +
			"\tmargin: auto;width: 50%;\n" +
			"\t}\n" +
			"\t\t\t}\n" +
			"\t.center {\n" +
			"\tdisplay: block;\n" +
			"\tmargin: auto;\n" +
			"\t\twidth: 50%;\n" +
			"\t}\n" +
			"</style>\n" +
			"</head>\n" +
			"<body>\n" +
			"<div class=\"main\">\n" +
			"<a href=\"//inq.news/app\">\n" +
			"\t<img src=\"//inqm.news/header.png\" width=\"100%\">\n" +
			"</a>\n" +
			"\t<h2>\n" +
			" <div style=\"border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;\">\n" +
			" <h4>A PHP Error was encountered</h4>\n" +
			"<p>Severity: Notice</p>\n" +
			"<p>Message:  Undefined variable: article</p>\n" +
			" <p>Filename: views/artview.php</p>\n" +
			" <p>Line Number: 66</p>                    I  \t\n" +
			"<p>Backtrace:</p>\n" +
			"\t<p style=\"margin-left:10px\">\n" +
			"File: /var/www/inq/application/views/artview.php<br />\n" +
			"Line: 66<br />\n" +
			"Function: _error_handler\t\t\t</p>\n" +
			"<p style=\"margin-left:10px\">\n" +
			"File: /var/www/inq/application/models/Api2_model.php<br />\n" +
			"Line: 131<br />\n" +
			"Function: view\t\t\t</p>                    I  \t\t\t\n" +
			"<p style=\"margin-left:10px\">\n" +
			"File: /var/www/inq/application/controllers/Api2.php<br />\n" +
			"Line: 17<br />\n" +
			"Function: artview\t\t\t</p>\n" +
			"<p style=\"margin-left:10px\">\n" +
			"File: /var/www/inq/public/index.php<br />\n" +
			"Line: 315<br />\n" +
			"Function: require_once\t\t\t</p>\n" +
			"</div></h2>\n" +
			"<p class=\"small\"></p>\n" +
			"\n" +
			"                 I  \t\t\t<p class=\"small\"></p>\n" +
			"\n" +
			"<br>\n" +
			"\n" +
			"<img class=\"imgarticle\" src=\"https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1668066031_INQBiz_Article.jpg\" width=\"100%\">\n" +
			"\t\t\t<div class=\"content\">\n" +
			"\t\t\t\thttps://inq.news/inqmbizbizzstickers\t\t\t</div>\n" +
			"\n" +
			"            </div>\n" +
			"\n" +
			"        </body>\n" +
			"\n" +
			"        </html>";


}


