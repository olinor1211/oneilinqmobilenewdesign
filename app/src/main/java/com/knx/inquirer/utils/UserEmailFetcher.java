package com.knx.inquirer.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

public class UserEmailFetcher {

    public static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    //Need: Permission of READ_CONTACTS
    //AccountManager: Need  FOR_LEVEL22:<uses-permission android:name="android.permission.GET_ACCOUNTS" /> and HIGHER_LEVEL:<uses-permission android:name="android.permission.READ_CONTACTS" />
    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google"); //GETTING the GMAIL Specifically
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }
}
