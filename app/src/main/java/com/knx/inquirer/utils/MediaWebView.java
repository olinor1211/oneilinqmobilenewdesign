package com.knx.inquirer.utils;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebBackForwardList;
import android.webkit.WebView;

public class MediaWebView extends WebView {
    private static final String TAG = "MediaWebView";

    public MediaWebView(Context context) {
        super(context);
    }

    public MediaWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MediaWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onWindowVisibilityChanged(int visibility) {  //protected
        if (visibility != View.GONE){
//            resumeTimers();
            Log.d(TAG, "onWindowVisibilityChanged: CALLED");
            super.onWindowVisibilityChanged(View.VISIBLE);
        }else{

//            pauseTimers();
            Log.d(TAG, "onWindowVisibilityChanged: GONE CALLED");
        }
    }


    @Override
    public void pauseTimers() {
        Log.d(TAG, "pauseTimers: CALLED");
        super.pauseTimers();
    }

    @Override
    public void resumeTimers() {
        Log.d(TAG, "resumeTimers: CALLED");
        super.resumeTimers();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}