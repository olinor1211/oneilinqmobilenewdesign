package com.knx.inquirer.utils;

import com.google.gson.JsonObject;
import com.knx.inquirer.models.Article;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.FeedResponse;
import com.knx.inquirer.models.MenuAdResponse;
import com.knx.inquirer.models.MyOption;
import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.models.SearchModel;
import com.knx.inquirer.models.TrendingModel;

import java.util.ArrayList;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {

    @GET("api2/init") // @GET("init")
    Single<String> getDailyKey();

    //androidx implementation
    @GET("api2/init")
    Call<String> getDailyKey2();

//    @POST("menu")
//    @FormUrlEncoded
//    Single<MenuResponse> getMenu(@Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);
//
//    //androidx implementation
//    @POST("menu")
//    @FormUrlEncoded
//    Call<MenuResponse> getMenu2(@Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);

    //androidx implementation
//    @POST("menu")
//    @FormUrlEncoded
//    Call<JsonObject> getMenu3(@Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);


    //FOR DEBUGGING ONLY
//    @POST("menu")
//    @FormUrlEncoded
//    Call<ResponseBody> getMenuResponse(@Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);

    //androidx implementation
    @POST("api2/menu")
    @FormUrlEncoded
    Call<JsonObject> getMenu3(@Field("ts") String ts, @Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);
//    Call<JsonObject> getMenu3(@Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);


//    @POST("menu")
//    @FormUrlEncoded
//    Single<MenuAdResponse> getMenuAd(@Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);

    @POST("api2/menu")
    @FormUrlEncoded  //MenuAdResponse
    Single<MenuAdResponse> getBannerAd(@Field("ts") String ts, @Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);

//    @POST("menu")
//    @FormUrlEncoded  //MenuAdResponse
//    Single<ArrayList<MenuAd>> getBannerAd(@Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);


    //    Single<ArrayList<MenuAd>>
//    @POST("menu")
//    @FormUrlEncoded
//    Single<ArrayList<Sections>> getBannerAd(@Field("device") String deviceId, @Field("notif") String notif, @Field("md5") String hash);
//

     //Client to Server
//    @Headers({"Accept: application/json",
//            "Content-Type: application/json"})


//    //FOR DEBUGGING ONLY
//    @POST("section")
//    @FormUrlEncoded
//    Call<ResponseBody> getNewsBySectionDebug(@Field("device") String deviceId, @Field("section") int section, @Field("md5") String hash);

    @POST("api2/article")
    @FormUrlEncoded
    Call<JsonObject> getArticle2(@Field("ts") String ts, @Field("device") String deviceId, @Field("id") String id, @Field("md5") String hash);


    @POST("api2/article")
    @FormUrlEncoded
    Single<Article> getArticle(@Field("ts") String ts, @Field("device") String deviceId, @Field("id") String id, @Field("md5") String hash);

    @POST("api2/scroll")
    @FormUrlEncoded
    Single<ArrayList<NewsModel>> getMoreNews(@Field("ts") String ts,@Field("device") String deviceId, @Field("section") int section, @Field("id") int id, @Field("direction") String direction ,@Field("md5") String hash);

    //UNUSED
    @POST("api2/trending")
    @FormUrlEncoded
    Single<ArrayList<TrendingModel>> getTrending(@Field("ts") String ts, @Field("device") String deviceId, @Field("md5") String hash);

    //For 1.MY_INQUIRER NEWS(MyInqNewsRepo) and 2. Its SECTIONS (FeedRepo)
//    @POST("api2/regfeed") ///////
//    @FormUrlEncoded
//    Single<FeedResponse> getFeed(@Field("ts") String ts, @Field("device") String deviceId, @Field("option") String option , @Field("md5") String hash);

    @POST("api2/section")
    @FormUrlEncoded
    Single<ArrayList<NewsModel>> getNewsBySection(@Field("ts") String ts, @Field("device") String deviceId, @Field("section") int section, @Field("md5") String hash);
//    Single<ArrayList<NewsModel>> getNewsBySection(@Field("device") String deviceId, @Field("section") int section, @Field("md5") String hash);

    //FOR MY_INQUIRER SECTIONS
    @POST("api2/section")
    @FormUrlEncoded
    Single<ArrayList<Feed>> getCustomNewsBySection(@Field("ts") String ts, @Field("device") String deviceId, @Field("section") int section, @Field("md5") String hash);

    //FOR MY_INQUIRER SCROLL DOWN for NEWS
    @POST("api2/regscroll")
    @FormUrlEncoded
    Single<ArrayList<Feed>> getMoreNewsMyInq(@Field("ts") String ts, @Field("device") String deviceId, @Field("option") int option, @Field("id") int id, @Field("direction") String direction ,@Field("md5") String hash);

    @POST("api2/register")
    @FormUrlEncoded
    Single<MyOption> registerUser(@Field("ts") String ts, @Field("device") String deviceId, @Field("name") String name, @Field("gender") String gender, @Field("bday") String bdate, @Field("mobile") String mobile,  @Field("options") String options, @Field("md5") String hash);
//    Single<MyOption> registerUser(@Field("ts") String ts, @Field("device") String deviceId, @Field("name") String name, @Field("gender") String gender, @Field("bday") String bdate, @Field("mobile") String mobile,  @Field("options") String options, @Field("md5") String hash);
//
    @POST("api2/regupdate")
    @FormUrlEncoded
    Single<String> registerUpdate(@Field("ts") String ts, @Field("device") String deviceId, @Field("option") int option, @Field("options") String options, @Field("md5") String hash);

    @POST("api2/search")
    @FormUrlEncoded
    Single<ArrayList<SearchModel>> getSearchedNews(@Field("ts") String ts, @Field("device") String deviceId, @Field("search") String keyword, @Field("md5") String hash);

    @POST("api2/share")
    @FormUrlEncoded
    Single<String> getShareLink2(@Field("ts") String ts, @Field("device") String deviceId, @Field("id") String id, @Field("type") String type, @Field("md5") String hash);

    @POST("api/share")
    @FormUrlEncoded
    Single<String> getShareLink(@Field("device") String deviceId, @Field("id") String id, @Field("type") String type, @Field("md5") String hash);

    //NOTIFICATION and REPORTS
    @POST("api/store-notification-device")
    @FormUrlEncoded
    Single<ResponseBody> registerNotificationPermission(@Field("device") String deviceId, @Field("notif") String id, @Field("type") String type,  @Field("password") String password, @Field("optout") String optout);

    @POST("api/check-notification-device-token")
    @FormUrlEncoded
    Single<ResponseBody> checkNotificationPermission(@Field("device") String deviceId, @Field("notif") String id, @Field("type") String type, @Field("password") String password);

    @POST("api/store-user-read-time")
    @FormUrlEncoded
    Single<ResponseBody> registerTimeArticleReads(@Field("device") String deviceId, @Field("news_id") String news_id, @Field("seconds") int seconds, @Field("password") String password);

    @POST("api/store-user-read-time")
    @FormUrlEncoded
    Single<ResponseBody> registerTimeListViewReads(@Field("device") String deviceId, @Field("section_id") String news_id, @Field("seconds") int seconds, @Field("password") String password);

    //NEW API 3
    @POST("api3/register")
    @FormUrlEncoded
    Single<JsonObject> registerUser3(@Field("ts") String ts, @Field("device") String deviceId,  @Field("password") String password,  @Field("email") String email, @Field("name") String name, @Field("gender") String gender, @Field("bday") String bday, @Field("mobile") String mobile,  @Field("sections") String sections, @Field("notify") String notify, @Field("md5") String hash);

    @POST("api3/login")
    @FormUrlEncoded
    Single<JsonObject> login(@Field("ts") String ts, @Field("device") String deviceId,  @Field("mobile") String mobile, @Field("password") String password, @Field("md5") String hash);

    @POST("api3/update")
    @FormUrlEncoded
    Single<JsonObject> updateUserDetails(@Field("ts") String ts, @Field("device") String deviceId,  @Field("dkey") String dkey, @Field("name") String name, @Field("gender") String gender, @Field("email") String email, @Field("bday") String bday, @Field("sections") String sections, @Field("notify") String notify, @Field("md5") String hash);

    @POST("api3/cpass")
    @FormUrlEncoded
    Single<JsonObject> changePassword(@Field("ts") String ts, @Field("device") String deviceId,  @Field("dkey") String dkey, @Field("password") String password, @Field("md5") String hash);

    @POST("api2/regfeed2")
    @FormUrlEncoded
    Single<FeedResponse> getFeed(@Field("ts") String ts, @Field("device") String deviceId, @Field("dkey") String option , @Field("md5") String hash);

    @POST("api2/artview")
    @FormUrlEncoded
    Single<ResponseBody> viewArticle(@Field("ts") String ts, @Field("device") String deviceId, @Field("id") String id, @Field("md5") String hash);
}
