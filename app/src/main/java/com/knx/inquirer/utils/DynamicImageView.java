package com.knx.inquirer.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
//import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DynamicImageView extends androidx.appcompat.widget.AppCompatImageView
{
    private static final String TAG = "DynamicImageView";
    public DynamicImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DynamicImageView(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final Drawable d = this.getDrawable();
        //
        if(d!=null){
            int height = MeasureSpec.getSize(heightMeasureSpec);
            int width = MeasureSpec.getSize(widthMeasureSpec);
            Log.d(TAG, "onMeasure: MeasureSpec height "+height);
            Log.d(TAG, "onMeasure: MeasureSpec width "+height);
            Log.d(TAG, "onMeasure: widthMeasureSpec  "+widthMeasureSpec);
            Log.d(TAG, "onMeasure: heightMeasureSpec  "+heightMeasureSpec);

            if(width >= height)
                height = (int) Math.ceil(width * (float) d.getIntrinsicHeight() / d.getIntrinsicWidth());
            else
                width = (int) Math.ceil(height * (float) d.getIntrinsicWidth() / d.getIntrinsicHeight());
            //
            Log.d(TAG, "onMeasure: Adjusted height "+ height);
            Log.d(TAG, "onMeasure: Adjusted width "+height);
            this.setMeasuredDimension(width, height);
        }else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}