package com.knx.inquirer.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {

    private static final String PREFERENCE_NAME = "INQ_SF";
    private final SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    public SharedPrefs(Context context) {
        sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public String getDailyKey() {
        String dailyKey = sharedpreferences.getString("DAILY_KEY", "NO_KEY");
        return dailyKey;
    }

    public void setDailyKey(String key) {
        editor = sharedpreferences.edit();
        editor.putString("DAILY_KEY", key);
        editor.apply();
    }

    public String getFirebaseToken() {
        String fbToken = sharedpreferences.getString("FIREBASE_TOKEN", "NO_TOKEN");
        return fbToken;
    }

    public void setFirebaseToken(String token) {
        editor = sharedpreferences.edit();
        editor.putString("FIREBASE_TOKEN", token);
        editor.apply();
    }

    public String getDeviceId() {
        String deviceId = sharedpreferences.getString("DEVICE_ID", "NO_DEVICE_ID");
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        editor = sharedpreferences.edit();
        editor.putString("DEVICE_ID", deviceId);
        editor.apply();
    }

    public boolean isTutViewed() {
        boolean isviewed = sharedpreferences.getBoolean("TUT_VIEWED", false);
        return isviewed;
    }

    public void setTutViewed(boolean isviewed) {
        editor = sharedpreferences.edit();
        editor.putBoolean("TUT_VIEWED", isviewed);
        editor.apply();
    }

    public int getLastSection() {
        int lastSection = sharedpreferences.getInt("LAST_SECTION", 0);
        return lastSection;
    }

    public void setLastSection(int lastSection) {
        editor = sharedpreferences.edit();
        editor.putInt("LAST_SECTION", lastSection);
        editor.apply();
    }

    public int getLastSectionCustom() {
        int lastSectionCustom = sharedpreferences.getInt("LAST_SECTION_CUSTOM", 99);
        return lastSectionCustom;
    }

    public void setLastSectionCustom(int lastSectionCustom) {
        editor = sharedpreferences.edit();
        editor.putInt("LAST_SECTION_CUSTOM", lastSectionCustom);
        editor.apply();
    }

    public int getOptionId() {
        int optionId = sharedpreferences.getInt("OPTION_ID", 0);
        return optionId;
    }

    public void setOptionId(int optionId) {
        editor = sharedpreferences.edit();
        editor.putInt("OPTION_ID", optionId);
        editor.apply();
    }

    public String getName() {
        String name = sharedpreferences.getString("REG_NAME", "NAME");
        return name;
    }

    public void setName(String name) {
        editor = sharedpreferences.edit();
        editor.putString("REG_NAME", name);
        editor.apply();
    }

    public String getGender() {
        String gender = sharedpreferences.getString("REG_GENDER", "");
        return gender;
    }

    public void setGender(String gender) {
        editor = sharedpreferences.edit();
        editor.putString("REG_GENDER", gender);
        editor.apply();
    }

    public String getEmail() {
        String email = sharedpreferences.getString("REG_EMAIL", "");
        return email;
    }

    public void setEmail(String email) {
        editor = sharedpreferences.edit();
        editor.putString("REG_EMAIL", email);
        editor.apply();
    }

    public String getBdate() {
        String gender = sharedpreferences.getString("REG_BDATE", "");
        return gender;
    }

    public void setBdate(String bdate) {
        editor = sharedpreferences.edit();
        editor.putString("REG_BDATE", bdate);
        editor.apply();
    }

    public String getMobile() {
        String gender = sharedpreferences.getString("REG_MOBILE", "");
        return gender;
    }

    public void setMobile(String bdate) {
        editor = sharedpreferences.edit();
        editor.putString("REG_MOBILE", bdate);
        editor.apply();
    }

    public String getCustomMenu() {
        String menu = sharedpreferences.getString("CUSTOM_MENU", "");
        return menu;
    }

    public void setCustomMenu(String menu) {
        editor = sharedpreferences.edit();
        editor.putString("CUSTOM_MENU", menu);
        editor.apply();
    }

    public Integer getDeviceKey() {
        Integer key = sharedpreferences.getInt("DEVICE_KEY", 0);
        return key;
    }

    public void setDeviceKey(Integer key) {
        editor = sharedpreferences.edit();
        editor.putInt("DEVICE_KEY", key);
        editor.apply();
    }

    public boolean isRegistered() {
        boolean isviewed = sharedpreferences.getBoolean("REGISTERED", false);
        return isviewed;
    }

    public void setAsRegistered(boolean isviewed) {
        editor = sharedpreferences.edit();
        editor.putBoolean("REGISTERED", isviewed);
        editor.apply();
    }

    public boolean isQRTutViewed() {
        boolean isviewed = sharedpreferences.getBoolean("QRTUT_VIEWED", false);
        return isviewed;
    }

    public void setQRTutViewed(boolean isviewed) {
        editor = sharedpreferences.edit();
        editor.putBoolean("QRTUT_VIEWED", isviewed);
        editor.apply();
    }

    public boolean isMyInqutViewed() {
        boolean isviewed = sharedpreferences.getBoolean("MYINQTUT_VIEWED", false);
        return isviewed;
    }

    public void setMyInqTutViewed(boolean isviewed) {
        editor = sharedpreferences.edit();
        editor.putBoolean("MYINQTUT_VIEWED", isviewed);
        editor.apply();
    }

    public int getLastSectionPosition() {
        int lastSectionPos = sharedpreferences.getInt("LAST_SECTION_POS", -1);
        return lastSectionPos;
    }

    public void setLastSectionPosition(int lastSectionPos) {
        editor = sharedpreferences.edit();
        editor.putInt("LAST_SECTION_POS", lastSectionPos);
        editor.apply();
    }

    public int getLastCustomSectionPosition() {
        int lastSectionPos = sharedpreferences.getInt("LAST_CUSTOM_SECTION_POS", -1);
        return lastSectionPos;
    }

    public void setLastCustomSectionPosition(int lastSectionPos) {
        editor = sharedpreferences.edit();
        editor.putInt("LAST_CUSTOM_SECTION_POS", lastSectionPos);
        editor.apply();
    }


    public void setStayInArticle (int id){
        editor = sharedpreferences.edit();
        editor.putInt("ARTICLE_STAY", id);
        editor.apply();
    }

    public int getStayInArticle (){
        int id = sharedpreferences.getInt("ARTICLE_STAY", 0);
        return id;
    }

    public boolean isNewUser() {
        boolean user = sharedpreferences.getBoolean("INQ_NEW_USER", true);
        return user;
    }

    public void setNewUser(boolean user) {
        editor = sharedpreferences.edit();
        editor.putBoolean("INQ_NEW_USER", user);
        editor.apply();
    }

    public boolean isNotifStatusChange() {
        boolean user = sharedpreferences.getBoolean("NOTIF_CHANGES", false);
        return user;
    }

    public void setNotifStatusChange(boolean user) {
        editor = sharedpreferences.edit();
        editor.putBoolean("NOTIF_CHANGES", user);
        editor.apply();
    }

    public int getStoryReadsCount() {
        int count = sharedpreferences.getInt("COUNT_STORY_READS", 0);
        return count;
    }

    public void setStoryReadsCount(int count) {
        editor = sharedpreferences.edit();
        editor.putInt("COUNT_STORY_READS", count);
        editor.apply();
    }

    public int getListViewReadsCount() {
        int count = sharedpreferences.getInt("COUNT_LIST_VIEW_READS", 0);
        return count;
    }

    public void setListViewReadsCount(int count) {
        editor = sharedpreferences.edit();
        editor.putInt("COUNT_LIST_VIEW_READS", count);
        editor.apply();
    }

    public int getListViewReadsCountMyInq() {
        int count = sharedpreferences.getInt("COUNT_LIST_VIEW_READS_MyInq", 0);
        return count;
    }

    public void setListViewReadsCountMyInq(int count) {
        editor = sharedpreferences.edit();
        editor.putInt("COUNT_LIST_VIEW_READS_MyInq", count);
        editor.apply();
    }


    public boolean hasNotificationAllowed() {
        boolean notification_flag = sharedpreferences.getBoolean("NOTIFICATION_FLAG", false);
        return notification_flag;
    }

    public void setNotificationAllowed(boolean notification_flag) {
        editor = sharedpreferences.edit();
        editor.putBoolean("NOTIFICATION_FLAG", notification_flag);
        editor.apply();
    }

    public String getNotificationFlag() {
        String notif = sharedpreferences.getString("NOTIFICATION_FLAG_YES_NO", "Y");
        return notif;
    }

    public void setNotificationFlag(String notif) {
        editor = sharedpreferences.edit();
        editor.putString("NOTIFICATION_FLAG_YES_NO", notif);
        editor.apply();
    }


    public String getSectionUrl() {
        String url = sharedpreferences.getString("SECTION_URL", "https://mobility.inq.news");
        return url;
    }

    public void setSectionUrl(String url) {
        editor = sharedpreferences.edit();
        editor.putString("SECTION_URL", url);
        editor.apply();
    }

    public String getSectionNameTempContainer() {
        String name = sharedpreferences.getString("SECTION_NAME_TEMP", "temp");
        return name;
    }

    public void setSectionNameTempContainer(String name) {
        editor = sharedpreferences.edit();
        editor.putString("SECTION_NAME_TEMP", name);
        editor.apply();
    }

    public String getSectionName() {
        String name = sharedpreferences.getString("SECTION_NAME", "");
        return name;
    }

    public void setSectionName(String name) {
        editor = sharedpreferences.edit();
        editor.putString("SECTION_NAME", name);
        editor.apply();
    }

    public boolean hasNotificationAllowedAlready() {
        boolean notification_flag = sharedpreferences.getBoolean("NOTIFICATION_ALREADY_FLAG", false);
        return notification_flag;
    }

    public void setNotificationAllowedAlready(boolean notification_flag) {
        editor = sharedpreferences.edit();
        editor.putBoolean("NOTIFICATION_ALREADY_FLAG", notification_flag);
        editor.apply();
    }

    public boolean hasAskNotificationPermission() {
        boolean notification_flag = sharedpreferences.getBoolean("NOTIFICATION_ASK_PERMISSION", false);
        return notification_flag;
    }

    public void setAskNotificationPermission(boolean notification_flag) {
        editor = sharedpreferences.edit();
        editor.putBoolean("NOTIFICATION_ASK_PERMISSION", notification_flag);
        editor.apply();
    }

    public boolean hasNotifPermissionChangedManually() {
        boolean notification_flag = sharedpreferences.getBoolean("NOTIFICATION_PERMITTED_MANUALLY", false);
        return notification_flag;
    }

    public void setNotifPermissionChangedManually(boolean notification_flag) {
        editor = sharedpreferences.edit();
        editor.putBoolean("NOTIFICATION_PERMITTED_MANUALLY", notification_flag);
        editor.apply();
    }


    public boolean hasAskNotifPermissionUpdateVersion() {
        boolean notification_flag = sharedpreferences.getBoolean("NOTIFICATION_ASK_UPDATE_VERSION", false);
        return notification_flag;
    }

    public void setAskNotifPermissionUpdateVersion(boolean notification_flag) {
        editor = sharedpreferences.edit();
        editor.putBoolean("NOTIFICATION_ASK_UPDATE_VERSION", notification_flag);
        editor.apply();
    }

    /**Start of NEW ASSIGNMENT OF VARIABLES**/
    public int getSectionMenuID() {
        int id = sharedpreferences.getInt("SECTION_MENU_ID", 1);
        return id;
    }

    public void setSectionMenuID(int id) {
        editor = sharedpreferences.edit();
        editor.putInt("SECTION_MENU_ID", id);
        editor.apply();
    }

    /**PODCAST */
    public int getPlayerStatus() {
        int vh = sharedpreferences.getInt("PODCAST_PLAYER_ACTIVE", -1);
        return vh;
    }

    public void setPlayerStatus(int vh) {
        editor = sharedpreferences.edit();
        editor.putInt("PODCAST_PLAYER_ACTIVE", vh);
        editor.apply();
    }

    public boolean isPlayClicked() {
        boolean player_button = sharedpreferences.getBoolean("PODCAST_PLAYER_BUTTON", false);
        return player_button;
    }

    public void setPlayClicked(boolean player_button) {
        editor = sharedpreferences.edit();
        editor.putBoolean("PODCAST_PLAYER_BUTTON", player_button);
        editor.apply();
    }

    public boolean isPodcastPlaying() {
        boolean player_button = sharedpreferences.getBoolean("PODCAST_PLAYING", false);
        return player_button;
    }

    public void setPodcastPlaying(boolean player_button) {
        editor = sharedpreferences.edit();
        editor.putBoolean("PODCAST_PLAYING", player_button);
        editor.apply();
    }

    public String getFontString() {
        String dailyKey = sharedpreferences.getString("FONT_STRING", "0");
        return dailyKey;
    }

    //new
    public void setFontString(String key) {
        editor = sharedpreferences.edit();
        editor.putString("FONT_STRING", key);
        editor.apply();
    }

    public boolean isWebNewsActOpen() {
        boolean open = sharedpreferences.getBoolean("WNA_OPEN", false);
        return open;
    }

    public void setWebNewsActOpen(boolean open) {
        editor = sharedpreferences.edit();
        editor.putBoolean("WNA_OPEN", open);
        editor.apply();
    }

    public boolean getNightMode() {
        boolean isviewed = sharedpreferences.getBoolean("NIGHT_MODE", false);
        return isviewed;
    }

    public void setNightMode(boolean isviewed) {
        editor = sharedpreferences.edit();
        editor.putBoolean("NIGHT_MODE", isviewed);
        editor.apply();
    }

    public int getLastBookmarkCount() {
        int count = sharedpreferences.getInt("BOOKMARK_COUNT_", 0);
        return count;
    }

    public void setLastBookmarkCount(int count) {
        editor = sharedpreferences.edit();
        editor.putInt("BOOKMARK_COUNT_", count);
        editor.apply();
    }



}