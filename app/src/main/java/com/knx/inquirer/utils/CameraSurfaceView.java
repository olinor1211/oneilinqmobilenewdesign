package com.knx.inquirer.utils;

import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.util.List;

public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = "CameraSurfaceView";
    private Camera camera;
    private SurfaceHolder surfaceHolder;
    private int cameraId = 1;

    //extendsSurfaceView
    public CameraSurfaceView(Context context) {
        super(context);
        //Initiate the Surface Holder properly
        this.surfaceHolder = this.getHolder();
        this.surfaceHolder.addCallback(this);
    }
    //extendsSurfaceView
    public CameraSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    //extendsSurfaceView
    public CameraSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    //extendsSurfaceView
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CameraSurfaceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    //SurfaceHolder.Callback
    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
        Log.d(TAG, "surfaceCreated: CALLED *** ");
        //Camera to be used Front/Back Settings
//        if(cameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
//            Global.rotateSw = 1;
//            cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
//        }else{
//            Global.rotateSw = 0;
//            cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
//        }

//        try {
            // open the camera
//            Global.rotateSw = 0;
//            cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
//            mCamera = Camera.open(cameraId);


//        }
//        catch (Exception e) {
//            Log.d(TAG, "surfaceCreated: 1 Exception_Error "+e.getMessage());
//        //			return;
//        }


        try {
            //Open the Camera in preview mode
            this.camera = Camera.open();
            this.camera.setPreviewDisplay(this.surfaceHolder);

        }catch(Exception e){
            Log.d(TAG, "surfaceCreated: Exception_Error: "+e.getMessage());
        }
    }

    //SurfaceHolder.Callback
    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged: CALLED *** ");
        //Get Camera Parameters
        Camera.Parameters parameters = camera.getParameters();
        //Get Camera AspectRatio Sizes
        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
		Camera.Size selected = sizes.get(0); //Get the First Size
//        parameters.setPreviewSize(width, height);
        parameters.setPreviewSize(selected.width,selected.height);
        Log.d(TAG, "surfaceChanged: WIDTH: "+selected.width);
        Log.d(TAG, "surfaceChanged: HEIGHT: "+selected.height);
        //Control focusModes
        List<String> focusModes = parameters.getSupportedFocusModes();
        if(focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)){
            Log.d(TAG, "surfaceChanged: FOCUS_MODE_CONTINUOUS_PICTURE CALLED");
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }else{
            if(focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)){
                Log.d(TAG, "surfaceChanged: FOCUS_MODE_AUTO CALLED");
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }
        }
        camera.setParameters(parameters);
        camera.setDisplayOrientation(90); //Set Camera Orientation
        camera.startPreview();
    }

    //SurfaceHolder.Callback
    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
        Log.d(TAG, "surfaceDestroyed: CALLED *** ");
        // Surface will be destroyed when replaced with a new screen. Always make sure to release the Camera instance
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    public Camera getCamera() {
        Log.d(TAG, "getCamera: CALLED *** ");
        return camera;
    }
}