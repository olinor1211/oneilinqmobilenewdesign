package com.knx.inquirer.utils;

public class BackgroundAudioService {

//public class BackgroundAudioService extends MediaBrowserServiceCompat implements MediaPlayer.OnCompletionListener, AudioManager.OnAudioFocusChangeListener {
//
//    public static final String COMMAND_EXAMPLE = "command_example";
//
//    private MediaPlayer mMediaPlayer;
//    private MediaSessionCompat mMediaSessionCompat;
//    private final int YOUR_UNIQUE_MEDIA_ROOT_ID = 99;
//
//    ////BroadcastReceiver
//    private BroadcastReceiver mNoisyReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if( mMediaPlayer != null && mMediaPlayer.isPlaying() ) {
//                mMediaPlayer.pause();
//            }
//        }
//    };
//
//    ////MediaSessionCompat.Callback
//    private MediaSessionCompat.Callback mMediaSessionCallback = new MediaSessionCompat.Callback() {
//        @Override
//        public void onCommand(String command, Bundle extras, ResultReceiver cb) {
//            super.onCommand(command, extras, cb);
//            if( COMMAND_EXAMPLE.equalsIgnoreCase(command) ) {
//                //Custom command here
//            }
//        }
//
//        @Override
//        public void onAddQueueItem(MediaDescriptionCompat description) {
//            super.onAddQueueItem(description);
//        }
//
//        @Override
//        public void onAddQueueItem(MediaDescriptionCompat description, int index) {
//            super.onAddQueueItem(description, index);
//        }
//
//        @Override
//        public void onRemoveQueueItem(MediaDescriptionCompat description) {
//            super.onRemoveQueueItem(description);
//        }
//
//        @Override
//        public void onPause() {
//            super.onPause();
//            if( mMediaPlayer.isPlaying() ) {
//                mMediaPlayer.pause();
//                setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED);
//                showPausedNotification();
//            }
//        }
//
//        @Override
//        public void onPlay() {
//            super.onPlay();
//            if( !successfullyRetrievedAudioFocus() ) {
//                return;
//            }
//
//            mMediaSessionCompat.setActive(true);
//            setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING);
//
//            showPlayingNotification();
//            mMediaPlayer.start();
//        }
//
//        @Override
//        public void onPlayFromMediaId(String mediaId, Bundle extras) {
//            super.onPlayFromMediaId(mediaId, extras);
//            try {
//                AssetFileDescriptor afd = getResources().openRawResourceFd(Integer.valueOf(mediaId));
//                if( afd == null ) {
//                    return;
//                }
//
//                try {
//                    mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
//
//                } catch( IllegalStateException e ) {
//                    mMediaPlayer.release();
//                    initMediaPlayer();
//                    mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
//                }
//
//                afd.close();
//                initMediaSessionMetadata();
//
//            } catch (IOException e) {
//                return;
//            }
//
//            try {
//                mMediaPlayer.prepare();
//            } catch (IOException e) {}
//
//            //Work with extras here if you want
//        }
//
//
////        @Override
////        public void onCommand(String command, Bundle extras, ResultReceiver cb) {
////            super.onCommand(command, extras, cb);
////            if( COMMAND_EXAMPLE.equalsIgnoreCase(command) ) {
////                //Custom command here
////            }
////        }
//
//        @Override
//        public void onSeekTo(long pos) {
//            super.onSeekTo(pos);
//        }
//    };
//
//
//    //onStartCommand
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        MediaButtonReceiver.handleIntent(mMediaSessionCompat, intent);
//        return super.onStartCommand(intent, flags, startId);
//    }
//
//    //BEGIN implemented METHODS MediaBrowserServiceCompat
//    @Nullable
//    @Override
//    public BrowserRoot onGetRoot(@NonNull String s, int i, @Nullable Bundle bundle) {
//
//        //RESOLVE: SHOULD not be return NULL, Must return UNIQUE_ROOT_ID
//        return new BrowserRoot(
//                "YOUR_UNIQUE_MEDIA_ROOT_ID",
//                null);
////        return null;
//    }
//
//    @Override
//    public void onLoadChildren(@NonNull String s, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {
//    }
//    //END implemented METHODS MediaBrowserServiceCompat
//
////    mMediaPlayer.setDataSource(url);
////    mMediaPlayer.setOnPreparedListener(this);
////    mMediaPlayer.prepareAsync();
//
//
////    mMediaPlayer.setDataSource(url);
////    mMediaPlayer.OnPreparedListener(
////    mMediaPlayer.prepareAsync();
//
//    //NOTE: need first to setOnPreparedListener..then prepareAsync.. before start
//    public void onPrepared(MediaPlayer player) {
//
//        try {
//            player.setDataSource("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-3.mp3");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        player.setOnPreparedListener(this::onPrepared);
//        player.prepareAsync();
//        player.start();
//    }
//
//
//
//    //implemented METHODS AudioManager.OnAudioFocusChangeListener
//    @Override
//    public void onAudioFocusChange(int focusChange) {
//
//        switch( focusChange ) {
//            case AudioManager.AUDIOFOCUS_LOSS:  //AUDIOFOCUS_LOSS
//                Log.d("TAG", "onAudioFocusChange: AUDIOFOCUS_LOSS CALLED ");
//                if( mMediaPlayer.isPlaying() ) {
//                    mMediaPlayer.pause();  //  mMediaPlayer.stop()
//                }
//                break;
//
//            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
//                Log.d("TAG", "onAudioFocusChange: AUDIOFOCUS_LOSS_TRANSIENT CALLED ");
////                if( mMediaPlayer.isPlaying() ) {
////                    mMediaPlayer.pause();
////                }
//
//                break;
//
//            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
//                Log.d("TAG", "onAudioFocusChange: AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK CALLED ");
////                if( mMediaPlayer != null ) {
//////                    mMediaPlayer.setVolume(0.3f, 0.3f);
////                }
//                break;
//
//            case AudioManager.AUDIOFOCUS_GAIN:
//                Log.d("TAG", "onAudioFocusChange: AUDIOFOCUS_GAIN CALLED ");
//                if( mMediaPlayer != null ) {
//                    if (!mMediaPlayer.isPlaying()) {
//                        onPrepared(mMediaPlayer);
////                        mMediaPlayer.start();
//                    }
//                    mMediaPlayer.setVolume(1.0f, 1.0f);
//                }
//                break;
//
//            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
//                Log.d("TAG", "onAudioFocusChange: AUDIOFOCUS_GAIN_TRANSIENT CALLED ");
//                if( mMediaPlayer != null ) {
//                    if (!mMediaPlayer.isPlaying()) {
//                        onPrepared(mMediaPlayer);
////                        mMediaPlayer.start();
//                    }
//                    mMediaPlayer.setVolume(1.0f, 1.0f);
//                }
//                break;
//
//            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
//                Log.d("TAG", "onAudioFocusChange: AUDIOFOCUS_GAIN_TRANSIENT CALLED ");
//                if( mMediaPlayer != null ) {
//                    if (!mMediaPlayer.isPlaying()) {
//                        onPrepared(mMediaPlayer);
////                        mMediaPlayer.start();
//                    }
//                    mMediaPlayer.setVolume(1.0f, 1.0f);
//                }
//                    break;
//        }
//    }
//
//    //mp.setDataSource(url);
//    //mp.setOnPreparedListener(this);
//    //mp.prepareAsync();
//    //
//    //public void onPrepared(MediaPlayer player) {
//    //    player.start();
//    //}
//
//    //implemented METHODS MediaPlayer.OnCompletionListener
//    @Override
//    public void onCompletion(MediaPlayer mp) {
//
//    }
//
//    //successfullyRetrievedAudioFocus
//    private boolean successfullyRetrievedAudioFocus() {
//        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//
//        int result = audioManager.requestAudioFocus(this,
//                AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
//        return result == AudioManager.AUDIOFOCUS_GAIN;
//    }
//
//    //initNoisyReceiver
//    private void initNoisyReceiver() {
//        //Handles headphones coming unplugged. cannot be done through a manifest receiver
//        IntentFilter filter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
//        registerReceiver(mNoisyReceiver, filter);
//    }
//
//    //initMediaPlayer
//    private void initMediaPlayer() {
//        mMediaPlayer = new MediaPlayer();
//        mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
//        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//        mMediaPlayer.setVolume(1.0f, 1.0f);
//    }
//
//    private void initMediaSessionMetadata() {
//        MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();
//        //Notification icon in card
//        metadataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
//        metadataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
//
//        //lock screen icon for pre lollipop
//        metadataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_ART, BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
//        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, "Display Title");
//        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, "Display Subtitle");
//        metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, 1);
//        metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS, 1);
//
//        mMediaSessionCompat.setMetadata(metadataBuilder.build());
//    }
//
//    //showPlayingNotification
//    private void showPlayingNotification() {
//        NotificationCompat.Builder builder = MediaStyleHelper.from(BackgroundAudioService.this, mMediaSessionCompat);
//       try{
//           if (builder == null) {
//               return;
//           }
//       }catch(Exception e){
//           e.printStackTrace();
//       }
//
//        builder.addAction(new NotificationCompat.Action(android.R.drawable.ic_media_pause, "Pause", MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY_PAUSE)));
//       builder.setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0).setMediaSession(mMediaSessionCompat.getSessionToken()));
//        builder.setSmallIcon(R.drawable.ic_launcher);
//        NotificationManagerCompat.from(BackgroundAudioService.this).notify(1, builder.build());
//
//
//    }
//
//    //showPausedNotification
//    private void showPausedNotification() {
//        NotificationCompat.Builder builder = MediaStyleHelper.from(this, mMediaSessionCompat);
//        if (builder == null) {
//            return;
//        }
//        builder.addAction(new NotificationCompat.Action(android.R.drawable.ic_media_play, "Play", MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY_PAUSE)));
//        builder.setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0).setMediaSession(mMediaSessionCompat.getSessionToken()));
//        builder.setSmallIcon(R.drawable.ic_launcher);
//        NotificationManagerCompat.from(this).notify(1, builder.build());
//
//    }
//
//    //initMediaSession
//    private void initMediaSession () {
//        ComponentName mediaButtonReceiver = new ComponentName(getApplicationContext(), MediaButtonReceiver.class);
//        mMediaSessionCompat = new MediaSessionCompat(getApplicationContext(), "Tag", mediaButtonReceiver, null);
//
//        mMediaSessionCompat.setCallback(mMediaSessionCallback);
//        mMediaSessionCompat.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
//
//        Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
//        mediaButtonIntent.setClass(this, MediaButtonReceiver.class);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0);
//        mMediaSessionCompat.setMediaButtonReceiver(pendingIntent);
//
//        setSessionToken(mMediaSessionCompat.getSessionToken());
//    }
//
//    private void setMediaPlaybackState(int state) {
//        PlaybackStateCompat.Builder playbackstateBuilder = new PlaybackStateCompat.Builder();
//        if( state == PlaybackStateCompat.STATE_PLAYING ) {
//            playbackstateBuilder.setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_PAUSE);
//        } else {
//            playbackstateBuilder.setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_PLAY);
//        }
//        playbackstateBuilder.setState(state, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 0);
//        mMediaSessionCompat.setPlaybackState(playbackstateBuilder.build());
//    }
//
//    //ONCREATE
//    @Override
//    public void onCreate () {
//        super.onCreate();
//
//        initMediaPlayer();
//        initMediaSession();
//        initNoisyReceiver();
//    }
//
//    //ONDESTROY
//    @Override
//    public void onDestroy () {
//        super.onDestroy();
//        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        audioManager.abandonAudioFocus(this);
//        unregisterReceiver(mNoisyReceiver);
//        mMediaSessionCompat.release();
//        NotificationManagerCompat.from(this).cancel(1);
//    }
}



























//        @Override
//        public boolean onMediaButtonEvent(Intent mediaButtonEvent) {
//            return super.onMediaButtonEvent(mediaButtonEvent);
//        }
//
//        @Override
//        public void onPrepare() {
//            super.onPrepare();
//        }
//
//        @Override
//        public void onPrepareFromMediaId(String mediaId, Bundle extras) {
//            super.onPrepareFromMediaId(mediaId, extras);
//        }
//
//        @Override
//        public void onPrepareFromSearch(String query, Bundle extras) {
//            super.onPrepareFromSearch(query, extras);
//        }
//
//        @Override
//        public void onPrepareFromUri(Uri uri, Bundle extras) {
//            super.onPrepareFromUri(uri, extras);
//        }
//

//        @Override
//        public void onPlayFromSearch(String query, Bundle extras) {
//            super.onPlayFromSearch(query, extras);
//        }
//
//        @Override
//        public void onPlayFromUri(Uri uri, Bundle extras) {
//            super.onPlayFromUri(uri, extras);
//        }
//
//        @Override
//        public void onSkipToQueueItem(long id) {
//            super.onSkipToQueueItem(id);
//        }
//
//
//        @Override
//        public void onSkipToNext() {
//            super.onSkipToNext();
//        }
//
//        @Override
//        public void onSkipToPrevious() {
//            super.onSkipToPrevious();
//        }
//
//        @Override
//        public void onFastForward() {
//            super.onFastForward();
//        }
//
//        @Override
//        public void onRewind() {
//            super.onRewind();
//        }
//
//        @Override
//        public void onStop() {
//            super.onStop();
//        }
//
//        @Override
//        public void onSeekTo(long pos) {
//            super.onSeekTo(pos);
//        }
//
//
//
//        @Override
//        public void onSetRating(RatingCompat rating) {
//            super.onSetRating(rating);
//        }
//
//        @Override
//        public void onSetRating(RatingCompat rating, Bundle extras) {
//            super.onSetRating(rating, extras);
//        }
//
//        @Override
//        public void onSetCaptioningEnabled(boolean enabled) {
//            super.onSetCaptioningEnabled(enabled);
//        }
//
//        @Override
//        public void onSetRepeatMode(int repeatMode) {
//            super.onSetRepeatMode(repeatMode);
//        }
//
//        @Override
//        public void onSetShuffleMode(int shuffleMode) {
//            super.onSetShuffleMode(shuffleMode);
//        }
//
//        @Override
//        public void onCustomAction(String action, Bundle extras) {
//            super.onCustomAction(action, extras);
//        }
//
//        @Override
//        public void onAddQueueItem(MediaDescriptionCompat description) {
//            super.onAddQueueItem(description);
//        }
//
//        @Override
//        public void onAddQueueItem(MediaDescriptionCompat description, int index) {
//            super.onAddQueueItem(description, index);
//        }
//
//        @Override
//        public void onRemoveQueueItem(MediaDescriptionCompat description) {
//            super.onRemoveQueueItem(description);
//        }

//public class BackgroundAudioService extends MediaBrowserServiceCompat
// implements MediaPlayer.OnCompletionListener, AudioManager.OnAudioFocusChangeListener  {