package com.knx.inquirer.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.security.Security;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class JavaMailApi extends AsyncTask<Void,Void,Void> { //extends AsyncTask<Void,Void,Void>
    private static final String TAG = "JavaMailApi";

    //1st.  Download these 3 and put in the libs directory
    //activation.jar //additional.jar //mail.jar
    //2nd. Add those line in dependencies: or EQUIVALENT is  implementation fileTree(dir: 'libs', include: ['*.jar']) //fileTree
    //implementation files('libs/activation.jar') //implementation files('libs/additionnal.jar') //implementation files('libs/mail.jar')
    //3rd. Need to create JSSEProvider
    //4th. Need to create UserEmailFetcher

    //Variables
    private Session mSession;
    private Context context;
    private String mEmail;
    private String mSubject;
    private String mMessage;
    //    private ProgressDialog mProgressDialog;
    private AlertDialog alertDialog;

    static {
        Security.addProvider(new JSSEProvider());
    }

    //Constructor
    public JavaMailApi(Context context, String emailRecipient, String subject, String message) {
        this.mEmail = emailRecipient;
        this.mSubject = subject;
        this.mMessage = message;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        showProgressDialog();

        //Show progress dialog while sending email
//        mProgressDialog = ProgressDialog.show(mContext,"Sending message", "Please wait...",false,false);
    }



    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
//        alertDialog.dismiss();
//        Toast.makeText(mContext,"Message Sent",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        //Creating properties
        Properties props = new Properties();
        //Configuring properties for gmail
        //If you are not using gmail you may need to change the values
        props.put("mail.smtp.host", "smtp.gmail.com"); //("mail.host", mailhost);
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.setProperty("mail.transport.protocol", "smtp");
        props.put("mail.smtp.socketFactory.fallback", "false"); //new
        props.setProperty("mail.smtp.quitwait", "false"); //new

//        mSession = Session.getDefaultInstance(props, this);

        //Creating a new session
        mSession = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() { ////Authenticating the password
                return new PasswordAuthentication(Global.EMAIL, "sheneil012205!!");
            }
        });

        try {
            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(mSession);
            //Setting sender address
//            mm.setFrom(new InternetAddress(Global.EMAIL));
            //Adding receiver
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(mEmail));
            //Adding subject
            mm.setSubject(mSubject);
            //Adding message
            mm.setText(mMessage);
            //Sending email
            Transport.send(mm);

//            BodyPart messageBodyPart = new MimeBodyPart();
//            messageBodyPart.setText(message);
//            messageBodyPart.setFileName(filePath);


//            messageBodyPart = new MimeBodyPart();
////          messageBodyPart.setDataHandler(new DataHandler(source));
//
//            Multipart multipart = new MimeMultipart();
//            multipart.addBodyPart(messageBodyPart);
//            multipart.addBodyPart(messageBodyPart);

//            mm.setContent(multipart);

//            DataSource source = new FileDataSource(filePath);
//

        } catch (MessagingException e) {
            Log.d(TAG, "MessagingException_ERROR: "+ e.getMessage());
        }
        return null;
    }

//    public void executeService(){
//        /** ExecutorService and Handler: Replacement for AsyncTask deprecated */
//        ExecutorService executor = Executors.newSingleThreadExecutor();
//        Handler handler = new Handler(Looper.getMainLooper());
//        executor.execute(() -> {
//            /** doIn Background work here */
//            //Creating properties
//            Properties props = new Properties();
//            //Configuring properties for gmail
//            //If you are not using gmail you may need to change the values
//            props.put("mail.smtp.host", "smtp.gmail.com"); //("mail.host", mailhost);
//            props.put("mail.smtp.socketFactory.port", "465");
//            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//            props.put("mail.smtp.auth", "true");
//            props.put("mail.smtp.port", "465");
//            props.setProperty("mail.transport.protocol", "smtp");
//            props.put("mail.smtp.socketFactory.fallback", "false"); //new
//            props.setProperty("mail.smtp.quitwait", "false"); //new
//
//            //Creating a new session
//            mSession = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
//                protected PasswordAuthentication getPasswordAuthentication() { ////Authenticating the password
//                    return new PasswordAuthentication(Global.EMAIL, "sheneil012205!!");//Global.PASSWORD
//                }
//            });
//
//            try {
//                //Creating MimeMessage object
//                MimeMessage mm = new MimeMessage(mSession);
//                //Setting sender address
//                mm.setFrom(new InternetAddress(Global.EMAIL));
//                //Adding receiver
//                mm.addRecipient(Message.RecipientType.TO, new InternetAddress(mEmail));
//                //Adding subject
//                mm.setSubject(mSubject);
//                //Adding message
//                mm.setText(mMessage);
//                //Sending email
//                Transport.send(mm);
//
////            BodyPart messageBodyPart = new MimeBodyPart();
////
////            messageBodyPart.setText(message);
////
////            Multipart multipart = new MimeMultipart();
////
////            multipart.addBodyPart(messageBodyPart);
////
////            messageBodyPart = new MimeBodyPart();
////
////            DataSource source = new FileDataSource(filePath);
////
////            messageBodyPart.setDataHandler(new DataHandler(source));
////
////            messageBodyPart.setFileName(filePath);
////
////            multipart.addBodyPart(messageBodyPart);
//
////            mm.setContent(multipart);
//
//            } catch (MessagingException e) {
//                Log.d(TAG, "MessagingException_ERROR: "+ e.getMessage());
//            }
//
//            handler.post(() -> {
//                /** UI Thread work here */
//            });
//        });

        /** BOILER PLATE*/
//        executor.execute(new Runnable() {
//            @Override
//            public void run() {
//                //doIn Background work here
//
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        //UI Thread work here
//                    }
//                });
//            }
//        });
//    }


//    private void showProgressDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );                        // LayoutInflater = allow to inflate the layout
//        View dialogView = inflater.inflate(R.layout.layout_email_dialog, null);     // View = allow to the view to display in the inflater
//        builder.setCancelable(false);
//        builder.setView(dialogView);  //set the View of Builder
//
//        alertDialog = builder.create();  //Create the DIALOG
//        alertDialog.show();   //Show first before getWindow
//
//        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);  //transparent background
//
//        ImageView mProgressLoadingDialog = alertDialog.findViewById(R.id.progressLoading);
//
////        Glide.with(mContext).asGif().load(R.drawable.progress_loading_blue).into(mProgressLoadingDialog);
//    }

}