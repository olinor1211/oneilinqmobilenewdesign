package com.knx.inquirer.utils;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.knx.inquirer.inqprime.utils.PrimeGlobal;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.disposables.CompositeDisposable;

//Extends: JobService
//Implements: ConnectivityReceiver.ConnectivityReceiverListener  ...for NetworkListener
//@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class NetworkSchedulerService extends JobService implements
        ConnectivityReceiver.ConnectivityReceiverListener {

    private static final String TAG = "NetworkSchedulerService";

//    private static final String TAG = NetworkSchedulerService.class.getSimpleName();

    private ConnectivityReceiver mConnectivityReceiver;  //ConnectivityReceiver
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    //onCreate: Service created
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****NetworkSchedulerService_CLASS_CALLED****");
        mConnectivityReceiver = new ConnectivityReceiver(this);
    }

    /**
     * When the app's NetworkConnectionActivity is created, it starts this service. This is so that the
     * activity and this service can communicate back and forth. See "setUiCallback()"
     */
    //onStartCommand:
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****NetworkSchedulerService_onStartCommand_CALLED****");
        return START_STICKY;  //reCreate the Service after the Process is killed
    }

    //onStartJob: registerReceiver
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i(TAG, "onStartJob " + mConnectivityReceiver);
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****NetworkSchedulerService_onStartJob_CALLED****");
        registerReceiver(mConnectivityReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        return true;
    }

    //onStopJob: unregisterReceiver
    @Override
    public boolean onStopJob(JobParameters params) {
        Log.i(TAG, "onStopJob");
        Log.d(TAG, "onCreate: FLOW_SEQUENCE ****NetworkSchedulerService_onStopJob_CALLED****");
        unregisterReceiver(mConnectivityReceiver);
        return true;
    }

    //Listener for onNetworkConnectionChanged
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            EventBus.getDefault().post("network_access");
        }else{
            EventBus.getDefault().post("no_network_access");
        }
    }
}
