package com.knx.inquirer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.JavaMailApi;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.utils.UserEmailFetcher;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryNotifs;
import com.knx.inquirer.viewmodels.NotifsViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import io.reactivex.disposables.CompositeDisposable;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SettingsActivity";
    private ImageView mBack;
    private TextView mAlert;
    private TextView mAppSetting;
    private TextView mFeedback;
    private TextView mRate;
    private final int PERMISSION_REQUEST_CODE = 02;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    //
    private QBadgeView badgeView;

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sf = new SharedPrefs(this);
        if(sf.getNightMode())setTheme(R.style.AppSettingDarkTheme);
        else setTheme(R.style.AppSettingNormalTheme);
        setContentView(R.layout.activity_settings);

        try{
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        }catch(Exception e){
            Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
        }

        initViews();
        //QBadgeView new Instance
        badgeView = new QBadgeView(SettingsActivity.this);
        updateBadgeCount();
    }

    private void initViews() {
        mBack = findViewById(R.id.burgerBack);
        mAlert = findViewById(R.id.burgerAlert);
        mAppSetting = findViewById(R.id.burgerAppSetting);
        mFeedback = findViewById(R.id.burgerSendFeedback);
        mRate = findViewById(R.id.burgerRateUs);
        mBack.setOnClickListener(this);
        mAlert.setOnClickListener(this);
        mAppSetting.setOnClickListener(this);
        mFeedback.setOnClickListener(this);
        mRate.setOnClickListener(this);
    }

    private void updateBadgeCount(){
        NotifsViewModel notifsViewModel = ViewModelProviders.of(this, new ViewModelFactoryNotifs(getApplication(), compositeDisposable)).get(NotifsViewModel.class);
        notifsViewModel.getUnreadNotifsCount().observe(this, count -> addBadgeAt(4, count));  //count of Unread notif.. pass the position..
    }

    //remove badge in notif temp
    //addBadgeAt method  ..return badgeView
    public Badge addBadgeAt(int position, int number) {
        // add badge
        return badgeView
                .setBadgeNumber(number)  //set number
                .setBadgeTextSize(8,true)
                .setBadgePadding(3,true)
                .setGravityOffset(0, 0, true)  //badge position // .setGravityOffset(12, 2, true)
                .bindTarget(mAlert);  //binding badge to specific item.  //R.id.ivsetting
    }

    //TRANSITION FINISH
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.burgerBack:
                finish();
                break;
            case R.id.burgerAlert:
                finish();
                EventBus.getDefault().post("appAlert");
                break;
            case R.id.burgerAppSetting:
                finish();
                EventBus.getDefault().post("appSetting");
                break;
            case R.id.burgerSendFeedback:
//                getPermissionAndGetEmail();
                sendEmail();
                break;
            case R.id.burgerRateUs:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName()));
//                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.knx.inquirer&hl=en"));
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    //intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
//    public void sendEmail() {
//        String brand = Build.BRAND!=null ? Build.BRAND: "None";
//        String model = Build.MODEL!=null ? Build.MODEL: "None";
//        String version = Build.VERSION.RELEASE!=null ? Build.VERSION.RELEASE: "None";
//        //CHECK First if has GMAIL EMAIL
//        if(!Global.EMAIL.isEmpty()){
//            try{
//                sendViaOthers(brand,model,version); //sendViaGmail
//            }catch (Exception e){
//                Log.d(TAG, "sendEmail: Exception_ERROR: "+e.getMessage());
//            }finally {
//                sendViaOthers(brand,model,version);
//            }
//        }else{
//            try{
//                sendViaOthers(brand,model,version);
//            }catch(Exception e){
//                Log.d(TAG, "sendEmail: Exception_ERROR: "+e.getMessage());
//            }
//        }
//    }

    //Need to fix issue on Permission looping
    public void sendEmail() {
//        Global.EMAIL = getEmails();
        String brand = Build.BRAND!=null ? Build.BRAND: "None";
        String model = Build.MODEL!=null ? Build.MODEL: "None";
        String version = Build.VERSION.RELEASE!=null ? Build.VERSION.RELEASE: "None";
        try{
            sendViaGmail(brand,model,version);
        }catch(Exception e){
            Log.d(TAG, "sendEmail: Exception_ERROR: "+e.getMessage());
            Log.d(TAG, "sendEmail: Exception_ERROR: Going to sendViaOthers");
            sendViaOthers(brand,model,version);
        }
//        finally {
//            sendViaOthers(brand,model,version);
//        }
    }

    private void sendViaOthers(String brand, String model, String version) {
        try{
            Log.d(TAG, "sendViaOthers: ResolveInfo sendViaOthers:");
            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND); //ACTION_SEND //intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
            emailIntent.setType("message/rfc822"); //setType("message/rfc822"); GMAIL or ???? //setType("application/image") //inquirermobile@mymegamobile.com
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"inquirermobile@mymegamobile.com"});  //SAME with  emailIntent.setData(Uri.parse("mailto:"+Global.EMAIL));
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Inquirer Mobile ANDROID\nBRAND: "+brand + ", MODEL: "+model+", VERSION: "+version);
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Please type your feedback here.");
            emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+Global.reportPhotoPath)); //shouldBe: "file:///storage.."   //emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///mnt/sdcard/output.png"));
            startActivity(Intent.createChooser(emailIntent, "Please send the feedback via email.")); //Send feedback via email...

        }catch(Exception e){
            Log.d(TAG, "sendViaOthers: Exception_ERROR "+ e.getMessage());
        }
    }

//    private void sendViaOthers(String brand, String model, String version) {
//        try {
//            Intent emailIntent = new Intent(Intent.ACTION_SENDTO); //ACTION_SENDTO  ACTION_MAIN ACTION_VIEW
////            emailIntent.addCategory(Intent.CATEGORY_APP_EMAIL);
//            emailIntent.setAction(Intent.ACTION_SEND);
//            emailIntent.setType("message/rfc822");
//            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"tips@mymegamobile.com"});  //{"tips@inquirer.net"}); ////{"tips@inquirer.net"});
//            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Inquirer Mobile ANDROID\nBRAND: "+brand + ", MODEL: "+model+", VERSION: "+version);
//            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Please type your feedback here.");
//            emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            this.startActivity(emailIntent);
//        } catch (android.content.ActivityNotFoundException e) {
//            Log.d(TAG, "sendViaOthers: "+ e.getMessage());
//            Toast.makeText(SettingsActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
//        }
//    }





    private void sendViaGmail(String brand, String model, String version) {
////        Intent gmail = new Intent( new Intent(android.content.Intent.ACTION_VIEW));//ACTION_VIEW// //ACTION_SEND
//        Intent gmail = new Intent( new Intent(android.content.Intent.ACTION_SEND));//ACTION_VIEW// //ACTION_SEND
//
////        Intent gmail = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
////                "mailto", Global.EMAIL, null));
////        gmail.resolveActivity(getPackageManager());
////        gmail.setClassName("com.knx.inquirer","com.google.com.google.android.gm.ComposeActivityGmailExternal"); //new://        gmail.setClassName("com.google.android.gm","com.google.com.google.android.gm.ComposeActivityGmailExternal"); //new:
////        gmail.setClassName("com.knx.inquirer","com.google.com.google.android.gm.ComposeActivityGmailExternal"); //new://        gmail.setClassName("com.google.android.gm","com.google.com.google.android.gm.ComposeActivityGmailExternal"); //new:
////        gmail.setData(Uri.parse("mailto:olinor1211@gmail.com"));
////        gmail.setClassName("com.google.android.gm", "com.google.android.gm.ConversationListActivity");
////        gmail.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail")
//
////        gmail.setPackage("com.google.android.gm");
//        gmail.setType("message/rfc822"); // //message/rfc822 //text/plain
//        gmail.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"tips@inquirer.net"}); //RECIPIENT
////        gmail.setData(Uri.parse(Global.EMAIL)); //SENDER
//        gmail.setData(Uri.parse("ronilo@mymegamobile.com"));
//        gmail.putExtra(android.content.Intent.EXTRA_SUBJECT, "BRAND: "+brand + "\nMODEL: "+model+"\nVERSION: "+version);
//        gmail.putExtra(android.content.Intent.EXTRA_TEXT, "GMAIL Content");
////        setFlags(FLAG_GRANT_READ_URI_PERMISSION.or( FLAG_GRANT_WRITE_URI_PERMISSION))
//
////        gmail.resolveActivity(getPackageManager()); //
//        startActivity(gmail);

//        Intent createChooser = Intent.createChooser(gmail,"Open In");
//        PackageManager pm = this.getApplicationContext().getPackageManager();
//        if(createChooser.resolveActivity(pm) != null )
//        {
//            this.startActivity(createChooser);
//        }else{
//            this.startActivity(createChooser);
//        }

        ///OK WORKING FOR REFERENCE: GETTING All IntentActivities in the DEVICE
        final Intent intent = new Intent(android.content.Intent.ACTION_SEND); // ACTION_SEND  ///ACTION_VIEW= to get the VIEW of APP

        final PackageManager pm = getPackageManager(); //Create Instance of PackageManager
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY); // final List<ResolveInfo> matches = pm.queryIntentActivities(intent,0); / GETTING ALL IntentActivities in the DEVICE
        Log.d(TAG, "sendViaGmail: ResolveInfo "+ matches);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches){
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail")){
                Log.d(TAG, "sendViaGmail: ResolveInfo "+ info);
//                sample result info = ResolveInfo{1a81832 com.google.android.gm/.ComposeActivityGmailExternal m=0x608000}
                // sample result in Intent.ACTION_VIEW for ResolveInfo ResolveInfo{67eb0f6 com.google.android.gm/.EmlViewerActivityGmail m=0x608000}
                best = info;
                break;
            }
        }

        if (best != null){
            Log.d(TAG, "sendViaGmail: ResolveInfo "+ best.activityInfo.packageName); //RESULT: com.google.android.gm
            Log.d(TAG, "sendViaGmail: ResolveInfo "+ best.activityInfo.name); //RESULT: com.google.android.gm.ComposeActivityGmailExternal  .. Intent.ACTION_VIEW = com.google.android.gm.EmlViewerActivityGmail
            Log.d(TAG, "sendViaGmail: ResolveInfo going to BEST 1 ");
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        }else{
            Log.d(TAG, "sendViaGmail: ResolveInfo going to BEST 2 ");
            intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmailExternal");
        }
        //
        intent.setType("message/rfc822"); // intent.setType("text/plain");  intent.setType("*/*"); //inquirermobile@mymegamobile.com
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"inquirermobile@mymegamobile.com"}); //RECIPIENT = SAME intent.setData(Uri.parse("mailto:"+Global.EMAIL)); SEND_TO
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "BRAND: "+brand + "\nMODEL: "+model+"\nVERSION: "+version);
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "Please type your feedback here.");
        startActivity(intent);
        Log.d(TAG, "sendViaGmail: ResolveInfo going to BEST 3 ");

//        grantUriPermission(packageName, contentUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        //Finally will execute if NULL
//        intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
//        startActivity(intent);
//        Log.d(TAG, "sendViaGmail: ResolveInfo EMAIL is: "+ Global.EMAIL);
    }

    //    private void sendEmail() {
////
//////        String mail = Global.EMAIL_RECIPIENT;
////        String message = mQuestions.getText().toString() +"\n\n\nFrom:\n"+Global.getOwnerStorename()+"\n"+ sp.getEmail();
////        String subject = category;
////        //Send Mail
//        JavaMailApi javaMailAPI = new JavaMailApi(this,"olinor1211@yahoo.com","subject","message");
////        javaMailAPI.executeService();
//        javaMailAPI.execute();
////
//    }
    //GETTING the GMAIL EMAIL First
//    private void getEmails() {
//        String email = UserEmailFetcher.getEmail(this);
//        Global.EMAIL = email;
//        Log.d(TAG, "UserEmailFetcher: " + email);
////        sendEmail();
//    }

    //GETTING the GMAIL EMAIL First
    private String getEmails() {
        String email = UserEmailFetcher.getEmail(this);
//        Global.EMAIL = email;
//        Log.d(TAG, "UserEmailFetcher: " + email);
        return  email;
//        sendEmail();
    }

//    private String getUserPrimaryEmail(){
//        AccountManager accountManager = AccountManager.get(this);
//        Account[] accounts = accountManager.getAccountsByType("com.google");
////        Account[] accounts = accountManager.getAccountsByType("com.google");
//
////        Account account = getAccount(accountManager);
//
//        return "account";
//    }

    private void getPermissionAndGetEmail() {
        if (!checkPermission(Manifest.permission.READ_CONTACTS)) {
            Log.d(TAG, "NOT YET PERMITTED: ");
            requestPermission(Manifest.permission.READ_CONTACTS);
        } else {
            sendEmail();
            Log.d(TAG, "PERMITTED ALREADY: ");
        }
    }

    private boolean checkPermission(String permission){
        int result = ContextCompat.checkSelfPermission(this, permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(String permission){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
            Toast.makeText(this, "Permission to use your contacts, allows you to send feedback.", Toast.LENGTH_LONG).show();
        }
        ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSION_REQUEST_CODE);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendEmail();
                    Log.d(TAG, "onRequestPermissionsResult: Permission YES");
                } else {
                    Log.d(TAG, "onRequestPermissionsResult: Permission Denied");
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        //check if registered already
        if(!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);  //additional
        Log.d(TAG, "onStart: called ");
        super.onStart();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

//    /**
//     * Create a MimeMessage using the parameters provided.
//     *
//     * @param to email address of the receiver
//     * @param from email address of the sender, the mailbox account
//     * @param subject subject of the email
//     * @param bodyText body text of the email
//     * @return the MimeMessage to be used to send email
//     * @throws MessagingException
//     */


//    public static MimeMessage createEmail(String to, String from, String subject, String bodyText)
//            throws MessagingException {
//        Properties props = new Properties();
//        Session session = Session.getDefaultInstance(props, null);
//
//        MimeMessage email = new MimeMessage(session);
//
//        email.setFrom(new InternetAddress(from));
//        email.addRecipient(javax.mail.Message.RecipientType.TO,
//                new InternetAddress(to));
//        email.setSubject(subject);
//        email.setText(bodyText);
//        return email;
//    }
//
//    /**
//     * Create a message from an email.
//     *
//     * @param emailContent Email to be set to raw of message
//     * @return a message containing a base64url encoded email
//     * @throws IOException
//     * @throws MessagingException
//     */
//    public static Message createMessageWithEmail(MimeMessage emailContent)
//            throws MessagingException, IOException {
//        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//        emailContent.writeTo(buffer);
//        byte[] bytes = buffer.toByteArray();
//        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
//        Message message = new Message();
//        message.setRaw(encodedEmail);
//        return message;
//    }
//
//    /**
//     * Send an email from the user's mailbox to its recipient.
//     *
//     * @param service Authorized Gmail API instance.
//     * @param userId User's email address. The special value "me"
//     * can be used to indicate the authenticated user.
//     * @param emailContent Email to be sent.
//     * @return The sent message
//     * @throws MessagingException
//     * @throws IOException
//     */
//    public static Message sendMessage(Gmail service, String userId, MimeMessage emailContent)
//            throws MessagingException, IOException {
//        Message message = createMessageWithEmail(emailContent);
//        message = service.users().messages().send(userId, message).execute();
//
//        System.out.println("Message id: " + message.getId());
//        System.out.println(message.toPrettyString());
//        return message;
//    }


}
