package com.knx.inquirer.inqprime.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.knx.inquirer.R;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PrimeAccountInfo extends AppCompatActivity {
    private static final String TAG = "PrimeAccountInfo";

//    private ImageView avatar;
//    private TextView username, name, mobileNo, email, gender, address, birthday;
//    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.layout_account_info);
        setContentView(R.layout.activity_prime_account_info);
        //no status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        initViews();


    }



//    //need to override attachBaseContext ..to make the fontPath working
//    @Override
//    protected void attachBaseContext( Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    //Calligraphy3 ADDED for Tablet Android10
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

//    private void initViews() {
//        avatar = findViewById(R.id.prime_accountInfo_avatar);
//        username = findViewById(R.id.prime_accountInfo_userName);
//        name = findViewById(R.id.prime_accountInfo_name);
//        mobileNo = findViewById(R.id.prime_accountInfo_mobileNo);
//        email = findViewById(R.id.prime_accountInfo_email);
//        gender = findViewById(R.id.prime_accountInfo_gender);
//        address = findViewById(R.id.prime_accountInfo_address);
//        birthday = findViewById(R.id.prime_accountInfo_birthday);
//
//
//        PrimeAccountInformationModel primeAccountInfoModel = new PrimeAccountInformationModel();
//
//        String user_name = primeAccountInfoModel.getUserName().isEmpty() ? "" : primeAccountInfoModel.getUserName();
//        String name_ = primeAccountInfoModel.getUserName().isEmpty() ? "" : primeAccountInfoModel.getName();
//        String mobile_no = primeAccountInfoModel.getUserName().isEmpty() ? "" : primeAccountInfoModel.getMobileNo();
//        String email_ = primeAccountInfoModel.getUserName().isEmpty() ? "" : primeAccountInfoModel.getEmail();
//        String gender_ = primeAccountInfoModel.getUserName().isEmpty() ? "" : primeAccountInfoModel.getGender();
//        String address_ = primeAccountInfoModel.getUserName().isEmpty() ? "" : primeAccountInfoModel.getAddress();
//        String birthday_ = primeAccountInfoModel.getUserName().isEmpty() ? "" : primeAccountInfoModel.getBirthday();
//        String avatar_ = primeAccountInfoModel.getUserName().isEmpty() ? String.valueOf(getResources().getDrawable(R.drawable.person_avatar)) : primeAccountInfoModel.getAvatar();
//
//        username.setText(user_name);
//        name.setText(name_);
//        mobileNo.setText(mobile_no);
//        email.setText(email_);
//        gender.setText(gender_);
//        address.setText(address_);
//        birthday.setText(birthday_);
//
//
//        Picasso.with(this)
//                .load(avatar_)
//                .centerInside()
////                .noFade()
////                .error(sect)
////                .placeholder(avatar_)
//                .into(avatar);
//    }
    }
