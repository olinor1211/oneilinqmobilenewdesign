package com.knx.inquirer.inqprime.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PrimeSharedPrefs {

    private static final String PREFERENCE_NAME = "PRIME_SF";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public PrimeSharedPrefs(Context context){
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    /**Switch Sections **/
    public Boolean getSectionSwitch(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_SECTIONS", false);
        return swSection;
    }

    public void setSectionSwitch(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_SECTIONS", swSection);
        editor.apply();
    }

    public Boolean getSectionSwitchNews(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_SECTIONS_NEWS", false);
        return swSection;
    }

    public void setSectionSwitchNews(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_SECTIONS_NEWS", swSection);
        editor.apply();
    }

    public Boolean getSectionSwitchGlobal(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_SECTIONS_GLOBAL", false);
        return swSection;
    }

    public void setSectionSwitchGlobal(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_SECTIONS_GLOBAL", swSection);
        editor.apply();
    }

    public Boolean getSectionSwitchSport(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_SECTIONS_SPORT", false);
        return swSection;
    }

    public void setSectionSwitchSport(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_SECTIONS_SPORT", swSection);
        editor.apply();
    }

    public Boolean getSectionSwitchLife(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_SECTIONS_LIFE", false);
        return swSection;
    }

    public void setSectionSwitchLife(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_SECTIONS_LIFE", swSection);
        editor.apply();
    }

    public Boolean getSectionSwitchBusiness(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_SECTIONS_BUSINESS", false);
        return swSection;
    }

    public void setSectionSwitchBusiness(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_SECTIONS_BUSINESS", swSection);
        editor.apply();
    }

    public Boolean getSectionSwitchTechnology(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_SECTIONS_TECHNOLOGY", false);
        return swSection;
    }

    public void setSectionSwitchTechnology(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_SECTIONS_TECHNOLOGY", swSection);
        editor.apply();
    }

    public Boolean getSectionSwitchOpinion(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_SECTIONS_OPINION", false);
        return swSection;
    }

    public void setSectionSwitchOpinion(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_SECTIONS_OPINION", swSection);
        editor.apply();
    }

    /**Switch Topics **/
    public Boolean getTopicsSwitch(){
        Boolean swTopics = sharedPreferences.getBoolean("SWITCH_TOPICS", false);
        return swTopics;
    }

    public void setTopicsSwitch(Boolean swTopics){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_TOPICS", swTopics);
        editor.apply();
    }

    public Boolean getTopicsSwitchHealth(){
        Boolean swTopics = sharedPreferences.getBoolean("SWITCH_TOPICS_HEALTH", false);
        return swTopics;
    }

    public void setTopicsSwitchHealth(Boolean swTopics){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_TOPICS_HEALTH", swTopics);
        editor.apply();
    }
    public Boolean getTopicsSwitchPolitics(){
        Boolean swTopics = sharedPreferences.getBoolean("SWITCH_TOPICS_POLITICS", false);
        return swTopics;
    }

    public void setTopicsSwitchPolitics(Boolean swTopics){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_TOPICS_POLITICS", swTopics);
        editor.apply();
    }
    public Boolean getTopicsSwitchBeauty(){
        Boolean swTopics = sharedPreferences.getBoolean("SWITCH_TOPICS_BEAUTY", false);
        return swTopics;
    }

    public void setTopicsSwitchBeauty(Boolean swTopics){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_TOPICS_BEAUTY", swTopics);
        editor.apply();
    }
    public Boolean getTopicsSwitchArts(){
        Boolean swTopics = sharedPreferences.getBoolean("SWITCH_TOPICS_ARTS", false);
        return swTopics;
    }

    public void setTopicsSwitchArts(Boolean swTopics){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_TOPICS_ARTS", swTopics);
        editor.apply();
    }
    public Boolean getTopicsSwitchMusic(){
        Boolean swTopics = sharedPreferences.getBoolean("SWITCH_TOPICS_MUSIC", false);
        return swTopics;
    }

    public void setTopicsSwitchMusic(Boolean swTopics){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_TOPICS_MUSIC", swTopics);
        editor.apply();
    }
    public Boolean getTopicsSwitchTravel(){
        Boolean swTopics = sharedPreferences.getBoolean("SWITCH_TOPICS_TRAVEL", false);
        return swTopics;
    }

    public void setTopicsSwitchTravel(Boolean swTopics){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_TOPICS_TRAVEL", swTopics);
        editor.apply();
    }

    public String getLastItemPositionPrime(){
        String id = sharedPreferences.getString("LAST_ITEM_POSITION", "");
        return id;
    }

    public void setLastItemPositionPrime(String id){
        editor = sharedPreferences.edit();
        editor.putString("LAST_ITEM_POSITION", id);
        editor.apply();
    }

    /**Account Info-Account Info**/
    public String getUserName(){
        String id = sharedPreferences.getString("PRIME_USER_NAME", "");
        return id;
    }

    public void setUserName(String id){
        editor = sharedPreferences.edit();
        editor.putString("PRIME_USER_NAME", id);
        editor.apply();
    }

    public String getAccountName(){
        String id = sharedPreferences.getString("PRIME_ACCOUNT_NAME", "");
        return id;
    }

    public void setAccountName(String id){
        editor = sharedPreferences.edit();
        editor.putString("PRIME_ACCOUNT_NAME", id);
        editor.apply();
    }

    public String getMobileNo(){
        String id = sharedPreferences.getString("PRIME_MOBILE_NO", "");
        return id;
    }

    public void setMobileNo(String id){
        editor = sharedPreferences.edit();
        editor.putString("PRIME_MOBILE_NO", id);
        editor.apply();
    }


    public String getAccountEmail(){
        String id = sharedPreferences.getString("PRIME_ACCOUNT_EMAIL", "");
        return id;
    }

    public void setAccountEmail(String id){
        editor = sharedPreferences.edit();
        editor.putString("PRIME_ACCOUNT_EMAIL", id);
        editor.apply();
    }

    public String getUserGender(){
        String id = sharedPreferences.getString("PRIME_USER_GENDER", "");
        return id;
    }

    public void setUserGender(String id){
        editor = sharedPreferences.edit();
        editor.putString("PRIME_USER_GENDER", id);
        editor.apply();
    }

    public String getUserAddress(){
        String id = sharedPreferences.getString("PRIME_USER_ADDRESS", "");
        return id;
    }

    public void setUserAddress(String id){
        editor = sharedPreferences.edit();
        editor.putString("PRIME_USER_ADDRESS", id);
        editor.apply();
    }

    public String getUserBirthday(){
        String id = sharedPreferences.getString("PRIME_USER_BIRTHDAY", "");
        return id;
    }

    public void setUserBirthday(String id){
        editor = sharedPreferences.edit();
        editor.putString("PRIME_USER_BIRTHDAY", id);
        editor.apply();
    }

    public String getUserAvatar(){
        String id = sharedPreferences.getString("PRIME_USER_AVATAR", "");
        return id;
    }

    public void setUserAvatar(String id){
        editor = sharedPreferences.edit();
        editor.putString("PRIME_USER_AVATAR", id);
        editor.apply();
    }


    /**INITIALIZATIONS**/
//    public Boolean hasAlreadyInitialize(){
//        Boolean init = sharedPreferences.getBoolean("INIT_SECTIONS_TOPICS", false);
//        return init;
//    }
//
//    public void setAlreadyInitialize(Boolean init){
//        editor = sharedPreferences.edit();
//        editor.putBoolean("INIT_SECTIONS_TOPICS", init);
//        editor.apply();
//    }


    /**FLAG IF SECTION SELECTED IS SINGLE, MULTIPLE or DEFAULT**/
    public String getSingleNewsSection(){
        String section = sharedPreferences.getString("SINGLE_NEWS_SECTION_", "");
        return section;
    }

    public void setSingleNewsSection(String section){
        editor = sharedPreferences.edit();
        editor.putString("SINGLE_NEWS_SECTION_", section);
        editor.apply();
    }

    public String getDefaultNewsSection(){
        String section = sharedPreferences.getString("DEFAULT_NEWS_SECTION_", "");
        return section;
    }

    public void setDefaultNewsSection(String section){
        editor = sharedPreferences.edit();
        editor.putString("DEFAULT_NEWS_SECTION_", section);
        editor.apply();
    }

    public String getOfflineArticlePhoto(){
        String pic = sharedPreferences.getString("OFFLINE_ARTICLE_PICTURE", "default");
        return pic;
    }

    public void setOfflineArticlePhoto(String pic){
        editor = sharedPreferences.edit();
        editor.putString("OFFLINE_ARTICLE_PICTURE", pic);
        editor.apply();
    }


    public Boolean getPrimeNotifSwitch(){
        Boolean swSection = sharedPreferences.getBoolean("SWITCH_PRIME_NOTIF", false);
        return swSection;
    }

    public void setPrimeNotifSwitch(Boolean swSection){
        editor = sharedPreferences.edit();
        editor.putBoolean("SWITCH_PRIME_NOTIF", swSection);
        editor.apply();
    }



}
