package com.knx.inquirer.inqprime.network;

import com.knx.inquirer.inqprime.model.PrimeNewsBySectionModel;
import com.knx.inquirer.models.Article;

import java.util.ArrayList;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PrimeApiService {

    @GET("init")
    Single<String> getDailyKey();

    @POST("article")
    @FormUrlEncoded
    Single<Article> getPrimeArticle(@Field("device") String deviceId, @Field("id") String id, @Field("md5") String hash);

//    @POST("section")
//    @FormUrlEncoded
//    Call<JsonObject> getPrimeNewsBy(@Field("device") String deviceId, @Field("section") int section, @Field("md5") String hash);
////    Call<ResponseBody> getPrimeNewsBy(@Field("device") String deviceId, @Field("section") int section, @Field("md5") String hash);

    @POST("section")
    @FormUrlEncoded
    Single<ArrayList<PrimeNewsBySectionModel>> getPrimeNewsBySection(@Field("device") String deviceId, @Field("section") int section, @Field("md5") String hash);

//
//    @POST("section")
//    @FormUrlEncoded
//    Call<ArrayList<PrimeNewsBySectionModel>> getPrimeNewsBySection(@Field("device") String deviceId, @Field("section") int section, @Field("md5") String hash);
}
