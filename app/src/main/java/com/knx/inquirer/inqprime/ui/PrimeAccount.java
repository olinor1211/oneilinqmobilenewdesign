package com.knx.inquirer.inqprime.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.utils.PrimeGlobal;
import com.knx.inquirer.inqprime.utils.PrimeSharedPrefs;
import com.knx.inquirer.utils.Global;
import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PrimeAccount extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "PrimeAccount";

    //ui
    private ExpandableLayout expand_accountInfo, expand_accountLog, expand_accountMembership;
    private TextView accountInfo, accountLog, accountMembership, signout;
    private TextView memberEmail, memberEmailBtn, memberPassword, memberPasswordBtn, memberPhone, memberPhoneBtn;
    private TextView memberCard, memberCardBtn, memberPlan, memberPlanBtn, memberBillingDetailsBtn;
    private EditText memberEmailEdit, memberPasswordEdit, memberPhoneEdit, memberCardEdit, memberPlanEdit;
    private TextView emailValidation, passwordValidation, phoneValidation, planValidation, cardValidation;
    private TextView planBasic, planStandard, planPremium;
//    private Spinner planSpinner;
    private ImageButton arrowBack;
    private RelativeLayout planSelection;


    //utils
    private PrimeSharedPrefs psp;
    private static final int PICK_IMAGE = 1;
    private DatabaseHelper helper;

    //variables
    private Uri imageUri;
    private String cardOriginalNo;
    private String planSpinList;




    //account info
    private ImageView avatar;
    private TextView username, name, mobileNo, email, gender, address, birthday;
    private FloatingActionButton fabAccountInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prime_account);
        //no status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initViews();
    }

    private void initViews() {
        psp = new PrimeSharedPrefs(this);
        helper = new DatabaseHelper(this);

        expand_accountInfo = findViewById(R.id.expand_accountInfo);
        expand_accountLog = findViewById(R.id.expand_accountLog);
        expand_accountMembership = findViewById(R.id.expand_accountMembership);

        accountInfo = findViewById(R.id.prime_tvAccountInfo);
        accountLog = findViewById(R.id.prime_tvAccountLog);
        accountMembership = findViewById(R.id.prime_tvAccountMembership);
        signout = findViewById(R.id.prime_tvLogout);
        emailValidation = findViewById(R.id.prime_accountMembership_emailValidation);
        passwordValidation = findViewById(R.id.prime_accountMembership_passwordValidation);
        phoneValidation = findViewById(R.id.prime_accountMembership_phoneValidation);
        planValidation = findViewById(R.id.prime_accountMembership_planValidation);
        cardValidation = findViewById(R.id.prime_accountMembership_cardValidation);
        planSelection = findViewById(R.id.prime_rl_accountMembership_accountPlan);
        planBasic = findViewById(R.id.planBasic);
        planStandard = findViewById(R.id.planStandard);
        planPremium = findViewById(R.id.planPremium);

//        planSpinner = findViewById(R.id.prime_accountMembership_accountPlan_Spinner);

//        planValidation = findViewById(R.id.prime_accountMembership_planValidation);


        //billing membership
        memberEmail = findViewById(R.id.prime_accountMembership_accountEmail);
        memberEmailEdit = findViewById(R.id.prime_accountMembership_accountEmail_Edit);
        memberEmailBtn = findViewById(R.id.prime_accountMembership_accountEmailButton);
        memberPassword = findViewById(R.id.prime_accountMembership_accountPassword);
        memberPasswordEdit = findViewById(R.id.prime_accountMembership_accountPassword_Edit);
        memberPasswordBtn = findViewById(R.id.prime_accountMembership_accountPasswordButton);
        memberPhone = findViewById(R.id.prime_accountMembership_accountPhone);
        memberPhoneEdit = findViewById(R.id.prime_accountMembership_accountPhone_Edit);
        memberPhoneBtn = findViewById(R.id.prime_accountMembership_accountPhoneButton);
        memberCard = findViewById(R.id.prime_accountMembership_accountCard);
        memberCardEdit = findViewById(R.id.prime_accountMembership_accountCard_Edit);
        memberCardBtn = findViewById(R.id.prime_accountMembership_accountCardButton);
        memberPlan = findViewById(R.id.prime_accountMembership_accountPlan);
//        memberPlanEdit = findViewById(R.id.prime_accountMembership_accountPlan_Edit);
        memberPlanBtn = findViewById(R.id.prime_accountMembership_accountPlanButton);
        memberBillingDetailsBtn = findViewById(R.id.prime_accountMembership_accountBillingDetails);


        //account info
        avatar = findViewById(R.id.prime_accountInfo_avatar);
        username = findViewById(R.id.prime_accountInfo_userName);
        name = findViewById(R.id.prime_accountInfo_name);
        mobileNo = findViewById(R.id.prime_accountInfo_mobileNo);
        email = findViewById(R.id.prime_accountInfo_email);
        gender = findViewById(R.id.prime_accountInfo_gender);
        address = findViewById(R.id.prime_accountInfo_address);
        birthday = findViewById(R.id.prime_accountInfo_birthday);
        fabAccountInfo = findViewById(R.id.fab_accountInfo);

        arrowBack = findViewById(R.id.prime_account_arrowBack);


        accountInfo.findViewById(R.id.prime_tvAccountInfo).setOnClickListener(this);
        accountLog.findViewById(R.id.prime_tvAccountLog).setOnClickListener(this);
        accountMembership.findViewById(R.id.prime_tvAccountMembership).setOnClickListener(this);
        signout.findViewById(R.id.prime_tvLogout).setOnClickListener(this);
        fabAccountInfo.findViewById(R.id.fab_accountInfo).setOnClickListener(this);
        arrowBack.findViewById(R.id.prime_account_arrowBack).setOnClickListener(this);
        memberEmailBtn.findViewById(R.id.prime_accountMembership_accountEmailButton).setOnClickListener(this);
        memberPasswordBtn.findViewById(R.id.prime_accountMembership_accountPasswordButton).setOnClickListener(this);
        memberPhoneBtn.findViewById(R.id.prime_accountMembership_accountPhoneButton).setOnClickListener(this);
        memberCardBtn.findViewById(R.id.prime_accountMembership_accountCardButton).setOnClickListener(this);
        memberPlanBtn.findViewById(R.id.prime_accountMembership_accountPlanButton).setOnClickListener(this);
        memberBillingDetailsBtn.findViewById(R.id.prime_accountMembership_accountBillingDetails).setOnClickListener(this);
        planBasic.findViewById(R.id.planBasic).setOnClickListener(this);
        planStandard.findViewById(R.id.planStandard).setOnClickListener(this);
        planPremium.findViewById(R.id.planPremium).setOnClickListener(this);

        //account info
        username.setText(PrimeGlobal.accountUserName);
        name.setText(PrimeGlobal.accountName);
        mobileNo.setText(PrimeGlobal.accountMobileNo);
        email.setText(PrimeGlobal.accountEmail);
        gender.setText(PrimeGlobal.accountGender);
        address.setText(PrimeGlobal.accountAddress);
        birthday.setText(PrimeGlobal.accountBirthday);

        //billing membership
        memberPhone.setText(PrimeGlobal.accountMobileNo);
//        memberPhone.setText("43241 342");
        memberEmail.setText(PrimeGlobal.accountEmail);
        memberPassword.setText("password: ********");
        memberPlan.setText(PrimeGlobal.accountPlan);
        //CARD number
        if (!PrimeGlobal.accountCardNo.isEmpty() && PrimeGlobal.accountCardNo.length() > 4) {
            String cardNo = PrimeGlobal.accountCardNo.substring(PrimeGlobal.accountCardNo.length() - 4);
            memberCard.setText(" •••• •••• •••• " + cardNo);
        } else {
            memberCard.setText("");

        }
        //AVATAR
        try{
            Picasso.get()
                    .load(PrimeGlobal.accountAvatar)
                    .fit()
//                .noFade()
//                .error(sect)
                    .placeholder(R.drawable.person_avatar)
                    .into(avatar);
        }catch (Exception e){
            Log.d(TAG, "initViewsException: "+e.getMessage());
        }
    }

    //need to override attachBaseContext ..to make the fontPath working
//    @Override
//    protected void attachBaseContext( Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    //Calligraphy3 ADDED for Tablet Android10
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.prime_tvAccountInfo:
                if(expand_accountInfo.isExpanded()){
                    expand_accountInfo.collapse();
                }else expand_accountInfo.expand();
                break;

            case R.id.prime_tvAccountLog:
                if(expand_accountLog.isExpanded()){
                    expand_accountLog.collapse();
                }else expand_accountLog.expand();
                break;

            case R.id.prime_tvAccountMembership:
                if(expand_accountMembership.isExpanded()){
                    expand_accountMembership.collapse();
                }else expand_accountMembership.expand();
                break;

            case R.id.prime_tvLogout:
                Toast.makeText(this, "Logging out..", Toast.LENGTH_SHORT).show();
                break;
            case R.id.fab_accountInfo:
                selectImage();
                break;

            case R.id.prime_accountMembership_accountEmailButton:
                String emailBtn = memberEmailBtn.getText().toString();
                if(emailBtn.equals("Change account email")){
                    changeEmail();
                }else displayChangedEmail();
                break;

            case R.id.prime_accountMembership_accountPasswordButton:
                String passBtn = memberPasswordBtn.getText().toString();
                if(passBtn.equals("Change password")){
                    changePassword();
                }else displayChangedPassword();
                break;

            case R.id.prime_accountMembership_accountPhoneButton:
                 String phoneBtn = memberPhoneBtn.getText().toString();
                if(phoneBtn.equals("Change phone number")){
                    changePhone();
                }else displayChangedPhone();
                break;

            case R.id.prime_accountMembership_accountCardButton:
                String cardBtn = memberCardBtn.getText().toString();
                if(cardBtn.equals("Change card number")){
                    changeCard();
                }else displayChangedCard();
                break;

            case R.id.prime_accountMembership_accountPlanButton:
                String planBtn = memberPlanBtn.getText().toString();
                if(planBtn.equals("Change plan")){
                    changePlan();
                }else displayChangedPlan();
                break;

            case R.id.prime_accountMembership_accountBillingDetails:
                Toast.makeText(this, "Billing Details", Toast.LENGTH_SHORT).show();
                break;

            case R.id.prime_account_arrowBack:
                onBackPressed();
                break;

            case R.id.planBasic:
                planSelection.setVisibility(View.INVISIBLE);
                memberPlan.setVisibility(View.VISIBLE);
                memberPlan.setText("Basic");
                break;

            case R.id.planStandard:
                planSelection.setVisibility(View.INVISIBLE);
                memberPlan.setVisibility(View.VISIBLE);
                memberPlan.setText("Standard");
                break;

            case R.id.planPremium:
                planSelection.setVisibility(View.INVISIBLE);
                memberPlan.setVisibility(View.VISIBLE);
                memberPlan.setText("Premium");
                break;

        }
    }

    private void displayChangedEmail() {
        //Check Net Connection
        if(Global.isNetworkAvailable(this)){
            if(isValidEmail(memberEmailEdit.getText().toString())){
                memberEmailEdit.setVisibility(View.INVISIBLE);
                memberEmail.setText(memberEmailEdit.getText().toString());
                memberEmail.setVisibility(View.VISIBLE);
                memberEmailBtn.setText("Change account email");
                emailValidation.setVisibility(View.INVISIBLE);
                email.setText(memberEmail.getText().toString());  //update account info temporarily
                helper.updatePrimeEmail(memberEmail.getText().toString(),PrimeGlobal.accountId); //update database

            }else{
                emailValidation.setVisibility(View.VISIBLE);
                memberEmailEdit.setFocusable(true);
                emailValidation.setText("Invalid email");
                Log.d(TAG, "displayChangedEmail: "+ isValidEmail(memberEmail.getText().toString()));
            }
        }else {
            emailValidation.setVisibility(View.VISIBLE);
            emailValidation.setText("No network connection");
        }

    }

    private void changeEmail() {
        memberEmail.setVisibility(View.INVISIBLE);
        memberEmailEdit.setText(memberEmail.getText().toString());
        memberEmailEdit.setVisibility(View.VISIBLE);
        memberEmailEdit.setFocusable(true);
        memberEmailEdit.setTextColor(getResources().getColor(R.color.prime_toggle_uncheck));
        memberEmailBtn.setText("Done");

    }


    private void displayChangedPassword() {

        //Check Net Connection
        if(Global.isNetworkAvailable(this)){
            passwordValidation.setVisibility(View.INVISIBLE);
            memberPasswordEdit.setVisibility(View.INVISIBLE);
//        memberPassword.setText(memberEmailEdit.getText().toString());
            memberPassword.setVisibility(View.VISIBLE);
            memberPasswordBtn.setText("Change password");

            String password = memberEmailEdit.getText().toString();
            //save password to server
//        helper.updatePrimeEmail(memberEmail.getText().toString(),PrimeGlobal.accountId); //update database

        }else {
            passwordValidation.setVisibility(View.VISIBLE);
            passwordValidation.setText("No network connection");
        }

    }

    private void changePassword() {
        memberPassword.setVisibility(View.INVISIBLE);
        memberPasswordEdit.setText("********");
        memberPasswordEdit.setVisibility(View.VISIBLE);
        memberPasswordEdit.setFocusable(true);
        memberPasswordEdit.setTextColor(getResources().getColor(R.color.prime_toggle_uncheck));
        memberPasswordBtn.setText("Done");
    }


    private void displayChangedPhone() {
        //Check Net Connection
        if(Global.isNetworkAvailable(this)){
            try{
                int length = memberPhoneEdit.getText().toString().isEmpty() ? 0 : memberPhoneEdit.getText().toString().length();

                if(length !=13){
                    phoneValidation.setVisibility(View.VISIBLE);
                    phoneValidation.setText("Invalid phone number");
                }else if(!memberPhoneEdit.getText().toString().substring(0,4).equals("+639")){  //resolving OutOfBoundsException
                    phoneValidation.setVisibility(View.VISIBLE);
                    phoneValidation.setText("Invalid phone number");
                }else{
                    memberPhoneEdit.setVisibility(View.INVISIBLE);
                    memberPhone.setText(memberPhoneEdit.getText().toString());
                    memberPhone.setVisibility(View.VISIBLE);
                    memberPhoneEdit.setFocusable(true);
                    memberPhoneBtn.setText("Change phone number");
                    phoneValidation.setVisibility(View.INVISIBLE);

                    mobileNo.setText(memberPhoneEdit.getText().toString());  //update account info temporarily
                    helper.updatePrimeMobileNo(memberPhoneEdit.getText().toString(),PrimeGlobal.accountId); //update database
                }

            }catch (Exception e){
                Log.d(TAG, "displayChangedPhone Exception: "+ e.getMessage());
            }

        }else {
            phoneValidation.setVisibility(View.VISIBLE);
            phoneValidation.setText("No network connection");
        }



    }

    private void changePhone() {
        memberPhone.setVisibility(View.INVISIBLE);
        memberPhoneEdit.setText(memberPhone.getText().toString());
        memberPhoneEdit.setVisibility(View.VISIBLE);
        memberPhoneEdit.setFocusable(true);
        memberPhoneEdit.setTextColor(getResources().getColor(R.color.prime_toggle_uncheck));
        memberPhoneBtn.setText("Done");
    }


    //CARD
    private void displayChangedCard() {

        //Check Net Connection
        if(Global.isNetworkAvailable(this)){
            try{
                int length = memberCardEdit.getText().toString().length();
                //don't check first if it's integer to be able to display the string text if there is no change
                if(length !=16){
                    cardValidation.setVisibility(View.VISIBLE);
                    cardValidation.setText("Invalid card number");
                }else{
                    String card_no = memberCardEdit.getText().toString();
                    //check there is no changed
                    if(!cardOriginalNo.equals(card_no)){
                        //check if it's integer.
                        if(TextUtils.isDigitsOnly(memberCardEdit.getText())){
                            memberCardEdit.setVisibility(View.INVISIBLE);
                            cardValidation.setVisibility(View.INVISIBLE);
                            memberCard.setVisibility(View.VISIBLE);
                            memberCardBtn.setText("Change card number"); //change button
                            String card = card_no.substring(memberCardEdit.getText().toString().length()-4);
                            memberCard.setText(" •••• •••• •••• " + card);
                            //card_no save to server
                            PrimeGlobal.accountCardNo = card_no;  //update global
                            helper.updatePrimeCardNo(card_no,PrimeGlobal.accountId); //update database
                        }else{
                            cardValidation.setVisibility(View.VISIBLE);
                            cardValidation.setText("Invalid card number ");
                        }

                    }else{
                        memberCardEdit.setVisibility(View.INVISIBLE);
                        cardValidation.setVisibility(View.INVISIBLE);
                        memberCard.setVisibility(View.VISIBLE);
                        memberCardBtn.setText("Change card number"); //change button
                        memberCard.setText("••••••••••••"+PrimeGlobal.accountCardNo.substring(PrimeGlobal.accountCardNo.length()-4));
                    }
                }
            }catch (Exception e){
                Log.d(TAG, "displayChangedCardException: "+ e.getMessage());
            }

        }else {
            cardValidation.setVisibility(View.VISIBLE);
            cardValidation.setText("No network connection");
        }



    }

    private void changeCard() {
        try{
            cardOriginalNo = PrimeGlobal.accountCardNo.isEmpty() ? PrimeGlobal.accountCardNo : "••••••••••••"+PrimeGlobal.accountCardNo.substring(PrimeGlobal.accountCardNo.length()-4);
            memberCard.setVisibility(View.INVISIBLE);
            memberCardEdit.setText(cardOriginalNo);
            memberCardEdit.setVisibility(View.VISIBLE);
            memberCardEdit.setFocusable(true);
            memberCardEdit.setTextColor(getResources().getColor(R.color.prime_toggle_uncheck));
            memberCardBtn.setText("Done");
        }catch (Exception e){
            Log.d(TAG, "changedCardException: "+ e.getMessage());
        }


    }



    //PLAN
    private void displayChangedPlan() {

        //Check Net Connection
        if(Global.isNetworkAvailable(this)){
            planValidation.setVisibility(View.INVISIBLE);
            memberPlanBtn.setText("Change plan");
            //save to server
            helper.updatePrimePlan(memberPlan.getText().toString(),PrimeGlobal.accountId); //update database

        }else {
            planValidation.setVisibility(View.VISIBLE);
            planValidation.setText("No network connection");
        }


    }

    private void changePlan() {
        memberPlan.setVisibility(View.INVISIBLE);
        planSelection.setVisibility(View.VISIBLE);
        memberPlanBtn.setText("Done");
    }






    //SELECT IMAGE ActivityForResult
    private void selectImage() {
        Intent gallery = new Intent();
        gallery.setType("image/*");
        gallery.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(gallery, "Select Picture"), PICK_IMAGE);
    }

    //onActivityResult for SELECT IMAGE
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {

            imageUri = data.getData();
            savePhoto(imageUri);  //upload photo
        }
    }


    private void savePhoto ( Uri imageUri){

        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";  //image string filename  unique
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);  //go to the fileDirectory

        try {
            File imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);   //creating the image from fileDirectory //
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);  // URI to bitmap

            FileOutputStream fos = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);  //compress bitmap
            fos.flush();
            fos.close();

            //
            String path = imageFile.getAbsolutePath();
            String filename = path.substring(path.lastIndexOf("/")+1);
            Uri avatar = Uri.fromFile(new File(path));  //storage filename to Uri


            helper.updatePrimeAvatar(avatar.toString(),PrimeGlobal.accountId);  //update profile picture  Uri.toString

            loadPicture(); //load
            deleteOldFile(filename); //delete all old files except the NEW ONE
            Log.d(TAG, "savePhoto: path " + path);
            Log.d(TAG, "savePhoto: imageFile.toString() " + imageFile.toString());
            Log.d(TAG, "savePhoto: Actual Image, Send to Server " + filename);


        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "IMAGE: " + psp.getUserAvatar());

        //send to server the

    }

    //
    private void loadPicture(){
        Picasso.get()
                .load(imageUri)
                .fit()
                .placeholder(R.drawable.person_avatar)
                .into(avatar);
    }

    private boolean isValidEmail(CharSequence email) {
        if (email == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    //delete file except this one
    private void deleteOldFile(String file) {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);  //go to the fileDirectory of the Apps

        //if Directory exist  ..Delete all files
        if (storageDir.exists() && storageDir.isDirectory()) {
            String[] children = storageDir.list();
            for (int i = 0; i < children.length; i++) {
                if(!children[i].equals(file)){  //
                    new File(storageDir, children[i]).delete();
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}



















//convert to bitmap
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
//                bitmap.
//                psp.setUserAvatar(String.valueOf(bitmap));
//                Log.d(TAG, "onActivityResult: IMAGE URI "+ psp.getUserAvatar());
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }



//Convert Bitmap to File .. Compress bitmap.. Save Image to Storage
//    String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//    String imageFileName = "JPEG_" + timeStamp + "_";  //image string filename  unique
//    File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);  //go to the fileDirectory
//
//        try {
//                File imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);   //creating the image from fileDirectory //
//
//                FileOutputStream fos = new FileOutputStream(imageFile);
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);  //compress bitmap
//                fos.flush();
//                fos.close();
//
//                String path = imageFile.getAbsolutePath();  //get path
//                sf.setSignatureFilePath(path);  //save to sf
////
////            String filename = path.substring(path.lastIndexOf("/")+1);
////            Toast.makeText(this, "absolute : "+path, Toast.LENGTH_SHORT).show();
////            Toast.makeText(this, "filename : "+filename, Toast.LENGTH_SHORT).show();
//                } catch (IOException e) {
//                e.printStackTrace();
//                }



////        image URI to File to Bitmap
//        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//        String imageFileName = "JPEG" + timeStamp + "_";  //image string filename  unique
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);  //go to the fileDirectory
//
//        File imageFile = null;   //creating the image from fileDirectory //
//        try {
////            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);  // URI to bitmap
////            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver() , Uri.parse(imageUri)); // URI to bitmap
//
////            imageFile = File.createTempFile(String.valueOf(imageUri), ".jpg", storageDir);
//            imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
//
////            FileOutputStream fos = new FileOutputStream(imageFile);
////            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);  //compress bitmap
////            fos.flush();
////            fos.close();
//
//            String path = imageFile.getAbsolutePath();  //get path then send to server
//            psp.setUserAvatar(imageUri.toString());  //uri to string
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
////            psp.setUserAvatar(imageUri.toString());  //uri to string
////        imageUri = URI.create(myPrefs.getString("url", "defaultString"));
//




//        String user_name = psp.getUserName()=="" ? "" : psp.getUserName();
//        String name_ = psp.getAccountName()=="" ? "" : psp.getAccountName();
//        String mobile_no = psp.getMobileNo()=="" ? "" : psp.getMobileNo();
//        String email_ = psp.getAccountEmail()=="" ? "" : psp.getAccountEmail();
//        String gender_ = psp.getUserGender()=="" ? "" : psp.getUserGender();
//        String address_ = psp.getUserAddress()=="" ? "" : psp.getUserAddress();
//        String birthday_ = psp.getUserBirthday()=="" ? "" : psp.getUserBirthday();
//        String avatar_ = psp.getUserAvatar()=="" ? String.valueOf(getResources().getDrawable(R.drawable.person_avatar)) : psp.getUserAvatar();
//


//            savePhoto(imageUri);  //upload photo
//            loadPicture(imageUri);
////            Uri avatarImage = Uri.parse(imageUri.toString());
////            avatarImage = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//
////            final Uri contentUri = ContentUris.withAppendedId(
//
////            String selectedFilePath = FilePath.getPath(getActivity(), uri);
////            final File file = new File(selectedFilePath);
//


//    String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@" +  //part before @
//            "(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
//
//    Pattern pat = Pattern.compile(emailRegex);
//		if (email == null)
//                return false;
//                return pat.matcher(email).matches();


//EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" // + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


//    public final static boolean isValidEmail(CharSequence target) {
//        if (target == null)
//            return false;
//
//        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
//    }



    ///PLAN  ORIGINAL
//    private void displayChangedPlan() {
//        String input = memberPlanEdit.getText().toString();
//        if(!input.isEmpty()){
//            memberPlanEdit.setVisibility(View.INVISIBLE);
//            planValidation.setVisibility(View.INVISIBLE);
//            memberPlan.setText(memberPlanEdit.getText().toString());
//            memberPlan.setVisibility(View.VISIBLE);
//
//            memberPlanBtn.setText("Change plan");
//            //save to server
//            helper.updatePrimePlan(memberPlan.getText().toString(),PrimeGlobal.accountId); //update database
//        }else{
//            planValidation.setVisibility(View.VISIBLE);
//            //spinner
//        }
//
//    }
//
//    private void changePlan() {
//        memberPlan.setVisibility(View.INVISIBLE);
//        memberPlanEdit.setText(memberPlan.getText().toString());
//        memberPlanEdit.setVisibility(View.VISIBLE);
//        memberPlanEdit.setFocusable(true);
//        memberPlanEdit.setTextColor(getResources().getColor(R.color.prime_toggle_uncheck));
//        memberPlanBtn.setText("Done");
//    }









//SPINNER
//    private void changePlan() {
//        planSpinner.setVisibility(View.VISIBLE);
//        memberPlan.setVisibility(View.INVISIBLE);
//        ArrayAdapter<String> myAdapter = new ArrayAdapter(PrimeAccount.this, R.layout.prime_dropdown_acct_plan, getResources().getStringArray(R.array.plan_list)){
//
//////            @Override
//////            public boolean isEnabled(int position) {
//////                if (position == 0) {
//////                    // Disable the first item from Spinner
//////                    // First item will be use for hint
//////                    return false;
//////                } else {
//////                    return true;
//////                }
//////            }
////
//
//            //                //getDropDownView SpinnerView
//            public View getDropDownView(int position, View convertView,
//                                        ViewGroup parent) {
//                View view = super.getDropDownView(position, convertView, parent);
//                TextView tv = (TextView) view;
//                tv.setTextColor(Color.WHITE);  //setting the textColor here
//                tv.setTextSize(13);
//                tv.setCompoundDrawables(null,null,null,null);  //removing the drawableRight...
//
//                if (position == 6) {
//                    tv.setPadding(50,40,0,40);
//                }
//                else {
//                    tv.setPadding(50,40,0,0);
//
//                }
//                return view;
//            }
//        };
////
//        planSpinner.setAdapter(myAdapter);
//        planSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                planSpinList = adapterView.getItemAtPosition(i).toString();
//                Log.d(TAG, "onItemSelected: Spin "+planSpinList);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//
//        });
//    }