package com.knx.inquirer.inqprime.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "PrimeSections")
public class PrimeSectionsModel {

    //Dont autoGenerate primary key to make onConflict = OnConflictStrategy.REPLACE take effect.. making newsId as primary key
//    @PrimaryKey(autoGenerate = true)
//    private int id;


    public PrimeSectionsModel() {

    }

    @PrimaryKey
    @NonNull
    private int primeId;

    private String PrimeSections;
    private Boolean Status;

    public PrimeSectionsModel(int primeId, String primeSections, Boolean status) {
        this.primeId = primeId;
        PrimeSections = primeSections;
        Status = status;
    }

    public int getPrimeId() {
        return primeId;
    }

    public String getPrimeSections() {
        return PrimeSections;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setPrimeId(int primeId) {
        this.primeId = primeId;
    }

    public void setPrimeSections(String primeSections) {
        PrimeSections = primeSections;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }
}
