package com.knx.inquirer.inqprime.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.inqprime.model.PrimeNewsBySectionModel;
import com.knx.inquirer.inqprime.repositories.RepositoryPrimeNewsBySection;
import java.util.List;

public class ViewModelPrimeNewsBySection extends AndroidViewModel {

    private RepositoryPrimeNewsBySection repositoryPrimeNewsBySection;

    public ViewModelPrimeNewsBySection(@NonNull Application application) {
        super(application);
        repositoryPrimeNewsBySection = new RepositoryPrimeNewsBySection();
    }

    public LiveData<List<PrimeNewsBySectionModel>> getPrimeNewsBySection(int section){
        return repositoryPrimeNewsBySection.getPrimeNewsBySection(section);
    }

    public LiveData<Boolean> getPrimeNewsBySectionError(){
        return repositoryPrimeNewsBySection.getPrimeNewsBySectionError();
    }

    public void getAllPrimeNewsBySection(int section){
        repositoryPrimeNewsBySection.getAllPrimeNewsBySection(section); //Just Call the repository
    }


//    public LiveData<List<NewsModel>> getCachedNews(){
//        return newsRepository.getCachedNews();
//    }
//
//    public LiveData<Boolean> getNewsError(){
//        return  newsRepository.getNewsError();
//    }
//
//    public LiveData<Throwable> getNewsThrowable(){
//        return  newsRepository.getNewsThrowable();
//    }
//
//    public LiveData<Integer> getNewsRecordCount(){
//        return  newsRepository.getNewsRecordCount();
//    }
}
