package com.knx.inquirer.inqprime.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.utils.OnViewHolderItemListener;
import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.utils.Global;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RVAdapterPrimeNotification extends RecyclerView.Adapter<RVAdapterPrimeNotification.ItemViewHolder>  {
    private static final String TAG = "RVAdapterPrimeNotificat";

    private List<NotificationModel> notifications;
    private Context context;
    private OnViewHolderItemListener mOnViewHolderItemListener;  //declare interface in RV adapter
    private DatabaseHelper helper;

    public RVAdapterPrimeNotification(Context context, List<NotificationModel> notifications, OnViewHolderItemListener onViewHolderItemListener) {
        this.notifications = notifications;
        this.context = context;
        this.mOnViewHolderItemListener = onViewHolderItemListener;
        this.helper = helper;

    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.prime_notification_list_row, parent, false);
        return new ItemViewHolder(v, mOnViewHolderItemListener);  //add interface as an additional parameter

    }


    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {

        //INCLUDE THE METHOD HERE or BIND THE METHOD HERE
        itemViewHolder.bind();

//        Showing data on the views
        String message = notifications.get(position).getMessage();
        String image =  notifications.get(position).getImage();
        String date = Global.convertDateToString(notifications.get(position).getTimestamp());


        itemViewHolder.newsData.setText(message.replaceAll("\\n", ""));
        itemViewHolder.notifDate.setText(date);
        ImageView imageV = itemViewHolder.notifImage;

        if (!image.isEmpty()) {
                Picasso.get()
                        .load(image)
                        .noFade()
                        .noPlaceholder()
                        .into(imageV);
        }

    }


    @Override
    public int getItemCount() {
        return notifications == null ? 0 : notifications.size(); //resolving null
    }

    //ItemViewHolder  //REMOVE  implements View.OnClickListener CHANGE WITH bind method for Multiple CLick Components
    public class ItemViewHolder extends RecyclerView.ViewHolder{

        ImageView notifImage;
        FrameLayout deleteLayout;
        TextView newsData, notifDate;
        LinearLayout notifHolder;   //notifHolder
        OnViewHolderItemListener onViewHolderItemListener;


        public ItemViewHolder(@NonNull View itemView, OnViewHolderItemListener onViewHolderItemListener) {
            super(itemView);
            this.onViewHolderItemListener = onViewHolderItemListener;


            notifHolder = itemView.findViewById(R.id.prime_ll_notification_items);  //prime_notif_holder
            newsData = itemView.findViewById(R.id.prime_notif_data);
            notifDate = itemView.findViewById(R.id.prime_notif_date);
            notifImage = itemView.findViewById(R.id.prime_notif_image);
            deleteLayout = itemView.findViewById(R.id.delete_layout_prime_notif);

            //attaching itemView to the interface..
//            itemView.setOnClickListener(this);
        }

        //call/use the interface method, ..getAdapterPosition() as the parameter  ..REMOVED onClick change with Bind method
//        @Override
//        public void onClick(View v) {
//            onViewHolderItemListener.onViewHolderItemClick(v, getAdapterPosition());  //
//
//        }


        //binded/invoked in onBindViewHolder.. BINDING THE COMPONENTS to be Accessed in UI
        public void bind() {
            //BACK LAYOUT
            deleteLayout.setOnClickListener(v -> {
                onViewHolderItemListener.onViewHolderItemClick(v, getAdapterPosition()); //onViewHolderItemClick
            });
            //FRONT LAYOUT
            notifHolder.setOnClickListener(view -> onViewHolderItemListener.onViewHolderItemClick(view,getAdapterPosition()));  //onViewHolderItemClick
            notifImage.setOnClickListener(view -> onViewHolderItemListener.onViewHolderItemClick(view,getAdapterPosition()));  //onViewHolderItemClick
        }
    }

}