package com.knx.inquirer.inqprime.repositories;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeNewsBySectionModel;
import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;
import com.knx.inquirer.inqprime.network.PrimeApiService;
import com.knx.inquirer.inqprime.utils.AsyncTaskRunner;
import com.knx.inquirer.inqprime.utils.PrimeGlobal;
import com.knx.inquirer.models.Article;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class RepositoryPrimeNewsBySection {
    private static final String TAG = "RepositoryPrimeNewsBySe";

    private DatabaseHelper helper;
    private Context context;
    private MutableLiveData<List<PrimeNewsBySectionModel>> mutableNewsData;  //PrimeNewsBySectionModel
    private MutableLiveData<Boolean> isPrimeNewsBySectionRepoError;
    private Handler runnableHandler = new Handler();
    private ArrayList<PrimeNewsBySectionModel> newsList = new ArrayList<>();
    private List<PrimeNewsOfflineModel> newsPrimeListOffline = new ArrayList<>();
    int countTest = 0; //tester
    
    //    private PrimeSharedPrefs psp;
//    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable;
    private PrimeApiService primeApiService;
    private MutableLiveData<List<PrimeNewsOfflineModel>> newsPrime;  //PrimeNewsBySectionModel


    public RepositoryPrimeNewsBySection() {
        helper = new DatabaseHelper(context);
        compositeDisposable = new CompositeDisposable();
    }

    public MutableLiveData<List<PrimeNewsBySectionModel>> getPrimeNewsBySection(int section) {
        isPrimeNewsBySectionRepoError = new MutableLiveData<>();
//        String deviceKey = "4616724363963694"; //tester  //4616724363963694
//        String deviceId = "f8fcf876237111fb";  //tester
        mutableNewsData = new MutableLiveData<>();
        primeApiService = ServiceGenerator.createService(PrimeApiService.class);

        //Chained API callback, getDailyKey() and getPrimeNewsBySection()
        primeApiService.getDailyKey()
                .flatMap((Function<String, SingleSource<ArrayList<PrimeNewsBySectionModel>>>) key -> {
                    String hashed = Global.Generate32SHA512(key + "inq" + PrimeGlobal.DEVICE_ID + "-megasection/" + section);
                    PrimeGlobal.DAILY_KEY = key;  //daily key
                    Log.d(TAG, "getPrimeNewsBySection: DAILYKEY "+ key);
                    //return another API Callback after the 1st Api callback
                    return primeApiService.getPrimeNewsBySection(PrimeGlobal.DEVICE_ID, section, hashed);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<PrimeNewsBySectionModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe:getPrimeNewsBySection called ");
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ArrayList<PrimeNewsBySectionModel> news) {
                        Log.d(TAG, "onSuccess:getPrimeNewsBySection called "+news.get(0).getNewsId());
                        mutableNewsData.setValue(news);  //Returned News to be displayed
                        isPrimeNewsBySectionRepoError.setValue(false); //Flag for Offline/Online

                        //save for OFFLINE use
//                        helper.deleteAllPrimeNewsOffline();//delete first  //Delete All
                        helper.deleteSectionPrimeNewsOffline(section);//delete first  //Delete only the specified section

                        for(int x=0; x<news.size();x++){
                            //ADD news to OFFLINE database
                            //make sure dont convert video into Bitmap..Cause NULL EXCEPTION : Resolved by Filtering here RepositoryPrimeNewsBySection
                            //saving only the ArticleNews and Avoid Videos
                            String image = news.get(x).getImage().substring(news.get(x).getImage().lastIndexOf(".")+1);
                            if( image.equals("png") || image.equals("jpg") || image.equals("jpeg") ) {
                                newsPrimeListOffline.add(new PrimeNewsOfflineModel(news.get(x).getNewsId(),news.get(x).getType(),news.get(x).getTitle(),news.get(x).getImage(),news.get(x).getPubdate(),"","",news.get(x).getLabel(),"","",section,null));
                                helper.insertPrimeNews(newsPrimeListOffline);
                                getPrimeArticleOffline(news.get(x).getNewsId());  //UPDATE TO GET THE CONTENT getting the news content for offline use  //
                            }else Log.d(TAG, "NOT IMAGE: "+ image);

                        }
                        //for HTML use src image..articleView..
                        saveImageToDB(newsPrimeListOffline);
                        //Reserved for SAME model, WAITING for API
//                        helper.deleteAllPrimeNewsOffline();
//                        helper.insertPrimeNews(newsPrime);
                        Log.d(TAG, "onSuccess: NEWS_ID "+news.get(0).getNewsId());

                    }


                    @Override
                    public void onError(Throwable e) {
                        isPrimeNewsBySectionRepoError.setValue(true);  //Flag for Offline/Online
                        Log.e("NEWS_API_ERR", e.toString());
                    }
                });
         return mutableNewsData;
    }
//
//
//    //For Offline News Storage
    public void getAllPrimeNewsBySection(int section) {
        primeApiService = ServiceGenerator.createService(PrimeApiService.class);
        //Chained API callback, getDailyKey() and getPrimeNewsBySection()
        primeApiService.getDailyKey()
                .flatMap((Function<String, SingleSource<ArrayList<PrimeNewsBySectionModel>>>) key -> {
                    String hashed = Global.Generate32SHA512(key + "inq" + PrimeGlobal.DEVICE_ID + "-megasection/" + section);
                    PrimeGlobal.DAILY_KEY = key;  //daily key
                    //return another API Callback after the 1st Api callback
                    return primeApiService.getPrimeNewsBySection(PrimeGlobal.DEVICE_ID, section, hashed);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<PrimeNewsBySectionModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ArrayList<PrimeNewsBySectionModel> news) {

                        if(!PrimeGlobal.isDeletedAlready){
                            PrimeGlobal.isDeletedAlready = true;
                            helper.deleteAllPrimeNewsOffline();//delete first  //Delete All
                        }

                        for(int x=0; x<news.size();x++){
                                newsPrimeListOffline.add(new PrimeNewsOfflineModel(news.get(x).getNewsId(),news.get(x).getType(),news.get(x).getTitle(),news.get(x).getImage(),news.get(x).getPubdate(),"","",news.get(x).getLabel(),"","",section,null));
                                helper.insertPrimeNews(newsPrimeListOffline);
                                getPrimeArticleOffline(news.get(x).getNewsId());  //getting the news content for offline use
                        }
//                        saveImageToDB(newsPrimeListOffline);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("NEWS_API_ERR", e.toString());
                    }
                });
    }
//

    public LiveData<Boolean> getPrimeNewsBySectionError(){
        return isPrimeNewsBySectionRepoError;
    }


    /**For OFFLINE CONTENT SAVING **/
    private void getPrimeArticleOffline(String id) {
        String hashed = Global.Generate32SHA512(PrimeGlobal.DAILY_KEY + "inq" + PrimeGlobal.DEVICE_ID + "-megaarticle/" + id);
        primeApiService.getPrimeArticle(PrimeGlobal.DEVICE_ID, id, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Article>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Article article) {
//                        helper.updatePrimeNews(article.getByline(),article.getContent().get(0).toString() ,article.getType(), Integer.valueOf(id));
                        helper.updatePrimeNews(article.getByline(),article.getContent1(),article.getType(), Integer.valueOf(id));
                        Log.d(TAG, "onSuccess: updated "+ countTest++);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: getPrimeArticle " + e.getMessage());
                    }
                });
    }

    //pass to AsyncTask
    private void saveImageToDB(List<PrimeNewsOfflineModel> newsPrimeList) {
        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner(newsPrimeList);
        asyncTaskRunner.execute();
    }

}


























//always partner with a Handler..WORKING but Cannot handle enough by runnable once go to activity
//    private Runnable convertAndSave = new Runnable() {
//        public void run() {
//            try {
//                //use newsPrimeList after saving all
//                for(int x = 0; x<newsPrimeList.size(); x++){
//                    //convert image
//                    byte[] imageByte = convertBitmapToByteArray(convertUrlToBitmap(newsPrimeList.get(x).getPhoto()));
//                    helper.updatePrimeNewsImage(imageByte,Integer.valueOf(newsPrimeList.get(x).getNews_Id()));
//                    Log.d(TAG, "run: called" + x + ".. "+ "" + " title.. " + newsPrimeList.get(x).getTitle());
//                    runnableHandler.postDelayed(convertAndSave, 200);
//                }
////                runnableHandler.removeCallbacks(convertAndSave); //stop runnable after forLoop
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    };



//WORKING
//    public MutableLiveData<List<PrimeNewsBySectionModel>> getPrimeNewsBySection(int section) {
////        String deviceKey = "4616724363963694"; //tester  //4616724363963694
////        String deviceId = "f8fcf876237111fb";  //tester
//          mutableNewsData = new MutableLiveData<>();
//
////        primeApiService = ServiceGenerator.createService(PrimeApiService.class);
////        hashed = Global.Generate32SHA512(deviceKey + "inq" + PrimeGlobal.DEVICE_ID + "-megasection/" + section);
////
////         primeApiService.getPrimeNewsBySection(PrimeGlobal.DEVICE_ID, section, hashed)
////
////                .subscribeOn(Schedulers.io())
////                .observeOn(AndroidSchedulers.mainThread())
////                .subscribe(new SingleObserver<ArrayList<PrimeNewsBySectionModel>>() {
////                    @Override
////                    public void onSubscribe(Disposable d) {
////                        Log.d(TAG, "onSubscribe: getPrimeNewsBySection called");
////                        compositeDisposable.add(d);
////                    }
//
//                    @Override
//                    public void onSuccess(ArrayList<PrimeNewsBySectionModel> news) {
//                        Log.d(TAG, "onSuccess:getPrimeNewsBySection called "+news.get(0).getNewsId());
//                        mutableNewsData.setValue(news);
//
////                        helper.deleteNews();
////                        helper.insertNews(news);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e("NEWS_API_ERR", e.toString());
//                    }
//                });
//        return mutableNewsData;
//    }

