package com.knx.inquirer.inqprime.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "PrimeLike")
public class PrimeLikeModel {

    //Dont autoGenerate primary key to make onConflict = OnConflictStrategy.REPLACE take effect.. making newsId as primary key
//    @PrimaryKey(autoGenerate = true)
//    private int id;

    @PrimaryKey
    @NonNull
    private int newsId;

    public PrimeLikeModel(int newsId) {
        this.newsId = newsId;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

}
