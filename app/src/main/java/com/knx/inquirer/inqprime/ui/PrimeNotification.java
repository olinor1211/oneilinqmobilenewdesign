package com.knx.inquirer.inqprime.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.adapters.RVAdapterPrimeNotification;
import com.knx.inquirer.inqprime.network.PrimeApiService;
import com.knx.inquirer.inqprime.utils.OnViewHolderItemListener;
import com.knx.inquirer.inqprime.utils.PrimeGlobal;
import com.knx.inquirer.inqprime.utils.PrimeSharedPrefs;
import com.knx.inquirer.models.Article;
import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryNotifs;
import com.knx.inquirer.viewmodels.NotifsViewModel;
import com.suke.widget.SwitchButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PrimeNotification extends AppCompatActivity implements OnViewHolderItemListener {
    private static final String TAG = "PrimeNotification";

    //utils
    private DividerItemDecoration dividerItemDecoration;
    private PrimeApiService primeApiService;
    private DatabaseHelper helper;
    private SharedPrefs sf;
    private PrimeSharedPrefs psp;
    private NotifsViewModel notifsViewModel;
    private PrimeGlobal.ProgressLoaderDialog progressLoaderDialog = new PrimeGlobal.ProgressLoaderDialog(this);
    private CompositeDisposable compositeDisposable;



    //ui
    private ImageButton arrowBack;
    private TextView noPrimeNotification;
    private TextView notificationTitleSw;
    private SwitchButton switchButton;
    private RecyclerView rvNotification;
//    private RecyclerView.Adapter rv_adapter;
    private List<NotificationModel> notification = new ArrayList<>();


    private RVAdapterPrimeNotification rvAdapterPrimeNotification;




    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        deleteNotifs();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prime_notification);

        EventBus.getDefault().register(this);
        //NO STATUS BAR
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initViews();

    }

    //need to override attachBaseContext ..to make the fontPath working
//    @Override
//    protected void attachBaseContext( Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    //Calligraphy3 ADDED for Tablet Android10
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void initViews() {

        //utils
        primeApiService = ServiceGenerator.createService(PrimeApiService.class);  //
        compositeDisposable = new CompositeDisposable();
        helper = new DatabaseHelper(this);
        sf = new SharedPrefs(this);
        psp = new PrimeSharedPrefs(this);


        arrowBack = findViewById(R.id.prime_notification_arrowBack);
        switchButton =  findViewById(R.id.sw_notification);
        notificationTitleSw =  findViewById(R.id.notification_title);

        //RV
        rvNotification = findViewById(R.id.rvPrimeNotification);
        noPrimeNotification = findViewById(R.id.tvNoPrimeNotification);

        //RV settings that works even there is no item inside the Adapter...
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        dividerItemDecoration = new DividerItemDecoration(this,linearLayoutManager.getOrientation()); //item decorations
        rvNotification.setLayoutManager(linearLayoutManager);
        rvNotification.addItemDecoration(dividerItemDecoration); //item decorations
        rvNotification.setHasFixedSize(true);
        //calling/creating an instance of RVAdapter Class is in this settings..
        rvAdapterPrimeNotification =  new RVAdapterPrimeNotification(this, notification, this::onViewHolderItemClick);  //pass the interface
        rvNotification.setAdapter(rvAdapterPrimeNotification);

        //Check Switch control status
        if(psp.getPrimeNotifSwitch()){
            switchButton.setChecked(psp.getPrimeNotifSwitch());
//            notificationTitleSw.setText("TURN OFF NOTIFICATION");
            showNotifs();
        }

        arrowBack.setOnClickListener(v -> {
            onBackPressed();
        });



        switchButton.setOnCheckedChangeListener((view, isChecked) -> {

            if(isChecked){
                psp.setPrimeNotifSwitch(true);
//                notificationTitleSw.setText("TURN OFF NOTIFICATION");
                showNotifs();
            }else {
                psp.setPrimeNotifSwitch(false);
//                notificationTitleSw.setText("TURN ON NOTIFICATION");
                noPrimeNotification.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void linkError(){
        Toast.makeText(this, "Unexpected error detected!", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "linkError called ");
        return;
    }

    //from a table
    private void showNotifs(){
//        notifsViewModel = ViewModelProviders.of(this).get(NotifsViewModel.class);
//        notifsViewModel.getNotifications().observe(this, data -> {

        CompositeDisposable compositeDisposable = new CompositeDisposable();
        notifsViewModel = ViewModelProviders.of(this, new ViewModelFactoryNotifs(this.getApplication(), compositeDisposable)).get(NotifsViewModel.class);
        notifsViewModel.getNotifications().observe(this, data -> {


            notification.clear();
            notification.addAll(data);

            if(data.size() > 0) {
//                notification = data;  //make global variable just to be able to access in the swipe

                rvAdapterPrimeNotification = new RVAdapterPrimeNotification(this, notification, this::onViewHolderItemClick); //
//                rv_adapter = new RVAdapterPrimeNotification(this, notification, this::onViewHolderItemClick); //
//                new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvNotification);  //attaching ItemTouch to RecyclerView //TEMP REMOVED

                rvNotification.setAdapter(rvAdapterPrimeNotification);
                rvAdapterPrimeNotification.notifyDataSetChanged();  //ADAPTER
            }else{
                rvNotification.removeItemDecoration(dividerItemDecoration);
                noPrimeNotification.setVisibility(View.VISIBLE);
            }
        });
    }



    //deleting All notifs
    public void deleteNotifs(){
        helper.deleteAllNotifications();
    }
//
//  //deleting ONE notifs
    public void deleteOneNotif(int referenceId) {
        helper.deleteOneNotification(referenceId);
    }

    private void dismissProgressLoading() {
        progressLoaderDialog.dismissDialog();
    }

    private void showProgressLoading() {
        progressLoaderDialog.showDialog();
    }


    //viewPrimeArticle
    private void viewPrimeArticle(String id) {
        showProgressLoading();
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
        primeApiService.getPrimeArticle(sf.getDeviceId(), id, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Article>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Article article) {

                        dismissProgressLoading();

                        Intent i = new Intent(PrimeNotification.this, PrimeArticle.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", id);
                        bundle.putString("photo", article.getCreative());
                        bundle.putString("title", article.getTitle());
                        bundle.putString("author", article.getByline());
                        bundle.putString("date", article.getPubdate());
//                        bundle.putString("content", article.getContent().get(0));
                        bundle.putString("type", article.getType());
                        bundle.putString("custom", article.getCustom());
                        bundle.putString("section_name", "SPORT"); //test section
                        //

                        Log.d(TAG, "onSuccess: ID "+id);

                        i.putExtras(bundle);
                        startActivity(i);
                    }


                    @Override
                    public void onError(Throwable e) {
                        dismissProgressLoading();
                        Toast.makeText(PrimeNotification.this, "Unexpected error detected!", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "onError: viewArticle called ");
                        return;
                    }
                });
    }


    @Override
    public void onBackPressed() {super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onViewHolderItemClick(View view, int position) {
        switch(view.getId()){
            case R.id.prime_ll_notification_items:
                readNotification(position);
                break;

            case R.id.prime_notif_image:
                readNotification(position);
                break;

            case R.id.delete_layout_prime_notif:
                //call direct the helper
//                helper.updateNotificationStatus(1, notification.get(position).getId());  //notification to read status
                //get the record of item that is about to delete..
                int referenceId = notification.get(position).getId();  //database ref id
                String newsId = notification.get(position).getNews_id(); //NOTIFICATION changes
                String image = notification.get(position).getImage();
                String link = notification.get(position).getLink();
                String title = notification.get(position).getTitle();
                String message = notification.get(position).getMessage();
                Date date = notification.get(position).getTimestamp();  // String date = notification.get(position).getTimestamp();
                int readStatus = notification.get(position).getReadstatus();

                deleteOneNotif(referenceId); //DELETE ONE NOTIF
                rvAdapterPrimeNotification.notifyItemRemoved(position);  //Notify adapter

                //Snackbar with UNDO... restore the deleted item
                Snackbar.make(findViewById(R.id.snackbar_primeNotification),  "One Notification is removed.", Snackbar.LENGTH_INDEFINITE)
                        .setAction("UNDO", v -> {
                            helper.insertNotification(new NotificationModel(newsId,image,link,title,message,date,0)); //re-insert again
                            rvAdapterPrimeNotification.notifyItemInserted(position);
                            noPrimeNotification.setVisibility(View.INVISIBLE);
                        })
                        .show();
                break;
        }

    }



    public void readNotification(int position){
        if (Global.isNetworkAvailable(getApplicationContext())) {
//                    //call direct the helper
//            helper.updateNotificationStatus(1, notification.get(position).getId());  //notification to read status
            helper.updateNotificationStatus2(1, notification.get(position).getNews_id());  //notification to read status
            Log.d(TAG, "onBindViewHolder:  helper.updateNotificationStatus " + notification.get(position).getId());

            if (!notification.get(position).getLink().isEmpty() && notification.get(position).getLink()!=null) {

                if (notification.get(position).getLink().contains("http")) {

                    Intent intent = new Intent(PrimeNotification.this, PrimeArticle.class);
                    intent.putExtra("url", notification.get(position).getLink());
                    startActivity(intent);

                }
                else if (!notification.get(position).getLink().contains("inq")) {
                    Log.d(TAG, "onBindViewHolder:  not contains(\"inq\") called ");
                    linkError();
                }
                else if (notification.get(position).getLink().substring(0, 3).equals("inq")) {
                    Log.d(TAG, "onBindViewHolder: equals(\"inq\") called ");
                    String articleID = notification.get(position).getLink().substring(notification.get(position).getLink().lastIndexOf("/") + 1);
                    viewPrimeArticle(articleID);
                }
                else{
                    Log.d(TAG, "onBindViewHolder: linkError called ");
                    linkError();
                }
            }else{
                Log.d(TAG, "onBindViewHolder: No PrimeNotification Link ");
            }

        }else {
            Global.ViewDialog alert = new Global.ViewDialog();
            alert.showDialog(this, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
        }
    }


}






























//    //    // sample data test
//    public void insertNotif(){
//        NotificationModel nmodel = new NotificationModel("https://combo.staticflickr.com/ap/build/images/refencing-announcement/bird2.jpg", "https://www.flickr.com/lookingahead", "God is good ",  "God's Love and Faithfulness. Jesus the Son of God. The God's favor. ", "September 09, 2019 02:53pm", 0);
//        helper.insertNotification(nmodel);
//    }
//}


//        switchButton.setChecked(true);
//        switchButton.isChecked();
//                switchButton.toggle();     //switch state
//                switchButton.toggle(true);//switch without animation
//                switchButton.setShadowEffect(true);//disable shadow effect
//                switchButton.setEnabled(true);//disable button
//                switchButton.setEnableEffect(true);//disable the switch animation


//switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
//@Override
//public void onCheckedChanged(SwitchButton view, boolean isChecked) {
//        //TODO do your job
//        }
//        });



//
//    @Override
//    public void onViewHolderItemClick(View view, int position) {
//        Toast.makeText(this, "Click It "+position, Toast.LENGTH_SHORT).show();
////        String id = newsList.get(position).getId();
////        PrimeGlobal.lastItemPosition = position;
////        PrimeGlobal.lastNewsIdDisplayed = Integer.valueOf(id);
//
//        if (Global.isNetworkAvailable(getApplicationContext())) {
////                    //call direct the helper
//                    helper.updateNotificationStatus(1, notification.get(position).getId());  //notification to read status
//                    Log.d(TAG, "onBindViewHolder:  helper.updateNotificationStatus " + notification.get(position).getId());
//
//                    if (!notification.get(position).getLink().isEmpty() && notification.get(position).getLink()!=null) {
//
//                        if (notification.get(position).getLink().contains("http")) {
//
//                            Intent intent = new Intent(PrimeNotification.this, PrimeArticle.class);
//                            intent.putExtra("url", notification.get(position).getLink());
////                            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
//                            startActivity(intent);
//
//                        }
//                        else if (!notification.get(position).getLink().contains("inq")) {
//                            Log.d(TAG, "onBindViewHolder:  not contains(\"inq\") called ");
//                            linkError();
//                        }
//                        else if (notification.get(position).getLink().substring(0, 3).equals("inq")) {
//                            Log.d(TAG, "onBindViewHolder: equals(\"inq\") called ");
//                            String articleID = notification.get(position).getLink().substring(notification.get(position).getLink().lastIndexOf("/") + 1);
//                            viewPrimeArticle(articleID);
//                        }
//                        else{
//                            Log.d(TAG, "onBindViewHolder: linkError called ");
//                            linkError();
//                        }
//                    }else{
//                        Log.d(TAG, "onBindViewHolder: No PrimeNotification Link ");
//                    }
//
//                }else {
//                    Global.ViewDialog alert = new Global.ViewDialog();
//                    alert.showDialog(this, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
//                }
//
//            }


