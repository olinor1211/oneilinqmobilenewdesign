package com.knx.inquirer.inqprime.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;

import java.util.List;

public class RepositoryPrimeAccountInformation {

    private DatabaseHelper helper;
    private Context context;

    public RepositoryPrimeAccountInformation() {
        helper = new DatabaseHelper(context);
    }

    public LiveData<List<PrimeAccountInformationModel>> getPrimeAccountInformationList(){return helper.getPrimeAccountInformation();}

}
