package com.knx.inquirer.inqprime.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.model.PrimeSectionsModel;
import com.knx.inquirer.inqprime.repositories.RepositoryPrimeAccountInformation;
import com.knx.inquirer.inqprime.repositories.RepositoryPrimeSections;

import java.util.List;

public class ViewModelPrimeSections extends AndroidViewModel {

    private RepositoryPrimeSections repositoryPrimeSections;

    public ViewModelPrimeSections(@NonNull Application application) {
        super(application);
        repositoryPrimeSections = new RepositoryPrimeSections();
    }

    public LiveData<List<PrimeSectionsModel>> getPrimeSectionsStatus(){return repositoryPrimeSections.getPrimeSectionsStatus();}

//    public LiveData<List<PrimeSectionsModel>> getPrimeSections(){return repositoryPrimeSections.getPrimeSections();}

}
