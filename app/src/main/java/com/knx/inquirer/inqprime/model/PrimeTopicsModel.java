package com.knx.inquirer.inqprime.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "PrimeTopics")
public class PrimeTopicsModel {

    public PrimeTopicsModel() {

    }

    @PrimaryKey
    @NonNull
    private int PrimeId;

    private String PrimeTopics;
    private Boolean Status;

    public PrimeTopicsModel(int primeId, String primeTopics, Boolean status) {
        this.PrimeId = primeId;
        PrimeTopics = primeTopics;
        Status = status;
    }

    public int getPrimeId() {
        return PrimeId;
    }

    public String getPrimeTopics() {
        return PrimeTopics;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setPrimeId(int primeId) {
        this.PrimeId = primeId;
    }

    public void setPrimeTopics(String primeSections) {
        PrimeTopics = primeSections;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }
}
