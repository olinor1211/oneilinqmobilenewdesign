package com.knx.inquirer.inqprime.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.knx.inquirer.R;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.utils.OnViewHolderItemListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;


public class RVAdapterPrimeBookmarks extends RecyclerView.Adapter<RVAdapterPrimeBookmarks.ItemViewHolder> {
    private static final String TAG = "RVAdapterPrimeBookmarks";

    public List<PrimeBookmarkModel> bookmarks;
//private MutableLiveData<List<PrimeBookmarkModel>> bookmarks = new MutableLiveData<>();
    private Context context;
    private OnViewHolderItemListener mOnViewHolderItemListener;  //declare interface in RV adapter

    public RVAdapterPrimeBookmarks(Context context, List<PrimeBookmarkModel> bookmarks, OnViewHolderItemListener onViewHolderItemListener) {
//    public RVAdapterPrimeBookmarks(Context context, MutableLiveData<List<PrimeBookmarkModel>> bookmarks, OnViewHolderItemListener onViewHolderItemListener) {
        this.bookmarks = bookmarks;
        this.context = context;
        this.mOnViewHolderItemListener = onViewHolderItemListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.prime_bookmark_list_items, viewGroup, false);
        return new ItemViewHolder(v, mOnViewHolderItemListener);  //add interface as an additional parameter
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {

        // Bind your data here
        itemViewHolder.bind();

        String date = bookmarks.get(position).getDate();
        String title = bookmarks.get(position).getTitle();
        String author = bookmarks.get(position).getAuthor();
        String photo = bookmarks.get(position).getImage();

        itemViewHolder.bookmarkTitle.setText(title);
        itemViewHolder.bookmarkDate.setText(date);
        itemViewHolder.bookmarkAuthor.setText(author);
        Log.d(TAG, "onBindViewHolder: bookmarkAuthor "+ author);

        if(!photo.isEmpty()){
            Picasso.get()
                    .load(photo)
                    .noFade()
                    .noPlaceholder()
                    .into(itemViewHolder.bookmarkImage);
        }

    }


    @Override
    public int getItemCount() {
        return bookmarks == null ? 0 : bookmarks.size();
    }


    //ItemViewHolder IMPLEMENTS OnClickListener
    public class ItemViewHolder extends RecyclerView.ViewHolder {
//        public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        SwipeRevealLayout swipeLayout;
        ConstraintLayout frontLayout;
        View deleteLayout;
        ImageView bookmarkImage;
        TextView bookmarkTitle, bookmarkDate, bookmarkAuthor;
        OnViewHolderItemListener onViewHolderItemListener;

        public ItemViewHolder(@NonNull View itemView, OnViewHolderItemListener onViewHolderItemListener) {
            super(itemView);
            this.onViewHolderItemListener = onViewHolderItemListener;

            swipeLayout = itemView.findViewById(R.id.swipe_layout);
            frontLayout = itemView.findViewById(R.id.front_layout);
            deleteLayout = itemView.findViewById(R.id.delete_layout);

            bookmarkImage = itemView.findViewById(R.id.prime_bookmark_list_item_image);
            bookmarkTitle = itemView.findViewById(R.id.prime_bookmark_list_item_title);
            bookmarkDate = itemView.findViewById(R.id.prime_bookmark_list_item_date);
            bookmarkAuthor = itemView.findViewById(R.id.prime_bookmark_list_item_author);

            //attaching itemView to the interface..
//            itemView.setOnClickListener(this);

        }

        //binded/invoked in onBindViewHolder.. BINDING THE COMPONENTS to be Accessed in UI
        public void bind() {
            //BACK LAYOUT
            deleteLayout.setOnClickListener(v -> {
                onViewHolderItemListener.onViewHolderItemClick(v, getAdapterPosition()); //onViewHolderItemClick
            });
            //FRONT LAYOUT
            frontLayout.setOnClickListener(view -> onViewHolderItemListener.onViewHolderItemClick(view,getAdapterPosition()));  //onViewHolderItemClick
        }
    }

}
