package com.knx.inquirer.inqprime.utils;

import android.view.View;

public interface OnViewHolderItemListener {
    void onViewHolderItemClick(View view, int position);
//    void onViewHolderDeleteItemClick(int position);
}
