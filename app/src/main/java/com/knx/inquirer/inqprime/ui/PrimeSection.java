package com.knx.inquirer.inqprime.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.utils.PrimeGlobal;
import com.knx.inquirer.inqprime.utils.PrimeSharedPrefs;
import com.suke.widget.SwitchButton;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PrimeSection extends AppCompatActivity {
    private static final String TAG = "PrimeSection";

    //utils
    private PrimeSharedPrefs psp;
    private DatabaseHelper helper;

    //ui
    private ImageButton arrowBack;
    private SwitchButton switchButtonSections, switchButtonSectionsNewsInfo, switchButtonSectionsSport, switchButtonSectionsGlobalNation, switchButtonSectionsLifestyle, switchButtonSectionsBusiness, switchButtonSectionsTechnology, switchButtonSectionsOpinion ;
    private SwitchButton switchButtonTopics, switchButtonTopicsHealth, switchButtonTopicsPolitics, switchButtonTopicsBeauty, switchButtonTopicsArts, switchButtonTopicsMusic, switchButtonTopicsTravel;
    private TextView apply;

    //variables
    private int countSection = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prime_section);

        //no status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);


        initView();
        switchButtonsSetup();  //Show/Update changed switch status
        switchButtonsListener();
    }

    //need to override attachBaseContext ..to make the fontPath working
//    @Override
//    protected void attachBaseContext( Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    //Calligraphy3 ADDED for Tablet Android10
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void initView() {

        psp = new PrimeSharedPrefs(this);
        helper = new DatabaseHelper(this);


        arrowBack = findViewById(R.id.prime_section_arrowBack);
        apply = findViewById(R.id.sections_Apply);

        switchButtonSections = findViewById(R.id.sw_sections);
        switchButtonSectionsNewsInfo = findViewById(R.id.sw_sectionsNewsInfo);
        switchButtonSectionsGlobalNation = findViewById(R.id.sw_sectionsGLobalnation);
        switchButtonSectionsSport = findViewById(R.id.sw_sectionsSport);
        switchButtonSectionsLifestyle = findViewById(R.id.sw_sectionsLifestyle);
        switchButtonSectionsBusiness = findViewById(R.id.sw_sectionsBusiness);
        switchButtonSectionsTechnology = findViewById(R.id.sw_sectionsTechnology);
        switchButtonSectionsOpinion = findViewById(R.id.sw_sectionsOpinion);

        switchButtonTopics = findViewById(R.id.sw_topics);
        switchButtonTopicsHealth = findViewById(R.id.sw_topicsHealthAndWellness);
        switchButtonTopicsPolitics = findViewById(R.id.sw_topicsPolitics);
        switchButtonTopicsBeauty = findViewById(R.id.sw_topicsBeauty);
        switchButtonTopicsArts = findViewById(R.id.sw_topicsArts);
        switchButtonTopicsMusic = findViewById(R.id.sw_topicsMusic);
        switchButtonTopicsTravel = findViewById(R.id.sw_topicsTravel);

        arrowBack.setOnClickListener(v -> {
            onBackPressed();
        });

        apply.setOnClickListener(v -> {
            Toast.makeText(this, "Settings' applied", Toast.LENGTH_SHORT).show();
            //SECTIONS
            if(switchButtonSections.isChecked()){  //MAIN SWITCH
                psp.setSectionSwitch(true);
            }else psp.setSectionSwitch(false);

            if(switchButtonSectionsNewsInfo.isChecked()){
                helper.updatePrimeSectionStatus(true,PrimeGlobal.PRIME_SECTION_NEWS_INFO_ID); //update Database
                psp.setSectionSwitchNews(true); //set TRUE
            }else{
                helper.updatePrimeSectionStatus(false,PrimeGlobal.PRIME_SECTION_NEWS_INFO_ID); //update Database
                psp.setSectionSwitchNews(false); //set FALSE
            }

            if(switchButtonSectionsGlobalNation.isChecked()){
                helper.updatePrimeSectionStatus(true,PrimeGlobal.PRIME_SECTION_GLOBAL_NATION_ID); //update Database
                psp.setSectionSwitchGlobal(true); //set TRUE

            }else {
                helper.updatePrimeSectionStatus(false,PrimeGlobal.PRIME_SECTION_GLOBAL_NATION_ID); //update Database
                psp.setSectionSwitchGlobal(false); //set False
            }

            if(switchButtonSectionsSport.isChecked()){
                helper.updatePrimeSectionStatus(true,PrimeGlobal.PRIME_SECTION_SPORTS_ID); //update Database
                psp.setSectionSwitchSport(true); //set TRUE

            }else {
                helper.updatePrimeSectionStatus(false,PrimeGlobal.PRIME_SECTION_SPORTS_ID); //update Database
                psp.setSectionSwitchSport(false); //set False
            }

            if(switchButtonSectionsLifestyle.isChecked()){
                helper.updatePrimeSectionStatus(true,PrimeGlobal.PRIME_SECTION_LIFESTYLE_ID); //update Database
                psp.setSectionSwitchLife(true); //set TRUE

            }else {
                helper.updatePrimeSectionStatus(false,PrimeGlobal.PRIME_SECTION_LIFESTYLE_ID); //update Database
                psp.setSectionSwitchLife(false); //set False
            }

            if(switchButtonSectionsBusiness.isChecked()){
                helper.updatePrimeSectionStatus(true,PrimeGlobal.PRIME_SECTION_BUSINESS_ID); //update Database
                psp.setSectionSwitchBusiness(true); //set TRUE

            }else {
                helper.updatePrimeSectionStatus(false,PrimeGlobal.PRIME_SECTION_BUSINESS_ID); //update Database
                psp.setSectionSwitchBusiness(false); //set False
            }

            if(switchButtonSectionsTechnology.isChecked()){
                helper.updatePrimeSectionStatus(true,PrimeGlobal.PRIME_SECTION_TECHNOLOGY_ID); //update Database
                psp.setSectionSwitchTechnology(true);
            }else{
                helper.updatePrimeSectionStatus(false,PrimeGlobal.PRIME_SECTION_TECHNOLOGY_ID); //update Database
                psp.setSectionSwitchTechnology(false);
            }

            if(switchButtonSectionsOpinion.isChecked()){
                helper.updatePrimeSectionStatus(true,PrimeGlobal.PRIME_SECTION_OPINION_ID);
                psp.setSectionSwitchOpinion(true); //set TRUE

            }else{
                helper.updatePrimeSectionStatus(false,PrimeGlobal.PRIME_SECTION_OPINION_ID);
                psp.setSectionSwitchOpinion(false); //set TRUE
            }

            //TOPICS
            if(switchButtonTopics.isChecked()){  //MAIN SWITCH
                psp.setTopicsSwitch(true);
            }else psp.setTopicsSwitch(false);

            if(switchButtonTopicsHealth.isChecked()){
                helper.updatePrimeTopicstatus(true,PrimeGlobal.PRIME_TOPIC_HEALTH_ID);
                psp.setTopicsSwitchHealth(true);
            }else {
                helper.updatePrimeTopicstatus(false,PrimeGlobal.PRIME_TOPIC_HEALTH_ID);
                psp.setTopicsSwitchHealth(false);
            }

            if(switchButtonTopicsPolitics.isChecked()){
                helper.updatePrimeTopicstatus(true,PrimeGlobal.PRIME_TOPIC_POLITICS_ID);
                psp.setTopicsSwitchPolitics(true);
            }else {
                helper.updatePrimeTopicstatus(false,PrimeGlobal.PRIME_TOPIC_POLITICS_ID);
                psp.setTopicsSwitchPolitics(false);
            }

            if(switchButtonTopicsBeauty.isChecked()){
                helper.updatePrimeTopicstatus(true,PrimeGlobal.PRIME_TOPIC_BEAUTY_ID);
                psp.setTopicsSwitchBeauty(true);
            }else {
                helper.updatePrimeTopicstatus(false,PrimeGlobal.PRIME_TOPIC_BEAUTY_ID);
                psp.setTopicsSwitchBeauty(false);
            }

            if(switchButtonTopicsArts.isChecked()){
                helper.updatePrimeTopicstatus(true,PrimeGlobal.PRIME_TOPIC_ARTS_ID);
                psp.setTopicsSwitchArts(true);
            }else {
                helper.updatePrimeTopicstatus(false,PrimeGlobal.PRIME_TOPIC_ARTS_ID);
                psp.setTopicsSwitchArts(false);
            }

            if(switchButtonTopicsMusic.isChecked()){
                helper.updatePrimeTopicstatus(true,PrimeGlobal.PRIME_TOPIC_MUSICID);
                psp.setTopicsSwitchMusic(true);
            }else {
                helper.updatePrimeTopicstatus(false,PrimeGlobal.PRIME_TOPIC_MUSICID);
                psp.setTopicsSwitchMusic(false);
            }

            if(switchButtonTopicsTravel.isChecked()){
                helper.updatePrimeTopicstatus(true,PrimeGlobal.PRIME_TOPIC_TRAVEL_ID);
                psp.setTopicsSwitchTravel(true);
            }else {
                helper.updatePrimeTopicstatus(false,PrimeGlobal.PRIME_TOPIC_TRAVEL_ID);
                psp.setTopicsSwitchTravel(false);
            }

        });

    }



    //
    private void switchButtonsSetup() {
        //SECTIONS
        switchButtonSections.setChecked(psp.getSectionSwitch());
        switchButtonSectionsNewsInfo.setChecked(psp.getSectionSwitchNews());
        switchButtonSectionsGlobalNation.setChecked(psp.getSectionSwitchGlobal());
        switchButtonSectionsSport.setChecked(psp.getSectionSwitchSport());
        switchButtonSectionsLifestyle.setChecked(psp.getSectionSwitchLife());
        switchButtonSectionsBusiness.setChecked(psp.getSectionSwitchBusiness());
        switchButtonSectionsTechnology.setChecked(psp.getSectionSwitchTechnology());
        switchButtonSectionsOpinion.setChecked(psp.getSectionSwitchOpinion());

        switchButtonSectionsNewsInfo.setEnabled(psp.getSectionSwitchNews());
        switchButtonSectionsGlobalNation.setEnabled(psp.getSectionSwitchGlobal());
        switchButtonSectionsSport.setEnabled(psp.getSectionSwitchSport());
        switchButtonSectionsLifestyle.setEnabled(psp.getSectionSwitchLife());
        switchButtonSectionsBusiness.setEnabled(psp.getSectionSwitchBusiness());
        switchButtonSectionsTechnology.setEnabled(psp.getSectionSwitchTechnology());
        switchButtonSectionsOpinion.setEnabled(psp.getSectionSwitchOpinion());

        //TOPICS
        switchButtonTopics.setChecked(psp.getTopicsSwitch());
        switchButtonTopicsHealth.setChecked(psp.getTopicsSwitchHealth());
        switchButtonTopicsPolitics.setChecked(psp.getTopicsSwitchPolitics());
        switchButtonTopicsBeauty.setChecked(psp.getTopicsSwitchBeauty());
        switchButtonTopicsArts.setChecked(psp.getTopicsSwitchArts());
        switchButtonTopicsMusic.setChecked(psp.getTopicsSwitchMusic());
        switchButtonTopicsTravel.setChecked(psp.getTopicsSwitchTravel());

        switchButtonTopicsHealth.setEnabled(psp.getTopicsSwitchHealth());
        switchButtonTopicsPolitics.setEnabled(psp.getTopicsSwitchPolitics());
        switchButtonTopicsBeauty.setEnabled(psp.getTopicsSwitchBeauty());
        switchButtonTopicsArts.setEnabled(psp.getTopicsSwitchArts());
        switchButtonTopicsMusic.setEnabled(psp.getTopicsSwitchMusic());
        switchButtonTopicsTravel.setEnabled(psp.getTopicsSwitchTravel());

        //for locking purpose if Main Switch is OFF
        if(switchButtonSections.isChecked()){
            switchButtonSectionsNewsInfo.setEnabled(true);
            switchButtonSectionsGlobalNation.setEnabled(true);
            switchButtonSectionsSport.setEnabled(true);
            switchButtonSectionsLifestyle.setEnabled(true);
            switchButtonSectionsBusiness.setEnabled(true);
            switchButtonSectionsTechnology.setEnabled(true);
            switchButtonSectionsOpinion.setEnabled(true);
        }
        //for locking purpose if Main Switch is OFF
        if(switchButtonTopics.isChecked()){
            switchButtonTopicsHealth.setEnabled(true);
            switchButtonTopicsPolitics.setEnabled(true);
            switchButtonTopicsBeauty.setEnabled(true);
            switchButtonTopicsArts.setEnabled(true);
            switchButtonTopicsMusic.setEnabled(true);
            switchButtonTopicsTravel.setEnabled(true);
        }

    }

    private void switchButtonsListener() {

        //SECTIONS
        switchButtonSections.setOnCheckedChangeListener((view, isChecked) -> {
            if(isChecked){
                switchButtonSectionsNewsInfo.setEnabled(true);
                switchButtonSectionsGlobalNation.setEnabled(true);
                switchButtonSectionsSport.setEnabled(true);
                switchButtonSectionsLifestyle.setEnabled(true);
                switchButtonSectionsBusiness.setEnabled(true);
                switchButtonSectionsTechnology.setEnabled(true);
                switchButtonSectionsOpinion.setEnabled(true);
            }
        });

        //TOPICS
        switchButtonTopics.setOnCheckedChangeListener((view, isChecked) -> {

            if(isChecked){
                switchButtonTopicsHealth.setEnabled(true);
                switchButtonTopicsPolitics.setEnabled(true);
                switchButtonTopicsBeauty.setEnabled(true);
                switchButtonTopicsArts.setEnabled(true);
                switchButtonTopicsMusic.setEnabled(true);
                switchButtonTopicsTravel.setEnabled(true);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: called ");
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called ");
    }
}




































































//        apply.setOnClickListener(v -> {

//
//            if(switchButtonSectionsNewsInfo.isChecked()){
//
//            }else psp.setSectionSwitchNews(false);
//
//            if(switchButtonSectionsGlobalNation.isChecked()){
//                psp.setSectionSwitchGlobal(true);
//            }else psp.setSectionSwitchGlobal(false);
//
//            if(switchButtonSectionsSport.isChecked()){
//                psp.setSectionSwitchSport(true);
//            }else psp.setSectionSwitchSport(false);
//
//            if(switchButtonSectionsLifestyle.isChecked()){
//                psp.setSectionSwitchLife(true);
//            }else psp.setSectionSwitchLife(false);
//
//            if(switchButtonSectionsBusiness.isChecked()){
//                psp.setSectionSwitchBusiness(true);
//            }else psp.setSectionSwitchBusiness(false);
//
//            if(switchButtonSectionsTechnology.isChecked()){
//                psp.setSectionSwitchTechnology(true);
//            }else psp.setSectionSwitchTechnology(false);
//
//            if(switchButtonSectionsOpinion.isChecked()){
//                psp.setSectionSwitchOpinion(true);
//            }else psp.setSectionSwitchOpinion(false);
//
//            //topics
//            if(switchButtonTopics.isChecked()){
//                psp.setTopicsSwitch(true);
//            }else psp.setTopicsSwitch(false);
//
//            if(switchButtonTopicsHealth.isChecked()){
//                psp.setTopicsSwitchHealth(true);
//            }else psp.setTopicsSwitchHealth(false);
//
//            if(switchButtonTopicsPolitics.isChecked()){
//                psp.setTopicsSwitchPolitics(true);
//            }else psp.setTopicsSwitchPolitics(false);
//
//            if(switchButtonTopicsBeauty.isChecked()){
//                psp.setTopicsSwitchBeauty(true);
//            }else psp.setTopicsSwitchBeauty(false);
//
//            if(switchButtonTopicsArts.isChecked()){
//                psp.setTopicsSwitchArts(true);
//            }else psp.setTopicsSwitchArts(false);
//
//            if(switchButtonTopicsMusic.isChecked()){
//                psp.setTopicsSwitchMusic(true);
//            }else psp.setTopicsSwitchMusic(false);
//
//            if(switchButtonTopicsTravel.isChecked()){
//                psp.setTopicsSwitchTravel(true);
//            }else psp.setTopicsSwitchTravel(false);
//
//
//        });


//        switchButtonTopicsHealth.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
////                Toast.makeText(PrimeSection.this, "ON", Toast.LENGTH_SHORT).show();
//            }else {
////                Toast.makeText(PrimeSection.this, "OFF", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        switchButtonTopicsPolitics.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
////                Toast.makeText(PrimeSection.this, "ON", Toast.LENGTH_SHORT).show();
//            }else {
////                Toast.makeText(PrimeSection.this, "OFF", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        switchButtonTopicsBeauty.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
////                Toast.makeText(PrimeSection.this, "ON", Toast.LENGTH_SHORT).show();
//            }else {
////                Toast.makeText(PrimeSection.this, "OFF", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        switchButtonTopicsArts.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
////                Toast.makeText(PrimeSection.this, "ON", Toast.LENGTH_SHORT).show();
//            }else {
////                Toast.makeText(PrimeSection.this, "OFF", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        switchButtonTopicsMusic.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
////                Toast.makeText(PrimeSection.this, "ON", Toast.LENGTH_SHORT).show();
//            }else {
////                Toast.makeText(PrimeSection.this, "OFF", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        switchButtonTopicsTravel.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
////                Toast.makeText(PrimeSection.this, "ON", Toast.LENGTH_SHORT).show();
//            }else {
////                Toast.makeText(PrimeSection.this, "OFF", Toast.LENGTH_SHORT).show();
//            }
//        });

//
//        switchButtonSectionsNewsInfo.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
//                PrimeGlobal.countSection = PrimeGlobal.countSection + 1;
////                countSection = countSection + 1;
//            }else {
//                PrimeGlobal.countSection = PrimeGlobal.countSection - 1;
//            }
//        });
//
//        switchButtonSectionsGlobalNation.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
//                PrimeGlobal.countSection = PrimeGlobal.countSection + 1;
////                countSection = countSection + 1;
//            }else {
//                PrimeGlobal.countSection = PrimeGlobal.countSection - 1;
//            }
//        });
//
//        switchButtonSectionsSport.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
//                PrimeGlobal.countSection = PrimeGlobal.countSection + 1;
////                countSection = countSection + 1;
////                Toast.makeText(PrimeSection.this, "ON", Toast.LENGTH_SHORT).show();
//            }else {
//                PrimeGlobal.countSection = PrimeGlobal.countSection - 1;
//            }
//        });
//
//        switchButtonSectionsLifestyle.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
//                PrimeGlobal.countSection = PrimeGlobal.countSection + 1;
////                countSection = countSection + 1;
//            }else {
//                PrimeGlobal.countSection = PrimeGlobal.countSection - 1;
//            }
//        });
//
//        switchButtonSectionsBusiness.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
//                PrimeGlobal.countSection = PrimeGlobal.countSection + 1;
////                countSection = countSection + 1;
//            }else {
//                PrimeGlobal.countSection = PrimeGlobal.countSection - 1;
//            }
//        });
//
//        switchButtonSectionsTechnology.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
//                PrimeGlobal.countSection = PrimeGlobal.countSection + 1;
////                countSection = countSection + 1;
//            }else {
//                PrimeGlobal.countSection = PrimeGlobal.countSection - 1;
//            }
//        });
//
//        switchButtonSectionsOpinion.setOnCheckedChangeListener((view, isChecked) -> {
//
//            if(isChecked){
//                PrimeGlobal.countSection = PrimeGlobal.countSection + 1;
////                countSection = countSection + 1;
//            }else {
//                PrimeGlobal.countSection = PrimeGlobal.countSection - 1;
//            }
//        });



//        switchButton.setChecked(true);
//        switchButton.isChecked();
//                switchButton.toggle();     //switch state
//                switchButton.toggle(true);//switch without animation
//                switchButton.setShadowEffect(true);//disable shadow effect
//                switchButton.setEnabled(true);//disable button
//                switchButton.setEnableEffect(true);//disable the switch animation





//        apply.setOnClickListener(v -> {
//            Toast.makeText(this, "Settings' applied", Toast.LENGTH_SHORT).show();
//            //sections
//            if(switchButtonSections.isChecked()){
//                psp.setSectionSwitch(true);
//            }else psp.setSectionSwitch(false);
//
//            if(switchButtonSectionsNewsInfo.isChecked()){
//                psp.setSectionSwitchNews(true);
//            }else psp.setSectionSwitchNews(false);
//
//            if(switchButtonSectionsGlobalNation.isChecked()){
//                psp.setSectionSwitchGlobal(true);
//            }else psp.setSectionSwitchGlobal(false);
//
//            if(switchButtonSectionsSport.isChecked()){
//                psp.setSectionSwitchSport(true);
//            }else psp.setSectionSwitchSport(false);
//
//            if(switchButtonSectionsLifestyle.isChecked()){
//                psp.setSectionSwitchLife(true);
//            }else psp.setSectionSwitchLife(false);
//
//            if(switchButtonSectionsBusiness.isChecked()){
//                psp.setSectionSwitchBusiness(true);
//            }else psp.setSectionSwitchBusiness(false);
//
//            if(switchButtonSectionsTechnology.isChecked()){
//                psp.setSectionSwitchTechnology(true);
//            }else psp.setSectionSwitchTechnology(false);
//
//            if(switchButtonSectionsOpinion.isChecked()){
//                psp.setSectionSwitchOpinion(true);
//            }else psp.setSectionSwitchOpinion(false);
//
//            //topics
//            if(switchButtonTopics.isChecked()){
//                psp.setTopicsSwitch(true);
//            }else psp.setTopicsSwitch(false);
//
//            if(switchButtonTopicsHealth.isChecked()){
//                psp.setTopicsSwitchHealth(true);
//            }else psp.setTopicsSwitchHealth(false);
//
//            if(switchButtonTopicsPolitics.isChecked()){
//                psp.setTopicsSwitchPolitics(true);
//            }else psp.setTopicsSwitchPolitics(false);
//
//            if(switchButtonTopicsBeauty.isChecked()){
//                psp.setTopicsSwitchBeauty(true);
//            }else psp.setTopicsSwitchBeauty(false);
//
//            if(switchButtonTopicsArts.isChecked()){
//                psp.setTopicsSwitchArts(true);
//            }else psp.setTopicsSwitchArts(false);
//
//            if(switchButtonTopicsMusic.isChecked()){
//                psp.setTopicsSwitchMusic(true);
//            }else psp.setTopicsSwitchMusic(false);
//
//            if(switchButtonTopicsTravel.isChecked()){
//                psp.setTopicsSwitchTravel(true);
//            }else psp.setTopicsSwitchTravel(false);
//
//
//        });