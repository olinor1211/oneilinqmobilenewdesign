package com.knx.inquirer.inqprime.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.model.PrimeLikeModel;
import com.knx.inquirer.inqprime.utils.PrimeGlobal;
import com.knx.inquirer.inqprime.utils.PrimeSharedPrefs;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.Arrays;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PrimeArticle extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "PrimeArticle";

    //utils
    private PrimeSharedPrefs psp;
    private SharedPrefs sf;
    private DatabaseHelper helper;

    //api
    private ApiService apiService;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    //ui
    private ImageButton articleArrowBack;
    private TextView articleArrowBackLabel;
    private WebView webView;
    private TextView articleComment;
    private ShimmerFrameLayout shimmerFrameLayout;
    private ImageButton zoom, comment, like, share, bookmark;

    //variables
    private String id,photo, title, author, date, content, type, custom,  sectionName;
    private String htmlStr = "";
    private List<String> photoArray;
    private int fontSize;
    private boolean commentFlag = false;
    private final int PRIME_BOOKMARK_PERMISSION_CODE = 10;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prime_article);
        //no status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        sf = new SharedPrefs(this);
        psp = new PrimeSharedPrefs(this);
        apiService = ServiceGenerator.createService(ApiService.class);
        helper = new DatabaseHelper(this);

        //optional, to be set in the manifest
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        initViews();
        fetchData();
        configWebViewSettings();
        generateWebArticle();
        loadHtml();

    }

    //loadDataWithBaseURL..setWebChromeClient..onProgressChanged..
    private void loadHtml(){
        webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {

                    //enable button after the article has been displayed
                    zoom.setClickable(true);
                    comment.setClickable(true);
                    like.setClickable(true);
                    share.setClickable(true);
                    bookmark.setClickable(true);

                    //remove shimmer effect
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmer();

                }
            }
        });
    }

    private void configWebViewSettings() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)

//        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
//        webView.getSettings().setAllowFileAccessFromFileURLs(true);
//        webView.getSettings().setLoadsImagesAutomatically(true);
//
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
//        webView.getSettings().setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }


    //generateWebArticle
    private void generateWebArticle() {

       htmlStr = htmlStr + "<html>";
        //start.. Head and Style
        htmlStr = htmlStr + "<script src=\"file:///android_asset/jquery.1.7.1.min.js\"></script>" +  //jquery.1.7.1.min.js
                "<script src=\"file:///android_asset/listCarousel.js\"></script>" + //listCarousel.js
                "<script src=\"https://api.dmcdn.net/all.js\"></script>" +  //api.dmcdn.net/all.js
                "<head>" + "<meta name=\"viewport\" content=\"width=device-width, user-scalable=no\" />" +  //meta ..name,content,user-scalable
                "<style type=\"text/css\"> " + //style type,
                ".slider-previous{ display:none;}" +  //style  .slider-previous
                "table { width=100% }" +  //style for TABLE
                "div {word-wrap: break-word; padding: 10px;}" +  //style for DIV
                "@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/MalloryMP-Bold.otf\")} " + //style div
                "@font-face {font-family: MyFont2;src: url(\"file:///android_asset/fonts/MalloryMP-Book.otf\")} " +
                "@font-face {font-family: MyFont3;src: url(\"file:///android_asset/fonts/NotoSerif-Regular.ttf\")}" +
                "</style></head>";   //end.. Style

        //start.. Body
        htmlStr = htmlStr + "<body style='margin:0;padding:0;'>"; //style for BODY


        //Photo
        if (photo.length() > 0) {  //photo

            htmlStr = htmlStr + "<br>";

            //carousel
            if (type.equals("4")) {

                photoArray = Arrays.asList(photo.split(","));

                htmlStr = htmlStr + "<ul id=\"cms-slider\">";

                for (int i = 0; i < photoArray.size(); i++) {

                    htmlStr = htmlStr + "<li><img width = 100% height = 200dp  src=" + photoArray.get(i) + "></li>";
                }

                htmlStr = htmlStr + "</ul>" ;

            }else if (type.equals("7")) {
//            String videos = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4";
                htmlStr = htmlStr +
                        "<div>" +
                        "<video style=\"width: 100%; height: auto; padding-bottom:0;\"" +
                        "controls poster=\"file:///android_asset/images/img_2.png\" autoplay muted>" +
                        "<source src=" + photo +
                        " type=\"video/mp4\">" +
                        "</video>" +
                        "</div>";


            }else {
                String offlineImage  = photo;
                htmlStr = htmlStr + "<center><img width = 100% src=" + photo + "></center>";
            }

        } else {
            htmlStr = htmlStr + "";
        }

        //title..date..author
        if(!title.isEmpty()) //title
            htmlStr = htmlStr + "<br><div><strong><font size=4 color=#000000 face=MyFont>" + title + "</font></strong><br><br>";

        if(!date.isEmpty()) //date
            htmlStr = htmlStr + "<font color=#949494 size=2.5 face=MyFont2>" + date + "<br>" + "</font>";

        if(!author.isEmpty()) //author
            htmlStr = htmlStr + "<font color=#949494 size=2.5 face=MyFont2>" + author + "<br>" + "</font></div>";

        //content
        if(!content.isEmpty()) {
            htmlStr = htmlStr + "<div><font face=MyFont3>" + content.replace("\n", "<br>") + "</font></div><br>";

            htmlStr = htmlStr + "<script>$(function() {$('#cms-slider').listCarousel();})</script>";
//            htmlStr = htmlStr + "<script>\n" +
//                    "var player = DM.player(document.getElementById('dmplayer'), {\n" +
//                    "  video: 'xwr14q',\n" +
//                    "  params: {\n" +
//                    "  autoplay: 1\n" +
//                    "  }" +
//                    "});\n" +
//                    "player.addEventListener('play', function(e){\n" +
//                    "    player.setMuted(1);\n" +
//                    "});" +
//                    "</script>";
//            htmlStr = htmlStr + "</body></html>";

            //add lines here for comment
            if(commentFlag){
                htmlStr = htmlStr + "Comment here...";
            }

            htmlStr = htmlStr + "</body></html>";

        }else{
            htmlStr = htmlStr + "";
            htmlStr = htmlStr + "</body></html>";
        }

    }


    private void initViews() {
        articleArrowBack = findViewById(R.id.prime_article_arrowBack);
        articleArrowBackLabel = findViewById(R.id.prime_article_arrowBackLabel);
        webView = findViewById(R.id.prime_article_webView);
//        articleComment = findViewById(R.id.prime_articleComment);
        shimmerFrameLayout = findViewById(R.id.prime_shimmer_article);
        zoom = findViewById(R.id.prime_article_zoom);
        comment = findViewById(R.id.prime_article_comment);
        like = findViewById(R.id.prime_article_like);
        share = findViewById(R.id.prime_article_share);
        bookmark = findViewById(R.id.prime_article_bookmark);

        //disable button to click, until article is displayed
        zoom.setClickable(false);
        comment.setClickable(false);
        like.setClickable(false);
        share.setClickable(false);
        bookmark.setClickable(false);

        //attach onClickListener
        articleArrowBack.setOnClickListener(this);
        zoom.setOnClickListener(this);
        comment.setOnClickListener(this);
        like.setOnClickListener(this);
        share.setOnClickListener(this);
        bookmark.setOnClickListener(this);

    }

    private void fetchData() {
        Bundle data = getIntent().getExtras();

            id = data.getString("id", "");
            photo = data.getString("photo", "");
            title = data.getString("title", "");
            author = data.getString("author", "");
            date = data.getString("date", "");
            content = data.getString("content", "");
            type = data.getString("type", "");
            custom = data.getString("custom", "");
            sectionName = data.getString("section_name", "");
            articleArrowBackLabel.setText(sectionName);

        Log.d(TAG, "fetchData: Photo "+ photo);
        Log.d(TAG, "fetchData: ID "+ id);
        Log.d(TAG, "fetchData: type "+ type);
        Log.d(TAG, "fetchData: custom "+ custom);
        Log.d(TAG, "fetchData: content "+ content);

    }

    //need to override attachBaseContext ..to make the fontPath working
//    @Override
//    protected void attachBaseContext( Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    //Calligraphy3 ADDED for Tablet Android10
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    //onCLickListener
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.prime_article_zoom:
                    if (fontSize == 0) {
                        fontSize = 1;
                        webView.getSettings().setTextZoom(110);
                    } else if (fontSize == 1) {
                        fontSize = 2;
                        webView.getSettings().setTextZoom(120);
                    } else if (fontSize == 2) {
                        fontSize = 3;
                        webView.getSettings().setTextZoom(130);
                    } else if (fontSize == 3) {
                        fontSize = 0;
                        webView.getSettings().setTextZoom(100);
                    }
                break;
            case R.id.prime_article_comment:
                SpannableStringBuilder comment = PrimeGlobal.toastText("Comment");
                Toast.makeText(this, comment, Toast.LENGTH_SHORT).show();
                break;
            case R.id.prime_article_like:
                addToLike();
                break;
            case R.id.prime_article_share:
//                shareArticleLink2(id, type);
                shareArticleLink(id, type);
                break;
            case R.id.prime_article_bookmark:
                addToBookmark();
                break;
            case R.id.prime_article_arrowBack:
                onBackPressed();
                break;

        }
    }

    private void addToLike() {
        int newsId = Integer.parseInt(id);
        PrimeLikeModel likeModel = new PrimeLikeModel(newsId);
        helper.insertPrimeLike(likeModel);

        SpannableStringBuilder like = PrimeGlobal.toastText("Like");
        Toast.makeText(this, like, Toast.LENGTH_SHORT).show();
    }

    private void addToBookmark() {
        int newsId = Integer.parseInt(id); //coverted to int to make it primaryKey

        //insert/save to database
        PrimeBookmarkModel bookmarkModel = new PrimeBookmarkModel(newsId, photo, title, date, author);
        helper.insertPrimeBookmark(bookmarkModel);

        SpannableStringBuilder text = PrimeGlobal.toastText("This article is added to your bookmark.");
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "addToBookmark: called");

    }


    //shareArticleLink
    private void shareArticleLink(String id, String type){
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megashare/" + id);
        apiService.getShareLink(sf.getDeviceId(), id, type, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setData(Uri.parse("mailto:"));
                        intent.setType("text/plain");

                        intent.putExtra(Intent.EXTRA_TEXT, title + "\nvia Inquirer Mobile: " + link);
                        Log.d(TAG, "onSuccess: shareArticleLink " + link);

                        try {
                            startActivity(Intent.createChooser(intent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(PrimeArticle.this, "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: shareArticleLink "+ e.getMessage());
                    }
                });
    }

    //shareArticleLink
    private void shareArticleLink2(String id, String type){
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megashare/" + id);

        apiService.getShareLink2(String.valueOf(unixTime), sf.getDeviceId(), id, type, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setData(Uri.parse("mailto:"));
                        intent.setType("text/plain");

                        intent.putExtra(Intent.EXTRA_TEXT, title + "\nvia Inquirer Mobile: " + link);
                        Log.d(TAG, "onSuccess: shareArticleLink " + link);

                        try {
                            startActivity(Intent.createChooser(intent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(PrimeArticle.this, "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: shareArticleLink "+ e.getMessage());
                    }
                });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}




















//    //WORKING
//    private Bitmap convertUrlToBitmap(String image) {
//        try{
//            URL url = new URL(image);
////            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            URLConnection connection = url.openConnection();
////            connection.setDoInput(true);
//            connection.connect();
//            // this will be useful so that you can show a typical 0-100% progress bar
////            int fileLength = connection.getContentLength();
////            InputStream input = connection.getInputStream();
//            InputStream input = new BufferedInputStream(connection.getInputStream());
//            Bitmap myBitmap = BitmapFactory.decodeStream(input);
//            Log.d(TAG, "convertUrlToBitmap: "+ myBitmap);
//            return myBitmap;
//        }catch (Exception e){
//            Log.d(TAG, "convertUrlToBitmap: Exception "+ e.getMessage());
//            return null;
//        }
//    }



//    ////WORKING
//    private byte[] convertBitmapToByteArray(Bitmap bitmap) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, baos);
//        return baos.toByteArray();
//    }




//    private void bookmarkStoragePermission() {
//        //if permission is not yet granted
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
//            ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
//        {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, PRIME_BOOKMARK_PERMISSION_CODE);
//        }//if permission is granted already
//        else{
//            PrimeGlobal.CreatePathDirectory("/inquirer prime");
////            validateFile();
//        }
//    }


//manage text size of the Toast
//        String text = "This article is added to your bookmark.";
//        SpannableStringBuilder smallText = new SpannableStringBuilder(text);
//        smallText.setSpan(new RelativeSizeSpan(.80f), 0, text.length(), 0);