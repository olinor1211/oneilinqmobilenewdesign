package com.knx.inquirer.inqprime.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;
import com.knx.inquirer.inqprime.repositories.RepositoryPrimeNewsOffline;

import java.util.List;

public class ViewModelPrimeNewsOffline extends AndroidViewModel {

    private RepositoryPrimeNewsOffline repositoryPrimeNewsOffline;

    public ViewModelPrimeNewsOffline(@NonNull Application application) {
        super(application);
        repositoryPrimeNewsOffline = new RepositoryPrimeNewsOffline();
    }

    public LiveData<List<PrimeNewsOfflineModel>> getAllPrimeNews(){return repositoryPrimeNewsOffline.getAllPrimeNews();}

}
