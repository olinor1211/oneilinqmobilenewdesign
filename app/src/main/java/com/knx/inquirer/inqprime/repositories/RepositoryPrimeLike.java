package com.knx.inquirer.inqprime.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeLikeModel;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class RepositoryPrimeLike {

    private DatabaseHelper helper;
    private Context context;

    public RepositoryPrimeLike() {
        helper = new DatabaseHelper(context);
    }

    public LiveData<List<PrimeLikeModel>> getPrimeLikeList(){return helper.getPrimeLike();}

}
