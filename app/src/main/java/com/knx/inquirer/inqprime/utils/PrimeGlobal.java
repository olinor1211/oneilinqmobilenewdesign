package com.knx.inquirer.inqprime.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.knx.inquirer.R;
import com.knx.inquirer.inqprime.ui.PrimeAccount;
import com.knx.inquirer.inqprime.ui.PrimeAccountInfo;
import com.knx.inquirer.inqprime.ui.PrimeBookmark;
import com.knx.inquirer.inqprime.ui.PrimeMembershipBilling;
import com.knx.inquirer.inqprime.ui.PrimeNotification;
import com.knx.inquirer.inqprime.ui.PrimeSection;
import com.knx.inquirer.utils.SharedPrefs;
import java.util.ArrayList;


import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import com.bumptech.glide.Glide;
import com.knx.inquirer.BuildConfig;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;
import retrofit2.HttpException;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


public class PrimeGlobal {
	private static final String TAG = "PrimeGlobal";

	public static Context context;
	public static int countSection = 0;
//	public static Boolean switch_news_info = false;
//	public static Boolean switch_global_nation = false;
//	public static Boolean switch_sports = false;
//	public static Boolean switch_lifestyle = false;
//	public static Boolean switch_business = false;
//	public static Boolean switch_technology = false;
//	public static Boolean switch_opinion = false;
	public static int lastNewsIdDisplayed = 0;
	public static int lastItemPosition = 0;
	public static String DEVICE_ID = "";
	public static String DAILY_KEY = "";
	public static Boolean isOnline = true;
	public static Boolean isDeletedAlready = false;
	public static Boolean isRunAlready = false;






	//user information 12
	public static Integer accountId = 0;
	public static String accountEmail = "";
	public static String accountPassword;
	public static String accountUserName = "";
	public static String accountName = "";
	public static String accountMobileNo = "";
	public static String accountGender = "";
	public static String accountAddress = "";
	public static String accountBirthday = "";
	public static String accountAvatar = "";
	public static String accountCardNo = "";
	public static String accountPlan = "";

	//sections topics
	public static final String  DEFAULT_SECTION = "SPORT";  //"NEWS INFO";

	public static final int  PRIME_SECTION_BREAKING_NEWS_ID = 22; //to be set
	public static final int  PRIME_SECTION_SPORTS_ID = 2;
	public static final int  PRIME_SECTION_ENTERTAINMENT_ID = 3;
	public static final int  PRIME_SECTION_TECHNOLOGY_ID = 5;
	public static final int  PRIME_SECTION_GLOBAL_NATION_ID = 6;
	public static final int  PRIME_SECTION_BUSINESS_ID = 7;
	public static final int  PRIME_SECTION_LIFESTYLE_ID = 8;
	public static final int  PRIME_SECTION_GLOBAL_NEWS_ID = 9;
	public static final int  PRIME_SECTION_SPECIAL1_ID = 21;  //to be set
	public static final int  PRIME_SECTION_NEWS_INFO_ID = 1;  //to be set
	public static final int  PRIME_SECTION_OPINION_ID = 2;   //to be set  //TEMP is 2
	public static final int  PRIME_SECTION_MOBILITY_ID = 200;

	public static final int  PRIME_TOPIC_HEALTH_ID = 21;
	public static final int  PRIME_TOPIC_POLITICS_ID = 22;
	public static final int  PRIME_TOPIC_BEAUTY_ID = 23;
	public static final int  PRIME_TOPIC_ARTS_ID = 24;
	public static final int  PRIME_TOPIC_MUSICID = 25;
	public static final int  PRIME_TOPIC_TRAVEL_ID = 26;







	/**START..ALL ARE METHODS**/
	//
	public static void CreatePathDirectory(String pathDirectory) {
		File theDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + pathDirectory);
		if (!theDir.exists()) {
			theDir.mkdir(); // if the directory does not exist, create it
		}
	}

	//manage text size in the Toast
	public static SpannableStringBuilder toastText(String text){
		SpannableStringBuilder textMessage = new SpannableStringBuilder(text);
		textMessage.setSpan(new RelativeSizeSpan(.80f), 0, text.length(), 0);
		return textMessage;
	}

	/**END..ALL ARE METHODS**/


	/**START..ALL ARE CLASSES**/
	/**PrimeMainMenu **/ //showPrimeMainMenu
	public static class PrimeMainMenu {

		private SharedPrefs sf;
		private Context context;
		private ImageView ivMenuBg;
        private ImageButton ibHamburger;

        public PrimeMainMenu(Context context, ImageView iv, ImageButton ibHamburger) {
			this.ivMenuBg = iv;
            this.ibHamburger = ibHamburger;
            this.context = context;
		}

		public void showPrimeMainMenu() {
			ivMenuBg.setVisibility(View.VISIBLE);

			final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);  //full screen
			dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);  //set background transparent

			dialog.setCancelable(true);
			dialog.setContentView(R.layout.prime_main_menu);

			ImageButton hamburgerIcon = dialog.findViewById(R.id.prime_ibHamburger2);
			TextView account = dialog.findViewById(R.id.prime_menuAccount);
				RelativeLayout accountRL = dialog.findViewById(R.id.rl_menuAccount);
				TextView accountInfo = dialog.findViewById(R.id.prime_menuAccount_info);
				TextView accountLog = dialog.findViewById(R.id.prime_menuAccount_log);
				TextView accountMembership = dialog.findViewById(R.id.prime_menuAccount_membership);
				TextView accountSignOut = dialog.findViewById(R.id.prime_menuAccount_signOut);
			TextView sections = dialog.findViewById(R.id.prime_menuSections);
			TextView bookmarks = dialog.findViewById(R.id.prime_menuBookmarks);
			TextView notifications = dialog.findViewById(R.id.prime_menuNotifications);
			TextView settings = dialog.findViewById(R.id.prime_menuSettings);

			//menu
			hamburgerIcon.setOnClickListener(v -> {
				if(dialog.isShowing()){
					dialog.dismiss();
					ivMenuBg.setVisibility(View.GONE);
					ibHamburger.setVisibility(View.VISIBLE);
				}
			});

			//account
			account.setOnClickListener(v -> {
				Intent i = new Intent(context, PrimeAccount.class);
				context.startActivity(i);
			});

			//sections
			sections.setOnClickListener(v -> {
				Intent i = new Intent(context, PrimeSection.class);
				context.startActivity(i);
			});

			//bookmark log
			bookmarks.setOnClickListener(v -> {
				Intent i = new Intent(context, PrimeBookmark.class);
				context.startActivity(i);
			});
			//notification
			notifications.setOnClickListener(v -> {
				Intent i = new Intent(context, PrimeNotification.class);
				context.startActivity(i);
			});

			//account log
			settings.setOnClickListener(v -> Toast.makeText(context, "Settings", Toast.LENGTH_SHORT).show());

			//onBackPressed in dialog
			dialog.setOnCancelListener(dialog1 -> {
			    dialog.dismiss();
                ivMenuBg.setVisibility(View.GONE);
				ibHamburger.setVisibility(View.VISIBLE);
			});

		dialog.show();

		}
	}

	/** Progress Loading Dialog**/
	//CLASS LoaderDialog
	public static class ProgressLoaderDialog {

		Dialog dialog;
		Context context;

		public ProgressLoaderDialog(Context context){
			this.context = context;
		}

		public void showDialog(){
			dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//			dialog.getWindow().setBackgroundDrawableResource(R.color.trans_black_dialog);
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.loader_dialog);

			ImageView ivLoader = dialog.findViewById(R.id.gifLoader);

			Glide.with(context).asGif().load(R.drawable.inqloader2).into(ivLoader);

			dialog.show();
		}

		public void dismissDialog(){
			dialog.dismiss();
		}
	}
	/**END..ALL ARE CLASSES**/
}





//////////////OLD
//			//account
//			account.setOnClickListener(v -> {
//
//				if(accountRL.getVisibility() == View.VISIBLE){
//					accountRL.setVisibility(View.GONE);
//				}else{
//					accountRL.setVisibility(View.VISIBLE);
//				}
//			});

//			//account info
//			accountInfo.setOnClickListener(v -> {
//				Intent i = new Intent(context, PrimeAccountInfo.class);
//				context.startActivity(i);
//			});
//
//			//account log
//			accountLog.setOnClickListener(v -> Toast.makeText(context, "Account Log", Toast.LENGTH_SHORT).show());
//
//			//account membership
//			accountMembership.setOnClickListener(v -> {
//				Intent i = new Intent(context, PrimeMembershipBilling.class);
//				context.startActivity(i);
//			});
//			//account signout
//			accountSignOut.setOnClickListener(v -> Toast.makeText(context, "Account Sign out", Toast.LENGTH_SHORT).show());
//////////////OLD
