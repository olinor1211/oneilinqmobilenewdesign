package com.knx.inquirer.inqprime.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "PrimeAccountInformation")
public class PrimeAccountInformationModel {

    //Dont autoGenerate primary key to make onConflict = OnConflictStrategy.REPLACE take effect.. making newsId as primary key
//    @PrimaryKey(autoGenerate = true)
//    private int id;

    @PrimaryKey
    @NonNull
    private int userId;

    private String accountEmail;
    private String accountPassword;
    private String userName;
    private String accountName;
    private String mobileNo;
    private String gender;
    private String address;
    private String birthday;
    private String avatar;
    private String cardNo;
    private String plan;


//    public PrimeAccountInformationModel(){}

    public PrimeAccountInformationModel(Integer userId, String accountEmail, String accountPassword, String userName, String accountName, String mobileNo, String gender, String address, String birthday, String avatar, String cardNo, String plan) {
        this.userId = userId;
        this.accountEmail = accountEmail;
        this.accountPassword = accountPassword;
        this.userName = userName;
        this.accountName = accountName;
        this.mobileNo = mobileNo;
        this.gender = gender;
        this.address = address;
        this.birthday = birthday;
        this.avatar = avatar;
        this.cardNo = cardNo;
        this.plan = plan;

    }

    public int getUserId() {
        return userId;
    }

    public String getAccountEmail() {
        return accountEmail;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public String getUserName() {
        return userName;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getCardNo() {
        return cardNo;
    }

    public String getPlan() {
        return plan;
    }


//
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//
//    public void setBirthday(String birthday) {
//        this.birthday = birthday;
//    }
//
//
//    public void setAvatar(String avatar) {
//        this.avatar = avatar;
//    }
//
//
//    public void setCardNo(String cardNo) {
//        this.cardNo = cardNo;
//    }
//
//
//    public void setAccountPassword(String accountPassword) {
//        this.accountPassword = accountPassword;
//    }
////
//    public void setPlan(String plan) {
//        this.plan = plan;
//    }
//
//
//    public void setUserId(int id) {
//        this.userId = id;
//    }
//
//    public void setAccountEmail(String accountEmail) {
//        this.accountEmail = accountEmail;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public void setAccountName(String accountName) {
//        this.accountName = accountName;
//    }
//
//    public void setMobileNo(String mobileNo) {
//        this.mobileNo = mobileNo;
//    }
//
}
