package com.knx.inquirer.inqprime.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.repositories.RepositoryPrimeBookmark;

import java.util.List;

public class ViewModelPrimeBookmark extends AndroidViewModel {

    private RepositoryPrimeBookmark repositoryPrimeBookmark;

    public ViewModelPrimeBookmark(@NonNull Application application) {
        super(application);
        repositoryPrimeBookmark = new RepositoryPrimeBookmark();
    }

    public LiveData<List<PrimeBookmarkModel>> getPrimeBookmarkList(){return repositoryPrimeBookmark.getPrimeBookmarkList();}
}
