package com.knx.inquirer.inqprime.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.repositories.RepositoryPrimeAccountInformation;

import java.util.List;

public class ViewModelPrimeAccountInformation extends AndroidViewModel {

    private RepositoryPrimeAccountInformation repositoryPrimeAccountInformation;

    public ViewModelPrimeAccountInformation(@NonNull Application application) {
        super(application);
        repositoryPrimeAccountInformation = new RepositoryPrimeAccountInformation();
    }

    public LiveData<List<PrimeAccountInformationModel>> getPrimeAccountInformationList(){return repositoryPrimeAccountInformation.getPrimeAccountInformationList();}

}
