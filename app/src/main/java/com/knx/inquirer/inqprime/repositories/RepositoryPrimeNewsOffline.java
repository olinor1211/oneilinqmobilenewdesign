package com.knx.inquirer.inqprime.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;

import java.util.List;

public class RepositoryPrimeNewsOffline {

    private DatabaseHelper helper;
    private Context context;

    public RepositoryPrimeNewsOffline() {
        helper = new DatabaseHelper(context);
    }

    public LiveData<List<PrimeNewsOfflineModel>> getAllPrimeNews(){return helper.getAllPrimeNews();}

}
