package com.knx.inquirer.inqprime.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeSectionsModel;
import com.knx.inquirer.inqprime.model.PrimeTopicsModel;

import java.util.List;

public class RepositoryPrimeTopics {

    private DatabaseHelper helper;
    private Context context;

    public RepositoryPrimeTopics() {

        helper = new DatabaseHelper(context);
    }

    public LiveData<List<PrimeTopicsModel>> getPrimeTopicsStatus(){return helper.getPrimeTopicsStatus();}

}
