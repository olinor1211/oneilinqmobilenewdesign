package com.knx.inquirer.inqprime.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.knx.inquirer.R;
import com.knx.inquirer.inqprime.model.PrimeNewsBySectionModel;
import com.knx.inquirer.inqprime.utils.OnViewHolderItemListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RVAdapterInqPrimeMain extends RecyclerView.Adapter<RVAdapterInqPrimeMain.ItemViewHolder>{
    private static final String TAG = "RVAdapterInqPrimeMain";

    private Context context;
    private List<PrimeNewsBySectionModel> newsList;
    private String sectionName;
//    private List<NewsModel> newsList;

    private OnViewHolderItemListener mOnViewHolderItemListener;  //declare interface in RV adapter

    public RVAdapterInqPrimeMain(Context context, List<PrimeNewsBySectionModel> newsList, OnViewHolderItemListener onViewHolderItemListener, String sectionName) { //List<NewsModel> newsList
        this.context = context;
        this.newsList = newsList;
        this.mOnViewHolderItemListener = onViewHolderItemListener;
        this.sectionName = sectionName;
//        this.newsList = newsList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inqprime_main_list_row, viewGroup, false);
        return new ItemViewHolder(v, mOnViewHolderItemListener);  //add interface as an additional parameter
    }


    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {

//        NewsModel news = newsList.get(position);
        PrimeNewsBySectionModel news = newsList.get(position);

        String date = news.getPubdate();
        String title = news.getTitle();
        String photo = news.getImage();
        String sectionName__ = news.getSection(); //to be set


        itemViewHolder.tvDate.setText(date);
        itemViewHolder.tvTitle.setText(title);

        if(!photo.isEmpty()){
            Picasso.get()
                    .load(photo)
                    .noFade()
                    .noPlaceholder()
                    .into(itemViewHolder.photoImage);
        }


            switch (sectionName){    //check what section of each RV item, getSection()
                case "NEWS INFO":
                    itemViewHolder.tvTag.setText("NEWS INFO");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.globalnation));
                    break;
                case "SPORT":
                    itemViewHolder.tvTag.setText("SPORT");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.sports));
                    break;
                case "ENTERTAINMENT":
                    itemViewHolder.tvTag.setText("ENTERTAINMENT");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.entertainment));
                    break;
                case "TECHNOLOGY":
                    itemViewHolder.tvTag.setText("TECHNOLOGY");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.technology));
                    break;
                case "GLOBAL NATION":
                    itemViewHolder.tvTag.setText("GLOBAL NATION");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.globalnation));
                    break;
                case "BUSINESS":
                    itemViewHolder.tvTag.setText("BUSINESS");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.business));
                    break;
                case "LIFESTYLE":
                    itemViewHolder.tvTag.setText("LIFESTYLE");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.lifestyle));
                    break;
                case "GLOBAL NEWS":
                    itemViewHolder.tvTag.setText("GLOBAL NEWS");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.globalnation));
                    break;
                case "OPINION":
                    itemViewHolder.tvTag.setText("OPINION");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.opinion));
                    break;
                default:
                    itemViewHolder.tvTag.setText("NEWS INFO");
                    itemViewHolder.tvTag.setBackgroundColor(context.getResources().getColor(R.color.globalnation));
                    break;
            }
    }



    @Override
    public int getItemCount()
    {
        return newsList == null ? 0 : newsList.size();
    }

    //1.implements the View.OnClickListener.. 2.use/declare the interface.., 3.put interface inside constructor.., inside onClick use/call the interface method
    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView photoImage;
        TextView tvDate;
        TextView tvTitle;
        TextView tvTag;
        OnViewHolderItemListener onViewHolderItemListener;

        public ItemViewHolder(@NonNull View itemView, OnViewHolderItemListener onViewHolderItemListener) {
            super(itemView);
            this.onViewHolderItemListener = onViewHolderItemListener;

            photoImage = itemView.findViewById(R.id.imagePhotoInqPrime);
            tvDate = itemView.findViewById(R.id.textDatesInqPrime);
            tvTitle = itemView.findViewById(R.id.textTitleInqPrime);
            tvTag = itemView.findViewById(R.id.textTagSectionInqPrime);

            //attaching itemView to the interface..
            itemView.setOnClickListener(this);

        }

        //call/use the interface method, ..getAdapterPosition() as the parameter
        @Override
        public void onClick(View v) {
            onViewHolderItemListener.onViewHolderItemClick(v, getAdapterPosition());
        }
    }

}













/*
//How to use Interface for onClick itemView in RecyclerVIew
1. create Interface
2. use interface in the ViewHolder: Note: implements the interface
    a. declare the interface
    b. put interface as an additional parameter in the Constructor
    c. set/call/use interface method inside the onClick method, Note: implements View.OnClickListener, Note: attach itemView to interface, use itemView.setOnClickListener(this)
3. use interface in the RecyclerView Adapter: Note: implements the interface
    a. declare the interface
    b. put interface as an additional parameter in the Constructor, the Activity will pass the interface as parameter
    c. put interface as an additional parameter int the onCreateViewHolder to be able to used by ViewHolder
4. use interface in the Activity Note: implements the interface
    a. declare the interface
    b. put interface as an additional parameter in the RecyclerView Adapter
    c. done. interface method will be called everytime the item is clicked

 */

