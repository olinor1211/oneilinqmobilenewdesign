package com.knx.inquirer.inqprime.utils;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

//class file filter
public class PrimeWildCardFileFilter implements FileFilter {
    private String _pattern;

    public PrimeWildCardFileFilter(String pattern) {
        _pattern = pattern.replace("*", ".*").replace("?", ".");
    }

    public boolean accept(File file) {
        return Pattern.compile(_pattern).matcher(file.getName()).find();
    }
}
