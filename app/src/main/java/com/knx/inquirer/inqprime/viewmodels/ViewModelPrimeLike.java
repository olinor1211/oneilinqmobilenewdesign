package com.knx.inquirer.inqprime.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.inqprime.model.PrimeLikeModel;
import com.knx.inquirer.inqprime.repositories.RepositoryPrimeLike;

import java.util.List;

public class ViewModelPrimeLike extends AndroidViewModel {

    private RepositoryPrimeLike repositoryPrimeLike;

    public ViewModelPrimeLike(@NonNull Application application) {
        super(application);
        repositoryPrimeLike = new RepositoryPrimeLike();
    }

    public LiveData<List<PrimeLikeModel>> getPrimeLikeList(){return repositoryPrimeLike.getPrimeLikeList();}

}
