package com.knx.inquirer.inqprime.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "PrimeBookmark")
public class PrimeBookmarkModel {

    //Dont autoGenerate primary key to make onConflict = OnConflictStrategy.REPLACE take effect.. making newsId as primary key
//    @PrimaryKey(autoGenerate = true)
//    private int id;

    @PrimaryKey
    @NonNull
    private int newsId;

    private String image;
    private String title;
    private String date;
    private String author;

    public PrimeBookmarkModel(int newsId, String image, String title, String date, String author) {
        this.newsId = newsId;
        this.image = image;
        this.title = title;
        this.date = date;
        this.author = author;
    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
