package com.knx.inquirer.inqprime.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;

import java.util.List;

public class RepositoryPrimeBookmark {

    private DatabaseHelper helper;
    private Context context;

    public RepositoryPrimeBookmark() {
        helper = new DatabaseHelper(context);
    }

    public LiveData<List<PrimeBookmarkModel>> getPrimeBookmarkList(){return helper.getPrimeBookmarks();}

}
