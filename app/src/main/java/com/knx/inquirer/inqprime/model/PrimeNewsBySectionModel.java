package com.knx.inquirer.inqprime.model;


import androidx.annotation.NonNull;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

//@Entity(tableName = "PrimeNewsBySection")
public class PrimeNewsBySectionModel {
    @PrimaryKey
    @SerializedName("id")
    @NonNull
    private String id;

    @SerializedName("type")
    private String type;

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("pubdate")
    private String pubdate;

    @SerializedName("link")
    public String link;

    @SerializedName("custom")
    public String label;

    @SerializedName("section")
    public String section;

    //Empty Constructor

    //Testing purposes Constructor not empty
    public PrimeNewsBySectionModel(String id, String type , String title, String image, String pubdate, String link, String label, String section) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.image = image;
        this.pubdate = pubdate;
        this.link = link;
        this.label = label;
        this.section = section;
    }
//
//    public int getColorNewsInfo() {
//        int color = Color.parseColor("#1773B3");
//        return color;
//    }
//
//    public int getColorGlobalNation() {
//        int color = Color.parseColor("#084D84");
//        return color;
//    }
//
//    public int getColorSport() {
//        int color = Color.parseColor("#D42230");
//        return color;
//    }
//
//    public int getColorLifeStyle() {
//        int color = Color.parseColor("#B0447A");
//        return color;
//    }
//
//    public int getColorBusiness() {
//        int color = Color.parseColor("#6BAE30");
//        return color;
//    }
//
//    public int getColorTechnology() {
//        int color = Color.parseColor("#2cc1f0");
//        return color;
//    }
//
//    public int getColorOpinion() {
//        int color = Color.parseColor("#88A4B7");
//        return color;
//    }
//
//



    //Getters and Setters

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @NonNull
    public String getNewsId() {
        return id;
    }

    public void setNewsId(@NonNull String newsId) {
        this.id = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubdate() {
        return pubdate;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

//    //to be able to use the .equals
//    @Override
//    public boolean equals(Object object) {
//        boolean result = false;
//        if (object == null || object.getClass() != getClass()) {
//        } else {
//            PrimeNewsBySectionModel news = (PrimeNewsBySectionModel) object;
//            if (this.newsId.equals(news.getNewsId())) {
//                result = true;
//            }
//        }
//        return result;
//    }

}
