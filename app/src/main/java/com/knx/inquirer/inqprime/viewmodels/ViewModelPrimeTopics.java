package com.knx.inquirer.inqprime.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.inqprime.model.PrimeTopicsModel;
import com.knx.inquirer.inqprime.repositories.RepositoryPrimeTopics;

import java.util.List;

public class ViewModelPrimeTopics extends AndroidViewModel {

    private RepositoryPrimeTopics repositoryPrimeTopics;

    public ViewModelPrimeTopics(@NonNull Application application) {
        super(application);
        repositoryPrimeTopics = new RepositoryPrimeTopics();
    }

    public LiveData<List<PrimeTopicsModel>> getPrimeTopicsStatus(){return repositoryPrimeTopics.getPrimeTopicsStatus();}

}
