package com.knx.inquirer.inqprime.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

//import com.amitshekhar.DebugDB;
//import com.amitshekhar.DebugDB;
import com.bumptech.glide.Glide;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.adapters.RVAdapterInqPrimeMain;
import com.knx.inquirer.inqprime.adapters.RVAdapterInqPrimeMainOffline;
import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.model.PrimeLikeModel;
import com.knx.inquirer.inqprime.model.PrimeNewsBySectionModel;
import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;
import com.knx.inquirer.inqprime.model.PrimeSectionsModel;
import com.knx.inquirer.inqprime.model.PrimeTopicsModel;
import com.knx.inquirer.inqprime.network.PrimeApiService;
import com.knx.inquirer.inqprime.utils.OnViewHolderItemListener;
import com.knx.inquirer.inqprime.utils.PrimeGlobal;
import com.knx.inquirer.inqprime.utils.PrimeSharedPrefs;
import com.knx.inquirer.inqprime.viewmodels.ViewModelPrimeAccountInformation;
import com.knx.inquirer.inqprime.viewmodels.ViewModelPrimeLike;
import com.knx.inquirer.inqprime.viewmodels.ViewModelPrimeBookmark;
import com.knx.inquirer.inqprime.viewmodels.ViewModelPrimeNewsBySection;
import com.knx.inquirer.inqprime.viewmodels.ViewModelPrimeNewsOffline;
import com.knx.inquirer.inqprime.viewmodels.ViewModelPrimeSections;
import com.knx.inquirer.inqprime.viewmodels.ViewModelPrimeTopics;
import com.knx.inquirer.models.Article;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodels.NewsViewModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.renderscript.*;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class InqPrimeMainActivity extends AppCompatActivity implements View.OnClickListener, OnViewHolderItemListener {

    private static final String TAG = "InqPrimeMainActivity";

    //recyclerView
    private RecyclerView rvInqPrimeHorizontal;
    private RecyclerView rvInqPrimeHorizontalOffline;
    private RVAdapterInqPrimeMain rvAdapterInqPrimeMain;
    private RVAdapterInqPrimeMainOffline rvAdapterInqPrimeMainOffline;
    private ArrayList<PrimeNewsBySectionModel> newsList = new ArrayList<>();
    private List<PrimeNewsOfflineModel> newsListOffline = new ArrayList<>();
    private List<PrimeNewsOfflineModel> newsArticle = new ArrayList<>(); //OFFLINE: for exclusive single NEWS_ID record

    private OnViewHolderItemListener onViewHolderItemListener;

    //api
    private NewsViewModel newsViewModel;
    private ViewModelPrimeNewsBySection viewModelPrimeNewsBySection;

    private CompositeDisposable compositeDisposable;
    private PrimeApiService primeApiService;
    private ApiService apiService;
    private DatabaseHelper helper;

    //utils
    private SharedPrefs sf;
    private PrimeSharedPrefs psp;
    private PrimeGlobal.ProgressLoaderDialog progressLoaderDialog = new PrimeGlobal.ProgressLoaderDialog(this);


    //ui
    private TextView mainTagSectionName, titlePrime, bodyPrime, datePrime, bookmarkPrimeLabel, likePrimeLabel, sharePrimeLabel ;
    private TextView refreshInternetConnection;
    private ImageView backgroundPrime, bookmarkPrime, likePrime, sharePrime;
    private ImageButton hamburgerPrime;
    private View viewColorOverlay;
    private ImageView menuBackground;
    private Bitmap bmScreenshot;
    private ConstraintLayout parentViewCl, clTagLabel, clBody;
    private RelativeLayout rlRecyclerView;
    private RelativeLayout shareRlPrime ;//, likeRlPrime, bookmarkRlPrime;
    private RelativeLayout rlSpinner;
    private LinearLayout llNoInternetConnection;
    private Spinner prime_spinLatest;
    private Dialog dialog;
    private final LinearSnapHelper snapHelper = new LinearSnapHelper();
    private final LinearSnapHelper snapHelperOffline = new LinearSnapHelper();
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
    private LinearLayoutManager linearLayoutManagerOffline = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);



    //variables
    //    private Integer[] colors_overlay = null;
//    private Integer[] colors_main_tag = null;
    private Integer[] colors_fill = null;
//    private String[] sections_name_main_tag = null;
    private String newsId, newsType, newsTitle, newsPhoto, newsDate, newsAuthor;
    private String testString = "Teenager Dennis Chung struck a late equalizer as the Philippines salvaged a 1-1 draw with Cambodia on Monday night in the 2019 Southeast Asian Games.";
    private String spinLatest;
//    private List<PrimeBookmarkModel> bookmarks = new ArrayList<>();
    private HashSet<Integer> bookmark_id = new HashSet<>();
//    private List<PrimeLikeModel> likes = new ArrayList<>();
    private HashSet<Integer> like_id = new HashSet<>();

    private List<PrimeSectionsModel> primeSectionsModel = new ArrayList<>();
    private List<PrimeTopicsModel> primeTopicsModels = new ArrayList<>();

    private boolean isConnectionChange = false;

    //Listen the EVENT in NetworkSchedulerService
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        onConnectionChange(msg);    //Refresh the display news depend on Online or Offline
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Log.d(TAG, "DATABASE DEBUG : "+ DebugDB.getAddressLog());  //Remove when RELEASE

        setContentView(R.layout.activity_inq_prime_main);
        //no status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        psp = new PrimeSharedPrefs(this);
        sf = new SharedPrefs(this);
        compositeDisposable = new CompositeDisposable();
        primeApiService = ServiceGenerator.createService(PrimeApiService.class);
        apiService = ServiceGenerator.createService(ApiService.class);
        helper = new DatabaseHelper(this);
//        PrimeGlobal.DEVICE_ID = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);

//        insertNotif(); //SAMPLE DATA

        initView();
        initSectionsTopics();  //initialization of database
        displayNews(); //NEWS Depends on Sections selected otherwise load the Default
        getTopics();  //TOPICS
        getBookmarkList();  //BOOKMARKS
        getLikeList();   //LIKES
        getAccountInfo();//test
//        getAllNewsBySection();



    }

//    private void getAllNewsBySection() {
//      if(!PrimeGlobal.isRunAlready){
//        PrimeGlobal.isRunAlready = true;
//        int[] sections = {1,2,3,5,6,7,8,9};
//
//        for(int x=0; x<sections.length; x++){
////            getAllPrimeNewsBySection(sections[x]);  //
//        }
//      }
//    }

    private void initSectionsTopics() {
        primeSectionsModel = helper.getPrimeSections(); //READING the DB of PrimeSections
        if(primeSectionsModel.size()<=0){  //IF NO RECORDS, Insert default data
            Log.d(TAG, "initSectionsTopics: No Records ");
            initSections(); //insert NAME of SECTIONS... use for observing the status of switch
            initTopics();   //insert NAME of TOPICS... use for observing the status of switch
            saveAccountInfo(); //temporary until API is available  //INSERTING USER credentials
        }else Log.d(TAG, "initSectionsTopics: Have Records");
    }


    //Displaying news is depends on the Sections switch selected, How many sections switch are selected  //then goTo getNewsBySection()
    private void displayNews() {

        ViewModelPrimeSections viewModelPrimeSections ;
        viewModelPrimeSections = ViewModelProviders.of(this).get(ViewModelPrimeSections.class);
        viewModelPrimeSections.getPrimeSectionsStatus().observe(this, data ->{
            if (data.size()>0){

                primeSectionsModel.clear();
                primeSectionsModel.addAll(data);
                Log.d(TAG, "displayNews: SECTION_STATUS _COUNT "+data.size());

                //display SINGLE section news feed //IF has only ONE SECTIONS switch SELECTED
                if (primeSectionsModel.size()==1){
                    Log.d(TAG, "displayNews: primeSectionsModel getPrimeId "+ primeSectionsModel.get(0).getPrimeId());
                    psp.setSingleNewsSection(primeSectionsModel.get(0).getPrimeSections()); //setSingleNewsSection
                                                                      //mainTagSectionName must change the TagName

//                    //Display news ONLINE or OFFLINE
                    if(Global.isNetworkAvailable(this)){
                        getNewsBySection(Integer.valueOf(primeSectionsModel.get(0).getPrimeId()),"single" ); //passing the sectionId and sectionName

                    }else {
                        //No internet
                        displayOfflineNews();
                    }


                }else if (primeSectionsModel.size()>1) //display MULTIPLE news feed  ////IF has ONE or MORE SECTIONS switch SELECTED
                {
                    for(int x = 0; x<primeSectionsModel.size(); x++){
                        //show multiple sections
                        Log.d(TAG, "displayNews: "+primeSectionsModel.get(x).getPrimeSections());
                        mainTagSectionName.setText(primeSectionsModel.get(0).getPrimeSections());  //change the TagName //TEMP: only the first sections switch is implemented to display
                    }
                    Log.d(TAG, "displayNews:MULTIPLE called");

                }else{
                    getDefaultNewsFeed(); //Default news
                }

            //DEFAULT NEWS
            }else{
                getDefaultNewsFeed();  //Default news
            }

        });

    }


    private void getDefaultNewsFeed() {
        if(Global.isNetworkAvailable(this)){
            llNoInternetConnection.setVisibility(View.GONE);
            prime_spinLatest.setVisibility(View.GONE);  //hide spinner
            psp.setDefaultNewsSection(PrimeGlobal.DEFAULT_SECTION);  //default section
            //display default news feed
            getNewsBySection(2,"default" ); //passing the sectionId and sectionName  //DEFAULT is 2 Sports

        }else{
            displayOfflineNews();
//            llNoInternetConnection.setVisibility(View.VISIBLE);
        }

    }


    private void getTopics() {

        ViewModelPrimeTopics viewModelPrimeTopics;
        viewModelPrimeTopics = ViewModelProviders.of(this).get(ViewModelPrimeTopics.class);
        viewModelPrimeTopics.getPrimeTopicsStatus().observe(this, data ->{
            if (data.size()>0) {
                primeTopicsModels.clear();
                primeTopicsModels.addAll(data);

                for(int x = 0; x<primeTopicsModels.size(); x++){
                    //show multiple topics
                    Log.d(TAG, "TOPIC_STATUS: "+primeTopicsModels.get(x).getPrimeTopics());
                }

                Log.d(TAG, "displayNews: TOPIC_STATUS _COUNT " + data.size());

            }else Log.d(TAG, "displayNews: TOPIC_STATUS _COUNT " + data.size());

        });

    }

    //need to override attachBaseContext ..to make the fontPath working
//    @Override
//    protected void attachBaseContext( Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    //Calligraphy3 ADDED for Tablet Android10
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


    private void initView() {

        Integer[] color_fill = {
                R.color.color_fill,
                R.drawable.bookmark_fill,
                R.drawable.like_fill,
                R.drawable.share_fill
        };

        colors_fill = color_fill;

        //parent layout
//        parentViewCl = findViewById(R.id.prime_cl_container);
        clTagLabel = findViewById(R.id.prime_cl_tagLabel);
        clBody = findViewById(R.id.prime_cl_body);
        rlSpinner = findViewById(R.id.rl_spinner);
        rlRecyclerView = findViewById(R.id.prime_rl_recyclerView);
        shareRlPrime = findViewById(R.id.prime_rl_share);
        llNoInternetConnection = findViewById(R.id.ll_noInternetConnection);
        //image button
        hamburgerPrime = findViewById(R.id.prime_ibHamburger);
        //image view
        bookmarkPrime = findViewById(R.id.prime_ivBookmark);
        likePrime = findViewById(R.id.prime_ivLike);
        //text view
        mainTagSectionName = findViewById(R.id.prime_tvSectionName);
        titlePrime = findViewById(R.id.prime_tvTitle);
        bodyPrime = findViewById(R.id.prime_tvBody);
        datePrime = findViewById(R.id.prime_tvDate);
        bookmarkPrimeLabel = findViewById(R.id.prime_bookmarkLabel);
        likePrimeLabel = findViewById(R.id.prime_likeLabel);
        refreshInternetConnection = findViewById(R.id.refreshNoInternetConnection);
        //spinner
        prime_spinLatest = findViewById(R.id.prime_spinLatest);

        menuBackground = findViewById(R.id.prime_blur);
        menuBackground.setVisibility(View.GONE);  //

        //color
        backgroundPrime = findViewById(R.id.prime_ivImageBackground);
        viewColorOverlay = findViewById(R.id.viewColorOverlay);

        //RV
        rvInqPrimeHorizontal = findViewById(R.id.rvInqPrimeMain);
        rvInqPrimeHorizontalOffline = findViewById(R.id.rvInqPrimeMainOffline);

        //RV settings DEfend on Connection
        String sectionName = psp.getDefaultNewsSection();
        //Set RV for Snapping

            //ONLINE*** RV settings
            rvInqPrimeHorizontal.setLayoutManager(linearLayoutManager);
            rvInqPrimeHorizontal.setHasFixedSize(true);
            rvAdapterInqPrimeMain =  new RVAdapterInqPrimeMain(this, newsList, this::onViewHolderItemClick, sectionName);  //pass the interface

            rvInqPrimeHorizontal.setAdapter(rvAdapterInqPrimeMain);
            snapHelper.attachToRecyclerView(rvInqPrimeHorizontal); //Implement SNAP_HELPER

            rvInqPrimeHorizontal.addOnScrollListener(new RecyclerView.OnScrollListener() {  //ScrollListener
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {  //ScrollStateChanged
                    super.onScrollStateChanged(recyclerView, newState);
                    View v = snapHelper.findSnapView(linearLayoutManager);  //View linear layout of RV
                    int pos = linearLayoutManager.getPosition(v);   // track down the item position

                    //ACCESSING RecyclerView ViewHolder class
                    RecyclerView.ViewHolder viewHolder = rvInqPrimeHorizontal.findViewHolderForAdapterPosition(pos);
//                    RelativeLayout rl1 = viewHolder.itemView.findViewById(R.id.rl_rv_main_container);
                    TextView tvTag = viewHolder.itemView.findViewById(R.id.textTagSectionInqPrime);

                    String photoStr = newsList.get(pos).getImage(); //

                    if (newState == RecyclerView.SCROLL_STATE_IDLE){    //Idle state of RV,
                        Log.d(TAG, "onScrollStateChanged: position is " + pos);
                        if(!photoStr.isEmpty()){
                            Picasso.get()
                                    .load(photoStr)
                                    .noFade()
                                    .noPlaceholder()
                                    .into(backgroundPrime);
                        }

                        //get ID for title,date,body... get ID and TYPE for share link
//                    String id = newsList.get(pos).getId();
                        String id = newsList.get(pos).getNewsId();
                        String type = newsList.get(pos).getType();
                        String title = newsList.get(pos).getTitle();
                        String date = newsList.get(pos).getPubdate();
                        String author = newsList.get(pos).getLabel();
                        String sectionName = tvTag.getText().toString();  //just a work around waiting for the section from APi
//                    String sectionName = newsList.get(pos).getSection();

                        newsId = id;
                        newsType = type;
                        newsTitle = title;
                        newsDate = date;
                        newsPhoto = photoStr;
                        newsAuthor = author;

                        titlePrime.setText(title);
                        datePrime.setText(date);



                        //sectionName, to be replace if there is a section name in API,, //change color background
//                    switch (psp.getSingleNewsSection()){  //to be replace with sectionName
                        switch (sectionName){  //to be replace with sectionName
                            case "NEWS INFO":
                                viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                                mainTagSectionName.setText(sectionName);
                                break;

                            case "GLOBALNATION":
                                viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                                mainTagSectionName.setText(sectionName);

                                break;
                            case "SPORT":
                                viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_sports));
                                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.sports)); //change the background tag
                                mainTagSectionName.setText(sectionName);

                                break;
                            case "LIFESTYLE":
                                viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_lifestyle));
                                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.lifestyle)); //change the background tag
                                mainTagSectionName.setText(sectionName);

                                break;
                            case "BUSINESS":
                                viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_business));
                                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.business)); //change the background tag
                                mainTagSectionName.setText(sectionName);

                                break;
                            case "TECHNOLOGY":
                                viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_technology));
                                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.technology)); //change the background tag
                                mainTagSectionName.setText(sectionName);

                                break;
                            case "OPINION":
                                viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_opinion));
                                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.opinion)); //change the background tag
                                mainTagSectionName.setText(sectionName);
                                break;
                            default:
                                viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                                mainTagSectionName.setText(sectionName);
                                break;
                        }


                        //bookmark checking.. newsId is originally String, convert to int
                        int news_ID = Integer.valueOf(newsId);
                        if(bookmark_id.contains(news_ID)){ // if(bookmark_id.contains(news_ID)){ //bookmark_id is a HASHset
                            bookmarkPrime.setImageResource(R.drawable.new_bookmark_fill); //bookmarkPrime.setImageResource(R.drawable.bookmark_fill);
//                        Toast.makeText(InqPrimeMainActivity.this, "Meron "+ bookmark_id + "...NEWSid "+news_ID, Toast.LENGTH_SHORT).show();
                        }else bookmarkPrime.setImageResource(R.drawable.new_bookmark_empty); //bookmarkPrime.setImageResource(R.drawable.bookmark_prime);

                        //likes checking..
                        if(like_id.contains(news_ID)){
                            likePrime.setImageResource(R.drawable.like_fill);
                        }else likePrime.setImageResource(R.drawable.like_prime);

                    }

//                if (newState == RecyclerView.SCROLL_STATE_IDLE){
//                    rl1.animate().setDuration(350).scaleX(1).scaleY(1).setInterpolator(new AccelerateInterpolator()).start();
//                }else{
//                    rl1.animate().setDuration(350).scaleX(0.75f).scaleY(0.75f).setInterpolator(new AccelerateInterpolator()).start();
//                }
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                }
            });

//          //OFFLINE**** RV settings
            rvInqPrimeHorizontalOffline.setLayoutManager(linearLayoutManagerOffline);
            rvInqPrimeHorizontalOffline.setHasFixedSize(true);
            rvAdapterInqPrimeMainOffline =  new RVAdapterInqPrimeMainOffline(this, newsListOffline, this::onViewHolderItemClick, sectionName);  //pass the interface
            rvInqPrimeHorizontalOffline.setAdapter(rvAdapterInqPrimeMainOffline);
            snapHelperOffline.attachToRecyclerView(rvInqPrimeHorizontalOffline); //OFFLINE

            rvInqPrimeHorizontalOffline.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                View v = snapHelperOffline.findSnapView(linearLayoutManagerOffline);
                int pos = linearLayoutManagerOffline.getPosition(v);

                //ACCESSING RecyclerView ViewHolder
                RecyclerView.ViewHolder viewHolder = rvInqPrimeHorizontalOffline.findViewHolderForAdapterPosition(pos);
                TextView tvTag = viewHolder.itemView.findViewById(R.id.textTagSectionInqPrime);


                String photoStr = newsListOffline.get(pos).getPhoto();

                if (newState == RecyclerView.SCROLL_STATE_IDLE){
                    Log.d(TAG, "onScrollStateChanged: position is " + pos);
                    if(!photoStr.isEmpty()){
                        Picasso.get()
                                .load(photoStr)
                                .noFade()
                                .noPlaceholder()
                                .into(backgroundPrime);
                    }

                    //get ID for title,date,body... get ID and TYPE for share link
//                    String id = newsList.get(pos).getId();
                    String id = newsListOffline.get(pos).getNews_Id();
                    String type = newsListOffline.get(pos).getImage_Type();
                    String title = newsListOffline.get(pos).getTitle();
                    String date = newsListOffline.get(pos).getPublished_Date();
                    String author = newsListOffline.get(pos).getAuthor();
                    String sectionName = tvTag.getText().toString();  //just a work around waiting for the section from APi
//                    String sectionName = newsList.get(pos).getSection();

                    newsId = id;
                    newsType = type;
                    newsTitle = title;
                    newsDate = date;

                    newsPhoto = photoStr;
                    newsAuthor = author;

                    titlePrime.setText(title);
                    datePrime.setText(date);

                    //sectionName, to be replace if there is a section name in API,, //change color background
//                    switch (psp.getSingleNewsSection()){  //to be replace with sectionName
                    switch (sectionName){  //to be replace with sectionName
                        case "NEWS INFO":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                            mainTagSectionName.setText(sectionName);
//                            mainTagSectionName.setText(psp.getSingleNewsSection());
                            break;

                        case "GLOBALNATION":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                            mainTagSectionName.setText(sectionName);

                            break;
                        case "SPORT":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_sports));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.sports)); //change the background tag
                            mainTagSectionName.setText(sectionName);

                            break;
                        case "LIFESTYLE":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_lifestyle));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.lifestyle)); //change the background tag
                            mainTagSectionName.setText(sectionName);

                            break;
                        case "BUSINESS":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_business));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.business)); //change the background tag
                            mainTagSectionName.setText(sectionName);

                            break;
                        case "TECHNOLOGY":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_technology));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.technology)); //change the background tag
                            mainTagSectionName.setText(sectionName);

                            break;
                        case "OPINION":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_opinion));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.opinion)); //change the background tag
                            mainTagSectionName.setText(sectionName);
                            break;
                        default:
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                            mainTagSectionName.setText(sectionName);
                            break;
                    }


                    //bookmark checking.. newsId is originally String, convert to int
                    int news_ID = Integer.valueOf(newsId);
                    if(bookmark_id.contains(news_ID)){ // if(bookmark_id.contains(news_ID)){
                        bookmarkPrime.setImageResource(R.drawable.new_bookmark_fill); // bookmarkPrime.setImageResource(R.drawable.bookmark_fill);
//                        Toast.makeText(InqPrimeMainActivity.this, "Meron "+ bookmark_id + "...NEWSid "+news_ID, Toast.LENGTH_SHORT).show();
                    }else bookmarkPrime.setImageResource(R.drawable.new_bookmark_empty); //bookmarkPrime.setImageResource(R.drawable.bookmark_prime);

                    //likes checking..
                    if(like_id.contains(news_ID)){
                        likePrime.setImageResource(R.drawable.like_fill);
                    }else likePrime.setImageResource(R.drawable.like_prime);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });



        //setting onClickListener ..implemented
        bookmarkPrime.setOnClickListener(this);
        bookmarkPrimeLabel.setOnClickListener(this);
        likePrime.setOnClickListener(this);
        likePrimeLabel.setOnClickListener(this);
        refreshInternetConnection.setOnClickListener(this);
        shareRlPrime.setOnClickListener(this);
        hamburgerPrime.setOnClickListener(this);
        titlePrime.setOnClickListener(this);
        datePrime.setOnClickListener(this);
        bodyPrime.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        //RESOLVED: NumberFormatException
        try{
            PrimeGlobal.lastNewsIdDisplayed = Integer.valueOf(newsId); //save the last news id...for LiveData for checking likes and bookmark, after returning to this activity
        }catch(Exception e){
            Log.d(TAG, "onClick: Exception "+ e.getMessage());
        }

        switch (v.getId()){
            case R.id.prime_ivBookmark:
//                bookmarkPrimeLabel.setTextColor(colors_fill[0]);
                bookmarkPrime.setImageResource(colors_fill[1]);
                addToBookmark();
                break;
            case R.id.prime_bookmarkLabel:
//                bookmarkPrimeLabel.setTextColor(colors_fill[0]);
                bookmarkPrime.setImageResource(colors_fill[1]);
                addToBookmark();
                break;
            case R.id.prime_ivLike:
//                likePrimeLabel.setTextColor(colors_fill[0]);
                likePrime.setImageResource(colors_fill[2]);
                addtoLike();
                break;
            case R.id.prime_likeLabel:
//                likePrimeLabel.setTextColor(colors_fill[0]);
                likePrime.setImageResource(colors_fill[2]);
                addtoLike();
                break;
            case R.id.prime_rl_share:
//                sharePrimeLabel.setTextColor(colors_fill[0]);
//                sharePrime.setImageResource(colors_fill[3]);
                shareLink(newsId, newsType); //shareLink2(newsId, newsType);
                break;
            case R.id.prime_ibHamburger:
                bmScreenshot = takeScreenShot(InqPrimeMainActivity.this);
                menuBackground.setImageBitmap(bmScreenshot);
                hamburgerPrime.setVisibility(View.GONE);

                PrimeGlobal.PrimeMainMenu dialog = new PrimeGlobal.PrimeMainMenu(this, menuBackground, hamburgerPrime);
                dialog.showPrimeMainMenu();
                break;

            case R.id.prime_tvTitle:
                if(Global.isNetworkAvailable(this)){
                    viewPrimeArticle(newsId);
                    Log.d(TAG, "onClick: ID "+ newsId);
                }else {
                    viewPrimeArticleOffline(newsId);
                    Log.d(TAG, "onClick: ID "+ newsId);
                }

                break;
            case R.id.prime_tvDate:
                if(Global.isNetworkAvailable(this)){
                    viewPrimeArticle(newsId);
                    Log.d(TAG, "onClick: ID "+ newsId);
                }else {
                    viewPrimeArticleOffline(newsId);
                    Log.d(TAG, "onClick: ID "+ newsId);
                }
                break;
            case R.id.prime_tvBody:
                if(Global.isNetworkAvailable(this)){
                    viewPrimeArticle(newsId);
                    Log.d(TAG, "onClick: ID "+ newsId);
                }else {
                    viewPrimeArticleOffline(newsId);
                    Log.d(TAG, "onClick: ID "+ newsId);
                }
                break;

            case R.id.refreshNoInternetConnection:
                llNoInternetConnection.setVisibility(View.GONE);
                getDefaultNewsFeed();
                break;
        }
    }

        //sharelink
    private void shareLink(String id, String type){
        String hashed = Global.Generate32SHA512(sf.getDailyKey() +"inq" + sf.getDeviceId() + "-megashare/" + id);

        apiService.getShareLink(sf.getDeviceId(), id, type, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {

                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");

                        emailIntent.putExtra(Intent.EXTRA_TEXT, newsTitle + "\nvia Inquirer Mobile: " + link);

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(InqPrimeMainActivity.this, "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: shareLink "+ e.getMessage());
                    }
                });
    }

    //sharelink
    private void shareLink2(String id, String type){
        long unixTime = System.currentTimeMillis() / 1000L;

        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megashare/" + id);

        apiService.getShareLink2(String.valueOf(unixTime), sf.getDeviceId(), id, type, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {

                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");

                        emailIntent.putExtra(Intent.EXTRA_TEXT, newsTitle + "\nvia Inquirer Mobile: " + link);

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(InqPrimeMainActivity.this, "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: shareLink "+ e.getMessage());
                    }
                });
    }

    //View Article
    private void viewPrimeArticle(String id) {

        showProgressLoading();

        String hashed = Global.Generate32SHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
        primeApiService.getPrimeArticle(sf.getDeviceId(), id, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Article>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Article article) {

                        dismissProgressLoading();

                        Intent i = new Intent(InqPrimeMainActivity.this, PrimeArticle.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", id);
                        bundle.putString("photo", article.getCreative());
                        bundle.putString("title", article.getTitle());
                        bundle.putString("author", article.getByline());
                        bundle.putString("date", article.getPubdate());
//                        bundle.putString("content", article.getContent().get(0));
                        bundle.putString("type", article.getType());
                        bundle.putString("custom", article.getCustom());
                        bundle.putString("section_name", mainTagSectionName.getText().toString());
                        Log.d(TAG, "onSuccess: ID "+id);
                        //NO content2 and image1

                        i.putExtras(bundle);
                        startActivity(i);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: getPrimeArticle " + e.getMessage());

                    }
                });
    }


    //View OFFLINE Article
    private void viewPrimeArticleOffline(String id) {
        newsArticle = helper.getArticleContent(Integer.valueOf(id));  //Exclusive arrayList for single newsId
        Log.d(TAG, "viewPrimeArticleOffline: Size "+ newsArticle.size());
        Bitmap bitmap = null;
        Uri offline_photo = null;
        if(newsArticle.size()==1){
            if(newsArticle.get(0).getOffline_photo()!=null){
                bitmap = convertByteArrayToBitmap(newsArticle.get(0).getOffline_photo());
                String storagePath = saveBitmapToStorage(bitmap); //Save Bitmap
                offline_photo = Uri.fromFile(new File(storagePath));  //storage filename to Uri
            }

            Intent i = new Intent(InqPrimeMainActivity.this, PrimeArticle.class);
            Bundle bundle = new Bundle();
            bundle.putString("id", id);
            bundle.putString("photo", offline_photo.toString());
            bundle.putString("title", newsArticle.get(0).getTitle());
            bundle.putString("author", newsArticle.get(0).getAuthor());
            bundle.putString("date", newsArticle.get(0).getPublished_Date());
            bundle.putString("content", newsArticle.get(0).getContent());
            bundle.putString("type", newsArticle.get(0).getImage_Type());
            bundle.putString("custom", newsArticle.get(0).getDescription());
            bundle.putString("section_name", mainTagSectionName.getText().toString());

            i.putExtras(bundle);
            startActivity(i);
        }else{
            Toast.makeText(this, "Unexpected error occur", Toast.LENGTH_SHORT).show();
        }
    }

    //result:
    private String saveBitmapToStorage(Bitmap bitmap) {
        deleteOldFile(); //DELETE PREVIOUS IMAGE
        String path = "";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);  //go to the fileDirectory
        try {
            File imageFile = File.createTempFile("news_photo", ".jpg", storageDir);   //creating the image from fileDirectory //
            FileOutputStream fos = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);  //compress bitmap
            fos.flush();
            fos.close();
            path = imageFile.getAbsolutePath();
            return path;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    //result: Bitmap, param: byteArray
    public static Bitmap convertByteArrayToBitmap(byte[] src){
        return BitmapFactory.decodeByteArray(src, 0, src.length);
    }

    private void deleteOldFile() {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);  //go to the fileDirectory of the Apps
        //if Directory exist  ..Delete all files
        if (storageDir.exists() && storageDir.isDirectory()) {
            String[] children = storageDir.list();
            for (int i = 0; i < children.length; i++) {
                new File(storageDir, children[i]).delete();
            }
        }
    }



    private void dismissProgressLoading() {
        progressLoaderDialog.dismissDialog();
    }

    private void showProgressLoading() {
        progressLoaderDialog.showDialog();
    }


    private void showDialog(){
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//			dialog.getWindow().setBackgroundDrawableResource(R.color.trans_black_dialog);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loader_dialog);
        ImageView ivLoader = dialog.findViewById(R.id.gifLoader);
        Glide.with(this).asGif().load(R.drawable.inqloader2).into(ivLoader);
        dialog.show();
    }

    //
    private void getBookmarkList() {

        ViewModelPrimeBookmark viewModelPrimeBookmark; //Creating a view_model object
        viewModelPrimeBookmark = ViewModelProviders.of(this).get(ViewModelPrimeBookmark.class); //initialize view_model PROVIDER

        viewModelPrimeBookmark.getPrimeBookmarkList().observe(this, data->{ //Accessing view_model METHOD
            bookmark_id.clear(); //clear first
            Log.d(TAG, "getBookmarkList: called size DATA "+ data.size());
                for(int x = 0; x <data.size(); x++) {
                    bookmark_id.add(data.get(x).getNewsId()); //add to HashSet
                    Log.d(TAG, "getBookmarkList: called bookmark_id " + bookmark_id);
                }

            //checking of bookmark list inside ViewModel callbacks
            int news_ID = PrimeGlobal.lastNewsIdDisplayed; //realtime marking of bookmarkED if it is so.
            if(bookmark_id.contains(news_ID)){
                Log.d(TAG, "getBookmarkList: called news_ID "+ news_ID);
                Log.d(TAG, "getBookmarkList: called bookmark_id contains "+ bookmark_id);
                bookmarkPrime.setImageResource(R.drawable.new_bookmark_fill); // bookmarkPrime.setImageResource(R.drawable.bookmark_fill);
            }else {
                bookmarkPrime.setImageResource(R.drawable.new_bookmark_empty); //bookmarkPrime.setImageResource(R.drawable.bookmark_prime);
                Log.d(TAG, "getBookmarkList: called bookmark_id NOT contains " + bookmark_id);
                Log.d(TAG, "getBookmarkList: called NOT news_ID " + news_ID);
            }

        });
    }

    //using LiveData and MVVM
    private void getLikeList() {
        ViewModelPrimeLike likeViewModel;
        likeViewModel = ViewModelProviders.of(this).get(ViewModelPrimeLike.class);

        likeViewModel.getPrimeLikeList().observe(this, data -> {
            Log.d(TAG, "getLikeList: called size DATA COUNT "+ data.size());
            like_id.clear(); //clear first
            for(int x = 0; x <data.size(); x++){
                like_id.add(data.get(x).getNewsId()); //add to HashSet
            }

            Log.d(TAG, "getLikeList: called size LIKES COUNT"+ like_id.size());
            //checking of like list
            int news_ID = PrimeGlobal.lastNewsIdDisplayed;
            if(like_id.contains(news_ID)){
                likePrime.setImageResource(R.drawable.like_fill);
            }else {
                likePrime.setImageResource(R.drawable.like_fill);
            }

        });
    }



    //get account info test
    private void getAccountInfo(){

        ViewModelPrimeAccountInformation viewModelPrimeAccountInformation;
        viewModelPrimeAccountInformation = ViewModelProviders.of(this).get(ViewModelPrimeAccountInformation.class);
        viewModelPrimeAccountInformation.getPrimeAccountInformationList().observe( this, data -> {
            if(data.size()>0){
                PrimeGlobal.accountId = data.get(0).getUserId()==0 ? 1 : data.get(0).getUserId();
                PrimeGlobal.accountEmail = data.get(0).getAccountEmail()=="" ? "" : data.get(0).getAccountEmail();
                PrimeGlobal.accountUserName = data.get(0).getUserName()=="" ? "" : data.get(0).getUserName();
                PrimeGlobal.accountName = data.get(0).getAccountName()=="" ? "" : data.get(0).getAccountName();
                PrimeGlobal.accountMobileNo = data.get(0).getMobileNo()=="" ? "" : data.get(0).getMobileNo();
                PrimeGlobal.accountGender = data.get(0).getGender()=="" ? "" : data.get(0).getGender();
                PrimeGlobal.accountAddress = data.get(0).getAddress()=="" ? "" : data.get(0).getAddress();
                PrimeGlobal.accountBirthday = data.get(0).getBirthday()=="" ? "" : data.get(0).getBirthday();
//                PrimeGlobal.accountAvatar = data.get(0).getAvatar()=="" ? "" : data.get(0).getAvatar();
                PrimeGlobal.accountCardNo = data.get(0).getCardNo()=="" ? "" : data.get(0).getCardNo();
                PrimeGlobal.accountPlan = data.get(0).getPlan()=="" ? "" : data.get(0).getPlan();
                PrimeGlobal.accountAvatar = data.get(0).getAvatar()=="" ? String.valueOf(getResources().getDrawable(R.drawable.person_avatar)) : data.get(0).getAvatar();
//                PrimeGlobal.accountCardNo = data.get(0).getCardNo()=="" ? "" : data.get(0).getCardNo();
//                PrimeGlobal.accountCardNo = data.get(0).getCardNo()=="" ? "" : data.get(0).getCardNo().substring(data.get(0).getCardNo().length()-4) ;
            }

        });

    }


    //DropDown Spinner... PROGRAMMATICALLY
    private void selectOtherCategories() {
        ArrayAdapter<String> myAdapter = new ArrayAdapter(InqPrimeMainActivity.this, R.layout.prime_dropdown_other_category, getResources().getStringArray(R.array.sections_other_category)){

            //getDropDownView SpinnerView
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTextColor(Color.WHITE);  //setting the textColor here
                tv.setTextSize(13);
                tv.setCompoundDrawables(null,null,null,null);  //removing the drawableRight...

                if (position == 6) {
                    tv.setPadding(50,40,0,40);
                }
                else {
                    tv.setPadding(50,40,0,0);

                }
                return view;
            }
        };

        prime_spinLatest.setAdapter(myAdapter);
        prime_spinLatest.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spinLatest = adapterView.getItemAtPosition(i).toString();
                Log.d(TAG, "onItemSelected: Spin "+spinLatest);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
    }
    ////////////////////


    private void addtoLike() {
        int news_id = Integer.parseInt(newsId); //coverted to int to make it primaryKey
            PrimeLikeModel likeModel = new PrimeLikeModel(news_id);
            helper.insertPrimeLike(likeModel);
    }


    private void addToBookmark() {
        int news_id = Integer.parseInt(newsId); //coverted to int to make it primaryKey
        //insert/save to database
        PrimeBookmarkModel bookmarkModel = new PrimeBookmarkModel(news_id, newsPhoto, newsTitle, newsDate, newsAuthor);
        helper.insertPrimeBookmark(bookmarkModel);

        SpannableStringBuilder text = PrimeGlobal.toastText("This article is added to your bookmark.");
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "addToBookmark: called");
    }

    //creating snapSHot bitmap and Blurring bitmap
    public Bitmap takeScreenShot(Activity activity)
    {
        Bitmap bitmapImage = null;
        Bitmap result = null;
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();  //screenShot
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        int width = activity.getWindowManager().getDefaultDisplay().getWidth();
        int height = activity.getWindowManager().getDefaultDisplay().getHeight();

        try {
            //screenShot to bitmap
            bitmapImage = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height  - statusBarHeight);
            view.destroyDrawingCache();
            Log.d(TAG, "takeScreenShot:width "+ width);
            Log.d(TAG, "takeScreenShot:height "+ height);

            //blurring bitmap
            RenderScript  rsScript = RenderScript.create(getApplicationContext());
            Allocation alloc = Allocation.createFromBitmap(rsScript, bitmapImage);
            ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rsScript,   Element.U8_4(rsScript));
            blur.setRadius(20);
            blur.setInput(alloc);

            result = Bitmap.createBitmap(bitmapImage.getWidth(), bitmapImage.getHeight(), Bitmap.Config.ARGB_8888);
            Allocation outAlloc = Allocation.createFromBitmap(rsScript, result);

            blur.forEach(outAlloc);
            outAlloc.copyTo(result);
            rsScript.destroy();

        }catch (Exception e){
            Log.d(TAG, "takeScreenShot: "+e.getMessage());
        }

        return result;
    }


    private void displayOfflineNews() {
        rvInqPrimeHorizontal.setVisibility(View.GONE);
        rvInqPrimeHorizontalOffline.setVisibility(View.VISIBLE);
        Log.d(TAG, "displayOfflineNews: SIze " + newsListOffline.size());

        ViewModelPrimeNewsOffline viewModelPrimeNewsOffline;
        viewModelPrimeNewsOffline = ViewModelProviders.of(this).get(ViewModelPrimeNewsOffline.class);
        viewModelPrimeNewsOffline.getAllPrimeNews().observe(this, data->{
             newsListOffline.clear();
             newsListOffline.addAll(data);


            if (newsListOffline.size()>0){


                //Catching OutOfBounds..
                try{

                    clTagLabel.setVisibility(View.VISIBLE);
                    clBody.setVisibility(View.VISIBLE);
                    rlRecyclerView.setVisibility(View.VISIBLE);
                    rlSpinner.setVisibility(View.VISIBLE);
                    hamburgerPrime.setVisibility(View.VISIBLE);

//setting initial main views
                    int defaultPosition = PrimeGlobal.lastItemPosition;

                    String photoStr = newsListOffline.get(defaultPosition).getPhoto();
                    if(!photoStr.isEmpty()){
                        Picasso.get()
                                .load(photoStr)
                                .noFade()
                                .noPlaceholder()
                                .into(backgroundPrime);
                    }

//initialize ID id for title,date,body...
                    String id = newsListOffline.get(defaultPosition).getNews_Id();
                    String type = newsListOffline.get(defaultPosition).getImage_Type();
                    String title = newsListOffline.get(defaultPosition).getTitle();
                    String date = newsListOffline.get(defaultPosition).getPublished_Date();
                    newsId = id;
                    newsType = type;
                    newsTitle = title;
                    newsDate = date;
                    newsPhoto = photoStr;

                    titlePrime.setText(title);
                    datePrime.setText(date);
                    bodyPrime.setText(testString); //

                    //show spinner when specific section is selected
                    selectOtherCategories();  //show spinner
                    prime_spinLatest.setVisibility(View.VISIBLE);  //show spinner

                    String sectionName = psp.getSingleNewsSection();
                    mainTagSectionName.setText(psp.getSingleNewsSection()); //change the section tag name
                    rvAdapterInqPrimeMainOffline = new RVAdapterInqPrimeMainOffline(this, newsListOffline, this::onViewHolderItemClick, sectionName);

                    switch (psp.getSingleNewsSection()) {
                        case "NEWS INFO":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                            break;
                        case "GLOBALNATION":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                            break;
                        case "SPORT":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_sports));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.sports)); //change the background tag
                            break;
                        case "LIFESTYLE":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_lifestyle));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.lifestyle)); //change the background tag
                            break;
                        case "BUSINESS":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_business));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.business)); //change the background tag
                            break;
                        case "TECHNOLOGY":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_technology));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.technology)); //change the background tag
                            break;
                        case "OPINION":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_opinion));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.opinion)); //change the background tag
                            break;
                        default:
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                            mainTagSectionName.setText(sectionName);
                            break;

                    }

                    rvInqPrimeHorizontalOffline.setAdapter(rvAdapterInqPrimeMainOffline);
                    rvInqPrimeHorizontalOffline.getLayoutManager().scrollToPosition(defaultPosition);  //setting the DEFAULT ITEM POSITION
                    rvAdapterInqPrimeMainOffline.notifyDataSetChanged();

                }catch (Exception e){
                    Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "displayOfflineNews: Exception "+e.getMessage());
                }



            } else{
                if(dialog!=null){
                    dialog.dismiss();
                }
                llNoInternetConnection.setVisibility(View.VISIBLE);
                Log.d(TAG, "getNewsBySectionOffline: NO DATA ");
            }

        });
    }



    private void getNewsBySection(int sectionId, String countSectionSelected  ) {
        Log.d(TAG, "getNewsBySection: called " + countSectionSelected);
        rvInqPrimeHorizontal.setVisibility(View.VISIBLE);
        rvInqPrimeHorizontalOffline.setVisibility(View.GONE);
        //dont show progressDialog if connection is changed
        if(isConnectionChange){
        }else showDialog();


        viewModelPrimeNewsBySection = ViewModelProviders.of(this).get(ViewModelPrimeNewsBySection.class);
        viewModelPrimeNewsBySection.getPrimeNewsBySection(sectionId).observe(this, (List<PrimeNewsBySectionModel> data) ->{
            Log.d(TAG, "getNewsBySection: viewModelPrimeNewsBySection called data.size() "+ data.size());

            if(data.size()>0){
                newsList.clear();
                newsList.addAll(data);

                //DISMISS dialog
            if(dialog!=null){
                dialog.dismiss();
            }
            //display the view
                clTagLabel.setVisibility(View.VISIBLE);
                clBody.setVisibility(View.VISIBLE);
                rlRecyclerView.setVisibility(View.VISIBLE);
                rlSpinner.setVisibility(View.VISIBLE);
                hamburgerPrime.setVisibility(View.VISIBLE);
                //showRV
//setting initial main views
                int defaultPosition = PrimeGlobal.lastItemPosition;

                String photoStr = newsList.get(defaultPosition).getImage();
                if(!photoStr.isEmpty()){
                    Picasso.get()
                            .load(photoStr)
                            .noFade()
                            .noPlaceholder()
                            .into(backgroundPrime);
                }
//initialize ID id for title,date,body...
//            String id = newsList.get(defaultPosition).getId();
                String id = newsList.get(defaultPosition).getNewsId();
                String type = newsList.get(defaultPosition).getType();
                String title = newsList.get(defaultPosition).getTitle();
                String date = newsList.get(defaultPosition).getPubdate();
                newsId = id;
                newsType = type;
                newsTitle = title;
                newsDate = date;
                newsPhoto = photoStr;

                titlePrime.setText(title);
                datePrime.setText(date);
                bodyPrime.setText(testString);

//            viewColorOverlay.setBackground(getResources().getDrawable(colors_overlay[defaultPosition]));
//            mainTagSectionName.setBackgroundColor(getResources().getColor(colors_main_tag[defaultPosition])); //change the background tag

                if(countSectionSelected.equals("single")){
                    //show spinner when specific section is selected
                    selectOtherCategories();  //show spinner
                    prime_spinLatest.setVisibility(View.VISIBLE);  //show spinner

                    String sectionName = psp.getSingleNewsSection();
                    mainTagSectionName.setText(psp.getSingleNewsSection()); //change the section tag name
                    rvAdapterInqPrimeMain = new RVAdapterInqPrimeMain(this, newsList, this::onViewHolderItemClick, sectionName);

                    switch (psp.getSingleNewsSection()){
                        case "NEWS INFO":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.news)); //change the background tag
                            break;
                        case "GLOBALNATION":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                            break;
                        case "SPORT":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_sports));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.sports)); //change the background tag
                            break;
                        case "LIFESTYLE":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_lifestyle));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.lifestyle)); //change the background tag
                            break;
                        case "BUSINESS":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_business));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.business)); //change the background tag
                            break;
                        case "TECHNOLOGY":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_technology));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.technology)); //change the background tag
                            break;
                        case "OPINION":
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_opinion));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.opinion)); //change the background tag
                            break;
                        default:
                            viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_newsinfo));
                            mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.globalnation)); //change the background tag
                            mainTagSectionName.setText(sectionName);
                            break;
                    }

                }else if(countSectionSelected.equals("multiple")) {
                    //show spinner when specific section is selected
                    selectOtherCategories();  //show spinner
                    prime_spinLatest.setVisibility(View.VISIBLE);  //show spinner

                    String sectionName = psp.getSingleNewsSection();
                    rvAdapterInqPrimeMain = new RVAdapterInqPrimeMain(this, newsList, this::onViewHolderItemClick, sectionName);
//                mainTagSectionName.setText(psp.getSingleNewsSection()); //change the section tag name
//                mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.news)); //change the background tag

                }else {  //DEFAULT
//                String sectionName2 = newsList.get(PrimeGlobal.lastItemPosition).getSection();
                    String sectionName = psp.getDefaultNewsSection();  //Default sectionName
                    rvAdapterInqPrimeMain = new RVAdapterInqPrimeMain(this, newsList, this::onViewHolderItemClick, sectionName);
                    mainTagSectionName.setText(psp.getDefaultNewsSection()); //change the section tag name
                    mainTagSectionName.setBackgroundColor(getResources().getColor(R.color.sports)); //change the background tag  //getColor(R.color.news))
                    viewColorOverlay.setBackground(getResources().getDrawable(R.drawable.gradient_sports));  //getDrawable(R.drawable.gradient_newsinfo))
                }
//
                Log.d(TAG, "getNewsBySection: "+ newsList);

                //DO NOT INVOKED RVAdapterInqPrimeMain..NO API RESPONSE data.size()<0
//          rvAdapterInqPrimeMain = new RVAdapterInqPrimeMain(this, newsList, this::onViewHolderItemClick);
                rvInqPrimeHorizontal.setAdapter(rvAdapterInqPrimeMain);
                rvInqPrimeHorizontal.getLayoutManager().scrollToPosition(defaultPosition);  //setting the DEFAULT ITEM POSITION
                rvAdapterInqPrimeMain.notifyDataSetChanged();

            }else{
                //DISMISS dialog
                Log.d(TAG, "getNewsBySection: NO DATA ");
                if (dialog!=null){dialog.dismiss();}
            }
        });
    }

//    private void getAllPrimeNewsBySection(int sectionId){
//        viewModelPrimeNewsBySection = ViewModelProviders.of(this).get(ViewModelPrimeNewsBySection.class);
//        viewModelPrimeNewsBySection.getAllPrimeNewsBySection(sectionId);
//    }



    //NETWORK INTERNET CHANGE MONITORING
    private void onConnectionChange(String msg) {
        isConnectionChange = true;
//        getNewsBySection(Integer.valueOf(primeSectionsModel.get(0).getPrimeId()),"single" );
        displayNews(); //refresh API call again
        if(msg.equals("network_access")){
            Log.d(TAG, "onConnectionChange: ONLINE");
            }else if(msg.equals("no_network_access")){
            Log.d(TAG, "onConnectionChange: OFFLINE");
            }
    }
    //NOTE: PRIME_SECTION_OPINION_ID is 2 Temporarily
    private void initSections() {
        PrimeSectionsModel p1 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_NEWS_INFO_ID,"NEWS INFO", false );
        PrimeSectionsModel p2 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_GLOBAL_NATION_ID,"GLOBALNATION", false );
        PrimeSectionsModel p3 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_SPORTS_ID,"SPORT", false );
        PrimeSectionsModel p4 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_LIFESTYLE_ID,"LIFESTYLE", false );
        PrimeSectionsModel p5 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_BUSINESS_ID,"BUSINESS", false );
        PrimeSectionsModel p6 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_TECHNOLOGY_ID,"TECHNOLOGY", false );
        PrimeSectionsModel p7 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_OPINION_ID,"OPINION", false );
        PrimeSectionsModel p8 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_BREAKING_NEWS_ID,"BREAKING NEWS", false );
        PrimeSectionsModel p9 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_ENTERTAINMENT_ID,"ENTERTAINMENT", false );
        PrimeSectionsModel p10 = new PrimeSectionsModel(PrimeGlobal.PRIME_SECTION_GLOBAL_NEWS_ID,"GLOBAL NEWS", false );

        helper.insertPrimeSections(p1);
        helper.insertPrimeSections(p2);
        helper.insertPrimeSections(p3);
        helper.insertPrimeSections(p4);
        helper.insertPrimeSections(p5);
        helper.insertPrimeSections(p6);
        helper.insertPrimeSections(p7);
        helper.insertPrimeSections(p8);
        helper.insertPrimeSections(p9);
        helper.insertPrimeSections(p10);

    }

    private void initTopics() {
        PrimeTopicsModel p1 = new PrimeTopicsModel(PrimeGlobal.PRIME_TOPIC_HEALTH_ID,"HEALTH AND WELLNESS", false );
        PrimeTopicsModel p2 = new PrimeTopicsModel(PrimeGlobal.PRIME_TOPIC_POLITICS_ID,"POLITICS", false );
        PrimeTopicsModel p3 = new PrimeTopicsModel(PrimeGlobal.PRIME_TOPIC_BEAUTY_ID,"BEAUTY", false );
        PrimeTopicsModel p4 = new PrimeTopicsModel(PrimeGlobal.PRIME_TOPIC_ARTS_ID,"ARTS", false );
        PrimeTopicsModel p5 = new PrimeTopicsModel(PrimeGlobal.PRIME_TOPIC_MUSICID,"MUSIC", false );
        PrimeTopicsModel p6 = new PrimeTopicsModel(PrimeGlobal.PRIME_TOPIC_TRAVEL_ID,"TRAVEL", false );

        helper.insertPrimeTopics(p1);
        helper.insertPrimeTopics(p2);
        helper.insertPrimeTopics(p3);
        helper.insertPrimeTopics(p4);
        helper.insertPrimeTopics(p5);
        helper.insertPrimeTopics(p6);
    }

    //saving account info test
    private void saveAccountInfo(){

        //for saving
        Integer userId = 1;
        String email = "megamobile@yahoo.com";
        String password = "********";
        String userName = "megamobile";
        String name = "David Haniel";
        String mobileNo = "+639454539768";
        String gender = "Male";
        String address = "3rd Floor Media Resource Plaza, Mola Street Brgy. La Paz, Makati City";
        String birthday = "11 - 20 - 2005";
        String avatar = String.valueOf(getResources().getDrawable(R.drawable.person_avatar));
        String cardNo = "123412341324";
        String plan = "Basic";

        PrimeAccountInformationModel primeAccountInfoModel = new PrimeAccountInformationModel(userId,email,password,userName,name,mobileNo,gender,address,birthday,avatar,cardNo,plan);
        helper.insertPrimeAccountInformation(primeAccountInfoModel);

    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "LIFE onResume: called PrimeGlobalCountSection " + PrimeGlobal.countSection);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, " LIFE onPause: called PrimeGlobalCountSection " + PrimeGlobal.countSection);
    }

    @Override
    protected void onStart() {
        EventBus.getDefault().register(InqPrimeMainActivity.this);

        super.onStart();
        Log.d(TAG, "LIFE onStart: called");
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(InqPrimeMainActivity.this);
        super.onStop();
        Log.d(TAG, "LIFE onStart: called");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //onViewHolderItemClick.. called every time viewHolder item is clicked
    @Override
    public void onViewHolderItemClick(View view, int position) {
        Log.d(TAG, "displayOfflineNews: onViewHolderItemClick SIze newsListOffline " + newsListOffline.size());
        Log.d(TAG, "displayOfflineNews: onViewHolderItemClick SIze newsList " + newsList.size());

        Log.d(TAG, "onViewHolderItemClick: position "+position);
        //check id defend on Online/Offline
        String id = Global.isNetworkAvailable(this) ? newsList.get(position).getNewsId() : newsListOffline.get(position).getNews_Id();
        PrimeGlobal.lastItemPosition = position; //PrimeGlobal.lastItemPosition is only set when item is clicked
        PrimeGlobal.lastNewsIdDisplayed = Integer.valueOf(id);

        if(Global.isNetworkAvailable(this)){
//            Log.d(TAG, "onViewHolderItemClick: ONLINE");
            Toast.makeText(this, "READING ONLINE", Toast.LENGTH_SHORT).show();
            viewPrimeArticle(id);
        }else {
//            Log.d(TAG, "onViewHolderItemClick: OFFLINE");
            Toast.makeText(this, "READING OFFLINE", Toast.LENGTH_SHORT).show();
            viewPrimeArticleOffline(id);
        }
    }
//
//    public void insertNotif(){
//        NotificationModel nmodel = new NotificationModel("20934324","https://combo.staticflickr.com/ap/build/images/refencing-announcement/bird2.jpg", "https://www.flickr.com/lookingahead", "God is good ",  "God's Love and Faithfulness. Jesus the Son of God. The God's favor. ", "September 09, 2019 02:53pm", 0);
//        helper.insertNotification(nmodel);
//    }
}





































































//
//    //WORKING TESTER
//    private void getNewsBySection(int section) {
////        String deviceKey = "2841957170674665"; //tester
////        String deviceId = "f8fcf876237111fb";  //tester
//        mutableNewsData = new MutableLiveData<>();  //Dont forget to initialize
//
//
//        String hashed = Global.Generate32SHA512("2841957170674665" + "inq" + "f8fcf876237111fb" + "-megasection/" + section);
////        hashed = Global.Generate32SHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megasection/" + 2);
//
//        primeApiService.getPrimeNewsBySection("f8fcf876237111fb", section, hashed)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<ArrayList<PrimeNewsBySectionModel>>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        Log.d(TAG, "onSubscribe: getPrimeNewsBySection called");
//                        compositeDisposable.add(d);
//                    }
//
//                    @Override
//                    public void onSuccess(ArrayList<PrimeNewsBySectionModel> news) {
//
//                        mutableNewsData.setValue(news);
//                        Log.d(TAG, "onSuccess:getPrimeNewsBySection called");
//
//                        Log.d(TAG, "onSuccess:getPrimeNewsBySection "+ news.get(0).getNewsId());
//
////                        helper.deleteNews();
////                        helper.insertNews(news);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e("NEWS_API_ERR", e.toString());
//                    }
//                });
//
//        ///
//
//    }









//    private void initRecyclerView() {
////        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//
////        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        rvInqPrimeHorizontal.setLayoutManager(linearLayoutManager);
//
////        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,linearLayoutManager.getOrientation()); //item decorations
////        rvInqPrimeHorizontal.addItemDecoration(dividerItemDecoration);
//
//        rvInqPrimeHorizontal.setHasFixedSize(true);
//        rvAdapterInqPrimeHorizontal =  new RVAdapterInqPrimeHorizontal(this,newsList);
//        rvInqPrimeHorizontal.setAdapter(rvAdapterInqPrimeHorizontal);
//        Log.d(TAG, "initRecyclerView: called");
//
//    }




//black and white
//        ImageView imageview = (ImageView) findViewById(R.id.imageView1);
//        ColorMatrix matrix = new ColorMatrix();
//        matrix.setSaturation(0);
//
//        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
//        imageview.setColorFilter(filter);






//    //working Blurring the bitmap image
//    @SuppressLint("NewApi")
//    Bitmap BlurImage (Bitmap input)
//    {
//        try
//        {
//            RenderScript  rsScript = RenderScript.create(getApplicationContext());
//            Allocation alloc = Allocation.createFromBitmap(rsScript, input);
////            ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rsScript,   Element.U8_4(rsScript));
//            ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rsScript,   Element.U8_4(rsScript));
//            blur.setRadius(21);
//            blur.setInput(alloc);
//
//            Bitmap result = Bitmap.createBitmap(input.getWidth(), input.getHeight(), Bitmap.Config.ARGB_8888);
//            Allocation outAlloc = Allocation.createFromBitmap(rsScript, result);
//
//            blur.forEach(outAlloc);
//            outAlloc.copyTo(result);
//
//            rsScript.destroy();
//            return result;
//        }
//        catch (Exception e) {
//            // TODO: handle exception
//            return input;
//        }
//
//    }






//
//        ViewModelPrimeBookmark viewModelPrimeBookmark;
//        viewModelPrimeBookmark = ViewModelProviders.of(this).get(ViewModelPrimeBookmark.class);
//
//        viewModelPrimeBookmark.getPrimeBookmarkList().observe(this, data->{
//            bookmark_id.clear();
//            Log.d(TAG, "getBookmarkList: called size is "+ data.size());
//            if(data.size()>=0){
//                for(int x = 0; x <data.size(); x++){
//                    bookmark_id.add(data.get(x).getNewsId()); //add to HashSet
//                    Log.d(TAG, "getBookmarkList: called size is "+ bookmark_id.toString());
//                }
//            }
//        });
//
//        int news_ID = PrimeGlobal.lastNewsIdDisplayed;
//        if(bookmark_id.contains(news_ID)){
//            bookmarkPrime.setImageResource(R.drawable.bookmark_fill);
//        }else bookmarkPrime.setImageResource(R.drawable.bookmark_prime);



//                    viewColorOverlay.setBackground(getResources().getDrawable(colors_overlay[pos]));
//                    mainTagSectionName.setBackgroundColor(getResources().getColor(colors_main_tag[pos]));
//                    mainTagSectionName.setText(sections_name_main_tag[pos]);

//
//                    viewColorOverlay.setBackground(getResources().getDrawable(colors_overlay[pos]));
//                    mainTagSectionName.setBackgroundColor(getResources().getColor(colors_main_tag[pos]));
//                    mainTagSectionName.setText(sections_name_main_tag[pos]);






//        if(PrimeGlobal.isBookmarkDeleted){
//            PrimeGlobal.isBookmarkDeleted = false;
//            updateBookmarkListAfterDeletion();
//        }else{
//            //bookmark checking.. newsId is originally String, convert to int
//            int news_ID = PrimeGlobal.lastNewsIdDisplayed;
//
//            if(bookmark_id.contains(news_ID)){
//                bookmarkPrime.setImageResource(R.drawable.bookmark_fill);
//            }else bookmarkPrime.setImageResource(R.drawable.bookmark_prime);
//
//
//            //likes checking..
//            if(like_id.contains(news_ID)){
//                likePrime.setImageResource(R.drawable.like_fill);
//            }else likePrime.setImageResource(R.drawable.like_prime);
//        }




//        psp.setAccountEmail(email);
//        psp.setUserName(userName);
//        psp.setAccountName(name);
//        psp.setMobileNo(mobileNo);
//        psp.setUserGender(gender);
//        psp.setUserAddress(address);
//        psp.setUserBirthday(birthday);
//        psp.setUserAvatar(avatar=="" ? psp.getUserAvatar() : avatar );





//
//
//
//private void getNewsBySection(int section, int pos) {
//
//        showProgressLoading();
////
////        newsViewModel = ViewModelProviders.of(this, new ViewModelFactoryNews(getApplication(), compositeDisposable, sf)).get(NewsViewModel.class);
////        newsViewModel.getNews(section, pos).observe(this, (List<NewsModel> news) -> {
//
//
//        newsList.clear();
//        newsList.addAll(news);
//
//        //setting initial main views
//
//        int defaultPosition = PrimeGlobal.lastItemPosition;
//
//        String photoStr = newsList.get(defaultPosition).getImage();
//        if(!photoStr.isEmpty()){
//        Picasso.with(getApplicationContext())
//        .load(photoStr)
//        .noFade()
//        .noPlaceholder()
//        .into(backgroundPrime);
//        }
//
//        //initialize ID id for title,date,body...
//        String id = newsList.get(defaultPosition).getId();
//        String type = newsList.get(defaultPosition).getType();
//        String title = newsList.get(defaultPosition).getTitle();
//        String date = newsList.get(defaultPosition).getPubdate();
//        newsId = id;
//        newsType = type;
//        newsTitle = title;
//        newsDate = date;
//        newsPhoto = photoStr;
//
//        titlePrime.setText(title);
//        datePrime.setText(date);
//        bodyPrime.setText(testString);
//        viewColorOverlay.setBackground(getResources().getDrawable(colors_overlay[defaultPosition]));
//        mainTagSectionName.setBackgroundColor(getResources().getColor(colors_main_tag[defaultPosition]));
//        mainTagSectionName.setText(sections_name_main_tag[defaultPosition]);
////            //
//        Log.d(TAG, "getNewsBySection: "+ newsList);
//
//        //display the view
//        if (news.size()>0){
//        dismissProgressLoading();
//        clTagLabel.setVisibility(View.VISIBLE);
//        clBody.setVisibility(View.VISIBLE);
//        rlRecyclerView.setVisibility(View.VISIBLE);
//        rlSpinner.setVisibility(View.VISIBLE);
//
////                rlBookmark.setVisibility(View.VISIBLE);
////                rlShare.setVisibility(View.VISIBLE);
////                rlLike.setVisibility(View.VISIBLE);
////                parentViewCl.setVisibility(View.VISIBLE);
//
//        }
//
//        rvAdapterInqPrimeMain = new RVAdapterInqPrimeMain(this, newsList, this::onViewHolderItemClick);
//        rvInqPrimeHorizontal.setAdapter(rvAdapterInqPrimeMain);
//        rvInqPrimeHorizontal.getLayoutManager().scrollToPosition(defaultPosition);  //setting the DEFAULT ITEM POSITION
//        rvAdapterInqPrimeMain.notifyDataSetChanged();
//
//        });
//        }




//check if sections and topics has already initialize,
//        if(!psp.hasAlreadyInitialize()){  //to be deleted
//            psp.setAlreadyInitialize(true);
//            initSections();
//            initTopics();
//            saveAccountInfo(); //done testing
//        }



//        String[] section_name = {
//                "NEWS INFO",
//                "SPORTS",
//                "ENTERTAINMENT",
//                "TECHNOLOGY",
//                "GLOBAL NATION",
//                "BUSINESS",
//                "LIFESTYLE",
//                "GLOBAL NEWS",
//                "OPINION",
//
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO",
//                "NEWS INFO"
//        };

//from initialize to global variable
//        colors_overlay = color_gradient;
//        colors_main_tag = color_main_tag;
//        sections_name_main_tag = section_name;





//        //tag colors carouselView
//        Integer[] color_gradient = {
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_sports,
//                R.drawable.gradient_entertainment,
//                R.drawable.gradient_technology,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_business,
//                R.drawable.gradient_lifestyle,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_opinion,
//
//
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//                R.drawable.gradient_newsinfo,
//
//        };

//        Integer[] color_main_tag = {
//                R.color.globalnation,
//                R.color.sports,
//                R.color.entertainment,
//                R.color.technology,
//                R.color.globalnation,
//                R.color.business,
//                R.color.lifestyle,
//                R.color.globalnation,
//                R.color.opinion,
//
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation,
//                R.color.globalnation
//        };
