package com.knx.inquirer.inqprime.repositories;
import android.content.Context;

import androidx.lifecycle.LiveData;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeAccountInformationModel;
import com.knx.inquirer.inqprime.model.PrimeSectionsModel;

import java.util.List;

public class RepositoryPrimeSections {

    private DatabaseHelper helper;
    private Context context;

    public RepositoryPrimeSections() {
        helper = new DatabaseHelper(context);
    }

    public LiveData<List<PrimeSectionsModel>> getPrimeSectionsStatus(){
        return helper.getPrimeSectionsStatus();

    }

//    public LiveData<List<PrimeSectionsModel>> getPrimeSections(){return helper.getPrimeSections();} //for initialization of sections and topics, insert if no record at all

}
