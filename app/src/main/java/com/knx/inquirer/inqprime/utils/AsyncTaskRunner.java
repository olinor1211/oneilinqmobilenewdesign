package com.knx.inquirer.inqprime.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.model.PrimeNewsOfflineModel;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class AsyncTaskRunner extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "AsyncTaskRunner";
    private List<PrimeNewsOfflineModel> newsPrimeList = new ArrayList<>();
    private DatabaseHelper helper;
    private Context context;



    public AsyncTaskRunner(List<PrimeNewsOfflineModel> newsPrimeList){
        this.newsPrimeList = newsPrimeList;
        helper = new DatabaseHelper(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
            for(int x = 0; x<newsPrimeList.size(); x++){
                    //make sure dont convert video into Bitmap..Cause NULL EXCEPTION : Resolved by Filtering inside RepositoryPrimeNewsBySection
                    //convert image    //convertBitmapToByteArray   //convertUrlToBitmap
                    byte[] imageByte = convertBitmapToByteArray(convertUrlToBitmap(newsPrimeList.get(x).getPhoto()));
                    helper.updatePrimeNewsImage(imageByte,Integer.valueOf(newsPrimeList.get(x).getNews_Id()));  //Update Image in DB
                    Log.d(TAG, "run: called" + x + ".. "+ "" + " title.. " + newsPrimeList.get(x).getTitle());
        }
        return null;
    }

    private Bitmap convertUrlToBitmap(String image) {
        try{
            URL url = new URL(image);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            URLConnection connection = url.openConnection();
//            connection.setDoInput(true);
            connection.connect();
            // this will be useful so that you can show a typical 0-100% progress bar
//            int fileLength = connection.getContentLength();
//            InputStream input = connection.getInputStream();
            InputStream input = new BufferedInputStream(connection.getInputStream());
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.d(TAG, "convertUrlToBitmap: "+ myBitmap);
            return myBitmap;
        }catch (Exception e){
            Log.d(TAG, "convertUrlToBitmap: Exception "+ e.getMessage());
            return null;
        }
    }

    private byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

}




