package com.knx.inquirer.inqprime.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "PrimeNewsOffline")
public class PrimeNewsOfflineModel {
    @PrimaryKey
    @NonNull
    private String news_Id;
    private String image_Type;
    private String title;
    private String photo;
    private String published_Date;
    private String author;
    private String content;
    private String description;
    private String link;
    private String tag;
    private int section;
    //for saving image
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] offline_photo;

    //There are multiple good constructors and Room will pick the no-arg constructor. You can use the @Ignore annotation to eliminate unwanted constructors.
//    public PrimeNewsOfflineModel(){
//    }

    public PrimeNewsOfflineModel(@NonNull String news_Id, String image_Type, String title, String photo, String published_Date, String author, String content, String description, String link, String tag, int section, byte[] offline_photo) {
        this.news_Id = news_Id;
        this.image_Type = image_Type;
        this.title = title;
        this.photo = photo;
        this.published_Date = published_Date;
        this.author = author;
        this.content = content;
        this.description = description;
        this.link = link;
        this.tag = tag;
        this.section = section;
        this.offline_photo = offline_photo;
    }

    @NonNull
    public String getNews_Id() {
        return news_Id;
    }

    public void setNews_Id(@NonNull String news_Id) {
        this.news_Id = news_Id;
    }

    public String getImage_Type() {
        return image_Type;
    }

    public void setImage_Type(String image_Type) {
        this.image_Type = image_Type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPublished_Date() {
        return published_Date;
    }

    public void setPublished_Date(String published_Date) {
        this.published_Date = published_Date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public byte[] getOffline_photo() {
        return offline_photo;
    }

    public void setOffline_photo(byte[] offline_photo) {
        this.offline_photo = offline_photo;
    }

    //to be able to use the  .equals method
    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
        } else {
            PrimeNewsOfflineModel news = (PrimeNewsOfflineModel) object;
            if (news_Id.equals(news.getNews_Id())) {
                result = true;
            }
        }
        return result;
    }
}
