package com.knx.inquirer.inqprime.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.inqprime.adapters.RVAdapterPrimeBookmarks;
import com.knx.inquirer.inqprime.model.PrimeBookmarkModel;
import com.knx.inquirer.inqprime.network.PrimeApiService;
import com.knx.inquirer.inqprime.utils.OnViewHolderItemListener;
import com.knx.inquirer.inqprime.utils.PrimeGlobal;
import com.knx.inquirer.inqprime.viewmodels.ViewModelPrimeBookmark;
import com.knx.inquirer.models.Article;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static java.lang.String.valueOf;

public class PrimeBookmark extends AppCompatActivity implements OnViewHolderItemListener {
    private static final String TAG = "PrimeBookmark";

    //utils
    private DatabaseHelper helper;
    private PrimeGlobal.ProgressLoaderDialog progressLoaderDialog = new PrimeGlobal.ProgressLoaderDialog(this);
    private PrimeApiService primeApiService;
    private SharedPrefs sf;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private  ViewModelPrimeBookmark viewModelPrimeBookmark;


    private RecyclerView rvBookmarks;
//    private RecyclerView.Adapter adapter;
    private ArrayList<PrimeBookmarkModel> bookmarks = new ArrayList<>();
//    private MutableLiveData<List<PrimeBookmarkModel>> bookmarks;
    private RVAdapterPrimeBookmarks rvAdapterPrimeBookmarks;

    //ui
    private ImageButton arrowBack;
    private TextView noPrimeBookmark;
    private DividerItemDecoration dividerItemDecoration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prime_bookmark);

        //no status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initViews();

        loadBookmarks();
    }


    //////
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//
//        // Only if you need to restore open/close state when
//        // the orientation is changed
//        if (adapter != null) {
//            adapter.saveStates(outState);
//        }
//    }

//    @Override
////    protected void onRestoreInstanceState(Bundle savedInstanceState) {
////        super.onRestoreInstanceState(savedInstanceState);
////
////        // Only if you need to restore open/close state when
////        // the orientation is changed
////        if (adapter != null) {
////            adapter.restoreStates(savedInstanceState);
////        }
////    }

    public void layoutOneOnClick(View v) {
        Toast.makeText(PrimeBookmark.this, "Layout", Toast.LENGTH_SHORT).show();
    }

    public void deleteOnClick(View v) {
        Toast.makeText(PrimeBookmark.this, "Delete", Toast.LENGTH_SHORT).show();
    }

    public void moreOnClick(View v) {
        Toast.makeText(PrimeBookmark.this, "MoreOn", Toast.LENGTH_SHORT).show();
    }


    ///////

    private void initViews() {

        helper = new DatabaseHelper(this);
        sf = new SharedPrefs(this);
        compositeDisposable = new CompositeDisposable();
        primeApiService = ServiceGenerator.createService(PrimeApiService.class);
        arrowBack = findViewById(R.id.prime_bookmark_arrowBack);

        //RV
        rvBookmarks = findViewById(R.id.rvPrimeBookmark);
        noPrimeBookmark = findViewById(R.id.tvNoPrimeBookmark);

        //RV settings that works even there is no item inside the Adapter...
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        dividerItemDecoration = new DividerItemDecoration(this,linearLayoutManager.getOrientation()); //item decorations
        rvBookmarks.setLayoutManager(linearLayoutManager);
        rvBookmarks.addItemDecoration(dividerItemDecoration); //item decorations
        rvBookmarks.setHasFixedSize(true);
        //calling/creating an instance of RVAdapter Class is in this settings..
        rvAdapterPrimeBookmarks =  new RVAdapterPrimeBookmarks(this, bookmarks, this::onViewHolderItemClick);  //pass the interface
//        rvAdapterPrimeBookmarks =  new RVAdapterPrimeBookmarks(this, bookmarks, this::onViewHolderItemClick);  //pass the interface
        rvBookmarks.setAdapter(rvAdapterPrimeBookmarks);
//        rvAdapterPrimeBookmarks =  new RVAdapterPrimeBookmarks(this, bookmarks, this::onViewHolderItemClick);  //pass the interface

        arrowBack.setOnClickListener(v -> {
            onBackPressed();
        });


    }

    //need to override attachBaseContext ..to make the fontPath working
//    @Override
//    protected void attachBaseContext( Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    //Calligraphy3 ADDED for Tablet Android10
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public void loadBookmarks() {
//        ViewModelPrimeBookmark viewModelPrimeBookmark;
        viewModelPrimeBookmark = ViewModelProviders.of(this).get(ViewModelPrimeBookmark.class);
        viewModelPrimeBookmark.getPrimeBookmarkList().observe(this, data ->{

            bookmarks.clear();
            bookmarks.addAll(data);

        if(data.size()>0){
//            adapter = new RVAdapterPrimeBookmarks(this, bookmarks, this::onViewHolderItemClick); //
            rvAdapterPrimeBookmarks = new RVAdapterPrimeBookmarks(this, bookmarks, this::onViewHolderItemClick); //
//            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvBookmarks);  //attaching ItemTouch to RecyclerView

            rvBookmarks.setAdapter(rvAdapterPrimeBookmarks);  // adapter
            rvAdapterPrimeBookmarks.notifyDataSetChanged();
        }else{
            rvBookmarks.removeItemDecoration(dividerItemDecoration);
            noPrimeBookmark.setVisibility(View.VISIBLE);
        }
        });
    }

    //Deleting Items in the RV by Swiping. This ItemTouch need to attach in RecyclerView
//    ItemTouchHelper.SimpleCallback itemTouchHelperCallback =  new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
//        ItemTouchHelper.SimpleCallback itemTouchHelperCallback =  new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.ACTION_STATE_SWIPE) {
//        //1
//        @Override
//        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
//            //can able to move item in different location in the Adapter list view
//            return false;
//        }
//        //2  Replaced with other options..
//        @Override
//        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
//
//            int position = viewHolder.getAdapterPosition();
//            //get the record of item that is about to delete..
//            int referenceId = bookmarks.get(position).getNewsId();  //database ref id
//            String image = bookmarks.get(position).getImage();
//            String title = bookmarks.get(position).getTitle();
//            String date = bookmarks.get(position).getDate();
//            String author = bookmarks.get(position).getAuthor();
//
//            deleteOneBookmark(referenceId); //DELETE ONE BOOKMARK
//            rvAdapterPrimeBookmarks.notifyItemRemoved(position);
//
//            //Snackbar with UNDO... restore the deleted item
//            Snackbar.make(findViewById(R.id.snackbar_primeBookmark),  "One bookmark is removed.", Snackbar.LENGTH_INDEFINITE)
//            .setAction("UNDO", v -> {
//                helper.insertPrimeBookmark(new PrimeBookmarkModel(referenceId, image,title,date,author)); //re-insert again
//                rvAdapterPrimeBookmarks.notifyItemInserted(position);
//            })
//            .show();
//
//        }

//        //3 onChildDraw Underneath Background in RecyclerView...just showing... Replaced with other options..
//        @Override
//        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
//
//            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
//                    .addSwipeRightBackgroundColor(ContextCompat.getColor(PrimeBookmark.this, R.color.dot_gray))
//                    .addSwipeRightActionIcon(R.drawable.ic_delete_black_24dp)
////                    .addSwipeRightLabel("Remove")
//                    .setSwipeRightLabelColor(R.color.prime_logo_bg)
//                    .create()
//                    .decorate();
//
//            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
//        }


//    };




    private void deleteOneBookmark(int referenceId) {
        helper.deleteOnePrimeBookmark(referenceId);
    }


    @Override
    public void onViewHolderItemClick(View view, int position) {
        String newsId = valueOf(bookmarks.get(position).getNewsId()); //convert newsId to String
        switch(view.getId()){
            case R.id.front_layout:
                viewPrimeArticle(newsId);
                break;
            case R.id.delete_layout:
                //get the record of item that is about to delete..
                int referenceId = bookmarks.get(position).getNewsId();  //database ref id
                String image = bookmarks.get(position).getImage();
                String title = bookmarks.get(position).getTitle();
                String date = bookmarks.get(position).getDate();
                String author = bookmarks.get(position).getAuthor();

                deleteOneBookmark(referenceId); //DELETE ONE BOOKMARK
                rvAdapterPrimeBookmarks.notifyItemRemoved(position);

                //Snackbar with UNDO... restore the deleted item
                Snackbar.make(findViewById(R.id.snackbar_primeBookmark),  "One bookmark is removed.", Snackbar.LENGTH_INDEFINITE)
                .setAction("UNDO", v -> {
                    helper.insertPrimeBookmark(new PrimeBookmarkModel(referenceId, image,title,date,author)); //re-insert again
                    rvAdapterPrimeBookmarks.notifyItemInserted(position);
                    noPrimeBookmark.setVisibility(View.INVISIBLE);
                })
                .show();
                break;

        }

    }

//    @Override
//    public void onViewHolderDeleteItemClick(int position) {
//        String newsId = valueOf(bookmarks.get(position).getNewsId()); //convert newsId to String
//        viewPrimeArticle(newsId);
//    }

    //viewPrimeArticle
    private void viewPrimeArticle(String id) {
        showProgressLoading();
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
        primeApiService.getPrimeArticle(sf.getDeviceId(), id, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Article>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Article article) {

                        dismissProgressLoading();

                        Intent i = new Intent(PrimeBookmark.this, PrimeArticle.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", id);
                        bundle.putString("photo", article.getCreative());
                        bundle.putString("title", article.getTitle());
                        bundle.putString("author", article.getByline());
                        bundle.putString("date", article.getPubdate());
//                        bundle.putString("content", article.getContent().get(0));
                        bundle.putString("type", article.getType());
                        bundle.putString("custom", article.getCustom());
                        bundle.putString("section_name", "SPORT");  //test section
                        //no content2 and image1

                        Log.d(TAG, "onSuccess: ID "+id);

                        i.putExtras(bundle);
                        startActivity(i);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: getPrimeArticle " + e.getMessage());

                    }
                });
    }

    private void dismissProgressLoading() {
        progressLoaderDialog.dismissDialog();
    }

    private void showProgressLoading() {
        progressLoaderDialog.showDialog();
    }

}
























//    String[] photos = {
//            "https://sports.inquirer.net/wp-content/blogs.dir/2/files/2018/07/ONE-FC-REIGN-OF-KINGS-61-300x200.jpg",
//            "https://technology.inquirer.net/wp-content/blogs.dir/4/files/2019/10/96422168_S.jpg",
//            "https://sports.inquirer.net/wp-content/blogs.dir/2/files/2019/08/pff-mva-new-1170x600-300x154.jpg",
//            "https://sports.inquirer.net/wp-content/blogs.dir/2/files/2019/11/AP19331157839035-300x200.jpg"
//    };

//    //loadBookmarks
//    public void loadBookmarks() {
//
//        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/inquirer/");
//
//        if (dir.exists() && dir.isDirectory()) {
//
//            if (dir.list().length > 0) {
//
//                //File Filter
//                File[] files = dir.listFiles(new PrimeWildCardFileFilter("*.html"));
//
//                //Arrays.sort(files, Collections.reverseOrder());
//
//                if (files != null) {
//
//                    String filename;
//                    int x = 0;
//
//                    for (File file : files) {
//
//                        BookmarksModel bmodel = new BookmarksModel();
//
//                        bmodel.filename = file.getName();
//
//                        filename = file.getName();
//
////                        String photo = Global.ReadFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/inquirer/" + file.getName());
//
////                        bmodel.photo = photo;
//
//                        bmodel.photo = photos[x];
//                        x++;
//
//                        String data = Global.ReadFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/inquirer/" + file.getName().replace(".html", ".txt"));
//
//                        String[] tokens = data.split("<del>");
//
//                        bmodel.title = tokens[0];
//
//                        if(TextUtils.isDigitsOnly(tokens[1])) {
//                            bmodel.type = tokens[1];
//                        }else {
//                            bmodel.type = "0";
//                        }
//
//                        bmodel.date = tokens[2];
//
//                        if(isIndexExists(tokens, 3)) {
//                            bmodel.id = tokens[3];
//                        }else{
//                            bmodel.id = filename.substring(0, filename.lastIndexOf('.'));
//                        }
//
////                        bookmarks.clear();
//                        bookmarks.add(bmodel);
//
//                        Log.d("FILE_CONTENT", bookmarks + "");
//
//                        adapter = new RVAdapterPrimeBookmarks(this, bookmarks, this::onViewHolderItemClick);
//                        rvBookmarks.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
//
//                    }
//
//                }
//
//            }
//
//        } else {
//            Log.d(TAG, "loadBookmarks No directory exists: ");
//
//        }
//
//    }
//
//    //isIndexExists
//    public boolean isIndexExists(String[] arr, int index){
//        if( arr != null && index >= 0 && index < arr.length){
//            return true;
//        }
//        return false;
//    }


//
//    @Override
//    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
//
//        int position = viewHolder.getAdapterPosition();
//        int referenceId = bookmarks.get(position).getNewsId();  //database ref id
//        String image = bookmarks.get(position).getImage();
//        String title = bookmarks.get(position).getTitle();
//        String date = bookmarks.get(position).getDate();
//        String author = bookmarks.get(position).getAuthor();
//
//        switch (direction){
//            case ItemTouchHelper.RIGHT:
//                deleteOneBookmark(referenceId); //DELETE ONE BOOKMARK
//                rvAdapterPrimeBookmarks.notifyItemRemoved(position);
//
//                //Snackbar with UNDO
//                Snackbar.make(findViewById(R.id.snackbar_primeBookmark),  "One bookmark is removed.", Snackbar.LENGTH_INDEFINITE)
//                        .setAction("UNDO", v -> {
//                            helper.insertPrimeBookmark(new PrimeBookmarkModel(referenceId, image,title,date,author)); //insert again
//                            rvAdapterPrimeBookmarks.notifyItemInserted(position);
//                        })
//                        .show();
//
//                break;
//
//            case ItemTouchHelper.LEFT:
//                break;
//
//        }



//
//            deleteOneBookmark(referenceId); //DELETE ONE BOOKMARK
//
//            //Snackbar with UNDO
//            Snackbar.make(this,"One bookmark is removed.", Snackbar.LENGTH_LONG)
//                    .setAction("UNDO", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                        }
//                    });


//            Toast.makeText(PrimeBookmark.this, "One bookmark is removed.", Toast.LENGTH_SHORT).show();
//            Log.d(TAG, "onSwiped: Deleted bookmark id  "+ referenceId);