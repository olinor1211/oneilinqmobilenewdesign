package com.knx.inquirer;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.utils.ApiService;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Handler;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class WebBookmarksActivity extends AppCompatActivity {
	private static final String TAG = "WebBookmarksActivity";

	private WebView webView;
//	private WebView webView2;
	private ImageView imageBack;
	private ImageView imageFont;
	private ImageView imageShare;
	private ImageView imageBookmark;
	private ImageView logo;
	private ShimmerFrameLayout mShimmerViewContainer;
	private ImageView bannerIvImage;
	private View parent;
	private ConstraintLayout flRelatedContainer, clInsertContentHolder, clArticleHolder;

	//androidx
	private String adbanner;  //private int adbanner;
	//
	private ImageView mMainImage, mArticleInsertImage;
	private CarouselView carouselView;
	private TextView mSectionName, mTitle, mAuthor, mDate;
	private SimpleExoPlayer exoPlayer3;
	private PlayerView videoArticle;
	private TextView mArticleInsertTitle;


	private long startTime, timeInMilliseconds = 0;
	private int timeCounter;
	private int fontSw = 0;
	private final int NOTIFICATION_PERMISSION_CODE = 10;
	private final int BOOKMARK_PERMISSION_CODE = 20;

	private String bannerAdLink;
	private int idStr;
	private String titleStr = "";
	private String imageStr = "";
	private String typeStr = "";
	private String pubdateStr = "";
	private String contentStr = "";
	private String contentStr2 = "<p><br></p>";
	private String authorStr = "";
	private String customStr = "";
	private String sectionNameStr = "";
	private String bannerLink;
	private String bannerImage;
	private Boolean hasLoadedOnce = false;
	private String htmlStr = "";
	private String httpLinkUrl;
	//
	private String newImage1tStr = "";
	private String insertTitle = "";
	private String insertType = "";
	private String insertNewsId = "";
	private String insertContent = "";
	private boolean hasInsert = false;

	private ApiService apiService;
	private SharedPrefs sf;
	private Handler customHandler = new Handler();
	private List<String> imgItems;
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private DatabaseHelper helper;
	private int section = 0;

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
	}

	@Override
	public void onBackPressed() {
		Log.d(TAG, "onBackPressed: WebNewsActivityLC");
		setResult(Global.NEWS_ARTICLE);
		Global.isArticleViewed = false;
		finish();
	}

//	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sf = new SharedPrefs(this);
		if(sf.getNightMode())setTheme(R.style.DarkTheme);
		else setTheme(R.style.LightTheme);
		setContentView(R.layout.web_bookmarks_view);  //SAME WITH web_news_view  web_deeplink_new_view

		helper = new DatabaseHelper(this);
		apiService = ServiceGenerator.createService(ApiService.class);

//		if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
//			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//		}

		try{
			if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}else{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
		}catch(Exception e){
			Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
		}

		initViews();
		removeAd(); //RemoveFirst
		initViewListeners();
		fetchIntentExtras();
		loadViews();
		checkIfBookmarked(idStr); //after fetch
		configWebViewSettings();

		//TEMP: Display OLD or NEW
		if(sectionNameStr.isEmpty())generateWebArticleNew();
		else generateWebArticle();

		webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
//		loadHtml();   //REMOVE Temp: onProgressChanged move to MyWebViewClient
		initFontSettings();
//		checkIfFiveStoryReads();
	}

	private void loadViews() {
		//Hide SectionName if Not TYPE=0
		if(!typeStr.equals("0")) mSectionName.setVisibility(View.GONE);

		Log.d(TAG, "loadViews: "+ imageStr);
		//SECTION_NAME ChangeColor Controller
		setTitleColor(sectionNameStr.toLowerCase());
		switch (sectionNameStr.toLowerCase()) {
			case "property":
				section = R.drawable.inquirerproperty;
				break;
			case "entertainment":
//				setTitleColor(Global.sectionEntertainment);
				section = R.drawable.inquirerentertainment;
				break;
			case "lifestyle":
//				setTitleColor(Global.sectionLifestyle);
				section = R.drawable.inquirerlifestyle;
				break;
			case "global nation":
				section = R.drawable.inquirerglobalnation; //???
				break;
			case "sports":
//				setTitleColor(Global.sectionSports);
				section = R.drawable.inquirersports;
				break;
			case "technology":
//				setTitleColor(Global.sectionTechnology);
				section = R.drawable.inquirertechnology;
				break;
			case "business":
//				setTitleColor(Global.sectionBusiness);
				section = R.drawable.inquirerbusiness;
				break;
			case "opinion":
//				setTitleColor(Global.sectionOpinion);
				section = R.drawable.inquireropinion;
				break;
			case "board talk":
				section = R.drawable.inquirerboardtalk;
				break;
			case "motoring":
				section = R.drawable.inquirermobility;
				break;
			case "inquirer rebound":
				section = R.drawable.inquirerrebound;
				break;
			default:
				section = R.drawable.inquirernewsinfo; //???
				break;
		}


		if(!imageStr.isEmpty()){
			switch (typeStr){
				case "4":
					carouselView.setVisibility(View.VISIBLE);
					mMainImage.setVisibility(View.GONE);
					videoArticle.setVisibility(View.GONE);
					//TEST CAROUSEL
//					imageStr = "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618920580_Covid%2019-1.jpg," +
//							"https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618910647_000_1UQ6RX.jpeg," +
//							"https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618916490_Front-Page141452-620x354.jpg," +
//							"https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618912845_a33b4a6125f7b3a1d62d316723cef69c.jpg";
					showCarousel(imageStr);
					break;
				case "6":
				case "7":
					//TEST VIDEO
//					imageStr = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
					videoArticle.setVisibility(View.VISIBLE);
					mMainImage.setVisibility(View.GONE);
					carouselView.setVisibility(View.GONE);
					showVideo(imageStr);
					break;
				default:
					mMainImage.setVisibility(View.VISIBLE);
					carouselView.setVisibility(View.GONE);
					videoArticle.setVisibility(View.GONE);

					try{
						showImage(imageStr, mMainImage);
					}catch (Exception e){
						Log.d(TAG, "loadViews: Exception_ERROR: "+e.getMessage());
					}

//					GlideApp
//							.with(this)
//							.load(imageStr)
////							.transition(withCrossFade()) //Remove to make it faster so override(Target.SIZE_ORIGINAL will takeEffect immediately
//							.apply(requestOptions().override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL))
//							.into(mMainImage);
////					ViewCompat.setTransitionName(mMainImage, "mMainImageBookmark"); //setTransitionName

					break;
			}
		}else{
			mMainImage.setImageResource(section);
//			GlideApp
//					.with(this)
//					.load(section)//.load(imageStr)
////					.transition(withCrossFade())
////					.apply(requestOptions())
//					.into(mMainImage);
//			ViewCompat.setTransitionName(mMainImage, "mMainImage"); //setTransitionName
		}

		if(!newImage1tStr.isEmpty() && !insertTitle.isEmpty()){
			hasInsert = true;
			switch (insertType){
				case "4":
//					carouselView.setVisibility(View.VISIBLE);
//					mMainImage.setVisibility(View.GONE);
//					videoArticle.setVisibility(View.GONE);
					//TEST CAROUSEL
//					imageStr = "https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618920580_Covid%2019-1.jpg," +
//							"https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618910647_000_1UQ6RX.jpeg," +
//							"https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618916490_Front-Page141452-620x354.jpg," +
//							"https://inqm.s3.ap-southeast-1.amazonaws.com/inq/1618912845_a33b4a6125f7b3a1d62d316723cef69c.jpg";
//					showCarousel(imageStr);
					break;
				case "6":
				case "7":
					//TEST VIDEO
//					imageStr = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
//					videoArticle.setVisibility(View.VISIBLE);
//					mMainImage.setVisibility(View.GONE);
//					carouselView.setVisibility(View.GONE);
//					showVideo(imageStr);
					break;
				default:
//					mArticleInsertImage.setVisibility(View.VISIBLE);
//					clInsertContentHolder.setVisibility(View.GONE); ////GONE MUNA
//					carouselView.setVisibility(View.GONE);
//					videoArticle.setVisibility(View.GONE);

					try{
						showImage(newImage1tStr, mArticleInsertImage);
					}catch (Exception e){
						Log.d(TAG, "loadViews: Exception_ERROR: "+e.getMessage());
					}

//					GlideApp
//							.with(this)
//							.load(newImage1tStr)
////							.transition(withCrossFade()) //Remove to make it faster so override(Target.SIZE_ORIGINAL will takeEffect immediately
//							.apply(requestOptions().override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL))
//							.into(mArticleInsertImage);
////					ViewCompat.setTransitionName(mArticleInsertImage, "mMainImageBookmark"); //setTransitionName
					break;
			}
		}
//		else{
//			clInsertContentHolder.setVisibility(View.GONE);
//		}

		mSectionName.setText(sectionNameStr.toUpperCase());
//		mTitle.setText(titleStr.toUpperCase());
//		mAuthor.setText(authorStr.toUpperCase());
//		mDate.setText(pubdateStr.toUpperCase());
		//Insert
//		mArticleInsertTitle.setText(insertTitle.toUpperCase());
		if(isValidated(titleStr)) {
			mTitle.setVisibility(View.VISIBLE);
			mTitle.setText(titleStr.toUpperCase());
		}
		if(isValidated(authorStr)){
			mAuthor.setVisibility(View.VISIBLE);
			mAuthor.setText(authorStr.toUpperCase());
		}
		if(isValidated(pubdateStr)){
			mDate.setVisibility(View.VISIBLE);
			mDate.setText(pubdateStr.toUpperCase());
		}

	}

	private void showImage(String imgStr, ImageView imgView){
		//Loading Image and get the Height and Width of the ImageUrl: And load the Image as Bitmap to make it Loading Faster
//		Glide.with(this)
		Glide.with(this)
				.asBitmap()
				.load(imgStr)
				.error(section)
				.apply(requestOptions())
				.into(new CustomTarget<Bitmap>() {
					@Override
					public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
						//Doubled the width and height just to make more clearer bitmap
//						Bitmap scaledBitmap = Bitmap.createScaledBitmap(resource, resource.getWidth()*2, resource.getHeight()*2, true);
//						imgView.setImageBitmap(scaledBitmap);
						imgView.setImageBitmap(resource);
					}
					@Override
					public void onLoadCleared(@Nullable Drawable placeholder) {
					}
				});
	}

	private void showVideo(String image){
		BandwidthMeter bandwidthMeter3 = new DefaultBandwidthMeter();
		TrackSelector trackSelector3 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter3));
		exoPlayer3 = ExoPlayerFactory.newSimpleInstance(this, trackSelector3);

		DefaultHttpDataSourceFactory dataSourceFactory3 = new DefaultHttpDataSourceFactory("exoplayer_3");
		MediaSource mediaSource3 = new ExtractorMediaSource.Factory(dataSourceFactory3).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(image));

		videoArticle.setUseController(true);  //play, stop
		videoArticle.setPlayer(exoPlayer3);
		videoArticle.setShowBuffering(true);

		exoPlayer3.prepare(mediaSource3);
		exoPlayer3.setPlayWhenReady(true);
		exoPlayer3.setVolume(35f);
	}

	private void showCarousel(String images) {
		String imagesUrl[] = images.split(",");
		if (carouselView != null) {
			ImageListener imageListener = (position1, imageView) -> {
				Glide
						.with(this)
						.load(imagesUrl[position1])
//							.transition(withCrossFade()) //Remove to make it faster so override(Target.SIZE_ORIGINAL will takeEffect immediately
						.apply(requestOptions())
						.into(imageView);
//				ViewCompat.setTransitionName(imageView, String.valueOf(idStr)); //setTransitionName
			};
			carouselView.setPageCount(imagesUrl.length);
			carouselView.setImageListener(imageListener);
		}
	}

//	private void setTitsetTitleColorleColor(String strColor) {
//		mSectionName.getBackground().setColorFilter(Color.parseColor(strColor), PorterDuff.Mode.SRC_ATOP); //OK
//	}

	private void setTitleColor(String sectionName) {
		if(Global.sectionList.size()>0){
			for(int x=0; x < Global.sectionList.size(); x++){
				if(Global.sectionList.get(x).getTitle().toLowerCase().equals(sectionName)){
					mSectionName.getBackground().setColorFilter(Color.parseColor(Global.sectionList.get(x).getColor()), PorterDuff.Mode.SRC_ATOP); //OK
				}
			}
		}
	}

	private void checkIfBookmarked(int idStr) {
		try{
			if(Global.bookmarkIdList.contains(idStr)){
				imageBookmark.setImageResource(R.drawable.new_bookmark_fill);
			}else imageBookmark.setImageResource(R.drawable.new_bookmark_empty);
		}catch(Exception e){
			Log.d(TAG, "checkIfBookmarked: Exception_ERROR"+e.getMessage());
		}
	}

	private void initFontSettings() {
		//Replace with SF
		String fontStr = sf.getFontString();

		if (fontStr.equals("0")) {
			adjustTextSize(0);
			fontSw = 0;
			webView.getSettings().setTextZoom(100);
		} else if (fontStr.equals("1")) {
			adjustTextSize((float) 1.5);
			fontSw = 1;
			webView.getSettings().setTextZoom(110);
		} else if (fontStr.equals("2")) {
			adjustTextSize(3);
			fontSw = 2;
			webView.getSettings().setTextZoom(120);
		}

	}

	private void adjustTextSize(float size){
		mTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,17+size);
		mAuthor.setTextSize(TypedValue.COMPLEX_UNIT_SP,11+ size);
		mDate.setTextSize(TypedValue.COMPLEX_UNIT_SP,11+ size);
	}

	private void configWebViewSettings() {

		webView.getSettings().setLoadsImagesAutomatically(true);
		webView.getSettings().setAllowContentAccess(true);
		webView.getSettings().setDomStorageEnabled(true);
		webView.getSettings().setJavaScriptEnabled(true);
//        webView.setWebChromeClient(new WebChromeClient()); //OLD
		webView.setWebViewClient(new MyWebViewClient());  //webView.setWebViewClient(new WebViewClient());
//        webView.getSettings().setAllowFileAccess(true);
		if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
			webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
		}
	}

	//MyWebViewClient: OPEN link externally from android webview
	public final class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
			Intent i = null;
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
				i = new Intent(Intent.ACTION_VIEW, request.getUrl());
			}
			if(i!=null)startActivity(i);
			return true;
		}


		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			imageFont.setClickable(false);
			imageShare.setClickable(false);
			imageBookmark.setClickable(false);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			imageFont.setClickable(true);
			imageShare.setClickable(true);
			imageBookmark.setClickable(true);
			mShimmerViewContainer.setVisibility(View.GONE);
			mShimmerViewContainer.stopShimmer();
			//VISIBLE After mShimmerViewContainer GONE
			clArticleHolder.setVisibility(View.VISIBLE);
		}
	}

//	private void loadHtml(){
//		webView.setWebChromeClient(new WebChromeClient() {
//			public void onProgressChanged(WebView view, int progress) {
//				if (progress == 100) {
//					imageFont.setClickable(true);
//					imageShare.setClickable(true);
//					imageBookmark.setClickable(true);
//					mShimmerViewContainer.setVisibility(View.GONE);
//					mShimmerViewContainer.stopShimmer();
//					//VISIBLE After mShimmerViewContainer GONE
//					clArticleHolder.setVisibility(View.VISIBLE);
//				}
//			}
//		});
//	}

	//generateWebArticle
	private void generateWebArticle() {

		htmlStr = htmlStr + "<!DOCTYPE html> " +
				"<html>";
		htmlStr = htmlStr +
				"<script src=\"file:///android_asset/jquery-3.5.1.min.js\"></script>" + //new jquery-3.5.1.min.js
				"<script src=\"file:///android_asset/listCarousel.js\"></script>" + //listCarousel.js
				"<script src=\"https://api.dmcdn.net/all.js\"></script>" +  //api.dmcdn.net/all.js
				"<head>" + "<meta name=\"viewport\" content=\"width=device-width, user-scalable=no\" />" +  //meta ..name,content,user-scalable
				"<style type=\"text/css\"> " + //style type,
				".slider-previous{ display:none;}" +  //style  .slider-previous
				"table { width=100% }" +  //style for TABLE
				"div {word-wrap: break-word; padding: 10px;}" +  //style for DIV
				"@font-face {font-family: MyFont; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-BOLD.otf\")} " + //style div
				"@font-face {font-family: MyFont2; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-LIGHT.otf\")} " +
				"@font-face {font-family: MyFont3; src: url(\"file:///android_asset/fonts/SF-UI-DISPLAY-REGULAR.otf\")}" +
				"</style></head>";   //style end

//		htmlStr = htmlStr + "<body style='margin:0;padding:0;'>"; //style for BODY
		htmlStr = htmlStr + (sf.getNightMode() ? Global.NIGHT_MODE : Global.NORMAL_MODE); //style for BODY
		//content
		if(!contentStr.isEmpty()) { //REMOVE ONE BR here for the meantime CONCATENATE <br>") + "</font></div><br>";
			htmlStr = htmlStr + "<div><font face=MyFont3>" + contentStr.replace("\n", "<br>") + "</font></div>";
			//For the meantime concatenate the 2 contents
			if(!contentStr2.isEmpty()){
				htmlStr = htmlStr + "<div><font face=MyFont3>" + contentStr2.replace("\n", "<br>") + "</font></div><br>";
			}else{
				htmlStr = htmlStr + "<p><br></p>";
			}

			htmlStr = htmlStr + "<script>$(function() {$('#cms-slider').listCarousel();})</script>";
		}else{
			htmlStr = htmlStr + "";
		}
		htmlStr = htmlStr + "</body></html>";
	}

	private void generateWebArticleNew() {
		//REMOVE all aside from webView
		mMainImage.setVisibility(View.GONE);
		carouselView.setVisibility(View.GONE);
		videoArticle.setVisibility(View.GONE);
		mSectionName.setVisibility(View.GONE);
		mTitle.setVisibility(View.GONE);
		mAuthor.setVisibility(View.GONE);
		mDate.setVisibility(View.GONE);

		htmlStr = htmlStr + contentStr;
		htmlStr = htmlStr + (sf.getNightMode() ? Global.NIGHT_MODE : Global.NORMAL_MODE); //style for BODY
	}

	private void initViewListeners() {
		imageBack.setOnClickListener(v -> {
//           overridePendingTransition(R.anim.stay, R.anim.slide_in_left); //not working To be tested next time
			//RESOLVE: BUGS arrowBack to fragment FINISHED the app and exited when app brought to background
			onBackPressed();
		});

		imageFont.setOnClickListener(v -> {

			if (fontSw == 0) {
				adjustTextSize((float)1.5 );
				fontSw = 1;
				sf.setFontString("1");
				webView.getSettings().setTextZoom(110);
			} else if (fontSw == 1) {
				adjustTextSize(3);
				fontSw = 2;
				sf.setFontString("2");
				webView.getSettings().setTextZoom(120);
			} else if (fontSw == 2) {
				adjustTextSize(0);
				fontSw = 0;
				sf.setFontString("0");
				webView.getSettings().setTextZoom(100);
			}
		});

		//sharelink
		imageShare.setOnClickListener(v -> {
			shareArticle(titleStr,typeStr,String.valueOf(idStr)); //shareArticle2(titleStr,typeStr,String.valueOf(idStr));
		});

		imageBookmark.setOnClickListener(v -> {
		imageBookmark.setEnabled(false);
            //Check the status of the ImageView First to know what process. Then Control the Button by disabling/enabling
			if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
				if(imageBookmark.getDrawable().getConstantState().equals(imageBookmark.getContext().getDrawable(R.drawable.new_bookmark_empty).getConstantState())){
	//            if(imageBookmark.getDrawable().getConstantState()==getResources().getDrawable(R.drawable.new_bookmark_empty).getConstantState()){
					addToBookmark(idStr,titleStr,imageStr,typeStr,pubdateStr,contentStr,"",contentStr2,authorStr,customStr,adbanner,sectionNameStr);
				}else{
					deleteOneBookmark(idStr);
				}
			}
			imageBookmark.setEnabled(true);
		});

//		bannerIvImage.setOnClickListener(v -> {
//			try {
//				Intent intent = new Intent();
//				intent.setAction(Intent.ACTION_VIEW);
//				intent.addCategory(Intent.CATEGORY_BROWSABLE);
//				intent.setData(Uri.parse(bannerAdLink));
//				startActivity(intent);
//			}catch(Exception e){
//				Log.d("TAGGG", "Ad Intent "+ e.getMessage());
//				return;
//			}
//		});
	}

	private void fetchIntentExtras() {
		Intent intent = getIntent();
		idStr = intent.getIntExtra("id",0);
		titleStr = intent.getStringExtra("title");
		imageStr = intent.getStringExtra("photo");
		typeStr = intent.getStringExtra("type");
		pubdateStr = intent.getStringExtra("pubdate");
		contentStr = intent.getStringExtra("content1");
		contentStr2 = intent.getStringExtra("content2");
		authorStr = intent.getStringExtra("author");
		customStr = intent.getStringExtra("custom");
		adbanner = intent.getStringExtra("adbanner");  //adbanner = intent.getIntExtra("adbanner",0);
		sectionNameStr = intent.getStringExtra("sectionName");  //adbanner = intent.getIntExtra("adbanner",0);
		//Insert
		newImage1tStr = intent.getStringExtra("image1");
		insertTitle = intent.getStringExtra("insertTitle");
		insertType = intent.getStringExtra("insertType");
		insertNewsId = intent.getStringExtra("insertNewsId");
		insertContent = intent.getStringExtra("insertContent");

		Log.d(TAG, "fetchIntentExtras: CONTENT HTML"+ contentStr);
		Log.d(TAG, "fetchIntentExtras: CONTENT HTML"+ contentStr2);
		Log.d(TAG, "fetchIntentExtras: CONTENT HTML"+ imageStr);
//		contentStr = "<p>SEOUL — For South Korean celebrities, making headlines for property investments has long been a sign of success.</p><p>The global popularity of Korean movies, television and K-pop has allowed A-listers to rake in cash and purchase properties in Korea’s most coveted locations.<br></p><p>But stars such as actors Kim Tae-hee and Ha Jung-woo have reportedly sold those properties in recent months. Industry watchers have called the offloading “profit-making moves” before the government’s stricter real estate rules on non-residential properties take effect.<br></p><p>And their moves apparently paid off.<br></p><p>Kim, who purchased a five-story retail property near Gangnam Station in June 2014 that was worth 13.2 billion won ($11.7 million) at the time, reportedly sold it for 20.3 billion won in March. She raked in 7.1 billion won from the deal.<br></p><p>For Kim, this was apparently a small step, as the actor and her husband Rain, whom she tied the knot with in 2017, reportedly owned a combined 81.4 billion-won worth of real estate properties as of 2020. Their portfolio includes properties in Seoul and a home in Irvine, California, according to reports.<br></p><p>Ha made roughly 4.6 billion won in profit when he reportedly sold his commercial building in western Seoul — with Starbucks as a key tenant — in March. He bought the building for 7.3 billion won in July 2018 and sold it for 11.9 billion won.<br></p><p>K-pop singer Soyou hopped on the bandwagon after she reportedly sold off her building in western Seoul’s hip Yeonnam-dong which she purchased for 1.5 billion won in 2016. She sold it for nearly double the price in April. The former member of girl group Sistar renovated the building into a retail property after purchasing it as a home.<br></p><p>Actor Han Hyo-joo is one of the investors who made the earliest moves, selling her property in central Seoul for 8 billion won last November, after buying it for 5.5 billion won in 2017, according to reports.<br></p>\n";
//		contentStr = "<p>Flexibility has become more important amid the pandemic, with homes turning into spaces for work, studies, and even exercise. At&nbsp;<a href=\"https://www.theartonbyrockwell.com/\">The Arton by Rockwell</a>, spacious living areas and co-working amenities provide more than enough room for just that.<br></p>";
//		contentStr = "";
//		contentStr2 = "<p><br></p>";

		if(!isValidated(titleStr)) titleStr ="";
		if(!isValidated(imageStr)) imageStr ="";
		if(!isValidated(typeStr)) typeStr ="";
		if(!isValidated(pubdateStr)) pubdateStr ="";
		if(!isValidated(contentStr)) contentStr ="";
		if(!isValidated(contentStr2)) contentStr2 ="";
		if(!isValidated(authorStr)) authorStr ="";
		if(!isValidated(adbanner)) adbanner ="";
		if(!isValidated(sectionNameStr)) sectionNameStr ="";
		if(!isValidated(newImage1tStr)) newImage1tStr ="";
		if(!isValidated(insertTitle)) insertTitle ="";
		if(!isValidated(insertType)) insertType ="";
		if(!isValidated(insertNewsId)) insertNewsId ="";
		if(!isValidated(insertContent)) insertContent ="";

		//Display Insert IF
//		if(!newImage1tStr.isEmpty()) hasInsert = true;
//		else clInsertContentHolder.setVisibility(View.GONE); //NO RELATED, HIDE IT

		//check banner ad
		checkAd2(adbanner);

	}

//	private boolean isValidated(String string){
//
//		try{
//			if(!string.isEmpty()) return true; //Check if Empty
//		}catch(Exception e){
//			Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
//		}finally {
//			if(string!=null) return true;  //Check if null
//			Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
//		}
//		return false;
//	}

	private boolean isValidated(String string){

		try{
			if(!string.isEmpty()) return true; //Check if Empty
		}catch(Exception e){
			Log.d(TAG, "isDisplayType01Validated: Exception_Error isEmpty** ");
			try{
				Log.d(TAG, "isDisplayType01Validated: Exception_Error isNull** ");
				if(string!=null) return true;  //Check if null
			}catch(Exception ex){
				Log.d(TAG, "isDisplayType01Validated: Exception_Error** ");
			}
		}
		return false;
	}

	private void initViews() {

		mShimmerViewContainer = findViewById(R.id.shimmer_article);
		imageBack = findViewById(R.id.imageBack);
		imageFont = findViewById(R.id.imageFont);
		imageShare = findViewById(R.id.imageShare);
		imageBookmark = findViewById(R.id.imageBookmark);
		logo = findViewById(R.id.logo);
		mMainImage = findViewById(R.id.ivArticleMainImage);
		carouselView = findViewById(R.id.cvArticleMainImageCarousel);
		videoArticle = findViewById(R.id.pvArticleMainImageVideo);
		mSectionName = findViewById(R.id.tvArticleSectionName);
		mTitle = findViewById(R.id.tvArticleTitle);
		mAuthor = findViewById(R.id.tvArticleAuthor);
		mDate = findViewById(R.id.tvArticleDate);
		webView = findViewById(R.id.webView);
		clArticleHolder = findViewById(R.id.webviewHolder);

		//
//		flRelatedContainer.setVisibility(View.GONE);
		//INSERT
//		mArticleInsertImage = findViewById(R.id.ivArticleInsertImage);
//		mArticleInsertTitle = findViewById(R.id.tvArticleInsertTitle);


		//test
//		bannerIvImage = findViewById(R.id.ivAdImageWebNews);

		//Start shimmer animation before loading article
		mShimmerViewContainer.setVisibility(View.VISIBLE);
		mShimmerViewContainer.startShimmer();

		//Disable clicking until article was fully loaded
		imageFont.setClickable(false);
		imageShare.setClickable(false);
		imageBookmark.setClickable(false);
	}

	//androidx
	public void checkAd2(String section_id) {
		removeAd(); //Remove Ad First
		Log.d(TAG, "checkAd2: getBannerAd2 section_id_CALLED: " + section_id);
		//CHECK if Global.bannerList is not empty
		if (Global.bannerList.size() > 0) {  //Global.bannerList
			for (int x = 0; x < Global.sectionsBanner2.size(); x++) {  //Iterate in Global.sectionsBanner not in Global.bannerList
				if (Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString().equals(getBannerList(section_id))) {
					Log.d(TAG, "checkAd2: getBannerAd2 Searched_FOUND: "+ Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString()+" == "+getBannerList(section_id));
					String image = Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("image").getAsString();
					String link = Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("link").getAsString();
					Log.d(TAG, "checkAd2: getBannerAd2 Searched_FOUND: IMAGE: "+image+" == LINK: "+link);
					displayAd(image, link); //Global.sectionsBanner
					break;
				}
			}
		}
	}

	private String getBannerList(String sectionId){
		String section_ID = "-1";
		//Note: Already check that Global.bannerList is not empty
		//Finding section_id inside Global.bannerList
		for(int x = 0; x<Global.bannerList.size(); x++){
			if(Global.bannerList.get(x).equals(sectionId)){
				section_ID = Global.bannerList.get(x);
				return section_ID;
			}
		}
		return section_ID;
	}

//	//androidx
	private void displayAd(String image, String link) {
		Log.d(TAG, "displayAd: getBannerAd2 IMAGE: "+image);
//		bannerIvImage.setVisibility(View.VISIBLE);
//		bannerAdLink = link;
		//GlideApp Usage: Need: MyGlide and UnsafeOkHttpClient JavaClass
//		GlideApp.with(this)
//				.load(image)
////							.transition(withCrossFade()) //Remove to make it faster so override(Target.SIZE_ORIGINAL will takeEffect immediately
//				.apply(requestOptions())
//				.into(bannerIvImage);
//		ViewCompat.setTransitionName(bannerIvImage, "BannerAdWebNews"); //setTransitionName
	}

	public void removeAd(){
//		bannerIvImage.setVisibility(View.GONE);
	}

	//NEED placeholder and Error  to DISPLAY Image
	private RequestOptions requestOptions(){
		RequestOptions options = new RequestOptions()
//                .centerCrop()
				.placeholder(section)
				.error(section)
//				.skipMemoryCache(true)
				.diskCacheStrategy(DiskCacheStrategy.ALL)
//				.diskCacheStrategy(DiskCacheStrategy.DATA)//.diskCacheStrategy(DiskCacheStrategy.ALL)
				.priority(Priority.HIGH);
		return options;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override  //onStart called once,
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	/**New Additional NewDesign*/
	public void addToBookmark(int newsId, String title, String mainImage, String type, String date, String content1, String image1, String content2, String author, String custom, String bannerSectionId,String sectionName) {
		sf.setLastBookmarkCount(sf.getLastBookmarkCount()+1); //
		//insert/save to database
		Log.d(TAG, "addToBookmark:getLastBookmarkCount "+sf.getLastBookmarkCount());
		BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId,sf.getLastBookmarkCount(), title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectionName);
//		BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectionName);
		helper.saveBookmark(bookmarkModel);
//		sf.setLastBookmarkCount();
		//Instant Updating the Latest Bookmarking: ACCESSING the ALL ViewHolder
		imageBookmark.setImageResource(R.drawable.new_bookmark_fill);
		toastMessage("This article is added to your bookmark.");
	}

	public void deleteOneBookmark(int newsId) {
		helper.deleteOneBookmark(newsId);
		//Instant Updating the Latest Bookmarking:
		imageBookmark.setImageResource(R.drawable.new_bookmark_empty);
		toastMessage("Bookmark deleted.");
	}

	public void shareArticle(String titleStr, String typeStr, String idStr){
		ApiService apiService = ServiceGenerator.createService(ApiService.class);
		String hashed = Global.Generate32SHA512(sf.getDailyKey() +"inq" + sf.getDeviceId() + "-megashare/" + idStr);
		apiService.getShareLink(sf.getDeviceId(), idStr, typeStr, hashed)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new SingleObserver<String>() {
					@Override
					public void onSubscribe(Disposable d) {
						compositeDisposable.add(d);
					}

					@Override
					public void onSuccess(String link) {

						Intent emailIntent = new Intent(Intent.ACTION_SEND);
						emailIntent.setData(Uri.parse("mailto:"));
						emailIntent.setType("text/plain");

						emailIntent.putExtra(Intent.EXTRA_TEXT, titleStr + "\nvia Inquirer Mobile: " + link);

						try {
							startActivity(Intent.createChooser(emailIntent, "Share with..."));
						} catch (android.content.ActivityNotFoundException ex) {
							Toast.makeText(getApplicationContext(), "There is no share client installed.", Toast.LENGTH_SHORT).show();
						}
					}
					@Override
					public void onError(Throwable e){
						getDailyKey();
						if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)) toastMessage(TAG+": onError: "+e.getMessage());
						Log.d(TAG, "onError: "+e.getMessage());
					}
				});
	}

	public void shareArticle2(String titleStr, String typeStr, String idStr){
		ApiService apiService = ServiceGenerator.createService(ApiService.class);
		long unixTime = System.currentTimeMillis() / 1000L;
		String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megashare/" + idStr);
		apiService.getShareLink2(String.valueOf(unixTime), sf.getDeviceId(), idStr, typeStr, hashed)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new SingleObserver<String>() {
					@Override
					public void onSubscribe(Disposable d) {
						compositeDisposable.add(d);
					}

					@Override
					public void onSuccess(String link) {

						Intent emailIntent = new Intent(Intent.ACTION_SEND);
						emailIntent.setData(Uri.parse("mailto:"));
						emailIntent.setType("text/plain");

						emailIntent.putExtra(Intent.EXTRA_TEXT, titleStr + "\nvia Inquirer Mobile: " + link);

						try {
							startActivity(Intent.createChooser(emailIntent, "Share with..."));
						} catch (android.content.ActivityNotFoundException ex) {
							Toast.makeText(getApplicationContext(), "There is no share client installed.", Toast.LENGTH_SHORT).show();
						}
					}
					@Override
					public void onError(Throwable e) {
						Log.d(TAG, "onError: "+e.getMessage());
					}
				});
	}

	private void getDailyKey(){
		apiService.getDailyKey2().enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if(response.isSuccessful()){
					sf.setDailyKey(response.body());
				}
			}
			@Override
			public void onFailure(Call<String> call, Throwable t) {
				Log.d(TAG, "onFailure: ");
			}
		});
	}

	public void toastMessage(String message) {
		if(Global.toast!=null) Global.toast.cancel(); //Remove First the existing toast
		Global.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT); Global.toast.show(); //Show NEW TOAST
	}
}