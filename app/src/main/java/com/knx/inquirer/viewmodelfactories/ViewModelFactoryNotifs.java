package com.knx.inquirer.viewmodelfactories;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.knx.inquirer.viewmodels.NotifsViewModel;

import io.reactivex.disposables.CompositeDisposable;

public class ViewModelFactoryNotifs implements ViewModelProvider.Factory {
    private Application mApplication;
    private CompositeDisposable mCompositeDisposable;

    public ViewModelFactoryNotifs(Application application, CompositeDisposable disposable) {
        mApplication = application;
        mCompositeDisposable = disposable;
    }

    //Passing Parameter in the ViewModel using ViewModelProvider.Factory
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new NotifsViewModel(mApplication, mCompositeDisposable);
    }
}
