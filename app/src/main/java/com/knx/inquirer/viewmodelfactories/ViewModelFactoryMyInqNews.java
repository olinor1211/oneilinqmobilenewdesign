package com.knx.inquirer.viewmodelfactories;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodels.MyInqNewsBySectionViewModel;
import com.knx.inquirer.viewmodels.MyInqNewsViewModel;

import io.reactivex.disposables.CompositeDisposable;

public class ViewModelFactoryMyInqNews implements ViewModelProvider.Factory {
    private Application mApplication;
    private SharedPrefs mSharedPrefs;
    private CompositeDisposable mCompositeDisposable;

    public ViewModelFactoryMyInqNews(Application application, CompositeDisposable disposable, SharedPrefs sharedPrefs) {
        mApplication = application;
        mCompositeDisposable = disposable;
        mSharedPrefs = sharedPrefs;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MyInqNewsViewModel(mApplication, mCompositeDisposable, mSharedPrefs);
    }
}
