package com.knx.inquirer.viewmodelfactories;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodels.TrendingViewModel;
import io.reactivex.disposables.CompositeDisposable;

public class ViewModelFactoryTrending implements ViewModelProvider.Factory {
    private Application mApplication;
    private SharedPrefs mSharedPrefs;
    private CompositeDisposable mCompositeDisposable;

    public ViewModelFactoryTrending(Application application, CompositeDisposable disposable, SharedPrefs sharedPrefs) {
        mApplication = application;
        mCompositeDisposable = disposable;
        mSharedPrefs = sharedPrefs;
    }

    //Passing Parameter in the ViewModel using ViewModelProvider.Factory
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new TrendingViewModel(mApplication, mCompositeDisposable, mSharedPrefs);
    }
}
