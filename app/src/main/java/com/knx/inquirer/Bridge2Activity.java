package com.knx.inquirer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import com.knx.inquirer.utils.SharedPrefs;

public class Bridge2Activity extends Activity{

	private SharedPrefs sf;
	private boolean viewed;

	  public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);


		  sf = new SharedPrefs(this);

		  viewed =  sf.isMyInqutViewed();
	        

	        if(!viewed)
	        {	
				sf.setMyInqTutViewed(true);

	            Intent intent = new Intent();
	            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.Tutorial3Activity");
	    	    startActivity(intent);
	    	    
	    	    finish();
	        }
	        else
	        {
	            Intent intent = new Intent();
	            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.ReportActivity");
	    	    startActivity(intent);
	    	    finish();
	        }
	  }
}