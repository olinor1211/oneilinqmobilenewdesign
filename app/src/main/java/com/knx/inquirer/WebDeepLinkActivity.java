package com.knx.inquirer;

//import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
        import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
        import android.provider.Settings;
//import android.support.annotation.NonNull;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.app.Fragment;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
        import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
        import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.BookmarksNewModel;
        import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.utils.ApiService;
//import com.knx.inquirer.utils.GlideApp;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;

        import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

        import javax.net.ssl.SSLHandshakeException;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

//@SuppressLint("NewApi")
public class WebDeepLinkActivity extends AppCompatActivity {
    private static final String TAG = "WebDeepLinkActivity";

    private WebView webView;
    private ImageView imageBack;
    private ImageView imageFont;
    private ImageView imageShare;
    private ImageView imageBookmark;
    private ShimmerFrameLayout mShimmerViewContainer;
    private TextView mSectionName, mTitle, mAuthor, mDate;
    private ConstraintLayout clArticleHolder;
    private int counter=0;

    //test
//    private String bannerAdLink;
//    private ImageView bannerIvImage;

    private String newsType = "0";
    private String newsId = "";
    private String newsTitle = "";
    private String httpLink = "";
    private String httpLinkUrl = "";
    private String notification = "";
    private boolean isSuccessful = false;

    //test
//    private String bannerLink;
//    private String bannerImage;

    private boolean hasHttp = false;
    private String htmlStr = "";
    private int fontSw = 0;

    private SharedPrefs sf;
    private ApiService apiService;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DatabaseHelper helper;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    //    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sf = new SharedPrefs(this);
        if(sf.getNightMode())setTheme(R.style.DarkTheme);
        else setTheme(R.style.LightTheme);
        setContentView(R.layout.web_deeplink_new2_view); //R.layout.web_deeplink_view); web_news_view

        apiService = ServiceGenerator.createService(ApiService.class);
        helper = new DatabaseHelper(this);

        try{
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        }catch(Exception e){
            Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
        }

        //additional
        String deviceId = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        sf.setDeviceId(deviceId);
//        int n = sf.getLastCustomSectionPosition(); Log.d(TAG, "onCreate: getLastCustomSectionPosition " +n);

        initViews();
        initViewListeners();


        //Test Notification Data
//        testNotif();
//        fetchIntentExtras();

        /**Notification**/
        if (getIntent().hasExtra("newsLink")) {  //check if has an Intent Extra from Notification
            Log.d(TAG, "onCreate: hasExtra ");
            String notification = getIntent().getStringExtra("newsLink");
            Log.d(TAG, "onCreate: NOTIFICATION newsLink_DATA: "+ notification);

            if(notification.isEmpty()){
                Intent i  = new Intent(this,MainActivity.class);
                startActivity(i);
                finish();
            }
            else if(notification.substring(0, 3).equals("inq"))
            {
                Log.d(TAG, "onCreate: NOTIFICATION newsLink_DATA = inq ");
                int li = notification.indexOf("/");
                String articleID = notification.substring(li+1);
                newsTitle = getIntent().getStringExtra("newsTitle");
                newsType = getIntent().getStringExtra("newsType");

                Log.d(TAG, "onCreate: NOTIFICATION newsLink_DATA articleID: "+articleID);
                getDailyKey(articleID);
                updateNotificationStatus(articleID);
            }else if(notification.substring(0, 4).equals("http"))
            {
                hasHttp = true;
                httpLink = getIntent().getStringExtra("newsLink");
                Log.d(TAG, "onCreate: notification httpLink "+ httpLink);
            }
        }

        //loading Views after getting ALL the VALUE from API
        configWebViewSettings();
        continueToView();

        //
        Global.isItemClicked = false;
    }


    private void testNotif() {
        String image ="https://lifestyle.inquirer.net/wp-content/blogs.dir/8/files/2021/01/kim-jose-atienza-ig-300x179.png";
        String link ="inq/123794";
        String title ="Kim Atienza bonds with son through golf, wishes him well on studies abroad";
        String body ="Kim Atienza bonds with son through golf, wishes him well on studies abroad";
        Date date = Global.convertToDate("2021-01-20 16:25:11"); // String date ="2021-01-20 16:25:11";
        String newsId ="123794";
        NotificationModel nmodel = new NotificationModel( newsId, image, link, title, body, date, 0);
        helper.insertNotification(nmodel);
    }

//    private void setTitleColor(String strColor) {
//        mSectionName.getBackground().setColorFilter(Color.parseColor(strColor), PorterDuff.Mode.SRC_ATOP); //OK
//    }

    public void continueToView(){

        try{
            if(hasHttp){
                Log.d(TAG, "continueToView: hasHttp called");
                webView.loadUrl(httpLink);
                hasHttp = false;
            }else if(httpLinkUrl!=null){
                if(httpLinkUrl.length() > 0){
                    Log.d(TAG, "continueToView: has httpLinkUrl called");
                    webView.loadUrl(httpLinkUrl);  //avoid loading of webArticle  ..generateWebArticle()
                }
            }
            else{
                Log.d(TAG, "continueToView: generateWebArticle called");
                generateWebArticleNew();
                webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
            }
        }
        catch(Exception e){
            Log.d(TAG, "continueToView: Exception_Error: "+e.getMessage());
        }
        initFontSettings();
    }

    private void viewArticleNew(String newsId, String dailyKey) {
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(dailyKey + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + newsId);
        apiService.viewArticle(String.valueOf(unixTime), sf.getDeviceId(),newsId, hashed)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {compositeDisposable.add(d);}

                    @Override
                    public void onSuccess(ResponseBody responseBody) {
                        try {
                            Global.articleViewNotification = responseBody.string();
                            if(Global.articleViewNotification.contains("<!DOCTYPE html>")){
                                Log.d(TAG, "onSuccess: DATA1 viewArticleNew "+  Global.articleView);
                                htmlStr = Global.articleViewNotification;
                                configWebViewSettings(); //
                                generateWebArticleNew();  //
                                try{
                                    webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
                                }catch(Exception e){
                                    Log.d(TAG, "onSuccess: articleView Exception_Error: "+e.getMessage());
                                }
                                continueToView();
                            }else{
                                Log.d(TAG, "onSuccess: viewArticleNew Response Error");
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "onSuccess:Exception viewArticleNew  "+e.getMessage());
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: viewArticleNew Throwable " + e.getMessage());
                        counter++;
                        if(!isSuccessful) viewArticleNew(newsId, dailyKey);

                        if(counter%50==0){
                            if(e instanceof HttpException) {
                                toastMessage("Server error encountered");
                            }else if(e instanceof SocketTimeoutException){
                                toastMessage("Connection timeout reached");
                            }else if(e instanceof UnknownHostException){
                                toastMessage("Internet connection required");
                            }else if(e instanceof SSLHandshakeException){
                                toastMessage("Server connection error");
                            }
                            else{
                                if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
                                    toastMessage(TAG+": onFailure: "+ e.getMessage()); // //Display in developer
                                }else{
                                    toastMessage("Connection error occurred on id #"+newsId); // //Display in user
                                }
                            }
                        }
                    }
                });
    }


//    public void viewArticleOld(String id)
//    {
//        Log.d(TAG, "viewArticle: NEWS_ID: "+id);
//        Log.d(TAG, "viewArticle: DEVICE_ID: "+ sf.getDeviceId());
//
////        showLoader();
////        String hashed = Global.Generate32SHA512(sf.getDailyKey() + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
////        apiService.getArticle(sf.getDeviceId(), id, hashed)
//        apiService.getDailyKey()
//                .flatMap((Function<String, SingleSource<Article>>) key -> {
//                    long unixTime = System.currentTimeMillis() / 1000L;
//                    String hashed = Global.Generate32SHA512(key + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);                    sf.setDailyKey(key);
//                    return apiService.getArticle(String.valueOf(unixTime), sf.getDeviceId(), id, hashed);
//                })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<Article>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        compositeDisposable.add(d);
//                    }
//
//                    @Override
//                    public void onSuccess(Article article) {
//                        String content1 = article.getContent1().isEmpty() ? article.getContent2a() : article.getContent2b();
//                        String content2 = article.getContent2b();
//
//                        idStr = id;
//                        titleStr = article.getTitle();
//                        imageStr = article.getCreative();
//                        typeStr = article.getType();
//                        pubdateStr = article.getPubdate();
//                        contentStr = content1;
//                        contentStr2 = content2;
//                        newImage1tStr = "";
//                        authorStr = article.getByline();
//                        customStr = article.getCustom();
//                        Log.d(TAG, "onSuccess: articleView ");
//                        configWebViewSettings(); //load settings
//                        generateWebArticle();  //loadView
//                        try{
//                            webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
//                        }catch(Exception e){
//                            Log.d(TAG, "onSuccess: articleView Exception_Error: "+e.getMessage());
//                        }
//                        continueToView();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        counter++;
//                        viewArticle(id);
//                        if(counter%30==0){
//                            if(e instanceof IllegalStateException || e instanceof JsonSyntaxException){
//                                Toast.makeText(getApplicationContext(), "Server return type error", Toast.LENGTH_SHORT).show();
//                            }
//                            else if(e instanceof HttpException) {
//                                Toast.makeText(getApplicationContext(), "Server error encountered", Toast.LENGTH_SHORT).show();
//                            }else if(e instanceof SocketTimeoutException){
//                                Toast.makeText(getApplicationContext(), "Connection timeout reached", Toast.LENGTH_SHORT).show();
//                            }else if(e instanceof UnknownHostException){
//                                Toast.makeText(getApplicationContext(), "Internet connection required", Toast.LENGTH_SHORT).show();
//                            }else if(e instanceof SSLHandshakeException){
//                                Toast.makeText(getApplicationContext(), "Server connection error", Toast.LENGTH_SHORT).show();
//                            }else{
//                                Toast.makeText(getApplicationContext(), "Unexpected error detected!", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                        Log.d(TAG, "onError: viewArticle Throwable_Exception: "+e);
//                        Log.d(TAG, "onError: viewArticle Throwable_Exception Message: "+e.getMessage());
//                        return;
//                    }
//                });
//    }

//    public void viewArticleOLD2(String id, String dailyKey) { // public void viewArticle(String id) {
//        long unixTime = System.currentTimeMillis() / 1000L;
//        String hashed = Global.Generate32SHA512(dailyKey + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
//        apiService.getArticle2(String.valueOf(unixTime),sf.getDeviceId(), id, hashed)
//                .enqueue(new Callback<JsonObject>() {
//                    @Override
//                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                        try{
//                            isSuccessful = true;
//                            String content1="";
//                            String content2="";
//                            String insertNewId="";
//                            String insertType="";
//                            String insertTitle="";
//                            String insertImage="";
//                            String insertContent="";
//                            //Sometimes NO Section_ID and DATE: Occur on ADS Type1
//                            String section_id="", pubdate="", author="", title="", photo="", type="";
//                            if(response.body().has("section_id")) section_id = !response.body().get("section_id").isJsonNull() ? response.body().get("section_id").getAsString() : "";
//                            if(response.body().has("pubdate")) pubdate = !response.body().get("pubdate").isJsonNull() ? response.body().get("pubdate").getAsString() : "";
//                            if(response.body().has("byline")) author = !response.body().get("byline").isJsonNull() ? response.body().get("byline").getAsString() : "";
//                            if(response.body().has("title")) title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
//                            if(response.body().has("creative")) photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
//                            if(response.body().has("type")) type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";
//
//                            //Get the SuperShare: Check if has an INSERT (for SuperShare)
//                            if(response.body().has("insert")){
//                                //Check if CONTENT as an ARRAY
//                                if(response.body().has("content")){
//                                    content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
//                                    content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
//                                }
//                                if(response.body().get("insert").getAsJsonObject().has("id")) insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
//                                if(response.body().get("insert").getAsJsonObject().has("type"))  insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
//                                if(response.body().get("insert").getAsJsonObject().has("title")) insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
//                                if(response.body().get("insert").getAsJsonObject().has("creative")) insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
//                                if(response.body().get("insert").getAsJsonObject().has("content"))  insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
//                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
//
//                            }else {
//                                //Check if CONTENT as a STRING
//                                if(response.body().has("content")) content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
//                            }
//                            //Get the Related Article: First Check if has RelatedArticle
//                            Global.relatedArticles.clear();
//                            if(response.body().has("related")){
//                                Log.d(TAG, "onResponse: CONTENT_  test1");
//                                for(int x=0; x<response.body().get("related").getAsJsonArray().size();x++){
//                                    Global.relatedArticles.add(response.body().get("related").getAsJsonArray().get(x).getAsJsonObject());
//                                }
//                            }
//
//                            idStr = id;
//                            titleStr = title;
//                            imageStr = photo;
//                            typeStr = type;
//                            pubdateStr = pubdate;
//                            contentStr = content1;
//                            contentStr2 = content2;
//                            newImage1tStr = insertImage;
//                            authorStr = author;
//                            customStr = "";
//                            sectionId = section_id;//
//                            getSectionName(section_id);//getSectionName
//
//                            Log.d(TAG, "onSuccess: articleView ");
//                            configWebViewSettings(); //load settings
//                            generateWebArticle();  //loadView
//                            try{
//                                webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
//                            }catch(Exception e){
//                                Log.d(TAG, "onSuccess: articleView Exception_Error: "+e.getMessage());
//                            }
//                            continueToView();
//
//                        }catch (Exception e){
//                            Log.d(TAG, "onResponse: viewArticle Exception: "+ e.getCause());
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<JsonObject> call, Throwable t) {
//                        counter++;
//                        if(!isSuccessful) viewArticle(id, dailyKey);
//
//                        if(counter%50==0){
//                            if(t instanceof HttpException) {
//                                toastMessage("Server error encountered");
////                                Toast.makeText(getApplicationContext(), "Server error encountered", Toast.LENGTH_SHORT).show();
//                            }else if(t instanceof SocketTimeoutException){
//                                toastMessage("Connection timeout reached");
////                                Toast.makeText(getApplicationContext(), "Connection timeout reached", Toast.LENGTH_SHORT).show();
//                            }else if(t instanceof UnknownHostException){
//                                toastMessage("Internet connection required");
////                                Toast.makeText(getApplicationContext(), "Internet connection required", Toast.LENGTH_SHORT).show();
//                            }else if(t instanceof SSLHandshakeException){
//                                toastMessage("Server connection error");
////                                Toast.makeText(getApplicationContext(), "Server connection error", Toast.LENGTH_SHORT).show();
//                            }
////                            else if(t instanceof IllegalStateException || t instanceof JsonSyntaxException){
////                                toastMessage("Connection error occurred on id #"+id);
//////                                Toast.makeText(getApplicationContext(), "Server return type error", Toast.LENGTH_SHORT).show();
////                            }
//                            else{
//                                //For Developer Debugging check email and name and mobile
//                                if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
//                                    toastMessage(TAG+": onFailure: "+ t.getMessage()); // //Display in developer
//                                }else{
//                                    toastMessage("Connection error occurred on id #"+id); // //Display in user
//                                }
//                            }
//                        }
//                        Log.d(TAG, "onError: viewArticle Throwable_Exception Message: "+t.getMessage());
//                    }
//                });
//    }

    private void updateNotificationStatus(String newsId) {
        helper.updateNotificationStatus2(1, newsId);  //notification to read status
        Log.d(TAG, "updateNotificationStatus: viewArticle Sucess NOTIFICATION ");
    }


    private void initFontSettings() {
        //Replace with SF
        String fontStr = sf.getFontString();
        if (fontStr.equals("0")) {
            adjustTextSize(0);
            fontSw = 0;
            webView.getSettings().setTextZoom(100);
        } else if (fontStr.equals("1")) {
            adjustTextSize((float)1.5);
            fontSw = 1;
            webView.getSettings().setTextZoom(110);
        } else if (fontStr.equals("2")) {
            adjustTextSize(3);
            fontSw = 2;
            webView.getSettings().setTextZoom(120);
        }
    }

    private void configWebViewSettings() {
        try{
            webView.getSettings().setAllowContentAccess(true);
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebViewClient(new MyWebViewClient()); //
            webView.getSettings().setLoadsImagesAutomatically(true);
            if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }
        }catch(Exception e){
            Log.d(TAG, "configWebViewSettings: Exception_Error: "+e.getMessage());
        }
    }

    //MyWebViewClient: OPEN link externally from android webview
    public final class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Intent i = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                i = new Intent(Intent.ACTION_VIEW, request.getUrl());
            }
            if(i!=null)startActivity(i);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            imageFont.setClickable(false);
            imageShare.setClickable(false);
            imageBookmark.setClickable(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            imageFont.setClickable(true);
            imageShare.setClickable(true);
            imageBookmark.setClickable(true);
            mShimmerViewContainer.setVisibility(View.GONE);
            mShimmerViewContainer.stopShimmer();
            //VISIBLE After mShimmerViewContainer GONE
            clArticleHolder.setVisibility(View.VISIBLE);
        }
    }

//    private void loadHtml(){
//        //webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
//
//        try{
//            webView.setWebChromeClient(new WebChromeClient() {
//                public void onProgressChanged(WebView view, int progress) {
//                    if (progress == 100) {
//                        imageFont.setClickable(true);
//                        imageShare.setClickable(true);
//                        imageBookmark.setClickable(true);
//                        //animation
//                        mShimmerViewContainer.setVisibility(View.GONE);
//                        mShimmerViewContainer.stopShimmer();
//                        //VISIBLE After mShimmerViewContainer GONE
//                        clArticleHolder.setVisibility(View.VISIBLE);
//
//                    }
//                }
//            });
//
//        }catch(Exception e){
//            Log.d(TAG, "loadHtml: Exception_Error: "+e.getMessage());
//        }
//    }

    private void generateWebArticleNew() {
        htmlStr = htmlStr + (sf.getNightMode() ? Global.NIGHT_MODE : Global.NORMAL_MODE); //style for BODY
    }

    private void initViewListeners() {

        imageBack.setOnClickListener(v -> {
            onBackPressed();
//            overridePendingTransition(R.anim.stay, R.anim.slide_out_left);
//            finish();
        });

        imageFont.setOnClickListener(v -> {
            if (fontSw == 0) {
                adjustTextSize((float)1.5);
                fontSw = 1;
                sf.setFontString("1");
                webView.getSettings().setTextZoom(110);
            } else if (fontSw == 1) {
                adjustTextSize(3);
                fontSw = 2;
                sf.setFontString("2");
                webView.getSettings().setTextZoom(120);
            } else if (fontSw == 2) {
                adjustTextSize(0);
                fontSw = 0;
                sf.setFontString("0");
                webView.getSettings().setTextZoom(100);
            }
        });

        //sharelink
        imageShare.setOnClickListener(v -> {
            shareLink2(newsId, newsType); //
        });

        imageBookmark.setOnClickListener(v -> {
            imageBookmark.setEnabled(false);
            //Check the status of the ImageView First to know what process. Then Control the Button by disabling/enabling
            if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                if(imageBookmark.getDrawable().getConstantState().equals(imageBookmark.getContext().getDrawable(R.drawable.new_bookmark_empty).getConstantState())){
                    addToBookmark(Integer.valueOf(newsId), Global.articleViewNotification);
                }else{
                    deleteOneBookmark(Integer.valueOf(newsId));
                }
            }
            imageBookmark.setEnabled(true);
        });
    }

    /**New Additional NewDesign*/
    public void addToBookmark(int newsId, String htmlContent) {
        sf.setLastBookmarkCount(sf.getLastBookmarkCount()+1);
        Log.d(TAG, "addToBookmark:getLastBookmarkCount "+sf.getLastBookmarkCount());
        //insert/save to database
//        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, sf.getLastBookmarkCount(),title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectionName);
        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, sf.getLastBookmarkCount(),Global.articleViewNotification);
        helper.saveBookmark(bookmarkModel);
        //Instant Updating the Latest Bookmarking: ACCESSING the ALL ViewHolder
        imageBookmark.setImageResource(R.drawable.new_bookmark_fill);
        toastMessage("This article is added to your bookmark.");
    }

    public void deleteOneBookmark(int newsId) {
        helper.deleteOneBookmark(newsId);
        //Instant Updating the Latest Bookmarking:
        imageBookmark.setImageResource(R.drawable.new_bookmark_empty);
        toastMessage("Bookmark deleted.");
    }

    public void toastMessage(String message) {
        if(Global.toast!=null) Global.toast.cancel(); //Remove First the existing toast
        Global.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT); Global.toast.show(); //Show NEW TOAST
    }

    private void initViews() {
        mShimmerViewContainer = findViewById(R.id.shimmer_article);
        imageBack = findViewById(R.id.imageBack);
        imageFont = findViewById(R.id.imageFont);
        imageShare = findViewById(R.id.imageShare);
        imageBookmark = findViewById(R.id.imageBookmark);
        webView = findViewById(R.id.webView);
        clArticleHolder = findViewById(R.id.webviewHolder);

        //Start shimmer animation before loading article
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        mShimmerViewContainer.startShimmer();

        //Disable clicking until article was fully loaded
        imageFont.setClickable(false);
        imageShare.setClickable(false);
        imageBookmark.setClickable(false);

    }

    //sharelink
    private void shareLink2(String id, String type){
        ApiService apiService = ServiceGenerator.createService(ApiService.class);
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime +"inq" + sf.getDeviceId() + "-megashare/" + id);
        apiService.getShareLink2(String.valueOf(unixTime), sf.getDeviceId(), id, type, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");

                        emailIntent.putExtra(Intent.EXTRA_TEXT, newsTitle + "\nvia Inquirer Mobile: " + link);

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(WebDeepLinkActivity.this, "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey(id);
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)) toastMessage(TAG+": onError: "+e.getMessage());
                    }
                });
    }

    @Override
    public void onBackPressed() {
        try{
                //Check if App is open/ or in the background: NOT OPEN: CLOSED_the_App finishActivity
                //Check if MainActivity is OPEN
                if(!Global.isMainActivityOpen){
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                }
                finish();
//            }
            super.onBackPressed();
        }catch(Exception e){
            Log.d(TAG, "onBackPressed: WebDeepLinkActivity Exception_Error: "+e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        super.onDestroy();
    }

    private void adjustTextSize(float size){
        mTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,17+size);
        mAuthor.setTextSize(TypedValue.COMPLEX_UNIT_SP,11+ size);
        mDate.setTextSize(TypedValue.COMPLEX_UNIT_SP,11+ size);
    }

    private void getDailyKey(String articleID){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
//                    viewArticle(articleID, response.body()); //
                    viewArticleNew(articleID, response.body()); //

                    Log.d(TAG, "onResponse: getDailyKey: "+sf.getDailyKey());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getDailyKey(articleID);
            }
        });
    }



}
