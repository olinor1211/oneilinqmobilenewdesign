package com.knx.inquirer;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Build.VERSION_CODES;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

public class WebActivity extends AppCompatActivity {
	
	private WebView webView;
	
	private ImageView imageBack;

	private ProgressBar pLoadingWebView;
	
	private String urlStr = "";

	private AudioManager amanager;

	@Override
	protected void onPause() {
		super.onPause();
		amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
	}

	@Override
	protected void onResume() {
		super.onResume();
		amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.web_view);

		if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		amanager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

		Intent intent = getIntent();
	    urlStr = intent.getStringExtra("url");
	    pLoadingWebView = findViewById(R.id.pLoadingWebView);

		imageBack = findViewById(R.id.imageBack);
		imageBack.setOnClickListener(v -> {
		          // TODO Auto-generated method stub
			      finish();
		     });

	    webView = findViewById(R.id.webView);

		if (Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN)
		webView.getSettings().setAllowUniversalAccessFromFileURLs(true);

	    webView.setWebViewClient(new WebViewClient());
		webView.getSettings().setLoadsImagesAutomatically(true);
		webView.getSettings().setAllowContentAccess(true);
		webView.getSettings().setDomStorageEnabled(true);
		webView.getSettings().setJavaScriptEnabled(true);

		if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
			webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
		}

	    webView.loadUrl(urlStr);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    pLoadingWebView.setVisibility(View.GONE);
                }
            }
        });

    }
 }
