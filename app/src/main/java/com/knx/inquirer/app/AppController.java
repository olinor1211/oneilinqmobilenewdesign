package com.knx.inquirer.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.knx.inquirer.BuildConfig;
import com.knx.inquirer.R;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.MyGlide;
import com.knx.inquirer.utils.NetworkSchedulerService;
import com.knx.inquirer.utils.SharedPrefs;
import com.squareup.picasso.Picasso;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Collections;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
//import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

//public class AppController extends Application{
public class AppController extends Application implements Application.ActivityLifecycleCallbacks {  //TEMP REMOVE
    private static final String TAG = "AppController";

    private static AppController appInstance;
//    private SharedPrefs sf;
    public static AppController getApp() {
        return appInstance;
    }
    private int count = 0;
    private MyGlide myGlide;

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;

        //font settings... Note: need to override attachBaseContext  to the Activity ..to make it working
        //CalligraphyConfig 3 ADDED for Tablet Android10
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/MalloryMP-Medium.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());


        //overriding Rx Error Handler here for Debugging
        RxJavaPlugins.setErrorHandler(throwable -> {
            count++;
            Log.d(TAG, "Rx_ErrorHandler ERROR_MESSAGE: "+ throwable.getMessage());
            Log.d(TAG, "Rx_ErrorHandler ERROR_CAUSE: "+throwable.getCause());
            if(throwable.getCause() instanceof IndexOutOfBoundsException){
//                Toast.makeText(appInstance, "App_Error:"+count+": Index out of bound", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Rx_ErrorHandler App_Error:"+count+": Index out of bound");
                Log.d(TAG, "Rx_ErrorHandler App_Error Cause:"+count+": "+throwable.getCause() );
                Log.d(TAG, "Rx_ErrorHandler App_Error Message:"+count+": "+throwable.getMessage() );
            }else{
//                Toast.makeText(appInstance, "App_Error:"+count+": "+throwable.getCause(), Toast.LENGTH_LONG).show();
                Log.d(TAG, "Rx_ErrorHandler App_Error:"+count+": "+throwable.getCause());
            }
        });

//        sf = new SharedPrefs(this);
        myGlide = new MyGlide(); //Instantiate MyGlide  use for AppGlide

        //TEMP REMOVE
//        double currentVersionCode = Double.valueOf(BuildConfig.VERSION_NAME);
//        Log.i("VERSION_NUM", currentVersionCode + "");
//        if(currentVersionCode == 9.3){
//            if(!sf.isSFCleared()){
//                sf.clearSharedPrefs();
//                sf.setSFCleared(true);
//            }
//        }else{
//            Log.i("UPGRADE_MSG", "did not clear SF");
//        }

        registerActivityLifecycleCallbacks(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scheduleJob();
        }

        //Replaced
        //font settings... Note: need to override attachBaseContext  to the Activity ..to make it working
//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/SF-UI-Text-Regular.otf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );
    }

    private void scheduleJob() {
        JobInfo myJob = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class))
                    .setMinimumLatency(1000)
                    .setOverrideDeadline(2000)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true)
                    .build();
        }

        JobScheduler jobScheduler = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jobScheduler.schedule(myJob);
        }
    }

    //ALL TEMP REMOVE from implements
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

//    @Override
    public void onActivityStarted(Activity activity) {
        try{
            Intent startServiceIntent = new Intent(this, NetworkSchedulerService.class);
            startService(startServiceIntent);
        }catch(Exception e){
            Log.d(TAG, "onActivityStarted: Exception_ERROR: "+ e.getMessage());
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        stopService(new Intent(this, NetworkSchedulerService.class));
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
