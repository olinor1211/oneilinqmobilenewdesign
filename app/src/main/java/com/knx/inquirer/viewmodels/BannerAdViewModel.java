package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.models.MenuAd;
import com.knx.inquirer.models.MenuAdResponse;
import com.knx.inquirer.models.Sections;
import com.knx.inquirer.repository.BannerAdRepository;

import java.util.List;

public class BannerAdViewModel extends AndroidViewModel {
    private BannerAdRepository bannerAdRepository;

    public BannerAdViewModel(@NonNull Application application) {  //Passing the Context = application.getApplicationContext()
        super(application);
        bannerAdRepository = new BannerAdRepository(application.getApplicationContext()); //Creating an instance of Repository
    }

    public LiveData<MenuAdResponse> getBannerAd(){  //MenuAdResponse
        return bannerAdRepository.getBannerAd();
    }

    public LiveData<Boolean> isBannerAdRepoError(){
        return bannerAdRepository.isBannerAdRepoError();
    }

    public LiveData<Throwable> getBannerAdError(){
        return bannerAdRepository.getBannerAdError();
    }
}
