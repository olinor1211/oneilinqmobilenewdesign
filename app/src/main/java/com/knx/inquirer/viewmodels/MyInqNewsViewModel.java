package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.models.Feed;
import com.knx.inquirer.repository.MyInqNewsRepository;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class MyInqNewsViewModel extends AndroidViewModel {

    private MyInqNewsRepository myInqNewsRepository;

    public MyInqNewsViewModel(@NonNull Application application, CompositeDisposable compositeDisposable, SharedPrefs sf) {
        super(application);
        myInqNewsRepository = new MyInqNewsRepository(application.getApplicationContext(),compositeDisposable, sf);
    }

    public LiveData<List<Feed>> getMyInqNews(){
        return myInqNewsRepository.getMyInqNews();
    }

    public LiveData<List<Feed>> getMyInqCachedNews(){
        return myInqNewsRepository.getMyInqCachedNews();
    }

//    public LiveData<List<Section>> getCachedMenu(){
//        return menuRepository.getCachedSections();
//    }

    public LiveData<Boolean> getMyInqError(){
        return  myInqNewsRepository.getMyInqError();
    }

    public LiveData<Throwable> getMyInqthrowable(){
        return  myInqNewsRepository.getMyInqThrowable();
    }

//    public LiveData<Integer> getSectionRecordCount(){
//        return  menuRepository.getSectionRecordCount();
//    }
}
