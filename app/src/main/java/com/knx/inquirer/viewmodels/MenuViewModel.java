package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonObject;
import com.knx.inquirer.models.MenuResponse;
import com.knx.inquirer.models.Section;
import com.knx.inquirer.repository.MenuRepository;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class MenuViewModel extends AndroidViewModel {

    private MenuRepository menuRepository;

    public MenuViewModel(@NonNull Application application, CompositeDisposable compositeDisposable, SharedPrefs sf) {
        super(application);
        menuRepository = new MenuRepository(application.getApplicationContext(),compositeDisposable, sf);
    }

    public MutableLiveData<JsonObject> getMenu(){
        return menuRepository.getMenu();
    }

//    public MutableLiveData<MenuResponse> getMenu(){
//        return menuRepository.getMenu();
//    }

    public LiveData<List<Section>> getCachedMenu(){
        return menuRepository.getCachedSections();
    }

    public LiveData<Boolean> getMenuError(){
        return  menuRepository.getMenuError();
    }

    public LiveData<Throwable> getMenuThrowable(){
        return  menuRepository.getMenuThrowable();
    }

    public LiveData<Integer> getSectionRecordCount(){
        return  menuRepository.getSectionRecordCount();
    }

    public LiveData<Integer> getNewsRecordCount(){
        return  menuRepository.getNewRecordCount();
    }

}
