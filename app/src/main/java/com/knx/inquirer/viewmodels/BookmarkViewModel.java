package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.repository.BookmarkRepository;

import java.util.List;

public class BookmarkViewModel extends AndroidViewModel {
    private BookmarkRepository bookmarkRepository;

    public BookmarkViewModel(@NonNull Application application) {  //Passing the Context = application.getApplicationContext()
        super(application);
        bookmarkRepository = new BookmarkRepository(application.getApplicationContext()); //Creating an instance of Repository
    }

    public LiveData<List<BookmarksNewModel>> getBookmarkList(){return bookmarkRepository.getBookmarkList();}

}
