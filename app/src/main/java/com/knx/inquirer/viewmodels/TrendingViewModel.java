package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.models.TrendingModel;
import com.knx.inquirer.repository.NewsRepository;
import com.knx.inquirer.repository.TrendingRepository;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class TrendingViewModel extends AndroidViewModel {

    private TrendingRepository trendingRepository;

    public TrendingViewModel(@NonNull Application application, CompositeDisposable compositeDisposable, SharedPrefs sf) {
        super(application);
        trendingRepository = new TrendingRepository(application.getApplicationContext(),compositeDisposable, sf);
    }

    public LiveData<List<TrendingModel>> getTrendingNews(){
        return trendingRepository.getTrendingNews();
    }

    public LiveData<Boolean> getNewsError(){
        return  trendingRepository.getNewsError();
    }

    public LiveData<Throwable> getNewsThrowable(){
        return  trendingRepository.getNewsThrowable();
    }
}
