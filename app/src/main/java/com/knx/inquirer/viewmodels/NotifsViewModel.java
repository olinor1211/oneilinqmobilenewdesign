package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.models.NotificationModel;
import com.knx.inquirer.repository.NotifsRepository;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class NotifsViewModel extends AndroidViewModel {

    private NotifsRepository notifsRepository;

    public NotifsViewModel(@NonNull Application application, CompositeDisposable compositeDisposable) {
        super(application);
        notifsRepository = new NotifsRepository(application.getApplicationContext(), compositeDisposable);
    }

    //select only here
    public LiveData<List<NotificationModel>> getNotifications(){
        return notifsRepository.getNotifications();
    }

    //select only here
    public LiveData<Integer> getUnreadNotifsCount(){
        return notifsRepository.getUnreadNotifsCount();
    }

}
