package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.FeedResponse;
import com.knx.inquirer.models.Option;
import com.knx.inquirer.repository.FeedRepository;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class FeedViewModel extends AndroidViewModel {

    private FeedRepository feedRepository;

    public FeedViewModel(@NonNull Application application, CompositeDisposable compositeDisposable, SharedPrefs sf) {
        super(application);
        feedRepository = new FeedRepository(application.getApplicationContext(),compositeDisposable, sf);
    }

    public LiveData<FeedResponse> getMenuandNews(){
        return feedRepository.getMenuandNews();
    }

    public LiveData<List<Option>> getCachedCustomSections(){
        return feedRepository.getCachedCustomSections();
    }

    public LiveData<List<Feed>> getCachedCustomNews(){
        return feedRepository.getCachedCustomNews();
    }

    public LiveData<Boolean> getFeedError(){
        return  feedRepository.getFeedError();
    }

    public LiveData<Integer> getCustomSectionRecordCount(){
        return  feedRepository.getCustomSectionRecordCount();
    }

    public LiveData<Integer> getCustomNewsRecordCount(){
        return  feedRepository.getCustomNewsRecordCount();
    }

}
