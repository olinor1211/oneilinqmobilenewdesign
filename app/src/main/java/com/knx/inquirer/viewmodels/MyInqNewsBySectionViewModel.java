package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.models.Feed;
import com.knx.inquirer.repository.MyInqNewsBySectionRepository;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class MyInqNewsBySectionViewModel extends AndroidViewModel {

    private MyInqNewsBySectionRepository newsRepository;

    public MyInqNewsBySectionViewModel(@NonNull Application application, CompositeDisposable compositeDisposable, SharedPrefs sf) {
        super(application);
        newsRepository = new MyInqNewsBySectionRepository(application.getApplicationContext(),compositeDisposable, sf);
    }

    public LiveData<List<Feed>> getCustomNews(int mParam, int mPos){
        return newsRepository.getCustomNews(mParam, mPos);
    }

/*
    public LiveData<List<NewsModel>> getCachedNews(){
        return newsRepository.getCachedNews();
    }
*/

    public LiveData<Boolean> getCustomNewsError(){
        return  newsRepository.getCustomNewsError();
    }

    public LiveData<Throwable> getCustomNewsThrowable(){
        return  newsRepository.getCustomNewsThrowable();
    }

/*    public LiveData<Integer> getNewsRecordCount(){
        return  newsRepository.getNewsRecordCount();
    }*/
}
