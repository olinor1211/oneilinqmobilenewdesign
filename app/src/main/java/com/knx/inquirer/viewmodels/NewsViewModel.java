package com.knx.inquirer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.repository.NewsRepository;
import com.knx.inquirer.utils.SharedPrefs;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class NewsViewModel extends AndroidViewModel {

    private NewsRepository newsRepository;

    public NewsViewModel(@NonNull Application application, CompositeDisposable compositeDisposable, SharedPrefs sf) {
        super(application);
        newsRepository = new NewsRepository(application.getApplicationContext(),compositeDisposable, sf);
    }

    public LiveData<List<NewsModel>> getNews(int mParam, int pos){
        return newsRepository.getNews(mParam, pos);
    }

    public LiveData<List<NewsModel>> getCachedNews(){
        return newsRepository.getCachedNews();
    }

    public LiveData<Boolean> getNewsError(){
        return  newsRepository.getNewsError();
    }

    public LiveData<Throwable> getNewsThrowable(){
        return  newsRepository.getNewsThrowable();
    }

    public LiveData<Integer> getNewsRecordCount(){
        return  newsRepository.getNewsRecordCount();
    }
}
