package com.knx.inquirer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.utils.ApiService;
//import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONException;

import java.io.IOException;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebNewsActivity extends AppCompatActivity {
    private static final String TAG = "WebNewsActivityNew";

    private WebView webView;
    private ConstraintLayout clArticleHolder;
    private ImageListener imageListener, imageInsertListener;
    private ImageView imageBack;
    private ImageView imageFont;
    private ImageView imageShare;
    private ImageView imageBookmark;

    private long currentTime = 0;
    private int timeCounter;
    private int fontSw = 0;

    private String htmlStr = "";
    private String httpLinkUrl;
    private String sectionId;
    private String newsId ="";
    private String newsType = "";
    private String newsTitle = "";
    private String newsPhoto = "";
    private String newsDate = "";
    private String newsContent = "";

    private boolean hasNewDailyKey = false;

    private ApiService apiService;
    private SharedPrefs sf;
    private Handler customHandler = new Handler();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DatabaseHelper helper;
    private int section = 0;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: startActivityForResult ");
        sf.setWebNewsActOpen(false);
        setResult(Global.NEWS_ARTICLE);
        Global.isArticleViewed = false;
        //Do not finish if activity is ReCreated
        if(!Global.isRelatedArticle) finish();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sf = new SharedPrefs(this);

        if(sf.getNightMode())setTheme(R.style.DarkTheme);
        else setTheme(R.style.LightTheme);
        setContentView(R.layout.web_news_view_new);

        helper = new DatabaseHelper(this);
        sf.setWebNewsActOpen(true);
        apiService = ServiceGenerator.createService(ApiService.class);

        try{
            if (Build.VERSION.SDK_INT < VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        }catch(Exception e){
            Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
        }

        /**Story Reads Counts **/
        if(sf.isNewUser()){  //default value of isNewUser is TRUE
            int count =  sf.getStoryReadsCount() + 1;
            sf.setStoryReadsCount(count);
        }

        try {
            initViews();
        } catch (JSONException e) {
            Log.d(TAG, "onCreate: JSONException"+e.getMessage());
        }
        initViewListeners();

//        getArticleView();

        configWebViewSettings();
        getArticleView();
        initFontSettings();
        checkIfFiveStoryReads();
        checkIfBookmarked(newsId); //after fetch
    }

    private void checkIfBookmarked(String newsId) {
        if(Global.bookmarkIdList.contains(Integer.valueOf(newsId))){
            imageBookmark.setImageResource(R.drawable.new_bookmark_fill);
        }else imageBookmark.setImageResource(R.drawable.new_bookmark_empty);
    }

    private void checkIfFiveStoryReads() {
        if(sf.getStoryReadsCount()>=5 && sf.isNewUser() && !sf.hasNotificationAllowed()){ //
            Log.d(TAG, "onCreate: read story count " + 5);
            sf.setStoryReadsCount(0);
            sf.setNewUser(false); //
            Global.NotificationPermission dialog = new Global.NotificationPermission(sf);
            dialog.showDialog(this,getResources().getString(R.string.notification_dialog));
        }
    }

    private void initFontSettings() {
        //Replace with SF
        String fontStr = sf.getFontString();
        if (fontStr.equals("0")) {
            fontSw = 0;
            webView.getSettings().setTextZoom(100);
        } else if (fontStr.equals("1")) {
            fontSw = 1;
            webView.getSettings().setTextZoom(110);
        } else if (fontStr.equals("2")) {
            fontSw = 2;
            webView.getSettings().setTextZoom(120);
        }
    }

    private void configWebViewSettings() {
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    public final class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Intent i = null;
            if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                i = new Intent(Intent.ACTION_VIEW, request.getUrl());
            }
            if(i!=null)startActivity(i);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            imageFont.setClickable(false);
            imageShare.setClickable(false);
            imageBookmark.setClickable(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            imageFont.setClickable(true);
            imageShare.setClickable(true);
            imageBookmark.setClickable(true);
            clArticleHolder.setVisibility(View.VISIBLE);
        }
    }

    private void generateWebArticle() {
        htmlStr = htmlStr + (sf.getNightMode() ? Global.NIGHT_MODE : Global.NORMAL_MODE); //style for BODY
    }

    private void initViewListeners() {
        imageBack.setOnClickListener(v -> {
            onBackPressed();
        });

        imageFont.setOnClickListener(v -> {
            if (fontSw == 0) {
                fontSw = 1;
                sf.setFontString("1");
                webView.getSettings().setTextZoom(110);
            } else if (fontSw == 1) {
                fontSw = 2;
                sf.setFontString("2");
                webView.getSettings().setTextZoom(120);
            } else if (fontSw == 2) {
                fontSw = 0;
                sf.setFontString("0");
                webView.getSettings().setTextZoom(100);
            }
        });

        imageShare.setOnClickListener(v -> {
           shareLink(newsId, newsType);
        });

        imageBookmark.setOnClickListener(v -> {
            imageBookmark.setEnabled(false);
            if (Build.VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                if(imageBookmark.getDrawable().getConstantState().equals(imageBookmark.getContext().getDrawable(R.drawable.new_bookmark_empty).getConstantState())){
                    addToBookmarkNew(Integer.valueOf(newsId));
                }else{
                    deleteOneBookmarkNew(Integer.valueOf(newsId));
                }
            }
            imageBookmark.setEnabled(true);
        });

    }

    private boolean isValidated(String string){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error isEmpty** ");
            try{
                Log.d(TAG, "isDisplayType01Validated: Exception_Error isNull** ");
                if(string!=null) return true;  //Check if null
            }catch(Exception ex){
                Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
            }
        }
        return false;
    }

    private void initViews() throws JSONException {
        webView = findViewById(R.id.webViewNew);
        clArticleHolder = findViewById(R.id.webViewNewHolder);
        imageFont = findViewById(R.id.imageFontNew);
        imageShare = findViewById(R.id.imageShareNew);
        imageBookmark = findViewById(R.id.imageBookmarkNew);
        imageBack = findViewById(R.id.imageBackNew);

        //Disable clicking until article was fully loaded
        imageFont.setClickable(false);
        imageShare.setClickable(false);
        imageBookmark.setClickable(false);
    }

    /**NEW ADDITIONAL NewDesign*/
    private void shareLink(String id, String type){
        Log.d(TAG, "shareLink: viewArticleNew TYPE: "+type);
        String hashed = Global.Generate32SHA512(sf.getDailyKey()  +"inq" + sf.getDeviceId() + "-megashare/" + id);
        apiService.getShareLink(sf.getDeviceId(), id, type, hashed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(String link) {

                        hasNewDailyKey = false;
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_TEXT, newsTitle + "\nvia Inquirer Mobile: " + link);

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Share with..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(WebNewsActivity.this, "There is no share client installed.", Toast.LENGTH_SHORT).show();
                        }
                        Log.d(TAG, "onSuccess: LINK_SHARE **** "+ link);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getDailyKey();
                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)) toastMessage(TAG+": onError: "+ e.getMessage()); // //Display in developer
                        Log.d(TAG, "onError: shareLink "+ e.getMessage());
                    }
                });
    }

    /** Records of seconds stay in article view **/
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            timeCounter++;
            customHandler.postDelayed(this, 2000);
        }
    };

    private void registerTimeArticleReads(String deviceId, String newsId, int seconds, String password){
        apiService.registerTimeArticleReads(deviceId, newsId, seconds, password)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseBody responseBody) {
                        try {
                            String r = responseBody.string();
                            if(r.contains("Store Successfully")){///
                                Log.d(TAG, "onSuccess: registerTimeArticleReads Successfully");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: registerTimeArticleReads error  getMessage" + e.getMessage());
                    }
                });
    }

    @Override
    public void onResume() {
        timeCounter=0;
        customHandler.postDelayed(updateTimerThread, 2000);  //calling handler runnable
        super.onResume();
    }

    @Override
    public void onPause() {
        customHandler.removeCallbacks(updateTimerThread);  //stop handler runnable
        sf.setStayInArticle(timeCounter);
        super.onPause();
    }

    @Override
    public void onStart() {
        customHandler.postDelayed(updateTimerThread, 2000);  //calling handler runnable
        Log.d(TAG, "onStart: WebNewsActivityLC" );
        super.onStart();
    }

    @Override
    public void onStop() {
        Global.isItemClicked=false;
        customHandler.removeCallbacks(updateTimerThread);  //stop handler runnable
        if(sf.getStayInArticle()>=1){
            registerTimeArticleReads(sf.getDeviceId(), newsId, sf.getStayInArticle(),Global.passStoreUserReadTime);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            if(!Global.isRelatedArticle)finish(); //Do not finish if activity is ReCreated //RESOLVED:
        }catch(Exception e){
            Log.d(TAG, "onStop: Exception_ERROR"+ e.getMessage());
        }
    }

    /**New Additional NewDesign*/
    public void addToBookmarkNew(int newsId) {
        sf.setLastBookmarkCount(sf.getLastBookmarkCount()+1);
//        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, sf.getLastBookmarkCount(),title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectionName);
        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, sf.getLastBookmarkCount(),newsType, newsPhoto, newsTitle,newsDate, newsContent);
        helper.saveBookmark(bookmarkModel);
        imageBookmark.setImageResource(R.drawable.new_bookmark_fill);
        toastMessage("This article is added to your bookmark.");
    }

    public void deleteOneBookmarkNew(int newsId) {
        helper.deleteOneBookmark(newsId);
        imageBookmark.setImageResource(R.drawable.new_bookmark_empty);
        toastMessage("Bookmark deleted.");
    }

    public void toastMessage(String message) {
        if(Global.toast!=null) Global.toast.cancel(); //Remove First the existing toast
        Global.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT); Global.toast.show(); //Show NEW TOAST
    }


    private void getArticleView() {
        if(getIntent().hasExtra("fromDeepLink")){ htmlStr = getIntent().getStringExtra("fromDeepLink");
        } else {
            htmlStr = getIntent().getStringExtra("viewArticleNow");
            newsId = getIntent().getStringExtra("newsId");
            newsType = getIntent().getStringExtra("newsType");
            newsPhoto = getIntent().getStringExtra("newsPhoto");
            newsTitle = getIntent().getStringExtra("newsTitle");
            newsDate = getIntent().getStringExtra("newsDate");
            newsContent = getIntent().getStringExtra("viewArticleNow");

        }
        webView.loadDataWithBaseURL("http:///android_asset/", htmlStr, "text/html", null, null);
    }

    private void getDailyKey(){
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    hasNewDailyKey = true;
                    sf.setDailyKey(response.body());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }
}