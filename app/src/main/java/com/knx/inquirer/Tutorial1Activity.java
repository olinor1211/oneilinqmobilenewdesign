package com.knx.inquirer;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.util.Log;
import android.widget.ImageView;

import com.knx.inquirer.utils.SharedPrefs;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Tutorial1Activity extends AppCompatActivity {
    private static final String TAG = "Tutorial1Activity";

    private GestureDetectorCompat detector;

    private ImageView imgClose;

    private SharedPrefs sf;

    private float x1,x2;
    static final int MIN_DISTANCE = 150;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    //    }
    //Calligraphy3 ADDED for Tablet Android10
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tutorial_pager_view);

        try{
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }else{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        }catch(Exception e){
            Log.d(TAG, "onCreate: Exception_ERROR: "+e.getMessage());
        }

        sf = new SharedPrefs(this);

        imgClose = findViewById(R.id.ivClose);

        imgClose.setOnClickListener(v -> {

            sf.setTutViewed(true);

            Intent intent = new Intent();
            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.MainActivity");
            startActivity(intent);
            finish();

        });

        //PrimeGlobal.WriteFile("yes" , Environment.getExternalStorageDirectory().getAbsolutePath() + "/inquirer/tutorial1.txt");
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }

    }

    public void onBackPressed() {

        sf.setTutViewed(true);

        Intent intent = new Intent();
        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.MainActivity");
        startActivity(intent);
        finish();
    }

}
		

