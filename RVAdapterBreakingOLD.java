package com.knx.inquirer.rvadapters;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.fragments.FragmentBreaking;
import com.knx.inquirer.models.NewsModel;
import com.knx.inquirer.models.PostArticleBreaking;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.PlayerWebView;
import com.knx.inquirer.utils.SharedPrefs;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.greenrobot.eventbus.EventBus;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class RVAdapterBreakingOLD extends RecyclerView.Adapter {

    private static final String TAG = "RVAdapterBreaking";
    private AudioManager amanager;
    private int sect;

    private ArrayList<NewsModel> news;
    private Context context;
    private FragmentBreaking fragment;
    private SharedPrefs sf;
    private String _id, photo, type, title, pubdate, label;
    private List<String> imgItems;
//    private int itemposition;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_IMAGE = 2;
    private final int VIEW_TYPE_VIDEO = 3;
    private final int VIEW_TYPE_CAROUSEL = 4;
    private final int VIEW_TYPE_IMAGE_DOUBLE = 5;
    private final int VIEW_TYPE_VIDEO_DOUBLE = 6;
    private final int VIEW_TYPE_VIDEO_ARTICLE = 7;
    private final int VIEW_TYPE_DM = 8;
    private final int VIEW_TYPE_INQPRIME = 9;
    private final int VIEW_TYPE_DM_LINK = 10;
//    private final int VIEW_TYPE_PODCAST11 = 110;
//    private final int VIEW_TYPE_PODCAST12 = 112;
//    private final int VIEW_TYPE_PODCAST13 = 113;
//    private final int VIEW_TYPE_PODCAST14 = 114;
//    private final int VIEW_TYPE_PODCAST10 = 115;

    //TYPE
    private final String NEWS_ITEM_0 = "0";
    private final String NEWS_ITEM_1 = "1";
    private final String IMAGE = "2";
    private final String VIDEO = "3";
    private final String CAROUSEL = "4";
    private final String IMAGE_DOUBLE = "5";
    private final String VIDEO_DOUBLE = "6";
    private final String VIDEO_ARTICLE = "7";
    private final String DAILY_MOTION = "8";
    private final String INQ_PRIME_TAG = "9";
    private final String DAILY_MOTION_LINK = "10";

    public DMTypeViewHolder  dmTypeViewHolder;

//    private final String PODCAST0 = "110";
//    private final String PODCAST1 = "111";
//    private final String PODCAST2 = "112";
//    private final String PODCAST3 = "113";
//    private final String PODCAST4 = "114";

    private OnLoadMoreListener loadMoreListener;  //declaring the interface inside this class
    private boolean isLoading = false, isMoreDataAvailable = true;

    //test
    private  Boolean hasImageType2 = false;
    private Boolean isVideoItemRemoved = null;
//    private int videoPosition;
    private SimpleExoPlayer exoPlayer, exoPlayer2, exoPlayer3;
    private PlayerWebView mVideoView;

    private int dmScreen;

    private ViewGroup.LayoutParams dmLayoutParams;

    private DatabaseHelper helper;
    private long mLastClickTime = 0;
    private Instant GlideApp;


    public RVAdapterBreakingOLD(Context context, ArrayList<NewsModel> news, FragmentBreaking fragment, SharedPrefs sf) {
        super();
        this.news = news;
        this.context = context;
        this.fragment = fragment;
        this.sf = sf;
        this.helper = new DatabaseHelper(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;   //
//        RecyclerView.ViewHolder vh2;   //

//        if(!isFirstCalled){
//            isFirstCalled = true;
//            View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.podcast_list_items, parent, false);
//////        new PodcastTypeViewHolder3(v2);
//            vh = new PodcastTypeViewHolder(v2);
////            return vh;
//        }

        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_row, parent, false);
            vh = new ItemViewHolder(v);  //calling instance of viewHolder
        }else if(viewType == VIEW_TYPE_IMAGE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_items, parent, false);
            vh = new ImageTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_list_items, parent, false);
            vh = new VideoTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_CAROUSEL) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carousel_list_items, parent, false);
            vh = new CarouselTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_IMAGE_DOUBLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_double_list_items, parent, false);
            vh = new ImageDoubleViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO_DOUBLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_double_list_items, parent, false);
            vh = new VideoDoubleTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_VIDEO_ARTICLE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_article_list_items, parent, false);
            vh = new VideoArticleTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_DM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_motion_list_items, parent, false);
//             dmLayoutParams = v.getLayoutParams(); //screen size
//            layoutParams.height = (int) (parent.getHeight() * .3);     //screen size .3
//            layoutParams.height = (int) (parent.getHeight() / 3);     //screen size  / 3  == 521
//            layoutParams.height = (int) (parent.getHeight());     //screen size  == 1563
//            layoutParams.height    //screen size  == -2
//            int screen =  (int) (parent.getHeight() * .3);  //468
//            int screen2 =  (int) (parent.getHeight() * .6);  //937
//            int screen3 =  (int) (parent.getHeight() * .9);  //1406
//             dmScreen =  (int) (parent.getHeight() * .3);  //468

//            int vis = v.getVisibility();
//            int vis2 = v.getWindowVisibility();
//
//            Log.d(TAG, "onCreateViewHolder: vis " + vis);
//            Log.d(TAG, "onCreateViewHolder: vis " + vis2);

            //Orig
            dmTypeViewHolder = new DMTypeViewHolder(v);
            vh = new DMTypeViewHolder(v);


        }else if(viewType == VIEW_TYPE_DM_LINK) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_motion_link_list_items, parent, false);
            vh = new DMLinkTypeViewHolder(v);

        }else if(viewType == VIEW_TYPE_INQPRIME) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inqprime_list_row, parent, false);
            vh = new InqPrimeTypeViewHolder(v);

        }
        else  {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }

        return vh;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,  int position) {

        if(position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }
        if (!(holder instanceof ProgressViewHolder)) {
            _id = news.get(position).getId();
            photo = news.get(position).getImage();
            type = news.get(position).getType();
            title = news.get(position).getTitle();
            pubdate = news.get(position).getPubdate();
            label = news.get(position).getLabel();

            Log.d(TAG, "onBindViewHolder: LAST_SECTION TYPE: "+ type);
            Log.d(TAG, "onBindViewHolder: LAST_SECTION: ID "+ sf.getLastSection());


            switch (sf.getLastSection()){
                case 2:  sect = R.drawable.inquirersports;
                    break;
                case 3:  sect = R.drawable.inquirerentertainment;
                    break;
                case 5:  sect = R.drawable.inquirertechnology;
                    break;
                case 6:  sect = R.drawable.inquirerglobalnation;
                    break;
                case 7:  sect = R.drawable.inquirerbusiness;
                    break;
                case 8:  sect = R.drawable.inquirerlifestyle;
                    break;
                case 12:  sect = R.drawable.inquirerproperty;
                    break;
                case 13:  sect = R.drawable.inquireropinion_blue;
                    break;
                case 16:  sect = R.drawable.inquirerboardtalk;
                    break;
                case 17:  sect = R.drawable.inquirermobility;
                    break;
                case 19:  sect = R.drawable.inquirerrebound;
                    break;
                default: sect = R.drawable.inquirernewsinfo;
            }

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(sect)
                    .error(sect)
                    //.skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .priority(Priority.HIGH);

            Log.d(TAG, "onBindViewHolder: LAST_SECTION: "+ sf.getLastSection());
            switch (type) {
//                case NEWS_ITEM_0: //0
//                    TextView newsTitle = ((ItemViewHolder) holder ).newstitle;
//                    TextView newsDate = ((ItemViewHolder) holder ).newspubdate;
//                    ImageView newsPhoto = ((ItemViewHolder) holder ).newsphoto;
//                    displayType01(options,_id,title,pubdate,photo,newsTitle,newsDate,newsPhoto);
//                    break;
                case NEWS_ITEM_0:
                case NEWS_ITEM_1: //1
                    Log.d(TAG, "IMAGE/VIDEO_TYPE : "+type);
                    Log.d(TAG, "IMAGE/VIDEO_TYPE : "+photo);
                    checkIfBookmarked(((ItemViewHolder) holder), news.get(holder.getAdapterPosition()).getId());
                    TextView newsTitle1 = ((ItemViewHolder) holder ).newstitle;
                    TextView newsDate1 = ((ItemViewHolder) holder ).newspubdate;
                    ImageView newsPhoto1 = ((ItemViewHolder) holder ).newsphoto;
                    displayType01(options,_id,title,pubdate,photo,newsTitle1,newsDate1,newsPhoto1); //pubdate here
                    break;

                case IMAGE: //Image 2
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    TextView adTitle2 = ((ImageTypeViewHolder) holder ).textImageTitle;
                    TextView adSponsor2= ((ImageTypeViewHolder) holder ).textImageSponsored;
                    ImageView adPhoto2 = ((ImageTypeViewHolder) holder ).imageOrdinaryPhoto;
                    displayType25(options,_id ,title,label,photo,adTitle2,adSponsor2,adPhoto2); //label here
                    break;

                case VIDEO: //Video 3
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    isVideoItemRemoved = true;

                    //androidx
                    if (isValidated(news.get(position).getTitle())) {  //if (news.get(position).getTitle().isEmpty()) {
                        ( (VideoTypeViewHolder) holder ).video3Title.setText(news.get(position).getTitle());
                        ( (VideoTypeViewHolder) holder ).video3Title.setVisibility(VISIBLE);
                    } else( (VideoTypeViewHolder) holder ).video3Title.setVisibility(INVISIBLE);




                    if (isValidated(news.get(position).getLabel())) {
                        ( (VideoTypeViewHolder) holder ).video3Sponsored.setText(news.get(position).getLabel());
                        ( (VideoTypeViewHolder) holder ).video3Sponsored.setVisibility(VISIBLE);

                    } else ((VideoTypeViewHolder) holder ).video3Sponsored.setVisibility(INVISIBLE);

                    if (isValidated(photo)) {
                        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                        exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

                        DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                        ( (VideoTypeViewHolder) holder ).newsVideo.setUseController(false);  //play, stop
                        ( (VideoTypeViewHolder) holder ).newsVideo.setPlayer(exoPlayer);
                        ( (VideoTypeViewHolder) holder ).newsVideo.setShowBuffering(true);

                        exoPlayer.prepare(mediaSource);
                        exoPlayer.setPlayWhenReady(true);
                        exoPlayer.setVolume(0f);
//                        exoPlayer.setRepeatMode(exoPlayer.REPEAT_MODE_ONE);
                    }
                    break;



                case CAROUSEL:  // Carousel 4
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    String images = news.get(position).getImage();
                    String imagesUrl[] = images.split(",");

                    if (( (CarouselTypeViewHolder)holder ).carouselView !=null ){
                        ImageListener imageListener = (position1, imageView) -> {
                            //GlideApp
                                     Glide
                                    .with(context)
                                    .load(imagesUrl[position1])
                                    .transition(withCrossFade())
                                    .apply(options)
                                    .into(imageView);
                            ViewCompat.setTransitionName(imageView, String.valueOf(_id)); //setTransitionName
                        };
                        ( (CarouselTypeViewHolder)holder ).carouselView.setPageCount(imagesUrl.length);
                        ( (CarouselTypeViewHolder)holder ).carouselView.setImageListener(imageListener);

                        //androidx
                        if(isValidated(news.get(position).getLabel())){
                            ( (CarouselTypeViewHolder)holder ).textCarouselTag.setText(news.get(position).getLabel());
                            ( (CarouselTypeViewHolder)holder ).textCarouselTag.setVisibility(INVISIBLE);
                        }else{( (CarouselTypeViewHolder)holder ).textCarouselTag.setVisibility(INVISIBLE);}

                        //androidx
                        if(isValidated(news.get(position).getTitle())){
                            ( (CarouselTypeViewHolder)holder ).textCarouselTitle.setText(news.get(position).getTitle());
                            ( (CarouselTypeViewHolder)holder ).textCarouselTitle.setVisibility(VISIBLE);

                        }else{( (CarouselTypeViewHolder)holder ).textCarouselTitle.setVisibility(INVISIBLE);}

                    }
                    break;

                case IMAGE_DOUBLE: //ImageDouble  5
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    TextView adTitle5 = ((ImageDoubleViewHolder) holder ).textDoubleTitle;
                    TextView adSponsor5= ((ImageDoubleViewHolder) holder ).textDoubleSponsored;
                    ImageView adPhoto5 = ((ImageDoubleViewHolder) holder ).imageDoubePhoto;
                    displayType25(options, _id,title,label,photo,adTitle5,adSponsor5,adPhoto5); //label here
                    break;

                case VIDEO_DOUBLE: //VideoDouble 6
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
//
                    if (isValidated(news.get(position).getTitle())) {
                        ((VideoDoubleTypeViewHolder) holder).video6Title.setText(news.get(position).getTitle());
                        ((VideoDoubleTypeViewHolder) holder).video6Title.setVisibility(VISIBLE);
                    } else ((VideoDoubleTypeViewHolder) holder).video6Title.setVisibility(INVISIBLE);


                    if (isValidated(news.get(position).getLabel())) {
                        ((VideoDoubleTypeViewHolder) holder).video6Sponsored.setText(news.get(position).getLabel());
                        ((VideoDoubleTypeViewHolder) holder).video6Sponsored.setVisibility(VISIBLE);
                    } else ((VideoDoubleTypeViewHolder) holder).video6Sponsored.setVisibility(INVISIBLE);

                    if (isValidated(photo)) {
                        BandwidthMeter bandwidthMeter2 = new DefaultBandwidthMeter();
                        TrackSelector trackSelector2 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter2));
                        exoPlayer2 = ExoPlayerFactory.newSimpleInstance(context, trackSelector2);

                        DefaultHttpDataSourceFactory dataSourceFactory2 = new DefaultHttpDataSourceFactory("exoplayer_video2");
                        MediaSource mediaSource2 = new ExtractorMediaSource.Factory(dataSourceFactory2).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                        ( (VideoDoubleTypeViewHolder) holder ).newsVideoDouble.setUseController(false);  //play, stop
                        ( (VideoDoubleTypeViewHolder) holder ).newsVideoDouble.setPlayer(exoPlayer2);
                        ( (VideoDoubleTypeViewHolder) holder ).newsVideoDouble.setShowBuffering(true);

                        exoPlayer2.prepare(mediaSource2);
                        exoPlayer2.setPlayWhenReady(true);
                        exoPlayer2.setVolume(0f);
//                        exoPlayer2.setRepeatMode(exoPlayer.REPEAT_MODE_ONE);
                    }
                    break;

                case VIDEO_ARTICLE: //Video Article  7
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);

                    isVideoItemRemoved = true;
//
                    if (isValidated(news.get(position).getTitle())) {
                        ( (VideoArticleTypeViewHolder) holder ).video7Title.setText(news.get(position).getTitle());
                        ( (VideoArticleTypeViewHolder) holder ).video7Title.setVisibility(VISIBLE);
                    } else ( (VideoArticleTypeViewHolder) holder ).video7Title.setVisibility(INVISIBLE);

                    if (isValidated(news.get(position).getLabel())) {
                        ( (VideoArticleTypeViewHolder) holder ).video7Sponsored.setText(news.get(position).getLabel());
                        ( (VideoArticleTypeViewHolder) holder ).video7Sponsored.setVisibility(VISIBLE);
                    } else ( (VideoArticleTypeViewHolder) holder ).video7Sponsored.setVisibility(INVISIBLE);

                    if (isValidated(photo)) {
                        BandwidthMeter bandwidthMeter3 = new DefaultBandwidthMeter();
                        TrackSelector trackSelector3 = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter3));
                        exoPlayer3 = ExoPlayerFactory.newSimpleInstance(context, trackSelector3);

                        DefaultHttpDataSourceFactory dataSourceFactory3 = new DefaultHttpDataSourceFactory("exoplayer_video3");
                        MediaSource mediaSource3 = new ExtractorMediaSource.Factory(dataSourceFactory3).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(photo));

                        ( (VideoArticleTypeViewHolder) holder ).newsVideoArticle.setUseController(false);  //play, stop
                        ( (VideoArticleTypeViewHolder) holder ).newsVideoArticle.setPlayer(exoPlayer3);
                        ( (VideoArticleTypeViewHolder) holder ).newsVideoArticle.setShowBuffering(true);

                        exoPlayer3.prepare(mediaSource3);
                        exoPlayer3.setPlayWhenReady(true);
                        exoPlayer3.setVolume(0f);
//                        exoPlayer3.setRepeatMode(exoPlayer3.REPEAT_MODE_ONE);
                    }
                    break;

                case DAILY_MOTION: //Dailymotion  8
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    //ADDED CHECKING OF VIDEO PLAYING WITH SOUND IN EVENT_PLAY
//                    Map<String, String> dmParams = new HashMap<>();
//                    dmParams.put("syndication", Global.syndicationKey);
//                    dmParams.put("app", Global.appPackageName);
//                    dmParams.put("api", "nativeBridge");

                    //USE Gradient ImgView as the TOP DISPLAY while waiting the VIDEO to Prepare
                    ImageView imageView = ( (DMTypeViewHolder) holder ).gradient;  //
                    imageView.setVisibility(VISIBLE);
                    imageView.setImageResource(sect);// Using the section image as the TOP DISPLAY

                    //androidx
                    if(isValidated(news.get(position).getTitle())){
                        ( (DMTypeViewHolder) holder ).textDmTitle.setText(news.get(position).getTitle());
                        ( (DMTypeViewHolder) holder ).textDmTitle.setVisibility(VISIBLE);
                    }else{
                        ( (DMTypeViewHolder) holder ).textDmTitle.setVisibility(INVISIBLE);
                    }
                    //androidx
                    if(isValidated(news.get(position).getLabel())){
                        ( (DMTypeViewHolder) holder ).textDmSponsored.setText(news.get(position).getLabel());
                        ( (DMTypeViewHolder) holder ).textDmSponsored.setVisibility(VISIBLE);
                    }else{
                        ( (DMTypeViewHolder) holder ).textDmSponsored.setVisibility(INVISIBLE);
                    }

                    //androidx
                    String vidUrl = news.get(position).getImage();
                    //androidx
                    if(isValidated(vidUrl)){
                        try{
                            int li = vidUrl.lastIndexOf('/')+1; //Get only the DM_Video_ID
                            vidUrl = vidUrl.substring(li);
                            ((DMTypeViewHolder) holder).dmPlayerWebView.showControls(true);
                            ((DMTypeViewHolder) holder).dmPlayerWebView.mute(); //no param
                            ((DMTypeViewHolder) holder).dmPlayerWebView.setPlayWhenReady(true); //true
                            ((DMTypeViewHolder) holder).dmPlayerWebView.load(vidUrl); //dmParams
                            setDelayDisplayThumbnail(imageView);  //DISAPPEARING the TOP DISPLAY/TOP_GRADIENT in 1secs..
                        }catch(Exception e){
                            Log.d(TAG, "onBindViewHolder: Exception_Error: DM "+ e.getMessage());
                        }

                    }else{
                        displayDefaultImage(imageView);
                    }
                    break;

                case INQ_PRIME_TAG:  //9
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    TextView newsTitle9 = ((InqPrimeTypeViewHolder) holder ).newsTitleInqPrime;
                        TextView newsDate9 = ((InqPrimeTypeViewHolder) holder ).newsPubdateInqPrime;
                        ImageView newsPhoto9 = ((InqPrimeTypeViewHolder) holder ).mImagePhotoInqPrime;
                        displayType01(options,_id,title,pubdate,photo,newsTitle9,newsDate9,newsPhoto9); //pubdate here
                        break;

                case DAILY_MOTION_LINK: //DailymotionLink  10
                    Log.d(TAG, "IMAGE/VIDEO TYPE : "+type);
                    //USE sect as the TOP DISPLAY while waiting the VIDEO to Prepare
                    ImageView imageView1 = ( (DMLinkTypeViewHolder) holder ).gradient;  //
                    imageView1.setImageResource(sect);// Using the section image as the TOP DISPLAY

                    //androidx
                    if(isValidated(news.get(position).getTitle())){
                        ( (DMLinkTypeViewHolder) holder ).textDmTitle.setText(news.get(position).getTitle());
                        ( (DMLinkTypeViewHolder) holder ).textDmTitle.setVisibility(VISIBLE);
                    }else{
                        ( (DMLinkTypeViewHolder) holder ).textDmTitle.setVisibility(INVISIBLE);
                    }

                    //androidx
                    if(isValidated(news.get(position).getLabel())){
                        ( (DMLinkTypeViewHolder) holder ).textDmSponsored.setText(news.get(position).getLabel());
                        ( (DMLinkTypeViewHolder) holder ).textDmSponsored.setVisibility(VISIBLE);
                    }else{
                        ( (DMLinkTypeViewHolder) holder ).textDmSponsored.setVisibility(INVISIBLE);
                    }

                    //androidx
                    String videoUrl = news.get(position).getImage();
                    //androidx
                    if(isValidated(videoUrl)){
                        try{
                            int li = videoUrl.lastIndexOf('/')+1; //Get only the DM_Video_ID
                            vidUrl = videoUrl.substring(li);
                            ((DMLinkTypeViewHolder) holder).dmPlayerWebView.showControls(true);
                            ((DMLinkTypeViewHolder) holder).dmPlayerWebView.mute(); //no param
                            ((DMLinkTypeViewHolder) holder).dmPlayerWebView.setPlayWhenReady(true); //true
                            ((DMLinkTypeViewHolder) holder).dmPlayerWebView.load(videoUrl); //dmParams
                            imageView1.setImageResource(R.drawable.no_color); //Return no_color as the TOP DISPLAY
                        }catch(Exception e){
                            Log.d(TAG, "onBindViewHolder: Exception_Error: DM "+ e.getMessage());
                        }

                    }else{
                        displayDefaultImage(imageView1);
                    }
                    break;
            }

        }else{
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }



    //called FIRST before onCreateViewHolder ...getItemViewType
    @Override
    public int getItemViewType(int position) {

        if (news.get(position) != null) {
            String type = news.get(position).getType(); //checking the type
            switch(type){
                case NEWS_ITEM_0:
                    return VIEW_TYPE_ITEM;
                case NEWS_ITEM_1:
                    Log.d(TAG, "getItemViewType: News " + type);
                    return VIEW_TYPE_ITEM;

                case IMAGE:
                    Log.d(TAG, "getItemViewType: Image " + type);
                    return VIEW_TYPE_IMAGE;

                case VIDEO:
                    Log.d(TAG, "getItemViewType: Video " + type);
                    return VIEW_TYPE_VIDEO;

                case CAROUSEL:
                    Log.d(TAG, "getItemViewType: Carousel " + type);
                    return VIEW_TYPE_CAROUSEL;

                case IMAGE_DOUBLE:
                    Log.d(TAG, "getItemViewType: ImageDouble " + type);
                    return VIEW_TYPE_IMAGE_DOUBLE;

                case VIDEO_DOUBLE:
                    Log.d(TAG, "getItemViewType: VideoDouble " + type);
                    return VIEW_TYPE_VIDEO_DOUBLE;

                case VIDEO_ARTICLE:
                    Log.d(TAG, "getItemViewType: VideoArticle " + type);
                    return VIEW_TYPE_VIDEO_ARTICLE;

                case DAILY_MOTION:
                    Log.d(TAG, "getItemViewType: DailyMotion " + type);
                    return VIEW_TYPE_DM;

                case INQ_PRIME_TAG:
                    Log.d(TAG, "getItemViewType: InqPrime " + type);
                    return VIEW_TYPE_INQPRIME;

                case DAILY_MOTION_LINK:
                    Log.d(TAG, "getItemViewType: DailyMotionLink"+ type);
                    return VIEW_TYPE_DM_LINK;

                default:
                    return -1;

            }

        }else {
            return VIEW_TYPE_LOADING;
        }
//        return news.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return news == null ? 0 : news.size();
    }

    //test ImageTypeViewHolder
    public class ImageTypeViewHolder extends RecyclerView.ViewHolder {
        private TextView textImageTitle, textImageSponsored;
        private ImageView imageOrdinaryPhoto;

        public ImageTypeViewHolder(@NonNull View itemView) {
            super(itemView);

            imageOrdinaryPhoto = itemView.findViewById(R.id.imageOrdinaryPhoto);
            textImageSponsored = itemView.findViewById(R.id.textImageSponsored);
            textImageTitle = itemView.findViewById(R.id.textImageTitle);

            itemView.setOnClickListener(v -> {
                Log.d(TAG, "ImageTypeViewHolder: TYPE_CLICKED: "+type);
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        int n = getAdapterPosition();
                        String link = news.get(n).getLink();
                        fragment.checkImageLink(link);
                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    //test VideoTypeViewHolder
    public class VideoTypeViewHolder extends RecyclerView.ViewHolder {

          PlayerView newsVideo;
          TextView video3Title, video3Sponsored;
          ImageView video3Background;
//          RelativeLayout video3Layout;


        public VideoTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideo = itemView.findViewById(R.id.news_video);
            this.video3Sponsored = itemView.findViewById(R.id.video3Sponsored);
            this.video3Title = itemView.findViewById(R.id.video3Title);
            this.video3Background = itemView.findViewById(R.id.video3imageGradient);
//            this.video3Layout = itemView.findViewById(R.id.rlVideo3);

            itemView.setOnClickListener(v -> {
                Log.d(TAG, "CLICKED TYPE : "+type);

                int n = getAdapterPosition();
                String link = news.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        fragment.checkVideoLink(link);
                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }


    // VideoDoubleTypeViewHolder
    public class VideoDoubleTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideoDouble;
        TextView video6Title, video6Sponsored;

        public VideoDoubleTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideoDouble = itemView.findViewById(R.id.news_video6);
            this.video6Sponsored = itemView.findViewById(R.id.video6Sponsored);
            this.video6Title = itemView.findViewById(R.id.video6Title);

            itemView.setOnClickListener(v -> {
                Log.d(TAG, "CLICKED TYPE : "+type);
                int n = getAdapterPosition();
                String link = news.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        fragment.checkVideoLink(link);
                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }

    // VideoArticleTypeViewHolder
    public class VideoArticleTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerView newsVideoArticle;
        TextView video7Title, video7Sponsored;

        public VideoArticleTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.newsVideoArticle = itemView.findViewById(R.id.news_video7);
            this.video7Sponsored = itemView.findViewById(R.id.video7Sponsored);
            this.video7Title = itemView.findViewById(R.id.video7Title);

            itemView.setOnClickListener(v -> {
                Log.d(TAG, "CLICKED TYPE : "+type);
                int n = getAdapterPosition();
                String videoUrl = news.get(n).getImage();
                if (Global.isNetworkAvailable(context)) {
                    if (!Global.isItemClicked) {
                        Global.isItemClicked = true;
                        Global.isArticleViewed = false; // //1RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
//                        EventBus.getDefault().post(new PostArticleBreaking(news.get(n).getId())); //
                        EventBus.getDefault().post(new PostArticleBreaking(news.get(n).getId(),type)); //

                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }

    //podcast
//    private void checkVideoSoundPlaying() {
//        AudioManager mAudioManager = (AudioManager) context.getSystemService(AUDIO_SERVICE);
//        if (mAudioManager.isMusicActive()) {
//            EventBus.getDefault().post(new PostPodcastPlaybackButton(0, "pause")); //CHANGING playBUTTON in frAGMENT
//        }
//    }


    //test CarouselTypeViewHolder
    public class CarouselTypeViewHolder extends RecyclerView.ViewHolder {

        private CarouselView carouselView;
        private TextView textCarouselTag, textCarouselTitle;

        public CarouselTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.carouselView = itemView.findViewById(R.id.carouselView);
            this.textCarouselTag = itemView.findViewById(R.id.textCarouselTag);
            this.textCarouselTitle = itemView.findViewById(R.id.textCarouselTitle);

            carouselView.setImageClickListener(position -> {
                Log.d(TAG, "CLICKED TYPE : "+type);
                int n = getAdapterPosition();
                String link = news.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    Log.d(TAG, "CarouselTypeViewHolder: has clicked ");
                    if (!Global.isItemClicked) {
                        Global.isItemClicked = true;
                        Global.isArticleViewed = false; // //2RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
//                        EventBus.getDefault().post(new PostArticleBreaking(news.get(n).getId())); //
                        EventBus.getDefault().post(new PostArticleBreaking(news.get(n).getId(),type)); //

                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }


    //test ImageDoubleViewHolder
    public class ImageDoubleViewHolder extends RecyclerView.ViewHolder {
        private TextView textDoubleTitle, textDoubleSponsored;
        private ImageView imageDoubePhoto;

        public ImageDoubleViewHolder(@NonNull View itemView) {
            super(itemView);

            imageDoubePhoto = itemView.findViewById(R.id.imageDoublePhoto);
            textDoubleSponsored = itemView.findViewById(R.id.textDoubleSponsored);
            textDoubleTitle = itemView.findViewById(R.id.textDoubleTitle);

            itemView.setOnClickListener(v -> {
//                releasePlayer();
                Log.d(TAG, "CLICKED TYPE : "+type);
                int n = getAdapterPosition();
                String link = news.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        fragment.checkImageLink(link);
                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }


    //DMTypeViewHolder
    public class DMTypeViewHolder extends RecyclerView.ViewHolder {

        PlayerWebView dmPlayerWebView;
        TextView textDmSponsored, textDmTitle;
        ImageView gradient;

        public DMTypeViewHolder(@NonNull View itemView) {

            super(itemView);
            this.dmPlayerWebView = itemView.findViewById(R.id.dmPlayerWebView);
            this.textDmSponsored = itemView.findViewById(R.id.textDmSponsored);
            this.textDmTitle = itemView.findViewById(R.id.textDmTitle);
            this.gradient = itemView.findViewById(R.id.imageDmGradient);

            gradient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gradient.setVisibility(INVISIBLE);
                    Log.d(TAG, "onClick: CLICKED");
                }
            });
        }

        public PlayerWebView getDmPlayerWebView(){
            return dmPlayerWebView;
        }
    }

    // DMLinkTypeViewHolder
    public class DMLinkTypeViewHolder extends RecyclerView.ViewHolder {
        PlayerWebView dmPlayerWebView;
        TextView textDmSponsored, textDmTitle;
        ImageView gradient;
        public DMLinkTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            this.dmPlayerWebView = itemView.findViewById(R.id.dmLinkPlayerWebView);
            this.textDmSponsored = itemView.findViewById(R.id.textDmLinkSponsored);
            this.textDmTitle = itemView.findViewById(R.id.textDmLinkTitle);
            this.gradient = itemView.findViewById(R.id.imageDmLinkGradient);

            gradient.setOnClickListener(v -> {  // gradient.setOnClickListener(v -> {
                Log.d(TAG, "CLICKED TYPE : "+type);
                int n = getAdapterPosition();
                String link = news.get(n).getLink();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked =true;
                        fragment.checkVideoLink(link);
                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }

            });

        }
    }

    //InqPrimeTypeViewHolder
    private class InqPrimeTypeViewHolder extends RecyclerView.ViewHolder {
        //Views
        TextView newsTitleInqPrime, newsPubdateInqPrime, tagInqPrime;
        ImageView mPrimeTag, mImagePhotoInqPrime;

        public InqPrimeTypeViewHolder(View itemView) {
            super(itemView);

            newsTitleInqPrime = itemView.findViewById(R.id.textTitleInqPrime);
            newsPubdateInqPrime = itemView.findViewById(R.id.textDatesInqPrime);
            mImagePhotoInqPrime = itemView.findViewById(R.id.imagePhotoInqPrime);
            mPrimeTag = itemView.findViewById(R.id.primeTag);
            tagInqPrime = itemView.findViewById(R.id.tvTagInqPrime);

            itemView.setOnClickListener(v -> {
                Log.d(TAG, "InqPrimeTypeViewHolder: TYPE_CLICKED: " + type);
                int itemposition = getAdapterPosition();
                if (Global.isNetworkAvailable(context)) {
                    if(!Global.isItemClicked){
                        Global.isItemClicked = true;
                        Global.isArticleViewed = false; // //3RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
//                        EventBus.getDefault().post(new PostArticleBreaking(news.get(itemposition).getId())); //
                        EventBus.getDefault().post(new PostArticleBreaking(news.get(itemposition).getId(),type)); //

                    }
                }else {
                    Global.ViewDialog alert = new Global.ViewDialog();
                    alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
                }
            });
        }
    }

    //ItemViewHolder
    private class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView newstitle, newspubdate, tag;
        ImageView newsphoto, imageGradient,bookmark;
        ItemViewHolder(View itemView) {
            super(itemView);

            newstitle = itemView.findViewById(R.id.textTitle);
            newspubdate = itemView.findViewById(R.id.textDetails);
            newsphoto = itemView.findViewById(R.id.imagePhoto);
            imageGradient = itemView.findViewById(R.id.imageGradient);
            tag = itemView.findViewById(R.id.tvTag);
            bookmark = itemView.findViewById(R.id.ivNewsListBookmark);

            bookmark.setOnClickListener(this);
            newstitle.setOnClickListener(this); newspubdate.setOnClickListener(this);
            newsphoto.setOnClickListener(this);imageGradient.setOnClickListener(this);
            tag.setOnClickListener(this);
        }

//        @SuppressLint("UseCompatLoadingForDrawables")
        @Override
        public void onClick(View view) {
            // BESTWAY: Preventing multiple clicks, using threshold of 1500 second
            //BESTWAY: AND DONT FORGET if the case is GOING to OTHER Activity INTENT...to AVOID Multiple Display of that Activity TODO: android:launchMode="singleTask ON ACTIVITY ex: <activity android:name=".WebNewsActivity" android:launchMode="singleTask"
            //CHECKPOINT HERE: Avoid Multiple Viewing of Article
            if (( SystemClock.elapsedRealtime() - Global.lastClickTime) < 1000){
                Global.isItemClicked=true;
                Log.d(TAG, "onSuccess: onPauseBreaking AVOIDED");
            }else Global.isItemClicked=false;

            Global.lastClickTime =  SystemClock.elapsedRealtime();

            //NOTE: Global.isBookmark is CONTROLLER for ALL bookmarkImageView to NOT process until the previous is DONE processing
            int itemposition = getAdapterPosition();
            if (Global.isNetworkAvailable(context)) {
                switch (view.getId()){
                    case R.id.ivNewsListBookmark:
                        bookmark.setEnabled(false);
                        Log.d(TAG, "onClick: TAG_TAG1 "+Global.isBookmark);
                        if(!Global.isBookmark){
                            //NEED TRY CATCH: BUGS_ENCOUNTERED when SUPER_FAST clicking of bookmark icon
                            try{
                                Global.isBookmark = true;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    if(bookmark.getDrawable().getConstantState().equals(bookmark.getContext().getDrawable(R.drawable.new_bookmark_empty).getConstantState())){ //R.drawable.bookmark_prime
    //                                    if(bookmark.getDrawable().getConstantState()==context.getResources().getDrawable(R.drawable.bookmark_prime).getConstantState()){
                                        if(fragment.saveBookmark(news.get(itemposition).getId())){ //CALL API then SAVING BOOKMARK to DB
                                            bookmark.setImageResource(R.drawable.new_bookmark_fill); //bookmark.setImageResource(R.drawable.bookmark_fill);
                                            Log.d(TAG, "onClick: TAG_TAG ADD "+Global.isBookmark);
                                        }
                                    }else{
                                        deleteOneBookmark(bookmark, Integer.valueOf(Integer.valueOf(news.get(itemposition).getId())));
                                        Log.d(TAG, "onClick: TAG_TAG DELETE "+Global.isBookmark);
                                    }
                                }
                            }catch (Exception e){
                                Log.d(TAG, "BOOKMARK onClick: Exception_ERROR: "+e.getMessage());
                            }
                        }
                        Global.isBookmark = false;
                        Log.d(TAG, "onClick: TAG_TAG4 "+Global.isBookmark);
                        bookmark.setEnabled(true);
                        break;
                    default: //Global.isItemClicked = controller to avoid multiple clicking
                        if(!Global.isItemClicked){
                            Global.isItemClicked = true; //will be TRUE ONLY upon SUCCESS in API
                            Log.d(TAG, "onClick: TAG_TAG5 ARTICLE "+Global.isBookmark);
//                            Global.isArticleViewed = false; // //4RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
//                            EventBus.getDefault().post(new PostArticleBreaking(news.get(itemposition).getId())); //
                            EventBus.getDefault().post(new PostArticleBreaking(news.get(itemposition).getId(),type)); //
                         }
                        break;
                }

            }else {
                Global.ViewDialog alert = new Global.ViewDialog();
                alert.showDialog(context, "You are not connected to any network. Please connect to a WiFi or turn on your mobile data");
            }
        }
    }

//    <style name="AppTheme" parent="Theme.AppCompat.NoActionBar">
//            ...
//    <item name="android:splitMotionEvents">false</item>
//    <item name="android:windowEnableSplitTouch">false</item>
//</style>

//    private void enableClick() {
//        //ENABLE view items
//        //Enable after 1 second
//            new Timer().schedule(new TimerTask() {
//                @Override
//                public void run() {
//                }
//            },2000);
//    }


    //ProgressViewHolder
    private class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.pLoadingMore);
        }
    }

    //NEW ADDITIONAL new_design
    public void deleteOneBookmark(ImageView bookmark, int newsId) {
        helper.deleteOneBookmark(newsId);
        //Instant Updating the Latest Bookmarking:
        bookmark.setImageResource(R.drawable.bookmark_prime);
        fragment.toastMessage("Bookmark deleted.");
    }

    private void checkIfBookmarked(ItemViewHolder holder, String idStr) {
        if(Global.bookmarkIdList.contains(Integer.valueOf(idStr))){
            holder.bookmark.setImageResource(R.drawable.new_bookmark_fill); // holder.bookmark.setImageResource(R.drawable.bookmark_fill);
        }else holder.bookmark.setImageResource(R.drawable.new_bookmark_empty); //holder.bookmark.setImageResource(R.drawable.bookmark_prime);
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }


    //INTERFACE
    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void releasePlayer() {

        if (exoPlayer != null || exoPlayer2 != null || exoPlayer3 != null) {
            exoPlayer.release(); exoPlayer2.release(); exoPlayer3.release();
            exoPlayer = null; exoPlayer2 = null; exoPlayer3 = null;

            Log.d("TAGGG", "RVAdapterBreaking  releasePlayer: called");
        }
    }

    public void pauseVideoPlay() {

        if (exoPlayer != null || exoPlayer2 != null || exoPlayer3 != null) {
            exoPlayer.setPlayWhenReady(false); exoPlayer2.setPlayWhenReady(false); exoPlayer3.setPlayWhenReady(false);
            exoPlayer.getPlaybackState(); exoPlayer2.getPlaybackState(); exoPlayer3.getPlaybackState();

            Log.d("TAGGG", "RVAdapterBreaking  releasePlayer: called");
        }
    }



    public void displayType01( RequestOptions mainOption, String mainId, String mainTitle,String mainDate, String mainPhoto, TextView news_title, TextView news_date, ImageView news_photo){
        //for Debugging
        Log.d(TAG, "displayType01: CALLED**  NEWS_ID: "+mainId);
        Log.d(TAG, "displayType01: CALLED**  TITLE: "+mainTitle);
        Log.d(TAG, "displayType01: CALLED**  DATE: "+mainDate);
        Log.d(TAG, "displayType01: CALLED**  PHOTO: "+mainPhoto);
        Log.d(TAG, "isDisplayType01Validated: TITLE "+isValidated(mainTitle));
        Log.d(TAG, "isDisplayType01Validated: DATE "+isValidated(mainDate));
        Log.d(TAG, "isDisplayType01Validated: PHOTO "+isValidated(mainPhoto));

        if (isValidated(mainTitle)) {  //(!mainTitle.equals("null") || !mainTitle.equals("")) { wrong || shud be &&
            news_title.setText(mainTitle);
            news_title.setVisibility(VISIBLE);
        } else news_title.setVisibility(INVISIBLE);

        if (isValidated(mainDate)) {
            news_date.setText(mainDate);
            news_date.setVisibility(VISIBLE);
        } else news_date.setVisibility(INVISIBLE);

        if (isValidated(mainPhoto)) {  //(!mainPhoto.equals("") || !mainPhoto.equals("null")) {
            displayImage01(mainId, mainPhoto, mainOption, news_photo);
        } else displayDefaultImage(news_photo);  //Default
    }

//    private boolean isValidated(String string){
//        try{
//            if(!string.isEmpty()) return true; //Check if Empty
//        }catch(Exception e){
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
//        }finally {
//            if(string!=null) return true;  //Check if null
//            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
//        }
//        return false;
//    }

    private boolean isValidated(String string){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
            if(string!=null) return true;  //Check if null
        }
        return false;
    }

    public void displayType25( RequestOptions mainOption, String id, String mainTitle, String mainLabel, String mainPhoto, TextView ad_title, TextView ad_sponsor, ImageView ad_photo){
        //for Debugging
        Log.d(TAG, "displayType25: CALLED**  NEWS_ID: "+id);
        Log.d(TAG, "displayType25: CALLED**  TITLE: "+mainTitle);
        Log.d(TAG, "displayType25: CALLED**  LABEL: "+mainLabel);
        Log.d(TAG, "displayType25: CALLED**  PHOTO: "+mainPhoto);
        Log.d(TAG, "isDisplayType25Validated: TITLE "+isValidated(mainTitle));
        Log.d(TAG, "isDisplayType25Validated: LABEL "+isValidated(mainLabel));
        Log.d(TAG, "isDisplayType25Validated: PHOTO "+isValidated(mainPhoto));

        //androidx
        if (isValidated(mainTitle)) {
            ad_title.setText(mainTitle);
            ad_title.setVisibility(VISIBLE);
        } else ad_title.setVisibility(INVISIBLE);

        //androidx
        if (isValidated(mainLabel)) {
            ad_sponsor.setText(mainLabel);
            ad_sponsor.setVisibility(VISIBLE);
        } else ad_sponsor.setVisibility(INVISIBLE);

        //androidx
        if (isValidated(mainPhoto)) {
            displayImage25(mainOption, mainPhoto, id, ad_photo);
        } else displayDefaultImage(ad_photo);
    }


    public void displayImage01(String id, String photo, RequestOptions request_option, ImageView photo_container ){
        Log.d(TAG, "displayImage01: CALLED***");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //GlideApp
                    Glide
                    .with(context)
                    .load(photo)
                    .transition(withCrossFade())
                    .apply(request_option)
                    .into(photo_container);
        }
        ViewCompat.setTransitionName(photo_container, id); //setTransitionName
    }

    public void displayImage25( RequestOptions request_option, String photo, String id, ImageView photo_container ){
        //GlideApp
                Glide
                .with(context)
                .load(photo)
                .transition(withCrossFade())
                .apply(request_option)
                .into(photo_container);
        ViewCompat.setTransitionName(photo_container, id); //setTransitionName
    }

    public void displayDefaultImage(ImageView iv){
        Log.d(TAG, "displayDefaultImage: CALLED** ");
        switch (sf.getLastSection()) {
            case 1:
            case 2:
                Glide
                        .with(context)
                        .load(R.drawable.inquirersports)
                        .into(iv);
                break;
            case 3:
                Glide
                        .with(context)
                        .load(R.drawable.inquirerentertainment)
                        .into(iv);
                break;
            case 5:
                Glide
                        .with(context)
                        .load(R.drawable.inquirertechnology)
                        .into(iv);
                break;
            case 6:
                Glide
                        .with(context)
                        .load(R.drawable.inquirerglobalnation)
                        .into(iv);
                break;
            case 7:
                Glide
                        .with(context)
                        .load(R.drawable.inquirerbusiness)
                        .into(iv);
                break;

            case 8:
                Glide
                        .with(context)
                        .load(R.drawable.inquirerlifestyle)
                        .into(iv);
                break;
            case 12:
                Glide
                        .with(context)
                        .load(R.drawable.inquirerproperty)
                        .into(iv);
                break;
            case 13:
                Glide
                        .with(context)
                        .load(R.drawable.inquireropinion_blue)
                        .into(iv);
                break;
            case 16:
                Glide
                        .with(context)
                        .load(R.drawable.inquirerboardtalk)
                        .into(iv);
                break;
            case 17:
                Glide
                        .with(context)
                        .load(R.drawable.inquirermobility)
                        .into(iv);
                break;
            case 19:
                Glide
                        .with(context)
                        .load(R.drawable.inquirerrebound)
                        .into(iv);
                break;
            default:
                Glide
                        .with(context)
                        .load(R.drawable.inquirernewsinfo)
                        .into(iv);
        }
    }

    private void setDelayDisplayThumbnail(ImageView thumbNail){
        Handler handler = new Handler();
        Log.d(TAG, "setDelayDisplayThumbnail: Called1: ");
        handler.postDelayed(() -> {
            thumbNail.setVisibility(GONE);
            Log.d(TAG, "setDelayDisplayThumbnail: Called2: ");
//            arrow.setVisibility(View.GONE);
        }, 1000); //1000
    }


    //INSIDE ADAPTER: Monitoring specific viewholder item  if attach to window or not
    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);

    }


    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Log.d(TAG, "onViewAttachedToWindow: "+holder.getAdapterPosition());
    }

}
