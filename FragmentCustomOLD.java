package com.knx.inquirer.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.knx.inquirer.R;
import com.knx.inquirer.database.DatabaseHelper;
import com.knx.inquirer.models.Banner;
import com.knx.inquirer.models.BookmarksNewModel;
import com.knx.inquirer.models.Feed;
import com.knx.inquirer.models.Option;
import com.knx.inquirer.models.PostArticleCustom;
import com.knx.inquirer.models.PostUIUpdate;
import com.knx.inquirer.models.Sections;
import com.knx.inquirer.rvadapters.RVAdapterCustom;
import com.knx.inquirer.rvadapters.RVAdapterEmpty_Custom;
import com.knx.inquirer.utils.ApiService;
import com.knx.inquirer.utils.GlideApp;
import com.knx.inquirer.utils.Global;
import com.knx.inquirer.utils.ServiceGenerator;
import com.knx.inquirer.utils.SharedPrefs;
import com.knx.inquirer.viewmodelfactories.ViewModelFactoryMyInqNews;
import com.knx.inquirer.viewmodels.BannerAdViewModel;
import com.knx.inquirer.viewmodels.BookmarkViewModel;
import com.knx.inquirer.viewmodels.MyInqNewsViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static java.lang.String.valueOf;

public class FragmentCustomOLD extends Fragment {
    private static final String TAG = "FragmentCustom";
    /*** Views ***/
    private RecyclerView rvCustom;  //fragments
    private SwipeRefreshLayout mSwipeToRefreshLayout;

    private Snackbar snackbar;
    private TextView txtLoading, txtNetStatus;

    //test
    private ImageView bannerIvImage;

    private String bannerAdLink;
    private String bannerSectionId ;

    private BannerAdViewModel bannerAdViewModel;

    /*** Adapters ***/

    private RVAdapterCustom adapter_custom;
    private RVAdapterEmpty_Custom adapter_empty_custom;

    /*** Variables ***/
    private String aidStr;
    private boolean hasLoadedOnce = false, isConnected = true;
    private ArrayList<Feed> newsmodel = new ArrayList<>();
    private boolean isFragmentVisible = false;
    private boolean isNewsRecollect = false;
    private boolean hasEvent = false;
    private boolean bookmarkResult = false;
    private boolean isNetworkChanged = false;
    private boolean hasNewDailyKey = false;
    private int counter = 0;
    private boolean isMyInqUpdated = false;

    /*** Utils ***/
    private SharedPrefs sf;
    private Global.MyLoaderDialog loaderDialog;
    private Global.NoInternetDialog alertNoInternet;
    private Global.ViewDialog alertErrorArticle;
    private DatabaseHelper helper;

    /*** ViewModel Components ***/
    private BookmarkViewModel bookmarkViewModel;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiService apiService;

//    private FeedViewModel feedViewModel;

    //test
    // private Section sectionBan;
    private Single<Banner> bannerSingle;
    private ArrayList<Sections> menuAdResponse = new ArrayList<>();
    private ArrayList<Option> optionmodel = new ArrayList<>();

    private Handler customHandler = new Handler();

    private int timeCounter;

    public FragmentCustomOLD() {
        // Required empty public constructor
    }

    //new Threads with podcast
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(String msg) {
        if(msg.equals("network_access") || msg.equals("no_network_access")){
            onConnectionChange(msg);
        }
        else if(msg.equals("myInqUpdated")||msg.equals("loginSuccess")){
            hasEvent = true;
            isMyInqUpdated = true; //
            getMyInqNews();
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onItemPostEvent(PostArticleCustom item) {
        Log.d(TAG, "onItemPostEvent: CALLED*** ");
        //RESOLVE BUG: viewArticle is called twice sometimes: create GlobalFlag
        if(!Global.isArticleViewed){
            Global.isArticleViewed = true;
            viewArticle(String.valueOf(item.getItempos()));
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void dismissSnackbar(PostUIUpdate update) {

        if (update.getMsg().equals("dismiss_myinq"))
            if (snackbar != null && snackbar.isShown())
                snackbar.dismiss();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: LifeCycle");
        super.onCreate(savedInstanceState);
        sf = new SharedPrefs(getContext());
        helper = new DatabaseHelper(getContext());
        //for NOTIFICATION DeepLink
        Global.isMainActivityOpen = true;
        String DEVICE_ID = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        //Set Device ID immediately
        sf.setDeviceId(DEVICE_ID);
        apiService = ServiceGenerator.createService(ApiService.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_breaking_custom, container, false);
        initViews(view);
        Log.d(TAG, "onCreateView: LifeCycle");
        if(sf.isRegistered()) getMyInqNews();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: LifeCycle");
        postponeEnterTransition(); //Later to be REMOVED
        if(!EventBus.getDefault().isRegistered(FragmentCustomOLD.this)) EventBus.getDefault().register(FragmentCustomOLD.this);
    }

    public void checkAd2(String section_id) {
        removeAd(); //Remove Ad First
        bannerSectionId = section_id; //bannerSectionId = to be passed to WebNewsActivity
        Log.d(TAG, "checkAd2: getBannerAd2 section_id_CALLED: " + section_id);
        //CHECK if Global.bannerList is not empty
        if (Global.bannerList.size() > 0) {  //Global.bannerList
            for (int x = 0; x < Global.sectionsBanner2.size(); x++) {  //Iterate in Global.sectionsBanner not in Global.bannerList
                if (Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString().equals(getBannerList(section_id))) {
                    Log.d(TAG, "checkAd2: getBannerAd2 Searched_FOUND: "+ Global.sectionsBanner2.get(x).getAsJsonObject().get("id").getAsString()+" == "+getBannerList(section_id));
                    String image = Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("image").getAsString();
                    String link = Global.sectionsBanner2.get(x).getAsJsonObject().get("banner").getAsJsonObject().get("link").getAsString();
                    Log.d(TAG, "checkAd2: getBannerAd2 Searched_FOUND: IMAGE: "+image+" == LINK: "+link);

                    displayAd(image, link); //Global.sectionsBanner
                    break;
                }
            }
        }
    }

    private String getBannerList(String sectionId){
        String section_ID = "-1";
        //Note: Already check that Global.bannerList is not empty
        //Finding section_id inside Global.bannerList
        for(int x = 0; x<Global.bannerList.size(); x++){
            if(Global.bannerList.get(x).equals(sectionId)){
                section_ID = Global.bannerList.get(x);
                return section_ID;
            }
        }
        return section_ID;
    }

    private void displayAd(String image, String link) {
        Log.d(TAG, "getBannerAd2 displayAd: CALLED: ");
        bannerIvImage.setVisibility(View.VISIBLE);
        bannerAdLink = link;

        //GlideApp Usage: Need: MyGlide and UnsafeOkHttpClient JavaClass
        GlideApp.with(getContext())
                .load(image)
                .transition(withCrossFade())
                .apply(requestOptions())
                .into(bannerIvImage);
        ViewCompat.setTransitionName(bannerIvImage, "BannerAd"); //setTransitionName
    }

    private RequestOptions requestOptions(){
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .priority(Priority.HIGH);
        return options;
    }

    public void removeAd(){
        Log.d(TAG, "getBannerAd2 removeAd: CALLED: ");
        bannerIvImage.setVisibility(View.GONE);
    }
    //
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {

            case 88: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    editMyInqPreference();

                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        //Show permission explanation dialog...
                        Toast.makeText(getContext().getApplicationContext(), "This app needs your device storage permission to store your updated preferences", Toast.LENGTH_SHORT).show();
                    }else{
                        //Never ask again selected, or device policy prohibits the app from having that permission.
                        //So, disable that feature, or fall back to another situation...
                        Toast.makeText(getContext().getApplicationContext(), "For you to edit, please allow storage access in your phone settings.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;

        }
    }

    @Override
    public void onDestroy() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
//        adapter_custom.pauseVideoPlay();
        super.onDestroy();
    }


    public void initViews(View view) {

        txtLoading = view.findViewById(R.id.txtloadingsection);
        txtNetStatus = view.findViewById(R.id.txtnetstatus);

        //test
        bannerIvImage = view.findViewById(R.id.ivAdImageCustom);
//        bannerIvImage.setAlpha(200);
        bannerIvImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(bannerAdLink));
                startActivity(intent);
            }
        });

        txtNetStatus.setVisibility(View.GONE);
        mSwipeToRefreshLayout = view.findViewById(R.id.swipenews);
        mSwipeToRefreshLayout.setColorSchemeColors(Color.parseColor("#003877"));

        //rv
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvCustom = view.findViewById(R.id.recyclerview);
        rvCustom.setHasFixedSize(true);
        rvCustom.setLayoutManager(layoutManager);  //LINEAR VERTICAL

        mSwipeToRefreshLayout.setOnRefreshListener(() -> {
            try{
                getMoreNewsOnSwipeMyInq();  //
            }catch (Exception e){
                mSwipeToRefreshLayout.setRefreshing(false);
                Log.d(TAG, "initViews: mSwipeToRefreshLayout getMoreNewsOnSwipeMyInq "+ e.getMessage());
            }
        });

        //Get instance for Singleton
        loaderDialog =  Global.MyLoaderDialog.getInstance(); //

        alertNoInternet = new Global.NoInternetDialog();
        alertErrorArticle = new Global.ViewDialog();

    }

    //Just change the Fragment from FragmentMyInqHolder to FragmentMyInqEdit
    public void callEditPrefs(){
        editMyInqPreference();
    }

    //Just change the Fragment from FragmentMyInqHolder to FragmentMyInqEdit
    private void editMyInqPreference() {
        Fragment mF = getParentFragment();
        // double check
        if (mF instanceof FragmentMyInqHolder) {
            getFragmentManager().beginTransaction()
                    .replace((getParentFragment()).getView().findViewById(R.id.fragment_container).getId(), new FragmentMyInqEdit())
                    .commit();
        }
    }

    //
    public void onArticleRequest() {
//        Global.isItemClicked = true;
        showLoader();
        mSwipeToRefreshLayout.setEnabled(false);
    }

    public void onArticleRequestSuccess() {
//        Global.isItemClicked = false;
        hideLoader();
        mSwipeToRefreshLayout.setEnabled(true);
    }

    public void onArticleRequestError() {
        Global.isItemClicked = false;
        hideLoader();
    }

    public void showLoader() {
        Log.d(TAG, "showLoader: myInqUpdated ");
        if(isFragmentVisible || hasEvent ) loaderDialog.showDialog(getContext());
    }

    public void hideLoader() {
        Log.d(TAG, "hideLoader: myInqUpdated ");
        if(isFragmentVisible || hasEvent) loaderDialog.dismissDialog(getContext());
    }

    public void RetryFetchCustom() {
        setBlankAdapter();
        mSwipeToRefreshLayout.setRefreshing(false);
    }

    public void setBlankAdapter() {
        txtLoading.setText("Loading sections...");
        rvCustom.setAdapter(null);
    }

    private void setEmptySection() {
        txtLoading.setText("Failed to load sections");
    }

    private void setEmptyAdapter() {
        adapter_empty_custom = new RVAdapterEmpty_Custom(this);
        rvCustom.setAdapter(adapter_empty_custom);
    }

    private void onPreloadingSection() {
        //Dont show LoadingProcess if NetworkChanged="access.." &&
        showLoader();
        mSwipeToRefreshLayout.setEnabled(false);
    }

    private void onSuccessLoadingSection() {
        hideLoader();
        mSwipeToRefreshLayout.setEnabled(true);
    }

    private void onConnectionChange(String msg) {
//        Log.d(TAG, "onConnectionChange: isFragmentVisible: "+isFragmentVisible);
//        Log.d(TAG, "onConnectionChange: newsmodel.size: "+newsmodel.size());

                if(msg.equals("network_access")){
                    Log.d(TAG, "onConnectionChange: network_access: ");
                    txtNetStatus.setVisibility(View.GONE);
                    //Refresh
                    isNetworkChanged = true;
                    if(isFragmentVisible){
                        if(newsmodel.size()<= 0) getMyInqNews(); //Call API only if has no news displayed
                    }

                }else if(msg.equals("no_network_access")){
                    isNetworkChanged = false;
                    Log.d(TAG, "onConnectionChange: no_network_access: ");
                    txtNetStatus.setVisibility(View.VISIBLE);
                    txtNetStatus.setText("No network connection. You are viewing offline news");
                }


//        if (isAdded() && isVisible())
//            if(newsmodel.size() > 0)
//                if(msg.equals("network_access")){
//                    txtNetStatus.setVisibility(View.GONE);
//                    //Refresh
//                    isNetworkChanged = true;
//                    getMyInqNews();
//                }else if(msg.equals("no_network_access")){
//                    txtNetStatus.setVisibility(View.VISIBLE);
//                    txtNetStatus.setText("No network connection. You are viewing offline news");
//                }

    }



    /*** Begin ViewModel methods ***/
    //Determine only by UNIQUE_ID of USER: sf.getOptionId()
    private void getMyInqNews(){
        Log.d(TAG, "getMyInqNews: CALLED: ");
        Log.d(TAG, "getMyInqNews: CALLED: isNewsRecollect "+ !isNewsRecollect);
//        Log.d(TAG, "getMyInqNews: CALLED: isNetworkChanged "+ !isNetworkChanged);
//        if(!isNetworkChanged || !isNewsRecollect) onPreloadingSection(); //Dont Display LoadingProcess if NetworkConnectionIsChangedManually and ApiIsCalledFromMeneVisibility
        if(!isNewsRecollect) onPreloadingSection(); //Dont Display LoadingProcess if NetworkConnectionIsChangedManually and ApiIsCalledFromMeneVisibility

        MyInqNewsViewModel myInqNewsViewModel = ViewModelProviders.of(this, new ViewModelFactoryMyInqNews(getActivity().getApplication(), compositeDisposable, sf)).get(MyInqNewsViewModel.class);
        myInqNewsViewModel.getMyInqNews().observe(this, feeds -> {

//            if(!isNetworkChanged) onSuccessLoadingSection();  //Dont Display LoadingProcess if NetworkConnectionIsChangedManually
            onSuccessLoadingSection(); //Dialog Dismiss will not be executed if Dialog is not showing.

            isMyInqUpdated = false; //Default: Stopping to callAgain the API if Error
            isNetworkChanged = false; //Default: Stopping to callAgain the API if Error
            hasLoadedOnce = true; //Flag to be able to run the getMyInqNews() in setMenuVisibility later after returning to MyInquirer TAB
            isNewsRecollect = false; //Flag to be able to run the getMyInqNews() in setMenuVisibility later after returning to MyInquirer TAB

            newsmodel.clear();
            newsmodel.addAll(feeds);
            adapter_custom = new RVAdapterCustom(getContext(), newsmodel, FragmentCustomOLD.this, sf);
            rvCustom.setAdapter(adapter_custom);
            adapter_custom.notifyDataSetChanged();
            adapter_custom.setLoadMoreListener(() -> getMoreNewsOnScrollMyInq()); //setLoadMoreListener Need Interface param..but supplied here an ArrowFunction ???

            if (newsmodel.size() > 0) {
                adapter_custom.setMoreDataAvailable(true);
            } else {
                adapter_custom.setMoreDataAvailable(false);
            }
        });

        //INQ_ERROR
        myInqNewsViewModel.getMyInqError().observe(this, isError -> {
            if(isError) {
//                if(!isNetworkChanged) onSuccessLoadingSection();  //Dont Display LoadingProcess if NetworkConnectionIsChangedManually
                onSuccessLoadingSection(); //Dialog Dismiss will not be executed if Dialog is not showing. Totally Dependent on ShowingDialog
                myInqNewsViewModel.getMyInqthrowable().observe(this, e -> {
                    String error_msg = Global.getResponseCode(e);
//                    if(!isNetworkChanged) toastMessage(error_msg); //Dont Display Toast if NetworkConnectionIsChangedManually
//                    if(!isDebugging || !isNetworkChanged) toastMessage(error_msg);
//                    else toastMessage(error_msg + TAG + " getMyInqError: "+e.getMessage());
                    //Dont Display ErrorMessage if has news displayed and not Visible
                    if(isFragmentVisible){
                        if(newsmodel.size() <= 0 || isMyInqUpdated) toastMessage(error_msg);
                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)) toastMessage(TAG+": getMyInqError: "+ e.getMessage()); // //Display in developer
                        Log.d(TAG, "getMyInqNews: getMyInqError_ERROR: "+ error_msg+ ": "+e.getMessage());
                    }

                });
//                mSwipeToRefreshLayout.setEnabled(true);
                //Display: Network message NO Connection
                if (!Global.isNetworkAvailable(getContext())) {
                    txtNetStatus.setVisibility(View.VISIBLE);
                    txtNetStatus.setText("No network connection. You are viewing offline news.");
                    Log.d(TAG, "getMyInqNews 1: No Network");
                    //Get only the Cached if has no news displayed: Usually occurs when first opening the app while noInternet
                    if(newsmodel.size() <= 0) getMyInqCachedNews();
                    Log.d(TAG, "getMyInqNews 2: No Network "+ newsmodel.size());
                } else {
//                    if(isNetworkChanged) isNetworkChanged = false; //Default false the isNetworkChanged
                    txtNetStatus.setVisibility(View.GONE);
                    Log.d(TAG, "getMyInqNews: WITH Network");
                    //Call Again if No News Display or if FromMyInqUpdated or (isNetworkChanged and newsmodel.size() <= 0)
                    //Call again if VisibleOnly
                    if(isFragmentVisible){
                        if(newsmodel.size() <= 0 || isMyInqUpdated || isNetworkChanged) getMyInqNews();
                    }
                }
            }
        });

        /**NEW ADDITIONAL NewDesign: For BookmarkIdList*/  /**NEW ADDITIONAL NewDesign: For BookmarkIdList*/
        bookmarkViewModel = ViewModelProviders.of(this).get(BookmarkViewModel.class); //initialize view_model PROVIDER
        bookmarkViewModel.getBookmarkList().observe(this, data -> { //Accessing view_model METHOD
            Global.bookmarkIdList.clear(); //Need TO CLEAR to be UPDATED every CHANGES
            Log.d(TAG, "getBookmarkList: called size DATA " + data.size());
            //Get the BOOKMARK_ID only: Will use for marking those already bookmarked
            for (int x = 0; x < data.size(); x++) {
                Global.bookmarkIdList.add(data.get(x).getNewsId()); //add to HashSet
            }
            if(adapter_custom!=null)adapter_custom.notifyDataChanged();
        });
//        isNetworkChanged = false; //back to default
    }
    /*** End ViewModel methods ***/

    /*** Start RxJava methods ***/
    private void getMoreNewsOnScrollMyInq() {
        try{

            // aidStr = is the Last NewsID in the RV_LIST_VIEW
            if (!newsmodel.contains(null)) {
                aidStr = newsmodel.get(newsmodel.size() - 1).getId();
            }
            rvCustom.post(() -> {
                if (!newsmodel.contains(null))
                    newsmodel.add(null);
                adapter_custom.notifyItemInserted(newsmodel.size() - 1);
            });

            apiService.getDailyKey()
                    .flatMap((Function<String, SingleSource<ArrayList<Feed>>>) key -> {
                        long unixTime = System.currentTimeMillis() / 1000L;
                        String hashed = Global.Generate32SHA512(key + unixTime +"inq" + sf.getDeviceId() + "-megascroll/" + "b" + "/" + aidStr + "/" + sf.getOptionId());
                        sf.setDailyKey(key);
                        return apiService.getMoreNewsMyInq(String.valueOf(unixTime),sf.getDeviceId(), sf.getOptionId(), Integer.parseInt(aidStr), "b", hashed);
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<ArrayList<Feed>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(ArrayList<Feed> news) {

                            if (newsmodel.contains(null)) {
                                newsmodel.remove(newsmodel.size() - 1);
                                adapter_custom.notifyItemRemoved(newsmodel.size());
                            }
                            //
                            newsmodel.addAll(news);
                            hideLoader();
                            //CALLING AGAIN the INTERFACE setLoadMoreListener and CALLING AGAIN the getMoreNewsOnScrollMyInq
                            //
                            adapter_custom.setLoadMoreListener(() -> getMoreNewsOnScrollMyInq());
                            //
                            if (newsmodel.size() > 0) {
                                adapter_custom.setMoreDataAvailable(true);
                            } else adapter_custom.setMoreDataAvailable(false);

                            adapter_custom.notifyDataChanged();
                        }

                        @Override
                        public void onError(Throwable e) {

                            if (newsmodel.contains(null)) {
                                newsmodel.remove(newsmodel.size() - 1);
                                adapter_custom.notifyItemRemoved(newsmodel.size());
                            }
                            //Show SnackBar
                            String error_msg = Global.getResponseCode(e);
                            snackbar = Snackbar.make(getActivity().findViewById(R.id.snackbarContainer), error_msg, Snackbar.LENGTH_INDEFINITE)
                                    .setAction("MORE NEWS", view -> {
                                        rvCustom.smoothScrollToPosition(newsmodel.size());
                                        getMoreNewsOnScrollMyInq(); //CALLING AGAIN the getMoreNewsOnScrollMyInq
                                    });
                            snackbar.show();
                        }
                    });

        }catch(Exception e){
            Log.d(TAG, "getMoreNewsOnScrollMyInq: Exception: "+e.getMessage());
        }
    }

    private void getMoreNewsOnSwipeMyInq() {
        Log.d(TAG, "getMoreNewsOnSwipeMyInq: CALLED last NewsID: "+newsmodel.get(0).getId());
        mSwipeToRefreshLayout.setRefreshing(true);
        apiService.getDailyKey()
                .flatMap((Function<String, SingleSource<ArrayList<Feed>>>) key -> {
                    long unixTime = System.currentTimeMillis() / 1000L;
                    String hashed = Global.Generate32SHA512(key + unixTime + "inq" + sf.getDeviceId() + "-megascroll/" + "a" + "/" + newsmodel.get(0).getId() + "/" + sf.getOptionId());
                    sf.setDailyKey(key);
                    return apiService.getMoreNewsMyInq(String.valueOf(unixTime), sf.getDeviceId(), sf.getOptionId(), Integer.parseInt(newsmodel.get(0).getId()), "a", hashed);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ArrayList<Feed>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ArrayList<Feed> news) {
                        mSwipeToRefreshLayout.setRefreshing(false);
                        if(news.size() > 0) {
                            if (!newsmodel.contains(news)) {
                                newsmodel.addAll(0, news);
                                //Sort news to latest
                                Collections.sort(newsmodel, (o1, o2) -> o2.getId().compareTo(o1.getId()));
                                adapter_custom.notifyDataChanged();
                            }
                        }else{
                            if(newsmodel.size()>0)toastMessage("Latest news stories are loaded");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSwipeToRefreshLayout.setRefreshing(false);
                        //Always Toast if has already news in the list
                        //RESOLVED: Getting updated news is automatically already coz of VIEWMODEL

                        if(!Global.isNetworkAvailable(getContext())){
                            toastMessage("No network connection");
                        }else{
                            if(newsmodel.size()>0)toastMessage("Latest news stories are loaded");
                        }

//                        Toast.makeText(getActivity().getApplicationContext(), "Latest news stories are loaded", Toast.LENGTH_SHORT).show();
//                        String error_msg = Global.getResponseCode(e);
//                        Toast.makeText(getContext(), error_msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

//    private void getMoreNewsOnSwipeMyInq2() {
//        mSwipeToRefreshLayout.setRefreshing(true);
//        long unixTime = System.currentTimeMillis() / 1000L;
//        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaregfeed/" + sf.getOptionId());
//
//        apiService = ServiceGenerator.createService(ApiService.class);
//        apiService.getFeed(String.valueOf(unixTime), sf.getDeviceId(), String.valueOf(sf.getOptionId()), hashed)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//
//           .subscribe(new SingleObserver<FeedResponse>() {
//
//            @Override
//            public void onSubscribe(Disposable d) {
//                compositeDisposable.add(d);
//            }
//
//            @Override
//            public void onSuccess(FeedResponse response) {
//
//                myInqRepoError.setValue(false);
//                myInqFeed.setValue(response.getFeed());
//
//                //Save to db
//                helper.deleteCustomNews();
//                helper.insertCustomNews(response.getFeed());
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                throwableMutableLiveData.setValue(e);
//                myInqRepoError.setValue(true);
//            }
//        });
//
//
//
//
//
//
//
//
//
//
//
//
//
//        apiService.getDailyKey()
//                .flatMap((Function<String, SingleSource<ArrayList<Feed>>>) key -> {
//                    long unixTime = System.currentTimeMillis() / 1000L;
//                    String hashed = Global.Generate32SHA512(key + unixTime + "inq" + sf.getDeviceId() + "-megascroll/" + "a" + "/" + newsmodel.get(0).getId() + "/" + sf.getOptionId());
//                    sf.setDailyKey(key);
//                    return apiService.getMoreNewsMyInq(String.valueOf(unixTime), sf.getDeviceId(), sf.getOptionId(), Integer.parseInt(newsmodel.get(0).getId()), "a", hashed);
//                })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<ArrayList<Feed>>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        compositeDisposable.add(d);
//                    }
//
//                    @Override
//                    public void onSuccess(ArrayList<Feed> news) {
//                        mSwipeToRefreshLayout.setRefreshing(false);
//                        if(news.size() > 0) {
//                            if (!newsmodel.contains(news)) {
//                                newsmodel.addAll(0, news);
//                                //Sort news to latest
//                                Collections.sort(newsmodel, (o1, o2) -> o2.getId().compareTo(o1.getId()));
//                                adapter_custom.notifyDataChanged();
//                            }
//                        }else{
//                            if(newsmodel.size()>0)toastMessage("Latest news stories are loaded");
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        mSwipeToRefreshLayout.setRefreshing(false);
//                        //Always Toast if has already news in the list
//                        //RESOLVED: Getting updated news is automatically already coz of VIEWMODEL
//
//                        if(!Global.isNetworkAvailable(getContext())){
//                            toastMessage("No network connection");
//                        }else{
//                            if(newsmodel.size()>0)toastMessage("Latest news stories are loaded");
//                        }
//
////                        Toast.makeText(getActivity().getApplicationContext(), "Latest news stories are loaded", Toast.LENGTH_SHORT).show();
////                        String error_msg = Global.getResponseCode(e);
////                        Toast.makeText(getContext(), error_msg, Toast.LENGTH_SHORT).show();
//                    }
//                });
//    }



//    private void viewArticle(String id) {
//        Log.d(TAG, "viewArticle: "+id);
//        onArticleRequest();
//        Log.d(TAG, "viewArticle:2 "+id);
//        long unixTime = System.currentTimeMillis() / 1000L;
//        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
//        apiService.getArticle(String.valueOf(unixTime), sf.getDeviceId(), id, hashed)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<Article>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        compositeDisposable.add(d);
//                    }
//
//                    @Override
//                    public void onSuccess(Article article) {
//                        Global.isItemClicked = true;
//                        onArticleRequestSuccess();
//                        String sectionId = article.getSectionId();
//                        String title = article.getTitle();
//                        String photo = article.getCreative();
//                        String type = article.getType();
//                        String pubdate = article.getPubdate();
//                        String content1 = article.getContent1().isEmpty() ? article.getContent2a() : article.getContent2b();
//
////                        String content1 = article.getContent() != null ? article.getContent().get(0).toString() : "";
////                        String image1 = article.getInsert() != null ? article.getInsert().getCreative(): "";
////                        String content2 = article.getContent() != null ? article.getContent().get(1).toString() : "";
//                        String image1 = "";
//                        String content2 = article.getContent2b();
//
//                        String author = article.getByline();
//                        String custom = article.getCustom();
//
//                        Log.d(TAG, "onSuccess: CONTENT_ 1: "+ content1);
//                        Log.d(TAG, "onSuccess: CONTENT_ 2: "+ content2);
//
//                        //Get the Related Article
//                        if(article.getRelated()!=null){
//                            Global.relatedArticles.clear();
//                            Global.relatedArticles.addAll(article.getRelated());
//                        }
//
//                        //Get the SuperShare Details
//                        //androidx
//                        Intent intent = new Intent();
//                        intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
//                        intent.putExtra("id", id);
//                        intent.putExtra("sectionId", sectionId);
//                        intent.putExtra("title", title); //intent.putExtra("title", article.getTitle() != null ? article.getTitle() : "");
//                        intent.putExtra("photo", photo);//intent.putExtra("photo", article.getCreative() != null ? article.getCreative() : "");
//                        intent.putExtra("type", type);//intent.putExtra("type", article.getType() != null ? article.getType() : "");
//                        intent.putExtra("pubdate", pubdate);//intent.putExtra("pubdate", ( article.getPubdate() != null ? article.getPubdate() : "" ));
//                        intent.putExtra("content1", content1);//intent.putExtra("content", article.getContent() != null ? article.getContent() : "");
//                        intent.putExtra("image1", image1);
//                        intent.putExtra("content2", content2);
//                        intent.putExtra("author", author);//intent.putExtra("author", ( article.getByline() != null ? article.getByline() : "" ));
//                        intent.putExtra("custom", custom);//intent.putExtra("custom", article.getCustom() != null ? article.getCustom() : "");
//                        intent.putExtra("adbanner", bannerSectionId); //intent.putExtra("adbanner", sectionType);
////                        intent.putExtra("myinqflag", "myinqflag");
//                        startActivityForResult(intent, Global.NEWS_ARTICLE);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        hideLoader();
//                        onArticleRequestError();
//                        String error_msg = Global.getResponseCode(e);
//                        Toast.makeText(getContext().getApplicationContext(), error_msg, Toast.LENGTH_SHORT).show();
//                    }
//                });
//    }

    private void viewArticle(String id) {
        onArticleRequest();
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
        apiService.getArticle2(String.valueOf(unixTime),sf.getDeviceId(), id, hashed)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try{
                            hasNewDailyKey = false;
                            Global.isItemClicked = true;
                            onArticleRequestSuccess();
                            String content1="";
                            String content2="";
                            String insertNewId="";
                            String insertType="";
                            String insertTitle="";
                            String insertImage="";
                            String insertContent="";
                            //Sometimes NO Section_ID and DATE: Occur on ADS Type1
                            String section_id="", pubdate="", author="", title="", photo="", type="";
                            if(response.body().has("section_id")) section_id = !response.body().get("section_id").isJsonNull() ? response.body().get("section_id").getAsString() : "";
                            if(response.body().has("pubdate")) pubdate = !response.body().get("pubdate").isJsonNull() ? response.body().get("pubdate").getAsString() : "";
                            if(response.body().has("byline")) author = !response.body().get("byline").isJsonNull() ? response.body().get("byline").getAsString() : "";
                            if(response.body().has("title")) title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
                            if(response.body().has("creative")) photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
                            if(response.body().has("type")) type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";

                            //Get the SuperShare: Check if has an INSERT (for SuperShare)
                            if(response.body().has("insert")){
                                //Check if CONTENT as an ARRAY
//                                content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
//                                content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
//                                insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
//                                insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
//                                insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
//                                insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
//                                insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
//                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");

                                if(response.body().has("content")){
                                    content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
                                    content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
                                }
                                if(response.body().get("insert").getAsJsonObject().has("id")) insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("type"))  insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("title")) insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("creative")) insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("content"))  insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
                            }else {
                                //Check if CONTENT as a STRING
                                if(response.body().has("content")) content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
                            }
                            //Get the Related Article: First Check if has RelatedArticle
                            Global.relatedArticles.clear();
                            if(response.body().has("related")){
                                Log.d(TAG, "onResponse: CONTENT_  test1");
                                for(int x=0; x<response.body().get("related").getAsJsonArray().size();x++){
                                    Global.relatedArticles.add(response.body().get("related").getAsJsonArray().get(x).getAsJsonObject());
                                }
                            }
                            //INTENT
                            Intent intent = new Intent();
                            intent.setClassName("com.knx.inquirer", "com.knx.inquirer.WebNewsActivity");
                            intent.putExtra("id", id);
                            intent.putExtra("sectionId", section_id);
                            intent.putExtra("title", title);
                            intent.putExtra("photo", photo);
                            intent.putExtra("type", type);
                            intent.putExtra("pubdate", pubdate);
                            intent.putExtra("content1", content1);
                            intent.putExtra("image1", insertImage);
                            intent.putExtra("insertTitle", insertTitle);
                            intent.putExtra("insertType", insertType);
                            intent.putExtra("insertNewsId", insertNewId);
                            intent.putExtra("insertContent", insertContent);
                            intent.putExtra("content2", content2);
                            intent.putExtra("author", author);
                            intent.putExtra("custom", ""); //No USED
                            intent.putExtra("adbanner", bannerSectionId);
                            startActivityForResult(intent, Global.NEWS_ARTICLE);

                        }catch (Exception e){
                            Log.d(TAG, "onResponse: viewArticle Exception: "+ e.getCause());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        onArticleRequestError();
                        String error_msg = Global.getResponseCode(t);
                        if(!hasNewDailyKey) getDailyKey(); //Resolution: For always giving Error After longest time from the backGround. Maybe expiration of DailyKey.

                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
                            toastMessage(TAG+": onFailure: "+ t.getMessage()); // //Display in developer
                        }else{
                            toastMessage(error_msg); // //Display in user
                        }
                        Log.d(TAG, "onError: onArticleRequestError "+ t.getMessage());
                    }
                });
    }
    /*** End RxJava methods ***/

    private void getDailyKey(){
        Log.d(TAG, "getDailyKey: RUN");
        apiService.getDailyKey2().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    sf.setDailyKey(response.body());
                    hasNewDailyKey = true;
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: getDailyKey: "+t.getMessage());
            }
        });
    }


    public void checkVideoLink (String link) {

        if (isValidated(link)) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(link));
                startActivity(intent);
            }catch(Exception e){
                Log.d(TAG, "checkVideoLink: error " + e.getMessage());
                return;
            }

        }else{
            Global.isItemClicked = false; //isItemClicked false here: Because after click it does not leave the fragmentBreaking
            return;
        }
    }

    private boolean isValidated(String string){
        try{
            if(!string.isEmpty()) return true; //Check if Empty
        }catch(Exception e){
            Log.d(TAG, "isDisplayType01Validated: Exception_Error: "+e.getMessage());
        }finally {
            if(string!=null) return true;  //Check if null
            Log.d(TAG, "isDisplayType01Validated: Exception_Error Finally_Executed** ");
        }
        return false;
    }

    public void displayFullview (String video) {

        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//DO NOT FORGET THIS EVER
            intent.setDataAndType(Uri.parse(video), "video/mp4");
            startActivity(intent);
        }catch(Exception e){
            Log.d(TAG, "displayFullview: Video6Link "+ e.getMessage());
            return;
        }

    }

    public void checkImageLink(String link){

        if (isValidated(link)) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(link));
                startActivity(intent);
            }catch(Exception e){
                Log.d(TAG, "checkImageLink: ImageDoubleLink error " + e.getMessage());
                return;
            }
        }else{
            Global.isItemClicked = false; //isItemClicked false here: Because after click it does not leave the fragmentBreaking
            return;
        }
    }

    //registerTimeListViewReads
    private void registerTimeListViewReads(String deviceId, String section, int seconds, String password){
        apiService.registerTimeListViewReads(deviceId, section, seconds, password)
                .subscribeOn(Schedulers.io()) //subscribeOn
                .observeOn(AndroidSchedulers.mainThread()) //observeOn
                .subscribe(new SingleObserver<ResponseBody>() {  //subscribe
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseBody responseBody) {
                        Log.d(TAG, "onSuccess: registerTimeListViewReads called: SECTION= "+ section+ " TIME= " +seconds);

                        try {
                            String r = responseBody.string();
                            Log.d(TAG, "onSuccess: ResponseBody:" + r);
                            if(r.contains("Store Successfully")){///
//                                sf.setNotificationAllowed(false);
                                Log.d(TAG, "onSuccess: registerTimeListViewReads Successfully");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: registerTimeListViewReads error  getMessage" + e.getMessage());
                    }
                });
    }

//    public void removeStopPodcastAudio(){
//        Log.d(TAG, "removeStopPodcastAudio: CALLED");
//        EventBus.getDefault().removeStickyEvent(PostPodcastWebNewsAct.class);  //for WebNewsAct
//        EventBus.getDefault().post("removePlaybackButton");  //for FragmentBreaking Page
//
//        ((AudioManager) getContext().getSystemService(
//                getContext().AUDIO_SERVICE)).requestAudioFocus(
//                null,
//                AudioManager.STREAM_MUSIC,
//                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
//    }

    /** Records of seconds stay in article view **/
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            try {
                timeCounter++;
                Log.d(TAG, "run: counting.. "+timeCounter);
                customHandler.postDelayed(this, 1000);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: LifeCycle");
    }

    // NOTE: in androidx onResume() is also isUserHintVisible
//    @Override
//    public void onResume() {
//        //ONLY CALLED if MY_INQUIRER_TAB is CLICKED
//        if(Global.isVisibleFragmentMyInqHolder){
//            //NOTE: sectionName always base on the SECTION_TAB_MENU
//            //Setting the sectionName into myInq: First: save sectionName Temporary and Retrieve once the user left the MY_INQUIRER_TAB
//
//            //Dont save in TEMP if sectionName is already MY INQUIRER
////            Log.d(TAG, "onResume: FRAG_CUTOM"+sf.getSectionName());
////            if(!sf.getSectionName().equals("SEARCH NEWS") && !sf.getSectionName().equals("MY INQUIRER") ){
////                sf.setSectionNameTempContainer(sf.getSectionName());
////            }
////            sf.setSectionName("MY INQUIRER");
////            Log.d(TAG, "onResume: FRAG_CUTOM"+sf.getSectionName());
//
//            //check if myInqEditSectionIsUpdated: CALL getMyInqNews() for REFRESH NewsList based on updatedEditedSectionList
////            if(Global.isMyInqEditSectionUpdated){
////                Log.d(TAG, "onResume: CALLED  myInqUpdated ");
////                Global.isMyInqEditSectionUpdated = false;
////                 getMyInqNews(); //Refresh
////            }
//
//
//            if(Global.isNetworkAvailable(getContext())){
//                // getMyInqNews(); Will Only be Called when: 1.FromNewStart(where (newsmodel.size() is ZERO) 2. NotFromEditUpdateInMyInq
//                if(newsmodel.size() <= 0 && !Global.isMyInqUpdated){
//                    Log.d(TAG, "onResume: VISIBLE_HOLDER SIZE_NEWS"+ newsmodel.size());
//                    Log.d(TAG, "onResume: VISIBLE_HOLDER getMyInqNews CALLED");
//                    getMyInqNews();
//                }else{
//                    Global.isMyInqUpdated = false; //Default after FromEditUpdateInMyInq
//                    Log.d(TAG, "onResume: VISIBLE_HOLDER SIZE_NEWS"+ newsmodel.size());
//                    Log.d(TAG, "onResume: VISIBLE_HOLDER getMyInqNews NOT_CALLED");
//                    //
//                }
//            }else{
//                //GET OFFLINE
//                getMyInqCachedNews();
//            }
//
//
//            customHandler.postDelayed(updateTimerThread, 2000);  //start handler runnable
//        }else{
//            customHandler.removeCallbacks(updateTimerThread);  //stop.. handler runnable
//        }
//        super.onResume();
//    }



    @Override
    public void onPause() {
        Log.d(TAG, "onPause: LifeCycle");
//        Global.isItemClicked = false; // Global.isItemClicked = false = Able to Click the ListView once leave to breakingFragment
        customHandler.removeCallbacks(updateTimerThread);  //stop handler runnable
        sf.setListViewReadsCountMyInq(timeCounter);
        Log.d(TAG, "onPause: called ..Timer FRAGMENT_CUSTOM  is "+sf.getListViewReadsCountMyInq());
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: LifeCycle");
        //send timeCounter, then reset timeCounter
            if(sf.getListViewReadsCountMyInq()>=1){
                Log.d(TAG, "onStop: Recorded COUNTER is  "+ sf.getListViewReadsCountMyInq() + " getLastSectionCustom is "+sf.getLastSectionCustom());
                String sectionId = valueOf(sf.getLastSectionCustom()); //NOTICE: getLastSectionPosition vs lastSectionPosition
                registerTimeListViewReads(sf.getDeviceId(),sectionId, sf.getListViewReadsCountMyInq(), Global.passStoreUserReadTimeList);
                timeCounter = 0;
            }
        super.onStop();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: LifeCycle");
//        if(isFragmentVisible){
//            if(newsmodel.size() <= 0) {
//                Log.d(TAG, "onResume: getMyInqNews*** ");
//                getMyInqNews();
//            }
//        }
        super.onResume();
    }

    /////
    private void getMyInqCachedNews(){
        Log.d(TAG, "getMyInqCachedNews1: CAlled "+ newsmodel.size() );
        MyInqNewsViewModel myInqNewsViewModel = ViewModelProviders.of(this, new ViewModelFactoryMyInqNews(getActivity().getApplication(), compositeDisposable, sf)).get(MyInqNewsViewModel.class);
        myInqNewsViewModel.getMyInqCachedNews().observe(this, feeds -> {
            newsmodel.clear();
            newsmodel.addAll(feeds);
            adapter_custom = new RVAdapterCustom(getContext(), newsmodel, FragmentCustomOLD.this, sf);
            rvCustom.setAdapter(adapter_custom);
            adapter_custom.notifyDataSetChanged();

            Log.d(TAG, "getMyInqCachedNews2: CAlled "+ newsmodel.size() );

            if(sf.getLastSectionCustom() == 99) {
                adapter_custom.setLoadMoreListener(() -> getMoreNewsOnScrollMyInq());
            }
//            else{
//                adapter_custom.setLoadMoreListener(() -> getMoreNewsOnScroll(sf.getLastSectionCustom()));
//            }
            Log.d(TAG, "getMyInqCachedNews3: CAlled "+ newsmodel.size() );

            if (newsmodel.size() > 0) {
                adapter_custom.setMoreDataAvailable(true);
            } else {
                adapter_custom.setMoreDataAvailable(false);
            }
        });
    }

    /**New Additional NewDesign*/
    public boolean saveBookmark(String newsId)
    {
        return  bookmarkArticle(newsId);
    }
    public void addToBookmark(int newsId, String title, String mainImage, String type, String date, String content1, String image1, String content2, String author, String custom, String bannerSectionId, String sectionName) {
        sf.setLastBookmarkCount(sf.getLastBookmarkCount()+1); //ADD COUNT
        Log.d(TAG, "addToBookmark:getLastBookmarkCount "+sf.getLastBookmarkCount());

        String sectName = getSectionName(sectionName).toUpperCase(); //
        //insert/save to database
        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, sf.getLastBookmarkCount(), title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectName);
//        BookmarksNewModel bookmarkModel = new BookmarksNewModel(newsId, title, mainImage, type,date,content1,image1,content2,author,custom,bannerSectionId,sectName);
        helper.saveBookmark(bookmarkModel);
        Global.isBookmark = false;
        toastMessage("This article is added to your bookmark.");
//        Toast.makeText(getActivity().getApplicationContext(), "This article is added to your bookmark.", Toast.LENGTH_SHORT).show();
    }
//    private boolean bookmarkArticle(String id) {
//        long unixTime = System.currentTimeMillis() / 1000L;
//        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime+ "inq" + sf.getDeviceId() + "-megaarticle/" + id);
//        apiService.getArticle(String.valueOf(unixTime), sf.getDeviceId(), id, hashed)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new SingleObserver<Article>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        compositeDisposable.add(d);
//                    }
//                    @Override
//                    public void onSuccess(Article article) {
//                        String sectionId = article.getSectionId();
//                        String title = article.getTitle();
//                        String photo = article.getCreative();
//                        String type = article.getType();
//                        String pubdate = article.getPubdate();
//                        String content1 = article.getContent1().isEmpty() ? article.getContent2a() : article.getContent2b();
////                        String content1 = article.getContent() != null ? article.getContent().get(0).toString() : "";
////                        String image1 = article.getInsert() != null ? article.getInsert().getCreative(): "";
//                      String image1 = "";
////                        String content2 = article.getContent() != null ? article.getContent().get(1).toString() : "";
//                        String content2 = article.getContent2b();
//
//                        String author = article.getByline();
//                        String custom = article.getCustom();
//
//                        //Get the SuperShare Details
//
//                        if(!title.isEmpty()){
//                            addToBookmark(Integer.valueOf(id),title,photo,type,pubdate,content1,image1,content2,author,custom,bannerSectionId,sectionId);
//                            bookmarkResult = true;
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        bookmarkResult = false;
//                        Global.isBookmark = false;
//                        String error_msg = Global.getResponseCode(e);
//                        Toast.makeText(getContext().getApplicationContext(), error_msg, Toast.LENGTH_SHORT).show();                    }
//                });
//        return bookmarkResult;
//    }

    private boolean bookmarkArticle(String id) {
        long unixTime = System.currentTimeMillis() / 1000L;
        String hashed = Global.Generate32SHA512(sf.getDailyKey() + unixTime + "inq" + sf.getDeviceId() + "-megaarticle/" + id);
        apiService.getArticle2(String.valueOf(unixTime),sf.getDeviceId(), id, hashed)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try{
                            hasNewDailyKey = false;
                            String content1="";
                            String content2="";
                            String insertNewId="";
                            String insertType="";
                            String insertTitle="";
                            String insertImage="";
                            String insertContent="";
                            String custom="";

                            //Sometimes NO Section_ID and DATE: Occur on ADS Type1
                            String section_id="", pubdate="", author="", title="", photo="", type="";
                            if(response.body().has("section_id")) section_id = !response.body().get("section_id").isJsonNull() ? response.body().get("section_id").getAsString() : "";
                            if(response.body().has("pubdate")) pubdate = !response.body().get("pubdate").isJsonNull() ? response.body().get("pubdate").getAsString() : "";
                            if(response.body().has("byline")) author = !response.body().get("byline").isJsonNull() ? response.body().get("byline").getAsString() : "";
                            if(response.body().has("title")) title = !response.body().get("title").isJsonNull() ? response.body().get("title").getAsString() : "";
                            if(response.body().has("creative")) photo = !response.body().get("creative").isJsonNull() ? response.body().get("creative").getAsString() : "";
                            if(response.body().has("type")) type = !response.body().get("type").isJsonNull() ? response.body().get("type").getAsString() : "";

                            //Check if has an INSERT (for SuperShare)
                            if(response.body().has("insert")){
                                //Check if CONTENT as an ARRAY
                                //response.body().get("content").getAsJsonArray().get(0).getAsString();
//                                content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
//                                content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
//                                insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
//                                insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
//                                insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
//                                insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
//                                insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
//                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");
//
                                if(response.body().has("content")){
                                    content1 = !response.body().get("content").getAsJsonArray().get(0).isJsonNull() ? response.body().get("content").getAsJsonArray().get(0).getAsString() : "";
                                    content2 = !response.body().get("content").getAsJsonArray().get(1).isJsonNull() ? response.body().get("content").getAsJsonArray().get(1).getAsString() : "";
                                }
                                if(response.body().get("insert").getAsJsonObject().has("id")) insertNewId= !response.body().get("insert").getAsJsonObject().get("id").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("id").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("type"))  insertType= !response.body().get("insert").getAsJsonObject().get("type").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("type").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("title")) insertTitle= !response.body().get("insert").getAsJsonObject().get("title").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("title").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("creative")) insertImage= !response.body().get("insert").getAsJsonObject().get("creative").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("creative").getAsString() : "";
                                if(response.body().get("insert").getAsJsonObject().has("content"))  insertContent= !response.body().get("insert").getAsJsonObject().get("content").isJsonNull() ? response.body().get("insert").getAsJsonObject().get("content").getAsString() : "";
                                Log.d(TAG, "onResponse: CONTENT_ HAS_INSERT");

                            }else {
                                //Check if CONTENT as a STRING
                                if(response.body().has("content")) content1 = !response.body().get("content").isJsonNull() ? response.body().get("content").getAsString() : "";
                            }
                            //NOTE:BOOKMARKS has NO RELATED ARTICLE

                            if(!title.isEmpty()){
                                addToBookmark(Integer.valueOf(id),title,photo,type,pubdate,content1,insertImage,content2,author,custom,bannerSectionId,section_id);// ectionId,sf.getSectionName());
                                bookmarkResult = true;
                            }

                        }catch (Exception e){
                            Log.d(TAG, "onResponse: viewArticle Exception: "+ e.getCause());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        if(!hasNewDailyKey) getDailyKey(); //Resolution: For always giving Error After longest time from the backGround. Maybe expiration of DailyKey.

                        bookmarkResult = false;
                        Global.isBookmark = false;
                        String error_msg = Global.getResponseCode(t);
                        //For Developer Debugging check email and name and mobile
                        if(sf.getName().equals(Global.DNAME) && sf.getEmail().equals(Global.DEMAIL) && sf.getMobile().equals(Global.DMOBILE)){
                            toastMessage(TAG+": onFailure: "+ t.getMessage()); // //Display in developer
                        }else{
                            toastMessage(error_msg); // //Display in user
                        }
                    }
                });

        return bookmarkResult;
    }

    public void toastMessage(String message) {
        if(Global.toast!=null) Global.toast.cancel(); //Remove First the existing toast
        Global.toast = Toast.makeText(getContext().getApplicationContext(), message, Toast.LENGTH_SHORT); Global.toast.show(); //Show NEW TOAST
    }

    private String getSectionName(String sectionId) {
        String sectionName = "";
        if(Global.sectionList.size()>0) {
            for(int x=0; x<Global.sectionList.size();x++){
                String id = String.valueOf(Global.sectionList.get(x).getId());
                if(id.equals(sectionId)){
                    sectionName = String.valueOf(Global.sectionList.get(x).getTitle());
                }
            }
        }
        return sectionName;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible){
            Log.d(TAG, "setMenuVisibility: CUSTOM_ IN LifeCycle");
            isFragmentVisible = true;
            //Resolved: Check if sf!=null setMenuVisibility is CALLED first in the LifeCycle so in the firstLaunch this getMyInqNews() will not run because the sf in null or the onCreate() is not yet called:
            //Dont call API if HAS_EVENT
            if(sf!=null && hasLoadedOnce && !hasEvent) { // if(sf!=null && hasLoadedOnce && newsmodel.size() <= 0) {
                isNewsRecollect = true; //Flag
                Log.d(TAG, "setMenuVisibility: getMyInqNews*** ");
                getMyInqNews();
            }
            //Flag for EVENT, set Default always ONLY after CHECKING if(sf!=null && hasLoadedOnce && !isMyInqUpdated), making sure the getMyInqNews() will be called again in the NEXT MenuVisibility
            hasEvent = false;
            isNetworkChanged = false; //Default
            //GET OFFLINE
//            getMyInqCachedNews();
            customHandler.postDelayed(updateTimerThread, 2000);  //start handler runnable
        }
        //OUT
        else{
            isFragmentVisible = false;
            Log.d(TAG, "setMenuVisibility: CUSTOM_ OUT LifeCycle");
            customHandler.removeCallbacks(updateTimerThread);  //stop.. handler runnable
        }
    }
}
